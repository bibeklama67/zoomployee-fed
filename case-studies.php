<?php include("partials/header.php");?>

<!-- bradcrumb -->
<section class="breadcrumb-wrapper">
    <div class="container position-relative">
        <div class="row">
            <div class="col-12">
                <ol class="breadcrumb mb-0">
                    <li class="breadcrumb-item"><a href="index">Home</a></li>
                    <li class="breadcrumb-item active">Case Studies</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!-- bradcrumb -->

<section class="case-studies-listings">
    <div class="container">
        <div class="row heading">
            <div class="col-md-4 title">Case Studies</div>
            <div class="col-md-8 desc">Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit
                officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet. Nulla
                facilisi. Aenean vestibulum nisi nisi, vitae elementum nunc feugiat ac. Vivamus placerat, nisl eu
                vulputate bibendum, mauris elit sodales tortor, id venenatis eros metus ac enim. Sed at metus quis dui
            </div>
        </div>
        <div class="row recently-uploaded">
            <div class="col-md-12">
                <h3>Recently Uploaded</h3>
            </div>
            <div class="col-md-6 case-studies-card-wrapper">
                <a href="case-studies-detail.php" class="case-studies-card red">
                    <div class="img-div">
                        <img src="assets/img/case-studies-1.png" alt="">
                    </div>
                    <div class="text-content">
                        <div class="info">
                            <span class="subtitle">hunting</span>
                            <h3>Springbuck Hunting in Southern Africa</h3>
                        </div>
                        <div class="time">
                            <span><svg width="20" height="20" viewBox="0 0 20 20" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M10 17.5C14.1421 17.5 17.5 14.1421 17.5 10C17.5 5.85786 14.1421 2.5 10 2.5C5.85786 2.5 2.5 5.85786 2.5 10C2.5 14.1421 5.85786 17.5 10 17.5Z"
                                        stroke="white" stroke-width="1.5" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M10 5.83398V10.0007L12.5 12.5007" stroke="white" stroke-width="1.5"
                                        stroke-linecap="round" stroke-linejoin="round" />
                                </svg>
                                12 Minute Read</span>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-6 case-studies-card-wrapper">
                <a href="case-studies-detail.php" class="case-studies-card red">
                    <div class="img-div">
                        <img src="assets/img/case-studies-2.png" alt="">
                    </div>
                    <div class="text-content">
                        <div class="info">
                            <span class="subtitle">hunting</span>
                            <h3>Springbuck Hunting in Southern Africa</h3>
                        </div>
                        <div class="time">
                            <span><svg width="20" height="20" viewBox="0 0 20 20" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M10 17.5C14.1421 17.5 17.5 14.1421 17.5 10C17.5 5.85786 14.1421 2.5 10 2.5C5.85786 2.5 2.5 5.85786 2.5 10C2.5 14.1421 5.85786 17.5 10 17.5Z"
                                        stroke="white" stroke-width="1.5" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M10 5.83398V10.0007L12.5 12.5007" stroke="white" stroke-width="1.5"
                                        stroke-linecap="round" stroke-linejoin="round" />
                                </svg>
                                12 Minute Read</span>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="row listing-wrapper">
            <div class="col-md-4 case-studies-card-wrapper">
                <a href="case-studies-detail.php" class="case-studies-card red">
                    <div class="img-div">
                        <img src="assets/img/case-studies-2.png" alt="">
                    </div>
                    <div class="text-content">
                        <div class="info">
                            <span class="subtitle">hunting</span>
                            <h3>Springbuck Hunting in Southern Africa</h3>
                        </div>
                        <div class="time">
                            <span><svg width="20" height="20" viewBox="0 0 20 20" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M10 17.5C14.1421 17.5 17.5 14.1421 17.5 10C17.5 5.85786 14.1421 2.5 10 2.5C5.85786 2.5 2.5 5.85786 2.5 10C2.5 14.1421 5.85786 17.5 10 17.5Z"
                                        stroke="white" stroke-width="1.5" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M10 5.83398V10.0007L12.5 12.5007" stroke="white" stroke-width="1.5"
                                        stroke-linecap="round" stroke-linejoin="round" />
                                </svg>
                                12 Minute Read</span>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4 case-studies-card-wrapper">
                <a href="case-studies-detail.php" class="case-studies-card red">
                    <div class="img-div">
                        <img src="assets/img/case-studies-1.png" alt="">
                    </div>
                    <div class="text-content">
                        <div class="info">
                            <span class="subtitle">hunting</span>
                            <h3>Springbuck Hunting in Southern Africa</h3>
                        </div>
                        <div class="time">
                            <span><svg width="20" height="20" viewBox="0 0 20 20" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M10 17.5C14.1421 17.5 17.5 14.1421 17.5 10C17.5 5.85786 14.1421 2.5 10 2.5C5.85786 2.5 2.5 5.85786 2.5 10C2.5 14.1421 5.85786 17.5 10 17.5Z"
                                        stroke="white" stroke-width="1.5" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M10 5.83398V10.0007L12.5 12.5007" stroke="white" stroke-width="1.5"
                                        stroke-linecap="round" stroke-linejoin="round" />
                                </svg>
                                12 Minute Read</span>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4 case-studies-card-wrapper">
                <a href="#" class="case-studies-card red">
                    <div class="img-div">
                        <img src="assets/img/case-studies-1.png" alt="">
                    </div>
                    <div class="text-content">
                        <div class="info">
                            <span class="subtitle">hunting</span>
                            <h3>Springbuck Hunting in Southern Africa</h3>
                        </div>
                        <div class="time">
                            <span><svg width="20" height="20" viewBox="0 0 20 20" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M10 17.5C14.1421 17.5 17.5 14.1421 17.5 10C17.5 5.85786 14.1421 2.5 10 2.5C5.85786 2.5 2.5 5.85786 2.5 10C2.5 14.1421 5.85786 17.5 10 17.5Z"
                                        stroke="white" stroke-width="1.5" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M10 5.83398V10.0007L12.5 12.5007" stroke="white" stroke-width="1.5"
                                        stroke-linecap="round" stroke-linejoin="round" />
                                </svg>
                                12 Minute Read</span>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4 case-studies-card-wrapper">
                <a href="#" class="case-studies-card red">
                    <div class="img-div">
                        <img src="assets/img/case-studies-2.png" alt="">
                    </div>
                    <div class="text-content">
                        <div class="info">
                            <span class="subtitle">hunting</span>
                            <h3>Springbuck Hunting in Southern Africa</h3>
                        </div>
                        <div class="time">
                            <span><svg width="20" height="20" viewBox="0 0 20 20" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M10 17.5C14.1421 17.5 17.5 14.1421 17.5 10C17.5 5.85786 14.1421 2.5 10 2.5C5.85786 2.5 2.5 5.85786 2.5 10C2.5 14.1421 5.85786 17.5 10 17.5Z"
                                        stroke="white" stroke-width="1.5" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M10 5.83398V10.0007L12.5 12.5007" stroke="white" stroke-width="1.5"
                                        stroke-linecap="round" stroke-linejoin="round" />
                                </svg>
                                12 Minute Read</span>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4 case-studies-card-wrapper">
                <a href="#" class="case-studies-card red">
                    <div class="img-div">
                        <img src="assets/img/case-studies-1.png" alt="">
                    </div>
                    <div class="text-content">
                        <div class="info">
                            <span class="subtitle">hunting</span>
                            <h3>Springbuck Hunting in Southern Africa</h3>
                        </div>
                        <div class="time">
                            <span><svg width="20" height="20" viewBox="0 0 20 20" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M10 17.5C14.1421 17.5 17.5 14.1421 17.5 10C17.5 5.85786 14.1421 2.5 10 2.5C5.85786 2.5 2.5 5.85786 2.5 10C2.5 14.1421 5.85786 17.5 10 17.5Z"
                                        stroke="white" stroke-width="1.5" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M10 5.83398V10.0007L12.5 12.5007" stroke="white" stroke-width="1.5"
                                        stroke-linecap="round" stroke-linejoin="round" />
                                </svg>
                                12 Minute Read</span>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4 case-studies-card-wrapper">
                <a href="#" class="case-studies-card red">
                    <div class="img-div">
                        <img src="assets/img/case-studies-1.png" alt="">
                    </div>
                    <div class="text-content">
                        <div class="info">
                            <span class="subtitle">hunting</span>
                            <h3>Springbuck Hunting in Southern Africa</h3>
                        </div>
                        <div class="time">
                            <span><svg width="20" height="20" viewBox="0 0 20 20" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M10 17.5C14.1421 17.5 17.5 14.1421 17.5 10C17.5 5.85786 14.1421 2.5 10 2.5C5.85786 2.5 2.5 5.85786 2.5 10C2.5 14.1421 5.85786 17.5 10 17.5Z"
                                        stroke="white" stroke-width="1.5" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M10 5.83398V10.0007L12.5 12.5007" stroke="white" stroke-width="1.5"
                                        stroke-linecap="round" stroke-linejoin="round" />
                                </svg>
                                12 Minute Read</span>
                        </div>
                    </div>
                </a>
            </div>
            <div class="pagination">
                <a class="prev page-numbers" href="#">Previous</a>
                <span aria-current="page" class="page-numbers current">1</span>
                <a class="page-numbers" href="#">2</a>
                <a class="page-numbers" href="#">3</a>
                <a class="page-numbers" href="#">4</a>
                <a class="page-numbers" href="#">5</a>
                <a class="next page-numbers" href="#">Next</a>
            </div>
        </div>
    </div>
</section>
<?php include("partials/footer.php");?>