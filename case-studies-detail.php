<?php include("partials/header.php");?>
<!-- bradcrumb -->
<section class="breadcrumb-wrapper">
    <div class="container position-relative">
        <div class="row">
            <div class="col-12">
                <ol class="breadcrumb mb-0">
                    <li class="breadcrumb-item"><a href="index">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">Case Studies</a></li>
                    <li class="breadcrumb-item active">Hunting Deers in the Chitwan’s Jungle</li>
                </ol>
            </div>
        </div>
    </div>
</section>

<section class="case-studies-detail welcome-banner">
    <div class="container">
        <div class="row">
            <div class="col-md-8 mx-auto content-wrapper">
                <div class="heading">
                    <h2>ELK Hunting Tips and Guiides and Outfitters</h2>
                </div>
                <div class="info">
                    <p>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat
                        duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet. </p>
                    <div class="social-share">
                        <p>Share in social</p>
                        <div class="links">
                            <ul class="list-unstyled d-flex mb-0 justify-content-center">
                                <li><a href="https://www.facebook.com/DANUBEHOMEBYBRIHATGROUP/" target="_blank"><i
                                            class="fab fa-facebook-f"></i> </a></li>
                                <li><a href="https://instagram.com/danubehomenepal" target="_blank"><i
                                            class="fab fa-instagram"></i> </a></li>
                                <li><a href="https://twitter.com/danubehome?lang=en" target="_blank"><i
                                            class="fab fa-twitter"></i> </a></li>

                                <li><a href="https://www.linkedin.com/company/danube-home" target="_blank"><i
                                            class="fab fa-linkedin-in"></i> </a></li>

                                <li><a href="https://www.youtube.com/channel/UCp2dYWlDaAq0UVm2VI5OFdw"
                                        target="_blank"><i class="fab fa-youtube"></i> </a></li>

                                <li><a href="https://www.snapchat.com/add/danubehome" target="_blank"><i
                                            class="fab fa-snapchat-ghost"></i> </a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="case-studies case-studies-detail">
    <div class="container">
        <div class="row feature-wrapper justify-content-center">
            <div class="col-md-6 col-xl-2 feature-card">
                <div class="img-div">
                    <img src="assets/img/duration-icon.svg" alt="">
                </div>
                <div class="text-content">
                    <span class="title">Duration</span>
                    <span class="value">10 Days</span>
                </div>
            </div>
            <div class="col-md-6 col-xl-2 feature-card">
                <div class="img-div">
                    <img src="assets/img/area-icon.svg" alt="">
                </div>
                <div class="text-content">
                    <span class="title">Hunting Area</span>
                    <span class="value">Chitwan</span>
                </div>
            </div>
            <div class="col-md-6 col-xl-2 feature-card">
                <div class="img-div">
                    <img src="assets/img/animal-icon.svg" alt="">
                </div>
                <div class="text-content">
                    <span class="title">Animal</span>
                    <span class="value">Tiger</span>
                </div>
            </div>
            <div class="col-md-6 col-xl-2 feature-card">
                <div class="img-div">
                    <img src="assets/img/altitude-icon.svg" alt="">
                </div>
                <div class="text-content">
                    <span class="title">Altitude</span>
                    <span class="value">500 Km</span>
                </div>
            </div>
        </div>
        <div class="introduction row">
            <div class="col-xl-8 mx-auto heading">
                <h2>About this trip</h2>
            </div>
            <div class="col-xl-8 mx-auto text-content expandable-content mh-270">
                <p>Nunc sed augue lacus viverra vitae congue eu consequat ac. Proin fermentum leo vel orci
                    porta. Non sodales neque sodales ut etiam. Pharetra magna ac placerat vestibulum lectus
                    mauris. Feugiat sed lectus vestibulum mattis ullamcorper. In fermentum posuere urna nec
                    tincidunt praesent semper feugiat nibh. Volutpat sed cras ornare arcu. Auctor eu augue ut
                    lectus. Viverra nibh cras pulvinar mattis nunc. Morbi leo urna molestie at. Lacus
                    suspendisse faucibus interdum posuere lorem. Eget dolor morbi non arcu risus quis varius
                    quam quisque. Senectus et netus et malesuada fames ac turpis egestas sed. Eu volutpat odio
                    facilisis mauris sit amet. Morbi tempus iaculis urna id volutpat lacus laoreet. At in tellus
                    integer feugiat scelerisque varius. Magna ac placerat vestibulum lectus. Sed euismod nisi
                    porta lorem mollis aliquam.</p>
                <p>Nunc sed augue lacus viverra vitae congue eu consequat ac. Proin fermentum leo vel orci
                    porta. Non sodales neque sodales ut etiam. Pharetra magna ac placerat vestibulum lectus
                    mauris. Feugiat sed lectus vestibulum mattis ullamcorper. In fermentum posuere urna nec
                    tincidunt praesent semper feugiat nibh. Volutpat sed cras ornare arcu. Auctor eu augue ut
                    lectus. Viverra nibh cras pulvinar mattis nunc. Morbi leo urna molestie at. Lacus
                    suspendisse faucibus interdum posuere lorem. Eget dolor morbi non arcu risus quis varius
                    quam quisque. Senectus et netus et malesuada fames ac turpis egestas sed. Eu volutpat odio
                    facilisis mauris sit amet. Morbi tempus iaculis urna id volutpat lacus laoreet. At in tellus
                    integer feugiat scelerisque varius. Magna ac placerat vestibulum lectus. Sed euismod nisi
                    porta lorem mollis aliquam.</p>
            </div>
            <div class="read-more-btn">
                Show More
            </div>
        </div>
    </div>
</section>

<section class="home testimonials case-studies">
    <div class="container">
        <!-- <div class="row heading">
            <div class="col-md-5 title">
                <span>Testimonials</span>
                <h2>SOME EXPERIENCES FROM OUR HUNTERS</h2>
            </div>
            <div class="col-md-5 desc">
                <p>
                    Et has minim elitr intellegat. Mea aeterno eleifend antiopam ad, nam no suscipit quaerendum. At nam
                    minimum ponderum. Est audiam animal molestiae te. Ex duo eripuit mentitum.Et has minim elitr
                    intellegat. Mea aeterno eleifend antiopam ad, nam no suscipit quaerendum. At nam minimum ponderum.
                </p>
            </div>
            <div class="col-md-2 action">
                <a href="#">View all</a>
            </div>
        </div> -->
        <div class="row p-0">
            <div class="owl-theme owl-carousel testimonial-carousel">
                <div class="item testimonial-card">
                    <div class="details">
                        <div class="img-div">
                            <img src="assets/img/testimonial-img.png" alt="">
                        </div>
                        <div class="info">
                            <span>Theresa Webb</span>
                            <span>Australia</span>
                        </div>
                    </div>
                    <div class="text-content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua. Nec nam aliquam sem et tortor consequat id porta nibh. Neque
                            sodales ut etiam sit amet nisl. Elementum sagittis vitae et leo duis ut diam quam nulla.
                            Magna fermentum iaculis eu non diam phasellus vestibulum. Id aliquet risus feugiat in ante
                            metus dictum at tempor. Ipsum dolor sit amet consectetur adipiscing. Urna neque viverra
                            justo nec ultrices. Aliquet eget sit amet tellus. Vulputate enim nulla aliquet porttitor
                            lacus luctus. Diam maecenas ultricies mi eget. Velit </p>
                    </div>
                </div>
                <div class="item testimonial-card">
                    <div class="details">
                        <div class="img-div">
                            <img src="assets/img/testimonial-img.png" alt="">
                        </div>
                        <div class="info">
                            <span>Theresa Webb</span>
                            <span>Australia</span>
                        </div>
                    </div>
                    <div class="text-content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua. Nec nam aliquam sem et tortor consequat id porta nibh. Neque
                            sodales ut etiam sit amet nisl. Elementum sagittis vitae et leo duis ut diam quam nulla.
                            Magna fermentum iaculis eu non diam phasellus vestibulum. Id aliquet risus feugiat in ante
                            metus dictum at tempor. Ipsum dolor sit amet consectetur adipiscing. Urna neque viverra
                            justo nec ultrices. Aliquet eget sit amet tellus. Vulputate enim nulla aliquet porttitor
                            lacus luctus. Diam maecenas ultricies mi eget. Velit </p>
                    </div>
                </div>
                <div class="item testimonial-card">
                    <div class="details">
                        <div class="img-div">
                            <img src="assets/img/testimonial-img.png" alt="">
                        </div>
                        <div class="info">
                            <span>Theresa Webb</span>
                            <span>Australia</span>
                        </div>
                    </div>
                    <div class="text-content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua. Nec nam aliquam sem et tortor consequat id porta nibh. Neque
                            sodales ut etiam sit amet nisl. Elementum sagittis vitae et leo duis ut diam quam nulla.
                            Magna fermentum iaculis eu non diam phasellus vestibulum. Id aliquet risus feugiat in ante
                            metus dictum at tempor. Ipsum dolor sit amet consectetur adipiscing. Urna neque viverra
                            justo nec ultrices. Aliquet eget sit amet tellus. Vulputate enim nulla aliquet porttitor
                            lacus luctus. Diam maecenas ultricies mi eget. Velit </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="gallery-section">
    <div class="container">
        <div class="row">
            <div class="col-12 heading">
                <h2>
                    Some Recent Images of this Trip
                </h2>
            </div>
        </div>

    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 px-0 owl-theme owl-carousel gallery-carousel" id="lightgallery">
                <div class="item gallery-card-wrapper" data-category="csw" data-src="assets/img/gallery-1.png"
                    data-sub-html="">
                    <img src="assets/img/gallery-1.png" alt="">
                </div>
                <div class="item gallery-card-wrapper" data-category="csw" data-src="assets/img/gallery-2.png"
                    data-sub-html="">
                    <img src="assets/img/gallery-2.png" alt="">
                </div>
                <div class="item gallery-card-wrapper" data-category="csw" data-src="assets/img/gallery-3.png"
                    data-sub-html="">
                    <img src="assets/img/gallery-3.png" alt="">
                </div>
                <div class="item gallery-card-wrapper" data-category="csw" data-src="assets/img/gallery-4.png"
                    data-sub-html="">
                    <img src="assets/img/gallery-4.png" alt="">
                </div>
                <div class="item gallery-card-wrapper" data-category="csw" data-src="assets/img/gallery-1.png"
                    data-sub-html="">
                    <img src="assets/img/gallery-1.png" alt="">
                </div>
                <div class="item gallery-card-wrapper" data-category="csw" data-src="assets/img/gallery-2.png"
                    data-sub-html="">
                    <img src="assets/img/gallery-2.png" alt="">
                </div>
                <div class="item gallery-card-wrapper" data-category="csw" data-src="assets/img/gallery-3.png"
                    data-sub-html="">
                    <img src="assets/img/gallery-3.png" alt="">
                </div>
                <div class="item gallery-card-wrapper" data-category="csw" data-src="assets/img/gallery-4.png"
                    data-sub-html="">
                    <img src="assets/img/gallery-4.png" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="more-packages  pb-800">
    <div class="container">
        <div class="row">
            <div class="col-12 heading">
                <h2>More Exciting Packages</h2>
            </div>
        </div>
        <div class="row list-wrapper">
            <div class="col-md-3 package-card-wrapper">
                <a href="package-detail.php" class="package-card d-block">
                    <div class="overlay"></div>
                    <div class="mask"></div>
                    <img src="assets/img/package1.png" alt="">
                    <div class="text-content">
                        <span class="title">Package Name</span>
                        <span class="info">5 days Max 10 People</span>
                    </div>
                </a>
            </div>
            <div class="col-md-3 package-card-wrapper">
                <a href="package-detail.php" class="package-card d-block">
                    <div class="overlay"></div>
                    <div class="mask"></div>
                    <img src="assets/img/package1.png" alt="">
                    <div class="text-content">
                        <span class="title">Package Name</span>
                        <span class="info">5 days Max 10 People</span>
                    </div>
                </a>
            </div>
            <div class="col-md-3 package-card-wrapper">
                <a href="package-detail.php" class="package-card d-block">
                    <div class="overlay"></div>
                    <div class="mask"></div>
                    <img src="assets/img/package1.png" alt="">
                    <div class="text-content">
                        <span class="title">Package Name</span>
                        <span class="info">5 days Max 10 People</span>
                    </div>
                </a>
            </div>
            <div class="col-md-3 package-card-wrapper">
                <a href="package-detail.php" class="package-card d-block">
                    <div class="overlay"></div>
                    <div class="mask"></div>
                    <img src="assets/img/package1.png" alt="">
                    <div class="text-content">
                        <span class="title">Package Name</span>
                        <span class="info">5 days Max 10 People</span>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>
<!-- bradcrumb -->
<?php include("partials/footer.php");?>