<?php include("partials/header.php")?>
<section class="welcome-banner position-relative">
    <img src="assets/img/welcome-bg.jpg" alt="">
    <div class="container d-flex h-100 align-items-center">
        <div class="row">
            <div class="col-xl-6 text-content">
                <span>Remote Staff
                    $7.00-$8.50/hour</span>
                <p class="text-white">We are the leading provider of remote admin, marketing, sales, bookkeeping,
                    design, customer support,
                    and e-commerce staff for small businesses.</p>
                <h6 class="text-white">No additional fees. Interview today. Cancel any time. </h6>
            </div>
        </div>
    </div>
</section>
<section class="about introduction">
    <div class="container">
        <div class="row">
            <div class="mx-auto col-xl-8 text-content">
                <div class="heading">
                    <h4>
                        We are an experienced and friendly team of experts who are passionate about supporting amazing
                        small
                        businesses and self-employed professionals who cannot afford to use expensive recruitment
                        agencies
                        but need talented and highly skilled staff to help them grow and succeed.
                    </h4>
                </div>
                <p>The founder of the company, Adam, is an entrepreneur who started and developed a number of small
                    businesses and always struggled to find affordable professionals to join his team because small
                    businesses have limited budgets and cannot afford expensive benefit packages, above-average
                    salaries, and outrageously priced offices in Manhattan. At the same time, small businesses more than
                    anyone else depend on finding highly skilled staff because in a small team, every specialist can
                    make a huge difference. Over the years, Adam tried every possible option, from hiring experts on
                    Fiverr and UpWork to employing students and interns for admin support. All these options were more
                    affordable than hiring full-time, qualified employees but the quality of their work varied greatly
                    so the outcome was a bit of a gamble. Moreover, it took him weeks to find and hire every new person,
                    and then days to onboard them and share all the information they need to start working with them.
                    Quite often, they left the company after just a few weeks or months, as soon as they had found
                    another job with a bigger company that was able to offer a better benefits package.
                </p>
                <p>
                    When Adam hired his first remote assistant from the Philippines, he instantly saw a huge difference.
                    Rose was extremely hard-working, well-educated, smart, proactive, full of ideas and energy, and an
                    incredible problem solver. She came at under $10 per hour, was happy to work 50 hours per week, and
                    had a positive personality that made working with her a pleasure. Adam’s friends, BNI network
                    members, and clients started noticing Rose’s impact on his business and began asking Adam if he
                    could find them someone similar. This is how Zoomployee started, and our team is still on a mission
                    to help small businesses, investors, startups, entrepreneurs, and self-employed professionals
                    succeed. We go above and beyond by hand-selecting prequalified, college-educated professionals out
                    of our fully vetted database. We’re confident we can fulfil any solution your company needs quickly
                    and with minimal effort from you. The days of being dependent on traditional employees are over.
                    Welcome to Zoomployee.</p>
                <div class="btn-wrapper text-center">
                    <a href="#" class="btn btn-success mx-auto">See our Services and Rates</a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="about contact-us d-sm-flex">
    <div class="img-div">
        <img src="assets/img/about-1.jpg" alt="">
    </div>
    <div class="text-content container">
        <div class="row justify-content-center h-100 align-items-center">
            <div class="col-8">
                <h2>“We love what we do and as a result, you will too”
                </h2>
                <h5>Adam Tabari, Founder of Zoomployee</h5>
                <a href="#" class="btn btn-success">Contact Us Now</a>
            </div>
        </div>
    </div>
</section>
<section class="about team">
    <div class="container">
        <div class="row">
            <div class="col-xl-8 mx-auto heading text-center">
                <h2>Meet Our Amazing Team
                </h2>
                <p>It is always difficult to write authentically about yourself because people tend to underplay what’s
                    amazing about them while keeping silent about their most unique quirks. We happen to think that it
                    is these irresistible quirks and flaws that make people interesting and unique. Therefore, we asked
                    our clients to do the write-ups below and describe our team members from what they know about them.
                    Our team is all about people, and we hope that these mini-profiles give you an insight into our
                    personalities and values – warts and all!
                </p>
            </div>
            <div class="team-card d-sm-flex">
                <div class="img-div">
                    <img src="assets/img/team1.jpg" alt="">
                </div>
                <div class="text-content">
                    <div class="info">
                        <h3>Adam Tabari <span>Founder & Fearless leader</span></h3>
                    </div>
                    <div class="expandable-text desc">
                        <p>Adam is the fearless leader of the team and is known for his passion for singing and his
                            extraordinary ability to spread positive vibes. If you are in a meeting and someone starts
                            singing love songs like Stand By Me and Tennessee Whiskey, you can bet it is Adam. With a
                            huge smile on his face every minute of the day – except maybe for when he opens his bills –
                            Adam oozes genuine empathy, support, and optimism, and is clearly determined to help people
                            in every way he can. Everyone who has ever met Adam will also know that he is a committed
                            family man – he talks non-stop about his wife Alexandra and their sons Levy, 4 and Aaron, 2.
                            Adam met his Paris-born wife on a dating app, which may explain why he is such a great
                            believer in the life-changing power of technology. </p>
                    </div>
                    <div class="read-more-btn">View more</div>
                </div>
            </div>
            <div class="team-card d-sm-flex">
                <div class="img-div">
                    <img src="assets/img/team1.jpg" alt="">
                </div>
                <div class="text-content">
                    <div class="info">
                        <h3>Adam Tabari <span>Founder & Fearless leader</span></h3>
                    </div>
                    <div class="expandable-text desc">
                        <p>Adam is the fearless leader of the team and is known for his passion for singing and his
                            extraordinary ability to spread positive vibes. If you are in a meeting and someone starts
                            singing love songs like Stand By Me and Tennessee Whiskey, you can bet it is Adam. With a
                            huge smile on his face every minute of the day – except maybe for when he opens his bills –
                            Adam oozes genuine empathy, support, and optimism, and is clearly determined to help people
                            in every way he can. Everyone who has ever met Adam will also know that he is a committed
                            family man – he talks non-stop about his wife Alexandra and their sons Levy, 4 and Aaron, 2.
                            Adam met his Paris-born wife on a dating app, which may explain why he is such a great
                            believer in the life-changing power of technology. </p>
                    </div>
                    <div class="read-more-btn">View more</div>
                </div>
            </div>
            <div class="team-card d-sm-flex">
                <div class="img-div">
                    <img src="assets/img/team1.jpg" alt="">
                </div>
                <div class="text-content">
                    <div class="info">
                        <h3>Adam Tabari <span>Founder & Fearless leader</span></h3>
                    </div>
                    <div class="expandable-text desc">
                        <p>Adam is the fearless leader of the team and is known for his passion for singing and his
                            extraordinary ability to spread positive vibes. If you are in a meeting and someone starts
                            singing love songs like Stand By Me and Tennessee Whiskey, you can bet it is Adam. With a
                            huge smile on his face every minute of the day – except maybe for when he opens his bills –
                            Adam oozes genuine empathy, support, and optimism, and is clearly determined to help people
                            in every way he can. Everyone who has ever met Adam will also know that he is a committed
                            family man – he talks non-stop about his wife Alexandra and their sons Levy, 4 and Aaron, 2.
                            Adam met his Paris-born wife on a dating app, which may explain why he is such a great
                            believer in the life-changing power of technology. </p>
                    </div>
                    <div class="read-more-btn">View more</div>
                </div>
            </div>
            <div class="team-card d-sm-flex">
                <div class="img-div">
                    <img src="assets/img/team1.jpg" alt="">
                </div>
                <div class="text-content">
                    <div class="info">
                        <h3>Adam Tabari <span>Founder & Fearless leader</span></h3>
                    </div>
                    <div class="expandable-text desc">
                        <p>Adam is the fearless leader of the team and is known for his passion for singing and his
                            extraordinary ability to spread positive vibes. If you are in a meeting and someone starts
                            singing love songs like Stand By Me and Tennessee Whiskey, you can bet it is Adam. With a
                            huge smile on his face every minute of the day – except maybe for when he opens his bills –
                            Adam oozes genuine empathy, support, and optimism, and is clearly determined to help people
                            in every way he can. Everyone who has ever met Adam will also know that he is a committed
                            family man – he talks non-stop about his wife Alexandra and their sons Levy, 4 and Aaron, 2.
                            Adam met his Paris-born wife on a dating app, which may explain why he is such a great
                            believer in the life-changing power of technology. </p>
                    </div>
                    <div class="read-more-btn">View more</div>
                </div>
            </div>
            <div class="team-card d-sm-flex">
                <div class="img-div">
                    <img src="assets/img/team1.jpg" alt="">
                </div>
                <div class="text-content">
                    <div class="info">
                        <h3>Adam Tabari <span>Founder & Fearless leader</span></h3>
                    </div>
                    <div class="expandable-text desc">
                        <p>Adam is the fearless leader of the team and is known for his passion for singing and his
                            extraordinary ability to spread positive vibes. If you are in a meeting and someone starts
                            singing love songs like Stand By Me and Tennessee Whiskey, you can bet it is Adam. With a
                            huge smile on his face every minute of the day – except maybe for when he opens his bills –
                            Adam oozes genuine empathy, support, and optimism, and is clearly determined to help people
                            in every way he can. Everyone who has ever met Adam will also know that he is a committed
                            family man – he talks non-stop about his wife Alexandra and their sons Levy, 4 and Aaron, 2.
                            Adam met his Paris-born wife on a dating app, which may explain why he is such a great
                            believer in the life-changing power of technology. </p>
                    </div>
                    <div class="read-more-btn">View more</div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include("partials/footer.php")?>