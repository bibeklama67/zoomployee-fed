$(function () {
    $(document).ready(function () {
        $("#pre-loader").hide();
        $('.home-testimonial-carousel').owlCarousel({
            loop: true,
            margin: 0,
            nav: false,
            autoplay: false,
            autoplayTimeout: 4000,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1000: {
                    items: 1
                }
            }
        })
       
    });

    // returntotop
    $(window).scroll(function () {
        if ($(this).scrollTop() >= 200) { $('#return-to-top').show(200); }
        else { $('#return-to-top').hide(200); }
    });
    $('#return-to-top').click(function () {
        $('body,html').animate({ scrollTop: 0 }, 1000);
    });
    // sticking the navbar
    $(function () {
        var header = $(".fixed-top");
        $(window).scroll(function () {
            var scroll = $(window).scrollTop();
            if (scroll >= 100) {
                header.removeClass("fixed-top").addClass("sticky-top");
            } else {
                header.removeClass("sticky-top").addClass("fixed-top");
            }
        });
    });

});
