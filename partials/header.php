    <?php
    $link = $_SERVER['PHP_SELF'];
    $link_array = explode('/', $link);
    $page = end($link_array);
    ?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css"
            href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
        <link rel="stylesheet" type="text/css"
            href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" />
        <link rel="stylesheet" type="text/css"
            href="https://cdn.jsdelivr.net/npm/lightgallery@1.6.11/dist/css/lightgallery.min.css">
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
        <link rel="shortcut icon" href="assets/img/logo.png">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/animate.css">
        <title>Zoomployee</title>
    </head>

    <body>
        <!-- pre-loader -->
        <!-- <div id="pre-loader">
            <img src="assets/img/pre-loader.gif" alt="">
        </div> -->
        <!-- navigation -->
        <header class="sticky">
            <nav class="navbar navbar-expand-xl navbar-light bg-light fixed-top">
                <div class="container">
                    <a class="navbar-brand center-item" href="/zoomployee">
                        <img src="assets/img/logo.png" alt="" class="d-inline-block align-text-top">
                    </a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                        data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false"
                        aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link " aria-current="page" href="/zoomployee">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" aria-current="page" href="packages.php">Packages & Fees</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" aria-current="page" href="about.php">About</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" aria-current="page" href="blogs.php">Blog</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" aria-current="page" href="contact-us.php">Contact</a>
                            </li>
                            <li class="nav-item call">
                                <span>Call us Today</span>
                                <a href="#">(786) 723-7237</a>
                            </li>
                            <li class="nav-item">
                                <img src="assets/img/logo-alt.png" alt="">
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        <!-- navigation -->