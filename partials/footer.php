<footer class="position-relative footer">
    <img class="saturn" src="assets/img/saturn.png" alt="">
    <img class="meteor" src="assets/img/meteor.png" alt="">
    <div class="container footer-wrapper position-relative">
        <div class="row main-content">
            <div class="col-md-6 col-lg-3 footer-content">
                <h6>Contact Us</h6>
                <ul>
                    <li>3029 NE 188th St Suite 1108</li>
                    <li>Aventura, Florida 33180, US</li>
                    <li><a href="#p">Call: +1 (786)-723-7237</a></li>
                    <li><a href="#p">info@zoomployee.com</a></li>
                </ul>
            </div>
            <div class="col-md-6 col-lg-4 footer-content">
                <h6>Our Space Office</h6>
                <ul>
                    <li>Zoomployee LLC</li>
                    <li>RA: 06h 20m 01.6s DEC: +69° 18' 40.7"</li>
                    <li>Distance: 174.98 light years</li>
                    <li>Registration # 4064-67853-5166590</li>
                </ul>
            </div>
            <div class="col-md-6 col-lg-3 footer-content">
                <h6>Quick Links</h6>
                <ul>
                    <li><a href="faq.php">Home</a></li>
                    <li><a href="case-studies.php">Packages & Fees</a></li>
                    <li><a href="#">About</a></li>
                    <li><a href="#">Blog</a></li>
                    <li><a href="#">Contact</a></li>
                </ul>
            </div>
            <div class="col-md-6 col-lg-2 footer-content img-div">
                <img src="assets/img/footer-img.png" alt="">
            </div>
        </div>
        <div class="row lower-footer-wrapper">
            <div class="col-md-6">
                <span>&#169;Copyright 2021. All rights reserved. <strong>Zoomployee LLC</strong></span>
            </div>
            <div class="col-md-6">
                <ul class="list-unstyled d-flex mb-0 social-links-wrapper">
                    <li><a href="https://www.facebook.com/DANUBEHOMEBYBRIHATGROUP/" target="_blank"><i
                                class="fab fa-facebook-f"></i> </a></li>
                    <li><a href="https://instagram.com/danubehomenepal" target="_blank"><i class="fab fa-instagram"></i>
                        </a></li>
                    <li><a href="https://www.linkedin.com/company/danube-home" target="_blank"><i
                                class="fab fa-linkedin-in"></i> </a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<a href="javascript:" id="return-to-top">
    <svg class="" focusable="false" viewBox="0 0 24 24" aria-hidden="true" data-testid="KeyboardArrowUpIcon">
        <path d="M7.41 15.41 12 10.83l4.59 4.58L18 14l-6-6-6 6z"></path>
    </svg>
</a>
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.lazyload/1.9.1/jquery.lazyload.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous">
</script>
<script type="text/javascript" src="assets/js/style.js"></script>
<script type="text/javascript" src="assets/js/wow.min.js"></script>
</script>
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js">
</script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/lightgallery@1.6.11/dist/js/lightgallery-all.min.js">
</script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://unpkg.com/feather-icons"></script>
<script>
$(document).ready(function() {
    $("#lightgallery").lightGallery({
        selector: ".item"
    });
});
</script>
<script>
feather.replace()
</script>
<script>
new WOW().init();
</script>
</body>

</html>