<?php include("partials/header.php");?>
<section class="page-not-found pb-800">
    <div class="container">
        <div class="row">
            <div class="col-md-6 mx-auto text-content text-center">
                <span>OPPS !!!</span>
                <h2>404 <span>Page Error</span></h2>
                <p>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat
                    duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.</p>
                <a href="#"><i data-feather="corner-up-left"></i>Take me <span> Home</span></a>
            </div>
            <div class="col-md-8 mx-auto img-wrapper">
                <img src="assets/img/404.png" alt="">
            </div>
        </div>
    </div>
</section>
<?php include("partials/footer.php");?>