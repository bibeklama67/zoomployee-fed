<?php include("partials/header.php")?>
<section class="welcome-banner position-relative">
    <img src="assets/img/welcome-bg.jpg" alt="">
    <div class="container d-flex h-100 align-items-center">
        <div class="row">
            <div class="col-xl-6 text-content">
                <span>Remote Staff
                    $7.00-$8.50/hour</span>
                <h6 class="text-white">No additional fees. Interview today. Cancel any time. </h6>
            </div>
        </div>
    </div>
</section>

<section class="blog blog-listings">
    <div class="container">
        <div class="row">
            <div class="col-md-6 blog-card-wrapper">
                <div class="blog-card">
                    <h6>Monitoring Productivity of Your Remote Staff</h6>
                    <p>One of the biggest challenges of working with staff who work from home is monitoring their
                        productivity and performance. Even though in a traditional office, business owners cannot
                        oversee every minute of what their employees are doing and research shows…
                    </p>
                    <div class="d-flex justify-content-between">
                        <a href="blog-detail.php">Read more</a>
                        <span>December 4,2021</span>
                    </div>
                </div>
            </div>
            <div class="col-md-6 blog-card-wrapper">
                <div class="blog-card">
                    <h6>Communicating With
                        Remote Staff</h6>
                    <p>One of the key challenges for every business owner who works with remote staff is how to
                        communicate with remote team members effectively. Training, liaising with, managing, assigning
                        tasks to, and motivating someone you have never seen in person has…
                    </p>
                    <div class="d-flex justify-content-between">
                        <a href="#">Read more</a>
                        <span>December 4,2021</span>
                    </div>
                </div>
            </div>
            <div class="col-md-6 blog-card-wrapper">
                <div class="blog-card">
                    <h6>Monitoring Productivity of Your Remote Staff</h6>
                    <p>One of the biggest challenges of working with staff who work from home is monitoring their
                        productivity and performance.
                    </p>
                    <div class="d-flex justify-content-between">
                        <a href="#">View Detail</a>
                        <span>December 4,2021</span>
                    </div>
                </div>
            </div>
            <div class="col-md-6 blog-card-wrapper">
                <div class="blog-card">
                    <h6>Monitoring Productivity of Your Remote Staff</h6>
                    <p>One of the biggest challenges of working with staff who work from home is monitoring their
                        productivity and performance.
                    </p>
                    <div class="d-flex justify-content-between">
                        <a href="#">View Detail</a>
                        <span>December 4,2021</span>
                    </div>
                </div>
            </div>
            <div class="col-md-6 blog-card-wrapper">
                <div class="blog-card">
                    <h6>Monitoring Productivity of Your Remote Staff</h6>
                    <p>One of the biggest challenges of working with staff who work from home is monitoring their
                        productivity and performance.
                    </p>
                    <div class="d-flex justify-content-between">
                        <a href="#">View Detail</a>
                        <span>December 4,2021</span>
                    </div>
                </div>
            </div>
            <div class="col-md-6 blog-card-wrapper">
                <div class="blog-card">
                    <h6>Monitoring Productivity of Your Remote Staff</h6>
                    <p>One of the biggest challenges of working with staff who work from home is monitoring their
                        productivity and performance.
                    </p>
                    <div class="d-flex justify-content-between">
                        <a href="#">View Detail</a>
                        <span>December 4,2021</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include("partials/footer.php")?>