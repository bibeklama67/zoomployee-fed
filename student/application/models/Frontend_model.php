<?php 
 if(!defined('BASEPATH')) exit('direct access invalid');

class Frontend_model extends CI_Model
{
		
	 function __construct()
    {
        parent::__construct();
        $db = $this->load->database('default', TRUE);
        // $this->otherdb = $this->load->database('otherdb',TRUE);
        // $this->smsdb =  $this->load->database('smsdb',TRUE);
        
    }

	public function getclasswisesubject()
    {
    	          $this->db->SELECT('c.classid,c.classname,s.subjectname,s.subjectid,s.slug');
                  $this->db->from('el_class c');
                  $this->db->join('el_subject s','c.classid=s.classid');
                  $this->db->order_by('c.priority', 'ASC');
                  //       $this->db->order_by('midas_products.subject', 'desc');
                  $query= $this->db->get();
            
        		  return $query->result('array');
                
    	 
    }
}