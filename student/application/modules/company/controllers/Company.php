<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Company extends MY_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index()
	{
		
	}
	
	
	function privacy(){
		
		$id=$_GET['id'];

		//id = 0 'GENERAL'
		if(!isset($id))
		{
		   $this->load->view('privacy_midas');
		}
		else if($id == '0')
		{
				$this->load->view('privacy');
		}
		else if($id == '146')   // FOR MCVTC
		{
				$this->load->view('privacymcvtc');
		}
			else if($id == '106')   // FOR B & B
		{
				$this->load->view('privacybnb');
		}


	
	}

	function terms(){
			

			$id=$_GET['id'];

		//id = 0 'GENERAL'

		if(!isset($id))
		{
		   $this->load->view('terms_new');
		}else if($id == '0')
		{
				$this->load->view('terms');	
		}
		else if($id == '146')   // FOR MCVTC
		{
				$this->load->view('termsmcvtc');	
		}
			else if($id == '106')   // FOR B & B
		{
				$this->load->view('termsbnb');	
		}
	}



}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
