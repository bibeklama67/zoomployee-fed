<html>
<head>
    <title>B&B</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/css/default/bootstrap.min.css">


    <script type="text/javascript" src="<?php echo base_url();?>/assets/js/default/bootstrap.min.js"></script>



    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/custom.css">
    <style>
        @media (max-width: 768px) {
          .navbar-collapse.navbar-absolute {
            position: absolute;
            margin: 0;
            background: none repeat scroll 0 0 #343e48;
            position: absolute;
            width: 100%;
            border-radius:4px;
            border: 1px solid #343e48;
        }
        .navbar-header{
            height:40px;
            overflow:hidden;

        }
        .navbar-toggle{
            margin-top:3px;
        }
        .navbar-brand{
            padding:10px;   
        }
    }

    .navbar-absolute + div {
        margin-top: 68px;
    }

    #main-nav{
        margin-bottom:5px;
    }
    .header-bg{
        background-image: none !important;
    }

    .header-bg > #logo {
        border-radius: 20px;
        padding: 10px;
    }

    #logo img {
        padding-top: 5px;
        padding-bottom: 5px;
        margin-right: auto;
        text-align: center;
        width: 100px;
        margin: 0 auto;
    }

    p, li {
        margin: 0 0 10px;
        font-family: sans-serif;
        font-size: 14px;
        text-align: justify;
    }
    body{
        border: none;
    }
</style>
</head>
<body>
    <header id="header" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="container header-bg">
            <!--logo start-->
            <div id="logo" class="col-sm-4 col-xs-12 col-lg-4 col-md-4"></div>
            <div id="logo" class="col-sm-4 col-xs-12 col-lg-4 col-md-4">
                <img class="img-responsive"  src="https://www.hamrodoctor.com/uploads/hospitals/5ae6f5ce55378.png" />
            </div>
            <div id="logo" class="col-sm-4 col-xs-12 col-lg-4 col-md-4"></div>
            <!--logo end-->

            <!-- navigation block end-->


            <div class="clearfix visible-xs-block"></div>
        </div>
    </header>

    <h1 style="text-align: center;margin-top: 0px;background: #42609B;color: #fff;padding: 25px;display: inline-block;width: 100%;">Privacy Policy</h1>
    <div class="container" style="margin-top: 25px;">

        <p>B&B Hospital (&ldquo;us&rdquo;, &ldquo;we&rdquo;, or &ldquo;B&B&rdquo;, which also includes its affiliates) is the author and publisher of the internet resource https://www.bbhospital.com.np/ (&ldquo;Website&rdquo;) on the world wide web as well as the software and applications provided by B&B, including but not limited to the mobile application &lsquo;B&B&rsquo;, and the software and applications of the brand names &lsquo;B&B Health App&rsquo;, &lsquo;B&B Patient App&rsquo;, &lsquo;B&B&rsquo; (together with the Website, referred to as the &ldquo;Services&rdquo;).</p>
        <p>This privacy policy ("<strong>Privacy Policy</strong>") explains how we collect, use, share, disclose and protect Personal information about the Users of the Services, including the Practitioners (as defined in the Terms of Use, which may be accessed via the following web link https://www.midas.com.np/terms?id=106 (the &ldquo;<strong>Terms of Use</strong>&rdquo;)), the End-Users (as defined in the Terms of Use), and the visitors of Website (jointly and severally referred to as &ldquo;you&rdquo; or &ldquo;Users&rdquo; in this Privacy Policy). We created this Privacy Policy to demonstrate our commitment to the protection of your privacy and your personal information. Your use of and access to the Services is subject to this Privacy Policy and our Terms of Use. Any capitalized term used but not defined in this Privacy Policy shall have the meaning attributed to it in our Terms of Use.</p>
        <p>BY USING THE SERVICES OR BY OTHERWISE GIVING US YOUR INFORMATION, YOU WILL BE DEEMED TO HAVE READ, UNDERSTOOD AND AGREED TO THE PRACTICES AND POLICIES OUTLINED IN THIS PRIVACY POLICY AND AGREE TO BE BOUND BY THE PRIVACY POLICY. YOU HEREBY CONSENT TO OUR COLLECTION, USE AND SHARING, DISCLOSURE OF YOUR INFORMATION AS DESCRIBED IN THIS PRIVACY POLICY. WE RESERVE THE RIGHT TO CHANGE, MODIFY, ADD OR DELETE PORTIONS OF THE TERMS OF THIS PRIVACY POLICY, AT OUR SOLE DISCRETION, AT ANY TIME. IF YOU DO NOT AGREE WITH THIS PRIVACY POLICY AT ANY TIME, DO NOT USE ANY OF THE SERVICES OR GIVE US ANY OF YOUR INFORMATION. IF YOU USE THE SERVICES ON BEHALF OF SOMEONE ELSE (SUCH AS YOUR CHILD) OR AN ENTITY (SUCH AS YOUR EMPLOYER), YOU REPRESENT THAT YOU ARE AUTHORISED BY SUCH INDIVIDUAL OR ENTITY TO (I) ACCEPT THIS PRIVACY POLICY ON SUCH INDIVIDUAL&rsquo;S OR ENTITY&rsquo;S BEHALF, AND (II) CONSENT ON BEHALF OF SUCH INDIVIDUAL OR ENTITY TO OUR COLLECTION, USE AND DISCLOSURE OF SUCH INDIVIDUAL&rsquo;S OR ENTITY&rsquo;S INFORMATION AS DESCRIBED IN THIS PRIVACY POLICY.</p>
        <ol>
        <li><strong>WHY THIS PRIVACY POLICY?</strong></li>
        </ol>
        <p>This Privacy Policy is published in compliance with Government of Nepal.</p>
        <ol start="2">
        <li><strong>COLLECTION OF PERSONAL INFORMATION</strong></li>
        </ol>
        <p>Generally some of the Services require us to know who you are so that we can best meet your needs. When you access the Services, or through any interaction with us via emails, telephone calls or other correspondence, we may ask you to voluntarily provide us with certain information that personally identifies you or could be used to personally identify you. You hereby consent to the collection of such information by B&B. Without prejudice to the generality of the above, information collected by us from you may include (but is not limited to) the following:</p>
        <ol>
            <li>contact data (such as your email address and phone number);</li>
            <li>demographic data (such as your gender, your date of birth);</li>
        </ol>
        <ul>
            <li>data regarding your usage of the services and history of the appointments made by or with you through the use of Services;</li>
        </ul>
        <ol>
            <li>other information that you voluntarily choose to provide to us (such as information shared by you with us through emails or letters.</li>
        </ol>
        <p>The information collected from you by B&B may constitute &lsquo;personal information&rsquo; or &lsquo;sensitive personal data or information&rsquo; under the SPI Rules.</p>
        <p><strong>&ldquo;Personal Information&rdquo;</strong>&nbsp;is defined under the SPI Rules to mean any information that relates to a natural person, which, either directly or indirectly, in combination with other information available or likely to be available to a body corporate, is capable of identifying such person.</p>
        <p>The SPI Rules further define &ldquo;Sensitive Personal Data or Information&rdquo; of a person to mean personal information about that person relating to:</p>
        <ol>
            <li>passwords;</li>
            <li>financial information such as bank accounts, credit and debit card details or other payment instrument details;</li>
        </ol>
        <ul>
            <li>physical, physiological and mental health condition;</li>
            <li>sexual orientation;</li>
        </ul>
        <ol>
            <li>medical records and history;</li>
            <li>biometric information;</li>
            <li>information received by body corporate under lawful contract or otherwise;</li>
        </ol>
        <ul>
            <li>visitor details as provided at the time of registration or thereafter; and</li>
            <li>call data records.</li>
        </ul>
        <p>B&B will be free to use, collect and disclose information that is freely available in the public domain without your consent.</p>
        <p>&nbsp;</p>
        <ol start="3">
        <li><strong>PRIVACY STATEMENTS</strong></li>
        </ol>
        <p><strong>3.1 </strong>ALL USERS NOTE:</p>
        <p>This section applies to all users.</p>
        <p><strong>3.1.1 </strong>Accordingly, a condition of each User&rsquo;s use of and access to the Services is their acceptance of the Terms of Use, which also involves acceptance of the terms of this Privacy Policy. Any User that does not agree with any provisions of the same has the option to discontinue the Services provided by B&B immediately.</p>
        <p><strong>3.1.2 </strong>An indicative list of information that B&B may require you to provide to enable your use of the Services is provided in the Schedule annexed to this Privacy Policy.</p>
        <p><strong>3.1.3 </strong>All the information provided to B&B by a User, including Personal Information or any Sensitive Personal Data or Information, is voluntary. You understand that B&B may use certain information of yours, which has been designated as Personal Information or &lsquo;Sensitive Personal Data or Information&rsquo; under the SPI Rules, (a) for the purpose of providing you the Services, (b) for commercial purposes and in an aggregated or non-personally identifiable form for research, statistical analysis and business intelligence purposes, (c) for sale or transfer of such research, statistical or intelligence data in an aggregated or non-personally identifiable form to third parties and affiliates (d) for communication purpose so as to provide You a better way of booking appointments and for obtaining feedback in relation to the Practitioners and their practice, (e) debugging customer support related issues.. (f) for the purpose of contacting you to complete any transaction if you do not complete a transaction after having provided us with your contact information in the course of completing such steps that are designed for completion of the transaction. B&B also reserves the right to use information provided by or about the End-User for the following purposes:</p>
        <ol>
            <li>Publishing such information on the Website.</li>
            <li>Contacting End-Users for offering new products or services.</li>
        </ol>
        <ul>
            <li>Contacting End-Users for taking product and Service feedback.</li>
        </ul>
        <ol>
            <li>Analyzing software usage patterns for improving product design and utility.</li>
            <li>Analyzing anonymized practice information for commercial use.</li>
            <li>Providing Lab Reports of users who has visited multiple health sectors.</li>
        </ol>
        <p>If you have voluntarily provided your Personal Information to B&B for any of the purposes stated above, you hereby consent to such collection and use of such information by B&B. However, B&B shall not contact you on your telephone number(s) for any purpose including those mentioned in this sub-section 4.1(iii), if such telephone number is registered with the Do Not Call registry (&ldquo;DNC Registry&rdquo;) under the PDPA without your express, clear and un-ambiguous written consent.</p>
        <p><strong>3.1.4 </strong>Collection, use and disclosure of information which has been designated as Personal Information or Sensitive Personal Data or Information&rsquo; under the SPI Rules requires your express consent. By affirming your assent to this Privacy Policy, you provide your consent to such use, collection and disclosure as required under applicable law.</p>
        <p><strong>3.1.5 </strong>B&B does not control or endorse the content, messages or information found in any Services and, therefore, B&B specifically disclaims any liability with regard to the Services and any actions resulting from your participation in any Services, and you agree that you waive any claims against B&B relating to same, and to the extent such waiver may be ineffective, you agree to release any claims against B&B relating to the same.</p>
        <p><strong>3.1.6 </strong>You are responsible for maintaining the accuracy of the information you submit to us, such as your contact information provided as part of account registration. If your personal information changes, you may correct, delete inaccuracies, or amend information by making the change on our member information page or by contacting us through&nbsp;admin@bbhospital.com.np. We will make good faith efforts to make requested changes in our then active databases as soon as reasonably practicable. If you provide any information that is untrue, inaccurate, out of date or incomplete (or becomes untrue, inaccurate, out of date or incomplete), or B&B has reasonable grounds to suspect that the information provided by you is untrue, inaccurate, out of date or incomplete, B&B may, at its sole discretion, discontinue the provision of the Services to you. There may be circumstances where B&B will not correct, delete or update your Personal Data, including (a) where the Personal Data is opinion data that is kept solely for evaluative purpose; and (b) the Personal Data is in documents related to a prosecution if all proceedings relating to the prosecution have not been completed.</p>
        <p><strong>3.1.7 </strong>If you wish to cancel your account or request that we no longer use your information to provide you Services, contact us through admin@bbhospital.com.np. We will retain your information for as long as your account with the Services is active and as needed to provide you the Services. We shall not retain such information for longer than is required for the purposes for which the information may lawfully be used or is otherwise required under any other law for the time being in force. After a period of time, your data may be anonymized and aggregated, and then may be held by us as long as necessary for us to provide our Services effectively, but our use of the anonymized data will be solely for analytic purposes. Please note that your withdrawal of consent, or cancellation of account may result in B&B being unable to provide you with its Services or to terminate any existing relationship B&B may have with you.</p>
        <p><strong>3.1.8 </strong>If you wish to opt-out of receiving non-essential communications such as promotional and marketing-related information regarding the Services, please send us an email at admin@bbhospital.com.np</p>
        <p><strong>3.1.9 </strong>B&B may require the User to pay with a Mobile Wallet, Credit Card, Debit Card, for Services for which subscription amount(s) is/are payable. B&B will collect such User&rsquo;s credit card number and/or other financial institution information such as bank account numbers and will use that information for the billing and payment processes, including but not limited to the use and disclosure of such credit card number and information to third parties as necessary to complete such billing operation. Verification of credit information, however, is accomplished solely by the user through the authentication process. User&rsquo;s credit-card/debit card details are transacted upon secure sites of approved payment gateways which are digitally under encryption, thereby providing the highest possible degree of care as per current technology. However, B&B provides you an option not to save your payment details. User is advised, however, that internet technology is not full proof safe and User should exercise discretion on using the same.</p>
        <p><strong>3.1.10 </strong>Due to the communications standards on the Internet, when a User or the End-User or anyone who visits the Website, B&B automatically receives the URL of the site from which anyone visits. B&B also receives the Internet Protocol (IP) address of each User&rsquo;s computer (or the proxy server a User used to access the World Wide Web), User&rsquo;s computer operating system and type of web browser the User is using, email patterns, as well as the name of User&rsquo;s ISP. This information is used to analyze overall trends to help B&B improve its Service. The linkage between User&rsquo;s IP address and User&rsquo;s personally identifiable information is not shared with or disclosed to third parties. Notwithstanding the above, B&B may share and/or disclose some of the aggregate findings (not the specific data) in anonymized form (i.e., non-personally identifiable) with advertisers, sponsors, investors, strategic partners, and others in order to help grow its business.</p>
        <p><strong>3.1.11 </strong>The Website uses temporary cookies to store certain (that is not sensitive personal data or information) that is used by B&B and its service providers for the technical administration of the Website, research and development, and for User administration. In the course of serving advertisements or optimizing services to its Users, B&B may allow authorized third parties to place or recognize a unique cookie on the User&rsquo;s browser. The cookies however, do not store any Personal Information of the User. You may adjust your internet browser to disable cookies. If cookies are disabled you may still use the Website, but the Website may be limited in the use of some of the features.</p>
        <p><strong>3.1.12 </strong>A User may have limited access to the Website without creating an account on the Website. Unregistered Users can make appointments with the doctors by providing their name and phone number. In order to have access to all the features and benefits on our Website, a User must first create an account on our Website. To create an account, a User is required to provide the following information, which such User recognizes and expressly acknowledges is Personal Information allowing others, including B&B, to identify the User: name, User ID, email address, country, ZIP/postal code, age, phone number, password chosen by the User and valid financial account information. Other information requested on the registration page, including the ability to receive promotional offers from B&B, is optional. B&B may, in future, include other optional requests for information from the User to help B&B to customize the Website to deliver personalized information to the User.</p>
        <p><strong>3.1.13 </strong>This Privacy Policy applies to Services that are owned and operated by B&B. B&B does not exercise control over the sites displayed as search results or links from within its Services. These other sites may place their own cookies or other files on the Users&rsquo; computer, collect data or solicit personal information from the Users, for which B&B is not responsible or liable. Accordingly, B&B does not make any representations concerning the privacy practices or policies of such third parties or terms of use of such websites, nor does B&B guarantee the accuracy, integrity, or quality of the information, data, text, software, sound, photographs, graphics, videos, messages or other materials available on such websites. The inclusion or exclusion does not imply any endorsement by B&B of the website, the website's provider, or the information on the website. If you decide to visit a third party website linked to the Website, you do this entirely at your own risk. B&B encourages the User to read the privacy policies of that website.</p>
        <p><strong>3.1.14 </strong>The Website may enable User to communicate with other Users or to post information to be accessed by others, whereupon other Users may collect such data. Such Users, including any moderators or administrators, are not authorized B&B representatives or agents, and their opinions or statements do not necessarily reflect those of B&B, and they are not authorized to bind B&B to any contract. B&B hereby expressly disclaims any liability for any reliance or misuse of such information that is made available by Users or visitors in such a manner.</p>
        <p><strong>3.1.15 </strong>B&B does not collect information about the visitors of the Website from other sources, such as public records or bodies, or private organizations, save and except for the purposes of registration of the Users (the collection, use, storage and disclosure of which each End User must agree to under the Terms of Use in order for B&B to effectively render the Services).</p>
        <p><strong>3.1.16 </strong>B&B maintains a strict "No-Spam" policy, which means that B&B does not intend to sell, rent or otherwise give your e-mail address to a third party without your consent.</p>
        <p><strong>3.1.17 </strong>B&B has implemented best international market practices and security policies, rules and technical measures to protect the personal data that it has under its control from unauthorized access, improper use or disclosure, unauthorized modification and unlawful destruction or accidental loss. However, for any data loss or theft due to unauthorized access to the User&rsquo;s electronic devices through which the user avails the Services, B&B shall not be held liable for any loss whatsoever incurred by the User.</p>
        <p><strong>3.1.18 </strong>B&B implements reasonable security practices and procedures and has a comprehensive documented information security program and information security policies that contain managerial, technical, operational and physical security control measures that are commensurate with respect to the information being collected and the nature of B&B&rsquo;s business.</p>
        <p><strong>3.1.19 </strong>B&B takes your right to privacy very seriously and other than as specifically stated in this Privacy Policy, will only disclose your Personal Information in the event it is required to do so by law, rule, regulation, law enforcement agency, governmental official, legal authority or similar requirements or when B&B, in its sole discretion, deems it necessary in order to protect its rights or the rights of others, to prevent harm to persons or property, to fight fraud and credit risk, or to enforce or apply the Terms of Use.</p>
        <p>&nbsp;</p>
        <p><strong>3.2 </strong>PRACTITIONERS NOTE:</p>
        <p>This section applies to all Practitioners.</p>
        <p><strong>3.2.1 </strong>As part of the registration as well as the application creation and submission process that is available to Practitioners on B&B, certain information, including Personal Information or Sensitive Personal Data or Information is collected from the Practitioners.</p>
        <p><strong>3.2.2 </strong>All the statements in this Privacy Policy apply to all Practitioners, and all Practitioners are therefore required to read and understand the privacy statements set out herein prior to submitting any Personal Information or Sensitive Personal Data or Information to B&B, failing which they are required to leave the Services, including the Website immediately.</p>
        <p><strong>3.2.3 </strong>Practitioners&rsquo; personally identifiable information, which they choose to provide to B&B, is used to help the Practitioners describe and identify themselves. This information is exclusively owned by B&B You will be the owner of your information and you consent to B&B collecting, using, processing and/or disclosing this information for the purposes hereinafter stated. B&B may use such information for commercial purposes and in an aggregated or non-personally identifiable form for research, statistical analysis and business intelligence purposes, and may sell or otherwise transfer such research, statistical or intelligence data in an aggregated or non-personally identifiable form to third parties and affiliates. B&B also reserves the right to use information provided by or about the Practitioner for the following purposes:</p>
        <ol>
            <li>Publishing such information on the Website.</li>
            <li>Contacting Practitioners for offering new products or services subject to the telephone number registered with the DNC Registry.</li>
        </ol>
        <ul>
            <li>Contacting Practitioners for taking product feedback.</li>
        </ul>
        <ol>
            <li>Analyzing software usage patterns for improving product design and utility.</li>
            <li>Analyzing anonymized practice information including financial, and inventory information for commercial use.</li>
        </ol>
        <p><strong>3.2.4 </strong>B&B automatically enables the listing of Practitioners&rsquo; information on its Website for every &lsquo;Doctor&rsquo; or &lsquo;Clinic&rsquo; added to a Practice using its software. The Practitioner information listed on Website is displayed when End-Users search for doctors on Website, and the Practitioner information listed on Website is used by End-Users to request for doctor appointments. Any personally identifiable information of the Practitioners listed on the Website is not generated by B&B and is provided to B&B by Practitioners who wish to enlist themselves on the Website, or is collected by B&B from the public domain. B&B displays such information on its Website on an as-is basis making no representation or warranty on the accuracy or completeness of the information. As such, we strongly encourage Practitioners to check the accuracy and completeness of their information from time to time, and inform us immediately of any discrepancies, changes or updates to such information. B&B will, however, take reasonable steps to ensure the accuracy and completeness of this information.</p>
        <p><strong>3.2.5 </strong>B&B may also display information for Practitioners who have not signed up or registered for the Services, provided that the Practitioners have consented to B&B collecting, processing and/or disclosing their information on the Website. Such Practitioners are verified by B&B or its associates, and B&B makes every effort to capture accurate information for such Practitioners. However, B&B does not undertake any liability for any incorrect or incomplete information appearing on the Website for such Practitioners.</p>
        <p>&nbsp;</p>
        <p><strong>3.3 </strong>END-USERS NOTE:</p>
        <p>This section applies to all End-Users.</p>
        <p><strong>3.3.1 </strong>As part of the registration/application creation and submission process that is available to End-Users on this Website, certain information, including Personal Information or Sensitive Personal Data or Information is collected from the End-Users.</p>
        <p><strong>3.3.2 </strong>All the statements in this Privacy Policy apply to all End-Users, and all End-Users are therefore required to read and understand the privacy statements set out herein prior to submitting any Personal Information or Sensitive Personal Data or Information to B&B, failing which they are required to leave the B&B immediately.</p>
        <p><strong>3.3.3 </strong>If you have inadvertently submitted any such information to B&B prior to reading the privacy statements set out herein, and you do not agree with the manner in which such information is collected, processed, stored, used or disclosed, then you may access, modify and delete such information by using options provided on the Website. In addition, you can, by sending an email to privacy@B&B.com, inquire whether B&B is in possession of your personal data, and you may also require B&B to delete and destroy all such information.</p>
        <p><strong>3.3.4 </strong>End-Users&rsquo; personally identifiable information, which they choose to provide on the Website is used to help the End-Users describe/identify themselves. Other information that does not personally identify the End-Users as an individual, is collected by B&B from End-Users (such as, patterns of utilization described above) and is exclusively owned by B&B. B&B may also use such information in an aggregated or non-personally identifiable form for research, statistical analysis and business intelligence purposes, and may sell or otherwise transfer such research, statistical or intelligence data in an aggregated or non-personally identifiable form to third parties and affiliates. In particular, B&B reserves with it the right to use anonymized End-User demographics information and anonymized End-User health information for the following purposes:</p>
        <ol>
            <li>Analyzing software usage patterns for improving product design and utility.</li>
            <li>Analyzing such information for research and development of new technologies.</li>
            <li>Using analysis of such information in other commercial product offerings of B&B.</li>
            <li>Sharing analysis of such information with third parties for commercial use.</li>
        </ol>
        <p><strong>3.3.5 </strong>B&B will communicate with the End-Users through email, phone and notices posted on the Website or through other means available through the service, including text and other forms of messaging. The End-Users can change their e-mail and contact preferences at any time by logging into their "Account" at http://myhealth.midas.com.np and changing the account settings.</p>
        <p><strong>3.3.6 </strong>At times, B&B conducts a User survey to collect information about End-Users' preferences. These surveys are optional and if End-Users choose to respond, their responses will be kept anonymous. Similarly, B&B may offer contests to qualifying End-Users in which we ask for contact and demographic information such as name, email address and mailing address. The demographic information that B&B collects in the registration process and through surveys is used to help B&B improve its Services to meet the needs and preferences of End-Users.</p>
        <p><strong>3.3.7 </strong>B&B may keep records of electronic communications and telephone calls received and made for making appointments or other purposes for the purpose of administration of Services, customer support, research and development and for better listing of Practitioners.</p>
        <p><strong>3.3.8 </strong>All B&B employees and data processors, who have access to, and are associated with the processing of sensitive personal data or information, are obliged to respect the confidentiality of every End-Users&rsquo; Personal Information or Sensitive Personal Data and Information. B&B has put in place procedures and technologies as per good industry practices and in accordance with the applicable laws, to maintain security of all personal data from the point of collection to the point of destruction. Any third-party data processor to which B&B transfers Personal Data shall have to agree to comply with those procedures and policies, or put in place adequate measures on their own.</p>
        <p><strong>3.3.9 </strong>B&B may also disclose or transfer End-Users&rsquo; personal and other information provided by a User, to a third party as part of reorganization or a sale of the assets of a B&B corporation division or company. Any third party to which B&B transfers or sells its assets to will have the right to continue to use the personal and other information that End-Users provide to us, in accordance with the Terms of Use</p>
        <p><strong>3.3.10 </strong>To the extent necessary to provide End-Users with the Services, B&B may provide their Personal Information to third party contractors who work on behalf of or with B&B to provide End-Users with such Services, to help B&B communicate with End-Users or to maintain the Website. Generally these contractors do not have any independent right to share this information, however certain contractors who provide services on the Website, including the providers of online communications services, may use and disclose the personal information collected in connection with the provision of these Services in accordance with their own privacy policies. In such circumstances, you consent to us disclosing your Personal Information to contractors, solely for the intended purposes only.</p>
        <p>&nbsp;</p>
        <p><strong>3.4 </strong>CASUAL VISITORS NOTE:</p>
        <p><strong>3.4.1 </strong>No sensitive personal data or information is automatically collected by B&B from any casual visitors of this website, who are merely perusing the Website.</p>
        <p><strong>3.4.2 </strong>Nevertheless, certain provisions of this Privacy Policy are applicable to even such casual visitors, and such casual visitors are also required to read and understand the privacy statements set out herein, failing which they are required to leave this Website immediately.</p>
        <p><strong>3.4.3 </strong>If you, as a casual visitor, have inadvertently browsed any other page of this Website prior to reading the privacy statements set out herein, and you do not agree with the manner in which such information is obtained, collected, processed, stored, used, disclosed or retained, merely quitting this browser application should ordinarily clear all temporary cookies installed by B&B. All visitors, however, are encouraged to use the &ldquo;clear cookies&rdquo; functionality of their browsers to ensure such clearing / deletion, as B&B cannot guarantee, predict or provide for the behaviour of the equipment of all the visitors of the Website.</p>
        <p><strong>3.4.4 </strong>You are not a casual visitor if you have willingly submitted any personal data or information to B&B through any means, including email, post or through the registration process on the Website. All such visitors will be deemed to be, and will be treated as, Users for the purposes of this Privacy Policy, and in which case, all the statements in this Privacy Policy apply to such persons.</p>
        <p><strong>&nbsp;</strong></p>
        <ol start="4">
            <li><strong>CONFIDENTIALITY AND SECURITY</strong></li>
        </ol>
        <p><strong>4.1 </strong>Your Personal Information is maintained by B&B in electronic form on its equipment, and on the equipment of its employees. Such information may also be converted to physical form from time to time. B&B takes all necessary precautions to protect your personal information both online and off-line, and implements reasonable security practices and measures including certain managerial, technical, operational and physical security control measures that are commensurate with respect to the information being collected and the nature of B&B&rsquo;s business.</p>
        <p><strong>4.2 </strong>No administrator at B&B will have knowledge of your password. It is important for you to protect against unauthorized access to your password, your computer and your mobile phone. Be sure to log off from the Website when finished. B&B does not undertake any liability for any unauthorized use of your account and password. If you suspect any unauthorized use of your account, you must immediately notify B&B by sending an email to <a href="mailto:admin@bbhospital.com.np">admin@bbhospital.com.np</a>. You shall be liable to indemnify B&B due to any loss suffered by it due to such unauthorized use of your account and password.</p>
        <p><strong>4.3 </strong>B&B makes all User information accessible to its employees, agents or partners and third parties only on a need-to-know basis, and binds only its employees to strict confidentiality obligations.</p>
        <p><strong>4.4 </strong>Part of the functionality of B&B is assisting the doctors to maintain and organize such information. B&B may, therefore, retain and submit all such records to the appropriate authorities, or to doctors who request access to such information.</p>
        <p><strong>4.5 </strong>Part of the functionality of the B&B is assisting the patients to access information relating to them. B&B may, therefore, retain and submit all such records to the relevant patients, or to their doctors.</p>
        <p><strong>4.6 </strong>Notwithstanding the above, B&B is not responsible for the confidentiality, security or distribution of your Personal Information by our partners and third parties outside the scope of our agreement with such partners and third parties. Further, B&B shall not be responsible for any breach of security or for any actions of any third parties or events that are beyond the reasonable control of B&B including but not limited to, acts of government, computer hacking, unauthorized access to computer data and storage device, computer crashes, breach of security and encryption, poor quality of Internet service or telephone service of the User etc.</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <ol start="5">
            <li><strong>CHANGE TO PRIVACY POLICY</strong></li>
        </ol>
        <p>B&B may update this Privacy Policy at any time, with or without advance notice. In the event there are significant changes in the way B&B treats User&rsquo;s personally identifiable information, or in the Privacy Policy itself, B&B will display a notice on the Website or send Users an email, as provided for above, so that you may review the changed terms prior to continuing to use the Services. As always, if you object to any of the changes to our terms, and you no longer wish to use the Services, you may contact admin@bbhospital.com.np to deactivate your account. Unless stated otherwise, B&B&rsquo;s current Privacy Policy applies to all information that B&B has about you and your account.</p>
        <p>If a User uses the Services or accesses the Website after a notice of changes has been sent to such User or published on the Website, such User hereby provides his/her/its consent to the changed terms.</p>
        <ol start="6">
            <li><strong>CHILDREN'S AND MINOR'S PRIVACY</strong></li>
        </ol>
        <p>B&B strongly encourages parents and guardians to supervise the online activities of their minor children and consider using parental control tools available from online services and software manufacturers to help provide a child-friendly online environment. These tools also can prevent minors from disclosing their name, address, and other personally identifiable information online without parental permission. Although the B&B Website and Services are not intended for use by minors, B&B respects the privacy of minors who may inadvertently use the internet or the mobile application.</p>
        <ol start="7">
            <li><strong>CONSENT TO THIS POLICY</strong></li>
        </ol>
        <p>You acknowledge that this Privacy Policy is a part of the Terms of Use of the Website and the other Services, and you unconditionally agree that becoming a User of the Website and its Services signifies your (i) assent to this Privacy Policy, and (ii) consent to B&B using, collecting, processing and/or disclosing your Personal Information in the manner and for the purposes set out in this Privacy Policy. Your visit to the Website and use of the Services is subject to this Privacy Policy and the Terms of Use.</p>
        <ol start="8">
            <li><strong>ADDRESS FOR PRIVACY QUESTIONS</strong></li>
        </ol>
        <p>Should you have questions about this Privacy Policy or B&B's information collection, use and disclosure practices, you may contact at <a href="mailto:admin@bbhospital.com.np">admin@bbhospital.com.np</a></p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p><strong><u>SCHEDULE</u></strong></p>
        <p><strong><u>Indicative List of Information by Nature of Service</u></strong></p>
        <ol>
            <li><strong>End-Users using the Website by registering for an account on the Website or &lsquo;B&B&rsquo; mobile application:</strong></li>
        </ol>
        <p>You can create an account by giving us information regarding your [name, mobile number, email address], and such other information as requested on the&nbsp;End-User registration page. This is to enable us to provide you with the facility to use the account to book your appointments and store other health related information.</p>
        <p><strong>&nbsp;</strong></p>
        <ol start="2">
            <li><strong>End-Users using the Website without registering for an account on the Website or &lsquo;B&B&rsquo; mobile application (i.e., &lsquo;Guest&rsquo; End-User):</strong></li>
        </ol>
        <p>You can use the Website without registering for an account, but to book an appointment, you may be asked certain information (including your [mobile number], and such other information as requested when you choose to use the Services without registration) to confirm the appointment.</p>
        <p>&nbsp;</p>
    </div>
</body>
</html>