<html>
<head>
    <title>MiDas Eclass</title>
	<?php $url="http://myhealth.midas.com.np/"; ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="<?php echo $url; ?>bootstrap/css/bootstrap.css">
    <script type="text/javascript" src="<?php echo $url; ?>bootstrap/js/jquery-1.11.1.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo $url; ?>bootstrap/css/main-style.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $url; ?>bootstrap/css/tinycarousel.css"/>
    <script type="text/javascript" src="<?php echo $url; ?>bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo $url; ?>bootstrap/js/bootstrap.js"></script>


    <link rel="stylesheet" type="text/css" href="<?php echo $url; ?>css/custom.css">
    <style>
        @media (max-width: 768px) {
          .navbar-collapse.navbar-absolute {
            position: absolute;
            margin: 0;
            background: none repeat scroll 0 0 #343e48;
            position: absolute;
            width: 100%;
            border-radius:4px;
            border: 1px solid #343e48;
        }
        .navbar-header{
            height:40px;
            overflow:hidden;

        }
        .navbar-toggle{
            margin-top:3px;
        }
        .navbar-brand{
            padding:10px;   
        }
    }

    .navbar-absolute + div {
        margin-top: 68px;
    }

    #main-nav{
        margin-bottom:5px;
    }
    .header-bg{
        background-image: none !important;
    }

    .header-bg > #logo {
        border-radius: 20px;
        padding: 10px;
    }

    #logo img {
        padding-top: 5px;
        padding-bottom: 5px;
        margin-right: auto;
        text-align: center;
        width: 100px;
        margin: 0 auto;
    }

    p, li {
        margin: 0 0 10px;
        font-family: sans-serif;
        font-size: 14px;
        text-align: justify;
    }
    body{
        border: none;
    }
    .text-justify{
            font-family: "Helvetica Neue", Arial, sans-serif;
    }
</style>
</head>
<body>
    <header id="header" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="container header-bg">
            <!--logo start-->
            <div id="logo" class="col-sm-4 col-xs-12 col-lg-4 col-md-4"></div>
            <div id="logo" class="col-sm-4 col-xs-12 col-lg-4 col-md-4">
                <img class="img-responsive"  src="https://www.midas.com.np/assets/images/midaseclass-xxs.png" />
            </div>
            <div id="logo" class="col-sm-4 col-xs-12 col-lg-4 col-md-4"></div>
            <!--logo end-->

            <!-- navigation block end-->


            <div class="clearfix visible-xs-block"></div>
        </div>
    </header>

    <h1 style="text-align: center;margin-top: 0px;background: #42609B;color: #fff;padding: 25px;display: inline-block;width: 100%;">Privacy Policy</h1>
    <div class="container" style="margin-top: 25px;">
  
<p class="text-justify">At MiDas Education Pvt. Ltd., we know you care about how your personal information is used and shared, and we take your privacy seriously. 
MiDas eCLASS is a communicating platform that helps parents users to send quick, simple messages to any device. MiDas eCLASS respects your information and treats it carefully.</p>

<p class="text-justify">You are responsible for any Content you provide in connection with the Services. We cannot control the actions of anyone with whom you or any other MiDas eCLASS users may choose to share information. Please be aware that no security measures are perfect or impenetrable and that we are not responsible for circumvention of any security measures contained on the Services. MiDas eCLASS does not encourage you to make any personally identifiable information (Personal Information) public other than what is necessary for you to use our Services. You understand and acknowledge that, even after removal, copies of Content may remain viewable in cached pages, archives and storage backups or if other users have copied or stored your Content.</p>

<p class="text-justify">MiDas eCLASS is a tool to help you communicate with teachers, school admins which helps support their children's education, and we take your privacy seriously. Maintaining the privacy of your Personal Information is a shared responsibility, and while we will limit what we ask for and what we do with your information, we encourage you not to share it unless you need to. Please keep in mind that you are responsible for the content of your account and all your messages.</p>

<h4 style="color: #697b8f !important;
    font-weight: bold;">What does this privacy policy cover?</h4>
<p class="text-justify">This Privacy Policy explains how MiDas eCLASS collects and uses information from you and other users who access or use the Services, including our treatment of Personal Information.</p>
<p class="text-justify">MiDas eCLASS collects limited Personal Information of children and we rely on a Teacher,School to obtain the consent of each child’s parent when collecting this information.</p>

<h4 style="color: #697b8f !important;
    font-weight: bold;">What information does MiDas eCLASS display or collect?</h4>
<p class="text-justify">When you use the Services, you may set up your personal profile, send messages, and transmit information as permitted by the functionality of the Services. MiDas eCLASS will not display your personal contact information to other users without your permission. The information we gather from users enables us to verify user identity, allows our users to set up a user account and profile through the Services, and helps us personalize and improve our Services. We retain this information to provide the Services to you and our other users and to provide a useful user experience.</p>
<h4 style="color: #697b8f !important;
    font-weight: bold;">Information you provide to us</h4>
<p class="text-justify">We receive and store any information you knowingly enter on the Services, whether via mobile phone, other wireless device, or that you provide to us in any other way. This information may include Personal Information such as your name, phone numbers, email addresses, photographs, and, in certain circumstances, your school, School affiliation. We use the Personal Information we receive about you to provide you with the Services and also for purposes such as:</p>
<ul class="dashed">
<li>- authenticating your identity,</li> 
<li>- responding to your requests for certain information,</li> 
<li>- customizing the features that we make available to you,</li> 
<li>- suggesting relevant services or products for you to use,</li> 
<li>- improving the Services and internal operations (including troubleshooting, testing, and analyzing usage),</li> 
<li>- communicating with you about new features,</li> 
<li>- sending messages from MiDas Education and its sister concerns,</li>
<li>- and, most importantly, protecting our users and working towards making sure our Services are safer and more secure.</li>
</ul>
<h4 style="color: #697b8f !important;
    font-weight: bold;">Information collected automatically</h4>
<p class="text-justify">We receive and store certain types of information whenever you use the Services. MiDas eCLASS automatically receives and records information on our server logs from MiDas App – for Parents.  We also may provide to our sister concerns aggregate information derived from automatically collected information about how our users, collectively, use our app. We may share this type of identifiable, aggregated statistical data so that MiDas Education and its sister concerns understand how often people use their services as well as MiDas eCLASS.</p>

<p class="text-justify">Will MiDas eCLASS share any of the personal information it receives? 
MiDas eCLASS relies on its users to provide accurate Personal Information in order to provide our Services. We try our best to protect that information and make sure that we are responsible in handling, disclosing, and retaining your data. We neither rent nor sell your Personal Information to anyone. However, we may share you information of products and service provided by MiDas Education and its sister concerns.</p>


<h4 style="color: #697b8f !important;
    font-weight: bold;">Is information about me secure?</h4>
<p class="text-justify">The security of personal information of the childrens, teachers and school is a top priority for MiDas eCLASS.
    MiDas eCLASS has protections in place to enhance our user's security, and routinely update these protections. We have administrative, technical, and physical safeguards designed to protect against unauthorized use, disclosure of or access to personal information. In particular:</p>
    <ul class="dashed">
        <li>- Our engineering team is dedicated to keeping your personal information secure.</li>
        <li>- MiDas eCLASS stores its data within servers.</li>
        <li>- MiDas eCLASS’ maintain its database and all backups.</li>
    </ul>
    <br>

    <p class="text-justify"> MiDas eCLASS endeavors to protect user information and to ensure that user account information is kept private.</p>

    <p class="text-jusitfy">The security of your personal information is extremely important to us and we take measures internally to make sure your information is safe with us. We routinely update our security features and implement new protections for your data.</p>


                                                     
    </div>
</body>
</html>
