<html>
<head>
    <title>B&B</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/css/default/bootstrap.min.css">


    <script type="text/javascript" src="<?php echo base_url();?>/assets/js/default/bootstrap.min.js"></script>    <style>
        @media (max-width: 768px) {
          .navbar-collapse.navbar-absolute {
            position: absolute;
            margin: 0;
            background: none repeat scroll 0 0 #343e48;
            position: absolute;
            width: 100%;
            border-radius:4px;
            border: 1px solid #343e48;
        }
        .navbar-header{
            height:40px;
            overflow:hidden;

        }
        .navbar-toggle{
            margin-top:3px;
        }
        .navbar-brand{
            padding:10px;   
        }
    }

    .navbar-absolute + div {
        margin-top: 68px;
    }

    #main-nav{
        margin-bottom:5px;
    }
    .header-bg{
        background-image: none !important;
    }

    .header-bg > #logo {
        border-radius: 20px;
        padding: 10px;
    }

    #logo img {
        padding-top: 5px;
        padding-bottom: 5px;
        margin-right: auto;
        text-align: center;
        width: 100px;
        margin: 0 auto;
    }

    p, li {
        margin: 0 0 10px;
        font-family: sans-serif;
        font-size: 14px;
        text-align: justify;
    }
    body{
        border: none;
    }
</style>
</head>
<body>
    <header id="header" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="container header-bg">
            <!--logo start-->
            <div id="logo" class="col-sm-4 col-xs-12 col-lg-4 col-md-4"></div>
            <div id="logo" class="col-sm-4 col-xs-12 col-lg-4 col-md-4">
                <img class="img-responsive" src="https://www.hamrodoctor.com/uploads/hospitals/5ae6f5ce55378.png" />
            </div>
            <div id="logo" class="col-sm-4 col-xs-12 col-lg-4 col-md-4"></div>
            <!--logo end-->

            <!-- navigation block end-->


            <div class="clearfix visible-xs-block"></div>
        </div>
    </header>

    <h1 style="text-align: center;margin-top: 0px;background: #42609B;color: #fff;padding: 25px;display: inline-block;width: 100%;">Terms and Conditions</h1>
    <div class="container" style="margin-top: 25px;">
<p>B&B Hospital (<strong>&ldquo;B&B&rdquo;</strong>) is the author and publisher of the internet resource https://www.bbhospital.com.np/ and the mobile application &lsquo;B&B&rsquo; (together, &ldquo;website&rdquo;). B&B owns and operates the services provided through the Website and App.</p>
<ol>
<li><strong> NATURE AND APPLICABILITY OF TERMS</strong></li>
</ol>
<p>Please carefully go through these terms and conditions&nbsp;<strong>(&ldquo;Terms&rdquo;)</strong>&nbsp;and the privacy policy available at https://www.midas.com.np/privacy/?id=106&nbsp;<strong>(&ldquo;Privacy Policy&rdquo;) </strong>before you decide to access the Website or avail the services made available on the Website by B&B. These Terms and the Privacy Policy together constitute a legal agreement&nbsp;<strong>(&ldquo;Agreement&rdquo;)</strong>&nbsp;between you and B&B in connection with your visit to the Website and your use of the Services (as defined below).</p>
<p>The Agreement applies to you whether you are -</p>
<ol>
<li>A medical practitioner or health care provider (whether an individual professional or an organization) or similar institution wishing to be listed, or already listed, on the Website, including designated, authorized associates of such practitioners or institutions&nbsp;<strong>(&ldquo;Practitioner(s)&rdquo;, &ldquo;you&rdquo; or &ldquo;User&rdquo;);</strong>or</li>
<li>A patient, his/her representatives or affiliates, searching for Practitioners through the Website&nbsp;<strong>(&ldquo;End-User&rdquo;, &ldquo;you&rdquo; or &ldquo;User&rdquo;);</strong>or</li>
</ol>
<ul>
<li>Otherwise a user of the Website&nbsp;<strong>(&ldquo;you&rdquo; or &ldquo;User&rdquo;)</strong>.</li>
</ul>
<p>&nbsp;</p>
<p>This Agreement applies to those services made available by B&B on the Website, which are offered free of charge to the Users&nbsp;<strong>(&ldquo;Services&rdquo;)</strong>, including the following:</p>
<ol>
<li>For Practitioners: Listing of Practitioners and their profiles and contact details, to be made available to the other Users and visitors to the Website;</li>
<li>For other Users: Facility to (i) create and maintain &lsquo;Health Accounts&rsquo;, (ii) search for Practitioners by name, specialty, and geographical area, or any other criteria that may be developed and made available by B&B,(iii) to make appointments with Practitioners and (iv) Providing Lab Reports of users who has visited multiple health sectors.</li>
</ol>
<p>The Services may change from time to time, at the sole discretion of B&B, and the Agreement will apply to your visit to and your use of the Website to avail the Service, as well as to all information provided by you on the Website at any given point in time.</p>
<p>This Agreement defines the terms and conditions under which you are allowed to use the Website and describes the manner in which we shall treat your account while you are registered as a member with us. If you have any questions about any part of the Agreement, feel free to contact us at https://www.bbhospital.com.np/.</p>
<p>By downloading or accessing the Website to use the Services, you irrevocably accept all the conditions stipulated in this Agreement, the&nbsp;Terms of Service&nbsp;and&nbsp;Privacy Policy, as available on the Website, and agree to abide by them. This Agreement supersedes all previous oral and written terms and conditions (if any) communicated to you relating to your use of the Website to avail the Services. By availing any Service, you signify your acceptance of the terms of this Agreement.</p>
<p>We reserve the right to modify or terminate any portion of the Agreement for any reason and at any time, and such modifications shall be informed to you in writing. You should read the Agreement at regular intervals. Your use of the Website following any such modification constitutes your agreement to follow and be bound by the Agreement so modified.</p>
<p>You acknowledge that you will be bound by this Agreement for availing any of the Services offered by us. If you do not agree with any part of the Agreement, please do not use the Website or avail any Services.</p>
<p>Your access to use of the Website and the Services will be solely at the discretion of B&B.</p>
<p>The Agreement is published in compliance of, and is governed by the provisions of Nepal law.</p>
<p>&nbsp;</p>
<ol start="2">
<li><strong>CONDITIONS OF USE</strong></li>
</ol>
<p>You must be 18 years of age or older to register, use the Services, or visit or use the Website in any manner. By registering, visiting and using the Website or accepting this Agreement, you represent and warrant to B&B that you are 18 years of age or older, and that you have the right, authority and capacity to use the Website and the Services available through the Website, and agree to and abide by this Agreement.</p>
<ol start="3">
<li><strong>TERMS OF USE APPLICABLE TO ALL USERS OTHER THAN PRACTITIONERS</strong></li>
</ol>
<p>The terms in this Clause 3 are applicable only to Users other than Practitioners.</p>
<p><strong>3.1 </strong>END-USER ACCOUNT AND DATA PRIVACY</p>
<p><strong>3.1.1</strong>&nbsp;The terms &ldquo;personal information&rdquo; and &ldquo;sensitive personal data or information&rdquo; are defined under the SPI Rules, and are reproduced in the Privacy Policy.</p>
<p><strong>3.1.2</strong>&nbsp;B&B may by its Services, collect information relating to the devices through which you access the Website, and anonymous data of your usage. The collected information will be used only for improving the quality of B&B&rsquo;s services and to build new services.</p>
<p><strong>3.1.3</strong>&nbsp;The Website allows B&B to have access to registered Users&rsquo; personal email or phone number, for communication purpose so as to provide you a better way of booking appointments and for obtaining feedback in relation to the Practitioners and their practice.</p>
<p><strong>3.1.4</strong>&nbsp;The Privacy Policy sets out,&nbsp;<em>inter-alia</em>:</p>
<ol>
<li>The type of information collected from Users, including sensitive personal data or information;</li>
<li>The purpose, means and modes of usage of such information;</li>
</ol>
<ul>
<li>How and to whom B&B will disclose such information; and,</li>
</ul>
<ol>
<li>Other information mandated by the SPI Rules.</li>
</ol>
<p><strong>&nbsp;3.1.5</strong>&nbsp;The User is expected to read and understand the Privacy Policy, so as to ensure &nbsp;&nbsp;&nbsp;&nbsp;that he or she has the knowledge of,&nbsp;<em>inter-alia</em>:</p>
<ol>
<li>the fact that certain information is being collected;</li>
<li>the purpose for which the information is being collected;</li>
</ol>
<ul>
<li>the intended recipients of the information;</li>
</ul>
<ol>
<li>the nature of collection and retention of the information;</li>
<li>and the name and address of the agency that is collecting the information and the agency that will retain the information; and</li>
<li>The various rights available to such Users in respect of such information.</li>
</ol>
<p><strong>3.1.6</strong>&nbsp;B&B shall not be responsible in any manner for the authenticity of the personal information or sensitive personal data or information supplied by the User to B&B or to any other person acting on behalf of B&B.</p>
<p><strong>3.1.7</strong>&nbsp;The User is responsible for maintaining the confidentiality of the User&rsquo;s account access information and password, if the User is registered on the Website. The User shall be responsible for all usage of the User&rsquo;s account and password, whether or not authorized by the User. The User shall immediately notify B&B of any actual or suspected unauthorized use of the User&rsquo;s account or password. Although B&B will not be liable for your losses caused by any unauthorized use of your account, you may be liable for the losses of B&B or such other parties as the case may be, due to any unauthorized use of your account.</p>
<p><strong>3.1.8 </strong>If a User provides any information that is untrue, inaccurate, not current or incomplete (or becomes untrue, inaccurate, not current or incomplete), or B&B has reasonable grounds to suspect that such information is untrue, inaccurate, not current or incomplete, B&B has the right to discontinue the Services to the User at its sole discretion.</p>
<p><strong>3.1.9 </strong>B&B may use such information collected from the Users from time to time for the purposes of debugging customer support related issues.</p>
<p><strong>3.1.10 </strong>Against every hospitals listed in http://myhealth.midas.com.np, you may see a 'call' option. When you choose this option, you choose to call the number through a telephony service provided by your telecom.</p>
<p>&nbsp;</p>
<p><strong>3.2 </strong>RELEVANCE ALGORITHM</p>
<p>B&B&rsquo;s relevance algorithm for the Practitioners is a fully automated system that lists the Practitioners, their profile and information regarding their Practice on its Website. These listings of Practitioners do not represent any fixed objective ranking or endorsement by B&B. B&B will not be liable for any change in the relevance of the Practitioners on search results, which may take place from time to time. The listing of Practitioners will be based on automated computation of the various factors including inputs made by the Users including their comments and feedback. Such factors may change from time to time, in order to improve the listing algorithm. B&B in no event will be held responsible for the accuracy and the relevancy of the listing order of the Practitioners on the Website.</p>
<p><strong>3.3 </strong>LISTING CONTENT AND DISSEMINATING INFORMATION</p>
<p><strong>3.3.1 </strong>B&B collects, directly or indirectly, and displays on the Website, relevant information regarding the profile and practice of the Practitioners listed on the Website, such as their specialization, qualification, fees, location, visiting hours, and similar details. B&B takes reasonable efforts to ensure that such information is updated at frequent intervals. Although B&B screens and vets the information and photos submitted by the Practitioners, it cannot be held liable for any inaccuracies or incompleteness represented from it, despite such reasonable efforts.</p>
<p><strong>3.3.2 </strong>The Services provided by B&B or any of its licensors or service providers are provided on an "as is" and &ldquo;as available&rsquo; basis, and without any warranties or conditions (express or implied, including the implied warranties of merchantability, accuracy, fitness for a particular purpose, title and non-infringement, arising by statute or otherwise in law or from a course of dealing or usage or trade). B&B does not provide or make any representation, warranty or guarantee, express or implied about the Website or the Services. B&B does not guarantee the accuracy or completeness of any content or information provided by Users on the Website. To the fullest extent permitted by law, B&B disclaims all liability arising out of the User&rsquo;s use or reliance upon the Website, the Services, representations and warranties made by other Users, the content or information provided by the Users on the Website, or any opinion or suggestion given or expressed by B&B or any User in relation to any User or services provided by such User.</p>
<p><strong>3.3.3 </strong>The Website may be linked to the website of third parties, affiliates and business partners. B&B has no control over, and not liable or responsible for content, accuracy, validity, reliability, quality of such websites or made available by/through our Website. Inclusion of any link on the Website does not imply that B&B endorses the linked site. User may use the links and these services at User&rsquo;s own risk.</p>
<p><strong>3.3.4 </strong>B&B assumes no responsibility, and shall not be liable for, any damages to, or viruses that may infect User&rsquo;s equipment on account of User&rsquo;s access to, use of, or browsing the Website or the downloading of any material, data, text, images, video content, or audio content from the Website. If a User is dissatisfied with the Website, User&rsquo;s sole remedy is to discontinue using the Website.</p>
<p><strong>3.3.5 </strong>If B&B determines that you have provided fraudulent, inaccurate, or incomplete information, including through feedback, B&B reserves the right to immediately suspend your access to the Website or any of your accounts with B&B and makes such declaration on the website alongside your name/your clinic&rsquo;s name as determined by B&B for the protection of its business and in the interests of Users. You shall be liable to indemnify B&B for any losses incurred as a result of your misrepresentations or fraudulent feedback that has adversely affected B&B or its Users.</p>
<p>&nbsp;</p>
<p><strong>3.4 </strong>BOOK APPOINTMENT</p>
<p>B&B enables Users to connect with Practitioners with Book facility that allows Users book an appointment through the Website and app.</p>
<p><strong>3.4.1 </strong>B&B will ensure Users are provided confirmed appointment on the Book facility. However, B&B has no liability if such an appointment is later cancelled by the Practitioner, or the same Practitioner is not available for appointment..</p>
<p><strong>3.4.2 </strong>The results of any search Users perform on the Website for Practitioners should not be construed as an endorsement by B&B of any such particular Practitioner. If the User decides to engage with a Practitioner to seek medical services, the User shall be doing so at his/her own risk.</p>
<p><strong>3.4.3</strong>&nbsp;Without prejudice to the generality of the above, B&B is not involved in providing any healthcare or medical advice or diagnosis and hence is not responsible for any interactions between User and the Practitioner. User understands and agrees that B&B will not be liable for:</p>
<ol>
<li>User interactions and associated issues User has with the Practitioner;</li>
<li>the ability or intent of the Practitioner(s) or the lack of it, in fulfilling their obligations towards Users;
<ul>
<li>any wrong medication or quality of treatment being given by the Practitioner(s), or any medical negligence on part of the Practitioner(s);</li>
</ul>
</li>
</ol>
<ol>
<li>inappropriate treatment, or similar difficulties or any type of inconvenience suffered by the User due to a failure on the part of the Practitioner to provide agreed Services;</li>
<li>any misconduct or inappropriate behaviour by the Practitioner or the Practitioner&rsquo;s staff;</li>
<li>cancellation or no show by the Practitioner or rescheduling of booked appointment or any variation in the fees charged</li>
</ol>
<p>&nbsp;</p>
<p><strong>3.4.5 </strong>Users are allowed to provide feedback about their experiences with the Practitioner, however, the User shall ensure that, the same is provided in accordance with applicable law. User however understands that, B&B shall not be obliged to act in such manner as may be required to give effect to the content of Users feedback, such as suggestions for delisting of a particular Practitioner from the Website.</p>
<p>&nbsp;</p>
<ul>
<li>Cancellation and Refund Policy
<ol start="3">
<li>In the event that, the Practitioner with whom User has booked a paid appointment via the Website, has not been able to meet the User, User will need to write to us at support@info.B&B.com.np within five (5) days from the occurrence of such event; in which case, the entire consultation amount as mentioned on the Website will be refunded to the User within the next five (5) to six (6) business days in the original mode of payment done by the User while booking. In case where the User, does not show up for the appointment booked with a Practitioner, without cancelling the appointment beforehand, the amount will not be refunded, and treated as under Clause 3.4.6. However, where cancellation charges have been levied (as charged by the Practitioner/Practice), you would not be entitled to complete refund even if you have cancelled beforehand.</li>
<li>Users will not be entitled for any refunds in cases where, the Practitioner is unable to meet the User at the exact time of the scheduled appointment time and the User is required to wait, irrespective of the fact whether the User is required to wait or choose to not obtain the medical services from the said Practitioner.</li>
</ol>
</li>
<li>NO DOCTOR-PATIENT RELATIONSHIP; NOT FOR EMERGENCY USE
<ul>
<li>Please note that some of the content, text, data, graphics, images, information, suggestions, guidance, and other material (collectively, &ldquo;Information&rdquo;) that may be available on the Website (including information provided in direct response to your questions or postings) may be provided by individuals in the medical profession. The provision of such Information does not create a licensed medical professional/patient relationship, between B&B and you and does not constitute an opinion, medical advice, or diagnosis or treatment of any particular condition, but is only provided to assist you with locating appropriate medical care from a qualified practitioner.</li>
</ul>
</li>
</ul>
<p><strong>3.5.2 </strong>It is hereby expressly clarified that, the Information that you obtain or receive from B&B, and its employees, contractors, partners, sponsors, advertisers, licensors or otherwise on the Website is for informational purposes only. We make no guarantees, representations or warranties, whether expressed or implied, with respect to professional qualifications, quality of work, expertise or other information provided on the Website. In no event shall we be liable to you or anyone else for any decision made or action taken by you in reliance on such information</p>
<p><strong>3.5.3 </strong>The Services are not intended to be a substitute for getting in touch with emergency healthcare. If you are an End-User facing a medical emergency (either on your or a another person&rsquo;s behalf), please contact an ambulance service or hospital directly.</p>
<ol start="4">
<li><strong>TERMS OF USE PRACTITIONERS</strong></li>
</ol>
<p>The terms in this Clause 4 are applicable only to Practitioners.</p>
<p><strong>4.1</strong>&nbsp;LISTING POLICY</p>
<p><strong>4.1.1 </strong>B&B, directly and indirectly, collects information regarding the Practitioners&rsquo; profiles, contact details, and practice. B&B reserves the right to take down any Practitioner&rsquo;s profile as well as the right to display the profile of the Practitioners, with or without notice to the concerned Practitioner. This information is collected for the purpose of facilitating interaction with the End-Users and other Users. If any information displayed on the Website in connection with you and your profile is found to be incorrect, you are required to inform B&B immediately to enable B&B to make the necessary amendments.</p>
<p><strong>4.1.2 </strong>B&B shall not be liable and responsible for the ranking of the Practitioners on external websites and search engines</p>
<p><strong>4.1.3 </strong>B&B shall not be responsible or liable in any manner to the Users for any losses, damage, injuries or expenses incurred by the Users as a result of any disclosures or publications made by B&B, where the User has expressly or implicitly consented to the making of disclosures or publications by B&B. If the User had revoked such consent under the terms of the Privacy Policy, then B&B shall not be responsible or liable in any manner to the User for any losses, damage, injuries or expenses incurred by the User as a result of any disclosures made by B&B prior to its actual receipt of such revocation.</p>
<p><strong>4.1.4 </strong>B&B reserves the right to moderate the suggestions made by the Practitioners through feedback and the right to remove any abusive or inappropriate or promotional content added on the Website. However, B&B shall not be liable if any inactive, inaccurate, fraudulent, or non- existent profiles of Practitioners are added to the Website.</p>
<p><strong>4.1.5 </strong>Practitioners explicitly agree that B&B reserves the right to publish the Content provided by Practitioners to a third party including content platforms.<strong> 4.1.7 </strong>You as a Practitioner hereby represent and warrant that you will use the Services in accordance with applicable law. Any contravention of applicable law as a result of your use of these Services is your sole responsibility, and B&B accepts no liability for the same.</p>
<p>&nbsp;</p>
<p><strong>4.2 </strong>PROFILE OWNERSHIP AND EDITING RIGHTS</p>
<p>B&B ensures easy access to the Practitioners by providing a tool to update your profile information. B&B reserves the right of ownership of all the Practitioner&rsquo;s profile and photographs and to moderate the changes or updates requested by Practitioners. However, B&B takes the independent decision whether to publish or reject the requests submitted for the respective changes or updates. You hereby represent and warrant that you are fully entitled under law to upload all content uploaded by you as part of your profile or otherwise while using B&B&rsquo;s services, and that no such content breaches any third party rights, including intellectual property rights. Upon becoming aware of a breach of the foregoing representation, B&B may modify or delete parts of your profile information at its sole discretion with or without notice to you.</p>
<p><strong>4.3 </strong>REVIEWS AND FEEDBACK DISPLAY RIGHTS OF B&B</p>
<p><strong>4.3.1 </strong>All Critical Content is content created by the Users of http://myhealth.midas.com.np&nbsp;<strong>(&ldquo;Website&rdquo;)</strong>&nbsp;and the clients of B&B customers and Practitioners, including the End-Users. As a platform, B&B does not take responsibility for Critical Content and its role with respect to Critical Content is restricted to that of an &lsquo;intermediary&rsquo; under the Information Technology Act, 2000. The role of B&B and other legal rights and obligations relating to the Critical Content are further detailed in Clauses 3.9 and 5 of these Terms. B&B&rsquo;s Feedback Collection and Fraud Detection Policy, is annexed as the Schedule hereto, and remains subject always to these Terms.</p>
<p><strong>4.3.2 </strong>B&B reserves the right to collect feedback and Critical Content for all the Practitioners, Clinics and Healthcare Providers listed on the Website.</p>
<p><strong>4.3.3 </strong>B&B shall have no obligation to pre-screen, review, flag, filter, modify, refuse or remove any or all Critical Content from any Service, except as required by applicable law.</p>
<p><strong>4.3.4 </strong>You understand that by using the Services you may be exposed to Critical Content or other content that you may find offensive or objectionable. B&B shall not be liable for any effect on Practitioner&rsquo;s business due to Critical Content of a negative nature. In these respects, you may use the Service at your own risk. B&B however, as an &lsquo;intermediary, takes steps as required to comply with applicable law as regards the publication of Critical Content. The legal rights and obligations with respect to Critical Content and any other information sought to be published by Users are further detailed in Clauses 3.9 and 5 of these Terms.</p>
<p><strong>4.3.5 </strong>B&B will take down information under standards consistent with applicable law, and shall in no circumstances be liable or responsible for Critical Content, which has been created by the Users. The principles set out in relation to third party content in the terms of Service for the Website shall be applicable mutatis mutandis in relation to Critical Content posted on the Website.</p>
<p><strong>4.3.6 </strong>If B&B determines that you have provided inaccurate information or enabled fraudulent feedback, B&B reserves the right to immediately suspend any of your accounts with B&B and makes such declaration on the website alongside your name/your clinics name as determined by B&B for the protection of its business and in the interests of Users.</p>
<p>&nbsp;</p>
<p><strong>4.4</strong>&nbsp;BOOK APPOINTMENT</p>
<p><strong>4.4.1 </strong>As a valuable partner on our platform we want to ensure that the Practitioners experience on the B&B booking platform is beneficial to both, Practitioners and their Users.</p>
<p><strong>4.4.2 </strong>Practitioner understands that, B&B shall not be liable, under any event, for any comments or feedback given by any of the Users in relation to the Services provided by Practitioner. The option of publishing or modifying or moderating or masking (where required by law or norm etc.) the feedback provided by Users shall be solely at the discretion of B&B.</p>
<p>&nbsp;</p>
<ol start="5">
<li><strong>RIGHTS AND OBLIGATIONS RELATING TO CONTENT</strong></li>
</ol>
<p><strong>&nbsp;</strong></p>
<p><strong>5.1 </strong>As mandated by Law of Nepal Government, B&B hereby informs Users that they are not permitted to host, display, upload, modify, publish, transmit, update or share any information that:</p>
<ol>
<li>belongs to another person and to which the User does not have any right to;</li>
<li>is grossly harmful, harassing, blasphemous, defamatory, obscene, pornographic, pedophilic, libelous, invasive of another's privacy, hateful, or racially, ethnically objectionable, disparaging, relating or encouraging money laundering or gambling, or otherwise unlawful in any manner whatever;</li>
</ol>
<ul>
<li>harm minors in any way;</li>
</ul>
<ol>
<li>infringes any patent, trademark, copyright or other proprietary rights;</li>
<li>violates any law for the time being in force;</li>
<li>deceives or misleads the addressee about the origin of such messages or communicates any information which is grossly offensive or menacing in nature;</li>
</ol>
<ul>
<li>impersonate another person;</li>
<li>contains software viruses or any other computer code, files or programs designed to interrupt, destroy or limit the functionality of any computer resource;</li>
</ul>
<ol>
<li>threatens the unity, integrity, defense, security or sovereignty of Nepal, friendly relations with foreign states, or public order or causes incitement to the commission of any cognizable offence or prevents investigation of any offence or is insulting any other nation.</li>
</ol>
<ul>
<li>Users are also prohibited from:</li>
</ul>
<ol>
<li>violating or attempting to violate the integrity or security of the Website or any B&B Content;</li>
<li>transmitting any information (including job posts, messages and hyperlinks) on or through the Website that is disruptive or competitive to the provision of Services by B&B;</li>
</ol>
<ul>
<li>intentionally submitting on the Website any incomplete, false or inaccurate information;</li>
</ul>
<ol>
<li>making any unsolicited communications to other Users;</li>
<li>using any engine, software, tool, agent or other device or mechanism (such as spiders, robots, avatars or intelligent agents) to navigate or search the Website;</li>
<li>attempting to decipher, decompile, disassemble or reverse engineer any part of the Website;</li>
</ol>
<ul>
<li>copying or duplicating in any manner any of the B&B Content or other information available from the Website;</li>
<li>framing or hot linking or deep linking any B&B Content.</li>
</ul>
<ol>
<li>circumventing or disabling any digital rights management, usage rules, or other security features of the Software.
<ul>
<li>B&B, upon obtaining knowledge by itself or been brought to actual knowledge by an affected person in writing or through email signed with electronic signature about any such information as mentioned above, shall be entitled to disable such information that is in contravention of Clauses 5.1 and 5.2. B&B shall also be entitled to preserve such information and associated records for at least 90 (ninety) days for production to governmental authorities for investigation purposes.</li>
</ul>
</li>
</ol>
<p>&nbsp;</p>
<ul>
<li>In case of non-compliance with any applicable laws, rules or regulations, or the Agreement (including the Privacy Policy) by a User, B&B has the right to immediately terminate the access or usage rights of the User to the Website and Services and to remove non-compliant information from the Website.</li>
</ul>
<p><strong>5.5&nbsp;&nbsp; </strong>B&B may disclose or transfer User-generated information to its affiliates or &nbsp;governmental authorities in such manner as permitted or required by applicable law, and you hereby consent to such transfer. The SPI Rules only permit B&B to transfer sensitive personal data or information including any information, to any other body corporate or a person in Nepal, or located in any other country, that ensures the same level of data protection that is adhered to by B&B as provided for under the SPI Rules, only if such transfer is necessary for the performance of the lawful contract between B&B or any person on its behalf and the User or where the User has consented to data transfer.</p>
<p>B&B respects the intellectual property rights of others and we do not hold any responsibility for any violations of any intellectual property rights</p>
<ol start="6">
<li><strong>TERMINATION</strong></li>
</ol>
<p><strong>6.1 </strong>B&B reserves the right to suspend or terminate a User&rsquo;s access to the Website and the Services with or without notice and to exercise any other remedy available under law, in cases where,</p>
<ol>
<li>Such User breaches any terms and conditions of the Agreement;</li>
<li>A third party reports violation of any of its right as a result of your use of the Services;</li>
</ol>
<ul>
<li>B&B is unable to verify or authenticate any information provide to B&B by a User;</li>
</ul>
<ol>
<li>B&B has reasonable grounds for suspecting any illegal, fraudulent or abusive activity on part of such User; or</li>
<li>B&B believes in its sole discretion that User&rsquo;s actions may cause legal liability for such User, other Users or for B&B or are contrary to the interests of the Website.</li>
</ol>
<p><strong>6.2 </strong>Once temporarily suspended, indefinitely suspended or terminated, the User may not continue to use the Website under the same account, a different account or re-register under a new account. On termination of an account due to the reasons mentioned herein, such User shall no longer have access to data, messages, files and other material kept on the Website by such User. The User shall ensure that he/she/it has continuous backup of any medical services the User has rendered in order to comply with the User&rsquo;s record keeping process and practices.</p>
<p>&nbsp;</p>
<ol start="7">
<li><strong>LIMITATION OF LIABILITY</strong></li>
</ol>
<p>In no event, including but not limited to negligence, shall B&B, or any of its directors, officers, employees, agents or content or service providers (collectively, the &ldquo;Protected Entities&rdquo;) be liable for any direct, indirect, special, incidental, consequential, exemplary or punitive damages arising from, or directly or indirectly related to, the use of, or the inability to use, the Website or the content, materials and functions related thereto, the Services, User&rsquo;s provision of information via the Website, lost business or lost End-Users, even if such Protected Entity has been advised of the possibility of such damages. In no event shall the Protected Entities be liable for:</p>
<ol>
<li>provision of or failure to provide all or any service by Practitioners to End- Users contacted or managed through the Website;</li>
<li>any content posted, transmitted, exchanged or received by or on behalf of any User or other person on or through the Website;</li>
</ol>
<p>iii. any unauthorized access to or alteration of your transmissions or data; or</p>
<ol>
<li>any other matter relating to the Website or the Service.</li>
</ol>
<p>In no event shall the total aggregate liability of the Protected Entities to a User for all damages, losses, and causes of action (whether in contract or tort, including, but not limited to, negligence or otherwise) arising from this Agreement or a User&rsquo;s use of the Website or the Services exceed, in the aggregate Rs. 1000/- (Rupees One Thousand Only).</p>
<ol start="8">
<li><strong>RETENTION AND REMOVAL</strong></li>
</ol>
<p>B&B may retain such information collected from Users from its Website or Services for as long as necessary, depending on the type of information; purpose, means and modes of usage of such information; and according to the SPI Rules. Computer web server logs may be preserved as long as administratively necessary.</p>
<ol start="9">
<li><strong>APPLICABLE LAW AND DISPUTE SETTLEMENT</strong></li>
</ol>
<p><strong>9.1 </strong>You agree that this Agreement and any contractual obligation between B&B and User will be governed by the laws of Nepal.</p>
<p><strong>9.2 </strong>Any dispute, claim or controversy arising out of or relating to this Agreement, including the determination of the scope or applicability of this Agreement to arbitrate, or your use of the Website or the Services or information to which it gives access, shall be determined by arbitration in Nepal, before a sole arbitrator appointed by B&B. Arbitration shall be conducted in accordance with the Arbitration and Conciliation Act, 1996. The seat of such arbitration shall be Bangalore. All proceedings of such arbitration, including, without limitation, any awards, shall be in the English language. The award shall be final and binding on the parties to the dispute.</p>
<p><strong>9.3 </strong>Subject to the above Clause 9.2, the courts at Bengaluru shall have exclusive jurisdiction over any disputes arising out of or in relation to this Agreement, your use of the Website or the Services or the information to which it gives access.</p>
<ol start="10">
<li><strong>CONTACT INFORMATION GRIEVANCE OFFICER</strong></li>
</ol>
<p><strong>10.1 </strong>If a User has any questions concerning B&B, the Website, this Agreement, the Services, or anything related to any of the foregoing, B&B customer support can be reached at the following email address: <a href="mailto:https://www.bbhospital.com.np/">https://www.bbhospital.com.np/</a></p>
<p>&nbsp;</p>
<p>YOU HAVE READ THESE TERMS OF USE AND AGREE TO ALL OF THE PROVISIONS CONTAINED ABOVE</p>
<p>&nbsp;</p>
</div>
</body>
</html>