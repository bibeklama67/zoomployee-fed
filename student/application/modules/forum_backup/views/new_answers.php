<?php $row = $response; 
if($row['answerbytype']=='STUDENT'){?>
<div class="col-md-12" style="margin-top: 10px;">
  <div class="col-md-1 pull-left">
    <img src="<?= $row['userimage']?>" width="40px" height="40px" class="img-responsive"/>    
  </div>
  <div class="col-md-11 pull-left">
    <label for="username" style="margin-bottom: 0px;"><a><span style="color:#717B9B; font-size:12px;"><?= $row['username']?></span></a></label>
    <a><small style="color: #7D7D7D;">Replied on&nbsp;</small></a>
    <small style="color:8f949a"><span><?= $row['dataposteddatetime']?></span> <!-- at <span id="time">11:15pm</span> --></small><br>
    <p><?= $row['answertext']?></p>
    <?php 
    if($row['mediatype']=='image'){
      ?>
      <img class="img-responsive" src="<?php echo $row['answerimage']; ?>" style="width:20%">
      <?php
    }
    ?>
    <a href="javascript:;" class="answer-actions <?= ($row['postliked']=='Y'?'liked':'')?> like-answer" data-refid="<?= $row['answerstoquestionsid']?>"><i class="fa fa-thumbs-up"></i> <span class="likescount"><?= ($row['likes']>0?$row['likes']>0:'Like')?></span></a>
  </div>
</div>
<?php }
else{
  ?>
  <!-- Add Teacher's Answer -->
  <?php
}
?>