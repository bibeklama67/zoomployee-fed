<?php $data = $data['response']; ?>

<?php foreach($data as $row){
  if($row['answerbytype']=='STUDENT'){?>
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;">
  <div class="col-md-1 col-xs-1 pull-left" style="padding-right: 0">
      <img src="<?= $row['userimage']?>" width="40px" height="40px" class="img-responsive"/>    
    </div>
    <div class="col-md-3 col-xs-3 pull-left">
      <label for="username" style="margin-bottom: 0px;"><a><span style="color:#717B9B; font-size:12px;"><?= $row['username']?></span></a></label><br>
     <!-- <a><small style="color: #7D7D7D;">Replied on&nbsp;</small></a>-->
      <small style="color:#8f949a"><span><?= $row['dataposteddatetime']?></span> <!-- at <span id="time">11:15pm</span> --></small><br>
     </div>
     <div class="col-sm-4 col-xs-4 "><p class="pull-right"><span class="font12">Average Rating</span><br>
     <span><big class="mi44 font18">4.429 <span class="glyphicon glyphicon-star myy"></span></big></span>
     </p></div>
     <div class="col-sm-4 col-xs-4 "><p class="pull-right"><span class="font12">Total Ratings</span><br>
     <span class="font18"><big>14</big></span></p></div>
      
    <div class=" col-sm-12 col-xs-12 answerstext">
    	       <p><?= $row['answertext']?></p>
       
            <?php 
            if($row['mediatype']=='image'){
              ?>
              <img class="img-responsive" src="<?php echo $row['answerimage']; ?>" style="width:20%">
              <?php
            }
            ?>
      <a href="javascript:;" class="answer-actions <?= ($row['postliked']=='Y'?'liked':'')?> like-answer" data-refid="<?= $row['answerstoquestionsid']?>" data-likes="<?= $row['likes']?>"><i class="fa fa-thumbs-up"></i></a>
      <a href="javascript:;" class="answer-actions <?= ($row['postliked']=='Y'?'liked':'')?> view-likes" data-refid="<?= $row['answerstoquestionsid']?>" data-actiontype="likes">
        <?php 
        if($row['likes']>1)
          $text = $row['likes'].' Likes';
        else if($row['likes']==1)
          $text = $row['likes'].' Like';
        else
          $text = 'Like';

        ?>
        <span class="likescount"><?= $text?></span>
      </a>
    </div>
    <!-- <div class="col-sm-12 col-xs-12 dtahy">
    	<div class="pull-right">
    		<strong class="dtahytxt">Does this answer help you?</strong><br>
    		<div class="acidjs-rating-stars my" style="margin-left: 30px;">
          <form style="">
           <input type="radio" class="rating-radio" name="group-<?= $qid?>" data-refid="<?= $qid?>" id="group-<?= $qid?>-0" value="5"><label for="group-<?= $qid?>-0"></label>
           <input type="radio" class="rating-radio" name="group-<?= $qid?>" data-refid="<?= $qid?>" id="group-<?= $qid?>-1" value="4"><label for="group-<?= $qid?>-1"></label>
           <input type="radio" class="rating-radio" name="group-<?= $qid?>" data-refid="<?= $qid?>" id="group-<?= $qid?>-2" value="3"><label for="group-<?= $qid?>-2"></label>
           <input type="radio" class="rating-radio" name="group-<?= $qid?>" data-refid="<?= $qid?>" id="group-<?= $qid?>-3" value="2"><label for="group-<?= $qid?>-3"></label>
           <input type="radio" class="rating-radio" name="group-<?= $qid?>" data-refid="<?= $qid?>" id="group-<?= $qid?>-4" value="1"><label for="group-<?= $qid?>-4"></label>
         </form>
       </div>
    	</div>
    </div> -->
  </div>
  <?php }
  else{
    ?>
    <!-- Add Teacher's Answer -->
    <?php
  }
}?>
<div class="col-sm-12">
  <div class="col-sm-12 col-xs-12 dtahy">
    <div class="pull-right">
      <strong class="dtahytxt">Does this answer help you?</strong><br>
      <div class="acidjs-rating-stars my" style="margin-left: 30px;">
        <form style="">
          <input type="hidden" id="ratyQId" value="<?php echo $questionid;?>">
          <input type="radio" class="rating-radio" name="group-<?= $questionid?>" data-refid="<?= $questionid?>" id="group-<?= $questionid?>-0" value="5"><label for="group-<?= $questionid?>-0"></label>
          <input type="radio" class="rating-radio" name="group-<?= $questionid?>" data-refid="<?= $questionid?>" id="group-<?= $questionid?>-1" value="4"><label for="group-<?= $questionid?>-1"></label>
          <input type="radio" class="rating-radio" name="group-<?= $questionid?>" data-refid="<?= $questionid?>" id="group-<?= $questionid?>-2" value="3"><label for="group-<?= $questionid?>-2"></label>
          <input type="radio" class="rating-radio" name="group-<?= $questionid?>" data-refid="<?= $questionid?>" id="group-<?= $questionid?>-3" value="2"><label for="group-<?= $questionid?>-3"></label>
          <input type="radio" class="rating-radio" name="group-<?= $questionid?>" data-refid="<?= $questionid?>" id="group-<?= $questionid?>-4" value="1"><label for="group-<?= $questionid?>-4"></label>
       </form>
     </div>
   </div>
 </div>
</div>

