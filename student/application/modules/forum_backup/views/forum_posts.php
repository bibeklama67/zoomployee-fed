<style type="text/css">
	.forum-actions,.answer-actions{
		color: #aaa;
	}
	.forum-actions i, .answer-actions i{
		font-size: 18px;
	}
	.forum-actions.liked, .answer-actions.liked{
		color: #3f8654;
	}
	.forum-date{
		font-weight: 700;
		color: #0c50a3;
	}
	.askers-icon{
		padding:9px 9px 9px 15px
	}
	.get_answers, .post-answer{
		background-color: #0c50a3;
		border: #0c50a3;		
	}
	.get_answers:hover,.get_answers:focus,.get_answers:active,.post-answer:hover,.post-answer:focus,.post-answer:active{
		background-color: #0c50b3;
		border: #0c50a3;				
	}
	.answer-text{
		width: 90% !important;
		border: none !important;
		padding: 0 10px;
		outline: none;
	}
	.answers-list{
		margin-bottom: 10px;
	}
	a.forum-actions:hover, a.forum-actions:focus {
		color: #23527c;
		text-decoration: none;
	} 
	#home-workk .rate-txt p{
		font-size: inherit !important;
	}
</style>
<?php
foreach ($response as $key => $row) {
	$qid =  $row['questionsofstudentsid']; ?>

		<div class="panel-group" id="accordion<?= $qid?>" role="tablist" aria-multiselectable="true">
		<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="heading<?= $qid?>">
				<div class="row panel-heading-up">
					<div class="col-xs-11 no-padding">
						<div class="col-xs-12">
							<a href="javascript:;" class="forum-actions" data-actiontype="askedby" data-refid="<?= $qid?>"><i class="fa fa-user-circle"></i></a>&nbsp;<small style="color: #aaa;"><?= $row['dataposteddatetime']?></small>					
						</div>					
						<div class="col-xs-12">
							<p style="color:#0c50a3; font-size: 12px; font-weight:normal; padding-top:5px;"><strong><?= $row['questiontext']?></strong></p>
							<?php 
							if($row['mediatype']=='image'){
								?>
								<img class="img-responsive" src="<?php echo $row['questionimage']; ?>" style="width:20%">
								<?php
							}
							?>
						</div>
						<!--old rating design starts-->
						<!--<div class="rating-wrapper">
							<div class="col-md-6 no-padding" style="margin-top: 20px;">
								<div class="col-md-6 rate-txt">
									<p>Rate this Question</p>
								</div>
								<div class="col-md-6">
									<div class="acidjs-rating-stars">
										<form style="margin-top: 10px;">
											<input type="radio" class="rating-radio" name="group-<?= $qid?>" data-refid="<?= $qid?>" id="group-<?= $qid?>-0" value="5"><label for="group-<?= $qid?>-0"></label>
											<input type="radio" class="rating-radio" name="group-<?= $qid?>" data-refid="<?= $qid?>" id="group-<?= $qid?>-1" value="4"><label for="group-<?= $qid?>-1"></label>
											<input type="radio" class="rating-radio" name="group-<?= $qid?>" data-refid="<?= $qid?>" id="group-<?= $qid?>-2" value="3"><label for="group-<?= $qid?>-2"></label>
											<input type="radio" class="rating-radio" name="group-<?= $qid?>" data-refid="<?= $qid?>" id="group-<?= $qid?>-3" value="2"><label for="group-<?= $qid?>-3"></label>
											<input type="radio" class="rating-radio" name="group-<?= $qid?>" data-refid="<?= $qid?>" id="group-<?= $qid?>-4" value="1"><label for="group-<?= $qid?>-4"></label>
										</form>
									</div>
								</div>

							</div>
							<div class="col-md-6 rating-box no-padding">

								<div class="col-md-4 rating-box-l" style="padding-top: 10px;padding-right: 0px; padding-left: 0px;">

									<span style="color:#737373;font-size: 28px;font-weight:100;line-height: 25px;padding-left: 25px;" class="pull-right">4.0</span>
									<div class="acidjs-rating-stars pull-right">
										<form>
											<input type="radio" name="rating-<?= $qid?>-2" id="rating-<?= $qid?>-0" value="5" disabled><label for="rating-<?= $qid?>-0"></label>
											<input type="radio" name="rating-<?= $qid?>-2" id="rating-<?= $qid?>-1" value="4" disabled><label for="rating-<?= $qid?>-1"></label>
											<input type="radio" name="rating-<?= $qid?>-2" id="rating-<?= $qid?>-2" value="3" disabled><label for="rating-<?= $qid?>-2"></label>
											<input type="radio" name="rating-<?= $qid?>-2" id="rating-<?= $qid?>-3" value="2" disabled><label for="rating-<?= $qid?>-3"></label>
											<input type="radio" name="rating-<?= $qid?>-2" id="rating-<?= $qid?>-4" value="1" disabled><label for="rating-<?= $qid?>-4"></label>
										</form>
									</div>
									<p style="margin-left: -6px;font-size: 12px;" class="pull-right"><img src="http://midas.com.np/elearning/assets/images/f-onlyme.png" style="margin: 0px;border: 0px;transform: scale(0.7);margin-top: -4px;">&nbsp;63,535,32 total</p>
								</div>
								<div class="col-md-8 rating-box-r" style="padding-top: 5px;">
									<div class="col-md-2">
										<div><i class="fa fa-star" aria-hidden="true">5</i></div>
										<div><i class="fa fa-star" aria-hidden="true">4</i></div>
										<div><i class="fa fa-star" aria-hidden="true">3</i></div>
										<div><i class="fa fa-star" aria-hidden="true">2</i></div>
										<div><i class="fa fa-star" aria-hidden="true">1</i></div>
									</div>

									<div class="col-md-10" style="padding-left: 5px;background: transparent;">
										<div id="myBar1">1021</div>
										<div id="myBar2">490</div>
										<div id="myBar3">400</div>
										<div id="myBar4">350</div>
										<div id="myBar5">1100</div>
									</div>
								</div>
							</div>
						</div>-->
						<!--old rating design ends-->
						
						<!--likes views and comments starts-->
						<!--<div class="col-xs-12">
							<a href="javascript:;" class="forum-actions <?= ($row['postliked']=='Y'?'liked':'')?> like-question" data-refid="<?= $qid?>" data-likes="<?= $row['likescount']?>"><i class="fa fa-thumbs-up"></i></a>
							<a href="javascript:;" class="forum-actions <?= ($row['postliked']=='Y'?'liked':'')?> view-likes" data-refid="<?= $qid?>" data-actiontype="likes">
								<?php 
								if($row['likescount']>1)
									$text = $row['likescount'].' Likes';
								else if($row['likescount']==1)
									$text = $row['likescount'].' Like';
								else
									$text = 'Like';

								?>
								<span class="likescount"><?= $text?></span>
							</a>
							<a href="javascript:;"  class="forum-actions" style="margin-left:10px"><i class="fa fa-eye"></i> <?= ($row['views']>0?$row['views']>0:'Views')?></a>
							<a href="javascript:;"  class="forum-actions" style="margin-left:10px"><i class="fa fa-comments"></i> <?= ($row['comments']>0?$row['comments']>0:'Comments')?></a>
						</div>-->
						<!--likes views and comments ends-->
						
						
						</div><div class="col-xs-1">
						<a role="button" data-toggle="collapse" data-forumid="<?= $qid?>" data-parent="#accordion" href="#collapse<?= $qid?>" aria-expanded="false" aria-controls="collapse<?= $qid?>" class="get_answers"><i class="arrow-down"></i></a>
					</div></div>

				</div>


				<div id="collapse<?= $qid?>" class="panel-collapse collapse answer-body" role="tabpanel" aria-labelledby="heading<?= $qid?>">
					<div class="panel-body"style="padding: 0px;">
						<div class="panel-body-up answers-wrapper" style="border-bottom: 2px solid #cecfff;">
							<div class="answers-list row"></div>
						</div>

						<div class="panel-body-down">
							<div class="col-md-12 comment-reply-area">
								<div class="col-md-12"style="padding: 0px; margin: 10px 0px;">
									<div class="col-md-1">
										<img src="<?= $this->session->userdata('image');?>" width="40px" height="40px" class="img-responsive"/>    
									</div>
									<div class=" col-md-9 no-padding" style="border-bottom: 1px solid #ccc;">
										<textarea placeholder="Write a comment.." class="answer-text"></textarea>
										
									</div>

									<div class=" col-md-2 no-padding" style="">
										<!-- <a href="" class="btn btn-info post-answer" style="position: absolute;bottom: 0 !important;height: 32px;border-radius: 4;" data-questionid="<?= $qid?>">POST</a> -->
										<a href="" class="btn btn-info post-answer pull-right" style="bottom: 0 !important;height: 32px;border-radius: 4;" data-questionid="<?= $qid?>">POST</a>
										<span class="pull-right Wrapicon">
											<label for="file-upload-image<?= $qid?>" class="custom-file-upload btn lbl-image_comment" data-questionid="<?= $qid?>">

											</label>
											<input id="file-upload-image<?= $qid?>" name="file_upload[]" type="file" class="image_comment image_comment<?= $qid?>" data-questionid="<?= $qid?>" data-previewTarget="<?php echo '.image_preview_comment'.$qid ;?>">
										</span>
										<span class="pull-right" style="margin-top: 15px;">
											<img src="" id="chat_iamge_preview" class="image_preview_comment<?php echo $qid; ?>">
										</span> 
									</div>


								</div>
							</div>
						</div>

					</div>		
				</div>
			</div>

		</div>
		<?php }?>
		<!-- Modal -->
		<div class="modal fade" id="action-data" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title"></h4>
					</div>
					<div class="modal-body views-wrapper">

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			$(document).off('change','.rating-radio');
			$(document).on('change','.rating-radio',function(){
				var infoData = { Questionsofstudentsid: $(this).data('refid'), Rating:$(this).val()};
				var response = postQuestionRating(infoData);

			});
		</script>