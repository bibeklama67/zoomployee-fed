<!-- <style>
 	span.arrow-up {
 		background: url(http://midas.com.np/elearning/assets/images/close.jpg);
 		background-repeat: no-repeat;
 		width: 35px;
 		height: 45px;
 		float: right;
 		margin-top: -13px;
 	}
 	span.arrow-down {
 		background: url(http://midas.com.np/elearning/assets/images/open.jpg);
 		background-repeat: no-repeat;
 		width: 36px;
 		height: 49px;
 		float: right;
 		margin-top: -13px;
 		
 	}
 </style>-->
 <style type="text/css">
 .image_preview {
 	width: 70px;
 	margin-left: 10px;
 	border-radius: 3px;
 	display: none;
 }
 </style>
 
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 outerbdiv">
 	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 outerbdiv-in">
 		<div class="qanda" id="home-workk">
 			<div class="col-lg-12 col-md-12 col-sm-12 ask-question-div" style="padding-left: 0px; z-index: 1">
 				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 default-bg">
 					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pf-n-iptxt pad-fix">
 						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rounded-pf-pic pad-fix">
 							<img src="<?= $this->session->userdata('image');?>" class="img-responsive img-circle pull-left"/>
 							<!--<img src="http://myschool.midas.com.np/api/elearning/../../uploads/students/1621337.jpg" class="img-responsive img-circle pull-left"/>-->

 							<textarea placeholder="Ask a question..." rows="3" data-toggle="modal" data-target="#myModal" id="questiontext" class="pull-right"></textarea>

 						</div>
 					</div>
 				</div>
 				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 post-btns default-bg">
 					<div class="col-xs-6"style="margin-top: 10px;"></div>
 					<div class="col-xs-6 post-opt"style="margin-top: 5px;">
 						<div class="pull-right" id="post-stat">
 							<input name="submit" value="Post" class="btn btn-success btn-send post-question" type="submit">
 						</div>
 						<div class="pull-right Wrapicon">
 							<label for="file-upload" class="custom-file-upload btn">
 								
 							</label>
 							<input id="file-upload" name="file_upload" type="file" class='qans-file'>
 						</div>
 						<div class="pull-right">
 							<img src="" id="chat_iamge_preview" class="image_preview">
 						</div> 
 					</div>
 				</div>
 			</div>
 			<div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 15px;">
 				<div class="dropdown pull-right">
 					<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="background: #ffffff;    color: #999998; padding-right: 0;"> Show Questions From
 						<i class="caret" aria-hidden="true" style="color: #B3B7BE;font-size: 10px;"></i>
 					</button>
 					<ul class="dropdown-menu" style="background: #FEFEFE;">
 						<li><a href="#"><img src="http://midas.com.np/elearning/assets/images/f-onlyme.png" style="transform: scale(0.8);">&nbsp;&nbsp;<span>Only Me</span></a></li>
 						<li><a href="#"><img src="http://midas.com.np/elearning/assets/images/f-myclass.png">&nbsp;&nbsp;<span>My Class</span></a></li>
 						<li><a href="#"><img src="http://midas.com.np/elearning/assets/images/f-myschool.png"style="transform: scale(1.1);">&nbsp;&nbsp;<span>My School</span></a></li>
 						<li><a href="#"><img src="http://midas.com.np/elearning/assets/images/f-all.png"style="transform: scale(1.1);">&nbsp;&nbsp;<span>All</span></a></li>
 					</ul>
 				</div>
 			</div>
 			<div class="col-lg-12 col-md-12 posted-questions-div">

 				<?php $this->load->vars(array('response'=>$data['response']));
 				$this->load->view('forum_posts')?>	
 			</div>	
 		</div>
 	</div>
 </div>

 <script>
  $('.collapse').on('shown.bs.collapse', function(){
    $(this).parent().find(".arrow-down").removeClass("arrow-down").addClass("arrow-up");
  }).on('hidden.bs.collapse', function(){
    $(this).parent().find(".arrow-up").removeClass("arrow-up").addClass("arrow-down");
  });
</script>  