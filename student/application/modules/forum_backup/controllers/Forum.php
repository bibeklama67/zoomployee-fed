<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forum extends Front_controller {

	function __construct()
	{
		parent::__construct();
   $d=$this->config->item('default');
   $df=$this->config->item($d); 
   $df['js'][]='student_progress/student_progress';
   $df['js'][]='common/student_api';
   $df['js'][]='dashboard/dashboard';

   $this->config->set_item($d,$df);
   // $this->load->model('dashboard_model','model');
   $this->load->model('frontend_model','frontmodel');
 }

 function homework()
 {
  $data = $this->input->post();
  $this->session->set_userdata('quizActivity', 'homeworklive');

  $this->load->vars(array(
    'data' => $data,
    'secondaryviewlink' => 'homework',

    ));
  $this->load->view('homework');   
} 

function mainhomeworkhelp()
 {
  $data = $this->input->post();
  $this->session->set_userdata('quizActivity', 'homeworklive');

  $this->load->vars(array(
    'data' => $data['data'],
    'subjectid'         =>    $data['subjectid'],    
    'classname'         =>    $data['classname'],
    'subjectname'       =>    $data['subjectname'],
    'chaptername'       =>    $data['chaptername'],
    'secondaryviewlink' => 'homework',

    ));
  $this->load->view('homework_wrapper');   
}

function answer($questionid)
{
  $data = $this->input->post();
  $this->session->set_userdata('quizActivity', 'homeworklive');

  $this->load->vars(array('questionid'=>$questionid,'data' => $data));
  $this->load->view('forum_answers');   
}

function newquestion()
{
  $data = $this->input->post();
  $this->session->set_userdata('quizActivity', 'homeworklive');

  $this->load->vars(array(
    'response'=>array($data['response']),
    ));
  $this->load->view('forum_posts');   
}

function newanswer()
{
 $data = $this->input->post();
 $this->session->set_userdata('quizActivity', 'homeworklive');

 $this->load->vars(array(
  'response'=>$data['response'],
  ));
 $this->load->view('new_answers');
}

function viewaskedby()
{
  $data = $this->input->post();
  $this->session->set_userdata('quizActivity', 'homeworklive');

  $this->load->vars(array(
    'response'=>$data['response'],
    ));
  $this->load->view('list_view');
}

}
