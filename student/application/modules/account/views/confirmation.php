<style type="text/css">
	body{
		background: #FFF !important;
	}
	ul.checkmark {
		list-style: none;
		margin-left: -27px;
	}

	ul.checkmark li:before {
		content:"\2713\0020";
		color: #0A0;
		font-weight: 600;
	}
	.packagewrapper{
		border: 1px solid #ccc;
		padding: 0;
	}

	.packagewrapper h3 {
		background: #fc9806 !important;
		color: #fff !important;
		padding: 15px;
		font-size: 16px;
		margin:0 !important;
	}
	.featurewrapper{
		padding: 5px;
	}
	.subscribe-btn{
		background: #ce5d35 !important;
		border-color: #ce5d35 !important;
		border-radius: 0px !important;
		font-size: 14px !important;
	}
	.packagewrapper .border-top {
		border-top: 2px solid #ce5d35 !important;
	}
</style>
<div>
	<?php 	
	
	foreach($packages as $packageid=>$package){ 
		$i=-1;?>
		<form method="post">

			<input type="hidden" name="packageid" value="<?= $packageid?>">
			<div class="col-xs-12 col-md-4 packagewrapper">
				<h3>You are subscribing to <?= $package['packagename']?></h3>
				<?php
				foreach($package as $key=>$features){
					$packagetotal[$key] = 0;

					if($key=='packagename')
						continue;
					$i++;
					?>
					<div class="featurewrapper" id="<?=$packageid.'_'.$key?>" <?= ($i>0?'style="display:none"':'')?>>	
						<ul class="checkmark">
							<?php
							foreach ($features as $id => $row) {
								?>
								<li><?= $row['featurename']?></li>
								<!-- <p><label><input type="checkbox" name="features_<?= $packageid?>_<?= $key?>[]" class="features" data-pid="<?= $packageid?>" data-day="<?= $key?>" value="<?= $id?>" data-amt="<?= $row['amount']?>" checked><?= $row['featurename']?></label></p> -->	
								<?php
								$packagetotal[$key] +=$row['amount'];
							}
							?>
						</ul>
					</div>			
					<?php }?>
					<div class="footer" style="padding: 0 15px 10px 15px;">
						<div class="border-top">
							<div class="col-xs-4 no-padding">Subscribe for: </div>
							<div class="col-md-6" style="display: inline-block;">
								<?php foreach($days[$packageid] as $day){ 
									if($day%32==0)
									{
										$month = $day/32;
										$duration = $month.' Month'.($month>1?'s':'');
										if($month>=12 && $month%12==0)
										{
											$year = $month/12;
											$duration = $year.' Year'.($year>1?'s':'');
										}
									}
									else
										$duration = $day .' Day'.($day>1?'s':'');
									?>
									<label><input type="radio" name="days" value="<?= $day?>" class="days"  data-pid="<?= $packageid?>" checked style="visibility: hidden;"> <?= $duration?></label><br>
									<?php }?>


								</div>
								<div class="col-xs-12 no-padding">		
									<div class="pull-left"><label class="valid-date" style="margin-top: 8px;">Valid till 31st March 2018.</label></div>
									<div class="pull-right">
										<?php foreach($days[$packageid] as $k=>$day){ ?>
										<div class="packagetotal" id="d<?= $packageid.'_'.$day?>" <?= ($k+1==count($days[$packageid])?'':'style="display:none"')?>><label style="font-size: 22px;"> Rs. <?= $packagetotal[$day]?>.00</label></div>
										<?php }?>
									</div>	
								</div>
								<div class="clear-both pull-right" style="margin-bottom: 10px;">
									<button class="btn btn-success subscribe-btn">Confirm</button>
								</div>
							</div>
						</div>
					</div>
				</form>

				<?php
			}?>
		</div>
		<script type="text/javascript">
			$('.subscribe-btn').on('click',function(){
				var data = $(this).closest('form').serialize();
				$.ajax({
					type:"POST",
					url: base_url+"account/api/purchase",
					data: data, 
					success:function(result) {
						console.log(result);
					}
				});
// console.log($('#subscribtion-form').serialize());
return false;
});
</script>