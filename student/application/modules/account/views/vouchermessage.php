<style type="text/css">
	p{
		text-align: justify;
	}
</style>
<?php 
// $head = '<div style="text-align:justify" class="col-xs-12"><p style="color: #dc5423; text-align:center;padding-top:10px">'.@$response[0][0]->childname.'</p><p style="color: #dc5423; text-align:center">You can use this service for 15 days free of charge.</p><br>';
$head = '';

switch ($type) {
	case 'HH':
	$html = '<p style="text-align:center;">Welcome to Online Tutoring!</p>'.$head.'<p><font align="justify">हरेक दिन बेलुका ५ बजे देखि ८ बजे सम्म सबै बिषयका शिक्षकहरू MiDas App मा उपलब्ध हुनुहुन्छ।<br /><br />पढेको कुर नबुझेमा, होमेवोर्क् गर्न नजानेमा वा पढाईको बारेमा कुनै जिज्ञासा भएमा, यस MiDas App को Online Tutoring मा गएर आफ्नो प्रश्न राख्नुहोस। तुरून्त उत्तर पाउनु हुन्छ ।</font></p><p><font align="justify"><strong>Online Tutoring</strong> is an online tutoring service to help students to understand lessons.<br /><br />Subject wise teachers will be available online everyday 5 PM to 8 PM to help students.<br /><br />At MiDas, we don\'t feed students the answers to homework questions. Instead, we show them how to learn. Our teachers are all experienced classroom teachers who know how to help students understand concepts—not just memorize them. Our teaching style is friendly and supportive.</font></p>';
	break;
	case 'T':
	$html = '<p style="text-align:center;">Welcome to Tutorial Videos!</p>'.$head.'<p>नर्सरी देखि कक्षा दश सम्मका मुख्य विषयका पाठहरुको Tutorial Videos MiDas App मा उपलब्ध छन्। यसको प्रयोगले गर्दा विध्यार्थीहरुले अप्ठेरा पाठहरु पनि सजिलै बुझ्छन र उनीहरुको सिकाई दिर्घकालीन हुन्छ ।</p>
	<p><strong>Tutorial videos</strong> in <strong>MiDas App</strong> guide students through each chapter of their course book with audio and visual aid. Innovatively animated Tutorial Videos in MiDas App help students to clearly grasp the essence of every lesson of their book. This helps them not only to visualize the practical aspects of the lessons but also effectively implement them in their daily life. The Tutorial Videos helps students to remember the lessons they have learnt.</p>';
	break;
	case 'PE':
	$html = '<p style="text-align:center;">Welcome to Exam Preparation!</p>'.$head.'<p>विध्यार्थीहरूले MiDas App को Exam Preparation मा गएर आफुले पढेको पाठ छानेर जाँचको तयारी गर्न सक्छन्।</p>
	<p>Students can prepare for exams through <strong>Exam Preparation</strong> in <strong>MiDas App</strong> by selecting the chapters of their course books. This will not only enable them to perform excellently in the exams but also help them  to avoid unnecessary stress.</p>';
	break;
	case 'QE':
	$html = '<p style="text-align:center;">Welcome to Quizzes &amp; Exercises!</p>'.$head.'<p>विध्यार्थीहरूले MiDas App को Quizzes &amp; Exercises मा गएर पाठमा आधारित बिभिन्न प्रकारका खेलहरु खेलेर सिक्न सक्छन ।</p>
	<p>Students can test their knowledge regarding the specific chapter of their course book through the interactive Quizzes and Exercises in MiDas App. This will help students become more sharp and dynamic by greatly enhancing their acquired knowledge.</p>';
	break;
	case 'LC':
	$html = '<p style="text-align:center;">Welcome to MiDas LIVE!</p>'.$head.'<p>
	<p style="text-align:center;">Please click on continue to join LIVE Class.</p>';
	break;
	default:
		# code...
	break;
}
// $html .= '<p style="color: #dc5423; text-align:center">This service expires on: '.explode(' ',@$response[0][0]->expirydate)[0].'</p></div>';

echo $html;
?>