<style>
	#subscriptiontable th, #subscriptiontable td{
		border:1px solid #ddd;
		font-size: 15px;
	}
	#subscriptiontable th{
		color: 
	}
	tr.header{
		background: #eaeef1 !important;
		color: #697b8f;
	}
	#subscriptiontable td{
		border:1px solid #ddd;
		font-size: 14px;
		background: #fff;
	}
	
	@media screen and (max-width: 767px){
		.table-responsive>.table>tbody>tr>td,
		.table-responsive>.table>tbody>tr>th,
		.table-responsive>.table>tfoot>tr>td,
		.table-responsive>.table>tfoot>tr>th,
		.table-responsive>.table>thead>tr>td,
		.table-responsive>.table>thead>tr>th {
			white-space: nowrap;
			padding: 10px;
			font-size: 13px;
		}
	}
	@media screen and (max-width: 350px){
		.table-responsive>.table>tbody>tr>td,
		.table-responsive>.table>tbody>tr>th,
		.table-responsive>.table>tfoot>tr>td,
		.table-responsive>.table>tfoot>tr>th,
		.table-responsive>.table>thead>tr>td,
		.table-responsive>.table>thead>tr>th {
			white-space: nowrap;
			padding: 7px;
			font-size: 12px;
		}
	}


</style>

<?php
$result = $data['result'];
$isparent = $data['isparent'];
?>
<div style="padding: 10px;">
	<div class="" style="margin-top: 10px; ">
		<?php if(!empty($result)){ ?>
		<table class="table" style="width:100%; border:1px solid #ddd" id="subscriptiontable">
			<tbody>
				<tr class="header">
					<!-- <th>S. No</th> -->
					<th width="72%">Courses</th>
					<th width="28%">Expiry Date</th>                                    
				</tr>
				<?php      
				if($isparent=='true')
				{
					$parentresult = $result;
					foreach ($parentresult as $key => $result) {						

						foreach($result as $key=>$row){
							if(count($result)>1 && $row->subscriptiontype=='SignUpVoucher')
								continue;
							?>
							<tr>
								<!-- <td><?= $key+1; ?></td> -->
								<td><?= 'Grade '.$row->classname.'<br>'.$row->subjectname;?></td>
								<!-- <td><?= 'Grade '.$row->classname.' <strong>('.$row->childname.')</strong><br>'.$row->subjectname;?></td> -->
								<?php 
								if(intval($row->noofdays) < 15)
									$expirydate = date('Y-m-d h:i A',strtotime($row->expirydate));
								else
									$expirydate = date('Y-m-d',strtotime($row->expirydate));

								$unsubscribebtn = '';
								if($row->isautonrenew !='f' && !$row->unsubscribeddate){
									$unsubscribebtn = '<br><button class="btn btn-sm btn-primary unsubscribePackage" data-userid="'.$row->userid.'" data-couponno="'.$row->couponno.'"">Unsubscribe</button>';
								}
								?>

								<td align="center">
									<?php 
										$subscription=$this->session->userdata('subscription');
										$subscriptiontype=$this->session->userdata('subscriptiontype');
										$daysremaining=$this->session->userdata('daysremaining');
										$days = $daysremaining / (1000*60*60*24);
										// var_dump($days);
										$curdate=date('y-m-d');
									?>
									<?php if($subscription=='Y'):?>
										<?php echo date('Y-m-d', strtotime($curdate. ' + '.floor($days).' days'));?>
									<?php else:?>
										<?= ($row->subscriptiontype=='SignUpVoucher'?'Unpaid <br> Subscription':$expirydate.$unsubscribebtn);?>
									<?php endif;?>
								</td>
								<!-- <td><?= $row->couponno.'<br>'.($row->subscriptiontype=='PaidVoucher'?'NRS, '.number_format($row->amount, 2):''); ?></td> -->
							</tr>
							<?php } 
						}
					}
					else{
						foreach($result as $key=>$row){
							if(count($result)>1 && $row->subscriptiontype=='SignUpVoucher')
								continue;
							?>
							<tr>
								<!-- <td><?= $key+1; ?></td> -->
								<td><?= 'Grade '.$row->classname.'<br>'.$row->subjectname;?></td>
								<?php 
								if(intval($row->noofdays) < 15)
									$expirydate = date('Y-m-d h:i A',strtotime($row->expirydate));
								else
									$expirydate = date('Y-m-d',strtotime($row->expirydate));
								?>
								<td><?= ($row->subscriptiontype=='SignUpVoucher'?'Unpaid <br> Subscription':$expirydate);?></td>
								<!-- <td><?= $row->couponno.'<br>'.($row->subscriptiontype=='PaidVoucher'?'NRS, '.number_format($row->amount, 2):''); ?></td> -->
							</tr>
							<?php } 
						}     ?>
					</tbody>
				</table>
				<?php
			} else { 
				echo '<div style="width:100%;display:block;text-align:center"><strong>You have not subscribed to MiDas eCLASS yet.</strong></div>';
			}  ?>       
		</div>
	</div>

	<!-- Modal -->
	<div id="unsubscribePackageModal" class="modal fade" role="dialog" style="display: none;">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Unsubscribe Package</h4>
				</div>
				<div class="modal-body">
					<p>Are You sure you want to unsubscribe this package?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success yesUnsubscribe">Yes</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
			</div>

		</div>
	</div>
	<?php if($data['source']!='web'){?>
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
	<?php }?>
	<script type="text/javascript">
		$(document).off('click','.unsubscribePackage');
		$(document).on('click','.unsubscribePackage',function(){
			if(confirm('Are you sure you want to unsubscribe?'))
			{
				// alert("You have been unsubscribed successfully.");
				var e =$(this);
				var url = '/account/api/unsubscribePackage';
				var userid   = $(this).data('userid');
				var couponno = $(this).data('couponno');
				var data = {userid:userid,couponno:couponno};
				$.post(url,data,function(result){
					var response = JSON.parse(result);
					console.log(response);
					if(response.type == 'success'){
					  e.hide();
						alert("You have been unsubscribed successfully.");
						// unsubscribePackagebtn.hide();
						// $('#unsubscribePackageModal .modal-body').html(response.message);
						
					}else{
						alert("Could not unsubscribe this package.please try again later.");
						// $('#unsubscribePackageModal .modal-body').html(response.message);
					}
				})
			}
		});
		// $(document).ready(function(){
		// 	var userid ='';
		// 	var couponno = '';
		// 	var unsubscribePackagebtn;
		// 	$(document).off('click','.unsubscribePackage');
		// 	$(document).on('click','.unsubscribePackage',function(){
		// 		alert('Are You sure you want to unsubscribe this package?');
		// 		unsubscribePackagebtn = $(this);
		// 		userid   = $(this).data('userid');
		// 		couponno = $(this).data('couponno');
		// 		// $('#unsubscribePackageModal').modal('show');

		// 	});
		// 	$(document).off('click','.yesUnsubscribe');
		// 	$(document).on('click','.yesUnsubscribe',function(){
		// 		var e = $(this);
		// 		var data = {userid:userid,couponno:couponno};
		// 		var url = '/account/api/unsubscribePackage';
		// 		$.post(url,data,function(result){
		// 			var response = JSON.parse(result);
		// 			console.log(response);
		// 			if(response.type == 'success'){
		// 				unsubscribePackagebtn.hide();
		// 				$('#unsubscribePackageModal .modal-body').html(response.message);
		// 				e.hide();
		// 			}else{
		// 				$('#unsubscribePackageModal .modal-body').html(response.message);
		// 			}
		// 		})
		// 	});

		// })
	</script>
