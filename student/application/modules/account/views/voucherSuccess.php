<?php 
if(!$response)
{
	?>
	<font align="center" style="width: 100%;display: flex;height: 50%;margin: 0 auto;align-items: center;text-align: center;font-size: 20px;color: #ce5d35;"><span style="text-align: center;width: 100%;
		">This feature will be available soon.</span></font>
		<?php
	}
	foreach($response as $res){
		$row = $res[0];
		if($row->noofdays%32==0)
		{
			$month = $row->noofdays/32;
			$duration = $month.' Month'.($month>1?'s':'');
			if($month>=12 && $month%12==0)
			{
				$year = $month/12;
				$duration = $year.' Year'.($year>1?'s':'');
			}
		}
		else
			$duration = $row->noofdays .' Day'.($row->noofdays>1?'s':'');
		?>
		<div class="container" style="width:100%; padding: 0px;">
			<div class="col-xs-12 wrapvsmsg">
				<div class="col-xs-12 vsmsg">
					<h3>Congratulations!!</h3>
					<h4>You have got Gift Voucher worth</h4>
					<h2>Rs. <?= $row->packageamount?>.00</h2>
					<h4 class="unbold">With the Voucher, we have activated following study material for you.</h4>
					<div class="col-xs-12 wraptable">
						<table class="table">
							<?php if($row->childname){?>
							<tr>
								<td class="tstyle1">Child Name:</td>
								<td style="color:#908e8f;"><?= $row->childname?></td>
							</tr>
							<?php }?>
							<tr>
								<td class="tstyle1">Grade:</td>
								<td style="color:#908e8f;"><?= $row->classname?></td>
							</tr>
							<tr>
								<td class="tstyle1">Subjects:</td>
								<td style="color:#908e8f;"><?= $row->subjects?></td>
							</tr>
							<tr>
								<td class="tstyle2">Features:</td>
								<td style="color:#afadae;;"><?= $row->features?></td>
							</tr>
							<tr>
								<td class="tstyle2">Subscribed For:</td>
								<td style="color:#afadae; "><?= $duration?></td>
							</tr>
							<tr>
								<td class="tstyle2">Subscribed on:</td>
								<td style="color:#afadae; "><?= $row->subscriptiondate?></td>
							</tr>
							<tr>
								<td class="tstyle1">Expires on:</td>
								<td style="color:#908e8f;"><?= $row->expirydate?></td>
							</tr>
							<tr>
								<td class="tstyle1">Price:</td>
								<td style="color:#908e8f;" >Rs. <?= $row->packageamount?>.00</td>
							</tr>
						</table></div>
					</div>
				</div></div>
				<?php }?>
				<div class="row">
					<div class="col-xs-12" style="margin-bottom: 37px; text-align: center;">
						<?php 
						if($fromweb){?>
						<a href="<?= base_url()?>tutorials" class="btn btn-info">Continue</a>
						<br>
						<?php }?>
					</div>
				</div>