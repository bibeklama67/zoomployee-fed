<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Api extends App_controller{
    function __construct(){
        parent::__construct();        
        $this->load->model('Account_model', 'model');
    }

    function test()
    {
        die("here");
        $this->db->trans_begin();
        $this->db->query('CREATE RULE "unqtest_ignore_duplicate" AS ON INSERT TO "unqtest" WHERE EXISTS(SELECT 1 FROM unqtest WHERE (title)=(NEW.title)) DO INSTEAD NOTHING;');
        sleep(2000);
        $this->db->insert('unqtest',array('id'=>1,'title'=>'test'));
        $this->db->insert('unqtest',array('id'=>1,'title'=>'test'));
        $this->db->query('DROP RULE "unqtest_ignore_duplicate" ON "unqtest";');
        $this->db->trans_commit();
    }

    function test1()
    {
        $this->db->insert('unqtest',array('id'=>1,'title'=>'test1'));
        $this->db->insert('unqtest',array('id'=>1,'title'=>'test1'));
    }
    /**
     * Get Account
     * @param type $authcode 
     * @return type
     */
    function index__(){
        try{
            // if($this->input->get('authcode')!='334caf14c5add138750e5c7b8d0aea67')
            // {
            //     echo '<font align="center" style="width: 100%;display: flex;height: 100%;margin: 0 auto;align-items: center;text-align: center;font-size: 20px;color: #ce5d35;"><span style="text-align: center;width: 100%;
            //     ">Your subscription detail will be available here.</span></font>';
            //     exit;
            // }
            // print_r($_GET);
            $getdata = $_GET;
            $result  = $this->model->getParentAccount($getdata);

            $this->load->vars(
                array(
                    'secondaryviewlink' => 'parent/myAccount',
                    'result'  => $result,
                    'isparent' => $getdata['isparent']
                    )
                );
            
            $this->render_page(); 

        } catch(Exception $e){
            echo '<p style="text-align: center; padding: 10px; background: white;">'.$e->getMessage().'</p>';
        }   
    }

    function index(){
        try{
            // if($this->input->get('authcode')!='334caf14c5add138750e5c7b8d0aea67')
            // {
            //     echo '<font align="center" style="width: 100%;display: flex;height: 100%;margin: 0 auto;align-items: center;text-align: center;font-size: 20px;color: #ce5d35;"><span style="text-align: center;width: 100%;
            //     ">Your subscription detail will be available here.</span></font>';
            //     exit;
            // }
            // print_r($_GET);
            $getdata = $_GET;
            $result  = $this->model->getParentAccount($getdata);

            // $this->load->vars(
            //     array(
            //         'secondaryviewlink' => 'parent/myAccount',
            //         'result'  => $result,
            //         'isparent' => $getdata['isparent']
            //         )
            //     );
            
            // $this->render_page(); 
            $data = array('result' => $result,'isparent' => $getdata['isparent']);
            $this->load->vars(array('data'=>$data));
            $this->load->view('parent/myAccount');

        } catch(Exception $e){
            echo '<p style="text-align: center; padding: 10px; background: white;">'.$e->getMessage().'</p>';
        }   
    }

    function myAccount($authcode=false){

        // echo "<p>".$response."</p>";
        $this->load->vars(array(
            'secondaryviewlink' =>    'parent/myAccount',
            'student_name'      =>    @$student_name
            ));

        $this->render_page(); 
    }

    /**
     * Get Sugscription
     * @return type
     */
    function subscription($authcode=false){
        try{
            if(!$authcode)
            {
                echo '<font align="center" style="width: 100%;display: flex;height: 100%;margin: 0 auto;align-items: center;text-align: center;font-size: 20px;color: #ce5d35;"><span style="text-align: center;width: 100%;
                ">You can purchase Voucher to subscribe MiDas App Study Material very soon.</span></font>';
                exit;
            }
            $response = "Subscription";
            
            if(!@$_GET['authcode'] || !@$_GET['mobileno'] || !@$_GET['classid'])
                throw new Exception("This feature is currently unavailable.", 1);
            $sessiondata = $_GET;
            $this->model->setSession($sessiondata);
            // print_r($sessiondata);
            $response = $this->model->getPackages();
            // print_r($response);
            $this->load->vars(array(
                'secondaryviewlink' =>    'subscription',
                'packages'      =>    $response['packages'],
                'days'      =>    $response['days']
                ));
            $this->render_page(); 
        } catch(Exception $e){
            $response = $e->getMessage();
            echo $response;
        }
        // echo "<p>".$response."</p>";
    }

    function confirm($reload=false)
    {
        try {
            if($reload)
            {
                $response = $this->model->getPackages(true);
                $this->load->vars(array(
                    'secondaryviewlink' =>    'confirmation',
                    'packages'      =>    $response['packages'],
                    'days'      =>    $response['days']
                    ));               
                $this->render_page(); 
            }
            else{
                $postdata = $this->input->post();
                $postdata['usertype'] = 'Student';
                $this->model->setCartSession($postdata);                      
            }
        } catch (Exception $e) {

        }
    }

    function purchase(){
        try {
         $postdata = $this->input->post();
         $postdata['usertype'] = 'Student';
         print_r($postdata);
         exit;
         $this->model->processPurchase($postdata);
     } catch (Exception $e) {
        echo $e->getMessage();
    }
}

function unsubscribePackage(){
    try{
        $type = "success";
        $message = "Package has been unsubscribed successfully.";
        $result = $this->model->unsubscribePackage();
        if(!$result)
            throw new Exception("Error Processing Request", 1);

    }catch(Exception $e){
        $type = "error";
        $message = $e->getMessage();
    }
    echo json_encode(array('type'=>$type,'message'=>$message));
}

function voucherSuccess($fromweb=false)
{
    try {

        $IParray=array_values(array_filter(explode(',',$_SERVER['HTTP_X_FORWARDED_FOR'])));
        $ipaddress = end($IParray);
        //var_dump($_GET);exit();

        if(!isset($_GET['authcode']))
            throw new Exception("Welcome to midas.", 1);

        if(!isset($_GET['type']))
            throw new Exception("Welcome to MiDas.", 1);

        $sessiondata = $_GET;
        
        $this->model->setSession($sessiondata);
        $sessiondata['userid'] = $this->session->userdata('apiuserid');     
        $sessiondata['ipaddress'] = $ipaddress;
        if($fromweb=='true')
            $sessiondata['devicetype'] = 'Web';
        else
            $sessiondata['devicetype'] = 'Android';
                
           // var_dump('here');exit;
        $response = $this->model->getPurchasedVoucher($sessiondata);  
        //var_dump($response);exit();
        
        if(!$response)
            throw new Exception("No subscription details available.", 1);

        // $this->load->vars(array('secondaryviewlink' =>  'voucherSuccess','response'=>$response,'fromweb'=>$fromweb));
        $this->load->vars(array('secondaryviewlink' =>  'vouchermessage','response'=>$response,'fromweb'=>$fromweb,'type'=>$_GET['type']));
        if($fromweb=='true')
            $this->render_page('account_view'); 
        else
            $this->render_page(); 
    } catch (Exception $e) {
        echo '<font align="center" style="width: 100%;display: flex;height: 100%;margin: 0 auto;align-items: center;text-align: center;font-size: 20px;color: #ce5d35;"><span style="text-align: center;width: 100%;
        ">'.$e->getMessage().'</span></font>';
        
    }
}

function hblpay()
{
    // ini_set('display_errors', 1);
    // ini_set('display_startup_errors', 1);
    // error_reporting(E_ALL);  
    try{
        if(!$_GET['txncode'])
            throw new Exception("Invalid Request.", 1);
        $response = $this->model->gethbltransaction($_GET);

        // $this->load->vars(array('response'=>$response,'fromapp'=>true));
        // $this->load->view('subscription/hblpgw');
        $this->load->vars(array('secondaryviewlink' => 'subscription/hblpgw','response'=>$response,'fromapp'=>true));
        $this->render_page(); 

    }catch (Exception $e) {
       echo '<font align="center" style="width: 100%;display: flex;height: 100%;margin: 0 auto;align-items: center;text-align: center;font-size: 20px;color: #ce5d35;"><span style="text-align: center;width: 100%;
       ">'.$e->getMessage().'</span></font>';
   }
}

function processNonVoucherUsers()
{
    $this->load->model('Process_model', 'pmodel');    
    $this->pmodel->processNonVoucherUsers();
}
}
