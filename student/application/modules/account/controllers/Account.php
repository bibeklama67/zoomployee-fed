<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends Front_controller {

	function __construct()
	{
		parent::__construct();
   $d=$this->config->item('default');
   $df=$this->config->item($d); 
   $df['js'][]='common/student_api';
   $df['js'][]='dashboard/dashboard';

   $this->config->set_item($d,$df);
 }

 public function index()
 {
  $userid=$this->session->userdata('userid');
  $title=$this->input->post('title');
  $student_name=$this->session->userdata('student_name');

  $this->load->vars(array(
    'secondaryviewlink' =>    'list',
    'formview'          =>    'student_progress/progress/studenthome',
    'searchview'        =>    'templates/header/search',
    'student_name'      =>    $student_name
    ));
  $this->render_page();		
}	

function listSubjects()
{
  $data = $this->input->post();
  $response = $data['response'];
  $title=$this->input->post('title');
  $this->load->vars(array(
    'response'          =>    $response,
    'title'             =>    $title

    ));
  $this->load->view('subjects', $response);
}

function listChapters()
{
  $data = $this->input->post();  
  $response = $data['response'];
  $title=$this->input->post('title');
  $this->load->vars(array(
    'response'          =>    $response,
    'title'             =>    $title,
    'subjectname'       =>    $data['subjectname']
    ));
  $this->load->view('chapters', $response);
}
}
