<?php 
if(!defined('BASEPATH')) exit('direct access invalid');

class Process_model extends CI_Model{
	
	function __construct() {
		parent::__construct();
		$this->simsdb =  $this->load->database('sims',TRUE);
		//$this->db = $this->load->database('default', TRUE);
		// ini_set('display_errors', 1);
		// ini_set('display_startup_errors', 1);
		// error_reporting(E_ALL); 
		$this->load->model('Account_model', 'model');

	}

	function processNonVoucherUsers()
	{
		$sql = "SELECT min(m.datapostdatetime) as usedatetime,m.userid,psr.mobileno,m.authcode
		from sch_studentcurrent s 
		join sch_parentstudentrelation psr on psr.studentid = s.studentid 
		join org_midasstudent m on m.userid = s.userid 
		left join eclassuse_log e on e.userid = m.userid
		left join vouchers.vouchers v on v.userid = m.userid 
		where
		v.voucherno is null and 
		m.datapostdatetime::date between '2017-12-18' and  '2017-12-24'
		and m.sellercode is null and gclassid not in (23,24,25)
		group by m.userid,psr.mobileno
		order by m.datapostdatetime DESC limit 30";
		$users = $this->simsdb->query($sql)->result();
		// echo $this->simsdb->last_query();
		foreach ($users as $key => $row) {
		echo "<br>";
		echo "<br>";
		echo "<br>";

			$postdata['authcode'] = $row->authcode;
			$postdata['isparent'] = 'false';
			$postdata['mobileno'] = $row->mobileno;
			$postdata['type'] = 'T';
			$postdata['childid'] = $row->userid;
			$postdata['istest'] = 'TRUE';
			$postdata['usedatetime'] = $row->usedatetime;
			$postdata['userid'] = $row->userid;
		print_r($postdata);
			
			$this->model->setSession($postdata);			
			$this->model->getPurchasedVoucher($postdata);
		}
	}
	
}