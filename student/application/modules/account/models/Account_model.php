<?php 
if(!defined('BASEPATH')) exit('direct access invalid');

class Account_model extends CI_Model{
	
	function __construct() {
		parent::__construct();
		$this->simsdb =  $this->load->database('sims',TRUE);
		//$this->db = $this->load->database('default', TRUE);
		// ini_set('display_errors', 1);
		// ini_set('display_startup_errors', 1);
		// error_reporting(E_ALL);        
	}

	/**
	 * Return account detals 
	 * on parent
	 * @return type
	 */

	function district(){
		$result =$this->simsdb->select('districtid,districtname')
		->from('district')
		->order_by('districtname')
		->get()
		->result();
		return $result;
	}
	function getParentAccount($getdata){
		try{
			if(empty($getdata['authcode']))
				throw new Exception("This feature will be available soon.", 1);

			if($getdata['userid'])
			{
				$userid=$getdata['userid'];
			}else
			{
				$userid = $this->getUserid($getdata['authcode'],$getdata['isparent'],@$getdata['mobileno']);
			}
			

			 //Final data
			// $userid   =  1000136879; //Testing data
			//var_dump($userid);exit;

           // $getdata['isparent']=false;
			if($getdata['isparent']=='true')
			{
				// $children = $this->simsdb->select('userid,studentfirstname,studentlastname')->join('sch_parentstudentrelation psr','psr.studentid = sch.studentid')->get_where('sch_studentcurrent sch',array('psr.mobileno' => $getdata['mobileno']))->result();
				// foreach ($children as $key=>$child) {
				$sql = "select DISTINCT on (classname,subscriptiontype)classname,subscriptiontype,couponno,expirydate,amount,noofdays, subjectname,isautonrenew,userid,unsubscribeddate from (SELECT DISTINCT on (us.couponno) couponno, classname, us.subscriptiontype,us.expirydate, sum(us.amount) as amount, noofdays, regexp_replace( regexp_replace( '*#*' || array_to_string( ARRAY_AGG ( DISTINCT CASE WHEN s.eclassactive = 'Y' THEN COALESCE(s.sortorder,0) :: TEXT || s.subjectname || '<br>&nbsp;&nbsp;<font color=\"#fc9705\">(Animated Video Lessons Included)</font>' ELSE COALESCE(s.sortorder,0) :: TEXT || s.subjectname END ), '*#*' ), '\*\#\*\d*', '*#*', 'g' ), '^\*\#\*', '', 'g' ) as subjectname,COALESCE(isautorenew,'f') as isautonrenew,userid,unsubscribeddate FROM el_user_subscription us JOIN el_class C ON C .classid = us.classid JOIN el_subject s ON s.subjectid = us.subjectid WHERE case when us.classid = 90 then true else s.isactive='Y' end and /*us.subscriptiontype != 'SignUpVoucher'*/ case when us.subscriptiontype = 'PaidVoucher' then true else us.subscriptiontype='SignUpVoucher' end  and userid = ".$userid." GROUP BY couponno, expirydate, noofdays, classname, us.subscriptiontype,isautonrenew,userid,unsubscribeddate) as t order by classname,subscriptiontype,expirydate desc";

				$result[$key] = $this->db->query($sql)->result();
				 //var_dump($this->db->last_query());exit;
				if($result[$key]){
					$dataArray  = array();
					$total      = 0; 
					foreach($result[$key] as $rkey=>$row){
							// $result[$key][$rkey]->childname = $child->studentfirstname.' '.$child->studentlastname;
						$result[$key][$rkey]->childname = '';					
						$row->subjectname = str_replace('*#*', '<br>', $row->subjectname);
					}						
				} 
				//var_dump($result);exit;
				// }
				return $result;
			}
			else
			{
				$sql = "select DISTINCT on (classname,subscriptiontype)classname,subscriptiontype,couponno,expirydate,amount,noofdays,priority, subjectname from (SELECT DISTINCT on (us.couponno,classname) couponno, classname, us.subscriptiontype,us.expirydate, sum(us.amount) as amount, noofdays, c.priority,regexp_replace( regexp_replace( '*#*' || array_to_string( ARRAY_AGG ( DISTINCT CASE WHEN s.eclassactive = 'Y' THEN COALESCE(s.sortorder,0) :: TEXT || s.subjectname || '<br>&nbsp;&nbsp;<font color=\"#fc9705\">(Animated Video Lessons Included)</font>' ELSE COALESCE(s.sortorder,0) :: TEXT || s.subjectname END ), '*#*' ), '\*\#\*\d*', '*#*', 'g' ), '^\*\#\*', '', 'g' ) as subjectname FROM el_user_subscription us JOIN el_class C ON C .classid = us.classid JOIN el_subject s ON s.subjectid = us.subjectid WHERE s.isactive='Y' and us.subscriptiontype != 'SignUpVoucher' and userid = ".$userid." GROUP BY couponno, expirydate, noofdays, classname, priority, us.subscriptiontype) as t order by classname,subscriptiontype, priority,expirydate desc";

				$result = $this->db->query($sql)->result();
				//var_dump($this->db->last_query());exit;

			// if($getdata['authcode']=='6d14c9568decee68c6d2329e51b92d95a71c576e')
			// 	echo $this->db->last_query();
				if($result){
					$dataArray  = array();
					$total      = 0; 
					foreach($result as $row){
						$row->subjectname = str_replace('*#*', '<br>', $row->subjectname);
					}

					return $result;
				} 
			}return $result;

		} catch(Exception $e){
			throw $e;
		}
	}

	/**
	 * Get userid by authcoe
	 * @param type $authcode 
	 * @return type
	 */
	function getUserid($authcode,$isparent,$mobileno){
		try{
			if(empty($authcode))
				throw new Exception("This feature will be available soon.", 1);

			if($isparent=='true')
			{
				$this->simsdb->select('userid')
				->from('org_midasuser')
				->where('mobilenumber', $mobileno);
			}
			else
			{
				$this->simsdb->select('userid')->from('org_midasstudent')->where('authcode', $authcode);
			}

			$result  = $this->simsdb->get()->row();

			if($result)
				return $result->userid;
			else 
				throw new Exception("This feature will be available soon.", 1);

		} catch(Exception $e) {
			throw $e;
		}
	}

	function setSession($sessiondata)
	{
		try {						
			if(@$sessiondata['isparent']=='true')
				$user = $this->simsdb->get_where('org_midasuser',array('authcode'=>$sessiondata['authcode']))->row();
			else
				$user = $this->simsdb->get_where('org_midasstudent',array('authcode'=>$sessiondata['authcode']))->row();
			if(!$user)
				throw new Exception("This feature is currently unavailable.", 1);   
			$this->session->set_userdata('apiuserid', $user->userid);
			$this->session->set_userdata('apimobileno', @$sessiondata['mobileno']);
			$this->session->set_userdata('apiclassid', @$sessiondata['classid']);
			$this->session->set_userdata('apichildid', @$sessiondata['childid']);
			$this->session->set_userdata('isparent', @$sessiondata['isparent']);
		} catch (Exception $e) {
			throw $e;
		}        
	}

	function setCartSession($postdata)
	{
		$this->session->set_userdata('packageid', $postdata['packageid']);
		$this->session->set_userdata('days', $postdata['days']);
		$this->session->set_userdata('usertype', $postdata['usertype']);
	}
	/**
	 * Get Package Details     
	 * @return type
	 */
	function getPackages($cartSession=false)
	{
		if($cartSession)
		{
			$packageid = $this->session->userdata('packageid');
			$noofdays = $this->session->userdata('days');
			$sql = "select pm.packageid,pm.packagename,pd.noofdays,pd.amount,pd.featuresid,fm.featurename from vs_packages_master pm join(
			select distinct noofdays,sum(amount)as amount,packageid,featuresid from vs_packages_detail where packageid = ".$packageid." and noofdays = ".$noofdays." group by packageid,noofdays,featuresid
			) pd on pd.packageid = pm.packageid
			join vs_features_master fm on fm.featuresid = pd.featuresid order by pm.packageid";
			// echo $sql;exit;
		}
		else
		{
			$classid = $this->session->userdata('apiclassid');
			$sql = "select pm.packageid,pm.packagename,pd.noofdays,pd.amount,pd.featuresid,fm.featurename from vs_packages_master pm join(
			select distinct noofdays,sum(amount)as amount,packageid,featuresid from vs_packages_detail where classid = ".$classid." group by packageid,noofdays,featuresid
			) pd on pd.packageid = pm.packageid
			join vs_features_master fm on fm.featuresid = pd.featuresid order by pm.packageid";
		}
		// $sql = "select pm.packageid,pm.packagename,pd.noofdays,pd.amount,pd.featuresid,fm.featurename from vs_packages_master pm join(
		// select distinct noofdays,amount,packageid,featuresid from vs_packages_detail where classid = ".$classid." group by packageid,noofdays,featuresid
		// ) pd on pd.packageid = pm.packageid
		// join vs_features_master fm on fm.featuresid = pd.featuresid order by pm.packageid";
		$qry = $this->db->query($sql);
		$result = $qry->result();
		$packages = array();
		$days = array();
	   // echo $result[0]->packageid;
	   // $days[$result[0]->packageid][] = $result[0]->noofdays;
		foreach ($result as $key => $row) {
			if(!@$days[$row->packageid])
				$days[$row->packageid][] = $row->noofdays;

			if(!in_array($row->noofdays, $days[$row->packageid]))
				$days[$row->packageid][] = $row->noofdays;
			$packages[$row->packageid]['packagename'] = $row->packagename;           
			$packages[$row->packageid][$row->noofdays][$row->featuresid]['featurename'] = $row->featurename;
			$packages[$row->packageid][$row->noofdays][$row->featuresid]['amount'] = $row->amount;
		}

		$response['packages'] = $packages;
		$response['days'] = $days;
		return $response;
	}

	function processPurchase($postdata)
	{
		try {
			if(!$this->session->userdata('apiuserid'))
				throw new Exception("Something went wrong. Please try again later.", 1);

			$packageid = $postdata['packageid'];
			$days = $postdata['days'];
			$featurename = 'features_'.$packageid.'_'.$days;
			$featuresarr = $this->input->post($featurename);
			$features = implode(',',$featuresarr);

			$nearexdays = $days-5;
			$curdate     = date('Y-m-d');
			$posteddate = date('Y-m-d H:i:s.u');
			$expdate = date('Y-m-d',strtotime($curdate. "+".$days." days"));
			$nearexpdate = date('Y-m-d',strtotime($curdate. "+".$nearexdays." days"));
			$selpackage = $this->db->select('t.*,p.packagename')->from('vs_packages_master p')->join('(
				select pm.packageid, sum(amount) as amount from vs_packages_master pm 
				join vs_packages_detail pd on pd.packageid = pm.packageid where pm.packageid = '.$packageid.' and pd.noofdays = '.$days.' group by pm.packageid ) as t','t.packageid = p.packageid')->get()->row();

			$voucher = $this->simsdb->order_by('voucherno')->get_where('vouchers.vouchers',array('vouchertype'=>'PaidVoucher','userid'=>null,'referenceno'=>null))->row();
			if(!$voucher)
				throw new Exception("No Vouchers available", 1);

			$vouchercode = $voucher->vouchercode;

		# Update vouchers table with vouchervalue, userid, senddatetime and Assign voucher code in case of purchased vouchers
			$voucherupdate = array('voucherfor' => $postdata['usertype'],
				'vouchervalue' => $selpackage->amount,
				'userid' => $this->session->userdata('apiuserid'),
				'mobileno'=>$this->session->userdata('apimobileno'),
				'senddatettime' => $posteddate,
				'usestatus' => 'Y',
				'useddatetime'=>$posteddate,
				'usertype' => ($this->session->userdata('isparent')=='false'?'Student':'Parent'));
			$this->simsdb->update('vouchers.vouchers',$voucherupdate,array('voucherno'=>$voucher->voucherno));

			$voucher = $this->simsdb->get_data('vouchers.vouchers',array('voucherno'=>$voucher->voucherno))->row();

		# Insert to voucheruses master / detail
			$vouchersusesmaster = array("userid" => $this->session->userdata('apiuserid'),
				"voucherno" => $voucher->voucherno,
				"vouchertype" => $voucher->vouchertype,
				"vouchervalue" => $voucher->vouchervalue,
				"numberofdays" => $days,
				"mobilenumber" => $this->session->userdata('apimobileno'),
				"packagename" => $selpackage->packagename,
				'usertype' => ($this->session->userdata('isparent')=='false'?'Student':'Parent'));
			$this->simsdb->insert('vouchers.vouchersusesmaster',$vouchersusesmaster);
			$vouchersusesmasterid = $this->get_insert_id('vouchers.vouchersusesmaster','vouchersusesmasterid');
		// $vouchersusesmasterid =1;



			$sql = "select DISTINCT on (fd.servicetype,pd.classid,pd.subjectid) fd.servicetype,pd.classid,pd.subjectid,c.classname,s.subjectname,pd.packagedetailid,noofdays,amount,fm.featurename from vs_packages_detail pd 
			join vs_features_master fm on fm.featuresid = pd.featuresid
			join vs_features_detail fd on fd.featuresid = pd.featuresid
			join el_class c on c.classid = pd.classid
			join el_subject s on s.subjectid = pd.subjectid
			where fm.featuresid in (".$features.") and pd.noofdays = ".$days." and pd.packageid = ".$packageid;

			$qry = $this->db->query($sql);
			$packagerate = $qry->result();

			$insertedpdid = array();
			foreach ($packagerate as $key => $rate){
				// if(!in_array($rate->packagedetailid, $insertedpdid))
				// {
				// 	array_push($insertedpdid, $rate->packagedetailid);
				// 	$amount = $rate->amount;
				// }
				// else
				// 	$amount = 0;

				if($key==0)
					$amount = $rate->amount;
				else
					$amount = 0;

				$vouchersusesdetail = array("vouchersusesmasterid" => $vouchersusesmasterid,
					"userid" => $this->session->userdata('apiuserid'),
					"voucherno" => $voucher->voucherno,
					"vouchertype" => $voucher->vouchertype,     
					"subjectid" => $rate->subjectid,
					"expirydate" => $expdate,
					"nearexpirydate" => $nearexpdate,
					"classid" => $rate->classid,
					"classname" => $rate->classname,
					"subjectname" => $rate->subjectname,
					"mobilenumber" => $this->session->userdata('apimobileno'),
					"servicetype" => $rate->servicetype,
					"servicename" => $rate->featurename,
					"noofdays" => $rate->noofdays,
					"amount" => $amount,
					"usesdatetime"=>$posteddate,
					'usertype' => ($this->session->userdata('isparent')=='false'?'Student':'Parent'));
			// echo "<pre>";
			// print_r($vouchersusesdetail);
				$this->simsdb->insert('vouchers.vouchersusesdetail',$vouchersusesdetail);

				$el_subcription = array("userid" => $this->session->userdata('apiuserid'),
					"usertype" => $postdata['usertype'],
					"mobileno" => $this->session->userdata('apimobileno'),
					"classid" => $rate->classid,
					"subjectid" =>  $rate->subjectid,
					"subscriptiontype" => $voucher->vouchertype,
					"subscriptiondate" => $posteddate,
					"expirydate" => $expdate,
					"nearexpirydate" => $nearexpdate,
					"couponno" => $voucher->vouchercode,
					"servicetype" => $rate->servicetype,
					"servicename" => $rate->featurename,
					"noofdays" => $days,
					"amount" => $amount,
					"ipaddress" => $_SERVER['REMOTE_ADDR']);
				$this->db->insert('el_user_subscription',$el_subcription);
			}


	   // # Insert to SMS to sent
		// $smsarray = array('createddatetime'=>date('Y-m-d H:i:s.u'),
		//     'createdby'=>'system',
		//     'messagetype'=>'Voucher Code',
		//     'mobilenumber'=>$postdata['mobileno'],
		//     'message'=>'Welcome to MiDas App!'."\n".'Following is the Voucher worth Rs. '.$vouchervalue.'.'."\n".' to activate Study Material of Grade '.$eclass->classname.' on MiDas App.'."\n".'Voucher No: '.$vouchercode."\n".'Thank you.',
		//     'status'=>'1',
		//     'istest' => '1',
		//     'tosenddatetime'=>date('Y-m-d H:i:s.u'));
		// $this->db->insert('smstosent',$smsarray);
		// $html = '<p>We have sent Voucher worth Rs. '.$vouchervalue.' as a text message to your mobile phone ('.$postdata['mobileno'].').</p><p>Please use the Voucher to activate <strong>Interactive Audiovisual Content</strong> for your child.</p>';
		// return $html;

		} catch (Exception $e) {
			throw $e;
		}
	}

	function getPurchasedVoucher($sessiondata)
	{
		try {



			$IParray=array_values(array_filter(explode(',',$_SERVER['HTTP_X_FORWARDED_FOR'])));
			$ipaddress = end($IParray);


			$userid = $sessiondata['userid'];
			if(!is_numeric($userid))
			{
				if($sessiondata['isparent']=='true')
					$query = $this->simsdb->select('userid')->get_where('org_midasuser',array('usercode'=>$userid))->row();
				else
					$query = $this->simsdb->select('userid')->get_where('org_midasstudent',array('studentcode'=>$userid))->row();
				$userid = $query->userid;
			}


			if($sessiondata['isparent']=='true')
			{
				$user = $this->simsdb->get_where('org_midasuser',array('userid'=>$userid))->row();
				if(!$user)
					throw new Exception("Parent user not found.", 1);
				// if(!@$sessiondata['apichildid'])
				// 	throw new Exception("Child not found.", 1);
				if(!is_numeric($sessiondata['childid']))
				{
					
					$query = $this->simsdb->select('userid')->get_where('org_midasstudent',array('studentcode'=>$sessiondata['childid']))->row();
					$sessiondata['childid'] = $query->userid;
				}

				if(@$sessiondata['childid'])
					$this->simsdb->where(array('sc.userid'=>$sessiondata['childid']));
				// $children = $this->simsdb->select('userid,studentfirstname,studentlastname')->join('sch_parentstudentrelation psr','psr.studentid = sc.studentid')->get_where('sch_studentcurrent sc',array('psr.mobileno'=>trim($user->mobilenumber)))->result();
				$children = $this->simsdb->select('ms.userid,studentfirstname,studentlastname,messagecount')->join('org_midasstudent ms','ms.userid = sc.userid')->join('sch_parentstudentrelation psr','psr.studentid = sc.studentid')->limit(1)->get_where('sch_studentcurrent sc',array('psr.mobileno'=>trim($user->mobilenumber)))->result();
				$childarr  = array();
				$childusers = array();
				// if($userid=='1000068080')	
				// {
				// 	print_r($sessiondata);
				// 	echo $this->simsdb->last_query();
				// 	print_r($children);exit;
				// }			
				foreach ($children as $child) {

					$hassubscription = $this->db->get_where('el_user_subscription',array('userid'=>$child->userid))->result();

					$hasvoucher = $this->simsdb->get_where('vouchers.vouchers',array('userid'=>$child->userid))->result();
					if(empty($hassubscription) && empty($hasvoucher))
					{
						$userdata['userid'] = $child->userid;
						$userdata['usertype'] = 'Student';
						$userdata['ipaddress'] = $sessiondata['ipaddress'];
						$userdata['devicetype'] = $sessiondata['devicetype'];
						$this->assignVoucherFixedDays($userdata);	
						// Set Log
						$logarr =  array(
							'midasuserid'=>$child->userid,
							'mobilenumber'=>$user->mobilenumber,
							'orgid'=>null,
							'macaddress'=>null,
							'ipaddress'=>$ipaddress,
							'logmachine'=>@$sessiondata['devicetype'],
							'event'=>'AssignVoucher - '.@$sessiondata['type'],
							'androidappversion'=>null,                
							'logfrom'=>'Api',
							'networktype' => null
							);	
						$this->simsdb->insert('schooluse_log',$logarr);		
					}

					$sql = "SELECT DISTINCT
					ON (noofdays) noofdays,
					'".$child->studentfirstname." ".$child->studentlastname."' as childname,
					array_to_string(array_agg(distinct classname),'')as classname,
					array_to_string(
					ARRAY_AGG (distinct s.subjectname),
					'<br>'
					) AS subjects,
					array_to_string(
					ARRAY_AGG (distinct servicename),
					'<br>'
					) AS features,
					sum(amount) as packageamount,
					subscriptiondate,
					expirydate
					FROM
					el_user_subscription AS us
					join el_class c on c.classid = us.classid
					JOIN el_subject s ON s.subjectid = us.subjectid
					WHERE
					userid = ".$child->userid."
					GROUP BY
					noofdays,subscriptiondate,expirydate";
					$qry = $this->db->query($sql);
					$result[] = $qry->result();    
					array_push($childusers, $child->userid);  
					if(!empty($childusers))
					{
						$messagecount = $child->messagecount-1;
						if($messagecount<0)
							$messagecount = 0;
						$updatearr['messagecount'] = $messagecount;

						if($messagecount<=0)
							$updatearr['loginmessage'] = 'N';

						$this->simsdb->update('org_midasstudent',$updatearr,'userid in ('.implode(',', $childusers).')');
					}          
				}   
				$children = $this->simsdb->select('ms.userid,studentfirstname,studentlastname,messagecount')->join('org_midasstudent ms','ms.userid = sc.userid')->join('sch_parentstudentrelation psr','psr.studentid = sc.studentid')->where('ms.messagecount>0')->limit(1)->get_where('sch_studentcurrent sc',array('psr.mobileno'=>trim($user->mobilenumber)))->result(); 
				if(empty($children))
				{
					$this->simsdb->update('org_midasuser',array('loginmessage'=>'N'),array('userid'=>$userid));
				}



			}
			else
			{
				// echo 'userid '.$userid;
				// if(@$sessiondata['istest']=='TRUE')
				// {
				// 	print_r($sessiondata);
				// 	exit;
				// }

				if(!$userid)
					throw new Exception("User not found", 1);

				$hassubscription = $this->db->select('subscriptionid',false)->get_where('el_user_subscription',array('userid'=>$userid))->row();

				

				
				$hasvoucher = $this->simsdb->select('voucherno',false)->get_where('vouchers.vouchers',array('userid'=>$userid))->row();

				

				if(empty($hassubscription) && empty($hasvoucher))
				{
					$userdata['userid'] = $userid;
					$userdata['usertype'] = 'Student';
					$userdata['ipaddress'] = $sessiondata['ipaddress'];
					$userdata['devicetype'] = $sessiondata['devicetype'];
					$userdata['usedatetime'] = @$sessiondata['usedatetime'];
					$d=$this->assignVoucherFixedDays($userdata);
					var_dump($d);exit;

					// Set Log
					$logarr =  array(
						'midasuserid'=>$userid,
						'mobilenumber'=>$this->session->userdata('apimobileno'),
						'orgid'=>null,
						'macaddress'=>null,
						'ipaddress'=>$ipaddress,
						'logmachine'=>@$sessiondata['devicetype'],
						'event'=>'AssignVoucher - '.@$sessiondata['type'],
						'androidappversion'=>null,                
						'logfrom'=>'Api',
						'networktype' => null
						);	
					$this->simsdb->insert('schooluse_log',$logarr);						
				}

				$sql = "SELECT DISTINCT
				ON (noofdays) noofdays,
				'' as childname,
				array_to_string(array_agg(distinct classname),'')as classname,
				array_to_string(
				ARRAY_AGG (distinct s.subjectname),
				'<br>'
				) AS subjects,
				array_to_string(
				ARRAY_AGG (distinct servicename),
				'<br>'
				) AS features,
				sum(amount) as packageamount,
				subscriptiondate,
				expirydate
				FROM
				el_user_subscription AS us
				join el_class c on c.classid = us.classid
				JOIN el_subject s ON s.subjectid = us.subjectid
				WHERE
				userid = ".$userid."
				GROUP BY
				noofdays,subscriptiondate,expirydate";
				$qry = $this->db->query($sql);
				$result[] = $qry->result();
				
				if($userid!='11000000477')	
				{
					$student = $this->simsdb->get_where('org_midasstudent',array('userid'=>$userid))->row();
					$messagecount = $student->messagecount-1;
					if($messagecount<0)
						$messagecount = 0;

					$updatearr['messagecount'] = $messagecount;

					if($messagecount<=0)
						$updatearr['loginmessage'] = 'N';	

					$this->simsdb->update('org_midasstudent',$updatearr,array('userid'=>$userid));
				}

			}
			foreach ($result as $key => $value) {
				if(empty($value))
					unset($result[$key]);
			}       
			return $result;
		} catch (Exception $e) {
			throw $e;
		}
	}

	function assignVoucherFixedAmount($postdata)
	{
		try {
			$userid = $postdata['userid'];
			$student = $this->simsdb->select('oc."CLASSTAG"')->join('class c','c.classid = sc.classid')->join('org_class oc','oc.classid = c.gclassid')->get_where('sch_studentcurrent sc',array('userid'=>$userid))->row();
			if(!$student)
				throw new Exception("student not found", 1);

			$eclass = $this->db->get_where('el_class',array('CLASSTAG'=>$student->CLASSTAG))->row();
			$sql = "SELECT DISTINCT
			noofdays,
			sum(amount) as amount,
			pm.packageid
			FROM
			vs_packages_master pm
			JOIN vs_packages_detail pd ON pm.packageid = pd.packageid
			WHERE
			pd.classid = ".$eclass->classid."
			AND pm.packagetype = 'Full' GROUP BY noofdays,pm.packageid";
			$qry = $this->db->query($sql);
			$package = $qry->row();
			// echo $this->db->last_query();
			// print_r($package);
			// exit;
			if($package->amount>$eclass->voucheramount)
			{
				$packagedays = round(($package->noofdays*$eclass->voucheramount)/$package->amount);
			}
			else
				$packagedays = $package->noofdays;

			$packageid = $package->packageid;
			$days = $packagedays;
			
			$nearexdays = $days-5;
			$curdate     = date('Y-m-d');
			$posteddate = date('Y-m-d H:i:s.u');
			$expdate = date('Y-m-d',strtotime($curdate. "+".$days." days"));
			$nearexpdate = date('Y-m-d',strtotime($curdate. "+".$nearexdays." days"));

			$selpackage = $this->db->select('t.*,p.packagename')->from('vs_packages_master p')->join('(
				select pm.packageid, sum(amount) as amount from vs_packages_master pm 
				join vs_packages_detail pd on pd.packageid = pm.packageid where pm.packageid = '.$packageid.' and pd.noofdays = '.$package->noofdays.' group by pm.packageid ) as t','t.packageid = p.packageid')->get()->row();
			// echo $this->db->last_query();
			// print_r($selpackage);exit;
			$voucher = $this->simsdb->order_by('voucherno')->get_where('vouchers.vouchers',array('vouchertype'=>'SignUpVoucher','userid'=>null,'referenceno'=>null))->row();
			
			if(!$voucher)
				throw new Exception("No Vouchers available", 1);

			$vouchercode = $voucher->vouchercode;

		# Update vouchers table with vouchervalue, userid, senddatetime and Assign voucher code in case of purchased vouchers
			$voucherupdate = array('voucherfor' => $postdata['usertype'],
				'vouchervalue' => $eclass->voucheramount,
				'userid' => $userid,
				'senddatettime' => $posteddate,
				'usestatus' => 'Y',
				'useddatetime'=>$posteddate,
				'usertype' => ($this->session->userdata('isparent')=='false'?'Student':'Parent'));
			$this->simsdb->update('vouchers.vouchers',$voucherupdate,array('voucherno'=>$voucher->voucherno));

			$voucher = $this->simsdb->get_where('vouchers.vouchers',array('voucherno'=>$voucher->voucherno))->row();

		# Insert to voucheruses master / detail
			$vouchersusesmaster = array("userid" => $userid,
				"voucherno" => $voucher->voucherno,
				"vouchertype" => $voucher->vouchertype,
				"vouchervalue" => $voucher->vouchervalue,
				"numberofdays" => $days,
				"mobilenumber" => '',
				"packagename" => $selpackage->packagename,
				'usertype' => ($this->session->userdata('isparent')=='false'?'Student':'Parent'));
			$this->simsdb->insert('vouchers.vouchersusesmaster',$vouchersusesmaster);
			$vouchersusesmasterid = $this->get_insert_id('vouchers.vouchersusesmaster','vouchersusesmasterid');
		// $vouchersusesmasterid =1;



			$sql = "select DISTINCT on (fd.servicetype,pd.classid,pd.subjectid) fd.servicetype,pd.classid,pd.subjectid,c.classname,s.subjectname,pd.packagedetailid,noofdays,amount,fm.featurename from vs_packages_detail pd 
			join vs_features_master fm on fm.featuresid = pd.featuresid
			join vs_features_detail fd on fd.featuresid = pd.featuresid
			join el_class c on c.classid = pd.classid
			join el_subject s on s.subjectid = pd.subjectid
			where pd.noofdays = ".$package->noofdays." and pd.packageid = ".$packageid;

			$qry = $this->db->query($sql);
			$packagerate = $qry->result();
// 			echo $this->db->last_query();
// print_r($packagerate);
// exit;
			$insertedpdid = array();
			foreach ($packagerate as $key => $rate){
				// if(!in_array($rate->packagedetailid, $insertedpdid))
				// {
				// 	array_push($insertedpdid, $rate->packagedetailid);
				// 	$amount = $rate->amount;
				// }
				// else
				// 	$amount = 0;

				if($key==0)
					// $amount = $rate->amount;
					$amount = $eclass->voucheramount;
				else
					$amount = 0;

				$vouchersusesdetail = array("vouchersusesmasterid" => $vouchersusesmasterid,
					"userid" => $userid,
					"voucherno" => $voucher->voucherno,
					"vouchertype" => $voucher->vouchertype,     
					"subjectid" => $rate->subjectid,
					"expirydate" => $expdate,
					"nearexpirydate" => $nearexpdate,
					"classid" => $rate->classid,
					"classname" => $rate->classname,
					"subjectname" => $rate->subjectname,
					"mobilenumber" => $this->session->userdata('apimobileno'),
					"servicetype" => $rate->servicetype,
					"servicename" => $rate->featurename,
					"noofdays" => $rate->noofdays,
					"amount" => $amount,
					"usesdatetime"=>$posteddate,
					'usertype' => ($this->session->userdata('isparent')=='false'?'Student':'Parent'));
			// echo "<pre>";
			// print_r($vouchersusesdetail);
				$this->simsdb->insert('vouchers.vouchersusesdetail',$vouchersusesdetail);

				$el_subcription = array("userid" => $userid,
					"usertype" => $postdata['usertype'],
					"mobileno" => $this->session->userdata('apimobileno'),
					"classid" => $rate->classid,
					"subjectid" =>  $rate->subjectid,
					"subscriptiontype" => $voucher->vouchertype,
					"subscriptiondate" => $posteddate,
					"expirydate" => $expdate,
					"nearexpirydate" => $nearexpdate,
					"couponno" => $voucher->vouchercode,
					"servicetype" => $rate->servicetype,
					"servicename" => $rate->featurename,
					"noofdays" => $days,
					"amount" => $amount,
					"ipaddress" => $_SERVER['REMOTE_ADDR']);
				$this->db->insert('el_user_subscription',$el_subcription);
			}			
		} catch (Exception $e) {
			throw $e;
			
		}
	}

	function assignVoucherFixedDays($postdata)
	{
		try {
			$userid = $postdata['userid'];
			$student = $this->simsdb->select('oc."CLASSTAG"')->join('org_class oc','oc.classid = sc.gclassid')->get_where('sch_studentcurrent sc',array('userid'=>$userid))->row();
                    

			// echo $this->simsdb->last_query();
			$mobileno = $this->session->userdata('apimobileno');

			if(!$student)
				throw new Exception("student not found", 1);

			$eclass = $this->db->select('classid,voucherdays,classname')->get_where('el_class',array('CLASSTAG'=>$student->CLASSTAG))->row();

			
			// if($_GET['istest'])
			// {
			// 	print_r($eclass);
			// 	exit;
			// }
			// Assign live class session
			// $this->assignLiveClass($eclass->classid,$userid);


			$sql = "SELECT
			pr.noofdays,
			pr.amount,
			pm.packageid,
			pr.pkgcode
			FROM
			vs_packages_master pm
			JOIN vs_packages_detail pd ON pm.packageid = pd.packageid
			join vs_packages_rate pr on pr.packageid = pm.packageid
			WHERE
			pd.classid = ".$eclass->classid." and pr.status = 1
			AND pm.packagetype = 'Full' and pm.packagecategory = 'General' GROUP BY pr.noofdays,pr.amount,pm.packageid,pr.pkgcode order by pr.noofdays desc limit 1";
			$qry = $this->db->query($sql);
			$package = $qry->row();
			//var_dump($this->db->last_query());exit;
			if(@$postdata['istest']=='TRUE')
			{
				echo "here";
				echo $this->db->last_query();
				exit;
			}

			if(!$package)
				throw new Exception("No Subscription Available.", 1);

			// if($postdata['userid']=='11000209056')
			// {
			// 	echo $this->db->last_query();
			// 	print_r($package);
			// 	exit;
			// }
			if($package->noofdays>$eclass->voucherdays)
			{
				$packageamount = round(($package->amount*$eclass->voucherdays)/$package->noofdays);
			}
			else
				$packageamount = $package->amount;

			$packageid = $package->packageid;
			$days = $eclass->voucherdays;

			$nearexdays = $days-5;
			$curdate     = date('Y-m-d');
			$posteddate = @$postdata['usedatetime']?$postdata['usedatetime']:date('Y-m-d H:i:s.u');
			$expdate = date('Y-m-d',strtotime($curdate. "+".$days." days"));
			$nearexpdate = date('Y-m-d',strtotime($curdate. "+".$nearexdays." days"));

			// $expdate = '2019-04-13';
			// $nearexpdate = '2019-04-10';

			$selpackage = $this->db->select('t.*,p.packagename')->from('vs_packages_master p')->join('(SELECT pm.packageid, amount FROM vs_packages_master pm join vs_packages_rate pr on pr.packageid = pm.packageid WHERE pm.packageid = '.$packageid.' and pr.noofdays = '.$package->noofdays.') as t','t.packageid = p.packageid')->get()->row();

			// echo $this->db->last_query();
			// print_r($selpackage);exit;

			$voucher = $this->simsdb->order_by('voucherno')->limit(1)->get_where('vouchers.vouchers',array('vouchertype'=>'SignUpVoucher','userid'=>null,'referenceno'=>null))->row();


			if(!$voucher)
				throw new Exception("No Vouchers available", 1);


			$vouchercode = $voucher->vouchercode;

		# Update vouchers table with vouchervalue, userid, senddatetime and Assign voucher code in case of purchased vouchers
			$voucherupdate = array('voucherfor' => $postdata['usertype'],
				'vouchervalue' => $packageamount,
				'userid' => $userid,
				'senddatettime' => $posteddate,
				'usestatus' => 'Y',
				'useddatetime'=>$posteddate,
				'devicetype' => @$postdata['devicetype'],
				'ipaddress' => @$postdata['ipaddress'],
				'usertype' => ($this->session->userdata('isparent')=='false'?'Student':'Parent'));
			$this->simsdb->update('vouchers.vouchers',$voucherupdate,array('voucherno'=>$voucher->voucherno));

			$voucher = $this->simsdb->get_where('vouchers.vouchers',array('voucherno'=>$voucher->voucherno))->row();

		# Insert to voucheruses master / detail
			$vouchersusesmaster = array("userid" => $userid,
				"voucherno" => $voucher->voucherno,
				"vouchertype" => $voucher->vouchertype,
				"vouchervalue" => $voucher->vouchervalue,
				"numberofdays" => $days,
				"mobilenumber" => $mobileno,
				"packagename" => $selpackage->packagename,
				'devicetype' => @$postdata['devicetype'],
				'ipaddress' => @$postdata['ipaddress'],
				'usertype' => ($this->session->userdata('isparent')=='false'?'Student':'Parent'));

			if(@$postdata['usedatetime'])
				$vouchersusesmaster['dataposteddatetime'] = @$postdata['usedatetime'];

			$this->simsdb->insert('vouchers.vouchersusesmaster',$vouchersusesmaster);
			$vouchersusesmasterid = $this->get_insert_id('vouchers.vouchersusesmaster','vouchersusesmasterid');
		// $vouchersusesmasterid =1;



			$sql = "select DISTINCT on (fd.servicetype,pd.classid,pd.subjectid) fd.servicetype,pd.classid,pd.subjectid,c.classname,s.subjectname,pd.packagedetailid,pr.noofdays,pr.amount,fm.featurename from vs_packages_detail pd 
			join vs_packages_rate pr on pr.packageid = pd.packageid
			join vs_features_master fm on fm.featuresid = pd.featuresid
			join vs_features_detail fd on fd.featuresid = pd.featuresid
			join el_class c on c.classid = pd.classid
			join el_subject s on s.subjectid = pd.subjectid
			where pr.noofdays = ".$package->noofdays." and pd.packageid = ".$packageid;

			$qry = $this->db->query($sql);
			$packagerate = $qry->result();
// 			echo $this->db->last_query();

			$insertedpdid = array();
			foreach ($packagerate as $key => $rate){
				// if(!in_array($rate->packagedetailid, $insertedpdid))
				// {
				// 	array_push($insertedpdid, $rate->packagedetailid);
				// 	$amount = $rate->amount;
				// }
				// else
				// 	$amount = 0;

				if($key==0)
					// $amount = $rate->amount;
					$amount = $packageamount;
				else
					$amount = 0;

				$vouchersusesdetail[] = array("vouchersusesmasterid" => $vouchersusesmasterid,
					"userid" => $userid,
					"voucherno" => $voucher->voucherno,
					"vouchertype" => $voucher->vouchertype,     
					"subjectid" => $rate->subjectid,
					"expirydate" => $expdate,
					"nearexpirydate" => $nearexpdate,
					"classid" => $rate->classid,
					"classname" => $rate->classname,
					"subjectname" => $rate->subjectname,
					"mobilenumber" => $this->session->userdata('apimobileno'),
					"servicetype" => $rate->servicetype,
					"servicename" => $rate->featurename,
					"noofdays" => $days,
					"amount" => $amount,
					"usesdatetime"=>$posteddate,
					'devicetype' => @$postdata['devicetype'],
					'ipaddress' => @$postdata['ipaddress'],
					'usertype' => ($this->session->userdata('isparent')=='false'?'Student':'Parent'));
			// echo "<pre>";
			// print_r($vouchersusesdetail);

				$el_subcription[] = array("userid" => $userid,
					"usertype" => $postdata['usertype'],
					"mobileno" => $this->session->userdata('apimobileno'),
					"classid" => $rate->classid,
					"subjectid" =>  $rate->subjectid,
					"subscriptiontype" => $voucher->vouchertype,
					"subscriptiondate" => $posteddate,
					"expirydate" => $expdate,
					"nearexpirydate" => $nearexpdate,
					"couponno" => $voucher->vouchercode,
					"servicetype" => $rate->servicetype,
					"servicename" => $rate->featurename,
					"noofdays" => $days,
					"amount" => $amount,
					"ipaddress" => $_SERVER['REMOTE_ADDR'],
					'devicetype' => @$postdata['devicetype']);				
			}	

			$this->simsdb->insert_batch('vouchers.vouchersusesdetail',$vouchersusesdetail);

			$this->db->insert_batch('el_user_subscription',$el_subcription);		
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}

	function assignLiveClass($classid,$userid)
	{
		try{
			$livesessions = $this->db->select('DISTINCT on (classid,subjectid) classid,subjectid,livesessionid',false)->order_by('classid,subjectid,livesessionid DESC')->get_where('ml_live_sessions',array('classid'=>$classid))->result();
			foreach ($livesessions as $key => $ls) {
				$sessionusers[] = array('userid'=>$userid,'livesessionid'=>$ls->livesessionid,'usertype'=>'STUDENT');
			}
			if($sessionusers)
				$this->db->insert_batch('ml_livesession_users',$sessionusers);
		}
		catch(Exception $e)
		{
			echo $e->getMessage();
		}
	}

	function assignVoucherFixedDays_old($postdata)
	{
		try {
			$userid = $postdata['userid'];
			$student = $this->simsdb->select('oc."CLASSTAG"')->join('class c','c.classid = sc.classid')->join('org_class oc','oc.classid = c.gclassid')->get_where('sch_studentcurrent sc',array('userid'=>$userid))->row();

			$mobileno = $this->session->userdata('apimobileno');

			if(!$student)
				throw new Exception("student not found", 1);

			$eclass = $this->db->get_where('el_class',array('CLASSTAG'=>$student->CLASSTAG))->row();
			$sql = "SELECT DISTINCT
			noofdays,
			sum(amount) as amount,
			pm.packageid
			FROM
			vs_packages_master pm
			JOIN vs_packages_detail pd ON pm.packageid = pd.packageid
			WHERE
			pd.classid = ".$eclass->classid."
			AND pm.packagetype = 'Full' GROUP BY noofdays,pm.packageid";
			$qry = $this->db->query($sql);
			$package = $qry->row();
			// echo $this->db->last_query();
			// print_r($package);
			// exit;
			if($package->noofdays>$eclass->voucherdays)
			{
				$packageamount = round(($package->amount*$eclass->voucherdays)/$package->noofdays);
			}
			else
				$packageamount = $package->amount;

			$packageid = $package->packageid;
			$days = $eclass->voucherdays;

			$nearexdays = $days-5;
			$curdate     = date('Y-m-d');
			$posteddate = date('Y-m-d H:i:s.u');
			$expdate = date('Y-m-d',strtotime($curdate. "+".$days." days"));
			$nearexpdate = date('Y-m-d',strtotime($curdate. "+".$nearexdays." days"));

			$selpackage = $this->db->select('t.*,p.packagename')->from('vs_packages_master p')->join('(
				select pm.packageid, sum(amount) as amount from vs_packages_master pm 
				join vs_packages_detail pd on pd.packageid = pm.packageid where pm.packageid = '.$packageid.' and pd.noofdays = '.$package->noofdays.' group by pm.packageid ) as t','t.packageid = p.packageid')->get()->row();
			// echo $this->db->last_query();
			// print_r($selpackage);exit;

			$voucher = $this->simsdb->order_by('voucherno')->limit(1)->get_where('vouchers.vouchers',array('vouchertype'=>'SignUpVoucher','userid'=>null,'referenceno'=>null))->row();

			if(!$voucher)
				throw new Exception("No Vouchers available", 1);


			$vouchercode = $voucher->vouchercode;

		# Update vouchers table with vouchervalue, userid, senddatetime and Assign voucher code in case of purchased vouchers
			$voucherupdate = array('voucherfor' => $postdata['usertype'],
				'vouchervalue' => $packageamount,
				'userid' => $userid,
				'senddatettime' => $posteddate,
				'usestatus' => 'Y',
				'useddatetime'=>$posteddate);
			$this->simsdb->update('vouchers.vouchers',$voucherupdate,array('voucherno'=>$voucher->voucherno));

			$voucher = $this->simsdb->get_where('vouchers.vouchers',array('voucherno'=>$voucher->voucherno))->row();

		# Insert to voucheruses master / detail
			$vouchersusesmaster = array("userid" => $userid,
				"voucherno" => $voucher->voucherno,
				"vouchertype" => $voucher->vouchertype,
				"vouchervalue" => $voucher->vouchervalue,
				"numberofdays" => $days,
				"mobilenumber" => $mobileno,
				"packagename" => $selpackage->packagename);
			$this->simsdb->insert('vouchers.vouchersusesmaster',$vouchersusesmaster);
			$vouchersusesmasterid = $this->get_insert_id('vouchers.vouchersusesmaster','vouchersusesmasterid');
		// $vouchersusesmasterid =1;



			$sql = "select DISTINCT on (fd.servicetype,pd.classid,pd.subjectid) fd.servicetype,pd.classid,pd.subjectid,c.classname,s.subjectname,pd.packagedetailid,noofdays,amount,fm.featurename from vs_packages_detail pd 
			join vs_features_master fm on fm.featuresid = pd.featuresid
			join vs_features_detail fd on fd.featuresid = pd.featuresid
			join el_class c on c.classid = pd.classid
			join el_subject s on s.subjectid = pd.subjectid
			where pd.noofdays = ".$package->noofdays." and pd.packageid = ".$packageid;

			$qry = $this->db->query($sql);
			$packagerate = $qry->result();
// 			echo $this->db->last_query();

			$insertedpdid = array();
			foreach ($packagerate as $key => $rate){
				// if(!in_array($rate->packagedetailid, $insertedpdid))
				// {
				// 	array_push($insertedpdid, $rate->packagedetailid);
				// 	$amount = $rate->amount;
				// }
				// else
				// 	$amount = 0;

				if($key==0)
					// $amount = $rate->amount;
					$amount = $packageamount;
				else
					$amount = 0;

				$vouchersusesdetail[] = array("vouchersusesmasterid" => $vouchersusesmasterid,
					"userid" => $userid,
					"voucherno" => $voucher->voucherno,
					"vouchertype" => $voucher->vouchertype,     
					"subjectid" => $rate->subjectid,
					"expirydate" => $expdate,
					"nearexpirydate" => $nearexpdate,
					"classid" => $rate->classid,
					"classname" => $rate->classname,
					"subjectname" => $rate->subjectname,
					"mobilenumber" => $this->session->userdata('apimobileno'),
					"servicetype" => $rate->servicetype,
					"servicename" => $rate->featurename,
					"noofdays" => $days,
					"amount" => $amount,
					"usesdatetime"=>$posteddate);
			// echo "<pre>";
			// print_r($vouchersusesdetail);

				$el_subcription[] = array("userid" => $userid,
					"usertype" => $postdata['usertype'],
					"mobileno" => $this->session->userdata('apimobileno'),
					"classid" => $rate->classid,
					"subjectid" =>  $rate->subjectid,
					"subscriptiontype" => $voucher->vouchertype,
					"subscriptiondate" => $posteddate,
					"expirydate" => $expdate,
					"nearexpirydate" => $nearexpdate,
					"couponno" => $voucher->vouchercode,
					"servicetype" => $rate->servicetype,
					"servicename" => $rate->featurename,
					"noofdays" => $days,
					"amount" => $amount,
					"ipaddress" => $_SERVER['REMOTE_ADDR']);				
			}	

			$this->simsdb->insert_batch('vouchers.vouchersusesdetail',$vouchersusesdetail);

			$this->db->insert_batch('el_user_subscription',$el_subcription);		
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}

	function get_insert_id($table,$field)
	{ 
		$rec = $this->simsdb->select('max('.$field.') as insertid',false)->from($table)->get()->row();
		return $rec->insertid;
	}

	function gethbltransaction($data)
	{
		try{
			// print_r($data);
			// exit;
			$hbl_merchantid = '000009101430064';
			$hbl_secret = 'PGG8GTGCQKUIFX45WWI8W6CPF6A866N7';
			$hbl_currencycode = '524';
			$hbl_nonsecure = 'Y';
			$hbl_paymentgatewayid = '9101430064';

			$transaction = $this->simsdb->get_where('vouchers.transactions',array('txncode'=>$data['txncode']))->row();
			if(!$transaction)
				throw new Exception("Invalid Request", 1);

			$hblamount = substr('000000000'.$transaction->amount.'00',-12);
			// $hblamount = substr('000000000'.'1'.'00',-12);
			$hblinvoiceno = $data['txncode'];
			$hblhash = $this->generatehblhash($hbl_merchantid.$hblinvoiceno.$hblamount.$hbl_currencycode.$hbl_nonsecure);
			$response = array("paymentgatewayid"=>$hbl_paymentgatewayid,
				'hblamount'=>$hblamount,
				'hblcurrency'=>$hbl_currencycode,
				'hblnonsecure'=>$hbl_nonsecure,
				'hblhash'=>$hblhash,
				'machinetype'=>'Android',
				'invoiceno'=>$data['txncode'],
				'packagename' => @$transaction->pkgname,
				'userdefined2'=>'Android');					
			return $response;
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}

	function generatehblhash($signatureString)
	{
		$hbl_secret = 'PGG8GTGCQKUIFX45WWI8W6CPF6A866N7';
		$signData = hash_hmac('SHA256', $signatureString,$hbl_secret, false);
		$signData = strtoupper($signData);
		return urlencode($signData);
	}

	function unsubscribePackage(){
		$post = $this->input->post();

		$updateArray = array('unsubscribeddate'=>date('Y-m-d H:i:s'),'unsubscribed'=>'t');
		$ncelltransactions = $this->simsdb->get_where('vouchers.ncelltransactions',array('userid'=>$post['userid'],'vouchercode'=>$post['couponno']))->row();
		if($ncelltransactions->source=='NCELLHE')
		{
			
			// $msisdn = substr($ncelltransactions->mobilenumber, 0,2).str_repeat("*", 6) . substr($ncelltransactions->mobilenumber, -2);
			$msisdn = $row->mobilenumber;			
			$callbackurl = "http://45.114.143.164/adpoke/cnt/midas/dct?msisdn=".$msisdn;
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $callbackurl);
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($ch);
			$this->simsdb->insert('test_log_tbl',['text'=>json_encode($response),'type'=>'NCELLHE unsubscribe']);
			$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			curl_close($ch);
			if($status_code!=200)
			{
                          // echo $callbackurl;
                          // print_r($response);
                          // exit;
			}
		}
		$elusersub = $this->simsdb->where(array('userid'=>$post['userid'],'vouchercode'=>$post['couponno']))->update('vouchers.ncelltransactions',$updateArray);
		
		$result = $this->db->where(array('userid'=>$post['userid'],'couponno'=>$post['couponno']))->update('el_user_subscription',array('unsubscribeddate'=>date('Y-m-d H:i:s')));
		return $result;
		
	}

}