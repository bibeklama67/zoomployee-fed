<style>
	table th, td{
		border:1px solid #ddd;
		font-size: 15px;
	}
	table th{
		color: 
	}
	tr.header{
		background: #eaeef1 !important;
		color: #697b8f;
	}
	table td{
		border:1px solid #ddd;
		font-size: 15px;
		background: #fff;
	}
</style>
<div style="padding: 10px;">
    <div class="table-responsive" style="margin-top: 10px; ">
    <?php if(!empty($result['dataArray'])){ ?>
        <table class="table" style="border:1px solid #ddd">
            <tbody>
                <tr class="header">
                    <th>S. No</th>
                    <th>Activate Code</th>
                    <th>Courses</th>
                    <th>Voucher Type</th>
                    <th>Status</th>
                    <th>Amount</th>
                    <th>Total Balance</th>
                </tr>
                <?php  
                $i = 1; $j = 1; $totalval=0;
                foreach($result['dataArray'] as $key=>$row){ ?>
                <tr>
                    <td><?php echo $i++; ?></td>
                    <td><?php echo $key; ?></td>
                    <td>
                        <b><?php echo $row['packagename']; ?></b> <br />
                        <?php foreach($row['servicetype'] as $rows){
                                echo $j.' : '.$rows.'<br />';
                                $j++;
                            } ?>
                    </td>
                    <td><?php echo $row['vouchertype'];?></td>
                    <td><?php echo $row['usestatus'];
                        if($row['usestatus']=='Y')
                            echo '('.$row['useddatetime'].' to '.$row['expirydate'].')';

                        ?></td>
                    <td><?php echo 'NRS, '.number_format($row['vouchervalue'], 2); ?></td>
                    <td><?php $totalval += $row['vouchervalue'];
                        echo 'NRS, '.number_format($totalval, 2); ?></td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    <?php
        } else { 
            echo '<p>Record not found.</p>';
        }  ?>       
    </div>
</div>
