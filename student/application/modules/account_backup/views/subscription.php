<h4 class="text-center">This feature will be available soon.</h4>
<div>
	<?php 	
	$i=-1;
	foreach($packages as $packageid=>$package){ ?>
	<form method="post">

		<input type="hidden" name="packageid" value="<?= $packageid?>">
		<div class="col-xs-12 col-md-4 packagewrapper">
			<h4><?= $package['packagename']?></h4>
			<?php
			foreach($package as $key=>$features){
				$packagetotal[$key] = 0;

				if($key=='packagename')
					continue;
				$i++;
				?>
				<div class="featurewrapper" id="<?=$packageid.'_'.$key?>" <?= ($i>0?'style="display:none"':'')?>>	
					<?php
					foreach ($features as $id => $row) {
						?>

						<p><label><input type="checkbox" name="features_<?= $packageid?>_<?= $key?>[]" class="features" data-pid="<?= $packageid?>" data-day="<?= $key?>" value="<?= $id?>" data-amt="<?= $row['amount']?>" checked> <?= $row['featurename']?></label></p>			
						<?php
						$packagetotal[$key] +=$row['amount'];
					}
					?>
				</div>			
				<?php }?>
				<div class="footer">
					<select class="days" data-pid="<?= $packageid?>" name="days">
						<?php foreach($days[$packageid] as $day){ 
							if($day%32==0)
							{
								$month = $day/32;
								$duration = $month.' Month'.($month>1?'s':'');
								if($month>=12 && $month%12==0)
								{
									$year = $month/12;
									$duration = $year.' Year'.($year>1?'s':'');
								}
							}
							else
								$duration = $day .' Day'.($day>1?'s':'');
							?>
							<option value="<?= $day?>"><?= $duration?></option>
							<?php }?>
						</select>
						<?php foreach($days[$packageid] as $k=>$day){ ?>
						<div class="packagetotal" id="d<?= $packageid.'_'.$day?>" <?= ($k>0?'style="display:none"':'')?>><?= $packagetotal[$day]?></div>
						<?php }?>
						<button class="btn btn-success subscribe-btn">Subscribe</button>
					</div>
				</div>
			</form>

			<?php
		}?>
	</div>
	<script type="text/javascript">
		var currentday = 0;
		$('.days').on('change',function(){
			currentday = $(this).val();
			var selector = $(this).closest('div.packagewrapper');
			var pid = $(this).data('pid');
			selector.find('.featurewrapper').hide();
			selector.find('.packagetotal').hide();
			selector.find('#'+pid+'_'+$(this).val()).show();
			selector.find('#d'+pid+'_'+$(this).val()).show();
		});

		var currentpid = 0;
		$('.features').on('click',function(){
			var pid = $(this).data('pid');
			var day = $(this).data('day');
			var selector = $(this).closest('div.packagewrapper');
			var featuretotal = 0;

			currentpid = pid;

			$('input[name=features_'+pid+'_'+day+']').each(function () {
				if(this.checked)
					featuretotal += $(this).data('amt');
			});
			selector.find('#d'+pid+'_'+day).text(featuretotal);

		});

		$('.subscribe-btn').on('click',function(){
			var data = $(this).closest('form').serialize();
			$.ajax({
				type:"POST",
				url: base_url+"account/api/purchase",
				data: data, 
				success:function(result) {
					console.log(result);		    
				}
			});
// console.log($('#subscribtion-form').serialize());
return false;
});
</script>