<?php 
if(!defined('BASEPATH')) exit('direct access invalid');

class Account_model extends CI_Model{
	
    function __construct() {
        parent::__construct();
        $this->simsdb =  $this->load->database('sims',TRUE);
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);        
    }

    /**
     * Return account detals 
     * on parent
     * @return type
     */
    function getParentAccount($authcode){
        try{
            if(empty($authcode))
                throw new Exception("This feature will be available soon.", 1);

            // $userid = $this->getUserid($authcode); //Final data
            $userid   =  1000136879; //Testing data

            $this->simsdb->select('v.usestatus, v.vouchercode, v.useddatetime, vm.vouchervalue, vm.packagename,
                vd.vouchertype, vd.expirydate, vd.servicetype')
            ->from('vouchers.vouchers as v')
            ->join('vouchers.vouchersusesmaster as vm', 'vm.voucherno=v.voucherno')
            ->join('vouchers.vouchersusesdetail as vd', 'vm.vouchersusesmasterid=vd.vouchersusesmasterid')
            ->where('v.userid', $userid)
            ->order_by('vm.vouchersusesmasterid', 'desc');

            $result = $this->simsdb->get()->result();
            if($result){
                $dataArray  = array();
                $total      = 0; 
                foreach($result as $row){
                    $dataArray[$row->vouchercode]['packagename']    = $row->packagename;
                    $dataArray[$row->vouchercode]['vouchervalue']   = $row->vouchervalue;
                    $dataArray[$row->vouchercode]['usestatus']      = $row->usestatus;
                    $dataArray[$row->vouchercode]['useddatetime']   = $row->useddatetime;
                    $dataArray[$row->vouchercode]['expirydate']     = $row->expirydate;
                    $dataArray[$row->vouchercode]['vouchertype']    = $row->vouchertype;
                    $dataArray[$row->vouchercode]['servicetype'][]  = $row->servicetype;
                }

                foreach($dataArray as $key=>$rowt){
                    $total += $rowt['vouchervalue'];
                }

                return array(
                    'total'=>$total, 
                    'dataArray'=>$dataArray
                    );
            } return array();

        } catch(Exception $e){
            throw $e;
        }
    }

    /**
     * Get uesrid by authcoe
     * @param type $authcode 
     * @return type
     */
    function getUserid($authcode){
        try{
            if(empty($authcode))
                throw new Exception("This feature will be available soon.", 1);

            $this->simsdb->select('userid')
            ->from('org_midasuser')
            ->where('authcode', $authcode);

            $result  = $this->simsdb->get()->row();
            if($result)
                return $result->userid;
            else 
                throw new Exception("This feature will be available soon.", 1);

        } catch(Exception $e) {
            throw $e;
        }
    }

    function setSession($sessiondata)
    {
        try {
         $user = $this->simsdb->get_where('org_midasstudent',array('authcode'=>$sessiondata['authcode']))->row();
         if(!$user)
            throw new Exception("This feature is currently unavailable.", 1);   
        $this->session->set_userdata('apiuserid', $user->userid);
        $this->session->set_userdata('apimobileno', $sessiondata['mobileno']);
        $this->session->set_userdata('apiclassid', $sessiondata['classid']);
    } catch (Exception $e) {
        throw $e;
    }        
}
    /**
     * Get Package Details     
     * @return type
     */
    function getPackages()
    {
       $classid = $this->session->userdata('apiclassid');
       $sql = "select pm.packageid,pm.packagename,pd.noofdays,pd.amount,pd.featuresid,fm.featurename from vs_packages_master pm join(
       select distinct noofdays,sum(amount)as amount,packageid,featuresid from vs_packages_detail where classid = ".$classid." group by packageid,noofdays,featuresid
       ) pd on pd.packageid = pm.packageid
       join vs_features_master fm on fm.featuresid = pd.featuresid order by pm.packageid";
       $qry = $this->db->query($sql);
       $result = $qry->result();
       $packages = array();
       $days = array();
       // echo $result[0]->packageid;
       // $days[$result[0]->packageid][] = $result[0]->noofdays;
       foreach ($result as $key => $row) {
        if(!@$days[$row->packageid])
           $days[$row->packageid][] = $row->noofdays;

       if(!in_array($row->noofdays, $days[$row->packageid]))
           $days[$row->packageid][] = $row->noofdays;
       $packages[$row->packageid]['packagename'] = $row->packagename;           
       $packages[$row->packageid][$row->noofdays][$row->featuresid]['featurename'] = $row->featurename;
       $packages[$row->packageid][$row->noofdays][$row->featuresid]['amount'] = $row->amount;
   }

   $response['packages'] = $packages;
   $response['days'] = $days;
   return $response;
}

function processPurchase($postdata)
{
    try {
        if(!$this->session->userdata('apiuserid'))
            throw new Exception("Something went wrong. Please try again later.", 1);

        $packageid = $postdata['packageid'];
        $days = $postdata['days'];
        $featurename = 'features_'.$packageid.'_'.$days;
        $featuresarr = $this->input->post($featurename);
        $features = implode(',',$featuresarr);

        $nearexdays = $days-5;
        $curdate     = date('Y-m-d');
        $posteddate = date('Y-m-d H:i:s.u');
        $expdate = date('Y-m-d',strtotime($curdate. "+".$days." days"));
        $nearexpdate = date('Y-m-d',strtotime($curdate. "+".$nearexdays." days"));
        $selpackage = $this->db->select('t.*,p.packagename')->from('vs_packages_master p')->join('(
            select pm.packageid, sum(amount) as amount from vs_packages_master pm 
            join vs_packages_detail pd on pd.packageid = pm.packageid where pm.packageid = '.$packageid.' and pd.noofdays = '.$days.' group by pm.packageid ) as t','t.packageid = p.packageid')->get()->row();

        $voucher = $this->simsdb->order_by('voucherno')->get_where('vouchers.vouchers',array('vouchertype'=>'PaidVoucher','userid'=>null))->row();
        if(!$voucher)
            throw new Exception("No Vouchers available", 1);
        
        $vouchercode = $voucher->vouchercode;

        # Update vouchers table with vouchervalue, userid, senddatetime and Assign voucher code in case of purchased vouchers
        $voucherupdate = array('voucherfor' => $postdata['usertype'],
            'vouchervalue' => $selpackage->amount,
            'userid' => $this->session->userdata('apiuserid'),
            'senddatettime' => $posteddate,
            'usestatus' => 'Y',
            'useddatetime'=>$posteddate);
        $this->simsdb->update('vouchers.vouchers',$voucherupdate,array('voucherno'=>$voucher->voucherno));

        # Insert to voucheruses master / detail
        $vouchersusesmaster = array("userid" => $this->session->userdata('apiuserid'),
            "voucherno" => $voucher->voucherno,
            "vouchertype" => $voucher->vouchertype,
            "vouchervalue" => $voucher->vouchervalue,
            "numberofdays" => $days,
            "mobilenumber" => $this->session->userdata('apimobileno'),
            "packagename" => $selpackage->packagename);
        $this->simsdb->insert('vouchers.vouchersusesmaster',$vouchersusesmaster);
        $vouchersusesmasterid = $this->get_insert_id('vouchers.vouchersusesmaster','vouchersusesmasterid');
        // $vouchersusesmasterid =1;

        

        $sql = "select DISTINCT on (fd.servicetype,pd.classid,pd.subjectid) fd.servicetype,pd.classid,pd.subjectid,c.classname,s.subjectname,pd.packagedetailid,noofdays,amount,fm.featurename from vs_packages_detail pd 
        join vs_features_master fm on fm.featuresid = pd.featuresid
        join vs_features_detail fd on fd.featuresid = pd.featuresid
        join el_class c on c.classid = pd.classid
        join el_subject s on s.subjectid = pd.subjectid
        where fm.featuresid in (".$features.") and pd.noofdays = ".$days." and pd.packageid = ".$packageid;

        $qry = $this->db->query($sql);
        $packagerate = $qry->result();

        $insertedpdid = array();
        foreach ($packagerate as $key => $rate){
            if(!in_array($rate->packagedetailid, $insertedpdid))
            {
                array_push($insertedpdid, $rate->packagedetailid);
                $amount = $rate->amount;
            }
            else
                $amount = 0;

            $vouchersusesdetail = array("vouchersusesmasterid" => $vouchersusesmasterid,
                "userid" => $this->session->userdata('apiuserid'),
                "voucherno" => $voucher->voucherno,
                "vouchertype" => $voucher->vouchertype,     
                "subjectid" => $rate->subjectid,
                "expirydate" => $expdate,
                "nearexpirydate" => $nearexpdate,
                "classid" => $rate->classid,
                "classname" => $rate->classname,
                "subjectname" => $rate->subjectname,
                "mobilenumber" => $this->session->userdata('apimobileno'),
                "servicetype" => $rate->servicetype,
                "servicename" => $rate->featurename,
                "noofdays" => $rate->noofdays,
                "amount" => $amount,
                "usesdatetime"=>$posteddate);
            // echo "<pre>";
            // print_r($vouchersusesdetail);
            $this->simsdb->insert('vouchers.vouchersusesdetail',$vouchersusesdetail);

            $el_subcription = array("userid" => $this->session->userdata('apiuserid'),
                "usertype" => $postdata['usertype'],
                "mobileno" => $this->session->userdata('apimobileno'),
                "classid" => $rate->classid,
                "subjectid" =>  $rate->subjectid,
                "subscriptiontype" => $voucher->vouchertype,
                "subscriptiondate" => $posteddate,
                "expirydate" => $expdate,
                "nearexpirydate" => $nearexpdate,
                "couponno" => $voucher->vouchercode,
                "ipaddress" => $_SERVER['REMOTE_ADDR']);
            $this->db->insert('el_user_subscription',$el_subcription);
        }

        
       // # Insert to SMS to sent
        // $smsarray = array('createddatetime'=>date('Y-m-d H:i:s.u'),
        //     'createdby'=>'system',
        //     'messagetype'=>'Voucher Code',
        //     'mobilenumber'=>$postdata['mobileno'],
        //     'message'=>'Welcome to MiDas App!'."\n".'Following is the Voucher worth Rs. '.$vouchervalue.'.'."\n".' to activate Study Material of Grade '.$eclass->classname.' on MiDas App.'."\n".'Voucher No: '.$vouchercode."\n".'Thank you.',
        //     'status'=>'1',
        //     'istest' => '1',
        //     'tosenddatetime'=>date('Y-m-d H:i:s.u'));
        // $this->db->insert('smstosent',$smsarray);
        // $html = '<p>We have sent Voucher worth Rs. '.$vouchervalue.' as a text message to your mobile phone ('.$postdata['mobileno'].').</p><p>Please use the Voucher to activate <strong>Interactive Audiovisual Content</strong> for your child.</p>';
        // return $html;

    } catch (Exception $e) {
        throw $e;
    }
    

}

function get_insert_id($table,$field)
{ 
    $rec = $this->simsdb->select('max('.$field.') as insertid',false)->from($table)->get()->row();
    return $rec->insertid;
}

}