<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Api extends App_controller{
    function __construct(){
        parent::__construct();        
        $this->load->model('Account_model', 'model');
    }

    /**
     * Get Account
     * @param type $authcode 
     * @return type
     */
    function index(){
        try{
            $authcode= $this->input->get('authcode');
            $result  = $this->model->getParentAccount($authcode);
            $this->load->vars(
                array(
                    'secondaryviewlink' => 'parent/myAccount',
                    'result'  => $result
                    )
                );
            
            $this->render_page(); 

        } catch(Exception $e){
            echo '<p style="text-align: center; padding: 10px; background: white;">'.$e->getMessage().'</p>';
        }   
    }

    function myAccount($authcode=false){
        // echo "<p>".$response."</p>";
        $this->load->vars(array(
            'secondaryviewlink' =>    'parent/myAccount',
            'student_name'      =>    @$student_name
            ));

        $this->render_page(); 
    }

    /**
     * Get Sugscription
     * @return type
     */
    function subscription($authcode=false){
        try{
            if(!$authcode)
            {
                echo '<font align="center" style="width: 100%;display: flex;height: 100%;margin: 0 auto;align-items: center;text-align: center;font-size: 20px;color: #ce5d35;"><span style="text-align: center;width: 100%;
                ">Online purchase facility will be available soon.</span></font>';
                exit;
            }
            $response = "Subscription";
            if(!@$_GET['authcode'] || !@$_GET['mobileno'] || !@$_GET['classid'])
                throw new Exception("This feature is currently unavailable.", 1);
            $sessiondata = $_GET;
            $this->model->setSession($sessiondata);
            $response = $this->model->getPackages();
            $this->load->vars(array(
                'secondaryviewlink' =>    'subscription',
                'packages'      =>    $response['packages'],
                'days'      =>    $response['days']
                ));
            $this->render_page(); 
        } catch(Exception $e){
            $response = $e->getMessage();
            echo $response;
        }
        // echo "<p>".$response."</p>";
    }

    function purchase(){
        try {
         $postdata = $this->input->post();
         $postdata['usertype'] = 'Student';
         $this->model->processPurchase($postdata);
     } catch (Exception $e) {
        echo $e->getMessage();
     }
 }
}
