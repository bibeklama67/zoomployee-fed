<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Creations extends Front_controller {

	function __construct()
	{

		parent::__construct();

		$d=$this->config->item('default');
		$df=$this->config->item($d); 
		$df['js'][]='common/student_api';
		$df['js'][]='creation/creation';
		$df['js'][]='ckeditor/ckeditor';
		$df['js'][]='forum/nepali_unicode';
		$this->config->set_item($d,$df);
		$this->load->model('frontend_model','frontmodel');
	}	

	public function unicode(){
		$this->load->view('test');
	}

	public function index()
	{
		$userid=$this->session->userdata('userid');
		$title=$this->input->post('title');
		$student_name=$this->session->userdata('student_name');

		$this->load->vars(array(
			'secondaryviewlink' =>    'creations/creations_wrapper',
			// 'formview'          =>    'practiceexam/practiceexam',
			'searchview'        =>    'templates/header/search',
			'student_name'      =>    $student_name
			));

		$this->render_page('creation_view');		
	}	

	function comments($mycreationsid)
	{
		$data = $this->input->post();
		//$this->session->set_userdata('quizActivity', 'homeworklive');

		$this->load->vars(array('mycreationsid'=>$mycreationsid,'data' => $data));
		$this->load->view('creation_comments');   
	}

	function newcomment($mycreationsid){
		$data = $this->input->post();

		$this->load->vars(array('mycreationsid'=>$mycreationsid,
			'data'=>$data
			));
		$this->load->view('comment_listing');   
	}

	function creation_post(){
		// die("here");
		$data = $this->input->post();
		$response = $data['response'];
		$userid=$this->session->userdata('userid');
		$title=$this->input->post('title');
		$student_name=$this->session->userdata('student_name');

		$this->load->vars(array(
			// 'response'			=>	$response,
			'data'				=>  $data,
			'secondaryviewlink' =>	'creations',
			'formview'          =>	'student_progress/progress/studenthome',
			'searchview'        =>	'templates/header/search',
			'student_name'      =>	$student_name
			));

		$this->load->view('creations/creations', $response);	
	}

	function newcreation()
	{
		$data = $this->input->post();

		$this->load->vars(array(
			'creations'=>array($data['response']),
			));
		$this->load->view('creation_post');   
	}

	function webcam(){
		$this->load->view('webcam');
	}
	function saveimage(){
		$this->load->view('saveimage');
	}

	
	function edit_creation(){
		$posttext = $this->input->post('posttext');
		$postimagefile = $this->input->post('postimagefile');
		$mycreationsid = $this->input->post('mycreationsid');
		$creationcategory = $this->input->post('creationcategory');
		$data = array(
			'postimagefile'=>$postimagefile,
			'posttext'=>$posttext,
			'mycreationsid'=>$mycreationsid,
			'creationcategory'=> $creationcategory,
			);
		$this->load->view('creation_modal', $data);
	}

	function edit_comment(){
		$comment = $this->input->post('comment');
		$mycreationsid = $this->input->post('mycreationsid');
		$commentid = $this->input->post('commentid');
		$data = array(
			'comment'=>$comment,
			'mycreationsid'=>$mycreationsid,
			'commentid'=> $commentid,
			);
		$this->load->view('comment_modal', $data);
	}

	function likedby(){
		$data = $this->input->post();
		$this->load->vars(array(
			'response'=>array($data['response']),
			));
		$this->load->view('creation_likedby');
	}
	

}
