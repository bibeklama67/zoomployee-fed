  <?php
    $data = $data['response'];
    $comments = $data['comments'];
    if($comments){
      foreach ($comments as $comment) {

  ?>
          <div class="col-md-12 new_comment uni_comment<?= $comment['commentid']?>" style="padding: 0px; margin-top: 0px;">
            <div class="pull-left">
             <img src="<?= $comment['userimage']?$comment['userimage']:base_url('assets/images/dummy-person.jpg');?>" width="40px" height="40px" class="img-responsive"/>  
            </div>
            <div class="pull-left" style="margin-left: 10px;">
              <label for="username" style="margin-bottom: 0px;"><a><span style="color:#717B9B; font-size:12px;"><?php echo $comment['username'];?></span></a></label><small id="comment_text<?= $comment['commentid']?>">&nbsp;&nbsp;<?php echo $comment['comment'];?></small> <br>
             <!--  <a><small style="color: #7D7D7D;">Like</small></a>
              <i class="fa fa-circle" aria-hidden="true" style="font-size:3px;"></i>
              <a><small style="color: #7D7D7D;">Reply</small></a> -->
              <!-- <i class="fa fa-circle" aria-hidden="true" style="font-size:3px;"></i> -->
              <small style="color:8f949a"><?php echo $comment['commenteddatetime'];?></small>
            </div>
             <?php
          if($this->session->userdata('usertype')== $comment['commentedbytype']){
            if($this->session->userdata('userid') == $comment['commentedbyuserid']){
              if($comment['status'] == 'P'){
                ?>
                <div class="dropdown pull-right">

                  <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background: #ffffff;">
                    <i class="fa fa-chevron-down" aria-hidden="true" style="color: #B3B7BE;font-size: 10px;margin-top: -5px;"></i>
                  </button>

                  <ul class="dropdown-menu">

                    <li><a href="javascript:;" class="edit_comment" data-mycreationsid="<?php echo $mycreationsid; ?>" data-commentid="<?= $comment['commentid']?>" data-comment="<?= $comment['comment']?>">Edit</a></li>
                    <li><a href="javascript:;" class="delete_comment" data-mycreationsid="<?php echo $mycreationsid; ?>" data-commentid="<?= $comment['commentid']?>" >Delete</a></li>
                  </ul>
                </div>
                <?php
              }
            }
          }
          ?>
          </div>
        <?php
        }
        }
      ?>

      <div id="comment_dialog" title="Confirmation Required" style="display: none">
    Are you sure, you want delete this comment?
    </div>