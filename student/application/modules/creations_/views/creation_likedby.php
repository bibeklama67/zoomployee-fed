<style>
    .views-wrapper ul {
        list-style: none;
    }
    .views-wrapper li {
        padding: 10px;
        border-bottom: 1px solid #ccc;
    }
    .views-wrapper li:last-child {
        padding: 10px !important;
        padding-bottom: 0 !important;
        border-bottom: none !important;
    }
</style>
<ul>
    <?php 
    $likedby = $response[0]['likes'];

    if($likedby){
        foreach ($likedby as $like) {
            ?>
            <li class="row">
              <div class="col-xs-2 no-padidng" style="padding-left: 13px;">

                <img src="<?= $like['userimage']?$like['userimage']:base_url('assets/images/dummy-person.jpg');?>" class="img-responsive">
            </div>
            <div class="col-xs-6">
                <?php echo $like['username'];?><br>
                <?php echo $like['school']?$like['school']:'';?>
            </div>
            <div class="col-xs-4 text-right">
                <?php echo $like['usertype']; ?><br>
                <?php echo $like['likeddatetime']; ?>
            </div>
        </li>
        <?php
    }
}
?>
</ul>