<?php
// ini_set('max_input_vars', '2500');
// ini_set('memory_limit', '256M');
defined('BASEPATH') OR exit('No direct script access allowed');

class Eclass extends Front_controller {

  function __construct()
  {
    parent::__construct();
		$d=$this->config->item('default');
		$df=$this->config->item($d); 
		// $df['js'][]='forum/nepali_unicode';
		$df['js'][]='common/student_api';
		//  $df['js'][]='forum/pramukhime';
		//  $df['js'][]='forum/pramukhindic';
   
		$df['js'][]='dashboard/dashboard';
		$df['js'][]='myclass/myclass';
		$df['js'][]='myclass/quiz';
		$df['js'][]='myclass/discussionquiz';
		$df['css'][] = 'newstyle';

		//  $df['js'][]='common/pusher.min';   
		//  $df['js'][]='ckeditor/ckeditor';

   $this->config->set_item($d,$df);
   $this->load->library('curl');
   $this->curl_apiprefix=$this->config->item('api_url').'api/';
     $this->load->model('teacherend/teacherend_model','tmodel');

  // $this->curl_apiprefix='http://dev.myschool.midas.com.np/api/';
 }
 
 
public function iframe()
 {

     $eclassid=$_GET['e'];    
     $orgid=$_GET['o'];
     $user=$_GET['u'];
     $section=$_GET['section'];

     if(isset($_GET['type']) && $_GET['type']=='new')
     {
          $this->session->set_userdata('url',base_url().'eclass/iframe?type=new&o='.$orgid.'&ip='.$_GET['ip'].'&u='.$user.'&name='.$_GET['name'].'&purl='.$_GET['purl']);
          if($eclassid!='')
           $checkteacher=$this->tmodel->checkauthteacher($user,$orgid,$eclassid);
         else
           $checkteacher=$this->tmodel->checkauthteacher($user,$orgid,false);
           $eclassid=$checkteacher->eid;
          $showclass='Y';
     }
     else{  $showclass='N'; }
	 
           if(empty($section) || $section=='null' || $section=='NULL')
		    {
				 $section='-1';
					 // var_dump($section);die();
				 $checkteacher=$this->tmodel->checkauthteacher($user,$orgid,$eclassid);
				 //var_Dump($checkteacher);exit;
				 if($checkteacher===false)
				 {
				   echo "Not Authentic Teacher";
				   exit;
				 }
			  
				 $getch_detail=$this->tmodel->getchapterinfo($_GET['ch']);
				 $this->session->set_userdata('subjectid',$getch_detail->subjectid);
				 $this->session->set_userdata('chapterid',$getch_detail->chapterid);
				 $this->session->set_userdata('subjectname',$getch_detail->subjectname);
				 $this->session->set_userdata('chaptername',$getch_detail->chaptername);
			}
	 
   
    // var_dump($getch_detail);exit;
     $this->session->set_userdata('teacher','Y');
     $this->session->set_userdata('showclass_dropdown',$showclass);
     $this->session->set_userdata('orgid',$orgid);
     $this->session->set_userdata('eclassid',$eclassid);
     $this->session->set_userdata('classname',$checkteacher->classname);

     $this->session->set_userdata('teacherid',$user);
     $this->session->set_userdata('teachername',$_GET['name']);
     $this->session->set_userdata('sectionid',$section);
     $this->session->set_userdata('fm','');
     $this->session->set_userdata('fs','');
     if(isset($_GET['fm']))
     {
		$this->session->set_userdata('fm',$_GET['fm']);
		$this->session->set_userdata('fs',$_GET['fs']);
     }
	 
    // var_Dump($this->session->userdata);exit;
    
    
   
     $this->load->vars(array(
       // 'secondaryviewlink' =>    'dashboard',
       'source'=>'MC',
       'formview'          =>    'myclass/dashboard_foriframe',
       'searchview'        =>    'templates/header/search',
       'student_name'      =>    '',
       'eclassid'=>$eclassid,
       'orgid'=>$orgid,
	   'teacherid'=>$user,
	   'sectionid'=>$section
       ));
   
     $this->render_page();		
 }
 
  function getclasssection()
  {

    //var_dump($this->session->all_userdata());exit;

    $res=$this->tmodel->get_classsection();
    if(count($res)<1)
    {
      $res[0]->secid='-1';
      $res[0]->section='ALL';
    }
    echo json_encode(array("section"=>@$res,"total"=>count($res)));
  }
  
}