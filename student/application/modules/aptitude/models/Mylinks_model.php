<?php
class Mylinks_model extends CI_Model
{
    function __construct() {
        parent::__construct();
        $this->ecademy = $this->load->database('exam', TRUE);
		$this->load->helper('array');
    }
	
	function get_subjects($quiz)
	{
		$classid = $this->session->userdata('eclassid');
		$this->ecademy->distinct('subjectid,subjectname')->select('subjectid,subjectname')->from('online_exam.class_subject_chapter')
				->where('classid',$classid) ;
		if($quiz && $quiz !='olymp' )
			$this->ecademy->where('subjectname','Quizzes');
		elseif($quiz=='olymp')
			$this->ecademy->like('subjectname','Olymp');
		else
			$this->ecademy->where('isactivesubject','Y');
		
	    $this->db->order_by('from_date','desc');
		
		$result = $this->ecademy->get()->result();
		
		//echo $this->ecademy->last_query();
		//echo "<BR><BR>";
		
		return $result;
		
	}
	
	public function get_test_link($type,$orgid,$classid,$subjectid,$studentid){
		
		try{
			if($type=='unplanned_quiz' ||  $type=='planned_quiz')
			{
				if($classid=='-1')
					throw new Exception('Classid Not Provided');
			}else{
				if($subjectid=='-1')
					throw new Exception('Subjectid Not Provided');
			}
			
			if($type!='aptitude' && $studentid==false)
					throw new Exception('Studentid Not Provided');
			
			if($type=='unplanned_quiz' || $type=='planned_quiz')
			{
				
				$this->ecademy->distinct('subjectid,subjectname')->select('subjectid,subjectname')->from('online_exam.class_subject_chapter')
				->where('classid',$classid) ;
				if($type =='unplanned_quiz' )
					$this->ecademy->where('subjectname','Quizzes');
				elseif($type=='planned_quiz')
					$this->ecademy->like('subjectname','Olymp');
				
				$this->db->order_by('from_date','desc');
				
				$result = $this->ecademy->get()->result();
				$result = element('0',$result);
				$link = array(rtrim("https://www.midasecademy.com/aptitude/aptitude_register/index/{$result->subjectid}/{$type}/{$orgid}/{$classid}/{$studentid}",'/'),
							  rtrim("https://www.midasecademy.com/aptitude/aptitude_register/get_my_results/{$type}/{$studentid}",'/'),
							  rtrim("https://www.midasecademy.com/aptitude/aptitude_register/get_top/{$result->subjectid}/{$type}",'/')
							  );
				return implode(',',$link);
			}else{
				//https://www.midasecademy.com/aptitude/aptitude_register/index/84/planned/10000002/42/18434761
				$data = $this->ecademy->distinct('classid')->select('classid')->from('online_exam.class_subject_chapter')->where('subjectid',$subjectid)->get()->result();
				$data = element('0',$data);
				
				$link = array(rtrim("https://www.midasecademy.com/aptitude/aptitude_register/index/{$subjectid}/{$type}/{$orgid}/{$data->classid}/{$studentid}",'/')); 
				if($type=='aptitude'):
						$link[] = '';//"https://www.midasecademy.com/aptitude/aptitude_register/past_results/{$subjectid}/{$type}/{$studentid}";
						$link[] = '';
				elseif($type=='self'):
						$d = $this->ecademy->select('aptitudeid')->from('online_exam.aptitude_test_class')->where('subjectid',$subjectid)->where('studentid',$studentid)->get()->row();
						$link[] = rtrim("https://www.midasecademy.com/aptitude/aptitude_register/past_results/{$d->aptitudeid}/{$type}/{$studentid}",'/');
						$link[] = '';
				else:
						$link[] = rtrim("https://www.midasecademy.com/aptitude/aptitude_register/get_top/{$subjectid}/{$type}",'/');
				endif;			  
				return implode(',',$link);
			}
			
		}catch(Exception $e)
		{
			throw $e;
		}
	}
	
	
}
?>