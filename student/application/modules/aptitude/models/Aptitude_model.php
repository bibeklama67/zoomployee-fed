<?php
class Aptitude_model extends CI_Model
{
    function __construct() {
        parent::__construct();
				
        $this->ecademy = $this->load->database('exam', TRUE);
			$this->load->helper('array');
    }

    public function savedata($postdata)
    {
		try{
		
				$sessdata = $this->session->all_userdata();
				//var_dump($sessdata);
				
				$cond = elements(array('studentid','subjectid','parent_mobileno'),$postdata);
				if(!element('studentid',$postdata))
					unset($cond['studentid']);
				
				$this->ecademy->where($cond);
				$d = $this->ecademy->escape_str(strtolower(element('student_name',$postdata)));
				$this->ecademy->where("lower(student_name)","'$d'",false);
				$data = $this->ecademy->get('online_exam.aptitude_test_class');
				
				if(!$data)
						throw new Exception("Registration Error!");
					
				if($data->num_rows() <= 0 )
				{
					   //insert data
					    $postdata['datapostdatetime']=date('Y-m-d H:i:s');
						$data =  $this->ecademy->insert('online_exam.aptitude_test_class',$postdata); 
						if(!$data)
							throw new Exception("Registration Error!");
						$id = $this->ecademy->insert_id();
						
				}else{
						//update
						$data = $data->row();
						$this->ecademy->where('aptitudeid',$data->aptitudeid);
						$postdata['dataupdatedon']=date('Y-m-d H:i:s');
						$dt =  $this->ecademy->update('online_exam.aptitude_test_class',$postdata); 
						if($dt === false)
								throw new Exception("Registration Error!");
						$id = $data->aptitudeid;
				}
				return $id;
		}catch(Exception $e)
		{
			throw $e;
		}
    }

	public function get_past_exams($aptid=false,$type='aptitude',$schoolid='10000002')
	{
		try{
			if(!$aptid)
				throw new Exception("Required Paramter Missing");
			
			$aptid = $this->ecademy->escape($aptid);
			
			$sql ="select aptitudeid,examid,current_question,student_name,subjectname,classname,start_date,last_updated_on,ae.completed_on,
										count(*) filter(where level='easy') total_easy, 
										count(*) filter(where level='easy' and aed.iscompleted='t' and provided_answer is not null) attempted_easy,
										count(*) filter(where level='easy' and aed.iscorrect='t') correct_easy,
										count(*) filter(where level='medium') total_medium, 
										count(*) filter(where level='medium' and aed.iscompleted='t' and provided_answer is not null) attempted_medium,
										count(*) filter(where level='medium' and aed.iscorrect='t') correct_medium,
										count(*) filter(where level='hard') total_hard, 
										count(*) filter(where level='hard' and aed.iscompleted='t' and provided_answer is not null) attempted_hard,
										count(*) filter(where level='hard' and aed.iscorrect='t') correct_hard,
										count(*) total,
										count(*) filter(where aed.iscompleted='t' and provided_answer is not null) attempted,
										count(*) filter(where aed.iscorrect='t') correct
						from online_exam.appt_exam ae
						join online_exam.appt_exam_detail aed using(examid)
						join online_exam.aptitude_test_class atc using(aptitudeid)
						join (select distinct classid,classname,subjectid,subjectname from online_exam.class_subject_chapter) csc on csc.subjectid=atc.subjectid
						where aptitudeid=$aptid and examtype='$type'
						group by examid,student_name,subjectname,classname,aptitudeid order by start_date desc";
						
						
			  $data = $this->ecademy->query($sql)->result();
			  
			  //if(!$data || count($data)<=0 )
				  //throw new Exception("No $type test taken till now.");
			  
				return $data;
		}catch(Exception $e){ throw $e; }
	}

	public function get_exam_completed($aptid=false,$type='aptitude',$schoolid='10000002')
	{
		try{
			if(!$aptid)
				throw new Exception("Required Paramter Missing");
			
			$aptid = $this->ecademy->escape($aptid);
			
			$sql ="select examid,current_question,student_name,subjectname,classname,start_date,
										count(*) filter(where level='easy') total_easy, 
										count(*) filter(where level='easy' and aed.iscompleted='t' and provided_answer is not null) attempted_easy,
										count(*) filter(where level='easy' and aed.iscorrect='t') correct_easy,
										count(*) filter(where level='medium') total_medium, 
										count(*) filter(where level='medium' and aed.iscompleted='t' and provided_answer is not null) attempted_medium,
										count(*) filter(where level='medium' and aed.iscorrect='t') correct_medium,
										count(*) filter(where level='hard') total_hard, 
										count(*) filter(where level='hard' and aed.iscompleted='t' and provided_answer is not null) attempted_hard,
										count(*) filter(where level='hard' and aed.iscorrect='t') correct_hard,
										count(*) total,
										count(*) filter(where aed.iscompleted='t' and provided_answer is not null) attempted,
										count(*) filter(where aed.iscorrect='t') correct
						from online_exam.appt_exam ae
						join online_exam.appt_exam_detail aed using(examid)
						join online_exam.aptitude_test_class atc using(aptitudeid)
						join (select distinct classid,classname,subjectid,subjectname from online_exam.class_subject_chapter) csc on csc.subjectid=atc.subjectid
						where aptitudeid=$aptid and ae.completed_on is not null and examtype='$type'
						group by examid,student_name,subjectname,classname order by start_date desc";
						
						
			  $data = $this->ecademy->query($sql)->result();
			  
			 // echo $this->ecademy->last_query();
			  //if(!$data || count($data)<=0 )
				  //throw new Exception("No $type test taken till now.");
			  
				return $data;
		}catch(Exception $e){ throw $e; }
		
	}
	
	public function get_setup_data($aptitudeid=false,$type='aptitude',$orgid='10000002')
	{
		if(!$aptitudeid)
					throw new Exception("Something Went Wrong.");

					$sql = "select csc.classname,csc.subjectname,s.setupid,forsubjectid,from_date,atd.chapterid,atd.qty_lvl1,
							atd.qty_lvl2,atd.qty_lvl3,s.start_time,student_name,examtype,examtype_full,exam_name,forschoolid,nextattemptdays,qselection
					from online_exam.apptestsetup s 
				join online_exam.aptitude_test_class atc on atc.subjectid = s.forsubjectid
				join online_exam.apptestsetup_detail atd on atd.setupid=s.setupid
				join (select distinct classid,classname,subjectid,subjectname from online_exam.class_subject_chapter) csc on csc.subjectid=s.forsubjectid
				where from_date = ( select max(from_date) from online_exam.apptestsetup 
									  where forsubjectid=s.forsubjectid  and forschoolid=s.forschoolid and examtype=s.examtype and ";
				
				if($type=='planned' || $type=='planned_quiz')
					$sql.= " from_date = current_date and isactive='t') ";
				else
					$sql.= " from_date <= current_date and isactive='t') ";

				$sql.=" and s.isactive='t' and atc.aptitudeid=$aptitudeid and s.forschoolid='$orgid' and examtype='$type'";
				
				$data = $this->ecademy->query($sql)->result();
				
				return $data;
	}
	
	public function populate_exam_questions($aptitudeid=false,$type='aptitude',$orgid='10000002')
	{
		try{
				//echo $aptitudeid;
				$data = $this->get_setup_data($aptitudeid,$type,$orgid);
				
				if(empty($data))
				{
					$fexam = $this->get_exams_within_next_three_months($aptitudeid,$type,$orgid);
					if(empty($fexam))
					{
						$et = array('planned'=>'Online exam',
								'aptitude'=>'Aptitude test',
								'planned_quiz'=>'Olympiad exam',
								'unplanned_quiz'=>'Daily quiz',
								'self' => 'Self assessment test ');
						$et = element($type,$et);
						throw new Exception("$et for your grade is not scheduled yet. <br> Please try later.");
					}else{
							$arr = array('examid'=>false,'setupdata'=>$fexam,'aptitudeid'=>$aptitudeid,'examtime'=>'future_dates','examdata'=>false);			
							return $arr;
						}					
				}
				
				$setupdata = element('0',$data);
				
				//exam setup exist Now check if this setup has already been populated in exam table.
				$setupid = $setupdata->setupid;
				
				if($type=='planned' || $type=='planned_quiz')
				{
					$curtime = strtotime(date('Y-m-d H:i:s',strtotime('+5 minutes', time() )));
					if( $curtime <= strtotime($setupdata->from_date.' '.$setupdata->start_time) )
					{ 
						//Exam Time Vako Xaina Show CountDown 
						$arr = array('examid'=>false,'setupdata'=>$setupdata,'aptitudeid'=>$aptitudeid,'examtime'=>'early','examdata'=>false);
						//var_dump($arr); exit;
						return($arr);
					}
				}
				
				$sql="select appt_exam.*,sum(timemins) totalmins from online_exam.appt_exam 
							join online_exam.appt_exam_detail using(examid)
							where setupid=$setupid and aptitudeid=$aptitudeid ";
							
				if($type == 'planned_quiz' || $type=='unplanned_quiz' || $type='planned')
				{
					$sql.= " and start_date::date = current_date ";
				}
				
				$sql.=" group by examid";

				//echo $sql; 
				$examdata = $this->ecademy->query($sql)->row();

				if($examdata)
				{
					//Exam already Exists for this person/subject So no need to populate exam questions
					return array('examid'=>$examdata->examid,'setupdata'=>$setupdata,'aptitudeid'=>$aptitudeid,'examtime'=>false,'examdata'=>$examdata);
				}else{
					//Exam Does not Exist for this user. So check if exam questions have been populated for other students
					$sql="select * from online_exam.appt_exam where setupid=$setupid limit 1";

					$examdata = $this->ecademy->query($sql)->row();
						if(!$examdata || $setupdata->qselection=='random')
						{
							//This is the first student so populate exam using setup 
							$examid = $this->startexam($aptitudeid,$type,$orgid);
						}else{
								//already questions have been populated for other users
								//populate exam questions from previous exam ( so that each students get the same questions )
								$examid = $examdata->examid;
								$examid = $this->startexam($aptitudeid,$type,$orgid,$examid);
						}
				
						$sql="select appt_exam.*,sum(timemins) totalmins from online_exam.appt_exam 
							join online_exam.appt_exam_detail using(examid)
							where examid=$examid
						group by examid";

						$examdata = $this->ecademy->query($sql)->row();
						return array('examid'=>$examdata->examid,'setupdata'=>$setupdata,'aptitudeid'=>$aptitudeid,'examtime'=>false,'examdata'=>$examdata);
				}

		}catch(Exception $e)
			{
				throw $e;
			}
	}

	public function get_exam_in_progress($aptid=false,$type='aptitude',$orgid='10000002')
	{
		try
		{
			if(!$aptid)
				throw new Exception("Required Paramter Missing");
		
			$sql ="select count(*) filter(where ae.completed_on is null) over() not_complete, 
							count(*) filter(where ae.completed_on is not null) over() completed, 
							ats.setupid,forsubjectid,from_date,ae.start_date,classname,subjectname,
							ae.last_updated_on,ae.completed_on,ae.examid,sum(ed.timemins) total_timemins,ats.start_time,ae.current_question,ae.aptitudeid
					   from 
						online_exam.appt_exam ae
						join online_exam.appt_exam_detail ed using(examid)
					  join online_exam.apptestsetup ats using(setupid)
					  join online_exam.aptitude_test_class atc using(aptitudeid)
					  join (select distinct classid,classname,subjectid,subjectname from online_exam.class_subject_chapter) csc on csc.subjectid=atc.subjectid
					 where aptitudeid=? and schoolid=? and ae.examtype=? 
					 group by ae.examid,ats.setupid,classname,subjectname
					 order by start_date desc";

			$row = $this->ecademy->query($sql,array($aptid,$orgid,$type));
			
			//$d0 = element('0',$row->result());
			if($row->num_rows()<=0)
			{
				return array();
				
			}else{
				 return $row->result();
			}
		}catch(Exception $e)
		{
			throw $e;
		}
	}

	function get_exams_within_next_three_months($aptitudeid,$type,$orgid){
		$apt = $this->ecademy->from('online_exam.aptitude_test_class')
					->where('aptitudeid',$aptitudeid)->get()->row();
					
		$this->ecademy->from('online_exam.apptestsetup')
				->where('forsubjectid',$apt->subjectid)->where('examtype',$type)->where('forschoolid',$orgid)
				->where("from_date::Date between current_date and current_date + interval '3 months'",false,false)
				->where("setupid not in (select setupid from online_exam.appt_exam where aptitudeid=$aptitudeid and start_date::date=current_date)",false,false);
		
		$data = $this->ecademy->get()->result();
		return $data;
	}

    public function get_distincit_subject()
    {
    	$sql = "select distinct subjectname from online_exam.class_subject_chapter";
    	return $this->ecademy->query($sql)->result_array();
    }

    public function get_classes($orgid)
    {
    	$sql = "select distinct (csc.classid), csc.classname, csc.classorder from online_exam.class_subject_chapter csc join online_exam.online_exam oe on csc.classid=oe.classid join online_exam.exam_schedule es on oe.exam_id=es.exam_id where oe.orgid=? and csc.isclassactive=? and CURRENT_TIMESTAMP between es.start_datetime and es.end_datetime order by csc.classorder";
		$res=$this->ecademy->query($sql,array($orgid,'Y'))->result();

		return $res;
    }
	
	public function get_result($examid = false, $examtype = 'aptitude')
	{

		  if( !in_array($examtype,array('unplanned_quiz')) )
		  {
			  $lvl = " ,level ";
		  }
		  else{
			  $lvl = "";
		  }			  
		  
		  $sql = "select aptitudeid,subjectid $lvl ,subjectname,sum(om) om, sum(marks) tm, count(qorder) total,
								count(qorder) filter (where iscompleted='t' and provided_answer is not null) attempted,
								count(qorder) filter (where iscompleted='t' and provided_answer is null) skipped,
								count(qorder) filter (where iscorrect='t') correct,e.examtype,atc.studentid,
								(e.completed_on - e.start_date) timetaken_format,e.start_date,e.completed_on
						from online_exam.appt_exam_detail ed
									join online_exam.appt_exam e using(examid)
									join online_exam.aptitude_test_class atc using(aptitudeid)
									join (select distinct classid,classname,subjectid,subjectname from online_exam.class_subject_chapter) csc using(subjectid)
						where examid = $examid
						group by e.examid,aptitudeid,subjectid $lvl ,subjectname,e.examtype
						order by e.start_date desc";
				
				$data = $this->ecademy->query($sql);
				$data = $data->result();
				$od = current($data);
				foreach($data as $d)
				{
						if($examtype=='unplanned_quiz')
							$d->level = 'hard';
							
						$pct = (int) ( ($d->correct * 100) / $d->total);
						$this->ecademy->where("percent_".$d->level.">=",$pct,false);
						
				}
				
				$this->ecademy->order_by("random()");
				$scheme = $this->ecademy->from("online_exam.appt_scheme")->get()->result();
				
				//var_dump($od); exit; 
				$this->ecademy->where('aptitudeid',$od->aptitudeid);
				$this->ecademy->where('examtype',$od->examtype);
				$this->ecademy->where('examid!=',$examid);
				$this->ecademy->where("completed_on is not null");
				$other_exams = $this->ecademy->from("online_exam.appt_exam")->get()->result();
				
				return array('scheme'=>$scheme,'data'=>$data,'other_exams'=>$other_exams);
	}
	
	public function get_current_question($examid=false)
	{
		try{
				if(!$examid)
						throw new Exception("Required Paramter Missing");
						
				$data = $this->ecademy->where('examid',$examid)->get('online_exam.appt_exam')->row();
				
				if(empty($data))
					 throw new Exception("Selected Exam Not Available Now"); 
				  
				if(empty($data->current_question))
				{
						$cq = 1; 
						$this->ecademy->update('online_exam.appt_exam',array('start_date'=>date('Y-m-d H:i:s')),array('examid'=>$examid));
				}
				else
						$cq = $data->current_question;
				
				if($cq > $data->total_question || !empty($data->completed_on) )
				{
					   $result = $this->get_result($examid,$data->examtype);
						return array('status'=>"Completed",'data'=>$result);
				}
				
				if($cq == $data->total_question)
						$lastq = true;
				else
						$lastq = false;
				//update exam_detail for current_question _ set start_time
					$this->ecademy->update('online_exam.appt_exam_detail',array('started_on'=>date('Y-m-d H:i:s')),array('qorder'=>$cq,'examid'=>$examid,'started_on'=>null));
					if($this->ecademy->_error_message()!='')
							throw new Exception("Start Time Could Not be Updated.");
						
					$data = $this->ecademy->where('examid',$examid)->where('qorder',$cq)->get('online_exam.appt_exam_detail')->row();
				
				return array('data'=>$data,'lastq'=>$lastq,'status'=>'running');
				
		}catch(Exception $e)
		{
			throw $e; 
		}
	}
	
	public function getoverallexam($examid=false)
	{
		try{
				if(!$examid)
					throw new Exception("Required Paramter Missing");
				
				$examid = $this->ecademy->escape($examid);
				
			  $sql =	"select ae.examid,atc.student_name,ae.aptitudeid,classname,subjectname, ae.start_date, ae.last_updated_on, ae.setupid,
									array_agg(aed.qorder) filter (where iscompleted='t' and provided_answer is not null) attempted,
									array_agg(aed.qorder) filter (where iscompleted='t' and provided_answer is null) skipped,
									count(*) total_questions,
									sum(marks) total_marks,
									sum(timemins) total_time,
									sum(timemins) filter (where iscompleted ='f') remaining_time,
									ae.examtype,ae.exam_date
							from online_exam.appt_exam ae
											join online_exam.appt_exam_detail aed on ae.examid=aed.examid
											join online_exam.apptestsetup ats on ats.setupid = ae.setupid
											join online_exam.aptitude_test_class atc on atc.aptitudeid = ae.aptitudeid
											join (select distinct subjectid,subjectname,classid,classname from online_exam.class_subject_chapter) csc on csc.subjectid=atc.subjectid
							where ae.examid=$examid
							group by ae.examid,subjectname,classname,student_name";
				//echo $sql;			
				return $this->ecademy->query($sql)->row();
				
		}catch(Exception $e)
		{
			throw $e; 
		}
	}
	
	public function save_answer($post=array())
	{
		try{
				$this->ecademy->trans_begin();
				if(empty($post))
					throw new Exception("No Answer posted");
				if(!element('examid',$post) || !element('qorder',$post))
					throw new Exception("Data Not Provided");
				
				$this->ecademy->select('max(qorder) mqorder',false)->from('online_exam.appt_exam_detail')->where('examid',element('examid',$post));
				$rmax = $this->ecademy->get()->row();
				$rmax = $rmax->mqorder;
				
				$cond = elements(array('examid','qorder'),$post);
				$data = elements(array('provided_answer','iscompleted','iscorrect','om'),$post);
				$data['completed_on'] = date('Y-m-d H:i:s');
				$res = $this->ecademy->update('online_exam.appt_exam_detail',$data,$cond);
				if($this->ecademy->affected_rows() <= 0 )
						throw new Exception("Error Saving Answer");
				
				$data=array();
				$this->ecademy->set('total_om','total_om + '.element('om',$post),false);
				$this->ecademy->set('current_question', element('qorder',$post) + 1 );
				
				if(element('qorder',$post)==$rmax)
					$this->ecademy->set('completed_on',date('Y-m-d H:i:s'));
				
				$this->ecademy->where('examid',element('examid',$post));
				$this->ecademy->update('online_exam.appt_exam');
				if($this->ecademy->affected_rows() <= 0)
						throw new Exception("Updating Exam Table Failed");
				$this->ecademy->trans_commit();
				
				return array( 'qorder' => element('qorder',$post)+1, 'last_q'=>(element('qorder',$post)==$rmax) );
				
		}catch(Exception $e)
		{
			$this->ecademy->trans_rollback();
			throw $e;
		}
	}
	
	public function startexam($aptitudeid=false,$type='aptitude',$orgid='10000002',$examid=false)
	{
		try{

			//echo "hello world"; exit;
			
			$this->ecademy->trans_begin();
				
				if(!$aptitudeid)
					throw new Exception("Required Paramter Missing");


				if($examid)
				{  //populate new exam from given examid
					$sql = "insert into online_exam.appt_exam ( aptitudeid,setupid,total_question,total_marks,examtype,schoolid,exam_date,exam_name )
						select '$aptitudeid' aptitudeid,setupid,total_question,total_marks,examtype,schoolid,exam_date,exam_name
					from online_exam.appt_exam where examid=$examid";
					$exdata = $this->ecademy->query($sql);
					if(!$exdata)
					{
						throw new Exception($this->ecademy->_error_message());
						//throw new Exception("Questions Could not be Populated!!");
					}
					
					$nexamid = $this->ecademy->insert_id();

					if(!$nexamid)
						throw new Exception("Questions Population Error!!");

					$sql1 = "insert into online_exam.appt_exam_detail (examid,quiz_contentid,qorder,timemins,marks,qtype,level)
								select '$nexamid' examid, quiz_contentid,qorder,timemins,marks,qtype,level from online_exam.appt_exam_detail
							 where examid=$examid";

					$dtl = $this->ecademy->query($sql1);
					if(!$dtl || $this->ecademy->_error_message()!='')
						throw new Exception($this->ecademy->_error_message());

					$examid = $nexamid;

				}else
					{

				$sql = "select s.setupid,forsubjectid,atd.chapterid,atd.qty_lvl1,atd.qty_lvl2,atd.qty_lvl3,
							s.from_date,s.start_time,s.exam_name,csc.classname,s.examtype
					from online_exam.apptestsetup s 
				join online_exam.aptitude_test_class atc on atc.subjectid = s.forsubjectid
				join online_exam.apptestsetup_detail atd on atd.setupid=s.setupid
				join (select distinct classid,classname from online_exam.class_subject_chapter) csc on csc.classid=s.forclassid
				where from_date = ( select max(from_date) from online_exam.apptestsetup where forschoolid=s.forschoolid and forsubjectid=s.forsubjectid and examtype=s.examtype and ";
				
				if($type!='planned' && $type!='planned_quiz')
					$sql.= " from_date <= current_date and isactive='t' ) ";
				else
					$sql.= " from_date = current_date and isactive='t' ) ";

				$sql.=" and atc.aptitudeid=$aptitudeid and s.forschoolid='$orgid' and examtype='$type' and s.isactive='t' ";

				$rows = $this->ecademy->query($sql);
				$results = $rows->result();
				$setupdata = element('0',$results);
				
				//echo $sql;
				//var_dump($setupdata); exit;
				//echo $sql; exit;
				if(!$setupdata) throw new Exception("No Such Exam Available !! Please try again later.");
				$setupid = $setupdata->setupid;
				$forclassname = $setupdata->classname;
				$forexamtype = $setupdata->examtype;
				$sql = "" ; 
				foreach($results as $r)
				{
					$lvls = array('easy'=>$r->qty_lvl1,'medium'=>$r->qty_lvl2,'hard'=>$r->qty_lvl3);
						//--
					foreach($lvls as $k=>$v)
					{
						if($v>0)
						{
						$sql.="(select quizcontentid as quiz_contentid,chapterid,quiztype qtype,
								level,timemins,marks,classid,classname 
									from el_content_quiz 
									join el_class using(classid)
								where 
									for_apptitude_test='t' and level='$k' 
									and chapterid=$r->chapterid order by random() limit $v ) UNION ";
						}
					}				
				} 

				$sql = trim($sql,"UNION ");
				$sql  = "select row_number() over() as qorder,count(*) over() total_questions,sum(marks) over() total_marks,* from (select * from ( $sql ) A order by qtype,random()) b";
				$result = $this->db->query($sql)->result();

				$r = element('0',$result);
				
				if(!$r)
					throw new Exception("Exam Questions Not Found");
				
				$exam_data = array('aptitudeid'=>$aptitudeid,'setupid'=>$setupid,'schoolid'=>$orgid,'examtype'=>$type,'total_question'=>$r->total_questions,'total_marks'=>$r->total_marks,'exam_name'=>$setupdata->exam_name);
				
				//set exam_date time for olympiyad and planned exam
				if($type=='planned' || $type=='planned_quiz')
					$exam_data['exam_date']  = $setupdata->from_date.' '.$setupdata->start_time;

				$dt = $this->ecademy->insert('online_exam.appt_exam',$exam_data);

				if(!$dt)
					throw new Exception("Something Went Wrong");
	
				$examid = $this->ecademy->insert_id();
				$classes=array('NURSERY','LKG','UKG','ONE','TWO','THREE','FOUR','FIVE','SIX','SEVEN','EIGHT','NINE','TEN');
				$levels = array('hard','medium','easy');
				$exam_ins = array();
				$str='';
				foreach($result as $r)
				{
					if($forexamtype=='aptitude' && in_array(strtoupper($forclassname),$classes))
					{
						$fc = array_search(strtoupper($forclassname),$classes);
						$qc = array_search(strtoupper($r->classname),$classes);
						
						$diff = $fc - $qc;
						
						$lvl = array_search(strtolower($r->level),$levels);
						
						$level = $lvl + $diff;
						
						//$str.="$fc = $forclassname | $qc = $r->classname | qc-fc = $diff | lvl = $lvl | $r->level | lvl+diff = $level <BR>";
						
						if($level > 2)
							$level=2;
						
						if($level < 0)
							$level = 0;
					
						$level = element($level,$levels);
					}else{
						$level = $r->level;
					}
					
					$d = array('examid'=>$examid,'quiz_contentid'=>$r->quiz_contentid,
					           'qorder'=>$r->qorder,'timemins'=>$r->timemins,'marks'=>$r->marks,
									'qtype'=>$r->qtype, 'level'=>$level );
					$exam_ins[] = $d;
					
					//$str.="$level = real: $r->level | $forclassname = real: $r->classname || $fc = real: $qc <BR><BR>";
				}
				
				$this->ecademy->insert_batch("online_exam.appt_exam_detail",$exam_ins);
				if($this->ecademy->_error_message()!='')
						throw new Exception("Something Went Wrong");
			}
			
			//throw new Exception($str);
			
			$this->ecademy->trans_commit();
				return $examid;
				
		}catch(Exception $e)
			{
				$this->ecademy->trans_rollback();
				//echo $e->getMessage();
				//exit;
				throw $e;
			}
		
	}

	function get_studentdetail($studentid=false)
	{
		if(!$studentid)
			throw new Exception("Studentid Not Found");
		
		 $sql = "select studentid,concat(studentfirstname,' ',studentlastname) student_name, COALESCE(guardianmobileno,studentmobileno) parent_mobileno,
		 concat(guardianfirstname,' ',guardianlastname) parent_name, organizationname school_name, org_organization.districtid, org_organization.address, 
		 case when isnagarpalika='Y' then concat(vdcname,' ','Municipality') ELSE vdcname END vdc,
		 eid classid
			  from sch_studentcurrent
			  join org_organization on organizationid=orgid
			  join org_class on org_class.classid = sch_studentcurrent.gclassid 
			  left join devcommittee on org_organization.vdcid = devcommittee.vdcid
		 where studentid=? order by ayearid desc limit 1";
		
		  return $this->ecademy->query($sql,array($studentid))->row();
	}

	function get_exam_list($type,$classid)
	{
		try{
				if($type=='planned' || $type='completed')
					$this->ecademy->where('examtype','planned');

				$this->ecademy->where('forclassid',$classid);

				$this->ecademy->from('online_exam.apptestsetup stup');

				if($type=='planned')
					$this->ecademy->where("concat(from_date,' ',start_time)::timestamp + (60 * interval '1 minute' ) >= current_timestamp",false,false);

				$this->ecademy->join("(select distinct classid,classname,subjectname,subjectid from online_exam.class_subject_chapter) csc",
										"csc.subjectid=stup.forsubjectid");
				$data = $this->ecademy->get()->result();

				return $data;

		}catch(Exception $e){
			throw $e;
		}
	}
		 
  public function ListExamSubjects($cond=false)
  {
	  if($cond)
		  $this->ecademy->where($cond);
	  
	 $data = $this->ecademy->select("organizationid as schoolid,subjectid,subjectname,classid,classname,examtype,organizationname as schoolname,concat(from_date,' ',start_time) exam_time")
			->from("(select * from (
select row_number() over(partition by examtype,forsubjectid,forschoolid order by from_date desc) sno,examtype,forsubjectid,forschoolid,from_date,start_time
from online_exam.apptestsetup where isactive='Y' ) as exdata where sno = 1) ats")
			->join("(select distinct classid,subjectid,subjectname,classname from online_exam.class_subject_chapter) csc","csc.subjectid=ats.forsubjectid")
			->join("(select distinct organizationid,organizationname from public.org_organization where ismyclass='Y') org","org.organizationid = ats.forschoolid")
			->get()->result();

		//echo $this->ecademy->last_query();
	  return $data;

  }
  
  function get_classid($subjectid)
		{
			$data = $this->ecademy->select('classid')->from('online_exam.class_subject_chapter')->where('subjectid',$subjectid)->get()->row();
			if(empty($data))
				return -100;
			return $data->classid;
		}
	
	
	function get_individual_result($examtype,$studentid)
	{
		
		$sql ="select distinct ae.start_date::date,atc.subjectid,ae.examtype 
				from online_exam.aptitude_test_class atc 
					join online_exam.appt_exam ae using(aptitudeid)
				where studentid='$studentid'	
				and examtype='$examtype' order by ae.start_date::date desc limit 100";
		
		$setups = $this->ecademy->query($sql)->result();
		$exdata = array();
		foreach($setups as $s)
		{
			$d = date('Y_m_d',strtotime($s->start_date));
			$dt = date('Y-m-d',strtotime($s->start_date));
			$exdata[$d] = $this->get_rank($s->subjectid,$s->examtype,$dt,$studentid);
		}
		return $exdata;
	}
	
	function get_rank($subjectid,$examtype,$start_date=false,$studentid=false)
	{
			/*$sql= select *, ROW_NUMBER() over(partition by setupid order by correct_ans desc, coalesce(timetaken,0) asc) as rnk		
					from (
								select count(*) over() total_students,studentid,student_name,parent_mobileno,schoolname,districtname,classname,subjectname,
										ae.setupid,ae.start_date,ae.completed_on,sum(timemins) total_time,
										count(examdtl_id) filter (where iscorrect='t') correct_ans, 
										extract(epoch from max(aed.completed_on) -  min(aed.started_on)) timetaken,ae.examtype,
										max(aed.completed_on) -  min(aed.started_on) timetaken_format,
										total_question
								from online_exam.appt_exam ae
								join online_exam.appt_exam_detail aed using(examid)
								join online_exam.aptitude_test_class atc using(aptitudeid)
								join (select distinct classid,classname,subjectid,subjectname from online_exam.class_subject_chapter) csc using(subjectid)
								join (select organizationid schoolid,organizationname schoolname from public.org_organization where ismyclass='Y') org using(schoolid)
								join public.district using(districtid) 
								where ae.setupid = '$setupid'
								group by ae.setupid,student_name,parent_mobileno,ae.examid,studentid,classname,subjectname,schoolname,start_date::date,districtname
					) A"; */
					
			$sql = "select *, ROW_NUMBER() over(partition by start_date::date order by correct_ans desc, coalesce(timetaken,0) asc) as rnk		
					from (
						select count(*) over() total_students,studentid,student_name,parent_mobileno,schoolname,districtname,classname,subjectname,
								ae.setupid,ae.start_date,ae.completed_on,sum(timemins) total_time,
								count(examdtl_id) filter (where iscorrect='t') correct_ans, 
								extract(epoch from max(aed.completed_on) -  min(aed.started_on)) timetaken,
								ae.examtype,
								max(aed.completed_on) -  min(aed.started_on) timetaken_format,
								total_question
						from online_exam.appt_exam ae
						join online_exam.appt_exam_detail aed using(examid)
						join online_exam.aptitude_test_class atc using(aptitudeid)
						join (select distinct classid,classname,subjectid,subjectname from online_exam.class_subject_chapter) csc using(subjectid)
						join (select organizationid schoolid,organizationname schoolname from public.org_organization where ismyclass='Y') org using(schoolid)
						join public.district using(districtid) 
						where ae.setupid in ( 
								select setupid from online_exam.apptestsetup where 
								examtype='$examtype' and forsubjectid=$subjectid )
							and start_date::date = '$start_date'::date	
						group by ae.setupid,student_name,parent_mobileno,ae.examid,studentid,classname,subjectname,schoolname,start_date::date,districtname
						having ae.completed_on is not null
			) A order by start_date::date desc,rnk asc";		
					
		if($studentid)
		{
			$sql = "select * from ($sql) B where studentid='$studentid'";
		}
		
		//echo "select * from ($sql) B where rnk<=5 "; 
		
		return $this->ecademy->query("select * from ($sql) B where rnk<=5 ")->result();	
	}
	
	function get_exam_dates($subjectid,$examtype,$limit=5,$offset=0)
	{
		$sql ="select distinct ae.start_date::date 
				from online_exam.aptitude_test_class atc 
				join online_exam.appt_exam ae using(aptitudeid) 
				where atc.subjectid='$subjectid' and examtype='$examtype'
				order by ae.start_date::date desc";
		$return = $this->ecademy->query($sql)->result();
		return $return;
	}

	function get_exam_by_id($examid)
	{
		$sql = "select * from online_exam.appt_exam where examid='$examid'";
		$return = $this->ecademy->query($sql)->result();
		
		return element('0',$return);
	}
	
	function get_chapter_list($subjectid=false,$aptitudeid=false,$orgid='10000002')
	{
		try{
			if(!$subjectid && !$aptitudeid)
				throw new Exception("Required Parameter Missing");
			
			if(!$subjectid)
			{
				$data = $this->ecademy->select("subjectid")->from("online_exam.aptitude_test_class")->where('aptitudeid',$aptitudeid)->get()->row();
				//echo $this->ecademy->last_query();
				
				if(!$data)
					throw new Exception("Id not yet registered");
				
					$subjectid=$data->subjectid;
			}
			
			if(!$subjectid)
				throw new Exception("Subject Not Found");
			
			$ed = $this->get_exam_in_progress($aptitudeid,'self',$orgid);
			$ed = element('0',$ed);	
			if($ed && empty($ed->completed_on))
			{
				redirect('/aptitude/aptitude_register/get_question/'.$ed->examid);
			}
			$data = $this->ecademy->select("chapterid,chaptername,subjectname,classname")->from("online_exam.class_subject_chapter")
									->where('subjectid',$subjectid)->not_like('chaptername','Notice board')
									->where('isactivechapter','Y')
									->not_like('chaptername','Specification Grid')->order_by('chapterorder','asc')
									->get()->result();
			return $data;
		}catch(Exception $e)
			{
				throw $e;
			}
	}
	
	function populate_exam_for_self($chapterid=array(),$aptitudeid=false,$orgid=false)
	{
		try{
			 $this->db->trans_begin();
		if(!array($chapterid) || count($chapterid)<3 || count($chapterid)>10 || empty($aptitudeid))
			throw new Exception("Wrong Parameters");
		
		$chcount = count($chapterid);
		switch($chcount)
		{
			case 3 : 
				$lvls = array('easy'=>8,'medium'=>5,'hard'=>3);
				break;
			case 4 :
				$lvls = array('easy'=>6,'medium'=>4,'hard'=>2);
				break;
			case 5: 
				$lvls = array('easy'=>5,'medium'=>3,'hard'=>2);
				break;
			case 6: 
				$lvls = array('easy'=>4,'medium'=>3,'hard'=>2);
				break;
			case 7:
				$lvls = array('easy'=>3,'medium'=>2,'hard'=>2);
				break;
			case 8:
				$lvls = array('easy'=>3,'medium'=>2,'hard'=>1);
				break;
			case 9: 
				$lvls = array('easy'=>3,'medium'=>2,'hard'=>1);
				break;
			case 10: 
				$lvls = array('easy'=>2,'medium'=>2,'hard'=>1);
				break;
		}
		 
		$sql='';
		foreach($chapterid as $c)
				{
					foreach($lvls as $k=>$v)
					{
						if($v>0)
						{
							$sql.="(select quizcontentid as quiz_contentid,chapterid,quiztype qtype,level,timemins,marks from el_content_quiz where 
								for_apptitude_test='t' and level='$k' and chapterid=$c order by random() limit $v ) UNION ";
						}
					}				
				} 

			   $sql = trim($sql,"UNION ");
				$sql  = "select row_number() over() as qorder,count(*) over() total_questions,sum(marks) over() total_marks,* from (select * from ( $sql ) A order by qtype,random()) b";
				$result = $this->db->query($sql)->result();

				$r = element('0',$result);
				
				if(!$r)
					throw new Exception("Exam Questions Not Found");
				
				$exam_data = array('aptitudeid'=>$aptitudeid,'setupid'=>'-1',
								  'schoolid'=>$orgid,'examtype'=>'self',
								  'total_question'=>$r->total_questions,'total_marks'=>$r->total_marks,
								  'exam_name'=>"Self Assessment");					
				
				$dt = $this->ecademy->insert('online_exam.appt_exam',$exam_data);

				if(!$dt)
					throw new Exception("Something Went Wrong");
				
				if($this->ecademy->_error_message()!='')
					throw new Exception("Something Went Wrong");
	
				$examid = $this->ecademy->insert_id();
				
				if(!$examid)
					throw new Exception("Error Inserting Questions");
				
				$exam_ins = array();
				foreach($result as $r)
				{
					$d = array('examid'=>$examid,'quiz_contentid'=>$r->quiz_contentid,
					           'qorder'=>$r->qorder,'timemins'=>$r->timemins,'marks'=>$r->marks,
									'qtype'=>$r->qtype, 'level'=>$r->level );
					$exam_ins[] = $d;
				}
			
				$this->ecademy->insert_batch("online_exam.appt_exam_detail",$exam_ins);
				if($this->ecademy->_error_message()!='')
						throw new Exception("Something Went Wrong");
			$this->db->trans_commit();
				
				return $examid;
				
		}catch(Exception $e)
			{
				$this->db->trans_commit();
				throw $e;
			}
	}
	
	function get_rules($setupid)
	{
		if(!empty(trim($setupid)))
		{
			$row = $this->ecademy->get_where('online_exam.appt_examrules',array('setupid'=>$setupid))->row();
			return $row;
		}
	}
	
	function autocomplete_oldexams()
	{
		$sql = "select examid from online_exam.appt_exam
					where examtype in ('planned_quiz','unplanned_quiz','planned')
				and completed_on is null and start_date::date <= current_date";
		$pexams = $this->ecademy->query($sql)->result();
		if(count($pexams)>0)
			foreach($pexams as $p)
			{
				$sql="update online_exam.appt_exam 
					  set completed_on = (select 
										case when max(ed.completed_on) is not null then max(ed.completed_on)
											when max(ed.started_on) is not null then max(ed.started_on)
											else max(e.start_date) + (sum(timemins) * interval '1 minute') 
										end as completed_on 
									from online_exam.appt_exam_detail ed
									join online_exam.appt_exam e using(examid)	
										where examid=$p->examid
									)
					 where start_date::date < current_date and examtype in ('planned_quiz','unplanned_quiz','planned') and examid=$p->examid";
			
				$this->ecademy->query($sql);
			}
		
	}
	
}