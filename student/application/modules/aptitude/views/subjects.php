<style>
    table tr th{
        font-weight:bold;
    }
    th{
        background-color:#86bace;
        color: #fff;
    }
    .table-hover > tbody > tr:hover {
        background-color: #D2D2D2 !important;
    }
    .table-striped tbody tr.selected td {
        background-color: #4fb7cb !important;
        font-weight: bold;
        color: #fff;
    }
    tr.active {
        color: #666;
    }
    .table>tbody>tr.active>td, .table>tbody>tr.active>th, .table>tbody>tr>td.active, .table>tbody>tr>th.active, .table>tfoot>tr.active>td, .table>tfoot>tr.active>th, .table>tfoot>tr>td.active, .table>tfoot>tr>th.active, .table>thead>tr.active>td, .table>thead>tr.active>th, .table>thead>tr>td.active, .table>thead>tr>th.active {
        background-color: #fff;
    }
    .table-striped>tbody>tr:nth-of-type(odd) {
        background-color: #f9f9f9;
    }

    table.dataTable.display tbody tr td {
        padding: 2px 6px;
    }

    .btn-primary {
        padding: 1px 8px;
        font-size: 14px;
    }

    .btn-success {
        padding: 1px 8px;
        font-size: 14px;
    }

    .dataTables_wrapper .dataTables_filter {
        float: right;
        text-align: left;
    }

    button, input, optgroup, select, textarea {
        text-align: left;
    }

    
    

</style>
<body>
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-light bg-white">
            <a class="navbar-brand" href="<?=base_url();?>">
                <img src="<?=base_url('assets/eacademy/images/logo.png');?>" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                    <li class="nav-item ">
                        <a class="nav-link" href="<?=base_url();?>">Home</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="<?= base_url('faqtour');?>">About eCADEMY <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item ">
                     <a class="nav-link" href="<?=base_url();?>applicationform">Application Form</a>
                  </li>
                    <li class="nav-item ">
                       <a class="nav-link contact" href="javascript:void(0)" >Contact Us</a>
                   </li>


                </ul>
            </div>
        </nav>
    </div>
    <section class="banner" style="padding-top: 8%;">

        <div class="container pt">
            <h3 class="text-center"> Aptitude Test Subject Wise URL(s) </h3>
            <table id="table_id" class="display table table-bordered table-striped" style="font-size: 14px;">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th>School</th>
                        <th>Class</th>
                        <th>Subject</th>
                        <th>Subject</th>
                        <th>From Date</th>
                        <th>Open Url</th>
                        <th>Copy Url</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($subjects as $key => $sub) {?>
                    <tr>
                        <td><?=$key+1;?></td>
						<td><?=ucfirst($sub->schoolname);?></td>
						<td><?=ucfirst($sub->classname);?></td>
                        <td><?= $sub->subjectname;?></td>
                        <td><?=ucfirst($sub->examtype);?></td>
                        <td><?=ucfirst($sub->exam_time);?></td>
						
                        <td><a class="btn btn-success" href="<?php echo base_url('/aptitude/aptitude_register/index/'.$sub->subjectid.'/'.$sub->examtype.'/'.$sub->schoolid.'/'.$sub->classid);?>" target="_blank" >Open</a></td>
                        <td><a href="javascript:void(0)" data-url="<?php echo base_url('/aptitude/aptitude_register/index/'.$sub->subjectid.'/'.$sub->examtype.'/'.$sub->schoolid.'/'.$sub->classid);?>" class="btn btn-primary link">Copy</a></td>
                    </tr>
                <?php  } ?>
                    
                </tbody>
            </table>
        </div>
    </section>


    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">

    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>

    <script>
        $(document).ready( function () {
            $('#table_id').DataTable({
                "pageLength": 25
            });
        } );

        $('.link').click(function (e) {
           e.preventDefault();
           var copyText = $(this).data('url');

           document.addEventListener('copy', function(e) {
              e.clipboardData.setData('text/plain', copyText);
              e.preventDefault();
          }, true);

           document.execCommand('copy');  
           alert('copied to clipboard'); 
        });

    </script>