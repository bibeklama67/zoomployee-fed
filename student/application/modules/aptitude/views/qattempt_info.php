		<?php 
						if(!empty($data->attempted))
							$attempted = explode(',',preg_replace('/({|})/','',$data->attempted));
						else
							$attempted = array();
					
						if(!empty($data->skipped))
							$skipped = explode(',',preg_replace('/({|})/','',$data->skipped));
						else
							$skipped = array();					
			?>
			<div class="h4 text-uppercase my-2 mb-3"> Questions </div>
			<div class="exam_details">
				<?php for($i=1;$i<=$data->total_questions;$i++): 
							if(in_array($i,$attempted))
								$cls = "numtab bg-success";
							else if(in_array($i,$skipped))
								$cls ="numtab bg-danger";
							else
								$cls = "numtab";
				?>	
						<div class="<?=$cls?> h5 text-white" data-value="<?=$i?>"><?=$i?></div>
				<?php endfor; ?>
			</div>
			<div style="clear:both"></div>
			<hr>
			<?php 
					 $at = count( $attempted ); 
					 $sk = count( $skipped ); 
					 $tat = $at + $sk;
					 $nv = $data->total_questions - $tat;
			?>
			<div>
			<div class="btn btn-info mr-3 mb-3">
			Not Visited <span class="ml-1 badge badge-light"><?=$nv?></span>
		
			</div>
			<div class="btn btn-success mr-3 mb-3">
			Attempted <span class="ml-1 badge badge-light"><?=$at?></span>
			</div>
			<div class="btn btn-danger mr-3 mb-3">
			Skipped <span class="ml-1 badge badge-light"><?=$sk?></span>
			</div>
			</div>
			