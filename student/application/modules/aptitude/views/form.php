
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700&display=swap" rel="stylesheet">
<body>
   
        <nav class="navbar navbar-expand-lg navbar-light bg-white border-bottom">
            <div class="container">
            <a class="navbar-brand" href="<?=base_url();?>">
                <img src="<?=base_url('assets/eacademy/images/logo.png');?>" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                    <li class="nav-item ">
                        <a class="nav-link" href="<?=base_url();?>">Home</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="<?= base_url('faqtour');?>">About eCADEMY <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item ">
                     <a class="nav-link" href="<?=base_url();?>applicationform">Application Form</a>
                  </li>
                    <li class="nav-item ">
                       <a class="nav-link contact" href="javascript:void(0)" >Contact Us</a>
                   </li>
                </ul>
            </div>
               </div>
        </nav>

    <section class="ampitude-form-wrapper position-relative">
        <div class="container py-5 ">

      <div class="row align-items-center vh-100">
            <div class="col-lg-6 align-self-start pt">



        <div class="row">
        <div class="col" style="text-align: center;margin-top: 6px;">
        <span id="" style="color:red"><?= (isset($error_message)?$error_message:'');?></span>
        </div>
        </div>


        <form id="aptitude_from" method="post" >
        
        
        <h3 class="text-left title mb-5"> Please Register for the Aptitude Test. </h3>

        <div class="mb-4 one">

        <input type="text" class="form-control" name="student_name" id="student_name" placeholder="Student Name" required>
        <div class="error_class"></div>
        </div>  <!-- End of Division-->

        <div class="mb-4 one">

        <input type="number" class="form-control" name="parent_mobileno" id="parent_mobileno" placeholder="Parent Mobile Number" pattern="\d{3}[\-]\d{3}[\-]\d{4}" required>
        <div class="error_class"></div>
        </div>  <!-- End of Division-->

        <div class="mb-4 one">

        <input type="text" class="form-control" name="parent_name" id="parent_name"         placeholder="Parent Name" required>
        <div class="error_class"></div>
        </div> <!-- End of Division-->

        <div class="mb-4 one">

        <select class="form-control" id="district" name="districtid" required>
        <option value="-1">Select District</option>
        <?php foreach ($district as $list) : ?>
        <option value="<?= $list->districtid; ?>"><?= $list->districtname; ?></option>

        <?php endforeach; ?>
        </select>
        <div class="error_class"></div>
        </div> <!-- End of Division-->

        <div class="mb-4 one">

        <input type="text" class="form-control" name="vdc" id="vdc" placeholder="Municipality/V.D.C">
        <div class="error_class"></div>
        </div> <!-- End of Division-->

        <div class="mb-4 one">

        <input type="text" class="form-control" name="address" id="address" placeholder="Address">
        <div class="error_class"></div>
        </div> <!-- End of Division-->

        <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 two">
        <label class="lable_class">Class</label>
        <?php $ssel=false; $sel=false; $cls=array('-1'=>'Select Class'); $subs=array('-1'=>'Select Subject');
        foreach($eclass as $key=>$val): ?>
        <?php $cls[$val->classid]=$val->classname; 
        $subs[$val->subjectid]=$val->subjectname;

        if($val->subjectid==$subject){ 
        $sel=$val->classid; $ssel=$val->subjectid; 
        } 
        ?>
        <?php endforeach; ?>
        <?=form_dropdown('classid',$cls,$sel,'class="form-control" id="eclass" required="required"'); ?>
        <!-- select class="form-control" id="eclass" name="classid" required>
        <option value="-1">Select Class</option>
        <?php foreach ($eclass as $key => $elist) : ?>
        <option value="<?= $elist->classid; ?>"><?= $elist->classname; ?></option>
        <?php endforeach; ?>
        </select -->
        <div class="error_class"></div>	
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 order-sm-2 two" id="subdiv">
        <label class="lable_class">Subject</label>
        <?=form_dropdown('subjectid',$subs,$ssel,"class='form-control' id='subject' required='required'")?>
        <!-- select class="form-control" id="subject" name="subjectid" required>
        <option value="-1">Select Subject</option>
        </select -->
        <div class="error_class"></div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 order-sm-2 two">
        <label class="lable_class" id="school_name"></label>
        <input type="text" class="form-control" name="school_name" id="school" placeholder="" required>
        <div class="error_class"></div>
        </div>
        </div>

         <div>
             <span id="err" style="color:red"></span>
        <p id="success" style="color:green"></p>
       
        </div>
        <div class="col-md-12 two">
        <button type="submit" class="btn btn-primary" id="btnsubmit" data-type="next" style="float:right;">Submit</button>
        </div>
        <div class=" one text-right">
        <button type="button" class="btn btn-primary" id="btnnext" data-type="next" >Next <em class="fa fa-angle-right"></em></button>
        </div>
  



       
        <div class="row">
        <div class="col-md-6">
        <img src="" class="flexdesign-left" />
        </div>
        </div>
        </form>

        </div>
        <div class="col-lg-6">
        <div class="register-form-right-bar ">
        <img class="img-fluid" src="<?=base_url('assets/eacademy/images/ampitude-register-bg.svg');?>" alt="">
        </div>
        </div>
      </div>


        </div>
    </section>
  
    

  

    <script>
		var count = 1;
        $(document).ready(function(){
            $('#school_name').text('School Name');
            $('#school').attr('placeholder','School Name');
			
			$('.two').hide();
			$('.one').show();
        })
		
		$('#btnnext').on('click',function(){
			
			var parent_mobileno = $('#parent_mobileno').val();
           if ($('#student_name').val() == '') {
                $('#err').html('Please Enter Student Name.');
                return false;
			}else if (parent_mobileno.length != 10) {
                $('#err').html('Mobile Number must be of 10 digits.');
                return false;
            }else if($('#parent_name').val() == '')
				{
					$('#err').html('Please Provide Parent Name');
					return false;
				}else if ($('#district').val() == '-1') {
					$('#err').html('Please Select District');
					return false;
				}else if($('#vdc').val()==''){
					$('#err').html('Please Provide Vdc/Municipality');
					return false;
				}else{
					$('#err').html('');
				}
			
			$('.one').hide();
			$('.two').show();
		
		});

        $('#eclass').change(function(e) {
            let eclassid = $(this).val();
            let subject_text = $( "#subject option:selected" ).text().toLowerCase();
            if (eclassid == '-1') {
                $('.subject').empty();
                $('#err').html('Please Select Class');
                return false;
            }
            if (eclassid == '116' || eclassid == '122' || eclassid == '124' || eclassid == '125' || eclassid == '118' || eclassid == '119' || eclassid == '120' || eclassid == '121' || eclassid == '126') {
                $('#school').attr('placeholder', 'College Name');
                $('#school_name').text('College Name');        
            } else {
                $('#school').attr('placeholder', 'School Name');
                $('#school_name').text('School Name');
            }
			
			$('#subdiv').load('/aptitude/aptitude_register/aptitude_test_class_getsubject',{classid:eclassid},
							function(data){
									
							});
			

        });

	/*
	            $.ajax({
                type: 'post',
                url: "<?php echo base_url('aptitude_test_class_getsubject') ?>",
                data: {
                    eclassid
                },
                success: function(response) {
                    var res = jQuery.parseJSON(response);
                    var subidlist = [147, 175, 79, 80, 133, 164, 107, 174, 81, 82, 134, 149, 106, 173, 83, 84, 135, 150, 178, 183, 259, 260, 514, 516, 517, 179, 177, 508, 518, 227, 228, 230, 239, 522, 180, 181, 523, 524, 526, 527, 528, 529, 231, 233, 236, 240, 519, 520, 521, 530, 531, 542, 543, 544, 547, 548, 532, 533, 534, 535, 536, 537, 538, 539, 540, 549, 550, 551, 552, 111, 112, 113, 115, 114, 116, 117, 243, 255, 248, 252, 256, 257, 251, 250, 254, 247, 249, 45, 95, 49, 48, 47, 130, 46, 52, 53, 54, 541, 51, 50, 131, 94, 353, 96, 399, 55, 56, 58, 57, 357, 102, 122, 389, 216, 397, 371, 401, 414, 479, 509, 419, 447, 488, 490, 97, 461, 59, 60, 62, 61, 367, 103, 143, 390, 215, 374, 415, 420, 446, 473, 487, 491, 98, 468, 63, 65, 66, 64, 394, 142, 139, 217, 377, 413, 416, 436, 474, 486, 492, 99, 444, 67, 68, 69, 70, 388, 108, 138, 392, 218, 417, 422, 445, 477, 485, 493, 100, 443, 71, 72, 137, 73, 109, 418, 400, 74, 475, 442, 423, 484, 219, 382, 494, 440, 176, 75, 76, 171, 121, 185, 101, 165, 476, 424, 431, 426, 483, 441, 510, 495, 502, 439, 437, 77, 78, 172, 132, 146, 425, 168, 478, 503, 432, 427, 438, 210, 496, 511
                    ];

                    var fourToSeven = ["SCIENCE AND ENVIRONMENT", "MATHEMATICS", "SOCIAL STUDIES AND POPULATION", "ENGLISH GRAMMER", "नेपाली व्याकरण", "COMPUTER SCIENCE", "ENGLISH GRAMMAR", "SOCIAL STUDIES", "SOCIAL STUDIES & POPULATION"];
                    var eightSubjects = ["ENGLISH", "नेपाली", "MATHEMATICS", "SCIENCE AND ENVIRONMENT", "SOCIAL STUDIES AND POPULATION", "COMPUTER SCIENCE"];

                    if (res.type == 'success') {
                        let html = '<option value="-1">Select Subject</option>';
                        var subject = '<?php echo strtolower($subject);?>';

                        $(res.response).each(function(index, value) {
                            // if (subidlist.indexOf(parseInt(value.subjectid)) > -1) {

                                if($.trim(subject_text) == value.subjectname.toLowerCase()){
                                    var selected = 'selected="selected"';
                                }else if(subject == value.subjectname.toLowerCase()){
                                    var selected = 'selected="selected"';
                                }else{
                                    var selected = '';
                                }
								
								html += '<option value="' + value.subjectid + '"'+ selected +' >' + ' ' + value.subjectname + '</option>';
                        });
                        $('#subject').empty();
                        $('#subject').html(html);
                    }
                }
            });
		*/
		
		$('.pt').on('click','button#startexam',function(){
				var aptitudeid = $(this).data('aptitudeid');
				$('.pt').load('/aptitude/aptitude_register/startexam',function(data){});
		});

        $('#aptitude_from').on('submit', function(e) 
		{
            e.preventDefault();
			e.stopPropagation();
            var parent_mobileno = $('#parent_mobileno').val();
			
            if ($('#eclass').val() == '-1') {
                $('#err').html('Please select Class');
                return false;
            }else if ($('#subject').val() == '-1') {
                $('#err').html('Please select Subjcet');
                return false;
            }else if (parent_mobileno.length != 10) {
                $('#err').html('Mobile Number must be of 10 digits.');
                return false;
            }else if ($('#student_name').val() == '') {
                $('#err').html('Please Enter Student Name.');
                return false;
            }else if ($('#school').val() == '') {
                $('#err').html('Please Enter School/College Name.');
                return false;
            } else if ($('#district').val() == '-1') {
                $('#err').html('Please Select District');
                return false;
            }

            //$('#btnsubmit').attr('disabled',true);
			
			let fdata = $(this).serializeArray();		
			$.post('/aptitude/aptitude_register/aptitude_test_class_submit',fdata,function(data)
			{
					if(data.type=='success')
					{
							//get_past_exams    get_exam_in_progress
							$.post('/aptitude/aptitude_register/get_past_exams',{aptitudeid:data.aptitudeid},
									function(data){
										$('div.pt').append(data);
										$('form *').attr('disabled',true).slideUp('slow');
										$('div.pt h2').html("You are Registered For the Apptitude Test."); 
									}
							);
					}else{
						alert("Registration Failed. Please Try Again Later");
						$('#err').html(data.message);
					}
				$('#btnsubmit').attr('disabled',false);
			},'json');
						
			/*
            $.ajax({
                type: 'post',
                url: "<?php echo base_url('aptitude-test-class-submit') ?>",
                data: $('#aptitude_from').serializeArray(),
                success: function(response) {
                    var res = jQuery.parseJSON(response);
                    $('#btnsubmit').removeAttr('disabled');
                    $('#err').html('');
                    $('#success').text('');
                    if (res.type == 'success') {
                        $('#success').text('Operation Successful');
                    } else {
                        $('#err').html(res.message);
                        return false;
                    }
                }
            });
			*/
        });
    </script>

    <style>
    .lable_class{
        font-size: 14px;
        font-weight: bold;
    }
    .error_class{
        color: red;
        padding: 6px 0px;
    }
    .card-title{
        font-family: 'Lato', sans-serif;
    }
    option{
        width: 250px;
        min-height:200px;
    }
</style>