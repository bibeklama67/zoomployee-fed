<style>
	 .cls1 div{
			background-image: linear-gradient(to right,#FFC53C, #FC8A32);
			padding:3px;
			text-align:center;
			font-weight:bold;
		}
		
	.cls1{
			border:1px solid #FC8A32; border-radius:5px;margin-top:10px;	
		}
		
	.cls2 div{
			background-image: linear-gradient(to right,#B6B0FC, #7C75DD);
			padding:3px;
			text-align:center;
			font-weight:bold;
		}
		
	.cls2{
			border:1px solid #7C75DD; border-radius:5px;margin-top:10px;	
		}

	.cls3 div{
			background-image: linear-gradient(to right,#67E591, #43B469);
			padding:3px;
			text-align:center;
			font-weight:bold;
		}
		
	.cls3{
			border:1px solid #43B469; border-radius:5px;margin-top:10px;	
		}		
		
	.cls4 div{
			background-image: linear-gradient(to right,#74CFFE, #29ACEF);
			padding:3px;
			text-align:center;
			font-weight:bold;
		}
		
	.cls4{
			border:1px solid #29ACEF; border-radius:5px;margin-top:10px;	
		}

		
	table.res td, table.res th{ border-top:none; padding:2px 10px; }
	
	body{ background-color:#F5F8FF !important; }
		
	table.top-students-list th,table.top-students-list td{ border-top:none; padding:0.05rem 0.75rem; }
</style>
<?php 
	$i=1;
	foreach($sdata as $data):

		if($i<=4)
			$class="cls$i";
		if($i>4 && $i%4==0)
			$class="cls1";
		if($i>4 && $i%4==1)
			$class="cls2";
		if($i>4 && $i%4==2)
			$class="cls3";
		if($i>4 && $i%4==3)
			$class="cls4";		
		$stu = current($data); 
		if(!$stu) continue;
		$i++; ?>
<div style="margin:10px">	
	 <div class="<?=$class?>">
		<div><?=date('Y-m-d',strtotime($stu->start_date))?></div>
		<table class="table  table-stacked top-students-list">
			<tbody class="stacked__head">
				<?php $j=1; foreach($data as $d):
						$j++;
				?>
					<tr>
						<td style="padding-top:6px"> <b>Rank : </b> <?=$d->rnk?> out of <?=$d->total_students?> </td> 
						<td style="padding-top:6px" align="right"><b> Time Taken: </b> <?php 
									$dt = explode(':',date("H:i:s",strtotime($d->timetaken_format)));
									if($dt[0]=='00')
									{	
										$ind=0; foreach($dt as $dp)
										{
											if($dp!='00')
											{
												echo ltrim($dp,'0');
												if($ind==1)
													echo ' min ';
												if($ind==2)
													echo ' sec';
											}
											$ind++;
										}
										
									}else{
										echo implode(':',$dt); 
									}
								?>	
						</td>
						</tr>
					<tr>
						<td colspan="2"><b>Correct Answer: </b> <?=$d->correct_ans?> out of <?=$d->total_question?> </td>
					</tr>	
					<tr>
					<td colspan="2"><b><?=$d->student_name?> (<?=$d->classname?>)</b></td>
					</tr>
					<tr>
						<td colspan="2" style="<?=(count($data)<$j)?'padding-bottom:6px;':'border-bottom:1px solid #dedede;padding-bottom:6px;'?>"><b><?=$d->schoolname?> (<?=$d->districtname?>)</b>
					</td>
					</tr>		
				<?php endforeach ?>
			</tbody>
		</table>
	</div>
</div>
<?php endforeach; ?>
<style>
	thead td{ font-weight:bold; }


	</style>