<?php 
$s = current($setupdata); 
if($s):
?><div id="questdiv" class="col-md-12 p-lg-5 p-3 pt-3" style="border:0px solid #bbb">
	<h4 style="font-family:lato;" class="font-weight-bold mb-4 text-center">
		<?php if(isset($fromajax)): ?>
			Upcoming <?=$s->examtype_full?> Exams
		<?php else: ?>
		Your is <?=$s->examtype_full?> exam is scheduled for:
		<?php endif; ?>
	</h4> 

<style>
		<?php if(!isset($fromajax)) : ?>
		 .cls1 div{
				background-image: linear-gradient(to right,#FFC53C, #FC8A32);
				padding:3px;
				text-align:center;
				font-weight:bold;
			}
			
		.cls1{
				border:1px solid #FC8A32; border-radius:5px;margin-top:10px;	
			}
			
		.cls2 div{
				background-image: linear-gradient(to right,#B6B0FC, #7C75DD);
				padding:10px;
				text-align:center;
				font-weight:bold;
			}
			
		.cls2{
				border:1px solid #7C75DD; border-radius:5px;margin-top:10px;	
			}
		<?php endif; ?>
		.cls3 div{
				background-image: linear-gradient(to right,#67E591, #43B469);
				padding:3px;
				text-align:center;
				font-weight:bold;
			}
			
		.cls3{
				border:1px solid #43B469; border-radius:5px;margin-top:10px;	
			}		
			
		.cls4 div{
				background-image: linear-gradient(to right,#74CFFE, #29ACEF);
				padding:3px;
				text-align:center;
				font-weight:bold;
			}
			
		.cls4{
				border:1px solid #29ACEF; border-radius:5px;margin-top:10px;	
			}
			
		table.res td, table.res th{ border-top:none; padding:2px 10px; }
		
		body{ background-color:#F5F8FF !important; }
		div.message{ text-align:center; padding:10px; border-bottom:1px solid #dedede; }
</style>
<?php $i=1; foreach($setupdata as $s): ?>
		<div class="cls<?=$i?>" style="max-width:400px;margin:5px auto">
			<div class="name"> <?=$s->exam_name?> </div>
			<table class="table res"> 
				<tr> <td> <b> Date &amp; Time: </b> </td> <td> <?=$s->from_date?> <?=$s->start_time?> </td> </tr>
			</table>
		</div>
<?php $i++; endforeach; ?>
</div>
<?php endif; ?>