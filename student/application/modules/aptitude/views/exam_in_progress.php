<?php $et = array('planned'=>'Online Exam',
				'aptitude'=>'Aptitude Test',
				'planned_quiz'=>'Olympiad Exam',
				'unplanned_quiz'=>'Daily Quiz',
				'self' => 'Self Assessment');
				
		if(isset($setupdata))	
		{
			$type=$setupdata->examtype;
			$orgid = $setupdata->forschoolid;
		}else if(isset($setupdata)){
			$type = $examtdata->examtype;
			$orgd = $examdata->orgid;
		}
		
?>
<style>
	.cls1 div.name{
				background-image: linear-gradient(to right,#FFC53C, #FC8A32);
				padding:3px;
				padding-bottom:0px;
				text-align:center;
				font-weight:bold;
			}
			
		.cls1{
				border:1px solid #FC8A32; border-radius:5px;margin-top:10px;	
			}
			
		.cls2 div.name{
				background-image: linear-gradient(to right,#B6B0FC, #7C75DD);
				padding:10px;
				text-align:center;
				font-weight:bold;
			}
			
		.cls2{
				border:1px solid #7C75DD; border-radius:5px;margin-top:10px;	
			}

</style>
<?php if($examtime=='early')
{
	   //var_dump($setupdata);
?>
	<div class="container">
		<!-- div class="studata">
			<span> Name : [ <?=$setupdata->student_name?> ] </span> || <span> Class: [ <?=$setupdata->classname?> ] </span> || <span> Subject: [ <?=$setupdata->subjectname?> ] </span>
		</div -->
		<div class="cls1">
			<div class="name"> <?=$setupdata->exam_name?> </div>
		<h6> Your <?=$setupdata->examtype_full?> Exam will start on <?=$setupdata->from_date?> <?=$setupdata->start_time?> <div id="timer">
			<?php if( strtotime(date('Y-m-d'). ' ' . $setupdata->start_time) - strtotime(date('Y-m-d H:i:s')) > 0 )
							echo gmdate('H:i:s',strtotime($setupdata->start_time) - strtotime(date('H:i:s')));
					  else
					  		echo "00:00:00";
				?> 
			</div> 
		</h6>
	
	<?php if(in_array($setupdata->examtype,array('planned','planned_quiz'))): ?>
			<div class="rules">

			</div>
			
		<?php endif; ?>
		</div> 
	<style>
		.studata{ background:maroon; color:white; padding:10px; border:2px solid #000; text-align:center; }
		.studata span { display:inline-block; width:32% !important; }
		h6{ margin-top:20px; text-align:center; } 
		div#timer{ color:maroon; font-size:2em; }
	</style>
	<script>
		$(function(){

			function startTimer()
			{
					var interval = setInterval(function()
					{
							let tm = $('#timer').html();
							let timer = tm.split(':');
							let h = parseInt(timer[0]);
							let m = parseInt(timer[1]);
							let s = parseInt(timer[2]);
							//if( (h==0 && m<=10) ) $('#divclock').removeClass('bg-primary').addClass('bg-danger');
							if ( (h == 0) && (m <= 3)) {
								clearInterval(interval);
								location.reload();
							}
							else{
									h = (m <= 0) ? --h : h;
									m = (s <= 0) ? --m : m;
									m = (m <= 0) ? 59 : m;
									s = (s <= 0) ? 59 : --s;		
							}
							h = (h < 10) ? '0' + h : h;
							s = (s < 10) ? '0' + s : s;
							m = (m < 10) ? '0' + m : m;
							$('#timer').html(h+':'+m+':'+s);							
							//if ( (h == 0) && (s == 0) && (m == 0)) clearInterval(interval);
					
					},1000);
			}
			startTimer();
		});
		
	</script>
	
		<div style="border-top:1px solid #dedede;margin-top:5px;padding:10px" id="upcoming_exams">
			
		</div>
		
  </div>	
<?php
} 
else if($examdata)
	{
?>
			<div class="container">
				<!-- div class="studata">
					<span> Name : [ <?=$setupdata->student_name?> ] </span> || <span> Class: [ <?=$setupdata->classname?> ] </span> || <span> Subject: [ <?=$setupdata->subjectname?> ] </span>
				</div -->
				<?php
						
					$from =  strtotime($examdata->exam_date);
					$to = strtotime("+{$examdata->totalmins} minutes",strtotime($examdata->exam_date));
					if($to <= strtotime(date('Y-m-d H:i:s'))):
						//if(date('Y-m-d') == date('Y-m-d',strtotime($examdata->exam_date))):
							echo "<h6> Sorry Exam is already Over. </h6>";
							exit;
							//Get future Exams
							//redirect("/aptitude/aptitude_register/future_exams/{$examdata->examid}");
							
							//var_dump($examdata);
						//else:
							//var_dump($examdata);
							//redirect("/aptitude/aptitude_register/get_question/{$examdata->examid}");
						//endif;
					else:
				?>
				<div class="cls1">
					<div class="name"> <?=$setupdata->exam_name?> </div>
						<h6> Your <?=$setupdata->examtype_full?>Exam will start on <?=$setupdata->from_date?> <?=$setupdata->start_time?>
							<div id="timer">
								<?php if( strtotime(date('Y-m-d'). ' ' . $setupdata->start_time) - strtotime(date('Y-m-d H:i:s')) > 0 )
										echo gmdate('H:i:s',strtotime($setupdata->start_time) - strtotime(date('H:i:s')));
								  else
										echo "00:00:00";
								?>
							</div>			
						</h6>
						<div class="studata">
							<span> Exam Time : [ <?=date('Y-m-d',strtotime($examdata->exam_date)); ?> 
							 ( <?=date("H:i:s",strtotime($examdata->exam_date))?>  to  <?=date('H:i:s', strtotime("+{$examdata->totalmins} minutes",strtotime($examdata->exam_date))); ?> ) ] </span> || 
							 <span> Exam Time: [ <?=$examdata->totalmins?> minutes ] </span> || 
							<span> Total Questions: [ <?=$examdata->total_question?> ] </span>
						</div>
				<?php if(in_array($examdata->examtype,array('planned','planned_quiz'))): ?>
						<div class="rules"></div>
				
					<style> 
						.rules{ margin:15px 10px; padding:10px; } 
						.rules h1{ text-align:center; }
					</style>
					<script>
						$(function(){
							/*$('#startexam').hide();
							$('div.rules').on("change",'input#iagree',function(){
									alert("Hello");
								if($(this).is(":checked"))
									$('#startexam').show();
								else
									$('#startexam').hide();
							});*/
							
							function startTimer()
							{
								var interval = setInterval(function()
								{
									let tm = $('#timer').html();
									let timer = tm.split(':');
									let h = parseInt(timer[0]);
									let m = parseInt(timer[1]);
									let s = parseInt(timer[2]);
									//if( (h==0 && m<=10) ) $('#divclock').removeClass('bg-primary').addClass('bg-danger');
									if ( (h == 0) && (m <= 0) && (s<=0) ) {
										clearInterval(interval);
										//location.reload();
									}
									else{
											h = (m < 0) ? --h : h;
											m = (s < 0) ? --m : m;
											m = (m <= 0) ? 59 : m;
											s = (s <= 0) ? 59 : --s;		
									}
									h = (h < 10) ? '0' + h : h;
									s = (s < 10) ? '0' + s : s;
									m = (m < 10) ? '0' + m : m;
									$('#timer').html(h+':'+m+':'+s);							
									//if ( (h == 0) && (s == 0) && (m == 0)) clearInterval(interval);
							
								},1000);
							}
							startTimer();
							
							$('#startexam a').on('click',function(e){
								e.preventDefault();
								e.stopPropagation();
								$.get('/aptitude/aptitude_register/verify_time/<?=$examdata->examid?>',function(data){
										if(data.type != 'success')
											alert(data.msg);
										else
											window.location.href= $('#startexam a').attr('href');
								},'json');
							});
							
					});
				</script>
				
				<?php endif; ?>
				
					<div style="text-align:right;margin:20px;" id="startexam">
						<a href="/aptitude/aptitude_register/get_question/<?=$examdata->examid?>"> 
							<button class="btn btn-success"> Start </button>
						</a>
					</div>
				<?php endif; ?>
				</div>
				
				<div style="border-top:1px solid #dedede;margin-top:5px;padding:10px" id="upcoming_exams">
					
				</div>
			</div>
			<style>
				.studata{ background:maroon; color:white; padding:10px; border:2px solid #000; text-align:center; }
				.studata span { display:inline-block; width:32% !important;font-size:0.8em }
				h6{ margin-top:20px; text-align:center; }  
				div#timer{ color:maroon; font-size:2em; }
				
			</style>
<?php 
	} 
	else 
		{
		
			if(count($ed)<=0): 
				$type=isset($type)?$type:false;  $orgid=isset($orgid)?$orgid:false;
				
			if($type && $orgid)
				$pfix = "/$type/$orgid";
			else if($type)
				$pfix ="/$type";
			else
				$pfix = '';
			?>
			
				<div class="container mt-4" style="text-align:center;padding:20px">
					<div class="card shadow">
						<div class="card-header lead">
							<?php if($type=='aptitude' || $type=='self'): ?>
									<div class="row">
										<div class="col-md-3">
											Class: <?=$setupdata->classname?> 
										</div>
										<div class="col-md-5">
											Subject: <?=$setupdata->subjectname?> 
										</div>
										<!-- div class="col-md-3" style="display:none">
											Exam: Self Assesment 
										</div -->
										<div class="col-md-3">
											Name: <?=$studentdata->student_name; ?> 
										</div>
									</div>
							<?php endif; ?>
						</div>
						<div class="rules" style="background:transparent"></div>
						<div class="card-body py-5 bg-light">
							<?php if($type=='self'): ?>
								<a href="/aptitude/aptitude_register/get_chapter_list/<?=$aptitudeid?>/<?=$type?>/<?=$orgid?>">
							<?php else: ?>
								<a href="/aptitude/aptitude_register/startexam/<?=$aptitudeid?><?=$pfix?>">
							<?php endif; ?>
									<button class="btn btn-success btn-lg" data-aptitudeid="<?=$aptitudeid?>" id="startexam" style="padding:12px 30px; border-radius:0"> Start <em class="fa fa-arrow-right ml-2"></em></button>
								</a>
						</div>
						
						<?php  if(in_array($type,array('planned_quiz','unplanned_quiz'))){
									if(is_array($ed) && !empty($ed))
									{
										$e = element('0',$ed);
										if(date('Y-m-d',strtotime($e->start_date)) == date('Y-m-d'))
											redirect("/aptitude/aptitude_register/get_question/$e->examid");
										else
											redirect("/aptitude/aptitude_register/startexam/{$aptitudeid}{$pfix}");
									}else{
										//echo "/aptitude/aptitude_register/startexam/{$aptitudeid}{$pfix}"; exit;
										redirect("/aptitude/aptitude_register/startexam/{$aptitudeid}{$pfix}");
									}
								}else if($type=='self')
								{
									if(empty($ed))
										redirect("/aptitude/aptitude_register/get_chapter_list/$aptitudeid/$type/$orgid");
								}
							?>
					</div>		 
				</div>
				
		<?php else: 
		//bg-primary text-white 
		?>	<style> .tbl{ border:none; margin-bottom:5px; } tr{ } 
					.tbl th, .tbl td{ border:none !important; padding:5px; font-size:1rem; } </style>
			<div class="p-3 py-2 rounded mb-2 text-center" id="prog"><strong class="text-center"> You already have following <?=element($type,$et);?> in progress. <br> Please continue the test. </strong></div>
			<?php $days=0; 
				   $count = 0;
				   $e1 = current($ed);
				$days = daysBetween(date('Y-m-d',strtotime($e1->start_date)),date('Y-m-d'));
				foreach($ed as $e):  								
				if(empty($e->completed_on)): $count++; ?>
					<div class="cls1" style="margin:5px 15px">
						<table class="table table-stack tbl" id="prog">
							<thead class="text-capitalize"> 
							<tr> 
								<th> <span class="d-none d-md-block">class</span>  </th>
								<th> <span class="d-none d-md-block">Subject</span> </th>
								<th> <span class="d-none d-md-block">Started on</span> </th> 
								<th><span class="d-none d-md-block">Last attempted on</span>  </th> 
								<th><span class="d-none d-md-block"></span></th> 
							</tr>
						</thead>
						<tbody>
							<tr>
								<td> <strong class="mr-1 d-inline-block d-md-none">Class:</strong> <?=$e->classname?> </td>
								<td> <strong class="mr-1 d-inline-block d-md-none">Subject:</strong> <?=$e->subjectname?> </td>
								<td> <strong class="mr-1 d-inline-block d-md-none">Started on:</strong> <?=date('Y-m-d H:i:s',strtotime($e->start_date))?></td>
								<td><strong class="mr-1 d-inline-block d-md-none">Last attempted on:</strong> <?=date('Y-m-d H:i:s',strtotime($e->last_updated_on))?></td>
								<td class="text-center">
										<a href="/aptitude/aptitude_register/get_question/<?=$e->examid?>">
										<button class="btn btn-primary btn-md" style="width:50%"> Continue </button>
										</a>
										<!-- button class="btn btn-primary btn-sm" style="padding:2px 10px"> Restart  </button -->
								</td>
								</tr>
						</tbody>
					</table>
					<?php endif; endforeach;
							//exam detail present but not-completed data is not available.
							//so show the result of latest exam.
					?>
						<div style="text-align:right">
							<?php 	$type=isset($type)?$type:false;  
									$orgid=isset($orgid)?$orgid:false;
											if($type && $orgid)
												$pfix = "/$type/$orgid";
											else if($type)
												$pfix ="/$type";
											else
												$pfix = '';
								if(!empty($setupdata->nextattemptdays))
										$edays=$setupdata->nextattemptdays;
								else
										$edays=7;
								
								
										
								if($days > $edays && $type!='self'):			
							?>
							
							<a href="/aptitude/aptitude_register/startexam/<?=$aptitudeid?><?=$pfix?>"> 
										<button class="btn btn-block btn-success">Start New</button>
							</a>
							<?php else: ?>
										<?php if($count<=0): 
												if($type=='self')
													redirect("/aptitude/aptitude_register/get_chapter_list/$aptitudeid/$type/$orgid");
										?>
											<script>
													$('div#prog').html('<h6> You have already taken this test rescently. <BR> <a href="/aptitude/aptitude_register/past_results/<?=$e1->aptitudeid?>" style="color:maroon"> Click Here For Result </a> </h6>');
													$('table#prog thead').hide();
											</script>
										<?php endif; ?>
							<?php endif; ?>
						</div>
					</div>
			<?php  
					if(in_array($type,array('planned_quiz','unplanned_quiz'))){
						if(is_array($ed) && !empty($ed))
						{
							$e = element('0',$ed);
							if(date('Y-m-d',strtotime($e->start_date)) == date('Y-m-d'))
								redirect("/aptitude/aptitude_register/get_question/$e->examid");
							else
								redirect("/aptitude/aptitude_register/startexam/{$aptitudeid}{$pfix}");
						}else{
								redirect("/aptitude/aptitude_register/startexam/{$aptitudeid}{$pfix}");
							}
					}
				?>
<?php endif;  } ?>
<style>
	@media (max-width: 767.98px) { 
		.table-stack thead{
			display:none;
		}
		.table-stack tbody{
			display:block;
		}
		.table-stack td{
			display:block;
		}
	 }
</style>
<script>
	$(function(){
			$('div#upcoming_exams').load("/aptitude/aptitude_register/future_exams/<?=$aptitudeid?>/<?=$type?>/<?=$orgid?>");
			$('div.rules').load("/aptitude/aptitude_register/get_rules/<?=$setupdata->setupid?>",function(data){
				if($.trim(data).length ==0){
					$('#startexam').show();
				}
			});
			$('#startexam').hide();
			$('div.rules').on("change",'input#iagree',function(){
				if($(this).is(":checked"))
					$('#startexam').show();
				else
					$('#startexam').hide();
			});
	});
</script>

<style> 
	.rules div{ margin:15px 10px; padding:10px; background-color:#f8f9fa!important; } 
	.rules div h1{ text-align:center; }
	body { background-color:#f8f9fa!important; }
</style>
<?php 
	function daysBetween($dt1, $dt2) 
	{
		return date_diff(
			date_create($dt2),  
			date_create($dt1)
		)->format('%a');
	}
?>