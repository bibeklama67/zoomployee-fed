<style> 
	body{ background-color:#F5F8FF !important; }
</style>
	<script type="text/javascript">
	
    </script>
<!-- script src="/application/modules/aptitude/views/disable_back.js"></script>
<script>
	history.pushState(null, null, location.href);
    window.onpopstate = function () {
        history.go(1);
    };
    function preventBack() {
        window.history.forward();
    }

    setTimeout("preventBack()", 0);
    window.onunload = function() {
        null
    };
</script -->
    <div class=" p-0 col-sm-12 mb-0">
		<?php if($data->examtype=='aptitude')
		{ ?>
		<div class=" p-3 text-center h5 m-0 col-sm-12 "> 
             <img src="https://www.midasecademy.com/assets/eacademy/images/logo.png" alt="Midas Technology" width="170"/>
		</div>
		<?php } ?>
		
		<?php $et = array('planned'=>'Online Exam',
				'aptitude'=>'Aptitude Test',
				'planned_quiz'=>'Olympiad',
				'unplanned_quiz'=>'Daily Quiz',
				'self' => 'Self Assessment');
		?>	
		
		<?php if($data->examtype=='aptitude') { ?>
		<div class="user-info-row   py-3 text-center h6 m-0 ">
			<ul class="list-group list-group-horizontal-sm">
			  <li class="list-group-item text-white bg-primary"><strong class="text-uppercase p-3"><?=element($data->examtype,$et)?></strong></li>
			   <li class="list-group-item"><strong>Name:</strong> <?=$data->student_name?></li>
			  <li class="list-group-item"> <strong>Class:</strong> <?=$data->classname?></li>
			  <li class="list-group-item"><strong>Subject:</strong> <?=$data->subjectname?></li>	 
			</ul>
		</div>
	<?php } ?>	
	</div>
	
	<div class="row p-0 mx-2">
		<?php if(!in_array($data->examtype,array('planned_quiz','unplanned_quiz'))): 
					$qdivcls = 'col-md-8';
			  else:
					$qdivcls = 'col-md-12';
			  endif;
		?>
		<div id="questdiv" class="<?=$qdivcls?> p-lg-5 p-3 pt-3" style="border:0px solid #bbb">		
		</div>
		<?php if(!in_array($data->examtype,array('planned_quiz','unplanned_quiz'))): ?>
		<div id="examinfo" class="col-md-4 p-4 " style="border:1px solid #bbb; background:#efefef;text-align:center; overflow-y: scroll; height: 600px;">
			 <div class=" h3 mb-3 text-uppercase">Exam Countdown</div>
			<div id="divclock" class="bg-primary text-white h1 mb-3" style="    text-align: center;
    border-radius: 3px;
    padding: 10px;
    font-size: 1.9rem;"> 
				<i class="fa fa-clock"></i> 
					<?php if($data->examtype!='planned') :?>
						<span id="timer"><?=gmdate('H:i:s',($data->remaining_time)*60)?></span> 
					<?php else: ?>
						<span id="timer">
							<?php $end = strtotime("+{$data->total_time} minutes", strtotime($data->exam_date));
								  $now = strtotime(date("Y-m-d H:i:s"));
								  $diff = $end - $now;
								  if($diff > 0 )
								  	{
										echo gmdate("H:i:s",$diff);
									}else{
										echo "00:00:00";
									}
							?>
							</span>
					<?php endif; ?>
				</div>				
			 <div class="mb-3 h5">Total Time of Exam : <?php 
						$tparts = preg_split('/:/',gmdate('H:i:s',($data->total_time)*60)); 
						if($tparts[0]!='00')
							echo $tparts[0] .' Hour'.($tparts[0]>1?'s, ':', ');
						echo $tparts[1] .' Minute'.($tparts[1]>1?'s ':' ');	
			?></div>
			<div style="clear:both"> </div>
			<hr>
			<div id="qattmptdiv">
				<?php $this->load->view('qattempt_info'); ?>
			</div>
		</div>
		<?php endif; ?>
	</div>	
	
	
	
<style>
		.numtab{        float: left;
    border-radius: 0 10px;
    padding: 5px;
    min-width: 40px;
    background: #666;
    margin: 3px;
    font-size: 17px;}
		.legend{ display:table-cell; vertical-align:middle; margin:auto; padding:15px 3px; margin:5px; }
		/* Main styles */
		.user-info-row{
			background:linear-gradient(0deg, #1e3c72 0%, #1e3c72 1%, #2a5298 100%);
			display:flex;
			justify-content: center;
		}
		@media (min-width: 576px) {	
			.opts{
			display:flex;
			flex-wrap:wrap;
		}
	}


	
		.opts li{
		    flex: 0 0 49%;
    padding: 7px 1% 7px 0;
		}
		.opts li input{
			display: inline-block;
			width: 18px;
			height: 18px;
			vertical-align: middle;
			margin-right: 10px;
			flex: 0 0 18px;
			margin-top:3px;

		}
.card-title{
        font-family: 'Lato', sans-serif;
    }
	h5.card-title{
		    line-height: 36px;
    font-size: 21px;
	}

	#examinfo::-webkit-scrollbar {
	width: 10px;
	}

	#examinfo::-webkit-scrollbar-track {
	background: linear-gradient(-20deg, #2b5876 0%, #4e4376 100%);
	}

	#examinfo::-webkit-scrollbar-thumb {
	box-shadow: inset 0 0 40px #fff;
	}

	</style>
<script>
	$(function()
	{
			let url = '/aptitude/aptitude_register/get_question/<?=$data->examid?>';
			$('#questdiv').load(url,{'qonly':'true'},function(){ startTimer(); });
			function startTimer()
			{
					var interval = setInterval(function()
					{
							let tm = $('#timer').html();
							let timer = tm.split(':');
							let h = parseInt(timer[0]);
							let m = parseInt(timer[1]);
							let s = parseInt(timer[2]);
							if( (h==0 && m<=10) ) $('#divclock').removeClass('bg-primary').addClass('bg-danger');
							if ( (h == 0) && (s == 0) && (m == 0)) clearInterval(interval);
							else{
									h = (m <= 0) ? --h : h;
									m = (s <= 0) ? --m : m;
									m = (m <= 0) ? 59 : m;
									s = (s <= 0) ? 59 : --s;		
							}
							h = (h < 10) ? '0' + h : h;
							s = (s < 10) ? '0' + s : s;
							m = (m < 10) ? '0' + m : m;
							$('#timer').html(h+':'+m+':'+s);
							
							//if ( (h == 0) && (s == 0) && (m == 0)) clearInterval(interval);
					
					},1000);
			}
	});
</script>
