<style> 
	.cls1 div.name{
				background-image: linear-gradient(to right,#FFC53C, #FC8A32);
				padding:3px;
				text-align:center;
				font-weight:bold;
			}
			
		.cls1{
				border:1px solid #FC8A32; border-radius:5px;margin-top:10px;	
			}
			
		.cls2 div.name{
				background-image: linear-gradient(to right,#B6B0FC, #7C75DD);
				padding:10px;
				text-align:center;
				font-weight:bold;
			}
			
		.cls2{
				border:1px solid #7C75DD; border-radius:5px;margin-top:10px;	
			}
			
		cls3 div.name{
				background-image: linear-gradient(to right,#67E591, #43B469);
				padding:3px;
				text-align:center;
				font-weight:bold;
			}
			
		.cls3{
				border:1px solid #43B469; border-radius:5px;margin-top:10px;	
			}		
			
		.cls4 div.name{
				background-image: linear-gradient(to right,#74CFFE, #29ACEF);
				padding:3px;
				text-align:center;
				font-weight:bold;
			}
			
		.cls4{
				border:1px solid #29ACEF; border-radius:5px;margin-top:10px;	
			}	
			
		body{ background-color:#F5F8FF !important; }	
</style>

<?php $d=current($data); 
				if(!$d): 
			?> 
	<table width="100%">
			<tr> <th class="center text-white">
				No Results to Show
				</th> 
			</tr>	
	</table>
			<?php else: ?>
	<div class="container">
		<div class="p-1 py-2 rounded mb-2 text-center">
			<h5><?php if($type=='self'): ?>
				Self Assessment 
			<?php else: ?>
				Aptitude Test 
			<?php endif; ?> Result </h5>
		</div>
		<div class="card-header">
			<?php if($type=='aptitude' || $type=='self'): ?>
					<div class="row">
						<div class="col-md-3">
							<strong> Class: </strong> <?=$d->classname?> 
						</div>
						<div class="col-md-5">
							<strong> Subject:</strong> <?=$d->subjectname?> 
						</div>
						<!-- div class="col-md-3" style="display:none">
							Exam: Self Assesment 
						</div -->
						<div class="col-md-3">
							<strong> Name: </strong> <?=$d->student_name; ?> 
						</div>
					</div>
			<?php endif; ?>
		</div>
		
	<?php $i=1; foreach($data as $d): ?>	
		<div class="cls<?=$i?>"> 
			<div class="name"> <?=date('Y-m-d H:i:s',strtotime($d->start_date))?> </div>
			 <div class="row m-0">
			 <div class="col-md-3 m-0 p-0">
				<table class="table table-bordered m-0">
					<tr> <th colspan="4" class="text-center"> Easy </th> </tr>
					<tr> <td class="text-center p-1">Total</td> <td class="text-center p-1">Attempted</td>
						 <td class="text-center p-1">Correct</td> <td class="text-center p-1">%</td> </tr>
					<tr> <td class="text-right  p-1"><?=$d->total_easy?></td><td class="text-right  p-1"><?=$d->attempted_easy?></td>
						 <td class="text-right p-1"><?=$d->correct_easy?></td> 
						 <td class="text-right p-1"><?=(int)($d->correct_easy*100/$d->total_easy)?>%</td> </tr>
				</table>
			 </div>
			 
			 <div class="col-md-3 m-0 p-0">
				<table class="table table-bordered m-0">
					<tr> <th colspan="4" class="text-center"> Medium </th> </tr>
					<tr> <td class="text-center p-1">Total</td> <td class="text-center p-1">Attempted</td>
						 <td class="text-center p-1">Correct</td> <td class="text-center p-1">%</td> </tr>
					<tr> <td class="text-right p-1"><?=$d->total_medium?></td>
						 <td class="text-right p-1"><?=$d->attempted_medium?></td>
						 <td class="text-right p-1"><?=$d->correct_medium?></td> 
						 <td class="text-right p-1"><?=(int)($d->correct_medium*100/$d->total_medium)?>%</td> </tr>
				</table>
			 </div>
			 
			 <div class="col-md-3 m-0 p-0">
				<table class="table table-bordered m-0">
					<tr> <th colspan="4" class="text-center"> Hard </th> </tr>
					<tr> <td class="text-center p-1">Total</td> <td class="text-center p-1">Attempted</td>
						 <td class="text-center p-1">Correct</td> <td class="text-center p-1">%</td> </tr>
					<tr> <td class="text-right p-1"><?=$d->total_hard?></td>
						 <td class="text-right p-1"><?=$d->attempted_hard?></td>
						 <td class="text-right p-1"><?=$d->correct_hard?></td> 
						 <td class="text-right p-1"><?=(int)($d->correct_hard*100/$d->total_hard)?>%</td> </tr>
				</table>
			 </div>
			 
			 <div class="col-md-3 m-0 p-0" style="margin-bottom:1px !important">
				<table class="table table-bordered m-0">
					<tr> <th colspan="4" class="text-center"> Aggregate </th> </tr>
					<tr> <td class="text-center p-1">Total</td> <td class="text-center p-1">Attempted</td>
						 <td class="text-center p-1">Correct</td> <td class="text-center p-1">%</td> </tr>
					<tr> <td class="text-right p-1"><?=$d->total?></td>
							<td class="text-right p-1"><?=$d->attempted?></td>
						 <td class="text-right p-1"><?=$d->correct?></td> 
						 <td class="text-right p-1"><?=(int)($d->correct*100/$d->total)?>%</td> </tr>
				</table>
			 </div>
			 </div>
		</div>	
	<?php endforeach; /* ?>
<table class="table table-bordered table-responsive m-0">
	<thead>
	
	<!-- tr> <th colspan="17" class="center text-white">
			
			Name: <?=$d->student_name?>  |  Class:<?=$d->classname?>  | Subject:<?=$d->subjectname?> 
	</th> </tr -->
	<tr> <th rowspan="2" class="center odd"> Exam Date & Time </th> 
		 <th colspan="4" class="center even"> Easy Questions </th> 
		 <th colspan="4" class="center odd"> Medium Questions </th> 
		 <th colspan="4" class="center even"> Hard Questions </th>
		 <th colspan="4" class="center odd"> Aggregate </th> </tr>
	<tr> <th class="center even" > Total </th> <th class="center even"> Attempted </th> <th class="center even"> Correct </th>  <th class="center even"> Percent </th>
		 <th class="center odd"> Total </th> <th class="center odd"> Attempted </th> <th class="center odd"> Correct </th> <th class="center odd"> Percent </th>
		 <th class="center even"> Total </th> <th class="center even"> Attempted </th> <th class="center even"> Correct </th> <th class="center even"> Percent </th>
		 <th class="center odd"> Total </th> <th class="center odd"> Attempted </th> <th class="center odd"> Correct </th>	<th class="center odd"> Percent </th>
		 </tr>
	</thead>
	<tbody>
		<?php foreach($data as $d): ?>
			<tr> 
				<td class="odd"> <?=date('Y-m-d H:i:s',strtotime($d->start_date))?> </td> 
				<td class="right even"> <?=$d->total_easy?> </td> 
				<td class="right even"> <?=$d->attempted_easy?> </td> 
				<td class="right even"> <?=$d->correct_easy?> </td> 
				<td class="right even"> <?=(int)($d->correct_easy*100/$d->total_easy)?>% </td> 
				 
				<td class="right odd"> <?=$d->total_medium?> </td> 
				<td class="right odd"> <?=$d->attempted_medium?> </td> 
				<td class="right odd"> <?=$d->correct_medium?> </td> 
				<td class="right odd"> <?=(int)($d->correct_medium*100/$d->total_medium)?>% </td> 
				
				<td class="right even"> <?=$d->total_hard?> </td> 
				<td class="right even"> <?=$d->attempted_hard?> </td> 
				<td class="right even"> <?=$d->correct_hard?> </td> 
				<td class="right even"> <?=(int)($d->correct_hard*100/$d->total_hard)?>% </td> 
				
				<td class="right odd"> <?=$d->total?> </td> 
				<td class="right odd"> <?=$d->attempted?> </td> 
				<td class="right odd"> <?=$d->correct?> </td> 
				<td class="right odd"> <?=(int)($d->correct*100/$d->total)?>% </td> 
			</tr>
		<?php endforeach; ?>
	</tbody>
</table> */ ?>
</div> 
<?php endif; ?>