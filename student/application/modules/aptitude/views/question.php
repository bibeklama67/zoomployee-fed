<section class="banner" style="padding-top: 2%;">
			<?php 
						$alphabet = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
						$nepali_alpha = array("क","ख","ग","घ","ङ","च","छ","ज","झ","ञ","ट","ठ","ड","ढ","ण","त","थ","द","ध","न","प","फ","ब","भ","म","य","र","ल","व","श","ष","स","ह","क्ष","त्र","ज्ञ");
						$nepali_num = array("०","१","२","३","४","५","६","७","८","९","१०","११","१२","१३","१४","१५","१६","१७","१८","१९","२०","२१","२२","२३","२४","२५","२६","२७","२८","२९","३०","३१","३२","३३","३४","३५","३६","३७","३८","३९","४०","४१","४२","४३","४४","४५","४६","४७","४८","४९","५०","५१","५२","५३","५४","५५","५६","५७","५८","५९","६०","६१","६२","६३","६४","६५","६६","६७","६८","६९","७०","७१","७२","७३","७४","७५","७६","७७","७८","७९","८०","८१","८२","८३","८४","८५","८६","८७","८८","८९","९०","९१","९२","९३","९४","९५","९६","९७","९८","९९","१००","१०१","१०२","१०३","१०४","१०५","१०६","१०७","१०८","१०९","११०","१११","११२","११३","११४","११५","११६","११७","११८","११९","१२०","१२१","१२२","१२३","१२४","१२५","१२६","१२७","१२८","१२९","१३०","१३१","१३२","१३३","१३४","१३५","१३६","१३७","१३८","१३९","१४०","१४१","१४२","१४३","१४४","१४५","१४६","१४७","१४८","१४९","१५०","१५१","१५२","१५३","१५४","१५५","१५६","१५७","१५८","१५९","१६०","१६१","१६२","१६३","१६४","१६५","१६६","१६७","१६८","१६९","१७०","१७१","१७२","१७३","१७४","१७५","१७६","१७७","१७८","१७९","१८०","१८१","१८२","१८३","१८४","१८५","१८६","१८७","१८८","१८९","१९०","१९१","१९२","१९३","१९४","१९५","१९६","१९७","१९८","१९९","२००","२०१","२०२","२०३","२०४","२०५","२०६","२०७","२०८","२०९","२१०","२११","२१२","२१३","२१४","२१५","२१६","२१७","२१८","२१९","२२०","२२१","२२२","२२३","२२४","२२५","२२६","२२७","२२८","२२९","२३०","२३१","२३२","२३३","२३४","२३५","२३६","२३७","२३८","२३९","२४०","२४१","२४२","२४३","२४४","२४५","२४६","२४७","२४८","२४९","२५०","२५१","२५२","२५३","२५४","२५५","२५६","२५७","२५८","२५९","२६०");
			?> <style>  </style>
        <div class="pt">
			<?=form_open("/aptitude/aptitude_register/save_and_next",array('id'=>'ansform'))?>
				<input type="hidden" name="examid" value="<?=$questions->examid?>" />
				<input type="hidden" name="qorder" value="<?=$questions->qorder?>" />
				<input type="hidden" name="qtype" value="<?=$questions->qtype?>" />
				<!-- input type="hidden" name="aptitudeid" value="<?=$questions->aptitudeid?>" / -->
				<input type="hidden" name="quiz_contentid" value="<?=$questions->quiz_contentid?>" />

		<div class="card shadow mb-4">
				<?php $q = $data->response; ?>
						<?php if(strtolower($questions->qtype)=='fitb'): ?>
								<?php $qtext = str_replace('::BLANK::',' ______________ ',$q->question); ?>
						<?php else: ?>
								<?php $qtext = $q->question; ?>
						<?php endif; ?>

			<div class="card-header bg-light">
					<h5 class="card-title mb-0">	<?=$questions->qorder?>.  <?=$qtext?>
					 <span class="badge badge-info p-2 font-weight-normal shadow-sm ml-2 d-none">[  <i class="fa fa-tag"></i> <?=$questions->marks?> Mark<?=$questions->marks>1?'s':''?>  ]</span>
					</h5>
			</div>
			<div class="card-body bg-light">
				
		
						<div class="col-sm-12">
							<ol class="opts mb-0" >
								<?php 
									$cnt = 0; 	
									foreach($q->answer as $a) { if($a->iscorrect=='Y') $cnt++; } $label='a';
									foreach($q->answer as $a): ?>
									 <li class="d-flex">
									
											<input type="<?=$cnt>1?'checkbox':'radio'?>" value='<?=$a->option?>' name="option[]" id="<?=$label?>"  class="opts" /> 
											<label for="<?=$label?>"> 
												<?=$a->option?>
											</label>
									
									</li>
									<?php $label++; endforeach; ?>
							</ol>
						</div>
					
			</div>
			
		</div>
		<div class="text-right pt-lg-3 py-4">
						<div class="form-check d-inline-block mr-3">
 <input type="checkbox" id="skip" class="mr-2" value="skip" name="skip" style="width:20px; height:20px;" /> 
<label for="skip" class="font-weight-bold"> Skip </skip>
</div>
	<button class="btn btn-primary btn-lg next-btn"  id="save_next"><span><?=$lastq?'Finish':'Save & Next'?> <em class="fa fa-angle-right"></em></span> </button> 
					</div>
			
			<?=form_close()?>
		</div>	
</section>
<script>
	$(function(){
			
			$('#skipq,#save_next').on('click',function(e)
			{
					e.preventDefault(); e.stopPropagation();
					let fdata = $('#ansform').serializeArray();
					let url = $('#ansform').attr('action');
					if( $(this).attr('id')=='save_next' )
					{ if($('.opts:checked').length <= 0 && $('#skip:checked').length <=0 ) { alert("Please provide your answer."); return; }
							fdata.push({name:'action','value':'confirm'});
					}else{ fdata.push({name:'action','value':'skip'}); }
					
					$.post(url,fdata,function(data){
							if(data.type=='success') { 
									let url = '/aptitude/aptitude_register/get_question/<?=$questions->examid?>'; 
									$('#questdiv').load(url,{'qonly':'qonly'},function(data){});
									let url1 = '/aptitude/aptitude_register/get_qattempt_info/<?=$questions->examid?>'; 
									$('#qattmptdiv').load(url1,{},function(){});
									//location.reload(); 
							}
					},'json');
			});
			
	});
</script>
<style>
  .next-btn{
	border-radius: 0;
	font-size: 16px;
	min-width: 160px;
	padding: 10px 15px;
	position: relative;
		overflow:hidden
  }	

  button#save_next:before {
    content: "";
    height: 100%;
    width: 100%;
    background-color: #044188;
    left: 0;
    position: absolute;
    top: 0;
    transition: all 200ms linear;
    transform: translateX(-100%) ;

}


  button#save_next:hover:before{
	transform: translateX(0) ;
  }
  button#save_next span{
	  position: relative;
	  z-index: 2;
  }
</style>