<style>
		 .cls1 div{
				background-image: linear-gradient(to right,#FFC53C, #FC8A32);
				padding:3px;
				text-align:center;
				font-weight:bold;
			}
			
		.cls1{
				border:1px solid #FC8A32; border-radius:5px;margin-top:10px;	
			}
			
		.cls2 div{
				background-image: linear-gradient(to right,#B6B0FC, #7C75DD);
				padding:10px;
				text-align:center;
				font-weight:bold;
			}
			
		.cls2{
				border:1px solid #7C75DD; border-radius:5px;margin-top:10px;	
			}

		.cls3 div{
				background-image: linear-gradient(to right,#67E591, #43B469);
				padding:3px;
				text-align:center;
				font-weight:bold;
			}
			
		.cls3{
				border:1px solid #43B469; border-radius:5px;margin-top:10px;	
			}		
			
		.cls4 div{
				background-image: linear-gradient(to right,#74CFFE, #29ACEF);
				padding:3px;
				text-align:center;
				font-weight:bold;
			}
			
		.cls4{
				border:1px solid #29ACEF; border-radius:5px;margin-top:10px;	
			}
			
		table.res td, table.res th{ border-top:none; padding:2px 10px; }
		
		body{ background-color:#F5F8FF !important; }
		div.message{ text-align:center; padding:10px; border-bottom:1px solid #dedede; }
</style>
<?php 
	if(is_array($data))
	{
		$stu = current($data); 
		if(is_array($stu))
			$stu = current($stu); 
		else
			$stu=false;
	}else{ $stu=false; }
	
	if(!$stu):
?>
	<div class="btn-group exam-details-btn-group d-block d-lg-flex" role="group" aria-label="Basic example">
		<div class="btn btn-primary "> <span class="badge badge-warning text-uppercase"> You have not taken this exam.</span></div>
	</div>	
<?php else: ?>
	  
<!-- div class="my-4 d-flex justify-content-center">
	<div class="btn-group exam-details-btn-group d-block d-lg-flex" role="group" aria-label="Basic example">
		<button type="button" class="btn btn-primary ">Student Name: <span class="badge badge-warning text-uppercase"><?=$stu?$stu->student_name:'N/A'?> </span></button>
		<button type="button" class="btn btn-primary">Mobile : <span class="badge badge-warning text-uppercase"><?=$stu?$stu->parent_mobileno:'N/A'?> </span></button>
		<button type="button" class="btn btn-primary ">School: <span class="badge badge-warning text-uppercase"><?=$stu?$stu->schoolname:'N/A'?>( <?=$stu?$stu->districtname:'N/A'?> )</span></button>
	</div>
</div -->
<div class="container-fluid">
		<div class="message"> <h5>
			<?php 
				$curdata = current($data); 
				
				$curdata = element('0',$curdata);
				
				if( $curdata && date('Y-m-d',strtotime($curdata->start_date)) != date('Y-m-d') )
				{ 
					if($curdata->examtype=='unplanned_quiz')
					{
						echo "Today, you have not participated in daily quizzes.";
					}else if($curdata->examtype == 'planned_quiz')
						{
							echo "Today, you have not participated in olympiad exam.";
						}
				}else if(empty($curdata->completed_on) || $curdata->completed_on==null)
					{
						if($curdata->examtype=='unplanned_quiz'){
							echo " Today, you have not completed Daily Quizzes.";
						}else{
							echo " Today, you have not completed olympiad exam.";
						}
					}
				?> </h5>
		</div>
	  <?php $i=1; 
				 foreach($data as $d):
					$d = element('0',$d);
					if(!empty($d->completed_on)):
							if($i<=4)
								$class="cls$i";
							if($i>4 && $i%4==0)
								$class="cls1";
							if($i>4 && $i%4==1)
								$class="cls2";
							if($i>4 && $i%4==2)
								$class="cls3";
							if($i>4 && $i%4==3)
								$class="cls4";
	   ?>
	   
	<div class="<?=$class?>">	
		<div> 
			<?=date('Y-m-d',strtotime($d->start_date))?>
		</div>		
		<table class="table table-stacked res">
			<thead class="stacked__head">
				<tr class="bg-light text-dark">
					<!-- th> SNo </th -->
					<?php if(!in_array($d->examtype,array('planned_quiz','unplanned_quiz'))): ?>
					<th> Class </th>
					<th> Subject </th>
					<?php endif; ?>
					<!-- th> Total Time </th -->
					<th> Correct Answers </th>
					<th> Time Taken </th>
					<th> Rank </th>
					<!-- th> Action </th -->
				</tr>
			</thead>
			<tbody>
				<tr>
					<!-- td><strong class="d-inline-block mr-2 d-lg-none">SNo:</strong><?=$i++?></td -->
					<?php if(!in_array($d->examtype,array('planned_quiz','unplanned_quiz'))): ?>
						<td><strong class="d-inline-block mr-2 d-lg-none">Class:</strong><?=$d->classname?></td>
						<td><strong class="d-inline-block mr-2 d-lg-none">Subject:</strong><?=$d->subjectname?></td>
					<?php endif; ?>
					<!-- td><strong class="d-inline-block mr-2 d-lg-none">Total Time:</strong><?=date('H:i:s',mktime(0,$d->total_time))?> </td -->
					<td><strong class="d-inline-block mr-2 d-lg-none">Correct Questions:</strong><?=$d->correct_ans?> out of <?=$d->total_question?> </td>
					
					<td><strong class="d-inline-block mr-2 d-lg-none">Time Taken:</strong>
							<?php $dt = explode(':',date("H:i:s",strtotime($d->timetaken_format)));
									if($dt[0]=='00')
									{
										$ind=0; foreach($dt as $dp)
										{
											if($dp!='00')
											{
												echo ltrim($dp,'0');
												if($ind==1)
													echo ' min ';
												if($ind==2)
													echo ' sec';
											}
											$ind++;
										}
										
									}else{
										echo implode(':',$dt); 
									}
							?> </td>
					<td><strong class="d-inline-block mr-2 d-lg-none">Rank:</strong><?=$d->rnk?> out of <?=$d->total_students?></td>
					<!-- td> <strong class="d-inline-block mr-2 d-lg-none">Action:</strong><a data-setupid="<?=$d->setupid?>" class="btn btn-info" style="min-width:120px;" id="showtop">Show Top 100 </a>	</td -->
				</tr>
			</tbody>
		</table>
		
	</div>
	<?php endif;  endforeach; ?>
</div>

<div id="loadtop"  style="display:none"></div>

<script>
	$('#showtop').click(function(){
		let setupid = $(this).data('setupid');
		$("div#loadtop").load("/aptitude/aptitude_register/get_top/"+setupid,
			function(data){
				
				$('div#loadtop').show();
				
			});
	});
</script>

<style>
	@media (max-width: 768px) { 
	.table-stacked tr,.table-stacked td{
		display:block
	}
	.stacked__head{
		display:none;
	}
	.top-students-list tbody tr td:first-child{
		background-color:#054DA1;
		color:#fff;
	}
	.exam-details-btn-group .btn{
		width:100%;
	}
}
</style>

<?php endif; ?>