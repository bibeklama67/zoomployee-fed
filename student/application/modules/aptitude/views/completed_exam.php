<?php $scheme = element('scheme',$data); 
		 $other_exams = element('other_exams',$data);
		 $data = element('data',$data);	
		 $dat = current($data); 
?>
<h4 style="font-family:lato;" class="font-weight-bold mb-4" style="text-align:center">
		<center> 🎉 Congratulations !! 🎉 <br> 
			<?php switch($dat->examtype){
					case 'unplanned_quiz': 
							echo "You have completed today's quizzes";
							break;
					case 'planned_quiz': 
							echo "You have completed your olympiad exam.";
							break;
					case 'self':
							echo "You have completed your self assessment.";
							break;
					case 'aptitude':
							echo "You have completed your self assessment.";
							break;
					case 'exam':
							echo "You have completed your online exam.";
							break;
			} ?>
		</center>	
</h4>
<hr>
	<?php $d=current($data); 
		if($d->examtype=='unplanned_quiz' || $d->examtype=='planned_quiz'):
		
		$t=$a=$c=0;$dtimetaken=false; 
		
		foreach($data as $d):
	?>
		<table class="table table-bordered">
			<tbody>
				<tr>
					<th style="text-align:left">Total Questions:</th>
						<td align="right"><?=$d->total?></td>  </tr>
				<tr> <th style="text-align:left">Attempted Questions:</th>
						<td align="right"><?=$d->attempted?></td>   </tr>
				<tr> <th style="text-align:left">Correct Answers:</th>
					<td align="right"><?=$d->correct?></td>   </tr>
				<tr> <th style="text-align:left">Time Taken:</th>
						<td align="right"><b>
							<?php 
								$dt = explode(':',date("H:i:s",strtotime($d->timetaken_format)));
									if($dt[0]=='00')
									{
										$ind=0; foreach($dt as $dp)
										{
											if($dp!='00')
											{
												echo ltrim($dp,'0');
												if($ind==1)
													echo ' min ';
												if($ind==2)
													echo ' sec';
											}
											$ind++;
										}
										
									}else{
										echo implode(':',$dt); 
									}
								?>
						</b></td>
				</tr>
			</tbody>
		</table>
	<?php
		endforeach;
		else: 
	?>	
		<table class="table table-responsive table-bordered">
			<thead>
				<tr>
					<th style="text-align:center">Question Types</th>
					<th style="text-align:center">Total Questions</th>
					<th style="text-align:center">Attempted Questions</th>
					<th style="text-align:center">Correct Answers</th>
				</tr>
			</thead>
	 <tbody>
	<?php $t=$a=$c=0;$dtimetaken=false; 
		foreach($data as $d):
				if(!$dtimetaken)
					$dimetaken = ltrim(date('H:i:s',strtotime($d->timetaken_format)),'00:');
	?>
		<tr> 
			<td><?=ucfirst($d->level)?></td> 
			<td align="right"><?=$d->total?></td> 
			<td align="right"><?=$d->attempted?></td> 
			<td align="right"><?=$d->correct?></td> 
		</tr>
	<?php 
			$t+=$d->total;
			$a+=$d->attempted;
			$c+=$d->correct;
			endforeach; ?>
	</tbody>
	<tfoot>
		<tr>
			<td align="right" class="font-bold"><strong>Total</strong> </td>
			<td align="right"><b><?=$t?></b></td>
			<td align="right"><b><?=$a?></b></td>
			<td align="right"><b><?=$c?></b></td>
		</tr>
		<tr>
			<td colspan="3" align="right"> <b> Total Time Taken:</b> </td> <td align="right"><b><?=$dimetaken?></b></td>
		<tr>
	</tfoot>
</table>
<?php endif; ?>

<?php /*var_dump($data); ?>
<div class="container">
	<?php $d = current($data); 
		if($d->examtype == 'aptitude'): 
			 $s = current($scheme); 
			 if($s):
			 ?>
		<p class="h5" style="text-align:center"> To improve your <?=$d->subjectname?>, you can join our Online Courses. </p>
		<p class="h5" style="text-align:right"> <?=$s->package_days?> Days Class: Rs. <?=number_format((float)$s->package_rate, 2, '.', '');?> <BR>
				<a href="/aptitude/aptitude_register/apply_for_scheme/<?=$s->schemeid?>"> Apply Now >>>> </a> </p>
	<?php endif; endif; ?>
</div>
<?php */  
/*
if(count($other_exams) > 0): ?>
<h3 style="position:absolute;bottom:0;width:100%;text-align:center"> 
	To view your past results, please 
		<?php if(in_array($d->examtype,array('planned_quiz','unplanned_quiz'))): ?>
			<a href="/aptitude/aptitude_register/get_my_results/<?=$d->examtype?>/<?=$d->studentid?>">Click Here >> </a> 
		<?php else: ?>
			<a href="/aptitude/aptitude_register/past_results/<?=$d->aptitudeid?>">Click Here >> </a> 
		<?php endif; ?>	
			</h3>
			
<?php endif; */ ?>
<?php 
function timeBetween($dt1, $dt2) {
    return date_diff(
        date_create($dt2),  
        date_create($dt1)
    )->format('%i');
}
?>
<style>
	.table-custom-dark{
		background-color:#267BDE;
		font-family: "lato";
		text-transform: uppercase;
		font-size: 14px;
		font-weight: normal;
		letter-spacing: 1px;
	}
	.table-custom-dark th{
		color:#fff;
	}
</style>