<div class="container" style="width:100%">
        <h6> Upcoming Exams </h6>
        <table class="table table-bordered table-striped">
            <thead>
            <tr> 
                <th> Class </th> 
                <th> Subject </th>
                <th> Exam </th>
                <th> Start Date </th> 
                <th> Start Time </th>
                <th> Action </th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($exams as $d): ?>
                    <tr>
                        <td><?=$d->classname?> </td> 
                        <td><?=$d->subjectname?> </td> 
                        <td> </td> 
                        <td><?=$d->from_date?> </td> 
                        <td><?=$d->start_time?> </td> 
                        <td> <a href="/aptitude/aptitude_register/index/<?=$d->subjectid?>/planned"> <button class="btn btn-info"> Start </button></td> 
                     </tr>
                <?php endforeach; ?>
        </tbody>
        </table>
</div>