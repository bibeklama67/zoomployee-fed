<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700&display=swap" rel="stylesheet">
<body>
   
        <nav class="navbar navbar-expand-lg navbar-light bg-white border-bottom">
            <div class="container">
            <a class="navbar-brand" href="<?=base_url();?>">
                <img src="<?=base_url('assets/eacademy/images/logo.png');?>" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                    <li class="nav-item ">
                        <a class="nav-link" href="<?=base_url();?>">Home</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="<?= base_url('faqtour');?>">About eCADEMY <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item ">
                     <a class="nav-link" href="<?=base_url();?>applicationform">Application Form</a>
                  </li>
                    <li class="nav-item ">
                       <a class="nav-link contact" href="javascript:void(0)" >Contact Us</a>
                   </li>
                </ul>
            </div>
               </div>
        </nav>

    <section class="ampitude-form position-relative p-5">
	
	<?php $et = array('planned'=>'Online Exam',
				'aptitude'=>'Aptitude Test',
				'planned_quiz'=>'Olympiad',
				'unplanned_quiz'=>'Daily Quiz'); 
				
		$studentid = $this->session->userdata('studentid');
		
		if(!$studentid)
			redirect('/login');
		$classid = $this->session->userdata('eclassid');
		$quiz = element('0',$quiz);
		$olymp = element('0',$olymp);
	?>
	<table class="table">
		<tr> <th> Examtype </th> <th> Link </th> </tr>
		<?php foreach($et as $k=>$v):
				if($k=='unplanned_quiz' || $k=='planned_quiz'):
					//var_dump($olymp);
					if($k=='unplanned_quiz')
						$subjectid=$quiz->subjectid;
					else
						$subjectid=$olymp->subjectid;
		?>
		<tr> <td> <?=$v?> </td> 
			<td>
				<b>Exam</b><BR>
				<b>Format:</b> https://www.midasecademy.com/aptitude/aptitude_register/index/[subjectid]/[examtype]/[orgid]/[classid]/[studentid]
				<BR>
			<a href="https://www.midasecademy.com/aptitude/aptitude_register/index/<?=$subjectid?>/<?=$k?>/10000002/<?=$classid?>/<?=$studentid?>"> https://www.midasecademy.com/aptitude/aptitude_register/index/<?=$subjectid?>/<?=$k?>/10000002/<?=$classid?>/<?=$studentid?> 
				</a> <BR><BR>
				<b>Result</b> <BR>
				<b>Format:</b> https://www.midasecademy.com/aptitude/aptitude_register/get_my_results/[examtype]/[studentid]
				<BR>
				<a href="https://www.midasecademy.com/aptitude/aptitude_register/get_my_results/<?=$k?>/<?=$studentid?>"> 
						https://www.midasecademy.com/aptitude/aptitude_register/get_my_results/<?=$k?>/<?=$studentid?>
				</a> <BR><BR>
				<b>Winners</b> <BR>
				<b>Format:</b> https://www.midasecademy.com/aptitude/aptitude_register/get_top/[subjectid]/[examtype]
						<BR>
						<a href="https://www.midasecademy.com/aptitude/aptitude_register/get_top/<?=$subjectid?>/<?=$k?>">
							https://www.midasecademy.com/aptitude/aptitude_register/get_top/<?=$subjectid?>/<?=$k?>
						</a>	
			</div>
			</td> </tr> 
			
		<?php endif; endforeach; ?>
		
	</table>
	
	</section>
	
