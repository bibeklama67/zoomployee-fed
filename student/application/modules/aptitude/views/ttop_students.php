<?php $stu = current($data); ?>
<div class="bg-white">
<table class="table table-striped">
	<thead>
		<tr>
			<td colspan="6" class="bg-success text-white" align="center"> 
				Top 100 Ranking Student Lists
			</td>
		</tr>
		<tr>
			<td colspan="6" class="bg-primary text-white" align="center">
				<div class="row">
					<div class="col-3">
						Exam Date: <?=$stu->start_date?>
					</div>
					<div class="col-3">
						Total Time: <?=date('H:i:s',mktime(0,$stu->total_time))?>
					</div>
					<div class="col-3">
						Total Questions: <?=$stu->total_question?>
					</div>
					<div class="col-3">
						Class/Subject : <?=$stu->classname?> ( <?=$stu->subjectname?> )
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td> Student Name </td>
			<td> School  </td>
			<td> District </td>
			<td align="center"> Time Taken </td>
			<td align="right"> Correct Questions </td>
			<td align="right"> Rank </td>
		</tr>
	</thead>
	<tbody>
		<?php foreach($data as $d): ?>
			<tr>
				<td align="left"><?=$d->student_name?></td>
				<td align="left"><?=$d->schoolname?></td>
				<td align="left"><?=$d->districtname?></td>
				<td align="center"><?=$d->timetaken_format?></td>
				<td align="right"><?=$d->correct_ans?></td>
				<td align="right"><?=$d->rnk?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>
</div>
<style>
	thead td{ font-weight:bold; }