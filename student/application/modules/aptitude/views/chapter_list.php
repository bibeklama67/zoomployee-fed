<div class="row justify-content-center">
    <div class="col-auto">
		<?php $c = current($chapters); ?>
		<?=form_open('/aptitude/aptitude_register/populate_exam_for_self','id="chapter_form"',array('aptitudeid'=>$aptitudeid,'orgid'=>$orgid))?>
		<table class="table table-responsive table-bordered">
		<thead>
			<tr>
			<th colspan="2"> <div class="row">
				<!-- div class="col-md-6"><b> Class: </b> <?=$c->classname?> </div -->
				<div class="col-md-12 text-center"><b> Subject: </b> <?=$c->subjectname?></div>
			</div>   </th></tr>
			<tr class="bg-info text-white"> 
			<th colspan="2"> Please select at least 3 and at most 10 chapters to take Self Assessment Test. </th>
			</tr>
			
			<tr> <th> Select </th> <th> Chapters </th> </tr>
		</thead>
		<tbody>
			<?php $i=1; foreach($chapters as $c): ?>
				<tr> <td> <?=form_checkbox("chapterid[$i]",$c->chapterid,false,"class='chapter form-control' style='width:20px; height:20px'")?> </td> <td> <?=$i++?>. <?=$c->chaptername?> </td> </tr> 
			<?php endforeach; ?>
		</tbody>
		<tfoot>
			<tr> <td colspan="2" align="right"> <?=form_submit('submit','Start Self Assessment Test',"class='btn btn-primary' id='submit'"); ?> </td> </tr>
		</tfoot>
		</table>
		<?=form_close(); ?>
	</div>
</div>
<script>
	$(function(){
		$('.chapter').on('change',function(e){
			if($('.chapter:checked').length > 10)
			{
				$(this).prop('checked',false);
				alert("You can select maximum of 10 chapters.");
			}
		});
		$('.chapter').trigger('change');
		
		$('#chapter_form').on('submit',function(e){
			if($('.chapter:checked').length < 3 || $('.chapter:checked').length > 10)
			{
				alert("Please select at least 3 and at most 10 chapters to take Self Assessment Test.");			
				e.stopPropagation();
				e.preventDefault();
			}
			return;
		});
	});
</script>
	