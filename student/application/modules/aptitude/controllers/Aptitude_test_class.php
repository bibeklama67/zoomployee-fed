<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aptitude_test_class extends MX_Controller {

  function __construct()
  {
    parent::__construct();

    $this->load->library('curl');
    $this->load->model('aptitude_model');
    $this->apiurl = $this->config->item('api_url') . 'api/';
    $this->orgid = '10044344';
  }
  
  public function form($subj)
  {
  	$subject = urldecode(base64_decode($subj));
    $data = array();
    $data['subject'] = $subject;
    $this->session->set_userdata('subject',$subject);
    
    $districtdata = $this->commoncurl('setup/setup/student_register/getDistrict', array());
    if ($districtdata->type == 'success') {
      $data['district'] = $districtdata->response;
    }

    $this->session->set_userdata('userid', '10000052');
    $eclassdata = $this->commoncurl('elearning/teacherend/studentApi/getexameclass', array('orgid' => $this->orgid));

    // echo "<pre>";print_r($eclassdata);die();
    if ($eclassdata->type == 'success') {
      $data['eclass'] = $eclassdata->response;
      if(empty($data['eclass'])){
      	$data['error_message'] = 'Exam not available right now. Please try again later.';
      }
    }

    $this->load->view('home/inc/header');
    $this->load->view('form',$data);
    $this->load->view('home/inc/footer');
  }


  function aptitude_test_class_submit()
  {
    $this->form_validation->set_rules('student_name', 'Student Name', 'required');
    $this->form_validation->set_rules('parent_name', 'Parent Name', 'required');
    $this->form_validation->set_rules('parent_mobileno', 'Parent Mobile', 'required');
    $this->form_validation->set_rules('school', 'School/College Name', 'required');
    $this->form_validation->set_rules('district', 'District', 'required');
    $this->form_validation->set_rules('vdc', 'Municipality/V.D.C', 'required');
    $this->form_validation->set_rules('address', 'Address', 'required');
    $this->form_validation->set_rules('eclass', 'Class', 'required');
    $this->form_validation->set_rules('subject', 'Subject', 'required');

    if ($this->form_validation->run() == FALSE) {
      $res = ["message" => validation_errors(), "type" => 'error'];

      echo json_encode($res);
      exit;
    }

    $post = $_POST;

    $data = array(
      'class_id' => $post['eclass'],
      'subject_id' => $post['subject'],
      'parent_mobileno' => $post['parent_mobileno'],
      'parent_name' => $post['parent_name'],
      'student_name' => $post['student_name'],
      'school_name' => $post['school'],
      'district_id' => $post['district'],
      'vdc' => $post['vdc'],
      'address' => $post['address'],
      'datapostdatetime' => date('Y-m-d H:i:s'),
    );

    if ($this->aptitude_model->savedata($data)){
      echo json_encode(array('type' => 'success', 'message' => 'Success'));
    }else{
      echo json_encode(array('type' => 'error', 'message' => 'Something Went Wrong!'));
    }
    
  }
  

  function aptitude_test_class_getsubject()
  {
  	$data = array();
    $sdata = $this->commoncurl('elearning/teacherend/studentApi/getexamsubjectlist', array('orgid' => $this->orgid, 'classid' => $_POST['eclassid']));
    if ($sdata->type == 'success') {

      $subject = $sdata->response;
      echo json_encode(array('type' => 'success', 'message' => 'SUBJECT LIST', 'response' => $subject));
    } else {
      echo json_encode(array('type' => 'error', 'message' => 'No any SUBJECT List', 'response' => array()));
    }
  }


  public function aptitude_test_subject_urls()
  {
  	$distinct_subjects = $this->aptitude_model->get_distincit_subject();
  	
  	foreach ($distinct_subjects as $key => $st) {
  		$distinct_subjects[$key]['encode_value'] = preg_replace('/=/i', '##',base64_encode(urlencode($st['subjectname'])));
  	}

  	// foreach ($distinct_subjects as $key => $st) {
  	// 	$distinct_subjects[$key]['decode_value'] = urldecode(base64_decode($st['encode_value']));
  	// }

  	// echo "<pre>";print_r($distinct_subjects);die();
  	$data['subjects'] = $distinct_subjects;
  	$this->load->view('home/inc/header');
    $this->load->view('subjects',$data);
    $this->load->view('home/inc/footer');
  }

  function commoncurl($url, $req_data)
  {

    $data = $this->curl->post_data($this->apiurl . $url, $req_data);
    $data = json_decode($data);

    return $data;
  }
}