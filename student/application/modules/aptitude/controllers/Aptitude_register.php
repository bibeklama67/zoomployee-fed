<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aptitude_register extends MX_Controller {

  function __construct()
  {
    parent::__construct();
	
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);
	 
    $this->load->library('curl');
    $this->load->model('aptitude_model');
    $this->apiurl = $this->config->item('api_url') . 'api/';
    $this->orgid = '10044344';
  }
  
  public function index($subj=false,$planned='aptitude',$orgid='10000002',$eid=false,$sid=false)
  {
	  	try
		{ 
			if(!$sid)
				$sid = $this->session->userdata('studentid');
			
			if(!$sid)
				$sid = $this->input->post('studentid');
			
			if(!$sid && $planned!='aptitude')
			{
				$this->session->set_userdata('redirectToCurrent',base_url(uri_string()));
				redirect('/login');
			}
			
			if($sid)
			{ // for registered users
				
				if(!$subj)
					throw new Exception("Class and Subject is Not Defined");	
				
					if(!$eid)
						$eid = $this->session->userdata('eclassid');
					
					$cid = $this->aptitude_model->get_classid($subj);
					
					if($eid != $cid)
					{
						throw new Exception("This Test is not meant for your class.");
					}
					
					$student_data = $this->aptitude_model->get_studentdetail($sid);
					
					$post=(array)$student_data;
					$post['subjectid'] = $subj;
					//var_dump($post);
					$aptitudeid = $this->aptitude_model->savedata($post);
					if(!$aptitudeid)
						throw new Exception("Data could not be saved.");
					
					if($planned=='self')
					{	
						$ed = $this->aptitude_model->get_exam_in_progress($aptitudeid,$planned,$orgid);
						$dload = array('ed'=>$ed,'aptitudeid'=>$aptitudeid,'type'=>$planned,'orgid'=>$orgid,'examtime'=>false,'examdata'=>false);
						$setupdata = $this->aptitude_model->get_setup_data($aptitudeid,$planned,$orgid);
						$dload['setupdata']=element('0',$setupdata);
						$dload['studentdata'] = $student_data;
						
						$this->load->view("home/inc/header");
						$this->load->view("exam_in_progress",$dload);
						$this->load->view("home/inc/footer"); 
					}else
						{  //if currently planned exam has been completed by the student today.
							if(in_array($planned,array('planned_quiz','unplanned_quiz','planned')))
							{
								$this->autocomplete_oldexams();
								$data = $this->aptitude_model->get_exam_completed($aptitudeid,$planned);
								if(is_array($data) && count($data)>0)
								{  		//if this person has already taken quiz and has completed redirect to the result page.
										$data = element('0',$data);
										if(date('Y-m-d',strtotime($data->start_date)) == date('Y-m-d'))
										{
											redirect("/aptitude/aptitude_register/get_question/{$data->examid}");
										}
								}
								
								$ed = $this->aptitude_model->get_exam_in_progress($aptitudeid,$planned,$orgid);
								if(!empty($ed))
								{
									//Already has exam in progress within given time so 
									//Redirect to the current question status
									//Or if he has completed today's exam then redirect to the Result Page
									$e = current($ed);
									
									$dat = $e->from_date.' '.$e->start_time;
									$tim = $e->total_timemins;
									$to = strtotime("+{$tim} minutes",strtotime($dat));
									$curtime = strtotime(date('Y-m-d H:i:s'));
									
									
									if($to >= $curtime && strtotime($dat) <= $curtime )
									{
										if( $planned=='unplanned_quiz' || !empty($e->current_question) ) 
											redirect("/aptitude/aptitude_register/get_question/{$e->examid}");
									}
								}	
							}
							
							if($planned=='planned' || $planned=='planned_quiz')
							{	
								//var_dump($aptitudeid);
								//no exam in progress or completed today
								//check if exam is scheduled for today if yes populate question and get to start page
								$dload = $this->aptitude_model->populate_exam_questions($aptitudeid,$planned,$orgid);
								
								if(element('examtime',$dload)=='future_dates')
								{
									//Exam Scheduled For Future
									$this->load->view('home/inc/header');
									$this->load->view('future_exams',$dload);
									$this->load->view('home/inc/footer');
									return;
								}
							}	
							
							 
							
							if($planned!='planned' && $planned!='planned_quiz')
							{	
								$ed = $this->aptitude_model->get_exam_in_progress($aptitudeid,$planned,$orgid);
								$dload = array('ed'=>$ed,'aptitudeid'=>$aptitudeid,'type'=>$planned,'orgid'=>$orgid,'examtime'=>false,'examdata'=>false);
								$setupdata = $this->aptitude_model->get_setup_data($aptitudeid,$planned,$orgid);
								if(empty($setupdata))
									throw new Exception("Exam Setup Not Exist");
								$dload['setupdata']=element('0',$setupdata);
								$dload['studentdata'] = $student_data;
								//echo "<pre>"; var_dump($dload); echo "</pre>"; exit;
							}
							
							$this->load->view("home/inc/header");
							$this->load->view("exam_in_progress",$dload);
							$this->load->view("home/inc/footer"); 	
						}
					}else{
			
							$this->form($subj);
						}
				
			
		}catch(Exception $e)
			{
				$message = $e->getMessage();
				
				if($planned=='unplanned_quiz')
					$message = "Quizzes for today is not available. Please try tomorrow.";
				$this->display_errors($message);
			}
  }
  
  public function form($subj)
  {
		$subject = $subj; //urldecode(base64_decode(preg_replace('/##/i','=',$subj)));
		$data = array();
		$data['subject'] = $subject;
    
		$districtdata = $this->commoncurl('setup/setup/student_register/getDistrict', array());
		if ($districtdata->type == 'success') {
			$data['district'] = $districtdata->response;
		}else{
			echo $districtdata->message;
		}

		$this->session->set_userdata('userid', '10000052');
		$eclassdata = $this->commoncurl('elearning/teacherend/studentApi/getexameclass', array('orgid' => $this->orgid));

		if ($eclassdata->type == 'success') {
			$data['eclass'] = $eclassdata->response;
			if(empty($data['eclass'])){
				$data['error_message'] = 'Exam not available right now. Please try again later.';
			}
		}else{
			echo $eclassdata->message;
		}
		$this->load->view('home/inc/header');
		$this->load->view('form',$data);
		$this->load->view('home/inc/footer');
  }


  function aptitude_test_class_submit()
  {
	  try{
	  
				$this->form_validation->set_rules('student_name', 'Student Name', 'trim|required');
				$this->form_validation->set_rules('parent_name', 'Parent Name', 'trim|required');
				$this->form_validation->set_rules('parent_mobileno', 'Parent Mobile', 'trim|required');
				$this->form_validation->set_rules('school_name', 'School/College Name', 'trim|required');
				$this->form_validation->set_rules('districtid', 'District', 'trim|required');
				$this->form_validation->set_rules('vdc', 'Municipality/V.D.C', 'trim|required');
				$this->form_validation->set_rules('address', 'Address', 'trim');
				$this->form_validation->set_rules('classid', 'Class', 'trim|required');
				$this->form_validation->set_rules('subjectid', 'Subject', 'trim|required');

				if (!$this->form_validation->run()) 
						throw new Exception(validation_errors());
				
				$post = $this->input->post();
				$post['datapostdatetime'] = date('Y-m-d H:i:s');
				
				$aptitudeid = $this->aptitude_model->savedata($post);
				
				echo json_encode(array('type' => 'success', 'message' => 'Success','aptitudeid'=>$aptitudeid));	
				
	  }catch(Exception $e)
				{
					echo json_encode(array('type' => 'error', 'message' => $e->getMessage()));
				}
  }
  
  
  function get_exam_in_progress($aptid=false)
  {
	 try{
			if(!$aptid)
				$aptid= $this->input->post('aptitudeid');
			
			if(!$aptid)
				throw new Exception("Required Paramters Missing");

			 $ed = $this->aptitude_model->get_exam_in_progress($aptid);

			 $this->load->view("exam_in_progress",array('ed'=>$ed,'aptitudeid'=>$aptid,'examdata'=>false,'examtime'=>false));		
			
		}catch(Exception $e)
	 {
		 echo $e->getMessage();
	 }
  }

	function get_past_exams($aptid=false)
	{
	 try{
			if(!$aptid)
				$aptid= $this->input->post('aptitudeid');
			
			if(!$aptid)
				throw new Exception("Required Paramters Missing");

			 $ed = $this->aptitude_model->get_past_exams($aptid);

			 $this->load->view("exam_in_progress",array('ed'=>$ed,'aptitudeid'=>$aptid,'examdata'=>false,'examtime'=>false));		
			
		}catch(Exception $e)
			{
				echo $e->getMessage();
			}
	}

  function aptitude_test_class_getsubject()
  {
		$data = array();
		$classid = $this->input->post('classid');
		$sdata = $this->commoncurl('elearning/teacherend/studentApi/getexameclass', array('orgid' => $this->orgid,'classid'=>$classid));
		if ($sdata->type == 'success') {
			$subject = $sdata->response;
			$this->load->view('list_subjects',array('subjects'=>$subject));
		}
  }

  
  function get_qattempt_info($examid=false)
  {
	  try{
				if(!$examid)
					throw new Exception("Exam does not exist");
				
						$data = $this->aptitude_model->getoverallexam($examid);
						if(empty($data))
								throw new Exception("Exam Currently Not Available");			
						$this->load->view('qattempt_info',array('data'=>$data));
				
	  }catch(Exception $e)
			{
				echo $e->getMessage();
			}
  }
	
	function autocomplete_oldexams()
	{
		$this->aptitude_model->autocomplete_oldexams();
	}
 
	function get_question($examid=false)
	{
		try{
				if(!$examid)
					throw new Exception("Required Paramters Missing");
			
				if($this->input->post('qonly'))
				{
					$qdata = $this->aptitude_model->get_current_question($examid);
					
					//var_dump($qdata);
					
					if(!$qdata)
						throw new Exception("Something is Wrong");
					
					$status = element('status',$qdata);
					
					if(strtolower($status) =='completed')
					{
							$this->load->view('completed_exam',$qdata);
							return;
					}else{
						$lastq = element('lastq',$qdata);
						$questions = element('data',$qdata);
					}
						if(empty($questions))
								throw new Exception("Question Not Found");	
					
					$dpost = array('Questionid'=>$questions->quiz_contentid,'quiztype'=>$questions->qtype); 
					$data = $this->commoncurl('elearning/liveclass/getQuestionAnswer',$dpost);
				
					if($data && $data->type!='success')
							throw new Exception($data->message);
					$this->load->view('question',array('questions'=>$questions,'data'=>$data,'lastq'=>$lastq));
					
				}else{
						$data = $this->aptitude_model->getoverallexam($examid);
						
						if(empty($data))
								throw new Exception("Exam Currently Not Available");
						
						$this->load->view('home/inc/header');				
						$this->load->view('exam_question',array('data'=>$data));
						$this->load->view('home/inc/footer');
				}
				
		}catch(Exception $e){
				echo $e->getMessage();
		}
	}
  
 
	function past_results($aptitudeid=false,$type='aptitude',$schoolid='10000002')
	{
		try{
			$data = $this->aptitude_model->get_exam_completed($aptitudeid,$type,$schoolid);
			if(empty($data))
				throw new Exception("Result Not Found");
			$this->load->view('home/inc/header');
			$this->load->view('past_results',array('data'=>$data,'type'=>$type));
			$this->load->view('home/inc/footer');
		
		}catch(Exception $e){
			$message='';
			if($type=='self')
					$message='You have not taken any Self Assessment Test yet.';
			else if($type='aptitude')
					$message='You have not taken Aptitude Test yet.';
			$this->display_errors($message);
		}
	}
	
	function get_chapter_list($aptitudeid,$type,$orgid)
	{
		try{
			$chapters = $this->aptitude_model->get_chapter_list(false,$aptitudeid,$orgid);
			$this->load->view('home/inc/header');
			$this->load->view("chapter_list",array('orgid'=>$orgid,'aptitudeid'=>$aptitudeid,'chapters'=>$chapters));
			$this->load->view('home/inc/footer');
		}catch(Exception $e){}
	}
	
	function populate_exam_for_self(){
		try{
				$aptitudeid=$this->input->post('aptitudeid');
				$chapters=$this->input->post('chapterid');
				$orgid = $this->input->post('orgid');
				if(count($chapters)<3 || count($chapters)>10)
					throw new Exception("Chapter count is not between 3 and 10");
				
				$data = $this->aptitude_model->populate_exam_for_self($chapters,$aptitudeid,$orgid);
				//var_dump($data);
				if(!$data)
					throw new Exception("Question for this Exam could not be found.");
				redirect("/aptitude/aptitude_register/get_question/$data");
		}catch(Exception $e){
			 $this->display_errors("Please try again later!! <BR>".$e->getMessage());
		}
		
	}
  
  function startexam($aptitudeid=false,$type='aptitude',$orgid='10000002')
  {
	  try{
					if(!$aptitudeid)
						throw new Exception("Required Paramters Missing.");
				
					 $examid = $this->aptitude_model->startexam($aptitudeid,$type,$orgid);
				 
				 if(!$examid)
						throw new Exception("Sorry Exam Not Available Now");
				
					redirect('/aptitude/aptitude_register/get_question/'.$examid);

	   }catch(Exception $e)
	   {
		   $message='';
		   if($type=='unplanned_quiz')
					$message = "Quizzes for today is not available. Please try tomorrow.";
				$this->display_errors($message);
	   }
  }

	function save_and_next()
	{
		try{
				$post = $this->input->post();
				$data = $this->commoncurl('elearning/liveclass/getQuestionAnswer',array('Questionid'=>element('quiz_contentid',$post),'quiztype'=>element('qtype',$post)));
				
				if($data->type !='success')
					throw new Exception($questions->message);
				
				$questions = $this->aptitude_model->get_current_question(element('examid',$post));
				
				$questions = element('data',$questions);				
				$c=0;
				$options = element('option',$post);
				
				if(empty($options))
					$options = array();
				
					foreach($data->response->answer as $a)
					{
						if($a->iscorrect=='Y' && in_array($a->option,$options))
							$c++;
					}
				
				if(count($options)==$c)
				{
					$post['iscorrect'] = 'Y'; 
					$post['om'] = $questions->marks;
				}else{
						$post['iscorrect'] = 'N';
						$post['om']=0;
				}
				
				$post['iscompleted']='t';
				if(element('skip',$post)!='skip')
						$post['provided_answer'] = json_encode($options);
						
				$sav_ans = $this->aptitude_model->save_answer($post);
				
				echo json_encode(array('type'=>'success'));
				
		}catch(Exception $e)
			{
				echo json_encode(array('type'=>'failure','message'=>$e->getMessage()));
			}
		
	}


	function get_exam_list($type='planned')
	{
		try{
		$studentid = $this->session->userdata('studentid');
		$classid = $this->session->userdata('eclassid');
		if(!$studentid || !$classid)
			throw new Exception("Required Parameter Missing");

		$exams = $this->aptitude_model->get_exam_list($type,$classid);

			$this->load->view('home/inc/header');
			$this->load->view('exam_lists',array('exams'=>$exams));
			$this->load->view('home/inc/footer');
		}catch(Exception $e)
			{
				echo $e->getMessage();
			}
	}

  function commoncurl($url, $req_data)
  { 
		$data = $this->curl->post_data($this->apiurl . $url, $req_data); 
		$data = json_decode($data);

    return $data;
  }

	function apply_for_scheme($schemeid=false)
	{
		echo "<h1> Application & Payment Integration Here </h1>";
	}


	function get_my_results($examtype=false,$studentid=false)
	{
		try{
			
			if(!$examtype || !in_array($examtype,array('planned','planned_quiz','unplanned_quiz')))
				throw new Exception("Requred Parameter Missing");
			
			if(!$studentid)
				$studentid = $this->session->userdata('studentd');
			
			if(!$studentid)
				throw new Exception("Student not recognized");
		
			$data = $this->aptitude_model->get_individual_result($examtype,$studentid);
			
			if(empty($data))
				throw new Exception("No Result Found");
			
			$this->load->view('home/inc/header');
			$this->load->view("individual_results",array('data'=>$data));
			$this->load->view('home/inc/footer');
		}catch(Exception $e){
			$message=$e->getMessage();
			if($examtype=='unplanned_quiz')
				$message='You have not participated in any quizzes.';
			if($examtype=='planned_quiz')
				$message='You have not participated in any Olympiad Exams yet.';
			$this->display_errors($message);
			//echo $e->getMessage();
		}
		
	}

	function get_top($subjectid=false,$examtype='unplanned_quiz',$orgid=10000002,$setupid=false)
	{
		try{
			if(!$subjectid)
				throw new Exception("Exam Not Specified.");
		
			$startdate = $this->aptitude_model->get_exam_dates($subjectid,$examtype);
			$data = array();
			
			foreach($startdate as $sd)
			{
				$s = preg_replace('/-/','_',$sd->start_date);
				
				$data[$s] = $this->aptitude_model->get_rank($subjectid,$examtype,$sd->start_date);
			}
			
			if(empty($data))
					throw new Exception("No Winners Yet");
			
			$this->load->view('home/inc/header');
			$this->load->view('top_students',array('sdata'=>$data));
			$this->load->view('home/inc/footer');
			
		}catch(Exception $e)
		{
			$message=$e->getMessage();
			if($examtype == 'unplanned_quiz')
				$message="Winners of daily quizzes will be announced soon.";
			if($examtype == 'planned_quiz')
				$message="Olyampiad for your grade has not started yet.";
			
			$this->display_errors($message);
			//echo $e->getMessage();
		}
		
	}
	
	function verify_time($examid=false){
		try{
			if(!$examid)
				throw new Exception("Required Parameter Missing.");
			$data = $this->aptitude_model->get_exam_by_id($examid);
			if(!$data)
				throw new Exception("Exam Not Found.");
			
			if(strtotime(date('Y-m-d H:i:s'))>= strtotime($data->exam_date))
				echo json_encode(array('type'=>'success','message'=>'go'));
			else
				throw new Exception("Wait For Timer To Expire");
			
		}catch(Exception $e){
				echo json_encode(array('type'=>'error','msg'=>$e->getMessage()));
		}
	}
	
	function future_exams($aptitudeid,$type,$orgid)
	{
		$data = $this->aptitude_model->get_exams_within_next_three_months($aptitudeid,$type,$orgid);
		$this->load->view('future_exams',array('setupdata'=>$data,'type'=>$type,'fromajax'=>'fromajax'));
	}
	
	function display_errors($message)
	{
		$this->load->view('error_page',array('message'=>$message));
	}
	
	function get_rules($setupid=false)
	{
		$rules = $this->aptitude_model->get_rules($setupid);
		$this->load->view('rules_page',array('rules'=>$rules));
	}

}