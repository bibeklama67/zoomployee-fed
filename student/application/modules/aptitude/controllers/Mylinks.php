<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Mylinks extends MX_Controller {
	
	function __construct()
	{
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);
		$this->load->model('mylinks_model','model');
	}
	
	function index(){
		$quiz = $this->model->get_subjects('quiz');
		$olymp = $this->model->get_subjects('olymp');
		$other = $this->model->get_subjects(false);
		
		$this->load->view('home/inc/header');
		$this->load->view('links',array('quiz'=>$quiz,'other'=>$other,'olymp'=>$olymp));
		$this->load->view('home/inc/footer');
	}
	
	public function aptitude_test_subject_urls($type='unplanned_quiz',$orgid='10000002',$classid='-1',$subjectid='-1',$studentid='-1')
	{
	  try{
			switch($type)
			{
				case 'onlineexam':
				case 'planned':
						$type='planned';
						break;
				case 'aptitude':
						$type='aptitude';
						break;
				case 'dailyquiz':
				case 'unplanned_quiz':
						$type='unplanned_quiz';
						break;
				case 'olympiad':
				case 'olymp':
				case 'planned_quiz':
						$type='planned_quiz';
						break;
				case 'self':
						$type='self';
						break;
				default: 
						throw new Exception("Undefined Exam Type");
			}
			
			if($orgid==-1)
				$orgid = '10000002';
			
			if($type=='unplanned_quiz' ||  $type=='planned_quiz')
			{
				if($classid=='-1')
					throw new Exception('Classid Not Provided');
			}else{
				if($subjectid=='-1')
					throw new Exception('Subjectid Not Provided');
			}
				
			if($type!='aptitude' && $studentid=='-1')
					throw new Exception('Studentid Not Provided');
				
			if($studentid=='-1')
					$studentid=false;
				
		$link = $this->model->get_test_link($type,$orgid,$classid,$subjectid,$studentid);	
		
		//echo json_encode(array('type'=>'success','links'=>$link));
		echo $link;
    	
	  }catch(Exception $e){
		  echo $e->getMessage(); 
		  //json_encode(array('type'=>'fail','msg'=>$e->getMessage()));
	  }
	}
}
?>