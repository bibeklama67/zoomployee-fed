<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require('pusher/lib/Pusher.php');

class Liveteachers extends Liveteacher_controller {

  function __construct()
  {
    parent::__construct();
    $d=$this->config->item('default');
    $df=$this->config->item($d); 

    // $df['js'][]='common/student_api';    
    $df['js'][]='liveteachers/liveteachers';
    $df['js'][]='common/pusher.min';
    $this->config->set_item($d,$df);

    $this->simsdb =  $this->load->database('sims',TRUE);
    $this->load->model('liveteacher_model','model');
   // $this->load->helper('image_helper');
    $this->AUTHKEY = '7974b0a1667033baa718';
    $this->SECRET =  '7f9a3b6eacaba743e07a';
    $this->APPID = '360756';
    $this->CLUSTER = 'ap1';

    // ini_set('display_errors', 1);
    // ini_set('display_startup_errors', 1);
    // error_reporting(E_ALL); 
  }
  public function index()
  {      
    if($this->session->userdata('isteacherloggedin'))
    {
      // $url=$this->session->userdata('redirectToCurrent');
      // $this->session->unset_userdata('redirectToCurrent');
      // redirect($url);

      redirect(base_url().'liveteachers/sessions');
    }
    $this->load->view('liveteachers/login/front_view');
  }

  public function login_false()
  {
    $this->load->vars(array('message'=>"username or password doesnot match."));
    $this->load->view('liveteachers/login/front_view');    
  }

  public function loginsuccess($usercode){
    $user = $this->simsdb->get_where('org_midasuser',array('usercode'=>$usercode))->row();

    $this->session->set_userdata('isteacherloggedin','1');
    $this->session->set_userdata('teacheruserid', $user->userid);
    $this->session->set_userdata('teachername', $user->name);
    redirect('liveteachers/sessions');
  }

  public function logout()
  {
    $this->session->sess_destroy();
    redirect('liveteachers');
  }

  function sessions(){
   $teachersession = $this->model->getTeacherSessions($this->session->userdata('teacheruserid'));
   $this->load->vars(array('sessions'=>$teachersession,'view'=>'liveclass_wrapper'));
   $this->render_page('liveteachers_view');   
 }

 function whiteboard()
 {
  $data = $this->input->post();

  $user = $this->simsdb->select('liveclassuserid')->get_where('forum.el_teachers',array('userid'=>$this->session->userdata('teacheruserid')))->row();
  $data['userid'] = $user->liveclassuserid;  
  $question = $this->model->livecurrent_question($data['roomid']);
  $room = $this->model->getroomdetails($data['roomid']);

  $teacherprofile = $this->simsdb->select("currentschoolid1 as currentschool,pastschoolid2 as pastschoolid1,pastschoolid3 as pastschoolid2,firstname,lastname, education, numberofyearsofteaching,numberofmonthsteaching,experience,
    teachingsubjects",false)->get_where('forum.el_teachers t',array('userid'=>$this->session->userdata('teacheruserid')))->row();
  // print_r($teacherprofile);
  $this->model->logliveclassteacher($data['roomid']);
  $data['question'] = $question;
  $this->load->vars(array(
    'data' => $data
    ));
  $data['current_question'] = $this->load->view('livecur_question','',true);
  $this->load->vars(array(
    'data'         =>    $data,
    'teacher'       => $teacherprofile
    ));
  $view = $this->load->view('scribblar_frame','',true); 
  echo json_encode(array('view'=>$view,'room'=>$room));
}

function current_question($sessionid){
  $question = $this->model->livecurrent_question($sessionid);
  $data['question'] = $question;
  $this->load->vars(array(
    'data' => $data
    ));
  $this->load->view('livecur_question');
}

function viewaskedby(){
  $data = $this->input->post();

  $this->load->vars(array(
    'response'=>$data['response'],
    ));
  $this->load->view('list_view');
}


function setbreakmins()
{
 $this->db->set('breaktill',"now() + interval '".$this->input->post('breakmins')." minute'",false);
 $this->db->set('breakmins',$this->input->post('breakmins'));
 $id = $this->db->where(array('sessionid'=>$this->input->post('sessionid')))->update('ml_live_sessions');
 return $id;
}

function pauselivesession()
{
  //  $this->db->set('breaktill',"now() + interval '".$this->input->post('breakmins')." minute'",false);
  // $this->db->set('breakmins',$this->input->post('breakmins'));
  $id = $this->db->update('ml_live_sessions',array('ispaused'=>'t'),array('sessionid'=>$this->input->post('sessionid')));

  return $id;
}

function endbreak()
{
  $id = $this->db->where(array('sessionid'=>$this->input->post('sessionid')))->update('ml_live_sessions',array('ispaused'=>'f'));
  return $id;
}

function startbreak()
{
  $id = $this->db->where(array('sessionid'=>$this->input->post('sessionid')))->update('ml_live_sessions',array('ispaused'=>'t'));
  return $id;
}

}
