<?php 
if(!defined('BASEPATH')) exit('direct access invalid');

class Liveteacher_model extends CI_Model
{

  function __construct()
  {
    parent::__construct();
    $this->simsdb =  $this->load->database('sims',TRUE);

  }


  function getTeacherSessions($userid)
  {

    $sessions = $this->db->select('c.classname,s.subjectname,ls.*')->join('el_class c','c.classid = ls.classid')->join('el_subject s','s.subjectid = ls.subjectid')->join('ml_livesession_users u','u.livesessionid = ls.livesessionid')->get_where('ml_live_sessions ls',array('u.userid'=>$userid))->result();
    return $sessions;
  }

  function livecurrent_question($sessionid){
    $query = $this->simsdb->query("select *,to_char(dataposteddatetime,'YYYY-MM-DD') || ' ' || trim(to_char(dataposteddatetime, 'HH12:MI AM'), '0') as curtime From forum.el_questionsof_students where sessionid = '$sessionid' and liveansstart is not null and liveansend is null and isactive = 'Y' and status ='Y'");
    $question = $query->row();
    return $question;
  }

  function getroomdetails($roomid)
  {
    // $this->db->update('ml_live_sessions',array('ispaused'=>null,'breakmins'=>null),array('sessionid'=>$roomid));
    $room = $this->db->select('breaktill,breakmins,ispaused')->get_where('ml_live_sessions',array('sessionid'=>$roomid))->row();
    return $room;
  }

  function logliveclassteacher($roomid)
  {
    $room = $this->db->select('DISTINCT sessionid,sessionname,ispaused,subjectid',false)->join('ml_livesession_schedule sc','sc.livesessionid = ls.livesessionid')->where('EXTRACT (DOW FROM now()) = sc.weekday AND CURRENT_TIMESTAMP (0) :: TIME BETWEEN sc.starttime AND sc.endtime')->get_where('ml_live_sessions ls',array('sessionid'=>$roomid))->row();

    if($room->ispaused=='f')
    {
      $logarray = array('sessionid'=>$roomid,
        'subjectid'=>$room->subjectid,
        'userid'=>$this->session->userdata('teacheruserid'),
        'usertype' => 'TEACHER');
      $this->db->insert('el_progress_liveclass',$logarray);
    }
  }
}

