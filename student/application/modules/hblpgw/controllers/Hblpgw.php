<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hblpgw extends App_controller{
	function __construct(){

		parent::__construct();
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);
		//error_reporting(E_ALL);
		// $d=$this->config->item('default');
		// $df=$this->config->item($d); 
		// $df['js'][]='common/student_api';

		// $df['js'][]='subscription/subscription';
		// $this->config->set_item($d,$df);
		// $this->load->model('subscription_model','model');
		$this->HBLMERCHANTID = '000009101430064';
		$this->HBLSECRET = 'PGG8GTGCQKUIFX45WWI8W6CPF6A866N7';
	}	

	public function index(){
		redirect(base_url().'hblpgw/pay');		
	}

	public function pay(){
		$invoiceno = 'midastest'.rand();
		$hashvalue = $this->hblhash($this->HBLMERCHANTID.$invoiceno.'000000000100'.'524'.'Y');
		$this->db->insert('test_log_tbl',array('text'=>$hashvalue,'type'=>'request'));

		$this->load->vars(array('hashvalue'=>$hashvalue,'invoiceno'=>$invoiceno));
		$this->load->view('payment');
	}

	function hblhash($signatureString)
	{
		$signData = hash_hmac('SHA256', $signatureString,'PGG8GTGCQKUIFX45WWI8W6CPF6A866N7', false);
		$signData = strtoupper($signData);
		return urlencode($signData);
	}

}