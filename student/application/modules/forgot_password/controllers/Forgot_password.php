<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forgot_password extends MX_Controller {

	function __construct()
	{
		parent::__construct();
      $d=$this->config->item('default');
      $df=$this->config->item($d); 
      $df['js'][]='login/login';
      $df['js'][]='signup/teacher_api';
      $df['js'][]='signup/teacher-signup';
      $df['js'][]='signup/parent-signup';
      $this->config->set_item($d,$df);
   }

   public function index()
   {
      echo 'hello';
      $this->load->view('forgot_password');
   }
}