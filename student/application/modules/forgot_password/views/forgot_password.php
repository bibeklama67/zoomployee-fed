
<?=doctype('html')?>

<html dir="ltr" lang="en-US">
<head>
<title>MiDas Apps</title>
<script>
  var base_url= "<?= base_url();?>";
</script>
<?php 
  $this->config->load('site_setting');
    $default=$this->config->item('default');
    $default_items=$this->config->item($default);
// print_r($default_items);
    $title=element('title',$default_items);

?>
<style type="text/css">
  .overlay {
    position: absolute;
    top: 0;
    left: 0;
    text-align: center;
    background: rgba(0,0,0,0.3);}

    .overlay img {
    position: fixed;
    top: 35%;
    left: 50%;
}

.btn-navy{
  padding: 8px 30px !important;
  }
</style>
<?php
      echo meta('content-type','text/html; charset=utf-8','equiv');
        $common_items=$this->config->item('common');
   
        $common_meta=element('meta',$common_items);
        $default_meta=element('meta',$default_items);
        if($common_meta && $default_meta)
          $meta=array_merge($common_meta,$default_meta);        
        else if($common_meta)
          $meta=$common_meta;
        else
          $meta=$default_meta;
          
        if(isset($meta) && is_array($meta))       
            foreach($meta as $k=>$v)          
              echo meta($k,$v); 
    ?>
<link rel="icon" type="image/png" href="http://midas.com.np/images/favicon.ico">    <?php
        $common_css=element('css',$common_items);
        $default_css=element('css',$default_items);

  
        if(isset($common_css) && is_array($common_css))
        {
          $fold=element('folder',$common_css);
          if($fold)

          foreach($fold as $f)
          foreach (glob("assets/css/$f/*.css") as $filename)
          {
            // $filename=ltrim($filename,"assets/");

            $link_arr = array('href'=>$filename,
                'rel' =>'stylesheet',
                'type'=>'text/css'
              );

            echo link_tag($link_arr); 
          }
            
          unset($common_css['folder']); 
                
          foreach($common_css as $c):
            $link_arr = array('href'=>"assets/css/common/$c.css",
              'rel' =>'stylesheet',
              'type'=>'text/css'
            );
            echo link_tag($link_arr);
          endforeach;
          
        }
        
        unset($link_arr); unset($common_css); 
        
        if(isset($default_css) && is_array($default_css))
        {
     
          $fold=element('folder',$default_css);
          if($fold)
          foreach($fold as $f)
          foreach (glob("assets/css/$f/*.css") as $filename)
          {

            // $filename=ltrim($filename,"assets/");
           
            $link_arr = array('href'=>$filename,
                'rel' =>'stylesheet',
                'type'=>'text/css'
              );
            echo link_tag($link_arr); 
          }
          unset($default_css['folder']);
                

          foreach($default_css as $c):
            $link_arr = array('href'=>"assets/css/$default/$c.css",
              'rel' =>'stylesheet',
              'type'=>'text/css'
            );
            echo link_tag($link_arr);
          endforeach;
        }
          unset($link_arr); unset($default_css);                    
    ?>  
    
    <?php 
        $common_js=element('js',$common_items);
        $default_js=element('js',$default_items);
                
        if(isset($common_js) && is_array($common_js)):
        
          $fold=element('folder',$common_js);
          if($fold):
            foreach($fold as $f):
              foreach (glob("assets/js/$f/*.js") as $filename):
                // $filename=ltrim($filename,"assets/");
                $j_arr = array('type'=>'text/javascript',
                         'src' =>$filename);
                echo script_tag($j_arr);
              endforeach;
            endforeach;
          endif;
          
          unset($common_js['folder']);
          
          foreach($common_js as $j):
            $j_arr = array('type'=>'text/javascript',
                     'src' =>"assets/js/common/$j.js");
            echo script_tag($j_arr);
          endforeach;
          
        endif;
        
        unset($j_arr);unset($common_js);
        
        if(isset($default_js) && is_array($default_js)):
        
          $fold=element('folder',$default_js);
          if($fold):
            foreach($fold as $f):
              foreach (glob("assets/js/$f/*.js") as $filename):
                // $filename=ltrim($filename,'assets/');
                $j_arr = array('type'=>'text/javascript',
                         'src' =>$filename);
                echo script_tag($j_arr);
              endforeach;
            endforeach;
          endif;
          
          unset($default_js['folder']);
                
          foreach($default_js as $j):
            $j_arr = array('type'=>'text/javascript',
                     'src' =>"assets/js/$default/$j.js");
            echo script_tag($j_arr);
          endforeach;
        
        endif;
        unset($j_arr);unset($default_js);
    ?>  
  </head>
<body style="height: 97%;">
   <?php
          $this->load->view('header');
              // $this->load->view('templates/header/navigation');  
              ?>
	<div class="container-fluid pad-fix">  

          
    <div class="jumbotron" style="position: relative;">
      <h1 style="color:#f27b06; padding:50px; text-align: center; font-size: -webkit-xxx-large;"><em> Where Learning Begins </em> </h1>
      <!-- <img src="<?php echo base_url();?>assets/images/background.jpg" class="img-responsive mar_auto">  -->
      <div class="text-center">
        <p style="color:#5c3100;font-size: 26px;margin-bottom: 10;">Forgot Password?</p>
       <span> <a href="#" class="btn btn-navy">I'm a Parent</a> <a href="#" class="btn btn-navy  ">I'm a Teacher</a></span>
     </div>
  </div>

<!-- body part end -->  
<!-- </body> -->
<script type="text/javascript" src="assets/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>


<!-- </html> -->
  <style> 
    
/* Smartphones (portrait and landscape) ----------- */
@media only screen and (min-device-width : 320px) and (max-device-width : 480px) {
/* Styles */
   h1{
     padding: 20px !important;
   }

}
  
</style>
                
         
              <div class="clear"></div>
                <div  id='aj_img' class="hidden" style="display:none" >
                   <?=img(image_url().'ajax-loader.gif')?>  Looking Up
                </div>
               <?=anchor("","","class='base_path hidden'")?>
                <div class="coverdiv overlay" style="display:none">
                     <img src="<?php echo image_url().'loaders.gif'?>">
                </div>
                <div class="error" style="display:none !important">
                   <?php $this->load->view('templates/body/error');?>
                </div>
             
  </div>
 <?php
              $this->load->view('templates/footer/footer.php');
              // $this->load->view('templates/header/navigation');  
              ?>
</body>
</html>
