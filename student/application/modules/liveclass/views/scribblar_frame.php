<LINK href="//s3.amazonaws.com/media.muchosmedia.com/scribblar/styles/style.css" rel="stylesheet" type="text/css">
  <style type="text/css">
    footer {
      position: relative !important;
    }
    .custom_teacherprofile ul{
      text-align: left;
      padding-left: 20px;
    }

    .custom_teacherprofile h5{
      font-size: 14px;
      font-weight: bold;
    }
  </style>
  <script type="text/javascript" src="//s3.amazonaws.com/media.muchosmedia.com/scribblar/scripts/includes.js"></script>
  <div class="current_question" style="display: none"><?= $data['current_question']?></div>

  <script type="text/javascript">
   //  var duration_interval;
   //  $(document).ready(function(){
   //   question_interval = window.setInterval(function() {
   //    var duration = 30000;      
   //  }, 3000);
   // });

   function swfLoadEvent(fn){
    //Ensure fn is a valid function
    if(typeof fn !== "function"){ return false; }
    //This timeout ensures we don't try to access PercentLoaded too soon
    var initialTimeout = setTimeout(function (){
        //Ensure Flash Player's PercentLoaded method is available and returns a value
        if(typeof e.ref.PercentLoaded !== "undefined" && e.ref.PercentLoaded()){
            //Set up a timer to periodically check value of PercentLoaded
            var loadCheckInterval = setInterval(function (){
                //Once value == 100 (fully loaded) we can do whatever we want
                if(e.ref.PercentLoaded() === 100){
                    //Execute function
                    fn();
                    //Clear timer
                    clearInterval(loadCheckInterval);
                  }
                }, 1500);
          }
        }, 200);
  }

  var targetID = "scribblar";
  var flashvars = {};
  /* pass necessary variables to the SWF */
  flashvars.userid = "<?= $data['userid'] ?>";
  flashvars.username = "<?= $data['username'] ?>";
  flashvars.roomid = "<?= $data['roomid'] ?>";
  flashvars.preferredLocales = "en_US";

  var params = {};
  params.allowscriptaccess = "always";
  params.allowFullScreenInteractive = "true";

  var attributes = {};
  attributes.id = "scribblar";
  attributes.name = "scribblar";

  var callback = function (e){

    //Only execute if SWFObject embed was successful
    if(!e.success || !e.ref){ return false; }

    var initialTimeout = setTimeout(function (){
        //Ensure Flash Player's PercentLoaded method is available and returns a value
        if(typeof e.ref.PercentLoaded !== "undefined" && e.ref.PercentLoaded()){
            //Set up a timer to periodically check value of PercentLoaded
            var loadCheckInterval = setInterval(function (){
                //Once value == 100 (fully loaded) we can do whatever we want
                if(e.ref.PercentLoaded() === 100){
                  // $('.custom_teacherprofile').show();
                    //Execute function
                    // fn();
                    //Clear timer
                    clearInterval(loadCheckInterval);
                  }
                }, 1500);
          }
        }, 200);

  };

  swfobject.embedSWF("//s3.amazonaws.com/media.muchosmedia.com/scribblar/v4/main.swf", "alternate", "100%", "100%", "11.1.0", "//s3.amazonaws.com/media.muchosmedia.com/swfobject/expressInstall.swf", flashvars, params, attributes, callback);
</script>
<div id="alternate"> <a href="//www.adobe.com/go/getflashplayer">This page requires the latest version of Adobe Flash. Please download it now.<br>
  <img src="//wwwimages.adobe.com/www.adobe.com/images/shared/download_buttons/get_flash_player.gif" border="0" alt="Get Adobe Flash Player" /> </a> </div>

  <!-- <div style="position: absolute;top: 215px;right: 0px;height: 210px;width: 240px;background: #fff; display: none; overflow-y: scroll;" class="custom_teacherprofile">
    <h4 style="text-align: center;">Your Teacher</h4>
    <?php 
    // print_r($teacher);
    if(@$teacher){
      $yearsteaching = ($teacher['numberofmonthsteaching']?$teacher['numberofmonthsteaching'].' Months':'').($teacher['numberofyearsofteaching']?$teacher['numberofyearsofteaching'].' Years':'');?>
      <p style="text-align: center;"><strong><?= $teacher['firstname'].' '.$teacher['lastname'] ?></strong></p>
      <div style="text-align: center;"><?= $yearsteaching?$yearsteaching.' of teaching experience':'N/A' ?></div>
      <?php if($teacher['currentschool']){?>
      <div style="text-align: center;"><p class="pri-bold">Currently Teaching at</p>
        <p class="sec-bold modal-teacher-currentschool"><?= ($teacher['currentschool']?$teacher['currentschool']:'N/A')?></p>
      </div>
      <?php }?>
      <?php }?>
    </div> -->