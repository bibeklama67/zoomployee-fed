<style>
  h5.bread{background:#eef1f6; font-weight:bold; padding:10px 5px; margin:2px 5px; color:#418755 !important;}
  .progressbar{padding:0;}
  .bread_wrap{border-bottom:1px solid #dfe4e8 !important;}
  h5 span.active{color:#526b81;}
  ul.chaps_list li{list-style:none; background:#f1f1f1;padding: 5px 10px;
    margin: 2px auto;}
    ul.chaps_list li:nth-child(even){
      background:#fbfbfb;
    }
    .progress{margin-top:5px;}
    .mar-top-10{margin-top:10px;}
    img.ads{
      border:1px solid #d5d5d5; margin:0 auto;
    }
    span.pro_per{color:#d57656;}
    .wrap-lcb{
      border:1px solid #ddd;
    }

    #cke_questionecktext{
      width: 100% !important;
    }
    .bread.custmart01{
      margin:2px;
      cursor: pointer;
    }
    @media (min-width: 768px) {
      .modal-xl {
        width: 90%;
        max-width:1200px;
      }
    }
    #liveqmodal { overflow: auto !important; }

  </style>
  <style type="text/css">
    .image_preview {
      width: 70px;
      margin-left: 10px;
      border-radius: 3px;
      display: none;
    }
    .target {
      border: solid 1px #aaa;
      min-height: 200px;
      width: 30%;
      margin-top: 1em;
      border-radius: 5px;
      cursor: pointer;
      transition: 300ms all;
      position: relative;
    }

    .contain {
      background-size: cover;
      position: relative;
      z-index: 10;
      top: 0px;
      left: 0px;
    }
    textarea {
      background-color: white;
    }
    .active_copypaste {
      box-shadow: 0px 0px 10px 10px rgba(0,0,255,.4);
    }
    
    .btn.fa.fa-pencil-square-o{

      font-size: 33px;
      margin-top: -7px;
      padding: 0;
    }

    pre{
      z-index: -10;
      display: none !important;
      visibility: hidden !important;
    }
    .inputapi-transliterate-indic-suggestion-menu-vertical{
      display: none !important;
    }

    .postcreation-header{
      padding: 0 !important;
      margin: 0 !important;
      background: #FFF;
      height: 34px;
      border-top-right-radius: 4px;
      border-top-left-radius: 4px;
    }

    .postcreation-title{
      width: 50%;
      padding: 0px 10px;
      display: block;
      float: left;
      color: #365899 !important;
      font-weight:600;
    }

    .postcreation-lang{
      float: right;
      width:100px;
      border-radius: 0;
      border: none;
    }

    .postcreation-textarea{
      border:none;
      border-bottom-left-radius: 4px;
      border-bottom-right-radius: 4px;
      margin-bottom: 10px;
      padding: 5px 10px;
    }
    .error{
      color:#f00;
    }
    #edit_question_error{
      float: left;
    }


  </style>

  <?php if(empty($data))
  echo 'No Chapters Available';
  else{

    ?>  
    <div class="qanda" id="live-chaps">
      
      <div class="clearfix giveheight"></div>
      <div class="row">
      <div class="col-xs-12">
      <?php 
      $response=$data['response'];

      $chap=$response['chapques'];
      $curlive = $response['currentlylive'];

      $a=1;
      if($curlive){
      ?>
      <!-- currently live question -->

      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 15px;">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading43128">
              <div class="row panel-heading-up">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border-bottom: 1px solid #cecfff;">
                  <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 no-padding">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 user-date-2917">
                      <a href="javascript:;" class="forum-actions" data-actiontype="askedby" data-refid="<?php echo $curlive['questionsofstudentsid'];?>"><i class="fa fa-user-circle"></i></a>&nbsp;<small style="color: #aaa;"><?php echo $curlive['dataposteddatetime'];?></small>          
                    </div>
                    <div class="question-points-wrapper col-md-9">
                      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 pad-fix" style="margin-top: -5px;">
                        <div class="col-xs-12">
                          <a href="javascript:;" class="forum-actions" style="margin-left:10px; float:right;"><i class="fa fa-eye"></i>
                           <span style="color:black;"><big><?php echo $curlive['views']?></big></span>
                         </a>
                       </div>
                     </div>
                     <div class="col-lg-10 col-md-6 col-sm-6 col-xs-6 pad-fix">                          
                       <div class="col-xs-12 indicator-bulb">

                        <strong style="color: red; float:right;">Currently LIVE</strong>
                        
                      </div>  
                    </div>
                  </div>
                </div>
                <div class="col-xs-1">
                  <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $curlive['questionsofstudentsid'];?>" aria-expanded="false" aria-controls="collapse<?php echo $curlive['questionsofstudentsid'];?>"  data-forumid="<?php echo $curlive['questionsofstudentsid'];?>" class="get_answers"><i class="arrow-down"></i></a>
                </div>
              </div>
              <div class=" col-md-12 col-lg-12 col-xs-12 questions-here">      
                <div class="col-md-10">
                 <p style="color:#0c50a3; font-size: 12px; font-weight:normal; padding-top:5px;"><strong><span id="questiontext<?= $curlive['questionsofstudentsid']?>"><?= $curlive['questiontext']?></span></strong>
                  <?php
                  if($curlive['questiontext']){
                    ?>
                    <input type="hidden" id="posttext" value="<?= $curlive['questiontext']?>">
                    <a href="javascript:;" id="speak" class="waves-effect waves-light btn speak" data-id="<?php echo $curlive['questionsofstudentsid'];?>"></a> 
                    <?php
                  }
                  ?></p>      
                </div>
                <div class="col-md-2">          
                </div>
                <?php 
                if($curlive['questionimage']){
                  ?>
                  <a href="<?php echo $curlive['questionimage'];?>" target="_blank">
                    <img class="img-responsive" src="<?php echo $curlive['questionimage']; ?>" style="width:20%" id="questionimage<?php echo $curlive['questionsofstudentsid'];?>">
                  </a>
                  <?php
                }
                ?>
              </div>
            </div>
          </div>
          <div id="collapse<?php echo $curlive['questionsofstudentsid'];?>" class="panel-collapse collapse answer-body" role="tabpanel" aria-labelledby="heading01">
            <div class="panel-body" style="padding: 0px;">
              <div class="panel-body-up answers-wrapper" style="border-bottom: 2px solid #cecfff;">
                <div class="answers-list row">

                </div>
              </div>

            </div>    
          </div>
        </div>
      </div>
    </div>

    <?php
  }
    foreach ($chap as $key => $value) { ?>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pad-fix sec-block-bg give-border">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pad-fix bread_wrap">
        <h5 class="bread"><span class="active"><a class="subjectbtn breadcrumb"><?php  echo $value['chaptername'];?></a></span>
        </h5>
      </div>
      <?php

      $ques=$chap[$key]['questions'];
      foreach ($ques as $value2) {
       $qid=$value2['questionsofstudentsid'];
       ?>
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading43128">
              <div class="row panel-heading-up">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border-bottom: 1px solid #cecfff;">
                  <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 no-padding">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 user-date-2917">
                      <a href="javascript:;" class="forum-actions" data-actiontype="askedby" data-refid="<?php echo $value2['questionsofstudentsid'];?>"><i class="fa fa-user-circle"></i></a>&nbsp;<small style="color: #aaa;"><?php echo $value2['dataposteddatetime'];?></small>          
                    </div>
                    <div class="question-points-wrapper col-md-9">
                      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 pad-fix" style="margin-top: -5px;">
                        <div class="col-xs-12">
                          <a href="javascript:;" class="forum-actions" style="margin-left:10px; float:right;"><i class="fa fa-eye"></i>
                           <span style="color:black;"><big><?php echo $value2['views']?></big></span>
                         </a>
                       </div>
                     </div>
                     <div class="col-lg-10 col-md-6 col-sm-6 col-xs-6 pad-fix">                          
                       <div class="col-xs-12 indicator-bulb">
                        <?php
                        if($value2['currentlylive']=='Y')
                        {


                          ?>
                          <strong style="color: red; float:right;">LIVE NOW !</strong>
                          <?php
                        }
                        if($value2['hasanswer']=='Y')
                        {

                          ?>
                          <strong style="color:#024ea2; float:right;">Answered</strong>
                          <?php
                        }
                        else
                          { ?>
                        <strong style="color: red; float:right;">Unanswered</strong>
                        <?php
                      } ?>

                    </div>  
                  </div>
                </div>
              </div>
              <div class="col-xs-1">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $value2['questionsofstudentsid'];?>" aria-expanded="false" aria-controls="collapse<?php echo $value2['questionsofstudentsid'];?>"  data-forumid="<?php echo $value2['questionsofstudentsid'];?>" class="get_answers"><i class="arrow-down"></i></a>
              </div>
            </div>
            <div class=" col-md-12 col-lg-12 col-xs-12 questions-here">      
              <div class="col-md-10">
               <p style="color:#0c50a3; font-size: 12px; font-weight:normal; padding-top:5px;"><strong><span id="questiontext<?= $qid?>"><?= $value2['questiontext']?></span></strong>
                <?php
                if($value2['questiontext']){
                  ?>
                  <input type="hidden" id="posttext" value="<?= $value2['questiontext']?>">
                  <a href="javascript:;" id="speak" class="waves-effect waves-light btn speak" data-id="<?php echo $value2['questionsofstudentsid'];?>"></a> 
                  <?php
                }
                ?></p>      
              </div>
              <div class="col-md-2">          
              </div>
              <?php 
              if($value2['questionimage']){
                ?>
                <a href="<?php echo $value2['questionimage'];?>" target="_blank">
                  <img class="img-responsive" src="<?php echo $value2['questionimage']; ?>" style="width:20%" id="questionimage<?php echo $value2['questionsofstudentsid'];?>">
                </a>
                <?php
              }
              ?>
            </div>
          </div>
        </div>
        <div id="collapse<?php echo $value2['questionsofstudentsid'];?>" class="panel-collapse collapse answer-body" role="tabpanel" aria-labelledby="heading01">
          <div class="panel-body" style="padding: 0px;">
            <div class="panel-body-up answers-wrapper" style="border-bottom: 2px solid #cecfff;">
              <div class="answers-list row">

              </div>
            </div>

          </div>    
        </div>
      </div>
    </div>
  </div>

  <?php

} ?>
</div>

<?php
}
?>
</div>
<?php }?>

</div>
</div>



<script>
  $('.collapse').on('shown.bs.collapse', function(){
    $(this).parent().find(".arrow-down").removeClass("arrow-down").addClass("arrow-up");
  }).on('hidden.bs.collapse', function(){
    $(this).parent().find(".arrow-up").removeClass("arrow-up").addClass("arrow-down");
  });
</script> 
<script type="text/javascript">
  $(document).off('click','#copypastebutton');
  $(document).on('click', '#copypastebutton', function(){
    if($('#question_popup_image').attr('src')){
      $('.error_btn_disable').text('You can only upload one media at a time.');
      return false;
    }
    if($(this).is('[disabled=disabled]')){
      $('.error_btn_disable').text('Please fill the question text before selecting any media.');
      return false;
    }
    $('#error_message').text('');
    $('#copyimagepaste').addClass('active_copypaste contain');
    $('#copypaste-modal').modal('show');
  });


  $(document).off('click','#ckeditor');
  $(document).on('click','#ckeditor', function(){
    $('#questiontext').val('');
    $('#nepaliquestiontext').val('');
    CKEDITOR.replace('questionecktext');
    $('#ckeditor1-modal').modal('show');

  });

  $(document).off('click','#webcambutton');
  $(document).on('click','#webcambutton', function(e){

    if($('#question_popup_image').attr('src')){
      $('.error_btn_disable').text('You can only upload one media at a time.');
      return false;
    }
    if($(this).is('[disabled=disabled]')){
      $('.error_btn_disable').text('Please fill the question text before selecting any media.');
      return false;
    }

    $.post(base_url+'forum/webcam',function(html){

      $('#webcam .modal-body').html(html);
      $('#webcam').modal('show');
    });
  });

  $("button[data-number=1]").click(function(){
    $('#copypaste-modal').modal('hide');
  });

  $("button[data-number=2]").click(function(){
    $('#action-data').modal('hide');
  });

  $("button[data-number=3]").click(function(){
    $('#ckeditor1-modal').modal('hide');
  });

  $("button[data-number=4]").click(function(){
    $('#image-popup').modal('hide');
  });
  $("button[data-number=5]").click(function(){
    $('#webcam').modal('hide');
  });

</script>
<div class="modal fade" id="copypaste-modal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close close-pasteimage" data-number="1">&times;</button>
        <h4 class="modal-title">Paste Image</h4>
      </div>

      <div class="modal-body">
        <span id="error_message"></span>
        <div class="copy_paste_image">
          <div class="row">
            <div class="span4 target" id="copyimagepaste" style="float: left; margin-left: 20px; width: 93%;"></div>
            <input type="hidden" name="copyimage" id="copyimage">
          </div>
          <!-- <a href="" id="test" target="_blank" style="float: left;">See image in new window</a> -->
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default close-pasteimage" data-number="1">Close</button>
        <button style="margin-top: -3px; background: #4266b2;" type="button" id="save-copypaste" data-dismiss="modal" class="btn btn-success btn-send">Post</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="action-data" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-number="2">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body views-wrapper">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-number="2">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="ckeditor1-modal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close ckeditor-close" data-number="3">&times;</button>
        <h4 class="modal-title">Ask a Question</h4>
      </div>
      <div class="modal-body">
        <textarea id="questionecktext" class="questionecktext"></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default ckeditor-close" data-number="3">Close</button>
        <button style="margin-top: -3px; background: #4266b2;" type="button" id="ckeditor_done" data-dismiss="modal" class="btn btn-success btn-send">Post</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="image-popup" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close close-image" data-number="4">&times;</button>
        <h4 class="modal-title">Image Attachment</h4>
      </div>
      <div class="modal-body">
        <img id="question_popup_image" class="image_preview img-responsive">
      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-default close-image" data-number="4">Close</button>
        <!-- <a href="javascript:void(0);" class="btn btn-success post-answer" data-dismiss="modal" data-questionid="<?php echo $questionsofstudentsid?>">POST</a> -->
        <input name="submit" value="Post" style="background: #4266b2;" class="btn btn-success btn-send post-question" data-dismiss="modal" type="submit">
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="webcam" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-number="5">&times;</button>
        <h4 class="modal-title">Camera</h4>
      </div>
      <div class="modal-body " style="height: 515px;">

      </div>
      <div class="modal-footer" style="padding: 10px !important;">

        <button style="margin-top: -3px; background: #4266b2;" type="button" id="save-webcam" class="btn btn-success btn-send">Post</button><!-- 
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
      </div>
    </div>
  </div>
</div>

<script>
//for the copy paste of the image

(function($) {
  var defaults;
  $.event.fix = (function(originalFix) {
    return function(event) {
      event = originalFix.apply(this, arguments);
      if (event.type.indexOf('copy') === 0 || event.type.indexOf('paste') === 0) {
        event.clipboardData = event.originalEvent.clipboardData;
      }
      return event;
    };
  })($.event.fix);
  defaults = {
    callback: $.noop,
    matchType: /image.*/
  };
  return $.fn.pasteImageReader = function(options) {
    if (typeof options === "function") {
      options = {
        callback: options
      };
    }
    options = $.extend({}, defaults, options);
    return this.each(function() {
      var $this, element;
      element = this;
      $this = $(this);
      return $this.bind('paste', function(event) {
        var clipboardData, found;
        found = false;
        clipboardData = event.clipboardData;
        return Array.prototype.forEach.call(clipboardData.types, function(type, i) {
          var file, reader;
          if (found) {
            return;
          }
          if (type.match(options.matchType) || clipboardData.items[i].type.match(options.matchType)) {
            file = clipboardData.items[i].getAsFile();
            reader = new FileReader();
            reader.onload = function(evt) {
              return options.callback.call(element, {
                dataURL: evt.target.result,
                event: evt,
                file: file,
                name: file.name
              });
            };
            reader.readAsDataURL(file);
            return found = true;
          }
        });
      });
    });
  };
})(jQuery);



$("html").pasteImageReader(function(results) {
  var dataURL, filename;
  filename = results.filename, dataURL = results.dataURL;
  $data.text(dataURL);
  $size.val(results.file.size);
  $type.val(results.file.type);
  $test.attr('href', dataURL);
  var img = document.createElement('img');
  img.src= dataURL;
  var w = img.width;
  var h = img.height;
  $width.val(w)
  $height.val(h);
  var src = dataURL.replace('data:image/png;base64,','')
  $("#copyimage").val(src);
  return $('.active_copypaste').html('<img src="'+dataURL+'" style="width:100%">');
});

var $data, $size, $type, $test, $width, $height;
$(function() {
  $data = $('.data');
  $size = $('.size');
  $type = $('.type');
  $test = $('#test');
  $width = $('#width');
  $height = $('#height');
  $('.target').on('click', function() {
    var $this = $(this);
    var bi = $this.css('background-image');
    if (bi!='none') {
      $data.text(bi.substr(4,bi.length-6));
    }


    $('.active_copypaste').removeClass('active_copypaste');
    $this.addClass('active_copypaste');

    $this.toggleClass('contain');

    $width.val($this.data('width'));
    $height.val($this.data('height'));
    // if ($this.hasClass('contain')) {
    //  $this.css({'width':$this.data('width'), 'height':$this.data('height'), 'z-index':'10'})
    // } else {
    //  $this.css({'width':'', 'height':'', 'z-index':''})
    // }

  })
})

</script>


<script type="text/javascript">
  $(document).ready(function(){
    pramukhIME.addKeyboard("PramukhIndic");
    pramukhIME.enable();  

  });
  

</script>

