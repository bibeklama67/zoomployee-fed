<style>
  h5.bread{background:#eef1f6; font-weight:bold; padding:10px 5px; margin:2px 5px; color:#418755 !important;}
  .progressbar{padding:0;}
  .bread_wrap{border-bottom:1px solid #dfe4e8 !important;}
  h5 span.active{color:#526b81;}
  ul.chaps_list li{list-style:none; background:#f1f1f1;padding: 5px 10px;
    margin: 2px auto;}
    ul.chaps_list li:nth-child(even){
      background:#fbfbfb;
    }
    .progress{margin-top:5px;}
    .mar-top-10{margin-top:10px;}
    img.ads{
      border:1px solid #d5d5d5; margin:0 auto;
    }
    span.pro_per{color:#d57656;}
  </style>
  <?php  $subject = explode('-', $subjectname); ?>

  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pad-fix bread_wrap">
    <h5 class="bread"> <a href="javascript:;" data-classid="<?php echo $data['classid']; ?>" class="classbtn breadcrumb">Grade <?php echo isset($data['classname'])?$data['classname']:''?><input type="hidden" id="homework_classid" value="<?php echo $data['classid']; ?>"></a><i class="fa fa-chevron-right"></i>
      <a href="javascript:;" class="subjectbtn breadcrumb" data-subjectname="<?= $data['subjectname']?>" data-subjectid="<?= $data['subjectid']?>" data-classname="<?php echo isset($data['classname'])?$data['classname']:''?>"><?php echo isset($data['subjectname'])?explode('-',$data['subjectname'])[0]:''?></a> <i class="fa fa-chevron-right"></i> 
      <span class="active"> <?php echo  isset($data['chaptername'])?$data['chaptername']:''?></span>
    </h5>
  </div>
  <input type="hidden" id="subjectname" value="<?= $data['subjectname']?>">
  <div class="col-xs-12" style="padding-top: 10px;"><button class="btn btn-info pull-right" id="newliveques" data-classid="<?= $data['classid']; ?>" data-subjectid="<?= $data['subjectid'] ?>" data-chapterid="<?= $data['chapterid'] ?>">Post a new question</button></div>

  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pad-fix mar-top-10">
    <div class="col-lg-9">
      <div class="col-xs-12 col-md-6 joinlivesession" data-livesessionid="<?= $data['livesessionid']?>">
      <div class="wrap-lcb">
      	<div class="thumbimg">
      		
      	</div>
      	<div class="thumblabel">
      		<div class="pull-left">
      			<p>Join Live Class</p>
      		</div>
      		
      	</div>
      	
      </div>

      
      </div>
      
      <div class="col-xs-12 col-md-6" data->
       <div class="wrap-lcb">
      	<div class="thumbimg">
      		
      	</div>
      	<div class="thumblabel">
      		 <a href="<?= base_url()?>homework_help" class="text-center">Post Your Questions</a>
      	</div>
      	
      </div>
      
     
      
      </div>
      
    </div>
    <?php
            //$this->load->view('templates/sidebar');
    $this->load->view('templates/sidebar');
    ?>
  </div>

  
<script>
  $(window).on('beforeunload', function() {
    $(window).scrollTop(0); 
});
</script>