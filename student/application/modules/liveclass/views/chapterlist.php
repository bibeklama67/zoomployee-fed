<?php if(empty($data))
echo 'No Chapters Available';
else{
  ?>  
  <div class="row">
    <div class="col-xs-12 livequeswrapper">
      <ul class="chaps_list">
       <?php
       $i=1;       
       foreach ($data['response'] as $row) {
         ?> 
         <li>
           <a formdest=".mainform" data-classid="<?= @$classid?>"  data-subjectid="<?php echo $row['subjectid'];?>" data-chapterid="<?php echo $row['chapterid'];?>" data-classname="<?php echo $classname; ?>" data-chaptername="<?php echo $row['chaptername'];?>" href="#" class="livequestionchap">
            <div class="col-lg-12 pad-fix">
              <div class="col-lg-7">
               <div class="  col-md-9 col-sm-8 col-xs-12">
                <h5><?php echo $i; ?>. <?php echo $row['chaptername'];?></h5>
              </div>
              <div class=" col-md-3 col-sm-4 col-xs-12">
               <h6 style="color:black; font-size:12px;">--<?php echo $row['topiccount'].' Topics' ?>--</h6> 
             </div>

           </div>
           <div class="col-lg-5">
            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 pad-fix">
              <div class="progress">
                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo round($row['progress'],2);?>%;">
                  <span class="sr-only"><?php echo round($row['progress'],2);?>%Complete</span>
                </div>
              </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
              <span class="pro_per"><?php echo round($row['progress'],2);?>%</span>
            </div>
          </div>
        </div>
      </a>
      <div class="clearfix"></div>
    </li>
    <?php $i++; }
    
    ?>
  </ul>
  <div class="newqueswrap" style="display: none;">
    <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pad-fix bread_wrap">
      <h5 class="bread"></h5>
    </div> -->
    <input type="hidden" id="livequeschapter">
    <textarea id="questionecktext" class="questionecktext form-control"></textarea>
  </div>
  <?php }?>
</div></div>
<script type="text/javascript">
  // CKEDITOR.replace('questionecktext');    
</script>