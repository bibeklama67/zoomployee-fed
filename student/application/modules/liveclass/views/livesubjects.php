<?php 
if(empty($response))
	echo 'No Subjects Available';
else{

	?>
	<div class="row-wrapper">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pad-fix">
			<?php
			if($this->session->userdata('classtype') != "" && $this->session->userdata('classtype')=='CC'){?>

			<h5  class="bread" id="<?= $response[0]['classid']; ?>"><?= $response[0]['classname']; ?>
			</h5>
		</div> 

		<?php }

		else{
			?>
			<h5 class="bread" id="<?= $response[0]['classid']; ?>">Grade <?= $response[0]['classname']; ?>
			</h5>
			<?php }?>
		</div>
		<?php
		$date = new DateTime();
		$today = $date->format( 'w' );

		// $today = date('w', strtotime(date('Y-m-d')));
		foreach ($response as $row) {
			$subject = explode('-', $row['subjectname']);

			if($row['islive'] == 'Y'){
				if($row['liveclass']){
					foreach ($row['liveclass'] as $live) {

						if($live['weekday'] == $today && $live['starttime']<= date('H:i:s') && $live['endtime']>= date('H:i:s')){
							?>
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 dia-wrap" >
								<a formdest=".mainform" data-subjectid="<?php echo $row['subjectid'];?>" data-classname="<?php echo $row['classname']; ?>" data-subjectname="<?php echo $row['subjectname'];?>" data-level="easy" data-view="keypoints" data-videotype="" class="subjectbtn <?= $source?>" href="dashboard/listChapters/" data-classid="<?= $response[0]['classid'];?>">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bord-dia pad-fix subject-class-height">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  bg-tit-dia pad-fix" >

											<h6 class="head-tit-dia"><?php echo $subject[0];?></h6> 
										</div>
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left:0px;">

											<table>
												<tr>
													<td style="padding:5px;" >

														<span>
															<?php
															if(trim($row['image']))
																$image = $row['image'];
															else
																$image = base_url()."assets/images/subjectimg.png";
															?>
															<img src="<?= $image?>" class="img-responsive pull-right " style="width: 81px;height: 81px;     transform: scale(0.9);">

														</span>

													</td>
												<!-- <td style="color:#8c8b8b; padding-left: 15px; font-size: 14px; font-weight: bold;">
													<p> <?php echo $row['chaptercount']; ?> Chapters</p>
												</td> -->
												<td>
													<p class="livenow"> Live Now</p>
												</td>
											</tr>
										</table>
									</div>


								</div>
							</a>
						</div>	
						<?php
					}
				}
			}			
			if(!array_search($today, array_column($row['liveclass'], 'weekday'))){

				?>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 dia-wrap" >
					<a  data-subjectid="<?php echo $row['subjectid'];?>" data-classname="<?php echo $row['classname']; ?>" data-subjectname="<?php echo $row['subjectname'];?>" data-level="easy" data-view="keypoints" data-videotype="" class="subjectbtns <?= $source?>" data-classid="<?= $response[0]['classid'];?>">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bord-dia pad-fix subject-class-height">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  bg-tit-dia pad-fix" >

								<h6 class="head-tit-dia"><?php echo $subject[0];?></h6> 
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left:0px;">

								<table>
									<tr>
										<td style="padding:5px;" >

											<span>
												<?php
												if(trim($row['image']))
													$image = $row['image'];
												else
													$image = base_url()."assets/images/subjectimg.png";
												?>
												<img src="<?= $image?>" class="img-responsive pull-right " style="width: 81px;height: 81px;     transform: scale(0.9);">

											</span>

										</td>
												<!-- <td style="color:#8c8b8b; padding-left: 15px; font-size: 14px; font-weight: bold;">
													<p> <?php echo $row['chaptercount']; ?> Chapters</p>
												</td> -->
												<td>
													<p class="livetime"><?php 
														$index = count($row['liveclass']);

														$startdate = date_create("2013-03-15 ".$row['liveclass'][0]['starttime']);
														$enddate = date_create("2013-03-15 ".$row['liveclass'][0]['endtime']);
														$starttime = date_format($startdate, 'gA');
														$endtime = date_format($enddate, 'gA');

														$dayNames = array(
															0=>'Sun',
															1=>'Mon', 
															2=>'Tue', 
															3=>'Wed', 
															4=>'Thu', 
															5=>'Fri', 
															6=>'Sat', 
															);
															echo 'LIVE ON '.'<br>'. $dayNames[$row['liveclass'][0]['weekday']].' - '.$dayNames[$row['liveclass'][$index-1]['weekday']].' '.$starttime. ' to ' .$endtime;?> </p>
														</td>
													</tr>
												</table>
											</div>


										</div>
									</a>
								</div>	
								<?php
							}
						}else{

							?>   
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 dia-wrap" >
								<a formdest=".mainform" data-subjectid="<?php echo $row['subjectid'];?>" data-classname="<?php echo $row['classname']; ?>" data-subjectname="<?php echo $row['subjectname'];?>" data-level="easy" data-view="keypoints" data-videotype="" href="dashboard/listChapters/" class="subjectbtn <?= $source?>" data-classid="<?= $response[0]['classid'];?>">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bord-dia pad-fix subject-class-height">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  bg-tit-dia pad-fix" >

											<h6 class="head-tit-dia"><?php echo $subject[0];?></h6> 
										</div>
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left:0px;">

											<table>
												<tr>
													<td style="padding:5px;" >

														<span>
															<?php
															if(trim($row['image']))
																$image = $row['image'];
															else
																$image = base_url()."assets/images/subjectimg.png";
															?>
															<img src="<?= $image?>" class="img-responsive pull-right " style="width: 81px;height: 81px;     transform: scale(0.9);">
														</span>

													</td>
													<td style="color:#8c8b8b; padding-left: 15px; font-size: 14px; font-weight: bold;">
														<p> <?php echo $row['chaptercount']; ?> Chapters</p>
													</td>
												</tr>
											</table>
										</div>


									</div>
								</a>
							</div>
							<?php }
						}
					}?>    
				</div>

				<div style="display: none"><?php  echo $this->session->userdata('eclassid');?> </div>
				<?php
				foreach ($allsubjects as $class) {

					?>
					<div class="row-wrapper">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pad-fix">
							<?php if($class['CLASSTAG'] != "" && $class['CLASSTAG'] !='null'){?>
							<h5 class="bread" id="<?= $class['id']; ?>">Grade <?= $class['classname']; ?></h5>
							<?php }
							else{
								?>
								<h5 class="bread" id="<?= $class['id']; ?>"><?= $class['classname']; ?></h5>
								<?php }?>
							</h5>
						</div>
						<?php
						foreach ($class as $subjects ) {
							foreach ($subjects as $subject) { 
								?>
								<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 dia-wrap" >
									<a formdest=".mainform" data-subjectid="<?php echo $subject['subjectid'];?>" data-classid="<?php echo $class['id'];?>" data-classname="<?php echo $class['classname']; ?>" data-subjectname="<?php echo $subject['subjectname'];?>" data-level="easy" data-view="keypoints" data-videotype="" href="dashboard/listChapters/" class="subjectbtn <?= $source?>" data-classid="<?= $class['id'];?>">
<!-- 
	<a formdest=".mainform" data-subjectid="<?php echo $subject['subjectid'];?>" data-subjectname="<?php echo $subject['subjectname'];?>" data-classid="<?php echo $class['id']; ?>" data-classname="<?php echo $class['classname']; ?>" href="practiceexam/practice/" class="subjectbtn "> -->

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bord-dia pad-fix subject-class-height">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  bg-tit-dia pad-fix" >

				<h6 class="head-tit-dia"><?php echo ucwords(strtolower($subject['subjectname']));//.' - '.ucwords(strtolower($class['classname']));?></h6> 
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left:0px;">

				<table>
					<tr>
						<td style="padding:5px;" >

							<?php
							if(trim($subject['icon']))
								$image = $subject['icon'];
							else
								$image = base_url()."assets/images/subjectimg.png";
							?>
							<span>
								<img src="<?= $image?>" class="img-responsive pull-right " style="width: 81px;height: 81px;     transform: scale(0.9);">
								<!-- <h2 style="border:3px solid #dadada;border-radius:50px; padding:15px;"><!-- <?php //echo round($row['progress'],2);?>% --><?php /*?><?php echo $row['chaptercount']; ?><?php */?></h2>
							</span>

						</td>
						<td style="color:#8c8b8b; padding-left: 15px; font-size: 14px; font-weight: bold;">
							<p> <?php echo ($subject['chaptercount']?$subject['chaptercount']:'No'); ?> Chapters</p>
						</td>
					</tr>
				</table>
			</div>  
		</div>
	</a>
</div>

<?php
}
}
?>
</div>
<?php
}
?>
<!-- SUBSCRIPTION DIALOG -->

<div id="subscription-dialog"></div>

<style type="text/css">
	.clickable.btn.btn-danger.btn-cust.start-button:hover{
		/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#aa0000+0,aa0000+7,c60102+40,c90303+47,d21715+50,d92e2c+53,dc3836+57,e84644+70,f95c5d+97,da4a45+100 */
		background: #aa0000; /* Old browsers */
		background: -moz-linear-gradient(top,  #aa0000 0%, #aa0000 7%, #c60102 40%, #c90303 47%, #d21715 50%, #d92e2c 53%, #dc3836 57%, #e84644 70%, #f95c5d 97%, #da4a45 100%); /* FF3.6-15 */
		background: -webkit-linear-gradient(top,  #aa0000 0%,#aa0000 7%,#c60102 40%,#c90303 47%,#d21715 50%,#d92e2c 53%,#dc3836 57%,#e84644 70%,#f95c5d 97%,#da4a45 100%); /* Chrome10-25,Safari5.1-6 */
		background: linear-gradient(to bottom,  #aa0000 0%,#aa0000 7%,#c60102 40%,#c90303 47%,#d21715 50%,#d92e2c 53%,#dc3836 57%,#e84644 70%,#f95c5d 97%,#da4a45 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#aa0000', endColorstr='#da4a45',GradientType=0 ); /* IE6-9 */

	}

</style>