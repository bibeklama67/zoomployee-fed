<style>
  .title_video_h{background:#eff3f6; padding:5px; color:#697b8f; font-weight:bold;}
  img.video_tn{margin:0 auto; width:100%;}
  .mar-10-btm{margin-bottom:10px;}
  .tn_vid_live span{position:absolute; bottom:10px; left:25px; font-weight:bold; color:#f00; border:2px solid #f00; padding:5px 10px; box-shadow: 5px 5px 5px #000;}

</style>
<?php if(empty($response))
echo 'No Subjects Available';
else{
  ?>
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pad-fix class-heading">
        <h5 class="bread">Grade <?= $this->session->userdata('classname'); ?>
        </h5>
      </div>
    <?php
    foreach ($response as $row) {

      ?>  
       
      
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <div class="col-lg-12">
            <h5 class="title_video_h"><?php echo ucwords(strtolower($row['subjectname'])); ?></h5>
          </div>
          <div class="col-lg-12 tn_vid_live ">
            <a formdest=".mainform" data-subjectid="<?php echo $row['subjectid'];?>" data-subjectname="<?php echo $row['subjectname'];?>" data-classid="<?php echo $row['classid']; ?>" data-classname="<?php echo $row['classname']; ?>" data-link="<?php echo $row['link']; ?>" data-linksource="<?php echo $row['linksource']; ?>" data-iscurrentlylive="<?php echo $row['iscurrentlylive']; ?>" data-date="<?php echo date("Y-m-d"); ?>" data-videotype="" href="teachersonline/teachersonlinevideo/" class="teacheronlinesubjectbtn ">
           <?php
              if($row['iscurrentlylive'] == 'Y'){
                ?>
            <img src="<?php echo $row['thumbnail']; ?>" class="img-responsive video_tn" />
             
                <span>LIVE NOW</span>
                <?php
              }else
              {
              ?>
              <div class="live-text-holder home-block">This video will be live 
        <?php
          echo $row['link'];
          ?>  </div>
          <?php
        }
        ?>
            </a>
          </div>
        </div>

   


      <?php }
    }?> 
 

  <?php
  foreach ($allsubjects as $class) {

    ?>

      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pad-fix class-heading">
        <h5 class="bread">Grade <?= $class['classname']; ?>
        </h5>
      </div>
      <?php
      foreach ($class as $subjects ) {
        foreach ($subjects as $subject) {


          ?>
          
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <div class="col-lg-12">
            <h5 class="title_video_h"><?php echo ucwords(strtolower($subject['subjectname'])); ?></h5>
          </div>
          <div class="col-lg-12 tn_vid_live ">
            <a formdest=".mainform" data-subjectid="<?php echo $subject['subjectid'];?>" data-subjectname="<?php echo $subject['subjectname'];?>" data-classid="<?php echo $class['id']; ?>" data-classname="<?php echo $class['classname']; ?>" data-link="<?php echo $subject['link']; ?>" data-linksource="<?php echo $subject['linksource']; ?>" data-iscurrentlylive="<?php echo $subject['iscurrentlylive']; ?>" data-videotype="" href="teachersonline/teachersonlinevideo/" class="teacheronlinesubjectbtn ">
              <?php
              if($subject['iscurrentlylive'] == 'Y'){
                ?>
                <img src="<?php echo $subject['thumbnail']; ?>" class="img-responsive video_tn" />
                <span>LIVE NOW</span>
                <?php
              }else{
                ?>
                <div class="live-text-holder home-block">This video will be live 
        <?php
          echo $subject['link'];
          ?>  </div>
                <?php
              }
              ?>
            </a>
          </div>
        </div>

     

          <?php
        }
      }
      ?>

    <?php
  }
  ?>