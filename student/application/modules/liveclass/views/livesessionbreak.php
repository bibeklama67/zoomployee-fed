  <div class="modal fade" id="livesession-break" role="dialog" data-backdrop="static" data-keyboard="false" style="display: none;     ">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">×</button>
          <h4 class="modal-title">Live Class</h4>
        </div>

        <div class="modal-body">   
        <p>This class will be taught by</p>     
          <div class="teacher-detail row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <img src="<?= $teacher['profileimage']?>" class="img-responsive teachers-dummy-head modal-teacher-image">                  
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 left-td">
              <div class="tcher-info">
                <div class="t-name mb5">
                  <p class="pri-bold modal-teacher-fullname"><?= $teacher['firstname'].' '.$teacher['lastname']?></p>
                  <p class="sec-bold modal-teacher-teachingsubjects"><?= $teacher['teachingsubjects']?></p>
                </div>
                <div class="t-exp mb5">
                  <p class="pri-bold">Years Of Experience</p>
                  <?php
                  $yearsteaching = ($teacher['numberofmonthsteaching']?$teacher['numberofmonthsteaching'].' Months':'').($teacher['numberofyearsofteaching']?$teacher['numberofyearsofteaching'].' Years':'');?>
                  <p class="sec-bold modal-teacher-numberofyearsofteaching"><?= $yearsteaching?$yearsteaching:'N/A'?></p>
                </div>
                <div class="t-skool mb5">
                  <p class="pri-bold">Currently Teaching at</p>
                  <p class="sec-bold modal-teacher-currentschool"><?= ($teacher['currentschool']?$teacher['currentschool']:'N/A')?></p>
                </div>
                <div class="t-skool mb5">
                  <p class="pri-bold">Past Schools</p>
                  <p class="sec-bold modal-teacher-pastschool"><?= ($teacher['pastschoolid1']?$teacher['pastschoolid1']:'N/A')?></p>
                </div>

              </div>
            </div>
          </div> 
        </div>
        <div class="modal-footer">
          <div class="error-msg col-xs-10" style="display: none;"></div>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>          
        </div>
      </div>
    </div>
  </div>