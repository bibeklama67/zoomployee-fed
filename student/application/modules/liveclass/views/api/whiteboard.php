<p style="position: absolute; top:0; color:transparent;">MiDas LIVE</p>
<LINK href="//s3.amazonaws.com/media.muchosmedia.com/scribblar/styles/style.css" rel="stylesheet" type="text/css">
  <style type="text/css">
    object{
          transform:scale(0.3,0.3);
		transform-origin: 0px 0px;
    }
  </style>
  <script type="text/javascript" src="//s3.amazonaws.com/media.muchosmedia.com/scribblar/scripts/includes.js"></script>
  <div class="current_question" style="display: none"><?= $data['current_question']?></div>

  <script type="text/javascript">
      var targetID = "scribblar";
      var flashvars = {};
      /* pass necessary variables to the SWF */
      flashvars.userid = "<?= $data['userid'] ?>";
      flashvars.username = "<?= $data['username'] ?>";
      flashvars.roomid = "<?= $data['roomid'] ?>";
      flashvars.preferredLocales = "en_US";

      var params = {};
      params.allowscriptaccess = "always";
      params.allowFullScreenInteractive = "true";

      var attributes = {};
      attributes.id = "scribblar";
      attributes.name = "scribblar";
      swfobject.embedSWF("//s3.amazonaws.com/media.muchosmedia.com/scribblar/v4/main.swf", "alternate", "100%", "100%", "11.1.0", "//s3.amazonaws.com/media.muchosmedia.com/swfobject/expressInstall.swf", flashvars, params, attributes);
    </script>
    <div id="alternate"> <a href="//www.adobe.com/go/getflashplayer">This page requires the latest version of Adobe Flash. Please download it now.<br>
      <img src="//wwwimages.adobe.com/www.adobe.com/images/shared/download_buttons/get_flash_player.gif" border="0" alt="Get Adobe Flash Player" /> </a> </div>