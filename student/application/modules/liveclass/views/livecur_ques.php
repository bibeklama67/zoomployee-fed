<style type="text/css">
  .forum-actions,.answer-actions{
    color: #aaa;
  }
  .forum-actions i, .answer-actions i{
    font-size: 18px;
  }
  .forum-actions.liked, .answer-actions.liked{
    color: #3f8654;
  }
  .forum-date{
    font-weight: 700;
    color: #0c50a3;
  }
  .askers-icon{
    padding:9px 9px 9px 15px
  }
  .get_answers, .post-answer{
    background-color: #0c50a3;
    border: #0c50a3;    
  }
  .get_answers:hover,.get_answers:focus,.get_answers:active,.post-answer:hover,.post-answer:focus,.post-answer:active{
    background-color: #0c50b3;
    border: #0c50a3;        
  }
  .answer-text{
    width: 90% !important;
    border: none !important;
    padding: 0 10px;
    outline: none;
  }
  .answers-list{
   /* margin-bottom: 10px;*/
 }
 a.forum-actions:hover, a.forum-actions:focus {
  color: #23527c;
  text-decoration: none;
} 
#home-workk .rate-txt p{
  font-size: inherit !important;
}
#cke_txtEditedQuestion{
  width: 100% !important;
}
p.Qndate, p.Qtxt,p.askedby{
  margin: 5px 0px;
}

</style>
<?php
$row = $data['question'];
if(empty($row)){

  ?>
  <div>
   <i class="fa fa-window-close close_curques" aria-hidden="true" style="color:red;position: absolute; right:0;top:0;"></i>
   <p class="Qtxt">There is no current question for this session</p>
 </div>
 <?php
}else{

  $qid =  $row->questionsofstudentsid; ?>

  <div class="">
    <div class="">
     <i class="fa fa-window-close close_curques" aria-hidden="true" style="color:red;position: absolute; right:2;top:2; display:inline-block;"></i>
     <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 no-padding" style="border-bottom: 1px solid #cecfff;">

     <p class="pull-left cur-heading"><strong>Current Question</strong></p>
      <p class="Qndate pull-right" id="livequesdate" data-rel="<?= $qid;?>"><small style="color: #000;font-weight: bold;">(Q.No - <?= $row->questionsofstudentsid?>)</small></p>   
      <p class="askedby pull-right" style="
      margin-right: 5px !important;
      "><small>Asked By</small></p>
    </div>
    <div class=" col-md-12 col-lg-12 col-xs-12">

      <p class="Qtxt"><span><?= $row->questiontext?></span>

      </p>

      <?php 
      if($row->questionimage){
        ?>
        <a href="http://myschool.midas.com.np/uploads/forum/image/<?php echo $row->questionimage;?>" target="_blank">
          <img class="img-responsive" src="http://myschool.midas.com.np/uploads/forum/image/<?php echo $row->questionimage; ?>" style="width:20%" id="questionimage<?= $qid?>">
        </a>
        <?php
      }
      ?>

    </div>

  </div>

</div>
<?php }
?>
