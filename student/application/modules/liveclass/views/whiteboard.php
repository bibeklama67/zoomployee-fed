 
<!--   <div class="col-xs-12" style="padding-bottom: 10px;"><button class="btn btn-info pull-right" id="newliveques" data-classid="<?= $data['classid']; ?>" data-subjectid="<?= $data['subjectid'] ?>" data-chapterid="<?= $data['chapterid'] ?>">Post a new question</button></div>
-->

<script type="text/javascript">
  var current_question = $('.current_question').html();

  $('#fliveq').html(current_question);
  <?php if($data['question']){?>
    $('#fliveq').toggleClass('hidediv showdiv');
    <?php } ?>
    $('.livequesbtns').show();
    $('#viewliveques').attr('data-sessionid',"<?= $data['roomid'] ?>");
    $('footer').hide();
  </script>

  <?php 
  if($teacher['breaktill'])
  {

  }
  else
  {
    $this->load->vars(array('data'=>$data,'teacher'=>@$teacher['response']));
    $this->load->view('scribblar_frame');
  }
  // $this->load->view('livesessionbreak');
  ?>


  <div class="modal fade" id="viewaskedbymodal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="liveclass-ques-modal" role="dialog" data-backdrop="static" data-keyboard="false" style="display: none;     ">
    <div class="modal-dialog" style="width: 900px;">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">×</button>
          <h4 class="modal-title">Ask a Question</h4>
        </div>

        <div class="modal-body">   
          <p>Loading...</p>     
        </div>
        <div class="modal-footer">
          <div class="error-msg col-xs-10" style="display: none;"></div>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button style="margin-top: -3px; background: #4266b2; display: none;" type="button" class="btn btn-success btn-postlive-ques" data-sessionid="<?= $data['data']['sessionid']?>">Post</button>
          <button style="margin-top: -3px; background: #4266b2; display: none;" type="button" id="ckeditor_done" data-dismiss="modal" class="btn btn-success btn-send">Post</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal tch01" id="teacherModal" role="dialog" style="">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
          <div class="no-teacher-detail row" style="display: none;">

          </div>
          <div class="teacher-detail row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <img src="<?= base_url()?>assets/images/dummyhead.png" class="img-responsive teachers-dummy-head modal-teacher-image">                  
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 left-td">
              <div class="tcher-info">
                <div class="t-name mb5">
                  <p class="pri-bold modal-teacher-fullname"></p>
                  <p class="sec-bold modal-teacher-teachingsubjects"></p>
                </div>
                <div class="t-exp mb5">
                  <p class="pri-bold">Years Of Experience</p>
                  <p class="sec-bold modal-teacher-numberofyearsofteaching"></p>
                </div>
                <div class="t-skool mb5">
                  <p class="pri-bold">Currently Teaching at</p>
                  <p class="sec-bold modal-teacher-currentschool"></p>
                </div>
                <div class="t-skool mb5">
                  <p class="pri-bold">Past Schools</p>
                  <p class="sec-bold modal-teacher-pastschool"></p>
                </div>

              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer clear-both">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>