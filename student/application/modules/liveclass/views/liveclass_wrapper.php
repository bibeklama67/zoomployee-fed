<style>
  h5.bread{background:#eef1f6; font-weight:bold; padding:10px 5px; margin:2px 5px; color:#418755 !important;}
  .progressbar{padding:0;}
  .bread_wrap{border-bottom:1px solid #dfe4e8 !important;}
  h5 span.active{color:#526b81;}
  ul.chaps_list li{list-style:none; background:#f1f1f1;padding: 5px 10px;
    margin: 2px auto;}
    ul.chaps_list li:nth-child(even){
      background:#fbfbfb;
    }
    .progress{margin-top:5px;}
    .mar-top-10{margin-top:10px;}
    img.ads{
      border:1px solid #d5d5d5; margin:0 auto;
    }
    span.pro_per{color:#d57656;}
    .wrap-lcb{
      border:1px solid #ddd;
      cursor: pointer;
    }

    #cke_questionecktext{
      width: 100% !important;
    }
    .bread.custmart01{
      margin:2px;
      cursor: pointer;
      color: #f80100 !important;
      font-size: 18px;
      text-align: center;
    }
    @media (min-width: 768px) {
      .modal-xl {
        width: 90%;
        max-width:1200px;
      }
    }

    .thumbimg{
      min-height: 134px; padding: 12px;
      text-align: center; 
    }

    .thumbimg img{
      display: inline-block;
    }
  </style>
  <?php  $subject = explode('-', $subjectname); ?>

  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pad-fix bread_wrap">
    <h5 class="bread"> <a href="javascript:;" data-classid="<?php echo $data['classid']; ?>" class="classbtn breadcrumb">Grade <?php echo isset($data['classname'])?$data['classname']:''?></a><i class="fa fa-chevron-right"></i>
     <a href="javascript:;" data-classid="<?php echo $data['classid']; ?>" class="pull-right mini-home-btn classbtn breadcrumb">
		<img src="<?= base_url()?>assets/images/home-btn.png" class="homepng">
		 </a>
      <?php echo isset($data['subjectname'])?explode('-',$data['subjectname'])[0]:''?>
      <a class="btn btn-info pull-right livequesbtns" id="viewliveques" data-classid="<?= $data['classid']?>" data-subjectid="<?= $data['subjectid']?>" style="margin-right: -4px; margin-top: -10px; display: none">View Questions</a>
      <a class="btn btn-info pull-right livequesbtns" id="newliveques" data-classid="<?= $data['classid']?>" data-subjectid="<?= $data['subjectid']?>" style="margin-right: 10px; margin-top: -10px; display: none">Post a new question</a>
    </h5>
  </div>
  <input type="hidden" id="subjectname" value="<?= $data['subjectname']?>">

  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pad-fix mar-top-10">
    <div class="col-lg-12 sessionwrapper">
      <?php
      // print_r($teacher);

      if(@$teacher['breaktill'])
        $class = 'liveclassbreak';
      else
        $class = 'joinlivesession';
      ?>
      <div class="col-xs-12 col-md-4 joinlivesession" data-sessionid="<?= $data['sessionid']?>">
        <div class="wrap-lcb">
         <div class="thumbimg">
          <img src="<?= base_url()?>assets/images/LIVE.png" class="img-responsive">
        </div>
        <div class="thumblabel">      		
         <h5 class="bread custmart01">Join LIVE Class NOW</h5>
       </div>

     </div>


   </div>

   <div class="col-xs-12 col-md-4 viewarchiveclass" data-subjectid="<?= $data['subjectid']?>" data-classname="<?= $data['classname']?>" data-subjectname="<?= $data['subjectname']?>">
     <div class="wrap-lcb">
       <div class="thumbimg">
         <img src="<?= base_url()?>assets/images/ArchiveVideos.png" class="img-responsive">
       </div>
       <div class="thumblabel">
        <h5 class="bread custmart01">
          Watch Previous Video Classes
        </h5>

      </div>

    </div>



  </div>

</div>
<div id="offlinesession" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Live Class</h4>
      </div>
      <div class="modal-body text-center">
        <p>This class is currently not live.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
      </div>
    </div>

  </div>
</div>

<div class="modal fade" id="liveclass-ques-modal" role="dialog" data-backdrop="static" data-keyboard="false" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title">Ask a Question</h4>
      </div>

      <div class="modal-body">   
        <p>Loading...</p>     
      </div>
      <div class="modal-footer">
        <div class="error-msg col-xs-10" style="display: none;"></div>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button style="margin-top: -3px; background: #4266b2; display: none;" type="button" class="btn btn-success btn-postlive-ques" data-sessionid="<?= $data['data']['sessionid']?>">Post</button>
        <button style="margin-top: -3px; background: #4266b2; display: none;" type="button" id="ckeditor_done" data-dismiss="modal" class="btn btn-success btn-send">Post</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="liveqmodal" role="dialog">
  <div class="modal-dialog modal-lg modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="clearfix"></div>
      <div class="modal-body" style="padding-top:10px;">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default billclose" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<?php
if(@$teacher['breaktill'])
{
  $this->load->vars(array('teacher'=>$teacher));
  $this->load->view('livesessionbreak');
}

  ?>

<?php
            //$this->load->view('templates/sidebar');
    // $this->load->view('templates/sidebar');
?>
</div>


<script>
  $(window).on('beforeunload', function() {
    $(window).scrollTop(0); 
  });

</script>