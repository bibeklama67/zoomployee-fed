<?php 
if(!defined('BASEPATH')) exit('direct access invalid');

class App_model extends CI_Model
{

  function __construct()
  {
    parent::__construct();
    $db = $this->load->database('default', TRUE);

    $this->simsdb =  $this->load->database('sims',TRUE);
        // $this->otherdb = $this->load->database('otherdb',TRUE);
        // $this->smsdb =  $this->load->database('smsdb',TRUE);

  }

  
  public function get_roomdetails($sessionid)
  {
    $room = $this->db->select('s.sessionid,u.userid')->join('ml_live_sessions s','s.livesessionid = u.livesessionid')->get_where('ml_livesession_users u',array('sessioncode'=>$sessionid))->row();
    return $room;
  }

  public function getsubjects($eclassid){

    $sql = "select s.chaptercount,s.subjectid,s.classid,s.subjectcode,s.subjectname,c.classname, case when iscurrentlylive='Y' then thumbnail else onlinetiming end as thumbnail,iscurrentlylive,link,linksource,case when sa.activationcode is not null then 0 else s.subjecttype end as subjecttype from (SELECT count(c.chapterid) as chaptercount, s.*
      FROM el_subject s
      JOIN el_chapter c ON c.subjectid = s.subjectid
     WHERE s.classid !=  $eclassid
      GROUP BY s.subjectid) as s
      join el_class c on c.classid=s.classid 
left join(select DISTINCT on (subjectid) subjectid,case when isthumburl='Y' then thumbnail else '".base_url()."../../uploads/tutorial_videos/thumb/' || thumbnail end as thumbnail,iscurrentlylive,link,linksource from el_teachersonline 
order by subjectid,iscurrentlylive desc) as t  on s.subjectid = t.subjectid
      left join el_stk_activation sa on sa.subjectid = s.subjectid and sa.userid = 100000001 and sa.expirydate > now()";

    $query= $this->db->query($sql);
    $class = $query->result();
    $response = array();
    $classid = '';
    $cnt = -1;
    foreach ($class as $key=> $row) {
      if($classid!=$row->classid)
      {
        $classid = $row->classid;
        $cnt++;
        $response[$cnt]['subject'] = array();
      }
      $response[$cnt]['id'] = $row->classid;
      $response[$cnt]['classname'] = $row->classname;
      if(!$row->thumbnail)
        $class[$key]->thumbnail="";
      if($row->iscurrentlylive=='Y')
      {
        if($row->linksource=='youtube')
          $class[$key]->link = $this->getYoutubeId($row->link);
        else
          $class[$key]->link = $row->link;
      }
      else
        $class[$key]->link = $row->thumbnail;
      array_push($response[$cnt]['subject'],array('subjectid'=>$row->subjectid,'subjectname'=>$row->subjectname,'thumbnail' =>$row->thumbnail, 'link'=>$row->link, 'linksource'=>$row->linksource, 'iscurrentlylive'=>$row->iscurrentlylive));
    }
 
    return $response;
  }  

  function getothersubjects($eclassid){
    $this->db->SELECT('c.classid,c.classname,c.CLASSTAG,os.subjectname,os.subjectid,ch.chaptercount,os.icon');
    $this->db->from('el_class c');
    $this->db->join('el_subject os','c.classid=os.classid');
    $this->db->join('(select count(*)as chaptercount,subjectid from el_chapter where isactive = \'Y\' group by subjectid) as ch','ch.subjectid = os.subjectid','left');
    $this->db->where('c.classid!='.$eclassid );
    // $this->db->where("c.isactive='Y'");
    $this->db->order_by('c.priority, os.sortorder', 'ASC');
    $query= $this->db->get();
    $class = $query->result();
    // /echo $this->db->last_query();
    $response = array();
    $classid = '';
    $cnt = -1;
    foreach ($class as $row) {
      if($classid!=$row->classid)
      {
        $classid = $row->classid;
        $cnt++;
        $response[$cnt]['subject'] = array();
      }
      $response[$cnt]['id'] = $row->classid;
      $response[$cnt]['classname'] = $row->classname;
      $response[$cnt]['CLASSTAG'] = $row->CLASSTAG;
      array_push($response[$cnt]['subject'],array('subjectid'=>$row->subjectid,'subjectname'=>$row->subjectname,'chaptercount'=>$row->chaptercount,'icon'=>($row->icon?'http://myschool.midas.com.np/uploads/subjects/'.$row->icon:'')));
    }
    return $response;

  }

  function getYoutubeId($url)
  {
    $parts = parse_url($url);
    if (isset($parts['host'])) {
      $host = $parts['host'];
      if (
        false === strpos($host, 'youtube') &&
        false === strpos($host, 'youtu.be')
        ) {
        return false;
    }
  }
  if (isset($parts['query'])) {
    parse_str($parts['query'], $qs);
    if (isset($qs['v'])) {
      return $qs['v'];
    }
    else if (isset($qs['vi'])) {
      return $qs['vi'];
    }
  }
  if (isset($parts['path'])) {
    $path = explode('/', trim($parts['path'], '/'));
    return $path[count($path) - 1];
  }
  return false;
}    

  public function getproductbyremarks($slug=false)
  {
    try
    {
     if(!$slug) throw new exception('Error in data parameter'); 
     $remarks=explode('-',$slug);

     $data=implode(' ',$remarks);
     $this->db->limit(1);
     $this->db->select('*');
     $this->db->from('midas_products');
     $this->db->where('lower(remarks)',trim($data));
     $this->db->where('isfullpackage','Y');
     $query= $this->db->get();

     return ( $query->result('array'));
   }
   catch (Exception $e)
   {
    echo $e->getMessage();
  }


}
public function getproductbyclass($class)
{

  try
  {
   if(empty($class)) throw new exception('Error in data parameter'); 


   $this->db->select('*');
   $this->db->from('midas_products');
   $this->db->where_in('class',$class);
   $this->db->where('isfullpackage','Y');
   $this->db->order_by('class','asc');
   $query= $this->db->get();

   return ( $query->result('array'));
 }
 catch (Exception $e)
 {
  echo $e->getMessage();
}


}



public function checkdata($mobileno)
{
 try
 {

   if(!($mobileno)) throw new exception('Error in data parameter'); 
   $this->otherdb->limit('1');
   $this->otherdb->select('*');
   $this->otherdb->from('productdownloadusers');
   $this->otherdb->where('mobilenumber',$mobileno);

   $querydata= $this->otherdb->get();
                // print_r($querydata->result('array'));exit;
   return $querydata->result('array');

 }
 catch (Exception $e)
 {
  echo $e->getMessage();
}
}
public function insertdetail($table,$data)
{
  try
  {

    if((trim($table)=='')) throw new Exception("No table found ", 1);
    if(empty($data)) throw new Exception("Empty array found", 1);

    $this->otherdb->insert($table,$data); 
    if($this->otherdb->affected_rows())
    {
      return true;
    }
    else return false;

  }
  catch(Exception $e )
  {
    echo $e->getMessage();
  }

}
public function updatedetail($table,$data,$colname,$key)
{
 try
 {

   if((trim($key)=='')) throw new Exception("No Specific key found ", 1);

   $this->otherdb->where($colname, $key);
   $this->otherdb->update($table, $data); 
   if($this->otherdb->affected_rows())
   {
    return true;
  }
  else return false;

}
catch(Exception $e )
{
  echo $e->getMessage();
}
}

public function checksms($mobileno)
{
 try
 {

   if((trim($mobileno)=='')) throw new Exception("Mobile Number npt found ", 1);

   $query=$this->smsdb->query("SELECT COUNT(*) as N FROM SMS_OUT 
    WHERE MOBILE_NUMBER = '$mobileno' AND TO_DATE(REC_DATE) = TO_DATE(SYSDATE)");

                    // $querydata= $this->smsdb->result('array');

   return $query->result('array');

 }
 catch(Exception $e )
 {
  echo $e->getMessage();
}
}

public function getid()
{

 $query=$this->smsdb->query("SELECT NVL(MAX(ID),0)+1 as SMSOUTID FROM SMS_OUT");
                 // $querydata= $this->smsdb->result('array');
 return $query->result('array');

}

public function sendsms($data)
{  

        // print_r($data);exit;
 return $this->smsdb->insert('SMS_OUT',$data); 

}
public function getorderid()
{
  $this->otherdb->select('max("MIDASECLASSORDERID")');
  $this->otherdb->from("MIDASECLASSORDER");
  $data= $this->otherdb->get();
  return $data->result('array');

}
public function checkvalidation($data)
{
 $this->otherdb->select('count("MIDASECLASSORDERID")');
 $this->otherdb->from("MIDASECLASSORDER");
 $this->otherdb->where($data);
 $data= $this->otherdb->get();
 return $data->result('array');
}

public function updateuser($userid)
{
  $this->otherdb->set('createddatetime',date('Y-m-d H:i:s'));
  $this->otherdb->where('productdownloaduserid',$userid);
  $this->otherdb->update('productdownloadusers');
  if($this->otherdb->affected_rows())
  {
    return true;
  }
  else
  {
    return false;
  }
}


public function checkdownloaddetail($data)
{
  try
  {
    if(empty($data)) throw new Exception("No data for checking", 1);
    $this->otherdb->select('productdownloadusersdetailid');
    $this->otherdb->from('productdownloadusersdetail');
    $this->otherdb->where($data);
    $data= $this->otherdb->get();
    $id= $data->result('array');
    if($this->otherdb->affected_rows())
    {
      return $id;
    }
    else
    {
      return false;
    }

  }
  catch(Exception $e )
  {
    echo $e->getMessage();
  }
}
public function insertdetails($table,$data)
{
  try
  {

    if((trim($table)=='')) throw new Exception("No table found ", 1);
    if(empty($data)) throw new Exception("Empty array found", 1);

    $this->otherdb->insert($table,$data); 
    if($this->otherdb->affected_rows())
    {
      return $this->otherdb->insert_id();
    }
    else return false;

  }
  catch(Exception $e )
  {
    echo $e->getMessage();
  }

}

function livecurrent_question($sessionid){

    $query = $this->simsdb->query("select *,to_char(dataposteddatetime,'YYYY-MM-DD') || ' ' || trim(to_char(dataposteddatetime, 'HH12:MI AM'), '0') as curtime From forum.el_questionsof_students where sessionid = '$sessionid' and liveansstart is not null and liveansend is null and isactive = 'Y' and status = 'Y'");
    $question = $query->row();
    return $question;
  }
}

