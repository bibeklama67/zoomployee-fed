<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Liveclass extends Front_controller {

	function __construct()
	{
		parent::__construct();
		$d=$this->config->item('default');
		$df=$this->config->item($d); 
		$df['js'][]='common/student_api';
		$df['js'][]='teachers_online/teachers_online';
		// $df['js'][]='liveclass/liveclass';
		$df['js'][]='dashboard/dashboard';		
		$df['js'][]='common/pusher.min';

		$this->config->set_item($d,$df);
		$this->load->model('teachersonline_model','model');
		$this->load->model('frontend_model','frontmodel');
		$this->simsdb =  $this->load->database('sims',TRUE);


	}

	public function index()
	{
		$data = $this->input->post('params');
		$teacher = $this->input->post('teacher');
		$teacher = $teacher['response'];

		// print_r($data);exit;
		// $this->session->set_userdata('quizActivity', 'homeworklive');
  //echo '<pre>'; var_dump($data); echo '</pre>';exit;
		$this->load->vars(array(
			'data'         =>    $data,
			'teacher'		=> 		$teacher			
			));
		$this->load->view('liveclass_wrapper'); 
	}	

	function whiteboard(){
		$data = $this->input->post();

		$teacher = $this->input->post('teacher');
		$teacher = $teacher['response'];

		if($this->session->userdata('usertype')=='TEACHER')		
			$user = $this->simsdb->select('name as username')->get_where('org_midasuser',array('userid'=>$data['childid']))->row();		
		else
			$user = $this->simsdb->select('username')->get_where('org_midasstudent',array('userid'=>$data['childid']))->row();

		$data['userid'] = 0;		
		$data['username'] = $user->username;
		$data['liveheader'] = 'true';
		$question = $this->model->livecurrent_question($data['roomid']);
		$data['question'] = $question;
		$this->load->vars(array(
			'data'         =>    $data
			));
		$data['current_question'] = $this->load->view('livecur_ques','',true);
		
		$this->load->vars(array(
			'data'         =>    $data,
			'teacher'		=> 	$teacher						
			));
		// $this->load->view('header/liveheader');
		$this->load->view('whiteboard',$data); 
	}

	function listchapters()
	{
		$data = $this->input->post();
		$this->load->vars(array(
			'data'         =>    $data['data'],
			'classid'		=> $data['classid']
			));
		$this->load->view('chapterlist'); 
	}

	function listSubjects()
	{
		$data = $this->input->post();
		$response = $data['response'];
		if($this->session->userdata('userid') == 11000000011){
			$allsubjects = $this->model->getothersubjects($this->session->userdata('eclassid'));
		}else{
			$allsubjects = '';
		}
		$title=$this->input->post('title');
		$vars = array(
			'response'          =>    $response,
			'allsubjects'   =>    $allsubjects,
			'classname'         =>    $data['classname'],
			'source' => $data['source'],    
			'title'             =>    $title);
		if($this->input->post('breadcrumb')=="true")
		{
			$this->load->vars(array('vars'=>$vars,'view'=>'livesubjects','hassubview'=>true));
			$this->load->view('dashboard/dashboard', $response);
		}
		else
		{
			$this->load->vars($vars);
			$this->load->view('livesubjects', $response);
		}
	}


	function listlivequestions()
	{
		$data=$this->input->post();
		// print_r($data);exit();
		$this->load->vars(array(
			'data'         =>    $data['data'],
			'classid'		=> $data['classid']
			));
		$this->load->view('livequestionlist'); 
	}

	function showvideo(){

		//$this->load->view('showvideo');
		$userid=$this->session->userdata('userid');
		$title=$this->input->post('title');
		$student_name=$this->session->userdata('student_name');

		$this->load->vars(array(
			'secondaryviewlink' =>    'showvideo',
			'formview'          =>    'student_progress/progress/studenthome',
			'searchview'        =>    'templates/header/headerlive',
			'student_name'      =>    $student_name
			));

		$this->render_page(); 
	}

	function archiveclass(){
		$data = $this->input->post();
		// print_r($data);
		// exit;
		// $user = $this->simsdb->select('username')->get_where('org_midasstudent',array('userid'=>$data['childid']))->row();
		// $data['userid'] = 0;		
		// $data['username'] = $user->username;
		// $data['liveheader'] = 'true';
		$this->load->vars(array(

			'data'         =>    $data
			));
		// $this->load->view('header/liveheader');
		$this->load->view('archiveclass',$data); 
	}


	function current_question($sessionid){
		$question = $this->model->livecurrent_question($sessionid);
		$data['question'] = $question;

		$this->load->vars(array(
			'data' => $data
			));
		$this->load->view('livecur_ques');
	}
}
