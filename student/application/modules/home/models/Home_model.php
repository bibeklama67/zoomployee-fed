<?php

class Home_model extends CI_Model
{
    function __construct() {
        parent::__construct();
        
        $this->ecademy = $this->load->database('sims', TRUE);
    }
    function savedata($table,$postdata)
    {
        if($this->ecademy->insert($table,$postdata))
        return $this->ecademy->insert_id() ;
        else
        return 0;
    }
    function updatedata($table,$postdata,$where)
    {
        $this->ecademy->where($where);
        if($this->ecademy->update($table,$postdata))
        return 1;
        else
        return 0;
    }

    function getfaq()
    {
        $sql="select fq.faqid,question,answer,question_nep,answer_nep,videolink from faqquestion fq join faqanswer fa on fq.faqid=fa.faqid where fq.status='Y' and fa.status='Y' order by orderby";
        $res=$this->ecademy->query($sql)->result();
        return $res;
    }
    function checkduplicatedata()
    {
        $sql="select * from applyform where guardianmobileno=? and classid=?";
        $res=$this->ecademy->query($sql,array($_POST['phone'],$_POST['eclass']));
        if($res->num_rows() > 0)
        return true;
        else
        return false;
    }
    function getapplicantsdata()
    {
        
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $from = $this->input->post('frmDate');
            $to = $this->input->post('toDate');
        }
        $sql="select af.* ,d.districtname,vdc.vdcname,case when af.from='0' then 'Form 0' when af.from='1'
         then 'Form 1' when af.from='2'
         then 'Form 2'  when af.from='3'
         then 'Form 3'
         when af.from='4'
         then 'Form 4' else 'Default' end as appfrom from applyform af
       left  join district d on af.guardiandistrictid=d.districtid
        left join devcommittee vdc on vdc.vdcid=af.guardianvdcid           
         where status='Y' and iscomplete='Y' ";
         $currentdate = date("Y-m-d");
          if ($this->input->post()) {
            $sql .= "and  TO_CHAR( af.datapostdatetime::timestamp, 'YYYY-MM-DD') between '$from' and '$to' ";

        }else{
            $sql .= "and  TO_CHAR( af.datapostdatetime::timestamp, 'YYYY-MM-DD') = '$currentdate' ";

        }

       //  $sql .= "and  TO_CHAR( af.datapostdatetime::timestamp, 'YYYY-MM-DD') between '2020-12-29' and '$currentdate' ";

         
         $sql .= "order by af.datapostdatetime desc";
        $res=$this->ecademy->query($sql)->result();
        foreach($res as $key=>$val)
        {
            $res[$key]->callstatus=$this->getcallstatus($val->applicationid);
            $res[$key]->class=$this->getclass($val->classid);
            if($val->subjectlist!='')
            $res[$key]->subject=$this->getsubject(array('classid'=>$val->classid,'sub'=>$val->subjectlist));
            else
            $res[$key]->subject='N/A';
        }
       // var_dump($res);exit;
        return $res;
    }
    function getclass($classid)
    {
        $sql="select distinct classname from online_exam.class_subject_chapter where classid=?";
        $res=$this->ecademy->query($sql,array($classid))->row();
        return $res->classname;

    }
    function getsubject($data)
    {
        $sql="SELECT string_agg(distinct subjectname, ',<br/>') as subjects from online_exam.class_subject_chapter where classid=? and subjectid in(".$data['sub'].")";
        $res=$this->ecademy->query($sql,array($data['classid']))->row();
        //var_dump($this->ecademy->last_query());exit;
        return $res->subjects;
    }
    function getcallstatus($id)
    {
        $sql="select * from applyformcallstatus where applyformid=?";
        $res=$this->ecademy->query($sql,array($id))->result();
        return $res;
    }
    function getqueuestatus($id)
    {
        $sql="select * from applyformqueue where appformid=?";
        $res=$this->ecademy->query($sql,array($id))->result();
        return $res;
    }
    function submitformqueue($id)
    {
        $data=array(
            'appformid'=>$id,
            'datapostdate'=>date('Y-m-d'),
            'dataposttime'=>date('H:i:s')
        );
       if($this->ecademy->insert('applyformqueue',$data))
       return true;
       else
       return false;
    }
    function releaseformqueue()
    {
        $this->ecademy->where('concat(datapostdate,dataposttime) < ',date('Y-m-d H:i:s', strtotime('-5 minutes')));

        if($this->ecademy->delete('applyformqueue'))
        return true;
        else 
        return false;
    }
    

    function gettelemarketerlist()
    {
        $sql="SELECT staffid,concat(firstname,' ',lastname)as staffname FROM pis_staff where isactive='Y' and  isecademy='Y' order by firstname asc";
        $res=$this->ecademy->query($sql)->result();
        return $res;
    }
}
