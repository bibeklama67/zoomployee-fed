
<body style="background-color:#b2d7f9" class="formbody">
    
    <section class="pt40 pb64">
        <div class="container">
            <div class="col-sm-8 offset-sm-2">
                <form class="booking-form" id="bookform" method="post">
                   <a href="<?=base_url();?>" class="goback"><i class="fa fa-arrow-circle-left" style="color:black"></i></a>
                    <p class="fs-32 text-center">
                       Application Form
                    </p>
                    <br>
                    <p class="fs-20 detailhead">
                       Student Details
                    </p>
                    <div class="row">
                        <div class="col mb16">
                            <input type="text" class="form-control" name="fname" id="fname" placeholder="First name" required>
                        </div>
                        <div class="col mb16">
                            <input type="text" class="form-control" name="lname" id="lname" placeholder="Last Name" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col mb16">
                            <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone Number" required>
                        </div>
                        <div class="col mb16">
                            <input type="email" class="form-control" name="email" id="email" placeholder="Email Address" required>
                        </div>
                    </div>
                    <div class="row forstudent">
                        <div class="col mb16">
                        <input type="text" class="form-control" name="school"  placeholder="School Name" required>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col mb16">
                         <select class="form-control" id="district" name="district" required>
                         <option value="-1">Select District</option>
                         <?php foreach($district as $list): ?>
                            <option value="<?=$list->districtid;?>"><?=$list->districtname;?></option>

                         <?php endforeach; ?>
                         </select>
                        </div>
                        <div class="col mb16">
                        <select class="form-control" id="vdc" name="vdc" required>
                         <option value="-1">Select Municipality</option>
                         </select>                       
                          </div>
                    </div>
                    <div class="row">
                        <div class="col mb16">
                        <input type="text" class="form-control" name="address" placeholder="Address" required>
                        </div>
                    </div>
                    <div class="row forstudent">
                        <div class="col mb16">
                        <select class="form-control" id="eclass" name="eclass" required>
                         <option value="-1">Select Class</option>
                         <?php foreach($eclass as $elist): ?>
                            <option value="<?=$elist->eid;?>"><?=$elist->classname;?></option>

                         <?php endforeach; ?>
                         </select>                        </div>
                    </div>
                    <div class="row forstudent">
                        <div class="col mb16 subject">
                        </div>
                        <div class="col mb16">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col mb16">
                         <span id="err" style="color:red"></span>
                         <p id="success" style="color:green"></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col mb16 text-center">
                            <button type="submit"  class="btn-primary" id="btnsubmit" data-type="next">Next</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
  <script>
  $('#district').change(function(e){
      let districtid=$(this).val();
      if(districtid=='-1')
      {
          $('#err').html('Please Select District');
          return false;
      }
                            $.ajax({
                                type: 'post',
                               
                                url: "<?php echo base_url('home/getvdc') ?>",
                                data:{districtid},
                               
                                  success: function (response) {
                                    var res = jQuery.parseJSON(response);

                                    if(res.type=='success')
                                    { 
                                        let html='<option value="-1">Select Municipality</option>';
                                        $(res.response).each(function( index,value ) {
                                            html +='<option value="'+value.vdcid+'">'+value.vdcname+'</option>';
                                            });
                                            $('#vdc').empty();
                                            $('#vdc').html(html);

                                    }


                                }
                            });
  })

  $('#eclass').change(function(e){
      let eclassid=$(this).val();
      if(eclassid=='-1')
      {
        $('.subject').empty();
          $('#err').html('Please Select Class');
          return false;
      }
                            $.ajax({
                                type: 'post',
                               
                                url: "<?php echo base_url('home/getsubject') ?>",
                                data:{eclassid},
                               
                                  success: function (response) {
                                    var res = jQuery.parseJSON(response);

                                    if(res.type=='success')
                                    { 
                                        let html='';
                                        $(res.response).each(function( index,value ) {
                                            html +='<input type="checkbox" name="subject[]" value="'+value.subjectid+'" />' +' ' + value.subjectname +'<br/>';
                                            });
                                            $('.subject').empty();
                                            $('.subject').html(html);

                                    }


                                }
                            });
  })
  $('.goback').click(function(e)
  {
      if($('#fname').val()!='')
      {
        if (confirm('Are you sure you want to leave?')) {
            location.href='<?=base_url();?>';
        } 
        else
        {
            return false;
        }
      }
  })

  $('#bookform').on('submit', function(e){
    e.preventDefault();
    $('#btnsubmit').attr('disabled','disabled');
                         if($('#btnsubmit').data('type')=='next')
                         {
                            $.ajax({
                                type: 'post',
                               
                                url: "<?php echo base_url('home/submitapplication') ?>",
                                data:$('#bookform').serializeArray(),
                               
                                  success: function (response) {
                                    var res = jQuery.parseJSON(response);
                                    $('#btnsubmit').removeAttr('disabled');
                                    $('#err').html('');
                                    $('#success').text('');
                                    if(res.type=='success')
                                    { 

                                       $('.detailhead').text('Parent Detail');
                                       $('.forstudent').hide();
                                       $('#fname').val('');
                                       $('#lname').val('');
                                       $('#phone').val('');
                                       $('#email').val('');
                                       
                                       $('#btnsubmit').data('type','submit');
                                       $('#btnsubmit').attr('data-id',res.id);
                                       $('#btnsubmit').text('Submit');
                                    }
                                    else
                                    {
                                        $('#err').html(res.message);
                                        return false;
                                    }


                                }
                            });


                         }
                         else
                         {
                            $.ajax({
                                type: 'post',
                               
                                url: "<?php echo base_url('home/submitapplication_guardian') ?>"+"/"+$('#btnsubmit').data('id'),
                                data:$('#bookform').serializeArray(),
                               
                                  success: function (response) {
                                    var res = jQuery.parseJSON(response);
                                    $('#err').html('');
                                    $('#success').text('');

                                    if(res.type=='success')
                                    { 

                                        $('#success').text(res.message);
                                        $('#bookform')[0].reset();


                                    }
                                    else
                                    {
                                        $('#err').html(res.message);
                                        return false;
                                    }


                                }
                            });

                         }
                             
  });




  </script>
    
  