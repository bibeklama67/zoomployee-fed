
<body style="background-color:#b2d7f9">
    
    <section class="pt40 pb64">
        <div class="container">
            <div class="col-sm-8 offset-sm-2">
                <form class="booking-form" id="bookform" method="post">
                <a href="<?=base_url();?>" class="goback"><i class="fa fa-arrow-circle-left" style="color:black"></i></a>

                    <p class="fs-32 text-center">
                        Book An Appointment
                    </p>
                    <br>
                    <div class="row">
                        <div class="col mb16">
                            <input type="text" class="form-control" id="fullname" name="fullname" placeholder="Full name" required>
                        </div>
                        <div class="col mb16">
                            <input type="email" class="form-control" name="email" placeholder="Email Address" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col mb16">
                            <input type="text" class="form-control" name="phone" placeholder="Phone Number" required>
                        </div>
                        <div class="col mb16">
                            <input type="text" class="form-control" name="classname" placeholder="Class" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col mb16">
                        <input type="text" class="form-control" name="school" placeholder="School Name" required>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col mb16">
                         <select class="form-control" id="district" name="district" required>
                         <option value="-1">Select District</option>
                         <?php foreach($district as $list): ?>
                            <option value="<?=$list->districtid;?>"><?=$list->districtname;?></option>

                         <?php endforeach; ?>
                         </select>
                        </div>
                        <div class="col mb16">
                        <select class="form-control" id="vdc" name="vdc" required>
                         <option value="-1">Select Municipality</option>
                         </select>                       
                          </div>
                    </div>
                    <div class="row">
                        <div class="col mb16">
                        <input type="text" class="form-control" name="address" placeholder="Address" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col mb16">
                         <span id="err" style="color:red"></span>
                         <p id="success" style="color:green"></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col mb16 text-center">
                            <button type="submit" class="btn-primary" id="c">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
  <script>
  $('#district').change(function(e){
      let districtid=$(this).val();
      if(districtid=='-1')
      {
          $('#err').html('Please Select District');
          return false;
      }
                            $.ajax({
                                type: 'post',
                               
                                url: "<?php echo base_url('home/getvdc') ?>",
                                data:{districtid},
                               
                                  success: function (response) {
                                    var res = jQuery.parseJSON(response);

                                    if(res.type=='success')
                                    { 
                                        let html='<option value="-1">Select Municipality</option>';
                                        $(res.response).each(function( index,value ) {
                                            html +='<option value="'+value.vdcid+'">'+value.vdcname+'</option>';
                                            });
                                            $('#vdc').empty();
                                            $('#vdc').html(html);

                                    }


                                }
                            });
  })
  $('.goback').click(function(e)
  {
      if($('#fullname').val()!='')
      {
        if (confirm('Are you sure you want to leave?')) {
            location.href='<?=base_url();?>';
        } 
        else
        {
            return false;
        }
      }
  })
  $('#bookform').on('submit', function(e){
    e.preventDefault();
        $.ajax({
                                type: 'post',
                               
                                url: "<?php echo base_url('home/submitappointment') ?>",
                                data:$('#bookform').serializeArray(),
                               
                                  success: function (response) {
                                    var res = jQuery.parseJSON(response);

                                    if(res.type=='success')
                                    { 
                                        location.href='<?=base_url();?>thanks';

                                    }
                                    else
                                    {
                                        $('#err').html(res.message);
                                        return false;
                                    }


                                }
                            });
  });


  </script>
    
  