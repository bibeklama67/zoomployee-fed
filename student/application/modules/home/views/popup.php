<style>
    
  
   .popupmodal {
    border-radius: 7px;
    overflow: hidden;
    background-color: transparent; }
     .popupmodal a {
      color: #fff; }
     .popupmodal .logo a img {
      width: 30px; }
     .popupmodal  .popupmodal-content {
      background-color: transparent;
      border: none;
      border-radius: 7px; }
       .popupmodal  .popupmodal-content  .popupmodal-body {
        border-radius: 7px;
        overflow: hidden;
        color: #fff;
        padding-left: 0px;
        padding-right: 0px;
        -webkit-box-shadow: 0 10px 50px -10px rgba(0, 0, 0, 0.9);
        box-shadow: 0 10px 50px -10px rgba(0, 0, 0, 0.9); }
         .popupmodal  .popupmodal-content  .popupmodal-body.overlay {
          position: relative; }
           .popupmodal  .popupmodal-content  .popupmodal-body.overlay:before {
            content: "";
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            width: 100%;
            background-color: rgba(0, 0, 0, 0.5); }
           .popupmodal  .popupmodal-content  .popupmodal-body.overlay .to-front {
            z-index: 2;
            position: relative; }
         .popupmodal  .popupmodal-content  .popupmodal-body h2 {
          font-size: 18px; }
         .popupmodal  .popupmodal-content  .popupmodal-body p {
          color: #777; }
         .popupmodal  .popupmodal-content  .popupmodal-body h3 {
          color: #000;
          font-size: 22px; }
         .popupmodal  .popupmodal-content  .popupmodal-body .close-btn {
          color: #000; }
         .popupmodal  .popupmodal-content  .popupmodal-body .line {
          border-bottom: 1px solid rgba(255, 255, 255, 0.3);
          padding-bottom: 10px; }
     .popupmodal .cancel a {
      color: rgba(255, 255, 255, 0.5);
      font-size: 13px;
      font-weight: bold; }
       .popupmodal .cancel a:hover {
        color: #fff; }
  
        .popupmodal .form-control {
    border: transparent;
    -webkit-box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.1);
    box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.1); }
    .popupmodal .form-control:active, .form-control:focus, .form-control:hover {
      -webkit-box-shadow: none !important;
      box-shadow: none !important; }
  
      .popupmodal .btn {
          
    border-radius: 4px;
    border: none; }
    .popupmodal .btn:active, .btn:focus {
      outline: none;
      -webkit-box-shadow: none !important;
      box-shadow: none !important; }
      .popupmodal .bg-3 {
        background: #f9eaf2;
    }
    

    .popupmodal .btn:active, .btn:focus {
      outline: none;
      -webkit-box-shadow: none !important;
      box-shadow: none !important; }
  
 .btnnext
 {
    color: #fff;
    background-color: #992a69;
    border-color: #8f2762;
    /* height: 61px!important; */
 }
 @media screen and (max-width: 600px) {
     .popupmodal-content{
         margin-left: 5px!important;
     }
     .popupmodal-body{
         width: 380px!important;
     }
     .popupmodal img{
        width: 320px!important;
    height: 250px!important;
     }
     .popupmodal title{
         color:black!important;
     }
 }
    </style>
<div class="popupmodal modal fade" id="notimodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
<div class="popupmodal-dialog modal-dialog modal-dialog-centered" role="document">
<div class="popupmodal-content modal-content rounded-0" style="    margin-left: -150px;
    margin-top: -20px;">
<div class="popupmodal-body modal-body bg-3" style="    width: 800px;">
<div class="px-3 to-front">
<div class="row align-items-center">
<div class="col text-right">
<a href="#" class="close-btn" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">x</span>
</a>
</div>
</div>
</div>
<div class="p-4 to-front" style="margin-top: -20px!important;">
<div class="text-center">

<p class="mb-4">अब सजिलै घर बसी बसी भेट्नुहोस् डक्टर मेरो डक्टरमा । तुरुन्तै भिडियो परापर्श गर्न, विशेषज्ञ डक्टरसँग परामर्श लिन, हस्पिटलमा ओपिडि सेवा बुक गर्न आजै, मेरो Doctor App डाउनलोड गर्नुहोस् l<br/>
थप जानकारीको लागि - www.merodoctor.com<br/>
सम्पर्क नम्बर - ✆ : 9847392164, 9847392191, 9823315189 </p>
<a href="https://www.merodoctor.com/" target="_blank"><img src="https://www.merodoctor.com/assets/img/promotion/merodocadvertisement.jpeg" style="   width: 700px;
  " />
</a>
<div class="row" style="margin-top:5px">
   
        <div class="col-6">
<a href="https://play.google.com/store/apps/details?id=np.com.midas.merodoctor&hl=ne&gl=US" target="_blank" class="btn btn-secondary btn-block " >Download App</a>
</div>
<div class="col-6">
<a href="https://www.merodoctor.com/" target="_blank" class="btn btn-primary btnnext btn-block" >Visit Website</a>
</div>
       

</div>
<p class="mb-0 cancel"><small>Need help? <a href="https://www.merodoctor.com/help" target="_blank" style="color:firebrick">Contact Us</a> </small></p>
</div>
</div>
</div>
</div>
</div>
</div>
<script>
    
</script>