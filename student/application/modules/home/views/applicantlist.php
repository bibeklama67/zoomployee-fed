<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" crossorigin="anonymous" />
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.4/css/buttons.dataTables.min.css" crossorigin="anonymous" />
<style>
    /* .leftmargin{
        margin-left: 20px !important;
    } */
</style>
<body style="overflow-x:auto;">
    <div class="container-fluid" style="margin-bottom:80px;">
        <nav class="navbar navbar-expand-lg navbar-light bg-white">
            <a class="navbar-brand" href="<?= base_url(); ?>">
                <img src="<?= base_url(); ?>assets/eacademy/images/logo.png" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                    <li class="nav-item ">
                        <a class="nav-link" href="<?= base_url(); ?>">Home</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="<?= base_url(); ?>faqtour">About eCADEMY <span class="sr-only">(current)</span></a>
                    </li>


                </ul>
            </div>
        </nav>
    </div>

    <div  style="padding:20px; background-color:#E7EAED; margin-bottom: 20px;">
        <form class="form-inline" method="POST" action="<?php echo base_url('/applicants') ?>">
            <div class="form-group">
                <label for="frmDate">From Date: </label>
                <input type="text" class="datepicker form-control " id="frmDate" name="frmDate" value="<?php echo $frmDate; ?>" style="margin-left: 20px;">
            </div>
            <div class="form-group" style="margin-left:30px" >
                <label for="toDate">To Date: </label>
                <input type="text" class="datepicker form-control " id="toDate" name="toDate" value="<?php echo $toDate; ?>"  style="margin-left: 20px;">
            </div>
          
            <button type="submit" class="btn btn-primary btn-sm" id="btnSearhall"  style="margin-left:30px;padding-top:0px !important;padding-bottom:0px !important">Search</button>
        </form>
    </div>



    <table id="example" class="display" style="width:2700px;">
        <thead>
            <tr>
                <th>Date Time</th>
                <th>Mobile Number</th>
                <th>Call Status</th>
                <th>Class</th>

                <th>Student Name</th>
                <th>Guardian Name</th>
                <th>District</th>
                <th>School Name</th>
                <th>Municipality</th>
                <th>Address</th>
                <th>Subjects</th>

                <th>Student Email</th>
                <th>From</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($list as $val) : ?>
                <tr id="callid<?= $val->applicationid; ?>">
                    <td><?= $val->datapostdatetime; ?></td>
                    <td><?= $val->guardianmobileno; ?><br />
                        <?php if ((int)$val->classid >= '50') {
                            echo $val->studentmobileno . ' (S)';
                        } ?>
                    </td>
                    <td>
                        <?php foreach ($val->callstatus as $list) : ?>
                            <a href="javascript:void(0)" class="getcalldata" style="color:black;" title="View Call Status" data-statusid="<?= $list->callstatusid; ?>" data-id="<?= $val->applicationid; ?>" data-callby="<?= $list->callerid; ?>" data-callbyname="<?= $list->callername; ?>" data-callstatus="<?= $list->callstatus; ?>" data-clientstatus="<?= $list->clientstatus; ?>" data-remarks="<?= $list->remarks; ?>"><?= '<b style="font-size:12px;">' . substr($list->datapostdatetime, 0, 19) . '<br/>' . $list->callstatus . '</b><span style="font-size:11px;">(' . $list->callername . ') ' . $list->clientstatus . '</span><br/>'; ?></a>

                        <?php endforeach; ?>
                        <a href="javascript:void(0)" id="callaction<?= $val->applicationid; ?>" class="callaction" style="float:right;color:black;" data-id="<?= $val->applicationid; ?>" data-number="<?= $val->guardianmobileno; ?>" data-name="<?= $val->studentfirstname . ' ' . $val->studentlastname; ?>" data-parent="<?= $val->guardianfirstname . ' ' . $val->guardianlastname; ?>" data-school="<?= $val->schoolname; ?>" data-address="<?= $val->districtname; ?>,<?= $val->vdcname; ?>,<?= $val->guardianaddress; ?>"><i class="fa fa-plus"></i></a>
                    </td>
                    <td><?= $val->class; ?></td>

                    <td><?= $val->studentfirstname . ' ' . $val->studentlastname; ?></td>
                    <td><?= $val->guardianfirstname . ' ' . $val->guardianlastname; ?></td>
                    <td><?= $val->districtname; ?></td>
                    <td><?= $val->schoolname; ?><br /><span style="color:blue;"><?= $val->schoolmedium; ?></span></td>
                    <td><?= $val->vdcname; ?></td>
                    <td><?= $val->guardianaddress; ?></td>
                    <td>
                    <?php
                    $fourToEvent = ["SCIENCE AND ENVIRONMENT", "MATHEMATICS", "SOCIAL STUDIES AND POPULATION", "ENGLISH GRAMMER", "नेपाली व्याकरण", "COMPUTER SCIENCE", "ENGLISH GRAMMAR", "SOCIAL STUDIES", "SOCIAL STUDIES & POPULATION"];
                    $eightSubject = ["ENGLISH", "नेपाली", "MATHEMATICS", "SCIENCE AND ENVIRONMENT", "SOCIAL STUDIES AND POPULATION", "COMPUTER SCIENCE"];
                       if($val->classid <= 7){
                           echo 'All Subjects';
                       }
                       elseif($val->classid >= 8 and $val->classid <= 11){
                           if(in_array($val->subject,$fourToEvent)){
                            echo $val->subject;
                           }
                             
                       }
                       elseif($val->classid == 40){
                        if(in_array($val->subject,$eightSubject)){
                            echo $val->subject;
                           }
                       }
                       else{
                           echo $val->subject;
                       }
                    ?>
                    
                   </td>

                    <td><?= $val->guardianemail; ?></td>
                    <td><?= $val->appfrom; ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>

    </table>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js" ></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" ></script>

    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js" integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk="   crossorigin="anonymous"></script>

    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js" crossorigin="anonymous">
    </script>
    <script src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" crossorigin="anonymous">
    </script>
    <script src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.html5.min.js" crossorigin="anonymous">
    </script>
    
    <script>
        $(document).ready(function() {
            $('#example').DataTable({
                "order": [
                    [0, "desc"]
                ],
                dom: 'Bfrtip',
                buttons: [
                    'excelHtml5'

                ],
                columnDefs: [{
                        width: 220,
                        targets: 2
                    },

                    {
                        width: 200,
                        targets: 4
                    },
                    {
                        width: 200,
                        targets: 5
                    },
                    {
                        width: 200,
                        targets: 7
                    },
                    {
                        width: 200,
                        targets: 9
                    },
                    {
                        width: 200,
                        targets: 11
                    },
                ],

            });


          
			$('#frmDate').datepicker({
				dateFormat: "yy-mm-dd",
				changeYear: true,
				changeMonth: true,
			});
            $('#toDate').datepicker({
				dateFormat: "yy-mm-dd",
				changeYear: true,
				changeMonth: true,
			});
		
		
        });

//  $("#btnSearhall").click(function (e) { 
//      e.preventDefault();
//      var frmdate = $('#frmDate').val();
//     var todate = $('#toDate').val();

    

//  });

        $(document).on('click', '.callaction', function() {
            var studentname = $(this).data('name');
            var mobno = $(this).data('number');
            var parentname = $(this).data('parent');
            var school = $(this).data('school');
            var address = $(this).data('address');
            var appid = $(this).data('id');
            $.ajax({
                type: 'post',

                url: "<?php echo base_url('home/getcallstatus') ?>",
                data: {
                    id: appid
                },
                success: function(response) {
                    var res = jQuery.parseJSON(response);
                    if (res.type == 'success') {
                        $('#callid' + appid).attr('style', 'color:red');
                        alert('Someone is calling this customer.');
                        return false;

                    } else {
                        $('#appid').val(appid);
                        $('#callnumber').text(mobno);
                        $('.callerstudent').text(studentname);
                        $('.callerparent').text(' ' + parentname);
                        $('.callerschool').text(school);
                        $('.calleraddress').text(address);
                        $('#callform')[0].reset();

                        $('#submitcallbtn').text('Save');

                        $('#callmodal').modal('show');
                    }
                }
            });


        })
        $(document).on('click', '.getcalldata', function() {
            var appid = $(this).data('id');

            var studentname = $('#callaction' + appid).data('name');
            var mobno = $('#callaction' + appid).data('number');
            var parentname = $('#callaction' + appid).data('parent');
            var school = $('#callaction' + appid).data('school');
            var address = $('#callaction' + appid).data('address');
            var callerid = $(this).data('callby');
            var callbyname = $(this).data('callbyname');

            var callstatus = $(this).data('callstatus');
            var clientstatus = $(this).data('clientstatus');
            var remarks = $(this).data('remarks');
            callstatusid
            $('#callstatusid').val($(this).data('statusid'));

            $('#callnumber').text(mobno);
            $('.callerstudent').text(studentname);
            $('.callerparent').text(' ' + parentname);
            $('.callerschool').text(school);
            $('.calleraddress').text(address);
            $('.calleraddress').text(address);
            $('#callby').val(callerid + '#' + callbyname);
            $('#callstatus').val(callstatus);
            $("input[name=clientstatus][value='" + clientstatus + "']").prop('checked', true);

            $('#remarks').val(remarks);
            $('#submitcallbtn').text('Update');
            $('#callmodal').modal('show');

        })
        $(document).on('click', '#submitcallbtn', function(e) {
            e.preventDefault();
            if ($('#callby').val() == '-1') {
                alert('Please select Call by');
                return false;
            }
            if ($('#callstatus').val() == '-1') {
                alert('Please select Call status');
                return false;
            }

            $('#submitcallbtn').attr('disabled', 'disabled');


            $.ajax({
                type: 'post',

                url: "<?php echo base_url('home/submitcallstatus') ?>",
                data: $('#callform').serializeArray(),

                success: function(response) {
                    var res = jQuery.parseJSON(response);
                    $('#submitcallbtn').removeAttr('disabled');
                    alert(res.message);

                    if (res.type == 'success') {
                        $('#callform')[0].reset();
                        $('#callmodal').modal('hide');

                    }
                    return false;


                }
            });
        })
        $(document).on('keydown', function(event) {
            if (event.key == "Escape") {
                $('#callmodal').modal('hide');
            }
        });
    </script>
    <div class="modal fade" id="callmodal" role="dialog">
        <div class="modal-dialog" style="margin-top: 15%;
    margin-left: 35%;">
            <div class="modal-content" style="    width: 500px;
    border: 5px solid #46434385;">

                <div class="modal-body  container">
                    <form id="callform">
                        <div class="row">
                            <div class="col-md-12" style="text-align:center">
                                <p style="color:#1f2021d4;font-size:17px;" id=""><span id="callnumber">980000000</span><span style="float:right;cursor:pointer;">
                                        <i class="fa fa-times" data-dismiss="modal"></i>
                                    </span></p>
                                <input type="hidden" name="appid" id="appid" value="" />
                                <input type="hidden" name="callstatusid" id="callstatusid" value="" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label style="color: #736d6de8;">Student:</label>
                                <span class="callerstudent">Name</span>
                            </div>
                            <div class="col-md-6">
                                <span class="callerparent" style="float:right;"> Name</span>

                                <label style="color: #736d6de8;float:right">Parent:</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label style="color: #736d6de8;">School:</label>
                                <span class="callerschool">Name</span>
                            </div>
                            <div class="col-md-6">
                                <span style="float:right" class="calleraddress">Address</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label style="color: #736d6de8;">Call By:</label>
                            </div>
                            <div class="col-md-9">
                                <select id="callby" name="callby" class="form-control">
                                    <option value="-1">Please select</option>
                                    <?php foreach ($staff as $slist) : ?>
                                        <option value="<?= $slist->staffid; ?>#<?= $slist->staffname; ?>"><?= $slist->staffname; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="row" style="margin-top:3px;">
                            <div class="col-md-3">
                                <label style="color: #736d6de8;">Call Status:</label>
                            </div>
                            <div class="col-md-9">
                                <select id="callstatus" name="callstatus" class="form-control">
                                    <option value="-1">Please select</option>
                                    <option value="First call">First call</option>

                                    <option value=" Follow up call"> Follow up call</option>
                                    <option value="Payment Follow up">Payment Follow up</option>




                                </select>

                            </div>
                        </div>
                        <div class="row" style="margin-top:5px;">
                            <div class="col-md-3">
                                <label style="color: #736d6de8;">Client Status:</label>
                            </div>
                            <div class="col-md-9">
                                <input type="radio" name="clientstatus" value="Positive" /> Positive
                                <input type="radio" name="clientstatus" value="Confirmed" /> Confirmed
                                <input type="radio" name="clientstatus" value="Trial" /> In Trial
                                <input type="radio" name="clientstatus" value="Paid" /> Paid<br />
                                <input type="radio" name="clientstatus" value="Cancelled" /> Cancelled
                                <input type="radio" name="clientstatus" value="Negative" /> Negative
                                <input type="radio" name="clientstatus" value="Not Received" /> Not Received
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label style="color: #736d6de8;">Remarks:</label>
                            </div>
                            <div class="col-md-9">
                                <textarea class="form-control" id="remarks" name="remarks" style="height:100px;"></textarea>
                            </div>
                        </div>
                        <div class="row" style="text-align:center;margin-top:5px;">
                            <div class="col-md-12">
                                <button type="button" id="submitcallbtn" class="btn" style="    background-color: #054DA1;color: #fff;">Save</button>
                            </div>
                        </div>
                    </form>

                </div>

            </div>
        </div>
    </div>