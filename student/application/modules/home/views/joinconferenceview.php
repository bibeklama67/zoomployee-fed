<link rel="stylesheet" href="<?=base_url();?>assets/eacademy/css/faqstyle.css?v=1.0">
		<link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;700&display=swap" rel="stylesheet">
        <style>
.pt{
    padding-top:94px;
}
.banner{
        background-color: #EF9EC5;
    padding-bottom: 20px;
}
#accordion{
    background-color: #EF9EC5;
    padding-bottom: 20px;
}
.groove{  
     border-style: groove;
    border-color: #b1fff4c7;
    padding: 7px;}
   
.formobile{display:none;}
@media only screen 
  and (min-device-width : 320px) 
  and (max-device-width : 480px) {
    .pt{
    padding-top:34px;
}
.masthead{
    margin-top: 3px;

  }
  
      .formobile{display:block;}
      .appbutton{display:none;}
  }
  
</style>
   <body>
   <div class="container-fluid">
         <nav class="navbar navbar-expand-lg navbar-light bg-white">
            <a class="navbar-brand" href="<?=base_url();?>">
            <img src="<?=base_url();?>assets/eacademy/images/logo.png" alt="" style="width:140px!important;height: 36px!important;">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
               <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
               <li class="nav-item ">
                     <a class="nav-link" href="<?=base_url();?>">Home</a>
                  </li>
                 
                  <?php if($applicationshow=='Y'):
                         if($id=='0')
                         {
                            $id='';
                         }
                     ?>
                    <li class="nav-item ">
                     <a class="nav-link" href="<?=base_url();?>applicationform<?=$id?>">Application Form</a>
                  </li>
                  <?php endif;?>
                  <li class="nav-item ">
                     <a class="nav-link contact" href="javascript:void(0)" >Contact Us</a>
                  </li>
               
                 
               </ul>
            </div>
         </nav>
      </div>
      <div class="container-fluid masthead">
			<div class="row">
				<div class="col-md-6 text-center mt-4">
					<h3>YOU CAN LEARN ABOUT US THROUGH</h3>
				</div>
				<div class="col-md-6 text-center mt-4 mb-4">
					<a href="#zoom" class="zoom btn btnfaq">ZOOM</a>
					<a href="#faqsection" class="faq btn btnfaq">FAQ's</a>
				
					<a href="#phone" class="phone btn btnfaq">PHONE</a>
					<a href="#fbsection" class="facebook btn btnfaq">FACEBOOK</a></b>
				</div>
			</div>
        </div>
        		<!-- Zoom Section -->
                <section class="main" id="zoom">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-6">
						<img class = "yellow imga text-center"src="<?=base_url();?>assets/eacademy/images/advertiser.png">
					</div>
					<div class="col-md-6 text-center mt-4">
						<h1><b>ZOOM CALL</b></h1> <br>
						<p class="text-left faqp">With the zoom, inquiring has never been this easy. Now, for any query related to MiDas eCADEMY or our classes and courses, you can simply click our Zoom link and reach out to us where our team will receive you and do the needful </p>
						<p class="text-left faqp">So remember, every week days, Sun to Fri, between 7AM to 7PM, you can reach out to us for any query, by calling us through zoom.</p>
						<br>
						<a href="https://zoom.us/my/talktous" target="_blank"><h2><span class="blue">CALL NOW</span></h2></a>
					</div>
				</div>
			</div>
		</section>
		<!-- FAQ Section -->
        <section class="banner" id="faqsection">
         <div class="container-fluid pt">
         <?php if($applicationshow=='Y'):?>

            <a class="nav-link button formobile" href="<?=base_url();?>applicationform<?=$id?>" style="width: 46%;padding: 6px 10px!important;margin-top: 7px;margin-left:25%;text-align: center;">Apply Now</a>
            <?php endif;?>
            <div class="row appbutton">

               <div class="col-lg-6 col-md-6 col-sm-6 order-sm-1" style="margin-left: 30px;">
                
               <?php foreach($faq as $key=> $list): 
               if($key==0)
               $grooveclass=" groove";
               else
               $grooveclass='';
                     
                  ?>
               <a href="javascript:void(0)" class="faqchange <?=$grooveclass;?>" data-id="<?=$list->faqid;?>" id="faq<?=$list->faqid;?>" data-vlink="<?=$list->videolink;?>" style="color:black;line-height:2"><?=($list->question_nep!='')?$list->question_nep:$list->question;?></a><br/>
               <?php endforeach;?>
                   
               </div>
               <div class="col-lg-5 col-md-5 col-sm-5 order-sm-2">
               <span class="iframe">
               <iframe id="myiframe" width="580" height="350" style="width:100%"
                src="<?=$faq[0]->videolink;?>">
                </iframe>
               </span>
               <?php if($applicationshow=='Y'):?>

               <a class="nav-link button appbutton" href="<?=base_url();?>applicationform<?=$id?>" style="margin-left: 42%;margin-top: 7px;   
               background-color: #054DA1;
            /* padding: 6px 10px!important; */
                padding: 15px 15px 15px 15px;">Apply Now</a>
               <?php endif;?>
             </div>
            </div>
            
         </div>
      </section>
      <div id="accordion">

      <?php foreach($faq as $key=> $list): ?>
                                <div class="col-sm-6 formobile">

                            <div class="card">
                              <div class="card-header" id="heading<?=$list->faqid;?>">
                                  <button class="btn btn-link fs-20" id="btnquestion<?=$key;?>" data-toggle="collapse" data-target="#collapse<?=$list->faqid;?>" aria-expanded="true" aria-controls="collapse<?=$list->faqid;?>">
                                    <p id="question<?=$key;?>" class="fs-18 biggest question"><?=($list->question_nep!='')?$list->question_nep:$list->question;?>
                                    </p>
                                  </button>
                              </div>
                              <div id="collapse<?=$list->faqid;?>" class="collapse" aria-labelledby="heading<?=$list->faqid;?>" data-parent="#accordion">
                                  <div class="card-body" style="font-weight:600;">
                                  <iframe  width="580" height="350" style="width:100%"
                                    src="<?=$list->videolink;?>">
                                    </iframe>
                                  </div>
                                </div>
                            </div>
                            </div>

                        <?php endforeach; ?>
                        </div>
	
		<!-- Talk over phone -->
		<section class="main" id="phone">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-6">
						<img class = "bluish imga text-center"src="<?=base_url();?>assets/eacademy/images/contact.png">
					</div>
					<div class="col-md-6 text-center mt-4">
						
						<h1><b>TALK OVER PHONE</b></h1> <br>
						<p class="text-left faqp">We are a call away to answer your queries related to MiDas eCADEMY or our courses and classes. <br>
							So feel free to call us in the following numbers anyday, anytime.
							<br>
							<br>
							9847345065(Pratima)
							<br>
							9847345054(Nibika)
						</p>
						<h2 class="text-center bluecolortext">We are also available on</h2>
						<i class="fab fa-viber" style="font-size:48px;color:red"></i>
					<i class="fab fa-whatsapp" style="font-size:48px;color:green"></i><b class="phnumber">9841493770</b> </p>
				</div>
			</div>
		</div>
	</section>
	<!-- Social Media -->
	<section class="mainsocial" id="fbsection" >
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-6 mt-4">
					<h1 class="text-center"><b>FOLLOW US</b></h1> <br>
					<p class="text-left faqp">We are on Facebook, Twitter and even Insta that you can make use of to reach out to us to ask anything related to MiDas eCADEMY and the classes and courses. Our team with high spirit are always there to respond your queries and concerns immediately.</p>
					<p class="text-left faqp">So follow our page, and stay connected with us there and whenever you feel like asking us anything, just feel free to message us.
					</p>
					<p class="text-center mt-5 faqp">
						<a href="#" ><span class="bluebig1"> LIKE</span></a>
						<a href="#" ><span class="bluebig2"> MESSENGER</span></a>
					</p>
				</div>
				<div class="col-md-6 faceB ">
					<img src="<?=base_url();?>assets/eacademy/images/fb.png" >
				</div>
			</div>
		</div>
    </section>
    <script>
  $(document).off('click','.faqchange');
     $(document).on('click','.faqchange',function(){
        $('.faqchange').removeClass('groove');
        $(this).addClass('groove');
      var vlink=$(this).data('vlink');
      var html='<iframe id="myiframe" width="580" height="350" src="'+vlink+'" style="width:100%"></iframe>';
      $('.iframe').html(html);
  })
  $(document).off('click','.question');
     $(document).on('click','.question',function(){
      $('.question').removeClass('groove');
        $(this).addClass('groove');
   })
   $(document).ready(function(){
      $('#question0').addClass('groove');

      $('#btnquestion0').trigger( "click" );
   })
  </script>