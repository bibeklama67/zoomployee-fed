








































































































































































































































































































































<style>
    .pt {
        padding-top: 84px;
    }

    .formobile {
        display: none;
    }

    .flexdesign-right {
        width: 80%;
        float: right;
    }

    .flexdesign-left {
        width: 80%;
        float: left;
    }

    @media only screen and (min-device-width : 320px) and (max-device-width : 480px) {
        .pt {
            padding-top: 14px;
        }

        .formobile {
            display: block;
        }

        .flexdesign-right,
        .flexdesign-left {
            width: 80%;
            margin-left: 10% !important;
            float: left;

        }
    }
</style>

<body>
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-light bg-white">
            <a class="navbar-brand" href="<?= base_url(); ?>">
                <img src="<?= base_url(); ?>assets/eacademy/images/logo.png" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                    <li class="nav-item ">
                        <a class="nav-link" href="<?= base_url(); ?>">Home</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="<?= base_url(); ?>faqtour">About eCADEMY <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link contact" href="javascript:void(0)">Contact Us</a>
                    </li>


                </ul>
            </div>
        </nav>
    </div>
    <section class="banner">
        <a class="nav-link button formobile" href="<?= base_url(); ?>faqtour" style="width: 46%;padding: 6px 10px!important;margin-top: 7px;margin-left:25%;text-align: center;">About eCADEMY</a>

        <div class="container-fluid pt">
            <h2 class="text-center"> Application Form</h2>
            <form id="bookform" method="post">
                <input type="hidden" name="from" id="from" value="<?= $from; ?>" />
                <div class="row ">


                    <div class="col-lg-6 col-md-6 order-sm-1">
                        <p class="fs-20 detailhead">
                            Student Details
                        </p>
                        <div class="row">
                            <div class="col mb16">
                                <input type="text" class="form-control" name="fname" id="fname" placeholder="First name" required>
                            </div>
                            <div class="col mb16">
                                <input type="text" class="form-control" name="lname" id="lname" placeholder="Last Name" required>
                            </div>
                        </div>

                        <div class="row forstudent">
                            <div class="col mb16">
                                <select class="form-control" id="eclass" name="eclass" required>
                                    <option value="-1">Select Class</option>
                                    <?php foreach ($eclass as $key => $elist) :

                                    ?>
                                        <option value="<?= $key; ?>"><?= $elist; ?></option>

                                    <?php
                                    endforeach; ?>
                                </select>
                            </div>
                            <div class="col mb16">
                                <input type="number" class="form-control" name="studentphone" id="studentphone" placeholder="Mobile Number" pattern="\d{3}[\-]\d{3}[\-]\d{4}">
                            </div>

                        </div>

                        <div class="row forstudent">
                            <div class="col mb16">
                                <input type="text" class="form-control" name="school" id="school" placeholder="School Name" required>

                            </div>
                            <div class="col mb16">
                                <select class="form-control" id="medium" name="medium" required>
                                    <option value="-1">Select Medium</option>
                                    <option value="ENGLISH">English Medium</option>
                                    <option value="NEPALI">Nepali Medium</option>
                                </select>
                            </div>


                        </div>
                        <div class="row forstudent">
                            <div class="row mb16 subject" style="    margin-left: 20px;">
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 order-sm-2">
                        <p class="fs-20 detailhead">
                            Parent Details
                        </p>
                        <div class="row">
                            <div class="col mb16">
                                <input type="text" class="form-control" name="pfname" id="pfname" placeholder="First name" required>
                            </div>
                            <div class="col mb16">
                                <input type="text" class="form-control" name="plname" id="plname" placeholder="Last Name" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col mb16">
                                <input type="number" class="form-control" name="phone" id="phone" placeholder="Mobile Number" pattern="\d{3}[\-]\d{3}[\-]\d{4}" required>
                            </div>
                            <div class="col mb16">
                                <input type="email" class="form-control" name="email" id="email" placeholder="Email Address" required>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col mb16">
                                <select class="form-control" id="district" name="district" required>
                                    <option value="-1">Select District</option>
                                    <?php foreach ($district as $list) : ?>
                                        <option value="<?= $list->districtid; ?>"><?= $list->districtname; ?></option>

                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="col mb16">
                                <select class="form-control" id="vdc" name="vdc" required>
                                    <option value="-1">Select Municipality</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col mb16">
                                <input type="text" class="form-control" name="address" placeholder="Address" required>
                            </div>
                        </div>
                        <div class="row">
                            <?php if ($from != '3') : ?>
                                <div class="col-md-8">
                                    <b>The classes will be only in English Medium.</b>
                                </div>
                            <?php endif; ?>
                            <div class="col-md-4">
                                <button type="submit" class="btn-primary" id="btnsubmit" data-type="next" style="float:right;">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col mb16">
                        <span id="err" style="color:red"></span>
                        <p id="success" style="color:green"></p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <img src="" class="flexdesign-left" />
                    </div>
                    <!-- <div class="col-md-6">
             <img src="<?= base_url(); ?>assets/eacademy/images/app2.png" class="flexdesign-right"  />
             </div> -->
                </div>
            </form>
        </div>
    </section>

    <div class="modal fade" id="successmodal" role="dialog" style="    margin-top: 100px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label=""><span>×</span></button> -->
                </div>

                <div class="modal-body">

                    <div class="thank-you-pop">
                        <img src="<?= base_url(); ?>assets/images/tick.png" alt="" style="width: 13%;margin-left: 40%;">
                        <p id="msg" style="color:green;text-align: center;font-size:16px;">Thank you for applying. We will contact you soon.<br />Please learn more about MiDas eCADEMY.</p>

                    </div>

                </div>
                <div class="modal-footer">
                    <a href="<?= base_url(); ?>faqtour?d=open" class="btn btn-success">OK</a>
                </div>

            </div>
        </div>
    </div>
    <script>
        var from = $('#from').val();
        $(".hidecheckbox").hide();


        function checkAll() {
            var checkBox = document.getElementById("allsub");
            var subSubjects = document.forms['bookform'].elements['subject[]'];
            if (checkBox.checked == true) {
                for (var i = 0; i < subSubjects.length; i++) {
                    document.getElementById("subjectlis" + i).checked = true;
                }
            } else {
                for (var i = 0; i < subSubjects.length; i++) {
                    document.getElementById("subjectlis" + i).checked = false;
                }
            }

        }


        $('#district').change(function(e) {
            let districtid = $(this).val();
            if (districtid == '-1') {
                $('#err').html('Please Select District');
                return false;
            }
            $.ajax({
                type: 'post',

                url: "<?php echo base_url('home/getvdc') ?>",
                data: {
                    districtid
                },

                success: function(response) {
                    var res = jQuery.parseJSON(response);

                    if (res.type == 'success') {
                        let html = '<option value="-1">Select Municipality</option>';
                        $(res.response).each(function(index, value) {
                            html += '<option value="' + value.vdcid + '">' + value.vdcname + '</option>';
                        });
                        $('#vdc').empty();
                        $('#vdc').html(html);

                    }


                }
            });
        })

        $('#eclass').change(function(e) {
            let eclassid = $(this).val();
            if (eclassid == '-1') {
                $('.subject').empty();
                $('#err').html('Please Select Class');
                return false;
            }
            $('#school').attr('placeholder', 'School Name');
            $('#studentphone').val('');
            $('#studentphone').hide();

            var imgurl = '<?= base_url(); ?>assets/eacademy/images/';
            if (eclassid == '40') {
                $('.flexdesign-left').attr('src', imgurl + '/banner/class-8-banner.png');
            } else if (eclassid == '41') {
                $('.flexdesign-left').attr('src', imgurl + '/banner/class-9-banner.png');
            } else if (eclassid == '42') {
                $('.flexdesign-left').attr('src', imgurl + '/banner/class-10-banner.png');
            } else if (eclassid == '5') {
                $('.flexdesign-left').attr('src', imgurl + '/banner/class-1-banner.png');
            } else if (eclassid == '6') {
                $('.flexdesign-left').attr('src', imgurl + '/banner/class-2-banner.png');
            } else if (eclassid == '7') {
                $('.flexdesign-left').attr('src', imgurl + '/banner/class-3-banner.png');
            } else if (eclassid == '8') {
                $('.flexdesign-left').attr('src', imgurl + '/banner/class-4-banner.png');
            } else if (eclassid == '9') {
                $('.flexdesign-left').attr('src', imgurl + '/banner/class-5-banner.png');
            } else if (eclassid == '10') {
                $('.flexdesign-left').attr('src', imgurl + '/banner/class-6-banner.png');
            } else if (eclassid == '11') {
                $('.flexdesign-left').attr('src', imgurl + '/banner/class-7-banner.png');
            } else if (eclassid == '2' || eclassid == '3' || eclassid == '4') {
                $('.flexdesign-left').attr('src', imgurl + '/banner/class-Nursery-banner.png');
            } else if (eclassid == '50' || eclassid == '52') {
                $('.flexdesign-left').attr('src', imgurl + '/banner/class-11-banner.png');
                $('#studentphone').show();
            } else if (eclassid == '51' || eclassid == '53') {
                $('.flexdesign-left').attr('src', imgurl + '/banner/class-12-banner.png');
                $('#studentphone').show();

            } else if (eclassid == '116' || eclassid == '122' || eclassid == '124' || eclassid == '125' || eclassid == '118' || eclassid == '119' || eclassid == '120' || eclassid == '121' || eclassid == '126') {
                $('.flexdesign-left').attr('src', imgurl + 'bbs3fee.png');
                $('#school').attr('placeholder', 'College Name');
                $('#studentphone').show();

            } else {
                // $('.flexdesign-left').attr('src', imgurl + 'class10fee.png');

            }


            $.ajax({
                type: 'post',

                url: "<?php echo base_url('home/getsubject') ?>",
                data: {
                    eclassid
                },

                success: function(response) {
                    var res = jQuery.parseJSON(response);

                    var subidlist = [147, 175, 79, 80, 133, 164, 107, 174, 81, 82, 134, 149, 106, 173, 83, 84, 135, 150, 178, 183, 259, 260, 514, 516, 517, 179, 177, 508, 518, 227, 228, 230, 239, 522, 180, 181, 523, 524, 526, 527, 528, 529, 231, 233, 236, 240, 519, 520, 521, 530, 531, 542, 543, 544, 547, 548, 532, 533, 534, 535, 536, 537, 538, 539, 540, 549, 550, 551, 552, 111, 112, 113, 115, 114, 116, 117, 243, 255, 248, 252, 256, 257, 251, 250, 254, 247, 249, 45, 95, 49, 48, 47, 130, 46, 52, 53, 54, 541, 51, 50, 131, 94, 353, 96, 399, 55, 56, 58, 57, 357, 102, 122, 389, 216, 397, 371, 401, 414, 479, 509, 419, 447, 488, 490, 97, 461, 59, 60, 62, 61, 367, 103, 143, 390, 215, 374, 415, 420, 446, 473, 487, 491, 98, 468, 63, 65, 66, 64, 394, 142, 139, 217, 377, 413, 416, 436, 474, 486, 492, 99, 444, 67, 68, 69, 70, 388, 108, 138, 392, 218, 417, 422, 445, 477, 485, 493, 100, 443, 71, 72, 137, 73, 109, 418, 400, 74, 475, 442, 423, 484, 219, 382, 494, 440, 176, 75, 76, 171, 121, 185, 101, 165, 476, 424, 431, 426, 483, 441, 510, 495, 502, 439, 437, 77, 78, 172, 132, 146, 425, 168, 478, 503, 432, 427, 438, 210, 496, 511

                    ];

                    var fourToSeven = ["SCIENCE AND ENVIRONMENT", "MATHEMATICS", "SOCIAL STUDIES AND POPULATION", "ENGLISH GRAMMER", "नेपाली व्याकरण", "COMPUTER SCIENCE", "ENGLISH GRAMMAR", "SOCIAL STUDIES", "SOCIAL STUDIES & POPULATION"];
                    var eightSubjects = ["ENGLISH", "नेपाली", "MATHEMATICS", "SCIENCE AND ENVIRONMENT", "SOCIAL STUDIES AND POPULATION", "COMPUTER SCIENCE"];

                    if (res.type == 'success') {
                        let html = '<div class="col-sm-6 col-md-6 col-lg-6"><h6>Select the subjects you want to study</h6></div><div class="col-sm-6 col-md-6 col-lg-6"></div>';
                        if (eclassid <= 7) {
                            html += '<div class="col-sm-6 col-md-6 col-lg-6"><input type="checkbox" checked id="allsub" name="allsub"  value="all" onclick="checkAll()"/>' + ' All Subjects</div>';
                        }
                        $(res.response).each(function(index, value) {
                            if (subidlist.indexOf(parseInt(value.subjectid)) > -1) {

                                if (eclassid <= 7) {
                                    html += '<div class="col-sm-6 col-md-6 col-lg-6 " style=" display: none;"><input type="checkbox" checked id="subjectlis' + index + '" name="subject[]"  value="' + value.subjectid + '" />' + ' ' + value.subjectname + '</div>';

                                } else if (eclassid >= 8 && eclassid <= 11) {
                                    if (fourToSeven.indexOf(value.subjectname) !== -1) {
                                        html += '<div class="col-sm-6 col-md-6 col-lg-6"><input type="checkbox" id="subjectlis" name="subject[]"  value="' + value.subjectid + '" />' + ' ' + value.subjectname + '</div>';
                                    }
                                    // html += '<div class="col-sm-6 col-md-6 col-lg-6"><input type="checkbox" id="subjectlis" name="subject[]"  value="' + value.subjectid + '" />' + ' ' + value.subjectname + '</div>';

                                } else if (eclassid == 40) {
                                    if (eightSubjects.indexOf(value.subjectname) !== -1) {
                                        html += '<div class="col-sm-6 col-md-6 col-lg-6"><input type="checkbox" id="subjectlis" name="subject[]"  value="' + value.subjectid + '" />' + ' ' + value.subjectname + '</div>';
                                    }
                                    // html += '<div class="col-sm-6 col-md-6 col-lg-6"><input type="checkbox" id="subjectlis" name="subject[]"  value="' + value.subjectid + '" />' + ' ' + value.subjectname + '</div>';

                                } else {
                                    html += '<div class="col-sm-6 col-md-6 col-lg-6"><input type="checkbox" id="subjectlis" name="subject[]"  value="' + value.subjectid + '" />' + ' ' + value.subjectname + '</div>';

                                }

                            }
                        });


                        $('.subject').empty();
                        $('.subject').html(html);

                    }


                }
            });
        })



        $('#bookform').on('submit', function(e) {
            e.preventDefault();
            var phone = $('#phone').val();
            if (phone.length != 10) {
                $('#err').html('Mobile Number must be of 10 digits.');
                return false;
            } else if ($('#eclass').val() == '-1') {
                $('#err').html('Please select Class');
                return false;
            } else if ($('#medium').val() == '-1') {
                $('#err').html('Please select Medium');
                return false;
            } else if ($('#district').val() == '-1') {
                $('#err').html('Please Select District');
                return false;
            } else if ($('#vdc').val() == '-1') {
                $('#err').html('Please Select Municipality');
                return false;
            }

            if ($('#eclass').val() == '50' || $('#eclass').val() == '51' || $('#eclass').val() == '52' || $('#eclass').val() == '53' || $('#eclass').val() == '116' || $('#eclass').val() == '122' || $('#eclass').val() == '126') {
                var studentphone = $('#studentphone').val();

                if (studentphone.length != 10) {
                    $('#err').html('Mobile Number must be of 10 digits.');
                    return false;
                }

            }

            $('#btnsubmit').attr('disabled', 'disabled');

            $.ajax({
                type: 'post',

                url: "<?php echo base_url('home/submitdirectapplication') ?>",
                data: $('#bookform').serializeArray(),

                success: function(response) {
                    var res = jQuery.parseJSON(response);
                    $('#btnsubmit').removeAttr('disabled');
                    $('#err').html('');
                    $('#success').text('');
                    if (res.type == 'success') {

                        // location.href='<?= base_url(); ?>faqtour?d=open';
                        $('#successmodal').modal('show');

                    } else {
                        $('#err').html(res.message);
                        return false;
                    }


                }
            });





        });
    </script>