<!doctype html>
<html lang="en">
    <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?=base_url();?>assets/eacademy/css/demo.css">


    <link rel="stylesheet" href="<?=base_url();?>assets/eacademy/vendor/bootstrap/css/bootstrap-reboot.css">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">
    <title>eCADEMY</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <link href="<?=base_url();?>assets/eacademy/css/bootstrap.min.css" rel="stylesheet">


    
    </head>
    <body class="">
        <nav class="navbar navbar-fixed-top top-navbar" id="top-navbar">
            <div class="container">
                <div class="row">
                    <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <a class="navbar-brand ">
                        <img src="<?=base_url();?>assets/eacademy/images/72ppi/logo.png" alt="midas.com" width="198" height="55">
                    </a>
                    <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="bg-white collapse navbar-collapse bs-navbar-collapse" id="nav-bar">
                    <ul class="nav navbar-nav navbar-right top-navbar-right">
                        <li class="no-separator dropdown big-menu">
                            <a href="/slider.html"  role="button">
                                Tour eCAFEMY
                            </a>
                        </li>
                        <li class="no-separator dropdown big-menu">
                            <a href=""  role="button">
                                Available Course
                
                            </a>
                        </li>
                        <li class="no-separator dropdown big-menu">
                            <a href=""  role="button">
                                Price
                            </a>
                        </li>
                        <li class="no-separator dropdown big-menu">
                            <a href="" role="button">
                                Offer
                            </a>
                        </li>
                        <li class="regi-btn">
                            <a href="<?=base_url();?>login" class="btn-blue-purple-gradient">
                                Log In</a>
                        </li>
                    </ul>
                </div>
                </div>
            </div>
        </nav>
        <section class="section banner-bg text-mc">
            <div class="col-sm-6 banner-image">
                <img src="<?=base_url();?>assets/eacademy/images/72ppi/banner-second.png"  alt="" width="900px" height="650px" class="img-responsive lazyload xs-inline-block">
            </div>
            <div class="section-content">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="main-content float-right">
                                <div class="small-info ">
                                    <h2>
                                        NURSERY TO MASTER’s IN ONE PLACE
                                    </h2>
                                    <p>
                                        eCADEMY makes it possible now to study any course anytime from anywhere
                                    </p>
                                    <a href=""></a><button>
                                        Enroll
                                    </button>
                                </div>
                                <div class="detail">
                                    <h3>
                                        What's Inside
                                    </h3>
                                    <ul>
                                        <li data-icon="-- ">Pre-school, School, +2, Bachelor’s, and Master’s courses.</li>
                                        <li data-icon="-- ">Classroom-like learning experience.</li>
                                        <li data-icon="-- ">Exam-preparation classes and mockup exams.</li>
                                        <li data-icon="-- ">LIVE teachers for doubt clearing.</li>
                                        <li data-icon="-- ">Audio/visual learning materials</li>
                                        <li data-icon="-- ">Periodic assessment and personalised feedbacks.</li>
                                        <li data-icon="-- ">Affordable pricing </li>
                                    </ul>
                                    <p>
                                        Need free orientation to learn more about eCADEMY before enrolling?
                                    </p>
                                    <p class="apply">
                                        Apply for an appointment to <b>directly talk to us</b>
                                    </p>
                                    <button>
                                        Book My <br>
                                        <span>Appointment</span>
                                    </button>
                                    
                                </div>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
       
         
        <section class=" section text-mc content content-right-side">
            <div class="section-content">
                <div class="container crg25">
                    <div class="row">
                        <div class="col-sm-6 section-mt">
                            <img  src="<?=base_url();?>assets/eacademy/images/72ppi/logo-2.png"  style="
                            height: 130px;
                        " alt="">
                            <h3 class="section-title f30 mb15">Personalized Learning Journeys</h3>
                            <p class="mb30-d">A digital academy to provide a classroom-like
                                digital setup that includes all the learning and
                                teaching materials, and stand-by teachers
                                needed for a student to accomplish
                                his/her academic goal.</p>
                            <a href="#" class=" btn-blue-purple-gradient">
						Apply</a>
                        </div>
                        
                        <div class="col-sm-6">
                            <img src="<?=base_url();?>assets/eacademy/images/72ppi/second-background-image.jpg"  alt="" width="600" height="600" class="img-responsive lazyload xs-inline-block">
                        </div>
                    </div>
                </div>
            </div>

        </section>
        
        <!-- <section class="section  text-mc">
            <div class="section-content">
                <div class="container crg25">
                    <div class="row totable">
                        <div class="col-sm-6 section-mt">
                            <img  src="<?=base_url();?>assets/eacademy/images/72ppi/logo-2.png"  style="
                            height: 130px;
                        " alt="">
                            <h3 class="section-title f30 mb15">Personalized Learning Journeys</h3>
                            <p class="mb30-d">A digital academy to provide a classroom-like
                                digital setup that includes all the learning and
                                teaching materials, and stand-by teachers
                                needed for a student to accomplish
                                his/her academic goal.</p>
                            <a href="#" class="btn btn-blue-purple-gradient">
						Learn More</a>
                        </div>
                        <div class="col-sm-6">
                            <img src="<?=base_url();?>assets/eacademy/images/72ppi/second-background-image.jpg"  alt="" width="489" height="556" class="img-responsive lazyload xs-inline-block">
                        </div>
                    </div>
                </div>
            </div>
        </section> -->
        
        <section class="section bg-baby-pink baby-pink-wave text-mc">
            <div class="section-content">
                <div class="container crg25">
                    <div class="row totable">
                        <div class="col-sm-6">
                            <img src="<?=base_url();?>assets/eacademy/images/72ppi/image1.png"  alt="" width="489" height="556" class="img-responsive lazyload xs-inline-block">
                        </div>
                        <div class="col-sm-6">
                            <h3 class="section-title f30 mb15">CLASSROOM-LIKE <br> LEARNING EXPERIENCE
                            </h3>
                            <p>
                                Your learning experience in eCADEMY will be as real as classroom learning, where you can study
                                what’s required per your syllabus and curriculum following the
                                standard study-load protocol set by education expert and education board.
                            </p>
                            <br>
                            <p>
                                All the learning materials and contents are kept in day wise fashion, which is further segregated
                                into notes, videos, tasks and assignments -- just like how in classroom, teacher starts from a
                                capter but in between there happens Q&A, interactions,
                                explanations, along with references and different perspectives that ends with
                                homeworks. Isn’t that super awesome?
                            </p>
                            <a href="#"  class=" btn-blue-purple-gradient">
                                Apply
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class=" section text-mc content content-right-side">
            <div class="section-content">
                <div class="container crg25">
                    <div class="row">
                        <div class="col-sm-6">
                            <h3 class="section-title f30 mb15">LIVE TEACHERS <br>
                                FOR DOUBT CLEARING</h3>
                                <p>
                                    One of the biggest challenges of online learning platform is that
                                    students are left on their own to study, but realistically speaking,
                                    most of the times, students may not understand certain thing that they wish there be someone to
                                    clear the doubt.
                                </p>
                                <br>
                                <p>
                                    Understanding the need and the challenges, eCADEMY provides
                                    stand-by teacher to reach out for doubt clearning, where our
                                    teachers will be a click away to come LIVE when any sudent
                                    is in need.
                                </p>
                            <a href="#" class=" btn-blue-purple-gradient">
						Apply</a>
                        </div>
                        
                        <div class="col-sm-6">
                            <img src="<?=base_url();?>assets/eacademy/images/72ppi/image2.png"  alt="" width="600" height="600" class="img-responsive lazyload xs-inline-block">
                        </div>
                    </div>
                </div>
            </div>

        </section>

        <section class="section bg-baby-pink baby-pink-wave text-mc">
            <div class="section-content">
                <div class="container crg25">
                    <div class="row totable">
                        <div class="col-sm-6">
                            <img src="<?=base_url();?>assets/eacademy/images/72ppi/image3.png"  alt="" width="489" height="556" class="img-responsive lazyload xs-inline-block">
                        </div>
                        <div class="col-sm-6">
                            <h3 class="section-title f30 mb15">SYLLABUS-MAPPED <br> LEARNING MATERIALS

                            </h3>
                            <p>
                                Pre-loaded contents that include interactive videos, teacher’s notes, illustrations, along with other useful referrences mapped according to the
                                syllabus of respective course and class, that students can access and revise numerous times until not thorough with.
                                
                            </p>
                            <a href="#"  class= "btn-blue-purple-gradient">
                                Apply
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section text-mc">
            <div class="section-content">
                <div class="container crg25">
                    <div class="row totable">
                        <div class="col-sm-6">
                            <h3 class="section-title f30 mb15">MOCK-UP TEST & <br> EXAM PREPARATION

                            </h3>
                            <p>
                                One of the goals of eCADEMY is to ensure that our students score well in
board exams, for which we conduct regular mock-up exams to prepare
our students for real exams. Also, we regularly conduct exam-preparation class to shape and groom our students from an exam-focused methadology.
                            </p>
                            <a href="#"  class="btn-blue-purple-gradient">
                                Apply
                            </a>
                        </div>
                        <div class="col-sm-6">
                            <img src="<?=base_url();?>assets/eacademy/images/72ppi/image4.png"  alt="" width="489" height="556" class="img-responsive lazyload xs-inline-block">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section bg-baby-pink baby-pink-wave text-mc">
            <div class="section-content">
                <div class="container crg25">
                    <div class="row totable">
                        <div class="col-sm-6">
                            <img src="<?=base_url();?>assets/eacademy/images/72ppi/image5.png"  alt="" width="489" height="556" class="img-responsive lazyload xs-inline-block">
                        </div>
                        <div class="col-sm-6">
                            <h3 class="section-title f30 mb15">STUDY AT YOUR <br>
                                OWN PACE

                            </h3>
                            <p>
                                One of the goals of eCADEMY is to ensure that our students score well in
board exams, for which we conduct regular mock-up exams to prepare
our students for real exams. Also, we regularly conduct exam-preparation class to shape and groom our students from an exam-focused methadology.
                            </p>
                            <a href="#"  class=" btn-blue-purple-gradient">
                                Apply
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section text-mc">
            <div class="section-content">
                <div class="container crg25">
                    <div class="row totable">
                        <div class="col-sm-6">
                            <h3 class="section-title f30 mb15">MOCK-UP TEST & <br> EXAM PREPARATION

                            </h3>
                            <p>
                                One of the goals of eCADEMY is to ensure that our students score well in
board exams, for which we conduct regular mock-up exams to prepare
our students for real exams. Also, we regularly conduct exam-preparation class to shape and groom our students from an exam-focused methadology.
                            </p>
                            <a href="#"  class=" btn-blue-purple-gradient">
                                Apply
                            </a>
                        </div>
                        <div class="col-sm-6">
                            <img src="<?=base_url();?>assets/eacademy/images/72ppi/image6.png"  alt="" width="489" height="556" class="img-responsive lazyload xs-inline-block">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section >
            <div class="feature-lists">
                <div class="feature" style="background-color:#D64B9A;">
                <h3 clss='section-title f30 mb15'>mONEY BACK <br>
                    OFFER</h3>
                <p>If we fail to meet your expectation,
                    you will get your money back.</p>
            </div>
            <div class="feature" style="background-color:#EAC900;">
                <h3 clss='section-title f30 mb15'>24/7
                    SUPPORT</h3>
                <p>If you face any kind of technical issue,
                    you are available 24/7 to fix it right away.</p>
            </div>
            <div class="feature" style="background-color:#00B8F1;">
                <h3 clss='section-title f30 mb15'>SPECIAL CLASS</h3>
                <p>We understand some students need special class,
                    for which we have the option available.</p>
            </div>
            <div class="feature" style="background-color:#F6931F;">
                <h3 clss='section-title f30 mb15'>iNTERACTIVE <br>
                    GAMES</h3>
                <p>To add fun in learning process, we have interactive
                    games available at FREE of cost.</p>
            </div>
            <div class="feature" style="background-color:#86C43F;">
                <h3 clss='section-title f30 mb15'>BUILD TEAM</h3>
                <p>We value interpersonal relationship in personal
                    development and thus have forums available for
                    students to engage with like minded.</p>
            </div>
            <div class="feature" style="background-color:#D70045;">
                <h3 clss='section-title f30 mb15'>tHERAPY</h3>
                <p>We have a team of certified psychologiest
                    and occupational theraist to help students
                    in need of therapy</p>
            </div>
        </div>
        </section>
        <section>
            <div class="courses-list">
                <h2>Available courses</h2>
                <div class="courses">
                    <div class="item">
                        <h3>Pre-school</h3>
                        <p>Nursery, LKG, UKG</p>
                        <button>
                            join
                        </button>
                    </div>
                    <div class="item">
                        <h3>SCHOOL</h3>
                        <p>Class 1 - 10 </p>
                        <button>
                            join
                        </button>
                    </div>
                    <div class="item ">
                        <h3>+ 2</h3>
                        <p>Science/Management</p>
                        <button>
                            join
                        </button>
                    </div>
                    <div class="item">
                        <h3>BACHELOR’S</h3>
                        <p>BBA/BSC/BIIT</p>
                        <button>
                            join
                        </button>
                    </div>
                    <div class="item">
                        <h3>MASTER’S</h3>
                        <p>MBA/MBS/MA </p>
                        <button>
                            join
                        </button>
                    </div>
                    <div class="item">
                        <h3>ENTRANCE</h3>
                        <p>IOM/IOE/CAT</p>
                        <button>
                            join
                        </button>
                    </div>
                </div>
            </div>
        </section>
       
        <section class="section apps-section" id="apps-section">
            <div class="section-content">
                <div class="container">
                    <h2 class="section-title-1  mb30 text-center" style="margin-top: 0;">ALSO, AVAILABLE ON
			</h2>
                    <div class="text-center mb90 courses-store-block">
                        <a href="" data-m-href="" data-app-store="AppleButton">
                            <img src="<?=base_url();?>assets/eacademy/images/72ppi/apple.jpg"   alt="app store" width="208" height="65" class="lazyload">
                        </a>
                        <a href="" data-m-href="" data-app-store="AndroidButton">
                            <img src="<?=base_url();?>assets/eacademy/images/72ppi/android.jpg" alt="google store" width="208" height="65" class="lazyload">
                        </a>
                    </div>

                    <h2 class="section-title-1  mb30 text-center" style="margin-top: 0;">PAYMENT PARTNER
                    </h2>
                    <div class="text-center mb90 courses-store-block">
                        <a href="" >
                            <img src=""   alt="" width="208" height="65" class="lazyload">
                        </a>
                        <a href="">
                            <img src="" alt="" width="208" height="65" class="lazyload">
                        </a>
                    </div>

                </div>
            </div>
        </section>
        <section class="faq">
            <div class="title">
                <h3>FAQ</h3>
                <p>FREQUENTLY ASKED QUESTIONS</p>
            </div>
            <div class="question-section">
                    <div class="row">
                    <div class="col-md-6">
                        <div class="answers">
                            <div>
                                <h3>
                                    <strong>Q.</strong>
                                   Is it like ZOOM class?
                                </h3>
                                <p>
                                    No. We are much beyond and broader than a video-conference-based
                                    class.
                                </p>
                            </div>
                            <div>
                                <h3>
                                    <strong>Q.</strong> Can it be accessed offline?
                                </h3>
                                <p>
                                    Yes.
                                </p>
                            </div>
                            <div>
                                <h3>
                                    <strong>Q.</strong> do i need to pay monthly or
                                    annually?
                                </h3>
                                <p>
                                    Whatever works best for you, but we suggest to go for annually as you will
                                    save some money in that.
                                </p>
                            </div>
                            <div>
                                <h3>
                                    <strong>Q.</strong> why is there no trial class?
                                </h3>
                                <p>
                                    Because we are just like real world academy, where you don’t get as such trial
                                    but money back option.
                                </p>
                            </div>
                            <div>
                                <h3>
                                    <strong>Q.</strong>DO ECADEMY PROVIDES
                                    CERTIFCATES?
                                </h3>
                                <p>
                                    No.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="quries">
                            <h3>Do you have a question?</h3>
                            <form action="" class="form">
                                <input type="text" placeholder="YOUR NAME">
                                <input type="text" placeholder="MOBILE NO.">
                                <input type="email" placeholder="E-MAIL"> <br>
                                <textarea cols="30" rows="7" placeholder="YOUR QUESTION"></textarea>
                                <a class="btn-blue-purple-gradient"href="">SUBMIT</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="ourplan">
            <div class="container">
                <h3>
                    WE PLAN TO ENROLL OVER 1 LAKH STUDENTS IN 2020.
                </h3>
                <p>
                    Refer and Earn
                </p>
                <form action="">
                    <input type="text" placeholder="Student's Name">
                    <input type="email" placeholder="E-Mail">
                    <input type="text" placeholder="Phone No.">
                </form>
                <button>
                    APPLY
                </button>
            </div>
    
        </section>
    
        <footer>
            <div class="footer-content">
                <div class="footer-section">
                    <div class="container">
                        <div class="row unit10">
                            <h3>footer</h3>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
       <script>
            (function() {
                var e, t, a, o, n, r;
                "NodeList"in window && !NodeList.prototype.forEach && (console.info("polyfill for IE11")
                )
                ;
                var l = {
                    onScrollEvent: function() {
                        function e(e) {
                            e = void 0 !== e ? e : window,
                            e.scrollY < a ? t.className = t.className.replace(/\ animate\b/g, "") : -1 == t.className.indexOf("animate") && (t.className += " animate")
                        }
                        var t = document.getElementById("top-navbar")
                          , a = 2;
                        window.onscroll = function(t) {
                            this.oldScroll > this.scrollY || (RAG_SCROLL_TOP = (window.scrollY || window.pageYOffset) + window.innerHeight + 0),
                            this.oldScroll = this.scrollY,
                            e(this)
                        }
                        ,
                        e()
                    },
                    onPageLoadFunctions: function() {
                        l.onScrollEvent(),
                        GA_FLAG && (document.querySelectorAll("#top-navbar a").forEach(function(e, t) {
                            ga("send", "event", "Top Bar", "navigation link click", "link name: " + e.innerText)
                        }))
                    }
                };
                l.onPageLoadFunctions()
            }
            )();
        </script>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        
    </body>
</html>
