<style>
.pay{
   margin:22px;
}
</style>
   <body>
      <div class="container-fluid">
         <nav class="navbar navbar-expand-lg navbar-light bg-white">
            <a class="navbar-brand" href="<?=base_url();?>">
            <img src="<?=base_url();?>assets/eacademy/images/logo.png" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
               <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                  <li class="nav-item active">
                  <!-- <?=base_url();?>etour -->
                     <a class="nav-link" href="<?=base_url();?>faqtour">Tour eCADEMY <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="#courses">Available Courses</a>
                  </li>
                  <!-- <li class="nav-item">
                     <a class="nav-link" href="#">Price</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="#" style="border-right:none">Offer</a>
                  </li> -->
                  <li class="nav-item">
                     <a class="nav-link contact" href="javascript:void(0)" >Contact Us</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link button" href="<?=base_url();?>applicationform" >Apply</a>
                  </li>
                  <li class="nav-item">
		     <?php //echo base_url(); ?>
                     <a class="nav-link button" href="<?=base_url();?>login" >Log In</a>
                  </li>
               </ul>
            </div>
         </nav>
      </div>
      <section class="section banner sec1">
         <div class="container-fluid no-padding">
            <div class="row align-items-center">
               <div class="col-lg-7 col-md-12 ">
                  <img src="<?=base_url();?>assets/eacademy/images/img1.jpg" class="img-fluid" alt="">
               </div>
               <div class="col-lg-5 col-md-12">
                  <div class="banner-content">
                     <p class="fs-26 red-color biggest mb0 text-uppercase mt32">NURSERY TO MASTER’S IN ONE PLACE</p>
                     <p class="thin fs-14" style="margin-top:-4px;">eCADEMY makes it possible now to study any course anytime from anywhere</p>
                     
                     <h3 class="blue-color biggest fs-26">
                        What's Inside
                     </h3>
                     <ul class="banner-list">
                        <li class="fs-18 light"><span>--</span> <span>Pre-school, School, +2, Bachelor’s, and Master’s courses.</span></li>
                        <li class="fs-18 light"><span>--</span> <span>Entrance Preparation.</span></li>
                        <li class="fs-18 light"><span>--</span> <span>Classroom-like learning experience.</span></li>
                        <li class="fs-18 light"><span>--</span> <span>Exam-preparation classes and mockup exams.</span></li>
                        <li class="fs-18 light"><span>--</span> <span>LIVE teachers for doubt clearing.</span></li>
                        <li class="fs-18 light"><span>--</span> <span>Audio/visual learning materials.</span></li>
                        <li class="fs-18 light"><span>--</span> <span>Periodic assessment and personalised feedbacks.</span></li>
                        <li class="fs-18 light"><span>--</span> <span>Affordable pricing.</span></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
         <div class="bg-gray sec-break">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-sm-7 offset-sm-1 text-center">
                     <p class="mt-16 mb0 thin text-center fs-fs-28">Need counseling to learn more about eCADEMY before enrolling?</p>
                     <p class="fs-28 light text-center myriad-regular">Apply for an appointment to <strong class="myriad-bold">directly talk to us</strong></p>
                  </div>
                  <div class="col-sm-3 text-center">
                     <a href="<?=base_url();?>applicationform" class="button big-twolined fs-28">Book My <span class="alt-blue">Appointment</span></a>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="banner sec2">
         <div class="container-fluid pt32">
            <h2 class="text-center fs-50 biggest text-uppercase">About Us</h2>
            <div class="row align-items-center">
               <div class="col-lg-7 col-md-12 order-sm-2 no-padding">
                  <img src="<?=base_url();?>assets/eacademy/images/img2.jpg" class="img-fluid" alt="">
               </div>
               <div class="col-lg-5 col-md-12 order-sm-1">
                  <div class="">
                     <img src="<?=base_url();?>assets/eacademy/images/logo.png" class="img-fluid logo-big" alt="">
                     <div class="sec-2">
                        <p class="nepali fs-32 red-color">
                           अब पढाउने जिम्मा हाम्रो। 
                        </p>
                        <br>
                        <p class="fs-18 text-justify">
                           A digital academy to provide a classroom-like
                           digital setup that includes all the learning and
                           teaching materials, and stand-by teachers
                           needed for a student to accomplish
                           his/her academic goal.
                        </p>
                        <br>
                        <div class="text-left">
                           <a href="<?=base_url();?>faqtour" class="btn-primary">Learn More</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="section-has-align bg-blue sec3">
         <div class="container-fluid">
            <div class="row">
               <div class="col-sm-6">
                  <img src="<?=base_url();?>assets/eacademy/images/sec3.png" alt="" class="img-fluid">
               </div>
               <div class="col-sm-5 pt32 text-right">
                  <h2 class="fs-36 text-right biggest text-uppercase">
                     Classroom-Like Learning Experience
                  </h2>
                  <p class="nepali fs-32 red-color">
                     sIffsf]&f d} k(] ;/x 
                  </p>
                  <p>
                     Your learning experience in eCADEMY will be as real as classroom learning, where
                     you can study what’s required per your syllabus and curriculum following the
                     standard study-load protocol set by education expert and education board.
                  </p>
                  <p>
                     All the learning materials and contents are kept in day wise fashion, which is further
                     segregated into notes, videos, tasks and assignments -- just like how in classroom,
                     teacher starts from a capter but in between there happens Q&A, interactions,
                     explanations, along with references and different perspectives that ends with
                     homeworks. Isn’t that super awesome?
                  </p>
                  <br>
                  <div class="text-center">
                     <a href="<?=base_url();?>applicationform" class="btn-primary">Apply</a>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="section-has-align sec4 ">
         <div class="container-fluid">
            <div class="row">
               <div class="col-sm-6 order-sm-2">
                  <img src="<?=base_url();?>assets/eacademy/images/sec4 (2).png" alt="" class="img-fluid scale">
               </div>
               <div class="col-sm-5 offset-sm-1 pt64 order-sm-1">
                  <h2 class="fs-36  biggest text-uppercase">
                     LIVE TEACHERS <br>
                     FOR DOUBT CLEARING
                  </h2>
                  <p class="nepali fs-32 red-color">
                     k(]sf] ga"h]df lzIfs pknAw  
                  </p>
                  <p>
                     One of the biggest challenges of online learning platform is that
                     students are left on their own to study, but realistically speaking,
                     most of the times, students may not understand certain thing that
                     they wish there be someone to clear the doubt.
                  </p>
                  <p>
                     Understanding the need and the challenges, eCADEMY provides
                     stand-by teacher to reach out for doubt clearning, where our
                     teachers will be a click away to come LIVE when any sudent
                     is in need.
                  </p>
                  <br>
                  <div class="text-center">
                     <a href="<?=base_url();?>applicationform" class="btn-primary">Apply</a>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="section-has-align bg-blue sec5">
         <div class="container-fluid">
            <div class="row">
               <div class="col-sm-6">
                  <img src="<?=base_url();?>assets/eacademy/images/sec4.png" alt="" class="img-fluid">
               </div>
               <div class="col-sm-5 pt80 pt-xs-32 text-right">
                  <h2 class="fs-36 text-right biggest text-uppercase">
                     SYLLABUS-MAPPED
                     LEARNING MATERIALS
                  </h2>
                  <p class="nepali fs-32 red-color">
                     पाठ्यक्रममा आधारित शैक्षिक सामग्री 
                  </p>
                  <p>
                     Pre-loaded contents that include interactive videos, teacher’s notes, illustrations,
                     along with other useful referrences mapped according to the
                     syllabus of respective course and class, that students
                     can access and revise numerous times until not thorough with.
                  </p>
                  <br>
                  <div class="text-center">
                     <a href="<?=base_url();?>applicationform" class="btn-primary">Apply</a>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="section-has-align sec6">
         <div class="container-fluid">
            <div class="row">
               <div class="col-sm-6 order-sm-2">
                  <img src="<?=base_url();?>assets/eacademy/images/sec5.png" alt="" class="img-fluid scale">
               </div>
               <div class="col-sm-5 offset-sm-1 pt80 pt-xs-32 order-sm-1">
                  <h2 class="fs-36  biggest text-uppercase">
                  EXAM PREPARATION CLASS & MOCK-TEST
                  </h2>
                  <p class="nepali fs-32 red-color">
                  जाँच तयारी कक्षा र नमुना परिक्षा
                  </p>
                  <p>
                     One of the goals of eCADEMY is to ensure that our students score well in
                     board exams, for which we conduct regular mock-up exams to prepare
                     our students for real exams. Also, we regularly conduct exam-preparation class
                     to shape and groom our students from an exam-focused methadology.
                  </p>
                  <br>
                  <div class="text-center">
                     <a href="<?=base_url();?>applicationform" class="btn-primary">Apply</a>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="section-has-align bg-blue sec7">
         <div class="container-fluid">
            <div class="row">
               <div class="col-sm-6">
                  <img src="<?=base_url();?>assets/eacademy/images/sec6.png" alt="" class="img-fluid">
               </div>
               <div class="col-sm-5 pt32 text-right">
                  <h2 class="fs-36 text-right biggest text-uppercase">
                     PERSONALIZED <br>
                     MONITORING & FEEDBACK
                  </h2>
                  <p class="nepali fs-32 red-color">
                  व्यक्ति विशेष निगरानी सुझाब र परामर्श
                  </p>
                  <p>
                     CADEMY is determined to ensure that its students excel academically
                     for which, we provide regular feedbacks and guidance in
                     each student periodically through our teachers.
                  </p>
                  <p>
                     In other words, we have teachers to keep a close eye on our students
                     to guide and mentor and help them get better in exam and everyday learning.
                  </p>
                  <br>
                  <div class="text-center">
                     <a href="<?=base_url();?>applicationform" class="btn-primary">Apply</a>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="section-has-align bg-pink sec8">
         <div class="container-fluid">
            <div class="row">
               <div class="col-sm-6 order-sm-2">
                  <img src="<?=base_url();?>assets/eacademy/images/sec7.png" alt="" class="img-fluid scale">
               </div>
               <div class="col-sm-5 offset-sm-1 pt80 pt-xs-32 order-sm-1">
                  <h2 class="fs-36  biggest text-uppercase">
                     STUDY AT YOUR
                     OWN PACE
                  </h2>
                  <p class="nepali fs-32 red-color">
                  आफ्नै गतिमा पढ्न पाइने 
                  </p>
                  <p>
                     Helping students to study any subject, anytime, anywhere
                     is our founding ethos, by which we have designed our
                     content management system in very sophisticated manner
                     with new-edge technology that a student can study in our
                     platform without fearing to be left behind
                  </p>
                  <br>
                  <div class="text-center">
                     <a href="<?=base_url();?>applicationform" class="btn-primary">Apply</a>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="banner-break-1">
         <div class="container-fluid no-padding">
            <div class="row no-padding">
               <div class="no-padding col-sm-4  colored-boxes pink">
                  <p class="text-center biggest fs-36 text-white">MONEY BACK <br> OFFER</p>
                  <p class="fs-18 text-center">If we fail to meet your expectation, you will get your money back.</p>
               </div>
               <div class="no-padding col-sm-4  colored-boxes yellow">
                  <p class="text-center biggest fs-36 text-white">24/7 <br>
                     SUPPORT
                  </p>
                  <p class="fs-18 text-center">If you face any kind of technical issue,
                     we are available 24/7 to fix it right away
                  </p>
               </div>
               <div class="no-padding col-sm-4  colored-boxes blue">
                  <p class="text-center biggest fs-36 text-white">SPECIAL CLASS</p>
                  <p class="fs-18 text-center">We understand some students need special class,
                     for which we have the option available.
                  </p>
               </div>
               <div class="no-padding col-sm-4  colored-boxes orange">
                  <p class="text-center biggest fs-36 text-white">INTERACTIVE <br>
                     GAMES
                  </p>
                  <p class="fs-18 text-center">To add fun in learning process, we have interactive
                     games available at FREE of cost.
                  </p>
               </div>
               <div class="no-padding col-sm-4  colored-boxes green">
                  <p class="text-center biggest fs-36 text-white">BUILD TEAM</p>
                  <p class="fs-18 text-center">We value interpersonal relationship in personal
                     development and thus have forums available for
                     students to engage with like minded.
                  </p>
               </div>
               <div class="no-padding col-sm-4  colored-boxes red">
                  <p class="text-center biggest fs-36 text-white">THERAPY</p>
                  <p class="fs-18 text-center">
                     We have a team of certified psychologiest
                     and occupational theraist to help students
                     in need of 
                     <therapy></therapy>
                  </p>
               </div>
            </div>
         </div>
      </section>
      <h2 class="fs-50 pt32 pb32 biggest text-center text-uppercase" id="courses">Available Courses</h2>
      <section class="banner-break-1">
         <div class="container-fluid no-padding">
            <div class="row no-padding">
               <div class="no-padding col-sm-4  colored-boxes blue-boxes">
                  <p class="text-center biggest fs-36 text-white text-uppercase">pre-school</p>
                  <p class="fs-18 alt-blue text-center">Nuresery, LKG, UKG</p>
                  <ul class="box-list">
                     <li>Alphabet</li>
                     <li>Numbers</li>
                     <li>Drawing</li>
                  </ul>
                  <a href="<?=base_url();?>applicationform" class="btn-primary biggest btn-white">APPLY</a>
               </div>
               <div class="no-padding col-sm-4  colored-boxes blue-boxes">
                  <p class="text-center biggest fs-36 text-white text-uppercase"> school</p>
                  <p class="fs-18 alt-blue text-center">Class 1-10</p>
                  <ul class="box-list">
                     <li>Science</li>
                     <li>Maths</li>
                     <li>English</li>
                     <li>Nepal</li>
                     <li>Social Study</li>
                  </ul>
                  <a href="<?=base_url();?>applicationform" class="btn-primary biggest btn-white">APPLY</a>
               </div>
               <div class="no-padding col-sm-4  colored-boxes blue-boxes">
                  <p class="text-center biggest fs-36 text-white text-uppercase">+2</p>
                  <p class="fs-18 alt-blue text-center">Science/Management</p>
                  <ul class="box-list">
                     <li>Science</li>
                     <li>Maths</li>
                     <li>English</li>
                     <li>Nepal</li>
                     <li>Social Study</li>
                  </ul>
                  <a href="<?=base_url();?>applicationform" class="btn-primary biggest btn-white">APPLY</a>
               </div>
               <div class="no-padding col-sm-4  colored-boxes blue-boxes">
                  <p class="text-center biggest fs-36 text-white text-uppercase">bachelor's</p>
                  <p class="fs-18 alt-blue text-center">BBA/BSC/BIIT</p>
                  <ul class="box-list">
                     <li>Science</li>
                     <li>Maths</li>
                     <li>English</li>
                     <li>Nepal</li>
                     <li>Social Study</li>
                  </ul>
                  <a href="<?=base_url();?>applicationform" class="btn-primary biggest btn-white">APPLY</a>
               </div>
               <div class="no-padding col-sm-4  colored-boxes blue-boxes">
                  <p class="text-center biggest fs-36 text-white text-uppercase">master's</p>
                  <p class="fs-18 alt-blue text-center">MBA/MBS/MA</p>
                  <ul class="box-list">
                     <li>Science</li>
                     <li>Maths</li>
                     <li>English</li>
                     <li>Nepal</li>
                     <li>Social Study</li>
                  </ul>
                  <a href="<?=base_url();?>applicationform" class="btn-primary biggest btn-white">APPLY</a>
               </div>
               <div class="no-padding col-sm-4  colored-boxes blue-boxes">
                  <p class="text-center biggest fs-36 text-white text-uppercase">entrance</p>
                  <p class="fs-18 alt-blue text-center">IOM/IOE/CAT</p>
                  <ul class="box-list">
                     <li>Science</li>
                     <li>Maths</li>
                     <li>English</li>
                     <li>Nepal</li>
                     <li>Social Study</li>
                  </ul>
                  <a href="<?=base_url();?>applicationform" class="btn-primary biggest btn-white">APPLY</a>
               </div>
            </div>
         </div>
      </section>
      <section class="app-plug pb64">
      <img src="<?=base_url();?>/assets/images/AvailableOn.png" style="
            width: 100%;
         ">
         <div class="container">
            <!-- <h2 class="fs-50 thinnest text-center text-uppercase">ALSO, AVAILABLE ON</h2>
            <div class="row pt64 pb64">
               <div class="col-sm-4">
                  <a href="#"><img src="<?=base_url();?>assets/eacademy/images/apple.png" alt="" class="img-fluid d-block ml-auto mr-auto"></a>
               </div>
               <div class="col-sm-4"></div>
               <div class="col-sm-4">
                  <a href="#"><img src="<?=base_url();?>assets/eacademy/images/android.png" alt="" class="img-fluid d-block ml-auto mr-auto"></a>
               </div>
            </div> -->
            <br>
            <h2 class="fs-50 thinnest text-center text-uppercase">payment partner</h2>
                 <div class="row justify-content-center alilgn-items-center">
                     
                   <img src="<?=base_url();?>assets/eacademy/images/payment/esewa.png" alt="" class="img-fluid pay" >
                   <img src="<?=base_url();?>assets/eacademy/images/payment/khalti.png" alt="" class="img-fluid pay" >
                   <img src="<?=base_url();?>assets/eacademy/images/payment/imepay.png" alt="" class="img-fluid pay" >
                   <img src="<?=base_url();?>assets/eacademy/images/payment/visa.png" alt="" class="img-fluid pay" >
                   <img src="<?=base_url();?>assets/eacademy/images/payment/mastercard.png" alt="" class="img-fluid pay">
                   <img src="<?=base_url();?>assets/eacademy/images/payment/allbank.png" alt="" class="img-fluid pay">
                   <img src="<?=base_url();?>assets/eacademy/images/payment/fonepay.png" alt="" class="img-fluid pay">
                   <img src="<?=base_url();?>assets/eacademy/images/payment/sctpay.png" alt="" class="img-fluid pay">

                 </div>
               
           
         </div>
      </section>
      <section class="bg-green pt64 pb64 faqs">
         <div class="container-fluid">
            <h2 class="fs-50 pt32 pb32 biggest text-center text-uppercase" style="line-height:24px;">FAQs <br><small class="fs-18 thinnest text-white">Frequently Asked Questions</small></h2>
            <div class="row">
               <div class="col-sm-6 faq-wrap">
                  <div id="accordion">
                  <?php foreach($faq as $list): ?>
                     <div class="card">
                        <div class="card-header" id="heading<?=$list->faqid;?>">
                           <button class="btn btn-link fs-20" data-toggle="collapse" data-target="#collapse<?=$list->faqid;?>" aria-expanded="true" aria-controls="collapse<?=$list->faqid;?>">
                              <p class="fs-28 biggest question"><?=$list->question;?>
                                 <span><i class="fa fa-chevron-down"></i></span>
                              </p>
                           </button>
                        </div>
                        <div id="collapse<?=$list->faqid;?>" class="collapse" aria-labelledby="heading<?=$list->faqid;?>" data-parent="#accordion">
                           <div class="card-body text-white" style="font-weight:600;">
                           <?=$list->answer;?>
                           </div>
                         </div>
                     </div>
                  <?php endforeach; ?>
                    
                  </div>
                  
                  <!-- <div class="faq">
                     <p class="fs-36 biggest question mb0">CAN IT BE ACCESSED OFFLINE?</p>
                     <p class="fs-18 answer thinnest">Yes.</p>
                  </div>
                  <div class="faq">
                     <p class="fs-36 biggest question mb0">DO I NEED TO PAY MONTHLY OR
                        ANNUALLY?
                     </p>
                     <p class="fs-18 answer thinnest">Whatever works best for you, but we suggest to go for annually as you will
                        save some money in that.
                     </p>
                  </div>
                  <div class="faq">
                     <p class="fs-36 biggest question mb0">WHY IS THERE NO TRIAL CLASS?</p>
                     <p class="fs-18 answer thinnest">Because we are just like real world academy, where you don’t get as such trial
                        but money back option.
                     </p>
                  </div> -->
               </div>
               <div class="col-sm-6">
                  <!-- <div class="faq">
                     <p class="fs-36 biggest question">DO ECADEMY PROVIDES
                        CERTIFCATES?
                     </p>
                     <p class="fs-18 answer thinnest">No.</p>
                  </div>
                  <br> -->
                  <p class="fs-36 text-white text-uppercase">DO YOU HAVE A QUESTION?</p>
                  <form class="faq-form text-center" id="faqform" method="post">
                     <div class="form-group">
                        <input type="text" class="form-control text-center" name="name" placeholder="Your Name" required>
                     </div>
                     <div class="form-group">
                        <input type="text" class="form-control text-center" name="mobile" placeholder="Mobile Number" required>
                     </div>
                     <div class="form-group">
                        <input type="email" class="form-control text-center" name="email" placeholder="Your Email" required>
                     </div>
                     <div class="form-group">
                        <textarea name="ques" class="form-control text-center" rows="4" placeholder="Your Question" required></textarea>
                     </div>
                     <div class="form-group">
                        <div id="err" style="color:red"></div>
                         <p id="success" style="color:yellow;margin-left:-100px;"></p> 
                     </div>
                       
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </form>
               </div>
            </div>
         </div>
      </section>
      <section class="pre-footer pt32 pb32">
         <div class="container">
            <p class="fs-36 biggest text-center question mb0">WE PLAN TO ENROLL OVER 1 LAKH STUDENTS IN 2020.</p>
            <p class="fs-36 thinnest answer thinnest text-center">Be our partner, refer and earn</p>
            <br>
            <form class="enroll-form faq-form " id="referform" method="post">
               <div class="form-row">
                  <div class="col">
                     <input type="text" class="form-control" placeholder="Student's name" name="studentname" required>
                  </div>
                  <div class="col">
                     <input type="email" class="form-control" placeholder="Email address" name="studentemail" required>
                  </div>
                  <div class="col">
                     <input type="text" class="form-control" placeholder="Phone Number" name="studentphone" required>
                  </div>
               </div>
               <br>
               <div class="form-group">
                        <div id="refererr" style="color:red"></div>
                         <p id="refersuccess" style="color:green;"></p> 
                     </div>
               <div class="row text-center">
                  <div class="col-sm-2 offset-sm-5">
                     <button type="submit"  class="btn-primary refere d-sm-block"  id="btnrefer" style="    margin-left: 20px;">REFER</button>
                  </div>
               </div>
            </form>
         </div>
      </section>
      <section class="footer">
         <div class="container-fluid">
            <div class="row align-items-center">
               <div class="col-sm-3">
                  <img src="<?=base_url();?>assets/eacademy/images/logo.png" alt="" class="foot-logo img-fluid">
                  <p class="fs-18 mb0">Midas Education Pvt Ltd</p>
                  <p class="fs-18 mb0">Thapathali, Kathmandu</p>
                  <p class="fs-18 mb0"><a href="tel:01 411 5692" class="text-dark">01 411 5692</a></p>
               </div>
               <div class="col-sm-6 text-center">
                  <p class="nepali fs-32 red-color mb0">अब पढाउने जिम्मा हाम्रो। </p>
                  <p class="fs-28 text-dark thin">...for a better learning experience</p>
               </div>
               <div class="col-sm-3">
                  <ul class="footer-list">
                     <li><a href="#" class="text-dark">About Us </a></li>
                     <li><a href="#" class="text-dark" data-toggle="modal" data-target=".contact-modal">Contact Us </a></li>
                     <li><a href="#" class="text-dark">Support </a></li>
                     <li><a href="#" class="text-dark">Videos </a></li>
                     <br>
                     <li><a href="#" class="text-dark">Disclaimer </a></li>
                     <li><a href="#" class="text-dark" data-toggle="modal" data-target=".privacy-modal">Privacy Policy </a></li>
                     <li><a href="#" class="text-dark" data-toggle="modal" data-target=".terms-modal">Terms of Services</a></li>
                  </ul>
                  <ul class="social-list text-center">
                     <li><a href=""><i class="fab fa-facebook-f"></i></a></li>
                     <li><a href=""><i class="fab fa-whatsapp"></i></a></li>
                     <li><a href=""><i class="fab fa-twitter"></i></a></li>
                     <li><a href=""><i class="fab fa-instagram"></i></a></li>
                  </ul>
                  <p class="text-center fs-16 thin follow">Follow Us</p>
               </div>
            </div>
         </div>
      </section>
      <div class="modal fade bd-example-modal-lg terms-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
         <div class="modal-dialog modal-lg">
            <div class="modal-content">
               <div class="modal-header">
                  <h4 class="modal-title" id="myLargeModalLabel">Terms Of Service</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                  </button>
               </div>
               <div class="modal-body">
                  <div class="terma">
                     <article id="main_article1">
                        <p class="text-justify">Welcome to MiDas eCLASS. MiDas eCLASS is a communicating tool that helps Parents communicating with their children's teachers, school admins, view activity of their children's and many more. We're glad you're here, but there are some rules you need to agree to before you use our services. 
                           These Terms of Service (the "Terms") are a binding contract between you and MiDas eCLASS. You using the Services in any way means that you agree to all of these Terms, and these Terms will remain in effect while you use the Services.
                        </p>
                        <p>
                           Our Services are constantly changing, to keep up with the dynamic needs of users everywhere - so, these Terms might need to change, too. If they do change, we will do our best to tell you in advance by placing a notification on the MiDas eCLASS. In certain situations (for example, where a change to the Terms is necessary to comply with legal requirements), we may not be able to give you advance notice. MiDas eCLASS takes the privacy of its users very seriously.
                           What are the basics of using MiDas eCLASS?
                           First, you have to sign up through our app which is available in Google Play Store. Then you need to follow the following process:
                        </p>
                        <ul class="dashed">
                           <li>1. Enter your Mobile Number</li>
                           <li>2. Enter your First name, last name and email (Optional)</li>
                           <li>3. Select district of the School</li>
                           <li>4. Choose the school from the list or create a new one if not available</li>
                           <li>5. Enter your children's first name, last name, class and class code (will be provided by the school). </li>
                           <li>6. Enter your desired password for MiDas eCLASS to complete the Sign Up process</li>
                        </ul>
                        <p>As a Parents, you can </p>
                        <ul class="mycustul">
                           <li>1. View Attendance of your Childrens</li>
                           <ul class="padleft50">
                              <li>- If your child is absent or late, you will be notified through message. </li>
                           </ul>
                           <li>2. View Homework of your children</li>
                           <ul class="padleft50">
                              <li>- You will be notified about your children's homework.</li>
                           </ul>
                           <li>3. Monitor/Evaluate Homework  of your class</li>
                           <ul class="padleft50">
                              <li>- You will be notified about the status of your children’s homework.</li>
                           </ul>
                           <li>4. Send message to Individual Teacher of your child</li>
                        </ul>
                        Please note, to get message, the techer or teachers should have installed MiDas App - For Teachers and should be connected to Internet.
                        <p class="text-justify">You promise to only use the Services for your personal, internal, non-commercial, educational use, and only in a manner that complies with all laws that apply to you.</p>
                        <!-- <h4>The Content and Data</h4> 
                           <p class="text-justify">The Data (including, but not limited to, messaging text, graphics, articles, photos, images, illustrations, homework, User Submissions and so forth) can be sent by Teacher or School Administrator. Some of the details of information that can be sent are as following:</p>
                           <ul>
                               <li>1. You will send message to Teacher for which you will be accountable</li>
                               <li>2. School Administrator should carefully monitor the content of the text sent by Teacher</li>
                               <li>3. Academic and student related information, Home work, Attendance, Mark Sheet will be sent by Teacher or School administrator</li>
                               <li>4. The information sent through MiDas App - for Teacher is very sensitive, accordingly due care should be taken before sending any messages</li>
                               <li>5. The school should monitor the security access of user of MiDas Apps - for Parents.</li>
                           </ul> -->
                        <h4>Who is responsible for what I see and do on the Services?</h4>
                        <p class="text-justify">Any information or content publicly posted or privately transmitted through the Services is the sole responsibility of the user themsleves, and you access all such information and content at your own risk, and we aren't liable for any errors or omissions in that information or content or for any damages or loss you might suffer in connection with it. </p>
                        <p class="text-justify">We require Parents to guard their User ID and Password with the appropriate confidentiality.</p>
                        <p class="text-justify">You are responsible for all Content you contribute, in any manner, to the Services, and you represent and warrant you have all rights necessary to do so, in the manner in which you contribute it. You will keep all your registration information accurate and current. You are responsible for all your activity in connection with the Services.</p>
                        <h4>Will MiDas eCLASS ever change the Services?</h4>
                        <p class="text-justify">
                           MiDas eCLASS is a dynamic communicating tool, so the Services will change over time. We may change or introduce new features and Services. 
                        </p>
                        <h4>Does MiDas eCLASS cost anything?</h4>
                        <p class="text-justify">The basic MiDas eCLASS Services are free and always will be - that is, we don't charge for signing up for a basic services such as</p>
                        <style>
                           ul.dashed li{
                           display: block;
                           }
                        </style>
                        <ul class="dashed">
                           <li>- School Calendar, </li>
                           <li>- Attendance,</li>
                           <li>- Homework, </li>
                           <li>- Exam Routine</li>
                           <li>- Marks and</li>
                           <li>- Messaging</li>
                        </ul>
                        <h4>Privacy</h4>
                        <p class="text-justify">We take the privacy of our users very seriously. Another important document to look at is our Privacy Policy, which outlines what personal information MiDas eCLASS collects from you and how we use that information to provide our service.</p>
                        <p class="text-justify">Note that, by using the Services, you may receive text messages on your phone or mobile device over cellular network or over the internet, which may cause you to incur usage charges or other fees or costs in accordance with your wireless or data service plan. Any and all such charges, fees, or costs are your sole responsibility. You should consult with your wireless carrier to determine what rates, charges, fees, or costs may apply to your use of the Services.</p>
                        <!-- <h4>For School Administrator</h4>
                           <ul>
                               <li>If you are a school administrator:</li>
                               <ul><li>You must maintain the accuracy of the information relating to your School. By way of example, you will only permit staff members (such as Parents) who are current employees of your school to use the Services and to create their own class.</li></ul>
                               <li>You must know that any teacher can send Mass message through this app to Individual / Group of Parents, Parents or Parents</li>
                               <ul><li>MiDas eCLASS  verifies you as an administrator of your School, you may have the ability to perform the following tasks:</li></ul>
                               <ul class="no-bull">
                                   <li>1.	Able to view and manage users and information affiliated with your School.</li>
                                   <li>2.	Add Parents, Parents or Students to the Services,</li>
                                   <li>3.	Remove Parents, Parents or Students from classes</li>
                                   <li>4.	Connect and upload or sync information relating to Parents, Parents or Students using a data upload or syncing mechanism </li>
                               </ul>
                           </ul>
                           <p class="text-justify">If you choose to do any of the above, you represent and warrant that you have all rights and have obtained all consents and authorizations necessary to perform such tasks.</p>
                           -->
                        <h4>Registration and security</h4>
                        <p class="text-justify">As a condition to using Services, you are required to register with MiDas eCLASS. You will provide MiDas eCLASS with accurate, complete, and updated registration information. You may not</p>
                        <ul class="dashed">
                           <li>- select or use the name of another person with the intent to impersonate that person; or</li>
                           <li>- use a name subject to any rights of any person other than you without appropriate authorization.</li>
                        </ul>
                        <h4>Consent to receive periodic messages</h4>
                        <p class="text-justify">You acknowledge and consent to receive notifications, messages, sms and phone calls from MiDas Education and its sister concerns about its various products and services.</p>
                        <p class="text-justify">By signing up for the services, you agree to receive communications from MiDas eCLASS  as well as class, and you represent and warrant that each person you invite and/or add has consented to receive communications from you and MiDas eCLASS. You may receive messages sent by other Parents, Parents, or Students of the School or from MiDas Education and its sister concerns.</p>
                        <h4>Indemnity</h4>
                        <p class="text-justify">You will indemnify and hold MiDas eCLASS, its parents, subsidiaries, affiliates, officers, and employees harmless  from any claim or demand made by any third party due to or arising out of your access to or use of the Services, your violation of this Agreement, or the infringement by you or any third party using your account of any intellectual property or other right of any person or entity.</p>
                        <h4>Limitation of liability</h4>
                        <p class="text-justify">To the fullest extent allowed by applicable law, in no event will MiDas eCLASS  or its suppliers or its service providers, or their respective officers, directors, employees, or agents be liable with respect to the services. MiDas eCLASS is a communication tool. While we are here to support you, we are not liable for anything that happens because of our service.</p>
                        <h4>Choice of law and arbitration</h4>
                        <p class="text-justify">This Agreement will be governed by and construed in accordance with the laws of the Nepal without regard to the conflict of laws provisions thereof</p>
                        <h4>Miscellaneous</h4>
                        <p class="text-justify">MiDas eCLASS will not be liable for any failure to perform its obligations hereunder where such failure results from any cause beyond <br> MiDas eCLASS ’s reasonable control, including, without limitation, mechanical, electronic or communications failure or degradation.</p>
                     </article>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="modal fade bd-example-modal-lg privacy-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
         <div class="modal-dialog modal-lg">
            <div class="modal-content">
               <div class="modal-header">
                  <h4 class="modal-title" id="myLargeModalLabel">Privacy Policy</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                  </button>
               </div>
               <div class="modal-body">
                <div class="terma">
                    <article id="main_article2">    
                        <p class="text-justify">At MiDas Education Pvt. Ltd., we know you care about how your personal information is used and shared, and we take your privacy seriously. 
                            MiDas eCLASS is a communicating platform that helps parents users to send quick, simple messages to any device. MiDas eCLASS respects your information and treats it carefully.</p>

                            <p class="text-justify">You are responsible for any Content you provide in connection with the Services. We cannot control the actions of anyone with whom you or any other MiDas eCLASS users may choose to share information. Please be aware that no security measures are perfect or impenetrable and that we are not responsible for circumvention of any security measures contained on the Services. MiDas eCLASS does not encourage you to make any personally identifiable information (Personal Information) public other than what is necessary for you to use our Services. You understand and acknowledge that, even after removal, copies of Content may remain viewable in cached pages, archives and storage backups or if other users have copied or stored your Content.</p>

                            <p class="text-justify">MiDas eCLASS is a tool to help you communicate with teachers, school admins which helps support their children's education, and we take your privacy seriously. Maintaining the privacy of your Personal Information is a shared responsibility, and while we will limit what we ask for and what we do with your information, we encourage you not to share it unless you need to. Please keep in mind that you are responsible for the content of your account and all your messages.</p>

                            <h4>What does this privacy policy cover?</h4>
                            <p class="text-justify">This Privacy Policy explains how MiDas eCLASS collects and uses information from you and other users who access or use the Services, including our treatment of Personal Information.</p>
                            <p class="text-justify">MiDas eCLASS collects limited Personal Information of children and we rely on a Teacher,School to obtain the consent of each child’s parent when collecting this information.</p>

                            <h4>What information does MiDas eCLASS display or collect?</h4>
                            <p class="text-justify">When you use the Services, you may set up your personal profile, send messages, and transmit information as permitted by the functionality of the Services. MiDas eCLASS will not display your personal contact information to other users without your permission. The information we gather from users enables us to verify user identity, allows our users to set up a user account and profile through the Services, and helps us personalize and improve our Services. We retain this information to provide the Services to you and our other users and to provide a useful user experience.</p>
                            <h4>Information you provide to us</h4>
                            <p class="text-justify">We receive and store any information you knowingly enter on the Services, whether via mobile phone, other wireless device, or that you provide to us in any other way. This information may include Personal Information such as your name, phone numbers, email addresses, photographs, and, in certain circumstances, your school, School affiliation. We use the Personal Information we receive about you to provide you with the Services and also for purposes such as:</p>
                            <ul class="dashed">
                                <li>- authenticating your identity,</li> 
                                <li>- responding to your requests for certain information,</li> 
                                <li>- customizing the features that we make available to you,</li> 
                                <li>- suggesting relevant services or products for you to use,</li> 
                                <li>- improving the Services and internal operations (including troubleshooting, testing, and analyzing usage),</li> 
                                <li>- communicating with you about new features,</li> 
                                <li>- sending messages from MiDas Education and its sister concerns,</li>
                                <li>- and, most importantly, protecting our users and working towards making sure our Services are safer and more secure.</li>
                            </ul>
                            <h4>Information collected automatically</h4>
                            <p class="text-justify">We receive and store certain types of information whenever you use the Services. MiDas eCLASS automatically receives and records information on our server logs from MiDas App – for Parents.  We also may provide to our sister concerns aggregate information derived from automatically collected information about how our users, collectively, use our app. We may share this type of identifiable, aggregated statistical data so that MiDas Education and its sister concerns understand how often people use their services as well as MiDas eCLASS.</p>

                            <p class="text-justify">Will MiDas eCLASS share any of the personal information it receives? 
                                MiDas eCLASS relies on its users to provide accurate Personal Information in order to provide our Services. We try our best to protect that information and make sure that we are responsible in handling, disclosing, and retaining your data. We neither rent nor sell your Personal Information to anyone. However, we may share you information of products and service provided by MiDas Education and its sister concerns.</p>


                                <h4>Is information about me secure?</h4>
                                <p class="text-justify">The security of personal information of the childrens, teachers and school is a top priority for MiDas eCLASS.
                                    MiDas eCLASS has protections in place to enhance our user's security, and routinely update these protections. We have administrative, technical, and physical safeguards designed to protect against unauthorized use, disclosure of or access to personal information. In particular:</p>
                                    <ul class="dashed">
                                        <li>- Our engineering team is dedicated to keeping your personal information secure.</li>
                                        <li>- MiDas eCLASS stores its data within servers.</li>
                                        <li>- MiDas eCLASS’ maintain its database and all backups.</li>
                                    </ul>
                                    <br>

                                    <p class="text-justify"> MiDas eCLASS endeavors to protect user information and to ensure that user account information is kept private.</p>

                                    <p class="text-jusitfy">The security of your personal information is extremely important to us and we take measures internally to make sure your information is safe with us. We routinely update our security features and implement new protections for your data.</p>


                                </article>

                            </div>
               </div>
            </div>
         </div>
      </div>
     <?php $this->load->view('popup');?>
   <script>
    $('#faqform').on('submit', function(e){
    e.preventDefault();
        $.ajax({
                                type: 'post',
                               
                                url: "<?php echo base_url('home/submitfaq') ?>",
                                data:$('#faqform').serializeArray(),
                               
                                  success: function (response) {
                                    var res = jQuery.parseJSON(response);
                                    $('#err').html('');
                                    $('#success').text('');

                                    if(res.type=='success')
                                    { 
                                       $('#faqform')[0].reset();
                                       $('#success').text(res.message);

                                    }
                                    else
                                    {
                                        $('#err').html(res.message);
                                        return false;
                                    }


                                }
                            });
  });
  $('#referform').on('submit', function(e){
    e.preventDefault();
        $.ajax({
                                type: 'post',
                               
                                url: "<?php echo base_url('home/submitrefer') ?>",
                                data:$('#referform').serializeArray(),
                               
                                  success: function (response) {
                                    var res = jQuery.parseJSON(response);
                                    $('#refererr').html('');
                                    $('#refersuccess').text('');

                                    if(res.type=='success')
                                    { 
                                       $('#referform')[0].reset();
                                       $('#refersuccess').text(res.message);

                                    }
                                    else
                                    {
                                        $('#refererr').html(res.message);
                                        return false;
                                    }


                                }
                            });
  });
   </script>
   <!-- Load Facebook SDK for JavaScript -->
      <div id="fb-root"></div>
      <script>
        window.fbAsyncInit = function() {
          FB.init({
            xfbml            : true,
            version          : 'v8.0'
          });
        };

        (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
     

         
      </script>

      <!-- Your Chat Plugin code -->
      <div class="fb-customerchat"
        attribution=setup_tool
        page_id="101070671711603"
  logged_in_greeting="Hi! How can we help you?  [ नमस्कार! हामी तपाइलाई कसरी मद्दत गर्न सक्छौ‌ ? ]"
  logged_out_greeting="Hi! How can we help you?  [ नमस्कार! हामी तपाइलाई कसरी मद्दत गर्न सक्छौ‌ ? ]">
      </div>
