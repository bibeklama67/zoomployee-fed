
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css" integrity="sha512-xA6Hp6oezhjd6LiLZynuukm80f8BoZ3OpcEYaqKoCV3HKQDrYjDE1Gu8ocxgxoXmwmSzM4iqPvCsOkQNiu41GA==" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha512-MoRNloxbStBcD8z3M/2BmnT+rg4IsMxPkXaGh2zD6LGNNFE80W3onsAhRcMAMrSoyWL9xD7Ert0men7vR8LUZg==" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g==" crossorigin="anonymous" />  
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" integrity="sha512-sMXtMNL1zRzolHYKEujM2AqCLUR9F2C4/05cdbxjjLSRvMQIciEPCQZo++nk7go3BtSuK9kfa/s+a4f4i5pLkw==" crossorigin="anonymous" />
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
<body class="alt-body" style="background-color:#b2d7f9">
    
    
    <section class="full-height">

      <div class="langs">
      <span style="margin-left:85%;">
      <a href="<?=base_url();?>"><i class="fa fa-times fs-20 text-dark"></i></a>
       </span>
        <p class="fs-20"><span>
        <a href="#googtrans(en|ne)" data-language="ne" class="lang-ne" >Nepali</a></span> | <span><a href="#googtrans(ne|en)" data-language="en" class="lang-en l-ahanger  text-dark" >English</a></span></p>
    </div>
        <div class="owl-carousel" id="full-slider">
            <div class="item">
                <div class="container-fluid custom-fluid-padding">
                    <p class="about-page-text fs-50 biggest">
                        About <br>eCADEMY
                    </p>
                    <p class="fs-14">
                        MiDas eCADEMY is a digital academy to provide a classroom-like digital setup that includes all the learning and teaching materials, and stand-by teachers needed for a student to accomplish his/her academic goal.
                    </p>
                    <p class="fs-24 biggest">
                        WE ARE HERE TO SOLVE THE FOLLOWING PROBLEMS: 
                    </p>
                    <div class="row align-items-center">
                        <div class="col-sm-7">
                            <ol class="numbered-list">
                                <li class="fs-14">
                                    There is no viable alternative to college for office-going individuals, thus they are left only with the  option of “self study.”  Self-study has its own challenges like no immediate help to reach out at the 
                                    time of any confusion; no one to guide through syllabus; no assignment or tasks under 
                                    somebody’s supervision.
                                </li>
                                
                                <li class="fs-14">
                                    Many students in class setup don’t feel comfortable asking their teachers to re-explain any topic 
                                    or subject.  Likewise, sometimes, teachers don’t meet the expectation of students but students have 
                                    no option but to adjust.
                                </li>
                                
                                <li class="fs-14">
                                    Teaching method has still been traditional but students prefer multidisciplinary approach that 
                                    are proven to be more effective.
                                </li>
                                
                                <li class="fs-14">
                                    In college level education, despite the importance of exam and grades, teaching method has not 
                                    been much exam centered.
                                </li>
                                
                                <li class="fs-14">
                                    Content distribution channels like Wikipedia, YouTube, or other free websites are not  designed to 
                                    serve our students that follow syllabus and academic methodology from Nepal, thus self-study 
                                    approach is still a challenge. 
                                </li>
                                
                                <li class="fs-14">
                                    Language and cultural barrier.  Most of the contents available on the internet are developed and 
                                    produced by groups and individuals from cultural backgrounds different from Nepal, that sometimes 
                                    can be hard for our students to relate or understand.  Moreover, differences in accent and prosidy 
                                    have created a certain level of difficulty to follow through lectures and contents from those groups. 
                                </li>
                                
                            </ol>
                        </div>
                        <div class="col-sm-5">
                            <div id="video-wrapper">
                                <iframe width="100%" height="315" src="https://www.youtube.com/embed/Jh6jZftn2e0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="container-fluid custom-fluid-padding">
                    <p class="about-page-text fs-50 biggest">
                        Why <br>eCADEMY
                    </p>
                    <div class="row align-items-center">
                        <div class="col-sm-7">
                            <ol class="numbered-list">
                                <li class="fs-14">
                                    <strong>For better learning experience:</strong>  eCADEMY follows a learning methodology proven to be effective and pragmatic.  The pre-loaded contents in our system are designed and tabled according to the respective syllabus, which is further broken down adhering to study-load protocol set by education experts.   These contents are a mix of notes, interactive videos, curated learning materials, quizzes and brainstorming sessions, along with lecturers from relevant professions to give real-world knowledge on the subject and topics.   This set of activities and learning framework are proven to be providing the better learning experience for students in comparison to the traditional approach.
                                </li> 

                                <li class="fs-14">
                                    <strong>To study at your own pace, from anywhere, anytime</strong>:  eCADEMY is accessible 24/7 to its users and can access as well without internet for pre-loaded contents.  This gives the liberty for every student to study at their own pace, at any time they want to.
                                </li>  
                                
                                <li class="fs-14">
                                    <strong>To perform well in exams</strong>:  In eCADEMY, we don’t only facilitate students to study but also conduct regular exams and mock-up tests to prepare our students for their school and board exams.  Periodically, we also conduct exam-focused classes and activities which shall eventually groom our students to score well in exam.
                                </li>
                                
                                <li class="fs-14">
                                    <strong>To monitor your academic performance and mentor accordingly</strong>:  eCADEMY is the highly sophisticated technology-based learning platform integrating both AI and real-world teachers and experts to closely monitor and analyze each student’s academic performance, their study habit and behavior, through various means and methods.  The analyzed report will then be shared with students and guardians with appropriate suggestions and feedback.
                                </li>    
                                   
                                <li class="fs-14">
                                    <strong>For guardian-like partner</strong>:  eCADEMY aims to be the trusted education partner, as per which, we offer a service for those parents and individuals who wish for a specialised and personalized class and approach, so that they feel assured and secured.
                                </li>   
                                
                            </ol>
                        </div>
                        <div class="col-sm-5">
                            <div id="video-wrapper">
                                <iframe width="100%" height="315" src="https://www.youtube.com/embed/Jh6jZftn2e0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="container-fluid custom-fluid-padding">
                    <p class="about-page-text fs-50 biggest">
                        How <br>It Works ?
                    </p>
                    <div class="row align-items-center">
                        <div class="col-sm-7">
                            <ol class="numbered-list">
                                <li class="fs-14">
                                    <strong>Syllabus-maped contents:</strong>Contents in our systems are designed and tabled per syllabus, which is further tabled in day wise fashion.  Each day, students are to learn a specific topic per study-load protocol.  Like for example, Gravity as a specific topic.  Then this specific topic is presented in the form of notes, interactive videos, pictures and illustrations, quizzes and brainstorming sessions and ends with assignments. Meanwhile, students have the freedom to reach out to available teacher to learn further on the topic if needs extra assistance in understanding the content.
                                </li> 

                                <li class="fs-14">
                                    <strong>MiDas Learning Process compliant</strong>:  eMiDas has a proven learning process methodology designed and defined from 20 years of research and experiments in education sector, according to which every little things to learn and understand are meticulously pinned within the system to suit for self study approach that to further turn more effective, interactive contents like quizzes and animations are added into the process.  This whole activities are further assisted by standby teachers available online.
                                </li>  
                                
                                <li class="fs-14">
                                    <strong> AI-based technology</strong>:  eCADEMY is developed using the artificial intelligence based cutting edge technology that makes it possible to monitor student’s performance, learning pattern and behavior, and weighs sincerity and potential in its students and communicate the same with the student and his/her guardian.
                                </li>
                                
                                <li class="fs-14">
                                    <strong>Real teachers assistance and involvement</strong>:  eCADEMY is human and technology integrated revolutionary system, and thus, we have 100s of dedicated teachers working throughout the shifts to assist our students in their learning process and are willing to involve directly in conducting specialised class or personalized coaching in order to play the guardian or tuition teacher like role when deemed necessary.  Also, these teachers are involved directly in conducting exams, mock-up tests, entrance preparation class, etc to make sure that the students excel well academically.
                                </li>    
                                   
                                <li class="fs-14">
                                    <strong>Team of experts</strong>:As part of our integral system, we have good number of education experts and professionals in our team, who are here to conduct special class based on real world experience to give broader knowledge and understanding on respective subject and topics.  Likewise, we also have professional therapist and counsellors to help students and guardians in need of the service
                                </li>   
                                
                            </ol>
                        </div>
                        <div class="col-sm-5">
                            <div id="video-wrapper">
                                <iframe width="100%" height="315" src="https://www.youtube.com/embed/Jh6jZftn2e0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="container-fluid custom-fluid-padding">
                    <p class="about-page-text fs-50 biggest pt64 pt-xs-16">
                        Understand <br /> Through Q&As
                    </p>
                    <div class="row align-items-center">
                        <div class="col-sm-12">
                            <div id="accordion">
                               <div class="row">
                              <?php foreach($faq as $list): ?>
                                <div class="col-sm-6">

                            <div class="card">
                              <div class="card-header" id="heading<?=$list->faqid;?>">
                                  <button class="btn btn-link fs-20" data-toggle="collapse" data-target="#collapse<?=$list->faqid;?>" aria-expanded="true" aria-controls="collapse<?=$list->faqid;?>">
                                    <p class="fs-18 biggest question"><?=$list->question;?>
                                        <span><i class="fa fa-chevron-down"></i></span>
                                    </p>
                                  </button>
                              </div>
                              <div id="collapse<?=$list->faqid;?>" class="collapse" aria-labelledby="heading<?=$list->faqid;?>" data-parent="#accordion">
                                  <div class="card-body" style="font-weight:600;">
                                  <?=$list->answer;?>
                                  </div>
                                </div>
                            </div>
                            </div>

                        <?php endforeach; ?>
                               </div>
                              </div>
                        </div>
                        <div class="col-sm-6">
                            <div id="accordion">
                                  
                              </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="container-fluid custom-fluid-padding">
                   
                    <p class="about-page-text fs-50 text-center biggest pt32 pb-xs-16">
                        In Pictures
                    </p>
                    <div class="row">
                        <div class="col-sm-6 offset-sm-3">
                            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                  <div class="carousel-item active">
                                    <img src="https://via.placeholder.com/600x420" class="d-block w-100" alt="...">
                                    <p class="text-center"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni, tenetur. Error, quasi.</p>
                                  </div>
                                  <div class="carousel-item">
                                    <img src="https://via.placeholder.com/600x420" class="d-block w-100" alt="...">
                                    <p class="text-center"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni, tenetur. Error, quasi.</p>
                                  </div>
                                  <div class="carousel-item">
                                    <img src="https://via.placeholder.com/600x420" class="d-block w-100" alt="...">
                                    <p class="text-center"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni, tenetur. Error, quasi.</p>
                                  </div>
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                  <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                  <span class="sr-only">Next</span>
                                </a>
                              </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="container-fluid custom-fluid-padding">
                 
                    <p class="about-page-text fs-50 text-center biggest pt80 pt-xs-16">
                       Congratulations
                    </p>
                    <p class="fs-36 thinnest text-center">You have successfully toured eCADEMY.</p>
                    <br>
                    <div class="row pb80">
                        <div class="col-sm-4 text-center">
                            <p class="fs-20 thin">
                                If You still need to talk to us
                            </p>
                            <p class="text-center">
                                <a href="" class="fs28 btn-primary text-uppercase">Chat</a>
                            </p>
                        </div>
                        <div class="col-sm-4 text-center">
                            <p class="fs-20 thin">
                                If you want to proceed with enrollment
                            </p>
                            <p class="text-center">
                                <a href="" class="fs28 btn-primary text-uppercase">Enroll</a>
                            </p>
                        </div>
                        <div class="col-sm-4 text-center">
                          <p class="fs-20">
                              Need To Think
                          </p>
                          <p class="text-center">
                              <a href="<?=base_url();?>" class="fs28 btn-primary red-color text-uppercase">Visit Later</a>
                          </p>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<script
src="https://code.jquery.com/jquery-3.5.1.min.js"
integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha512-M5KW3ztuIICmVIhjSqXe01oV2bpe248gOxqmlcYrEzAvws7Pw3z6BK0iGbrwvdrUQUhi3eXgtxp5I8PDo9YfjQ==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw==" crossorigin="anonymous"></script>

<script src="<?=base_url();?>assets/eacademy/js/script.js"></script>
<script>
    $('#full-slider').owlCarousel({
        loop:false,
        navRewind:false,
        margin:20,
        nav:true,
        items:1,
    });
    
</script>
<script type="text/javascript">
  function googleTranslateElementInit() {
      new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.FloatPosition.TOP_LEFT}, 'google_translate_element');
  }
  
  function triggerHtmlEvent(element, eventName) {
      var event;
      if (document.createEvent) {
          event = document.createEvent('HTMLEvents');
          event.initEvent(eventName, true, true);
          element.dispatchEvent(event);
      } else {
          event = document.createEventObject();
          event.eventType = eventName;
          element.fireEvent('on' + event.eventType, event);
      }
  }
  
  jQuery('.langs p span a').click(function(e) {
      e.preventDefault();
      var theLang = jQuery(this).attr('data-lang');
      jQuery('.goog-te-combo').val(theLang);
      
      //alert(jQuery(this).attr('href'));
      window.location = jQuery(this).attr('href');
      location.reload();
      
  });
</script>
</body>
</html>