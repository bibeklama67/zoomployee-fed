
      <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha512-M5KW3ztuIICmVIhjSqXe01oV2bpe248gOxqmlcYrEzAvws7Pw3z6BK0iGbrwvdrUQUhi3eXgtxp5I8PDo9YfjQ==" crossorigin="anonymous"></script>
      <script src="<?=base_url();?>assets/eacademy/js/script.js"></script>
      <script>
       $(document).on("click",".contact",function() {

        // alert();

     $('.contact-modal').modal('show');
   });
   // if(localStorage.getItem('popState') != 'shown'){
   //          popupbehave();
         
   //        }
   //        else if(localStorage.getItem('date') && (localStorage.getItem('date')< '<?=date('Y-m-d');?>'))
   //        {
   //          popupbehave();

   //        }
   //        else if(localStorage.getItem('time') && (localStorage.getItem('time') < '<?=date('H:i', strtotime('-5 minutes'));?>'))
   //        {
   //          popupbehave();

   //        }
   function popupbehave()
          {
            $("#notimodal").modal('show');

      
            localStorage.setItem('popState','shown');
            localStorage.setItem('date','<?=date('Y-m-d');?>');
            localStorage.setItem('time','<?=date('H:i');?>');
          }
   </script>
   <script async src="https://www.googletagmanager.com/gtag/js?id=G-75WT4JF4PV"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-75WT4JF4PV');
</script>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-185154141-1"></script>
<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-185154141-1');
</script>
   <!-- Modal -->
   <div class="modal fade bd-example-modal-lg contact-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
           <div class="modal-content">
              <div class="modal-header" style="padding: 10px; background: #EAEEF1;">
                 <h4 class="modal-title" id="myLargeModalLabel" style="font-weight: bold;color: #418755 !important;">Contact Us</h4>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                 <span aria-hidden="true">×</span>
                 </button>
              </div>
              <div class="modal-body">
               <div class="container-fluid">
               <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-12 fs-14">
                    <h2 style="margin-top: 0px; color:black;" class="fs-24">MiDas Education Pvt. Ltd.</h2>
                    <br>
                    PEA Marg,<br>
                    Thapathali, Kathmandu, Nepal.<br>
                    <a href="http://www.facebook.com/midasecademy" target="_blank" style="    color: #3f8654;">www.facebook.com/MiDaseCADEMY </a><br>
                    <br>
                    <strong>For inquiry:</strong><br>
                    Tel Ph. No: 9847345054, 9847345065<br>
                    Email Address: <a href="mailto:info@midas.com.np" style="    color: #3f8654;">info@midas.com.np</a><br><br>

                    <!-- <strong>For sales inquiry:</strong><br />
                    Tel Ph. No: 9851184862<br />
                    Email Address: <a href="mailto:sales@midas.com.np">sales@midas.com.np</a><br><br /> -->

                    <strong>For technical support:</strong><br>
                    Tel Ph. No: 98492926100<br>
                    Email Address: <a href="mailto:support@midas.com.np" style="    color: #3f8654;">support@midas.com.np</a><br>
                </div>
                <div class="col-lg-6 col-md-6 col-12">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7065.597003155044!2d85.317644!3d27.692622!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb3d496a85fdb0885!2sMiDas%20Technologies%20Pvt.%20Ltd!5e0!3m2!1sen!2snp!4v1599275510144!5m2!1sen!2snp" width="100%" height="300" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
               </div>
               </div>
              </div>
           </div>
        </div>
     </div>
							<!-- Modal Close -->
   </body>
  
</html>