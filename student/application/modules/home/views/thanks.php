
<body style="background-color:#b2d7f9">
    
    <section class="pt120 pb64">
        <div class="container">
            <div class="col-sm-8 offset-sm-2">
                <div class="booking-form bk-alt">
                    <div class="row">
                        <div class="col-6">
                            <a href="<?=base_url();?>"><i class="fa fa-times fs-20 text-dark"></i></a>
                        </div>
                        <div class="col-6 text-right">
                            <p class="fs-20"><span><a href="#" class="text-dark">Nepali</a></span> | <span><a href="#" class="text-dark">English</a></span></p>
                        </div>
                    </div>
                    <br>
                    <div class="modal-con text-center">
                        <p class="fs-36 biggest pt120 pt-xs-32">Thank You For Applying</p>
                        <br>
                        <p>Our team shall get back to you in a while.  Meanwhile,
                            please tour our eCADEMY as it may clear some of your
                            queries and doubts </p>
                            <br>
                                <a href="<?=base_url();?>etour" class="btn-primary">Tour Ecademy</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
   
    
    
    