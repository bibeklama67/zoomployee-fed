<style>
.pt{
    padding-top:94px;
}
.groove{  
     border-style: groove;
    border-color: #b1fff4c7;
    padding: 7px;}
   
.formobile{display:none;}
@media only screen 
  and (min-device-width : 320px) 
  and (max-device-width : 480px) {
    .pt{
    padding-top:34px;
}
      .formobile{display:block;}
      .appbutton{display:none;}
  }
</style>
   <body>
      <div class="container-fluid">
         <nav class="navbar navbar-expand-lg navbar-light bg-white">
            <a class="navbar-brand" href="<?=base_url();?>">
            <img src="<?=base_url();?>assets/eacademy/images/logo.png" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
               <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
               <li class="nav-item ">
                     <a class="nav-link" href="<?=base_url();?>">Home</a>
                  </li>
                 
                  <?php if($applicationshow=='Y'):
                         if($id=='0')
                         {
                            $id='';
                         }
                     ?>
                    <li class="nav-item ">
                     <a class="nav-link" href="<?=base_url();?>applicationform<?=$id?>">Application Form</a>
                  </li>
                  <?php endif;?>
                  <li class="nav-item ">
                     <a class="nav-link contact" href="javascript:void(0)" >Contact Us</a>
                  </li>
               
                 
               </ul>
            </div>
         </nav>
      </div>
      <section class="banner">
         <div class="container-fluid pt">
         <?php if($applicationshow=='Y'):?>

            <a class="nav-link button formobile" href="<?=base_url();?>applicationform<?=$id?>" style="width: 46%;padding: 6px 10px!important;margin-top: 7px;margin-left:25%;text-align: center;">Apply Now</a>
            <?php endif;?>
            <div class="row appbutton">

               <div class="col-lg-6 col-md-6 col-sm-6 order-sm-1" style="margin-left: 30px;">
                
               <?php foreach($faq as $key=> $list): 
               if($key==0)
               $grooveclass=" groove";
               else
               $grooveclass='';
                     
                  ?>
               <a href="javascript:void(0)" class="faqchange <?=$grooveclass;?>" data-id="<?=$list->faqid;?>" id="faq<?=$list->faqid;?>" data-vlink="<?=$list->videolink;?>" style="color:black;line-height:2"><?=($list->question_nep!='')?$list->question_nep:$list->question;?></a><br/>
               <?php endforeach;?>
                   
               </div>
               <div class="col-lg-5 col-md-5 col-sm-5 order-sm-2">
               <span class="iframe">
               <iframe id="myiframe" width="580" height="350" style="width:100%"
                src="<?=$faq[0]->videolink;?>">
                </iframe>
               </span>
               <?php if($applicationshow=='Y'):?>

               <a class="nav-link button appbutton" href="<?=base_url();?>applicationform<?=$id?>" style="margin-left: 42%;padding: 6px 10px!important;margin-top: 7px;">Apply Now</a>
               <?php endif;?>
             </div>
            </div>
            
         </div>
      </section>
      <div id="accordion">

      <?php foreach($faq as $key=> $list): ?>
                                <div class="col-sm-6 formobile">

                            <div class="card">
                              <div class="card-header" id="heading<?=$list->faqid;?>">
                                  <button class="btn btn-link fs-20" id="btnquestion<?=$key;?>" data-toggle="collapse" data-target="#collapse<?=$list->faqid;?>" aria-expanded="true" aria-controls="collapse<?=$list->faqid;?>">
                                    <p id="question<?=$key;?>" class="fs-18 biggest question"><?=($list->question_nep!='')?$list->question_nep:$list->question;?>
                                    </p>
                                  </button>
                              </div>
                              <div id="collapse<?=$list->faqid;?>" class="collapse" aria-labelledby="heading<?=$list->faqid;?>" data-parent="#accordion">
                                  <div class="card-body" style="font-weight:600;">
                                  <iframe  width="580" height="350" style="width:100%"
                                    src="<?=$list->videolink;?>">
                                    </iframe>
                                  </div>
                                </div>
                            </div>
                            </div>

                        <?php endforeach; ?>
                        </div>
      <script>
  $(document).off('click','.faqchange');
     $(document).on('click','.faqchange',function(){
        $('.faqchange').removeClass('groove');
        $(this).addClass('groove');
      var vlink=$(this).data('vlink');
      var html='<iframe id="myiframe" width="580" height="350" src="'+vlink+'" style="width:100%"></iframe>';
      $('.iframe').html(html);
  })
  $(document).off('click','.question');
     $(document).on('click','.question',function(){
      $('.question').removeClass('groove');
        $(this).addClass('groove');
   })
   $(document).ready(function(){
      $('#question0').addClass('groove');

      $('#btnquestion0').trigger( "click" );
   })
  </script>