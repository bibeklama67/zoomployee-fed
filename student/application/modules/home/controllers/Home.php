<?php
(defined('BASEPATH')) or exit('No direct script access allowed');
class Home extends MX_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->library('curl');
    $this->load->model('Home_model', 'model');
    $this->apiurl = $this->config->item('api_url') . 'api/';
    $this->orgid = '10044344';
    //10044891

  }
  function index()
  {
    //echo base_url(); exit;
    redirect(base_url("/login"));

    $faq = $this->model->getfaq();
    $this->load->view('inc/header');

    $this->load->view('index', array('faq' => $faq));
    $this->load->view('inc/footer');
  }
  function apply()
  {
    $data = array();

    $districtdata = $this->commoncurl('setup/setup/student_register/getDistrict', array());
    if ($districtdata->type == 'success') {
      $data['district'] = $districtdata->response;
    }

    $this->session->set_userdata('userid', '10000052');
    $eclassdata = $this->commoncurl('elearning/teacherend/studentApi/geteclass', array('orgid' => $this->orgid));
    //var_Dump($eclassdata);exit;
    if ($eclassdata->type == 'success') {
      $data['eclass'] = $eclassdata->response;
    }

    $this->load->view('inc/header');
    $this->load->view('apply', $data);
    $this->load->view('inc/footer');
  }
  function application($id)
  {
    $data = array();
    $data['from'] = $id;

    $districtdata = $this->commoncurl('setup/setup/student_register/getDistrict', array());
    if ($districtdata->type == 'success') {
      $data['district'] = $districtdata->response;
    }
    $this->session->set_userdata('userid', '1000000252');
    $this->session->set_userdata('childid', '1000000252');

    $eclassdata = $this->commoncurl('elearning/teacherend/studentApi/geteclass', array('orgid' => $this->orgid));
    //var_Dump($this->session);exit;
    if ($eclassdata->type == 'success') {
      // $data['eclass']=$eclassdata->response;
      $sec = array();
      $bbs = array();
      $newbbs = array();
      $mbs = array();
      $cmat = array();
     // print_r($eclassdata->response );

      foreach ($eclassdata->response as $value) {
        if ($value->eid <= 39){
          if($value->eid !=38){
            $other[$value->eid] =  $value->classname;
          }
         
        }
        // if ($value->eid > 39) {
          if ($value->eid > 39 && $value->eid <= 53) {
            $sec[$value->eid] =  $value->classname;
          }
          
          if ($value->eid > 121 && $value->eid <= 125) {
            $bbs[$value->eid] = $value->classname;
          }
          ksort($bbs);
          if($value->eid == 126){
            $cmat[$value->eid] = $value->classname;
          }

          if ($value->eid == 116) {
            $tempbbs[$value->eid] = $value->classname;

           
          }

          if ($value->eid >= 118 && $value->eid <= 121) {
            $mbs[$value->eid] = $value->classname;
          }

          ksort($mbs);
        // }
      }
      ksort($bbs);
      $newbbs = array_slice($bbs, 0, 2, true) +
      $tempbbs +
      array_slice($bbs, 2, NULL, true);
      $completeclss = $other + $sec +$cmat+ $newbbs + $mbs;
     $data['eclass'] = $completeclss;
    //$data['eclass']=$eclassdata->response;
    }

    $this->load->view('inc/header');
    $this->load->view('application', $data);
    $this->load->view('inc/footer');
  }
  function applicants()
  {
    $data['frmDate'] = date('Y-m-d');
    $data['toDate'] =  date('Y-m-d');
    if ($this->input->server('REQUEST_METHOD') == 'POST') {
      $data['frmDate']= $this->input->post('frmDate');
      $data['toDate'] = $this->input->post('toDate');
  }
  

    $data['list'] = $this->model->getapplicantsdata();
    $data['staff'] = $this->model->gettelemarketerlist();
    $this->load->view('inc/header');
    $this->load->view('applicantlist', $data);
    $this->load->view('inc/footer');
  }
  function faqtour__($id)
  {
    $data['faq'] = $this->model->getfaq();
    $data['id'] = $id;
    $data['applicationshow'] = 'Y';

    if (isset($_GET['d']) && @$_GET['d'] == 'open') {
      $data['applicationshow'] = 'N';
    }
    $this->load->view('inc/header');
    $this->load->view('faqtour', $data);
    $this->load->view('inc/footer');
  }
  function faqtour($id)
  {
    $data['faq'] = $this->model->getfaq();
    $data['id'] = $id;
    $data['applicationshow'] = 'Y';

    if (isset($_GET['d']) && @$_GET['d'] == 'open') {
      $data['applicationshow'] = 'N';
    }
    $this->load->view('inc/header');
    $this->load->view('faqtour_new', $data);
    $this->load->view('inc/footer');
  }
  function joinconference()
  {
    $data['faq'] = $this->model->getfaq();
    $data['id'] = 0;
    $data['applicationshow'] = 'Y';

    if (isset($_GET['d']) && @$_GET['d'] == 'open') {
      $data['applicationshow'] = 'N';
    }
    $this->load->view('inc/header');
    $this->load->view('joinconferenceview', $data);
    $this->load->view('inc/footer');
  }

  function bookform()
  {
    $data = array();

    $districtdata = $this->commoncurl('setup/setup/student_register/getDistrict', array());
    if ($districtdata->type == 'success') {
      $data['district'] = $districtdata->response;
    }

    $this->load->view('inc/header');
    $this->load->view('bookform', $data);
    $this->load->view('inc/footer');
  }
  function thanks()
  {

    $this->load->view('inc/header');
    $this->load->view('thanks');
    $this->load->view('inc/footer');
  }

  function etour()
  {
    $faq = $this->model->getfaq();


    $this->load->view('inc/header');
    $this->load->view('etour', array('faq' => $faq));
    // $this->load->view('inc/footer');

  }
  function getvdc()
  {
    $data = array();

    $vdcdata = $this->commoncurl('setup/setup/student_register/getVdc', $_POST);

    if ($vdcdata->type == 'success') {
      $vdc = $vdcdata->response;
      echo json_encode(array('type' => 'success', 'message' => 'VDC', 'response' => $vdc));
    } else {
      echo json_encode(array('type' => 'error', 'message' => 'No any Municipality List', 'response' => array()));
    }
  }
  function getsubject()
  {
    $data = array();
    $sdata = $this->commoncurl('elearning/liveclass/getLiveSubjects', array('Orgid' => $this->orgid, 'Classid' => $_POST['eclassid'], 'Source' => 'HH', 'Type' => 'Active', 'isdemo' => ''));
    if ($sdata->type == 'success') {
      //var_dump($sdata->response->subject);exit;

      $subject = $sdata->response->subject;
      echo json_encode(array('type' => 'success', 'message' => 'SUBJECT LIST', 'response' => $subject));
    } else {
      echo json_encode(array('type' => 'error', 'message' => 'No any SUBJECT List', 'response' => array()));
    }
  }
  function submitappointment()
  {
    $this->form_validation->set_rules('fullname', 'Full Name', 'required');
    $this->form_validation->set_rules('email', 'Email', 'required');
    $this->form_validation->set_rules('phone', 'Phone', 'required');
    $this->form_validation->set_rules('classname', 'Class name', 'required');
    $this->form_validation->set_rules('school', 'School Name', 'required');
    $this->form_validation->set_rules('district', 'District', 'required');
    $this->form_validation->set_rules('vdc', 'Municipality', 'required');
    $this->form_validation->set_rules('address', 'Address', 'required');
    if ($this->form_validation->run() == FALSE) {
      $res = ["message" => validation_errors(), "type" => 'error'];

      echo json_encode($res);
      exit;
    }

    $post = $_POST;
    if ($post['district'] == '-1') {
      $res = ["message" => 'Please Select District', "type" => 'error'];

      echo json_encode($res);
      exit;
    }
    if ($post['vdc'] == '-1') {
      $res = ["message" => 'Please Select Municipality', "type" => 'error'];

      echo json_encode($res);
      exit;
    }
    $data = array(
      'fullname' => $post['fullname'],
      'mobileno' => $post['phone'],
      'classname' => $post['classname'],
      'schoolname' => $post['schoolname'],
      'districtid' => $post['district'],
      'municipalityid' => $post['vdc'],
      'address' => $post['address'],
      'datapostdatetime' => date('Y-m-d H:i:s')
    );
    if ($this->model->savedata('myappointment', $data) > 0)
      echo json_encode(array('type' => 'success', 'message' => 'Succesfuly Inserted'));
    else
      echo json_encode(array('type' => 'error', 'message' => 'Something Went Wrong!'));
  }
  function submitfaq()
  {
    $this->form_validation->set_rules('name', 'Full Name', 'required');
    $this->form_validation->set_rules('email', 'Email', 'required');
    $this->form_validation->set_rules('mobile', 'Phone', 'required');
    $this->form_validation->set_rules('ques', 'Question', 'required');

    if ($this->form_validation->run() == FALSE) {
      $res = ["message" => validation_errors(), "type" => 'error'];

      echo json_encode($res);
      exit;
    }

    $post = $_POST;

    $data = array(
      'fullname' => $post['name'],
      'mobileno' => $post['mobile'],
      'email' => $post['email'],
      'question' => $post['ques'],
      'datapostdatetime' => date('Y-m-d H:i:s')
    );
    if ($this->model->savedata('faqquestion', $data) > 0)
      echo json_encode(array('type' => 'success', 'message' => 'Your Queries has been Submitted. Thank You !'));
    else
      echo json_encode(array('type' => 'error', 'message' => 'Something Went Wrong!'));
  }
  function submitrefer()
  {
    $this->form_validation->set_rules('studentname', 'Student Name', 'required');
    $this->form_validation->set_rules('studentemail', 'Email', 'required');
    $this->form_validation->set_rules('studentphone', 'Phone', 'required');

    if ($this->form_validation->run() == FALSE) {
      $res = ["message" => validation_errors(), "type" => 'error'];

      echo json_encode($res);
      exit;
    }

    $post = $_POST;

    $data = array(
      'studentname' => $post['studentname'],
      'studentmobileno' => $post['studentphone'],
      'studentemail' => $post['studentemail'],
      'datapostdatetime' => date('Y-m-d H:i:s')
    );
    if ($this->model->savedata('refer', $data) > 0)
      echo json_encode(array('type' => 'success', 'message' => 'Your Name has been Submitted. Thank You !'));
    else
      echo json_encode(array('type' => 'error', 'message' => 'Something Went Wrong!'));
  }

  function submitapplication()
  {
    $this->form_validation->set_rules('fname', 'First Name', 'required');
    $this->form_validation->set_rules('lname', 'Last Name', 'required');
    $this->form_validation->set_rules('email', 'Email', 'required');
    $this->form_validation->set_rules('phone', 'Phone', 'required');
    $this->form_validation->set_rules('school', 'School Name', 'required');
    $this->form_validation->set_rules('district', 'District', 'required');
    $this->form_validation->set_rules('vdc', 'Municipality', 'required');
    $this->form_validation->set_rules('address', 'Address', 'required');
    $this->form_validation->set_rules('eclass', 'Class', 'required');

    if ($this->form_validation->run() == FALSE) {
      $res = ["message" => validation_errors(), "type" => 'error'];

      echo json_encode($res);
      exit;
    }

    $post = $_POST;

    $data = array(
      'studentfirstname' => $post['fname'],
      'studentlastname' => $post['lname'],
      'studentmobileno' => (int)$post['phone'],
      'studentemail' => $post['email'],
      'schoolname' => $post['school'],
      'studentdistrictid' => $post['district'],
      'studentvdcid' => $post['vdc'],
      'studentaddress' => $post['address'],
      'classid' => $post['eclass'],
      'subjectlist' => implode(',', $post['subject']),

      'datapostdatetime' => date('Y-m-d H:i:s')
    );
    $save_id = $this->model->savedata('applyform', $data);
    if ($save_id > 0)
      echo json_encode(array('type' => 'success', 'message' => 'Success', 'id' => $save_id));
    else
      echo json_encode(array('type' => 'error', 'message' => 'Something Went Wrong!'));
  }
  function submitapplication_guardian($id)
  {
    $this->form_validation->set_rules('fname', 'First Name', 'required');
    $this->form_validation->set_rules('lname', 'Last Name', 'required');
    $this->form_validation->set_rules('email', 'Email', 'required');
    $this->form_validation->set_rules('phone', 'Phone', 'required');

    $this->form_validation->set_rules('district', 'District', 'required');
    $this->form_validation->set_rules('vdc', 'Municipality', 'required');
    $this->form_validation->set_rules('address', 'Address', 'required');

    if ($this->form_validation->run() == FALSE) {
      $res = ["message" => validation_errors(), "type" => 'error'];

      echo json_encode($res);
      exit;
    }

    $post = $_POST;

    $data = array(
      'guardianfirstname' => $post['fname'],
      'guardianlastname' => $post['lname'],
      'guardianmobileno' => $post['phone'],
      'guardianemail' => $post['email'],
      'guardiandistrictid' => $post['district'],
      'guardianvdcid' => $post['vdc'],
      'guardianaddress' => $post['address'],

      'iscomplete' => 'Y',
      'datapostdatetime' => date('Y-m-d H:i:s')
    );
    if ($this->model->updatedata('applyform', $data, array('applicationid' => $id)) > 0)
      echo json_encode(array('type' => 'success', 'message' => 'Your application has been submitted. Thank you!'));
    else
      echo json_encode(array('type' => 'error', 'message' => 'Something Went Wrong!'));
  }

  function submitdirectapplication()
  {
    $this->form_validation->set_rules('fname', 'First Name', 'required');
    $this->form_validation->set_rules('lname', 'Last Name', 'required');
    $this->form_validation->set_rules('pfname', 'Parent First Name', 'required');
    $this->form_validation->set_rules('plname', 'Parent Last Name', 'required');
    $this->form_validation->set_rules('email', 'Email', 'required');
    $this->form_validation->set_rules('phone', 'Phone', 'required');
    // $this->form_validation->set_rules('studentphone', 'Phone', 'required');
    $this->form_validation->set_rules('school', 'School Name', 'required');
    $this->form_validation->set_rules('district', 'District', 'required');
    $this->form_validation->set_rules('vdc', 'Municipality', 'required');
    $this->form_validation->set_rules('address', 'Address', 'required');
    $this->form_validation->set_rules('medium', 'Medium', 'required');
    $this->form_validation->set_rules('eclass', 'Class', 'required');
    $this->form_validation->set_rules('subject[]', 'Subject', 'required');

    if ($this->form_validation->run() == FALSE) {
      $res = ["message" => validation_errors(), "type" => 'error'];

      echo json_encode($res);
      exit;
    }

    $post = $_POST;

    $data = array(
      'studentfirstname' => $post['fname'],
      'studentlastname' => $post['lname'],
      'studentmobileno' => (@$post['studentphone'] != '') ? (int)$post['studentphone'] : 0,
      'studentemail' => $post['email'],
      'schoolname' => $post['school'],
      'studentdistrictid' => $post['district'],
      'studentvdcid' => $post['vdc'],
      'studentaddress' => $post['address'],
      'classid' => $post['eclass'],
      'schoolmedium' => $post['medium'],
      'subjectlist' => implode(',', $post['subject']),
      'guardianfirstname' => $post['pfname'],
      'guardianlastname' => $post['plname'],
      'guardianmobileno' => $post['phone'],
      'guardianemail' => $post['email'],
      'guardiandistrictid' => $post['district'],
      'guardianvdcid' => $post['vdc'],
      'guardianaddress' => $post['address'],
      'from' => $post['from'],
      'iscomplete' => 'Y',

      'datapostdatetime' => date('Y-m-d H:i:s')
    );

    $checkduplicate = $this->model->checkduplicatedata();

    if ($checkduplicate === true) {
      echo json_encode(array('type' => 'success', 'message' => 'Success', 'id' => 101));
      exit;
    } else {
      $save_id = $this->model->savedata('applyform', $data);
      if ($save_id > 0)
        echo json_encode(array('type' => 'success', 'message' => 'Success', 'id' => $save_id));
      else
        echo json_encode(array('type' => 'error', 'message' => 'Something Went Wrong!'));
    }
  }
  function submitcallstatus()
  {
    //$this->form_validation->set_rules('appid', 'Application Id', 'required');
    $this->form_validation->set_rules('callby', 'Call By', 'required');
    $this->form_validation->set_rules('callstatus', 'Call Status', 'required');
    $this->form_validation->set_rules('clientstatus', 'Cient Status', 'required');
    $this->form_validation->set_rules('remarks', 'remarks', 'required');


    if ($this->form_validation->run() == FALSE) {
      $res = ["message" => validation_errors(), "type" => 'error'];

      echo json_encode($res);
      exit;
    }

    $post = $_POST;
    $staffdetail = explode("#", $post['callby']);
    $insert = array(
      'callerid' => $staffdetail[0],
      'callername' => $staffdetail[1],
      'callstatus' => $post['callstatus'],
      'clientstatus' => $post['clientstatus'],
      'remarks' => $post['remarks'],
      'applyformid' => @$post['appid']
    );
    if ((int)$post['appid'] > 0) {
      $save_id = $this->model->savedata('applyformcallstatus', $insert);
      if ($save_id > 0)
        echo json_encode(array('type' => 'success', 'message' => 'Success'));
      else
        echo json_encode(array('type' => 'error', 'message' => 'Something Went Wrong!'));
    } else {
      unset($insert['applyformid']);
      $save_id = $this->model->updatedata('applyformcallstatus', $insert, array('callstatusid' => $post['callstatusid']));
      if ($save_id > 0)
        echo json_encode(array('type' => 'success', 'message' => 'Success'));
      else
        echo json_encode(array('type' => 'error', 'message' => 'Something Went Wrong!'));
    }
  }
  function getcallstatus()
  {
    $data = $this->model->getqueuestatus($_POST['id']);
    if (count($data) > 0)
      echo json_encode(array('type' => 'success', 'message' => 'Data is present'));
    else {
      $this->model->submitformqueue($_POST['id']);
      echo json_encode(array('type' => 'error', 'message' => 'No any data'));
    }
    $this->model->releaseformqueue();
  }

  function commoncurl__($url, $post = false)
  {
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $this->apiurl . $url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => false,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => $post,
      CURLOPT_HTTPHEADER => array(
        "Apikey: 0cfbd2e2053e0d7eee49e642552cc94e",
        "Content-Type: application/json"
      ),
    ));

    $response = curl_exec($curl);
    var_dump($post);
    var_dump($response);
    exit();
    $err = curl_error($curl);
    curl_close($curl);

    return json_decode($response);
  }
  function commoncurl($url, $req_data)
  {

    $data = $this->curl->post_data($this->apiurl . $url, $req_data);
    $data = json_decode($data);

    return $data;
  }
  
  
  function surveyform($formno)
  {
	  if($formno == 1)
	  {
	     $link ="https://docs.google.com/forms/d/e/1FAIpQLSd98HkE_V9OMh5LW8R28QSKaDo5wKiPa-FDk3dQRuitub67Mw/viewform";
	  }
	  else if($formno == 2)
	  {
		  $link = "https://docs.google.com/forms/d/e/1FAIpQLSf-bBdr19--7nak_taE6FpX9UKnOnP75ESl6xE5zN0yGf-Esg/viewform";
	  }else if($formno == 3)
	  {
		  $link = "https://docs.google.com/forms/d/e/1FAIpQLSd1nV7bu2OvUgIjyFhwJ8zCbc3Wko_2TogtcBv7MuiV1yEohw/viewform?usp=sf_link";
	  }else if($formno == 4)
	  {
		  $link = "https://forms.gle/zP7tJ2gzmtP6cjKBA";
	  }else if($formno == 5)
	  {
		  $link = "https://forms.gle/UMb9yFSt5ANMJCu56";
	  }else if($formno == 6)
	  {
		  $link = "https://docs.google.com/forms/d/1DW6_ndEPCXAWtmaMpqtuhHrVq5d-aL2oiwscIncNqJE/edit?ts=604a0ab1&gxids=7628";
	  }else if($formno == 7)
	  {
		  $link = "https://docs.google.com/forms/d/e/1FAIpQLSdFyEcwq80_7TuOfjSsLwMsibW_wvg6wcLz_Q_zIqw7hq6JMA/viewform?usp=sf_link";
	  }else if($formno == 8){
		  $link = "https://play.google.com/store/apps/details?id=com.midas.eclassnew";
	  }
	  
	  $this->load->view('survey/surveyform1',array('link'=>$link));
  }
  
}
