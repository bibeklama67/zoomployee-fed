
<?=doctype('html')?>

<html dir="ltr" lang="en-US" xmlns="http://www.w3.org/1999/xhtml"
      xmlns:fb="http://ogp.me/ns/fb#">

<head>
<title>कक्षा १० को परिक्षा (SEE) को तयारिको लागि LIVE Learning Aid</title>
<script>
  var base_url= "<?= base_url();?>";
</script>
<!--<meta property="og:image" content="http://www.midas.com.np/assets/images/blog-img/MidasLive.jpg" />-->
<?php 
  $this->config->load('site_setting');
    $default=$this->config->item('default');
    $default_items=$this->config->item($default);
// print_r($default_items);
    $title=element('title',$default_items);

?>
   <style>
	     @import url(https://fonts.googleapis.com/css?family=Oswald);
	   @import url(https://fonts.googleapis.com/css?family=Ek+Mukta:400,600,700);
  .head_main_wrap{ background:#fff;}
	   img#fbsharebtn{
		      margin-top: 17px;
    margin-right: 15px;
	   }
  

	 
</style>
<?php
      echo meta('content-type','text/html; charset=utf-8','equiv');
        $common_items=$this->config->item('common');
   
        $common_meta=element('meta',$common_items);
        $default_meta=element('meta',$default_items);
        if($common_meta && $default_meta)
          $meta=array_merge($common_meta,$default_meta);        
        else if($common_meta)
          $meta=$common_meta;
        else
          $meta=$default_meta;
          
        if(isset($meta) && is_array($meta))       
            foreach($meta as $k=>$v)          
              echo meta($k,$v); 
    ?>
<link rel="icon" type="image/png" href="http://midas.com.np/images/favicon.ico"> 
 <?php
        $common_css=element('css',$common_items);
        $default_css=element('css',$default_items);

  
        if(isset($common_css) && is_array($common_css))
        {
          $fold=element('folder',$common_css);
          if($fold)

          foreach($fold as $f)
          foreach (glob("assets/css/$f/*.css") as $filename)
          {
            // $filename=ltrim($filename,"assets/");

            $link_arr = array('href'=>$filename,
                'rel' =>'stylesheet',
                'type'=>'text/css'
              );

            echo link_tag($link_arr); 
          }
            
          unset($common_css['folder']); 
                
          foreach($common_css as $c):
            $link_arr = array('href'=>"assets/css/common/$c.css",
              'rel' =>'stylesheet',
              'type'=>'text/css'
            );
            echo link_tag($link_arr);
          endforeach;
          
        }
        
        unset($link_arr); unset($common_css); 
        
        if(isset($default_css) && is_array($default_css))
        {
     
          $fold=element('folder',$default_css);
          if($fold)
          foreach($fold as $f)
          foreach (glob("assets/css/$f/*.css") as $filename)
          {

            // $filename=ltrim($filename,"assets/");
           
            $link_arr = array('href'=>$filename,
                'rel' =>'stylesheet',
                'type'=>'text/css'
              );
            echo link_tag($link_arr); 
          }
          unset($default_css['folder']);
                

          foreach($default_css as $c):
            $link_arr = array('href'=>"assets/css/$default/$c.css",
              'rel' =>'stylesheet',
              'type'=>'text/css'
            );
            echo link_tag($link_arr);
          endforeach;
        }
          unset($link_arr); unset($default_css);                    
    ?>  
 <?php 
        $common_js=element('js',$common_items);
        $default_js=element('js',$default_items);
                
        if(isset($common_js) && is_array($common_js)):
        
          $fold=element('folder',$common_js);
          if($fold):
            foreach($fold as $f):
              foreach (glob("assets/js/$f/*.js") as $filename):
                // $filename=ltrim($filename,"assets/");
                $j_arr = array('type'=>'text/javascript',
                         'src' =>$filename);
                echo script_tag($j_arr);
              endforeach;
            endforeach;
          endif;
          
          unset($common_js['folder']);
          
          foreach($common_js as $j):
            $j_arr = array('type'=>'text/javascript',
                     'src' =>"assets/js/common/$j.js");
            echo script_tag($j_arr);
          endforeach;
          
        endif;
        
        unset($j_arr);unset($common_js);
        
        if(isset($default_js) && is_array($default_js)):
        
          $fold=element('folder',$default_js);
          if($fold):
            foreach($fold as $f):
              foreach (glob("assets/js/$f/*.js") as $filename):
                // $filename=ltrim($filename,'assets/');
                $j_arr = array('type'=>'text/javascript',
                         'src' =>$filename);
                echo script_tag($j_arr);
              endforeach;
            endforeach;
          endif;
          
          unset($default_js['folder']);
                
          foreach($default_js as $j):
            $j_arr = array('type'=>'text/javascript',
                     'src' =>"assets/js/$default/$j.js");
            echo script_tag($j_arr);
          endforeach;
        
        endif;
        unset($j_arr);unset($default_js);
    ?>  
  </head>
<body id="blogbody">
	<div class="head_main_wrap">
		  <div class="container" style="padding:10px 0;">
			<div class="col-lg-6 col-md-4 col-sm-3 col-xs-4" style=" margin: -9px; margin-left: -1px; padding-left: 7px;  ">
			<a href="http://midas.com.np/">
			  <img src="http://www.midas.com.np/assets/images/logo_transparent.png" class="img-responsive" style="padding-bottom: 0px;"></a>
			</div>
			<div class="col-lg-6 col-md-8 col-sm-9 col-xs-8" style="right: 10px; top:0px;">
			<i class="pull-right" style="color:#f27b06; padding:15px 0px; text-align: center; font-size: 20px"> Learn Whenever Wherever</i>
			</div>
		</div>
	</div> 

	<div class="container " style="padding: 0px 5px;  margin-bottom: 15px; ">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pad-fix sec-block-bg give-border">
					<div class="wrap-content row1">
						<div class="wrap-arc-l">
										<div class="wrap-heading" >
											<h1 class="heading-l">
											
													कक्षा १० को परिक्षा (SEE) को तयारिको लागि LIVE Learning Aid
													
											</h1>
										</div>
										<hr class="border-btm">
										<div class="misc">
                    
											<div class="pull-left" style="padding: 7px 5px;">
                          <div class="wrap4border">
                              <p class="arc-src"> MiDas Education</p>
                        <p class="arc-date"> Date : Sunday, October 08, 2017</p>
                          </div>												
											</div>
											<div class="pull-right mar-ryt15">		
<!--											<a href="javascript:void(0)" id="share_button" onclick="javascript:genericSocialShare('http://www.facebook.com/sharer.php?u=http://www.midas.com.np/design/blogdetail')"><img src="<?= base_url()?>assets/images/fb-sharebutton.png" width="45" height="45" class="img-responsive" id="fbsharebtn"></a>-->

												 <!-- Load Facebook SDK for JavaScript -->
         <div id="fb-root"></div>
         <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
        <!-- Your share button code -->
        <?php $page_url = base_url()."design/".$this->uri->segment(2)?>
        <div class="fb-share-button" 
        data-href="<?php echo $page_url?>" 
        data-layout="button">
      </div>
											</div>
											<script type="text/javascript">
												 function genericSocialShare(url){
												 window.open(url,'sharer','toolbar=0,status=0,width=648,height=395');
												 return true;
												}
											</script>
											<script>
											$('#share_button').bind('click', function(e){
        e.preventDefault();
//        var title = 'Title I want';
//        var im_url = 'url_to_image';
//        var facebook_appID = 'YourFacebookAppID'
//        url = "https://www.facebook.com/dialog/feed?app_id="+ facebook_appID +    "&link=" + encodeURIComponent("http://www.THEPAGE.com")+ 
//                    "&name=" + encodeURIComponent(title) + 
//                    "&caption=" + encodeURIComponent('Shared from MY_PAGE') + 
//                    "&description=" + encodeURIComponent('DESCRIPTION') + 
//                    "&picture=" + encodeURIComponent("http://www.THEPAGE.com" +im_url) +
//                    "&redirect_uri=https://www.facebook.com";
//        window.open(url);
//    });
											
											</script>
										</div>
										
										<div class="clearfix"></div>
										<hr class="">
										<div class="wrap-desc">
											<p>  <img src="http://www.midas.com.np/assets/images/blog-img/MidasLive.jpg" class="img-responsive" width="" height="" style="padding-bottom: 0px; float: left;margin-right: 15px; margin-top: 5px;">
											
											Science, Maths, Opt. Maths, English, नेपाली<br>
											LIVE @ 7 PM to 9 PM [ Sunday to Friday]<br>


											Classes: कार्तिक देखि फाल्गुन सम्म

											
												<br></p>
												<hr>
											
											<p>
												
												
											<strong>MiDas LIVE Online Coaching Class का फाइदाहरू </strong>	<br>

1. जाँचको तयारीको Special Videos<br>

अनुभबी शिक्षकहरूले SEE को तयारी गर्न चहाने विध्यार्थीहरूको लागि हरेक विषयका भिडियोहरू तयार गर्नुभएको छ। ती भिडियोहरू विध्यार्थीहरूले जहाँबाट, जहिले पनि, जति पटक पनि हेर्न सक्नुहुन्छ।<br><br>


२. आफुलाई जे आउदैन तेहि सिक्नुहोस<br>

हरेक विषयका अनुभबी शिक्षकहरू बेलुका ७ देखि ९ बजे सम्म [ Sunday to Friday] विध्यार्थीहरूलाई जाँचको तयारी गराउन उपलब्ध हुनुहुन्छ। विध्यार्थीहरूले कुनै पनि समयमा आफुले नजानेको वा नबुझेको प्रश्न सोध्न सक्नुहुन्छ। शिक्षकहरूले ति प्रश्नहरू MiDas LIVE मार्फत विध्यार्थीहरूको सामु बुझाएर गरिदिनुहुन्छ। शिक्षकले विध्यार्थीलाई बुझाउनु भएको भिडियो रेकर्ड गरेर पनि रखिएको हुन्छ जसले गर्दा, विध्यार्थीहरूले ति भिडियोहरू दोहोर्याएर हेर्न सक्नुहुन्छ।<br><br>

3. विध्यार्थीहरूको सिक्न सक्ने क्षमता अनुसार सिकाइने<br>

सबै विध्यार्थीहरूको सिक्न सक्ने क्षमता एउटै हुदैन। सबै विध्यार्थीहरूलाई एउटै तरिकाले सिकाउन मिल्दैन। MiDas LIVE मा शिक्षकहरूले विध्यार्थीहरूको क्षमता हेरिकन छुट्टा छुट्टै तरिकाले सिकाउनुहुन्छ।<br><br>

4. अरू विध्यार्थीहरूको प्रश्नबाट पनि सिक्न सकिने<br>

अरू विध्यार्थीहरूले शिक्षकहरूसंग सिकेको भिडियोहरू हेरेर पनि सिक्न सकिन्छ।<br><br>

5. समय र पैसाको बचत<br>

घरमा नै बसेर आफुले चहाएको बेला, आफुले नजानेको कुर सिक्न पाइने हुनाल समयको बचत हुन्छ। साथै यसरी पढ्दा खर्च पनि कम लाग्छ ।<br><br>

</p>
											<p style="
    padding: 0px 15px;
    background: rgba(255, 165, 0, 0.27);
    border: 1px solid orange;
">
												Rs. 500.00 per month [1 Subject]<br>
Rs. 1, 500.00 for 5 months [1 Subject]<br>

Rs. 1, 000.00 per month [3 Subjects]<br> 
Rs. 1, 500.00 per month [5 Subjects]<br>

Rs. 5, 000.00 for 5 months [5 Subjects]

											</p>


<p>
LIVE Learning Aid मार्फत सिक्न Internet को आवश्यक पर्छ।<br><br>

थप जानकारिको लागि:<br>
Ph: 9851228009, 9851170896
											</p>


									</p>
										</div>
																			
									</div>
						
					</div>
					<div class="clearfix"></div>					
				</div>
				
	</div>
	<footer>
	<div class="foot-wrap " id="foot-wraper">
		<div class="container">
			
			<div class="row">
				<div class="col-md-5 col-sm-6 ">
					<div class="copy-right ">
					  <p>© 2017 Midas Education Pvt. ltd. </p>
						<ul class="clearfix" style="list-style-type: none; padding-left: 0px;">
							<li>
								<a href="#" data-toggle="modal" data-target="#myModal1">Term of use</a></li>
								<!-- Modal1 -->
								<div id="myModal1" class="modal fade" role="dialog">
									<div class="modal-dialog">

										<!-- Modal content-->
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">×</button>
												<h3 class="modal-title bread">Terms of use</h3>
											</div>
											<div class="modal-body custmodal">
												<section class="terma">

													<article id="main_article1">
														<p class="text-justify">Welcome to MiDas App - For Parents. MiDas App - For Parents is a communicating tool that helps Parents communicating with their children's teachers, school admins, view activity of their children's and many more. We're glad you're here, but there are some rules you need to agree to before you use our services. 

															These Terms of Service (the "Terms") are a binding contract between you and MiDas App - For Parents. You using the Services in any way means that you agree to all of these Terms, and these Terms will remain in effect while you use the Services.
														</p>
														<p>
															Our Services are constantly changing, to keep up with the dynamic needs of users everywhere - so, these Terms might need to change, too. If they do change, we will do our best to tell you in advance by placing a notification on the MiDas App - For Parents. In certain situations (for example, where a change to the Terms is necessary to comply with legal requirements), we may not be able to give you advance notice. MiDas App - For Parents takes the privacy of its users very seriously.
															What are the basics of using MiDas App - For Parents?

															First, you have to sign up through our app which is available in Google Play Store. Then you need to follow the following process:</p>
															<ul class="dashed">
																<li>1. Enter your Mobile Number</li>
																<li>2. Enter your First name, last name and email (Optional)</li>
																<li>3. Select district of the School</li>
																<li>4. Choose the school from the list or create a new one if not available</li>
																<li>5. Enter your children's first name, last name, class and class code (will be provided by the school). </li>
																<li>6. Enter your desired password for MiDas App - For Parents to complete the Sign Up process</li>
															</ul>
															<p>As a Parents, you can </p>
															<ul class="mycustul">
																<li>1. View Attendance of your Childrens</li>
																<ul class="padleft50"><li>- If your child is absent or late, you will be notified through message. </li></ul>
																<li>2. View Homework of your children</li>
																<ul class="padleft50"><li>- You will be notified about your children's homework.</li></ul>
																<li>3. Monitor/Evaluate Homework  of your class</li>
																<ul class="padleft50"><li>- You will be notified about the status of your children’s homework.</li></ul>
																<li>4. Send message to Individual Teacher of your child</li>
															</ul>
															Please note, to get message, the techer or teachers should have installed MiDas App - For Teachers and should be connected to Internet.


															<p class="text-justify">You promise to only use the Services for your personal, internal, non-commercial, educational use, and only in a manner that complies with all laws that apply to you.</p> 

										<!-- <h4>The Content and Data</h4> 

										<p class="text-justify">The Data (including, but not limited to, messaging text, graphics, articles, photos, images, illustrations, homework, User Submissions and so forth) can be sent by Teacher or School Administrator. Some of the details of information that can be sent are as following:</p>
										<ul>
											<li>1. You will send message to Teacher for which you will be accountable</li>
											<li>2. School Administrator should carefully monitor the content of the text sent by Teacher</li>
											<li>3. Academic and student related information, Home work, Attendance, Mark Sheet will be sent by Teacher or School administrator</li>
											<li>4. The information sent through MiDas App - for Teacher is very sensitive, accordingly due care should be taken before sending any messages</li>
											<li>5. The school should monitor the security access of user of MiDas Apps - for Parents.</li>
										</ul> -->

										<h4>Who is responsible for what I see and do on the Services?</h4>

										<p class="text-justify">Any information or content publicly posted or privately transmitted through the Services is the sole responsibility of the user themsleves, and you access all such information and content at your own risk, and we aren't liable for any errors or omissions in that information or content or for any damages or loss you might suffer in connection with it. </p>

										<p class="text-justify">We require Parents to guard their User ID and Password with the appropriate confidentiality.</p>

										<p class="text-justify">You are responsible for all Content you contribute, in any manner, to the Services, and you represent and warrant you have all rights necessary to do so, in the manner in which you contribute it. You will keep all your registration information accurate and current. You are responsible for all your activity in connection with the Services.</p>

										<h4>Will MiDas App - For Parents ever change the Services?</h4>

										<p class="text-justify">
											MiDas App - For Parents is a dynamic communicating tool, so the Services will change over time. We may change or introduce new features and Services. </p>

											<h4>Does MiDas App - For Parents cost anything?</h4>

											<p class="text-justify">The basic MiDas App - For Parents Services are free and always will be - that is, we don't charge for signing up for a basic services such as</p>
											<style>
												ul.dashed li{
													display: block;
												}</style>
												<ul class="dashed">
													<li>- School Calendar, </li>
													<li>- Attendance,</li>
													<li>- Homework, </li>
													<li>- Exam Routine</li>
													<li>- Marks and</li>
													<li>- Messaging</li>
												</ul>
												<h4>Privacy</h4>
												<p class="text-justify">We take the privacy of our users very seriously. Another important document to look at is our Privacy Policy, which outlines what personal information MiDas App - For Parents collects from you and how we use that information to provide our service.</p>

												<p class="text-justify">Note that, by using the Services, you may receive text messages on your phone or mobile device over cellular network or over the internet, which may cause you to incur usage charges or other fees or costs in accordance with your wireless or data service plan. Any and all such charges, fees, or costs are your sole responsibility. You should consult with your wireless carrier to determine what rates, charges, fees, or costs may apply to your use of the Services.</p>

										 <!-- <h4>For School Administrator</h4>
										<ul>
										<li>If you are a school administrator:</li>
										<ul><li>You must maintain the accuracy of the information relating to your School. By way of example, you will only permit staff members (such as Parents) who are current employees of your school to use the Services and to create their own class.</li></ul>
										<li>You must know that any teacher can send Mass message through this app to Individual / Group of Parents, Parents or Parents</li>
										<ul><li>MiDas App - For Parents  verifies you as an administrator of your School, you may have the ability to perform the following tasks:</li></ul>
										<ul class="no-bull">
										<li>1.	Able to view and manage users and information affiliated with your School.</li>
										<li>2.	Add Parents, Parents or Students to the Services,</li>
										<li>3.	Remove Parents, Parents or Students from classes</li>
										<li>4.	Connect and upload or sync information relating to Parents, Parents or Students using a data upload or syncing mechanism </li>
										</ul>
										</ul>
										<p class="text-justify">If you choose to do any of the above, you represent and warrant that you have all rights and have obtained all consents and authorizations necessary to perform such tasks.</p>
									-->
									<h4>Registration and security</h4>
									<p class="text-justify">As a condition to using Services, you are required to register with MiDas App - For Parents. You will provide MiDas App - For Parents with accurate, complete, and updated registration information. You may not</p>
									<ul class="dashed">
										<li>- select or use the name of another person with the intent to impersonate that person; or</li>
										<li>- use a name subject to any rights of any person other than you without appropriate authorization.</li>
									</ul>

									<h4>Consent to receive periodic messages</h4>
									<p class="text-justify">You acknowledge and consent to receive notifications, messages, sms and phone calls from MiDas Technologies and its sister concerns about its various products and services.</p>
									<p class="text-justify">By signing up for the services, you agree to receive communications from Midas app - for Parents  as well as class, and you represent and warrant that each person you invite and/or add has consented to receive communications from you and Midas app - for Parents. You may receive messages sent by other Parents, Parents, or Students of the School or from MiDas Technologies and its sister concerns.</p>
									<h4>Indemnity</h4>
									<p class="text-justify">You will indemnify and hold MiDas App - For Parents, its parents, subsidiaries, affiliates, officers, and employees harmless  from any claim or demand made by any third party due to or arising out of your access to or use of the Services, your violation of this Agreement, or the infringement by you or any third party using your account of any intellectual property or other right of any person or entity.</p>
									<h4>Limitation of liability</h4>
									<p class="text-justify">To the fullest extent allowed by applicable law, in no event will Midas app - for Parents  or its suppliers or its service providers, or their respective officers, directors, employees, or agents be liable with respect to the services. MiDas App - For Parents is a communication tool. While we are here to support you, we are not liable for anything that happens because of our service.</p>
									<h4>Choice of law and arbitration</h4>
									<p class="text-justify">This Agreement will be governed by and construed in accordance with the laws of the Nepal without regard to the conflict of laws provisions thereof</p>
									<h4>Miscellaneous</h4>
									<p class="text-justify">MiDas App - For Parents will not be liable for any failure to perform its obligations hereunder where such failure results from any cause beyond <br> MiDas App - For Parents ’s reasonable control, including, without limitation, mechanical, electronic or communications failure or degradation.</p>
								</article>
							</section>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					</div>

				</div>
			</div>
			<!-- Modal1 Close -->
			
			<li><a href="#" data-toggle="modal" data-target="#myModal2">Privacy policy</a></li>
			<!-- Modal2 -->
			<div id="myModal2" class="modal fade" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">×</button>
							<h3 class="modal-title">Privacy Policy</h3>
						</div>
						<div class="modal-body">
							<section class="terma">
								<article id="main_article2">    
									<p class="text-justify">At MiDas Technologies Pvt. Ltd., we know you care about how your personal information is used and shared, and we take your privacy seriously. 
										MiDas App - For Parents is a communicating platform that helps parents users to send quick, simple messages to any device. MiDas App - For Parents respects your information and treats it carefully.</p>

										<p class="text-justify">You are responsible for any Content you provide in connection with the Services. We cannot control the actions of anyone with whom you or any other MiDas App - For Parents users may choose to share information. Please be aware that no security measures are perfect or impenetrable and that we are not responsible for circumvention of any security measures contained on the Services. MiDas App - For Parents does not encourage you to make any personally identifiable information (Personal Information) public other than what is necessary for you to use our Services. You understand and acknowledge that, even after removal, copies of Content may remain viewable in cached pages, archives and storage backups or if other users have copied or stored your Content.</p>

										<p class="text-justify">MiDas App - For Parents is a tool to help you communicate with teachers, school admins which helps support their children's education, and we take your privacy seriously. Maintaining the privacy of your Personal Information is a shared responsibility, and while we will limit what we ask for and what we do with your information, we encourage you not to share it unless you need to. Please keep in mind that you are responsible for the content of your account and all your messages.</p>

										<h4>What does this privacy policy cover?</h4>
										<p class="text-justify">This Privacy Policy explains how MiDas App - For Parents collects and uses information from you and other users who access or use the Services, including our treatment of Personal Information.</p>
										<p class="text-justify">MiDas App - For Parents collects limited Personal Information of children and we rely on a Teacher,School to obtain the consent of each child’s parent when collecting this information.</p>

										<h4>What information does MiDas App - For Parents display or collect?</h4>
										<p class="text-justify">When you use the Services, you may set up your personal profile, send messages, and transmit information as permitted by the functionality of the Services. MiDas App - For Parents will not display your personal contact information to other users without your permission. The information we gather from users enables us to verify user identity, allows our users to set up a user account and profile through the Services, and helps us personalize and improve our Services. We retain this information to provide the Services to you and our other users and to provide a useful user experience.</p>
										<h4>Information you provide to us</h4>
										<p class="text-justify">We receive and store any information you knowingly enter on the Services, whether via mobile phone, other wireless device, or that you provide to us in any other way. This information may include Personal Information such as your name, phone numbers, email addresses, photographs, and, in certain circumstances, your school, School affiliation. We use the Personal Information we receive about you to provide you with the Services and also for purposes such as:</p>
										<ul class="dashed">
											<li>- authenticating your identity,</li> 
											<li>- responding to your requests for certain information,</li> 
											<li>- customizing the features that we make available to you,</li> 
											<li>- suggesting relevant services or products for you to use,</li> 
											<li>- improving the Services and internal operations (including troubleshooting, testing, and analyzing usage),</li> 
											<li>- communicating with you about new features,</li> 
											<li>- sending messages from MiDas Technologies and its sister concerns,</li>
											<li>- and, most importantly, protecting our users and working towards making sure our Services are safer and more secure.</li>
										</ul>
										<h4>Information collected automatically</h4>
										<p class="text-justify">We receive and store certain types of information whenever you use the Services. MiDas App - For Parents automatically receives and records information on our server logs from MiDas App – for Parents.  We also may provide to our sister concerns aggregate information derived from automatically collected information about how our users, collectively, use our app. We may share this type of identifiable, aggregated statistical data so that MiDas Technologies and its sister concerns understand how often people use their services as well as MiDas App - For Parents.</p>

										<p class="text-justify">Will MiDas App - For Parents share any of the personal information it receives? 
											MiDas App - For Parents relies on its users to provide accurate Personal Information in order to provide our Services. We try our best to protect that information and make sure that we are responsible in handling, disclosing, and retaining your data. We neither rent nor sell your Personal Information to anyone. However, we may share you information of products and service provided by MiDas Technologies and its sister concerns.</p>


											<h4>Is information about me secure?</h4>
											<p class="text-justify">The security of personal information of the childrens, teachers and school is a top priority for MiDas App - For Parents.
												MiDas App - For Parents has protections in place to enhance our user's security, and routinely update these protections. We have administrative, technical, and physical safeguards designed to protect against unauthorized use, disclosure of or access to personal information. In particular:</p>
												<ul class="dashed">
													<li>- Our engineering team is dedicated to keeping your personal information secure.</li>
													<li>- MiDas App - For Parents stores its data within servers.</li>
													<li>- MiDas App - For Parents’ maintain its database and all backups.</li>
												</ul>
												<br>

												<p class="text-justify"> MiDas App - For Parents endeavors to protect user information and to ensure that user account information is kept private.</p>

												<p class="text-jusitfy">The security of your personal information is extremely important to us and we take measures internally to make sure your information is safe with us. We routinely update our security features and implement new protections for your data.</p>


											</article>

										</section>
									</div>

									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									</div>
								</div>

							</div>
						</div>
						<!-- Modal2 Close -->
						
						<li><a href="blog.php">Blog</a></li>
						<li><a href="#" data-toggle="modal" data-target="#myModal3">Contact</a></li>
						<!-- Modal -->
						<div id="myModal3" class="modal fade" role="dialog">
							<div class="modal-dialog">

								<!-- Modal content-->
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">×</button>
										<h3 class="modal-title">Contact Us</h3>
									</div>
									<div class="modal-body" style="height: auto; overflow-y: none; color:black;">
										<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
											<h2 style="margin-top: 0px; color:black;">MiDas Education Pvt. Ltd.</h2>
											PEA Marg,<br>
											Thapathali, Kathmandu, Nepal.<br>
											Toll Free No: 1660 - 01 – 00000<br>
											<a href="http://www.facebook.com/MiDaseCLASS">www.facebook.com/MiDaseCLASS </a><br>
											<br>
											<strong>For general inquiry:</strong><br>
											Tel Ph. No: +977-1-4244461, 9851184862, 9851170897<br>
											Email Address: <a href="mailto:info@midas.com.np">info@midas.com.np</a><br><br>

											<strong>For sales inquiry:</strong><br>
											Tel Ph. No: 9851184862<br>
											Email Address: <a href="mailto:sales@midas.com.np">sales@midas.com.np</a><br><br>

											<strong>For support inquiry:</strong><br>
											Tel Ph. No: 9851184863<br>
											Email Address: <a href="mailto:support@midas.com.np">support@midas.com.np</a><br>
										</div>
										<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
											<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3532.8006477335307!2d85.31598866169557!3d27.69255568184849!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb19b0546e8977%3A0xb3d496a85fdb0885!2sMidas+Technologies+Pvt.+Ltd!5e0!3m2!1sen!2snp!4v1492766789690" width="100%" height="350" frameborder="2px solid" style="border:0" allowfullscreen=""></iframe>
										</div>
									</div>
									<div class="modal-footer" style="clear:both;">
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									</div>
								</div>

							</div>
						</div>
						<!-- Modal Close -->
					</ul>
				</div>
			</div>
			
			<div class="col-md-7 col-sm-6">
				<div class="copy-right1">
					<p class="quick-links">Quick Links </p>
					<ul class="clearfix" style="list-style-type: none;">
						<li><a href="https://play.google.com/store/apps/details?id=com.midas.midasstudent" target="_blank">Midas App-For Parents</a></li>
						<li><a href="https://play.google.com/store/apps/details?id=com.midas.midasteacher&amp;hl=en" target="_blank">Midas App-For Teacher</a></li>
						<li><a href="#">Midas Technologies</a></li>
						<li><a href="#">Midas Education</a></li>
					</ul>
				</div>
			</div>
		</div>
		
	</div>
</div>




</footer>	
	<script>
        $(document).ready(function() {
  function setHeight() {
    windowHeight = $(window).innerHeight();
      var xwin = windowHeight-400;
    $('.sec-block-bg').css('min-height', xwin);
  };
  setHeight();
  
  $(window).resize(function() {
    setHeight();
  });
});
    
</script>
</body>
</html>


	