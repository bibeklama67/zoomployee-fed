<html dir="ltr" lang="en-US">
<head>
  <title>Midas-E-Learning</title>

  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
<!-- <link rel="icon" type="image/png" href="images/favicon.png">
-->    <link href="http://midas.com.np/elearning/assets/css/default/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="http://midas.com.np/elearning/assets/css/default/circle.css" rel="stylesheet" type="text/css" />
<link href="http://midas.com.np/elearning/assets/css/default/sm-cust.css" rel="stylesheet" type="text/css" />
<link href="http://midas.com.np/elearning/assets/css/default/style.css" rel="stylesheet" type="text/css" />
<link href="http://midas.com.np/elearning/assets/css/default/tinycarousel.css" rel="stylesheet" type="text/css" />


<script type="text/javascript" src="http://midas.com.np/elearning/assets/js/default/ajquery-2.1.1.js" ></script><script type="text/javascript" src="http://midas.com.np/elearning/assets/js/default/bootstrap.min.js" ></script><script type="text/javascript" src="http://midas.com.np/elearning/assets/js/default/jquery-ui.min.js" ></script><script type="text/javascript" src="http://midas.com.np/elearning/assets/js/default/jquery.menu-aim.js" ></script><script type="text/javascript" src="http://midas.com.np/elearning/assets/js/default/jquery.tinycarousel.js" ></script><script type="text/javascript" src="http://midas.com.np/elearning/assets/js/default/main.js" ></script><script type="text/javascript" src="http://midas.com.np/elearning/assets/js/default/modernizr.js" ></script><script type="text/javascript" src="http://midas.com.np/elearning/assets/js/default/zdefault.js" ></script><script type="text/javascript" src="http://midas.com.np/elearning/assets/js/template/login/login.js" ></script>  
<script type="text/javascript" src="assets/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>

</head>
<body>
 <style type="text/css">
  .overlay {
    position: absolute;
    top: 0;
    left: 0;
    text-align: center;
    background: rgba(0,0,0,0.3);}

    .overlay img {
      position: fixed;
      top: 35%;
      left: 50%;
    }
    .main-logo{padding:10px; max-height:80px;}
    .logo_brand{padding:0;}
    ul.navbar-nav li{padding:5px;}
    .mar_auto{margin:0 auto;}
    .btn-home{position: absolute;
      left: 45%;
      bottom: 38%;}
      .jumbotron{padding-top:0; margin-bottom:0; 
        background:url('assets/images/background.jpg'); 
        background-repeat:no-repeat; background-position: center;
        background-color:#f1efeb;
        background-size:contain;

      }
      .btn-navy{background:#0362a9; border-radius:30px; color:#fff; padding: 5px 45px;}
      .btn-navy:focus,.btn-navy:hover{color:#fff; background:#6e99d4;}
      .navbar-default {
        background-color:#fff;
      }
      .navbar{margin-bottom:0;}
      .copy-text{color:#737373;}
      .navbar-right{margin-top:10px;}
      .navbar-header{margin-bottom:20px;}
      .sum-control{border-radius:0; background:#f1efeb;}
      .btn-wrap{padding:20% 10% 10% 10%}
      .container.err-block{min-height: 250 px !important;padding: 10px 25px 0px 5px !important;}.bg-white.jumbotron{background:#fff !important;}
      .alert.alert-danger{box-shadow: 5px 5px #fafafa;}
    </style>

    <nav class="navbar navbar-default">
      <div class="container-fluid container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">

          <a class="navbar-brand logo_brand" href="#"><img src="http://midas.com.np/elearning/assets/images/logo.png" class="img-responsive main-logo"></a>
        </div> 
        <form name="login" action="" method="post">     
          <ul class="nav navbar-nav navbar-right">
            <li>
              <input type="text" id="Username" class="form-control sum-control Username" name="Username" placeholder="User ID">       
            </li>
            <li><input type="password" id="Password" class="form-control sum-control Password" name="Password" placeholder="Password">
              <input type="hidden" name="ipaddress" id="ipaddress" value="192.168.2.2">
              <a href="#" style="padding:5px 0 5px 10px;">Forgot Password ?</a></li>
              <li><button class="btn-success btn login" id="login-btn">Log In</button></li>
            </ul>
          </form>

        </div><!-- /.container-fluid -->
        <div class="container pad-fix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pad-fix">
            <div class="col-lg-7 col-md-6 col-sm-6 hidden-xs">
            </div>
            <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 pad-fix">
             <div id="message_wrapper container" ><div class="message"><p></p></div></div>
           </div>
         </div>
       </div>
     </nav>