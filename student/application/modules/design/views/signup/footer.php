 <div class="container">                        
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 foot-container">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 foot-box">
            	<h4>About us</h4>
                <p>MiDas eCLASS is a Teaching and Learning Software (available in the form of Android Apps also) that has Tutorial Videos and Interactive Games of all chapters of the book that students study in their schools. Using this software students can watch movie and play games of the chapters they need to study in the school, so that ....   <a href="#" id="read-more">Read more</a>
                </p>
            
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 foot-box">
            	<h4>Navigation</h4>
                <ul class="foot-nav">
                	<li><a href="#">Home</a></li>
                    <li><a href="#">Midas eCLASS</a></li>
                    <li><a href="#">Contact us</a></li>
                </ul>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 contact foot-box">
            	<h4>Get in touch with us</h4>
                <ul class="contact-us">
                	<li><img src="images/mobile.jpg">+977-1-4244461, 4102094, 4102095</li><div class="clearfix"></div>
                    <li><img src="images/email.jpg"><a href="#">info@midas.com.np</a></li><div class="clearfix"></div>
                    <li><img src="images/loc.jpg"><a href="#">Location map</a></li><div class="clearfix"></div>
                </ul>
            </div>
        
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 copyright">
        	<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
	        	<h3>&copy; <span id="two">2015</span> MiDas Education Pvt. Ltd.</h3>
            </div>    
            <div class="pull-right f-fblogo">
            	<a href="#"><img src="images/facebook-logo.png" /></a>
            </div>
        
        </div>  
    </div>                      

						<!--			Footer portion ends						-->
</div>


</body>
</html>