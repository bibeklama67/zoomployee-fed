
<?=doctype('html')?>

<html dir="ltr" lang="en-US">
<head>
<title>MiDas Apps</title>
<script>
  var base_url= "<?= base_url();?>";
</script>
<?php 
  $this->config->load('site_setting');
    $default=$this->config->item('default');
    $default_items=$this->config->item($default);
// print_r($default_items);
    $title=element('title',$default_items);

?>
<style type="text/css">
  .overlay {
    position: absolute;
    top: 0;
    left: 0;
    text-align: center;
    background: rgba(0,0,0,0.3);}

    .overlay img {
    position: fixed;
    top: 35%;
    left: 50%;
}
</style>
<?php
      echo meta('content-type','text/html; charset=utf-8','equiv');
        $common_items=$this->config->item('common');
   
        $common_meta=element('meta',$common_items);
        $default_meta=element('meta',$default_items);
        if($common_meta && $default_meta)
          $meta=array_merge($common_meta,$default_meta);        
        else if($common_meta)
          $meta=$common_meta;
        else
          $meta=$default_meta;
          
        if(isset($meta) && is_array($meta))       
            foreach($meta as $k=>$v)          
              echo meta($k,$v); 
    ?>
<link rel="icon" type="image/png" href="http://midas.com.np/images/favicon.ico">    <?php
        $common_css=element('css',$common_items);
        $default_css=element('css',$default_items);

  
        if(isset($common_css) && is_array($common_css))
        {
          $fold=element('folder',$common_css);
          if($fold)

          foreach($fold as $f)
          foreach (glob("assets/css/$f/*.css") as $filename)
          {
            // $filename=ltrim($filename,"assets/");

            $link_arr = array('href'=>$filename,
                'rel' =>'stylesheet',
                'type'=>'text/css'
              );

            echo link_tag($link_arr); 
          }
            
          unset($common_css['folder']); 
                
          foreach($common_css as $c):
            $link_arr = array('href'=>"assets/css/common/$c.css",
              'rel' =>'stylesheet',
              'type'=>'text/css'
            );
            echo link_tag($link_arr);
          endforeach;
          
        }
        
        unset($link_arr); unset($common_css); 
        
        if(isset($default_css) && is_array($default_css))
        {
     
          $fold=element('folder',$default_css);
          if($fold)
          foreach($fold as $f)
          foreach (glob("assets/css/$f/*.css") as $filename)
          {

            // $filename=ltrim($filename,"assets/");
           
            $link_arr = array('href'=>$filename,
                'rel' =>'stylesheet',
                'type'=>'text/css'
              );
            echo link_tag($link_arr); 
          }
          unset($default_css['folder']);
                

          foreach($default_css as $c):
            $link_arr = array('href'=>"assets/css/$default/$c.css",
              'rel' =>'stylesheet',
              'type'=>'text/css'
            );
            echo link_tag($link_arr);
          endforeach;
        }
          unset($link_arr); unset($default_css);                    
    ?>  
 <?php 
        $common_js=element('js',$common_items);
        $default_js=element('js',$default_items);
                
        if(isset($common_js) && is_array($common_js)):
        
          $fold=element('folder',$common_js);
          if($fold):
            foreach($fold as $f):
              foreach (glob("assets/js/$f/*.js") as $filename):
                // $filename=ltrim($filename,"assets/");
                $j_arr = array('type'=>'text/javascript',
                         'src' =>$filename);
                echo script_tag($j_arr);
              endforeach;
            endforeach;
          endif;
          
          unset($common_js['folder']);
          
          foreach($common_js as $j):
            $j_arr = array('type'=>'text/javascript',
                     'src' =>"assets/js/common/$j.js");
            echo script_tag($j_arr);
          endforeach;
          
        endif;
        
        unset($j_arr);unset($common_js);
        
        if(isset($default_js) && is_array($default_js)):
        
          $fold=element('folder',$default_js);
          if($fold):
            foreach($fold as $f):
              foreach (glob("assets/js/$f/*.js") as $filename):
                // $filename=ltrim($filename,'assets/');
                $j_arr = array('type'=>'text/javascript',
                         'src' =>$filename);
                echo script_tag($j_arr);
              endforeach;
            endforeach;
          endif;
          
          unset($default_js['folder']);
                
          foreach($default_js as $j):
            $j_arr = array('type'=>'text/javascript',
                     'src' =>"assets/js/$default/$j.js");
            echo script_tag($j_arr);
          endforeach;
        
        endif;
        unset($j_arr);unset($default_js);
    ?>  
  </head>
<body style="height: 100%;">
 <header> 
<nav class="navbar navbar-default">
  <div class="container-fluid container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      
      <a class="navbar-brand logo_brand" href="#" style="margin:0px; margin-left: -1px;"><img src=
      "<?php echo base_url();?>assets/images/logo_transparent.png" class="img-responsive "></a>
    </div> 
    <form name="login" action="" method="post" id="frm-login">     
      <ul class="nav navbar-nav navbar-right">
        <li>
          <input type="text" id="Username" class="form-control sum-control Username" name="Username" placeholder="User ID">       
        </li>
        <li><input type="password" id="Password" class="form-control sum-control Password" name="Password" placeholder="Password">
        <input type="hidden" name="ipaddress" id="ipaddress" value="<?php echo $_SERVER['REMOTE_ADDR'];?>">
        <input type="hidden" name="loginType" id="loginType" value="">
        <input type="hidden" name="loginSupport" id="loginSupport" value="">
        <input type="hidden" name="loginResponse" id="loginResponse" value="">
       <a href="<?php echo base_url('forgot-password') ?>" style="padding:5px 0 1px 10px;">Forgot Password ?</a>
       <li>
        <!-- <button class="btn-success btn login" id="login-btn">Log In</button> -->
        <!-- Dropdown button -->
        <div class="btn-group">
          <button type="button"  class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Log In As <span class="caret"></span>
          </button>
          <ul class="dropdown-menu custddm-2817" style="min-width:100px; right:-1px;">
            <li><a href="#" id="login-btn" class="login login-student" data-usertype="student">Student</a></li>
            <li><a href="javascript:void(0)" id="login-btn-parent" class="login_parent" data-usertype="parent">Parent</a></li>
            <li><a href="#" id="btn-login-teacher">Teacher</a></li>
          </ul>
        </div>

      </li>
      </ul>
      </form>

  </div><!-- /.container-fluid -->
  <div class="container pad-fix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-6 pad-fix">
      <div class="col-lg-7 col-md-6 col-sm-6 hidden-xs">
      </div>
      <div class="col-lg-5 col-md-5 col-sm-6 col-xs-6 pad-fix login-error-msg">
       <div id="message_wrapper container" ><div class="message"><p></p></div></div>
     </div>
   </div>
 </div>
</nav>
</header>
	
 <footer>
            FOOTER</footer>
</body>
</html>
