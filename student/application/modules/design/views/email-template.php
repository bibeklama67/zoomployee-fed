<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>[Email Subjects]</title>
	<style type="text/css">
		body {
			padding-top: 0 !important;
			padding-bottom: 0 !important;
			padding-top: 0 !important;
			padding-bottom: 0 !important;
			margin: 0 !important;
			width: 100% !important;
			-webkit-text-size-adjust: 100% !important;
			-ms-text-size-adjust: 100% !important;
			-webkit-font-smoothing: antialiased !important;
		}
		
		.tableContent img {
			border: 0 !important;
			display: block !important;
			outline: none !important;
		}
		
		a {
			color: #382F2E;
		}
		
		p,
		h1,
		h2,
		ul,
		ol,
		li,
		div {
			margin: 0;
			padding: 0;
		}
		
		h1,
		h2 {
			font-weight: normal;
			background: transparent !important;
			border: none !important;
		}
		table.m-table1e img{
		max-height:60px;
		max-width:60px;	
			}
		
		@media screen and (max-width:720px){
			.m-table1{
				width:100% !important;}
			table.m-table1e img{
			width:100% !important;
			 !important;
			 
			
			}
			
			@media screen and (max-width:321px){
			table.m-table1c img{
			display:none !important;
			
			}
			}
						
						}
		
		@media only screen and (max-width:480px) {
			table[class="MainContainer"],
			td[class="cell"] {
				width: 100% !important;
				height: auto !important;
			}
			td[class="specbundle"] {
				width: 100% !important;
				float: left !important;
				font-size: 13px !important;
				line-height: 17px !important;
				display: block !important;
				padding-bottom: 15px !important;
			}
			td[class="specbundle2"] {
				width: 80% !important;
				float: left !important;
				font-size: 13px !important;
				line-height: 17px !important;
				display: block !important;
				padding-bottom: 10px !important;
				padding-left: 10% !important;
				padding-right: 10% !important;
			}
			td[class="spechide"] {
				display: none !important;
			}
			img[class="banner"] {
				width: 100% !important;
				height: auto !important;
			}
			td[class="left_pad"] {
				padding-left: 15px !important;
				padding-right: 15px !important;
			}
		}
		
		@media only screen and (max-width:540px) {
			table[class="MainContainer"],
			td[class="cell"] {
				width: 100% !important;
				height: auto !important;
			}
			td[class="specbundle"] {
				width: 100% !important;
				float: left !important;
				font-size: 13px !important;
				line-height: 17px !important;
				display: block !important;
				padding-bottom: 15px !important;
			}
			td[class="specbundle2"] {
				width: 80% !important;
				float: left !important;
				font-size: 13px !important;
				line-height: 17px !important;
				display: block !important;
				padding-bottom: 10px !important;
				padding-left: 10% !important;
				padding-right: 10% !important;
			}
			td[class="spechide"] {
				display: none !important;
			}
			img[class="banner"] {
				width: 100% !important;
				height: auto !important;
			}
			td[class="left_pad"] {
				padding-left: 15px !important;
				padding-right: 15px !important;
			}
		}
		
		.contentEditable h2.big,
		.contentEditable h1.big {
			font-size: 26px !important;
		}
		
		.contentEditable h2.bigger,
		.contentEditable h1.bigger {
			font-size: 37px !important;
		}
		
		td,
		table {
			vertical-align: top;
		}
		
		td.middle {
			vertical-align: middle;
		}
		
		a.link1 {
			font-size: 13px;
			color: #27A1E5;
			line-height: 24px;
			text-decoration: none;
		}
		
		a {
			text-decoration: none;
		}
		
		.link2 {
			color: #ffffff;
			border-top: 10px solid #27A1E5;
			border-bottom: 10px solid #27A1E5;
			border-left: 18px solid #27A1E5;
			border-right: 18px solid #27A1E5;
			border-radius: 3px;
			-moz-border-radius: 3px;
			-webkit-border-radius: 3px;
			background: #27A1E5;
		}
		
		.link3 {
			color: #555555;
			border: 1px solid #cccccc;
			padding: 10px 18px;
			border-radius: 3px;
			-moz-border-radius: 3px;
			-webkit-border-radius: 3px;
			background: #ffffff;
		}
		
		.link4 {
			color: #27A1E5;
			line-height: 24px;
		}
		
		h2,
		h1 {
			line-height: 20px;
		}
		
		p {
			font-size: 14px;
			line-height: 21px;
			color: #6d6d6d;
		}
		
		.contentEditable li {}
		
		.appart p {}
		
		.bgItem {
			background: #ffffff;
		}
		
		.bgBody {
			background: #ffffff;
		}
		
		img {
			outline: none;
			text-decoration: none;
			-ms-interpolation-mode: bicubic;
			width: auto;
			max-width: 100%;
			clear: both;
			display: block;
			float: none;
		}
	</style>


	<script type="colorScheme" class="swatch active">
		{ "name":"Default", "bgBody":"ffffff", "link":"27A1E5", "color":"AAAAAA", "bgItem":"ffffff", "title":"444444" }
	</script>


</head>

<body  class="template-body" paddingwidth="0" paddingheight="0" bgcolor="#d1d3d4" style="padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased;" offset="0" toppadding="0" leftpadding="0">
<table class="m-table" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tbody>
			<tr>
				<td class="web-padfix">
					<!-- padding-->
					<div class="padding pad01" style="height:80px"></div>
					<table class="m-table1" width="700" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff" style="font-family:helvetica, sans-serif;" class="MainContainer">
						<!-- =============== START HEADER =============== -->
						<tbody>
							<tr>
								<td class="web-padfix">
									<table class="m-table1a" width="100%" border="0" cellspacing="0" cellpadding="0">
										<tbody>
											<tr>
												<td valign="top" width="20" style="background: #5A849A">&nbsp;</td>
												<td  class="m-tpad15" style="padding-left: 15px !important">
													<table class="m-table1b" width="100%" border="0" cellspacing="0" cellpadding="0">
														<tbody>
															<tr>
																<td class="movableContentContainer">
																
																	<!-- =============== END HEADER =============== -->
																	<!-- =============== START BODY =============== -->
																	<div class="movableContent d03" style="border: 0px; padding-top: 0px; position: relative;">
																		<table class="m-table1e" width="100%" border="0" cellspacing="0" cellpadding="0">
																			<tbody>
																				<tr>

																				</tr>
																				<tr>

																				</tr>
																			</tbody>
																		</table>



																	</div>
																	<div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
																		<table class="m-table1f" width="100%" border="0" cellspacing="0" cellpadding="0">
																			<tbody>
																				<tr>
																					<!--      <td height="40"></td>-->
																				</tr>
																				<tr class="web-none">
																					<td>
																						<table width="100%" border="0" cellspacing="0" cellpadding="0">
																							<tbody>
																			
	
																							</tbody>
																						</table>
																					</td>
																				</tr>
																			</tbody>
																		</table>



																	</div>
																	<div class="movableContent d04" style="border: 0px; padding-top: 0px; position: relative;">
																		<table  class="m-table1g" width="100%" border="0" cellspacing="0" cellpadding="0">
																			<tbody>
																				<tr>
																					<td class="height5" height="20"></td>
																				</tr>
							<tr>
								<td style="background:#F6F6F6; border-radius:6px;-moz-border-radius:6px;-webkit-border-radius:6px">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tbody>
											<tr>
												<td width="40" valign="top">&nbsp;</td>
												<td valign="top">
													<table  class="m-table1f" width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
														<tr>
															<td class="web-none" height='25'></td>
														</tr>
														<tr>
															<td>
																<div class=' d05 contentEditableContainer contentTextEditable'>
																	<div class='contentEditable' style='text-align: center;'>
																		<h2 style="font-size: 20px; padding-bottom: 8px; border-bottom:1px solid #409ea8 !important">Email Main Title</h2>
																		<br>
																		<p> Email Short Description :Lorem Ipsum is simply dummy text of the printing and typesetting industry. Has been the industry's standard dummy text ever since the 1500s.
																		</p>

																		<br>
																	</div>
																</div>
															</td>
														</tr>
														<tr>
															<td class="web-none" height='24'></td>
														</tr>
													</table>
												</td>
												<td width="40" valign="top">&nbsp;</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
																			</tbody>
																		</table>



																	</div>
																	<div class="movableContent d06" style="border: 0px; padding-top: 0px; position: relative;">
																		<table class="m-table1h" width="100%" border="0" cellspacing="0" cellpadding="0">
																			<tbody>
																				<tr>
																					<td  class="height5" height="20"></td>
																				</tr>
																				<tr>
																					<td>
																						<table  class="m-table1i" width="100%" border="0" cellspacing="0" cellpadding="0">
																							<tbody>
																								<tr>
																									<td valign="top" class="specbundle">
																										<table class="table1k" width="100%" cellpadding="0" cellspacing="0" align="center">
																											<tr>
																												<td height='15'></td>
																											</tr>
																											<tr>
																												<td>
																													<div class='contentEditableContainer contentTextEditable'>
																														<div class='contentEditable' style='text-align: left;'>
																															
																															<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Has been the industry's standard dummy text ever since the 1500s. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Has been the industry's standard dummy text ever since the 1500s. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Has been the industry's standard dummy text ever since the 1500s.</p>
																															<br>

																														</div>
																													</div>
																												</td>
																											</tr>
																										</table>
																									</td>
																									<td width="20" class="spechide">&nbsp;</td>
																									<td class="specbundle" valign="top" width="250" align="center">
																										<div class='contentEditableContainer contentImageEditable'>
																											<div class='contentEditable'>
																												<img src="http://www.myschool.midas.com.np/api/school/../../uploads/ScienceSir.png" alt="side image"   data-default="placeholder" border="0" width="100%">
																											</div>
																										</div>
																									</td>
																								</tr>
																							</tbody>
																						</table>
																					</td>
																				</tr>
																			</tbody>
																		</table>


																	</div>
																	<!-- =============== END BODY =============== -->
																	<!-- =============== START FOOTER =============== -->
																	<div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
																		<table width="100%" border="0" cellspacing="0" cellpadding="0">
																			<tbody>
																				<tr>
																					<td class=" web-padfix" height="28"><hr style="border-bottom: 1px solid #ddd;"></td>
																				</tr>
																				<tr>
																					<td>
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tbody>
															<tr>
																<td valign="top" width="90" class="spechide">&nbsp;</td>
																<td>
																	<table width="100%" cellpadding="0" cellspacing="0" align="center">
																		<tr>
																			<td>
																				<div class='contentEditableContainer contentTextEditable'>
																					<div class='contentEditable' style='text-align: center;color:#AAAAAA;'>
																						<p>
																							<img style="margin: 0 auto;width:100px" src="http://www.midas.com.np/assets/images/logo_transparent.png"><br>
																							<span> Sent by Midas Technology Pvt. Ltd </span> <br/>
																							<span>[ADDRESS]</span> <br/>
																							<span>[PHONE] </span><br/>

																						</p>
																					</div>
																				</div>
																			</td>
																		</tr>
																	</table>
																</td>
																<td valign="top" width="90" class="spechide">&nbsp;</td>
															</tr>
														</tbody>
													</table>
																					</td>
																				</tr>
																			</tbody>
																		</table>
																		<div class="padding" style="height:40px"></div>
																	</div>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
												<td class="web-none" valign="top" width="20">&nbsp;</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
					<!-- padding-->
					<div class="padding pad01" style="height:80px"></div>
				</td>
			</tr>
		</tbody>
</table>
</body>

</html>