<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pad-fix bread_wrap" style="width: 97%;margin-left: 18px;">
	<h5 class="bread"> Grade Six  > Science And Environment - SIX  >  Measurement</h5>
		<div class="col-lg-9 col-md-9 col-sm-9" id="hwhelp">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 outerbdiv">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 outerbdiv-in">
					<div class="qanda" id="home-workk">
						<div class="col-lg-12 col-md-12 ask-question-div" style="padding-left: 0px;">
							<div class="col-lg-12 col-md-12 col-sm-12 default-bg">
								<div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 pf-n-iptxt" style="padding-left: 0px;">

									<img src="http://midas.com.np/elearning/assets/images/smita-pf_03.png" class="img-responsive img-circle pull-left" style=" width: 8%;"/>


									<textarea placeholder="Ask a question.." rows="4" data-toggle="modal" data-target="#myModal" class="pull-right" style="    width: 90%;
									border: 1px solid #d3d3d3;
									border-radius: 4px;
									resize: none;"></textarea>

								</div>	
							</div>
							<div class="col-lg-12 col-md-12 col-xs-12 post-btns default-bg">
								<div class="col-xs-2"style="margin-top: 10px;">
									<button type="button" class="pull-right"><i class="fa fa-camera" aria-hidden="true" style="color:#808080; font-size: 20px;">&nbsp;&nbsp;</i></button>
								</div>
								<div class="col-xs-6"style="margin-top: 10px;"></div>
								<div class="col-xs-4 post-opt"style="margin-top: 5px;">
									<div class="pull-right" id="post-stat">
										<div class="dropdown" style="display: inline-block;padding-right: 10px;">
											<button class="btn btn-primary dropdown-toggle privacy-btn" type="button" data-toggle="dropdown">&nbsp;&nbsp;MiDas
												<span class="caret"></span>
											</button>
											<ul class="dropdown-menu">
												<li><a href="#"><img src="http://midas.com.np/elearning/assets/images/myschool.jpg"width="20px" height="20px">&nbsp;&nbsp;All</a></li>
												<li><a href="#"><img src="http://midas.com.np/elearning/assets/images/myschool.jpg"width="20px" height="20px">&nbsp;&nbsp;My School</a></li>
												<li><a href="#"><img src="http://midas.com.np/elearning/assets/images/myclass.jpg"width="20px" height="20px">&nbsp;&nbsp;My Class</a></li>
												<li><a href="#"><img src="http://midas.com.np/elearning/assets/images/onlyme.jpg"width="20px" height="20px">&nbsp;&nbsp;Only Me</a></li>
											</ul>
										</div>
										<input name="submit" value="Post" class="btn btn-success btn-send post-creation" type="submit">
									</div>	 
								</div>
							</div>
						</div>
						<div class=" col-lg-12 col-md-12 filter-question">
							<div class="col-md-4">
								<label for="show-creations"><span style="color: #8c8c8c;font-weight: 600;font-size: 16;">Show Questions From:</span></label>
							</div>
							<div class="col-md-8" style="padding-left: 0px;">
								<ul class="filter-question-list">
									<li><a href="#"><img src="http://midas.com.np/elearning/assets/images/f-onlyme.png">&nbsp;&nbsp;Only Me</a></li>
									<li><a href="#"><img src="http://midas.com.np/elearning/assets/images/f-myclass.png">&nbsp;&nbsp;My Class</a></li>
									<li><a href="#"><img src="http://midas.com.np/elearning/assets/images/f-myschool.png">&nbsp;&nbsp;My School</a></li>
									<li><a href="#"><img src="http://midas.com.np/elearning/assets/images/f-all.png">&nbsp;&nbsp;All</a></li>
								</ul>
							</div>

						</div>
						<div class="col-lg-12 col-md-12 posted-questions-div">
							<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingOne">
										<div class="col-md-12 panel-heading-up">
											<div class="col-md-4"style="padding: 0px;">
												<div class="col-md-12"style="padding: 0px; margin-top: 0px;">
													<div class="pull-left">
														<img src="http://midas.com.np/elearning/assets/images/smita-pf_03.png" width="40px" height="40px" class="img-responsive"/>	  
													</div>
													<div class="pull-left"style="margin-left: 10px;">
														<label for="username" style="margin-bottom: 0px;"><span style="color:#717B9B;">Shritika Karki</span></label><br>
														<small style="color: #7D7D7D;">Sidhartha Banasthali, Jhapa</small><br />
													</div>
												</div>
											</div>
											<div class="col-md-5 posted-question-header">
												<ul style="list-style-type: none; padding-left: 0px;" class="pull-left">
													<li><a><strong>to</strong></a></li>
													<li><a><img src="http://midas.com.np/elearning/assets/images/logo_midas.jpg" class="img-responsive" width="88px" height="33px"></a></li>
													<li><a style="font-size: 11px;margin-top: 10px;">:October 5,2015 at 2:55pm</a></li>
												</ul>
											</div>
											<div class="col-md-3 like-n-colbtn"style="padding: 0px;">
												<ul style="list-style-type: none; padding-left: 0px;" class="pull-right">
													<li><a>Like</a></li>
													<li><a><button type="button"  ><i class="fa fa-thumbs-up" aria-hidden="true" style="font-size: 12px; color:#adb4bc;"></i></button><label for="likes"> 1k </label></a></li>
													<li><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne"> <span class="arrow-up">&nbsp;&nbsp;<br><span style="color:red; fone-size:6px;"></span></span></a></li>
												</ul>
											</div>
										</div>
										<div class="col-md-12 panel-heading-low">
											<p style="color:#0c50a3; padding-top:5px;"><strong >What is measurement ?</strong></p>
										</div>
									</div>
									<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
										<div class="panel-body"style="padding: 0px;margin-top: 5px;">
											<div class="panel-body-up" style="border-bottom: 2px solid #cecfff;">
												<div class="col-md-12" style="margin-top: 10px;">
													<div class="col-md-1 pull-left">
														<img src="http://midas.com.np/elearning/assets/images/smita-pf_03.png" width="40px" height="40px" class="img-responsive"/>	  
													</div>
													<div class="col-md-11 pull-left">
														<label for="username" style="margin-bottom: 0px;"><a><span style="color:#717B9B; font-size:12px;">Raju Baniya</span></a></label>
														<a><small style="color: #7D7D7D;">Replied on&nbsp;</small></a>
														<small style="color:8f949a"><span id="date">November 2, 2016&nbsp;</span> at <span id="time">11:15pm</span></small><br>
														<p class="comment-replied">Thankyou u ferjioj a  h afhuahf ahufh ahfasuhd fauhefhanca cnauhe fuahunf ahdfuhefuhan huhe hawefdh aehf wehf duwh uh gwegygwe gew ggglgkgkegdfakwegrkauwegrkawgerkaweygrkawegkug</p>
													</div>
												</div>
												<div class="col-md-12" style="margin-top: 10px;">
													<div class="col-md-11 pull-left">
														<div class="pull-right">
															<label for="username" style="margin-bottom: 0px;"><a><span style="color:#717B9B; font-size:12px;">Raju Baniya</span></a></label>
															<a><small style="color: #7D7D7D;">Replied on&nbsp;</small></a>
															<small style="color:8f949a"><span id="date">November 2, 2016&nbsp;</span> at <span id="time">11:15pm</span></small>
														</div><br>
														<p class="comment-replied" style="text-align: justify;">Thankyou u ferjioj a  h afhuahf ahufh ahfasuhd fauhefhanca cnauhe fuahunf ahdfuhefuhan huhe hawefdh aehf wehf duwh uh gwegygwe gew ggglgkgkegdfakwegrkauwegrkawgerkaweygrkawegkug</p>
													</div>
													<div class="col-md-1 pull-left">
														<img src="http://midas.com.np/elearning/assets/images/smita-pf_03.png" width="40px" height="40px" class="img-responsive"/>	  
													</div>

												</div>
											</div>
											<div class="panel-body-down">
												<div class="col-md-6" style="margin-top: 20px;">
													<div class="col-md-6 rate-txt">
														<p>Rate this answer</p>
													</div>
													<div class="col-md-6"style="padding-left: 0px;margin-top: -2px;">
														<div class="acidjs-rating-stars">
															<form style="margin-top: 10px;">
								<input type="radio" name="group-1" id="group-1-0" value="5" /><label for="group-1-0"></label><!--
								--><input type="radio" name="group-1" id="group-1-1" value="4" /><label for="group-1-1"></label><!--
								--><input type="radio" name="group-1" id="group-1-2" value="3" /><label for="group-1-2"></label><!--
								--><input type="radio" name="group-1" id="group-1-3" value="2" /><label for="group-1-3"></label><!--
							--><input type="radio" name="group-1" id="group-1-4"  value="1" /><label for="group-1-4"></label>
						</form>
					</div>
				</div>
				
			</div>
			<div class="col-md-6 rating-box">
				<div class="col-md-4 rating-box-l" style="padding-top: 10px;padding-right: 0px;">

					<span style="color:#737373;font-size: 28px;font-weight:100;line-height: 25px;padding-left: 25px;" class="pull-right">4.0</span>
					<div class="acidjs-rating-stars pull-right">
						<form >
							<input type="radio" name="group-2" id="group-2-0" value="5" /><label for="group-2-0"></label><!--
							--><input type="radio" name="group-2" id="group-2-1" value="4" /><label for="group-2-1"></label><!--
							--><input type="radio" checked="checked" name="group-2" id="group-2-2" value="3" /><label for="group-2-2"></label><!--
							--><input type="radio" name="group-2" id="group-2-3" value="2" /><label for="group-2-3"></label><!--
						--><input type="radio" name="group-2" id="group-2-4"  value="1" /><label for="group-2-4"></label>
					</form>
				</div>
				<p style="margin-left: -6px;font-size: 12px;" class="pull-right"><img src="http://midas.com.np/elearning/assets/images/f-onlyme.png"style="margin: 0px;border: 0px;transform: scale(0.7);margin-top: -4px;">&nbsp;63,535,32 total</p>
			</div>
			<div class="col-md-8 rating-box-r"style="padding-top: 5px;">
				<div class="col-md-2">
					<div id="myProgress"><i class="fa fa-star" aria-hidden="true">5</i></div>
					<div id="myProgress"><i class="fa fa-star" aria-hidden="true">4</i></div>
					<div id="myProgress"><i class="fa fa-star" aria-hidden="true">3</i></div>
					<div id="myProgress"><i class="fa fa-star" aria-hidden="true">2</i></div>
					<div id="myProgress"><i class="fa fa-star" aria-hidden="true">1</i></div>
				</div>
				<div class="col-md-10" style="padding-left: 5px;">
					<div id="myProgress"><div id="myBar1">1021</div></div>
					<div id="myProgress"><div id="myBar2">490</div></div>
					<div id="myProgress"><div id="myBar3">400</div></div>
					<div id="myProgress"><div id="myBar4">350</div></div>
					<div id="myProgress"><div id="myBar5">1100</div></div></div>
				</div>
			</div>
			<div class="col-md-12 comment-reply-area">
				<div class="col-md-12"style="padding: 0px; margin-top: 0px;">
					<div class="col-md-1">
						<img src="http://midas.com.np/elearning/assets/images/smita-pf_03.png" width="40px" height="40px" class="img-responsive"/>	  
					</div>
					<div class=" col-md-11">
						<textarea placeholder="Write a comment.." rows="2" data-toggle="modal" data-target="#myModal"></textarea>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><!---->
</div>
</div>
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
	<div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingTwo">
			<div class="col-md-12 panel-heading-up">
				<div class="col-md-4"style="padding: 0px;">
					<div class="col-md-12"style="padding: 0px; margin-top: 0px;">
						<div class="pull-left">
							<img src="http://midas.com.np/elearning/assets/images/smita-pf_03.png" width="40px" height="40px" class="img-responsive"/>	  
						</div>
						<div class="pull-left"style="margin-left: 10px;">
							<label for="username" style="margin-bottom: 0px;"><span style="color:#717B9B;">Shritika Karki</span></label><br>
							<small style="color: #7D7D7D;">Sidhartha Banasthali, Jhapa</small><br />
						</div>
					</div>
				</div>
				<div class="col-md-5 posted-question-header">
					<ul style="list-style-type: none; padding-left: 0px;" class="pull-left">
						<li><a><strong>to</strong></a></li>
						<li><a><img src="http://midas.com.np/elearning/assets/images/logo_midas.jpg" class="img-responsive" width="88px" height="33px"></a></li>
						<li><a style="font-size: 11px;margin-top: 10px;">:October 5,2015 at 2:55pm</a></li>
					</ul>
				</div>
				<div class="col-md-3 like-n-colbtn"style="padding: 0px;">
					<ul style="list-style-type: none; padding-left: 0px;" class="pull-right">
						<li><a>Like</a></li>
						<li><a><button type="button"  ><i class="fa fa-thumbs-up" aria-hidden="true" style="font-size: 12px; color:#adb4bc;"></i></button><label for="likes"> 1k </label></a></li>
						<li><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo"> <span class="arrow-down">&nbsp;&nbsp;<br><span style="color:red; fone-size:6px;"></span></span></a></li>
					</ul>
				</div>
			</div>
			<div class="col-md-12 panel-heading-low">
				<p style="color:#0c50a3; padding-top:5px;"><strong >What is measurement ?</strong></p>
			</div>
		</div>
		<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
			<div class="panel-body"style="padding: 0px;margin-top: 5px;">
				<div class="panel-body-up" style="border-bottom: 2px solid #cecfff;">
					<div class="col-md-12" style="margin-top: 10px;">
						<div class="col-md-1 pull-left">
							<img src="http://midas.com.np/elearning/assets/images/smita-pf_03.png" width="40px" height="40px" class="img-responsive"/>	  
						</div>
						<div class="col-md-11 pull-left">
							<label for="username" style="margin-bottom: 0px;"><a><span style="color:#717B9B; font-size:12px;">Raju Baniya</span></a></label>
							<a><small style="color: #7D7D7D;">Replied on&nbsp;</small></a>
							<small style="color:8f949a"><span id="date">November 2, 2016&nbsp;</span> at <span id="time">11:15pm</span></small><br>
							<p class="comment-replied">Thankyou u ferjioj a  h afhuahf ahufh ahfasuhd fauhefhanca cnauhe fuahunf ahdfuhefuhan huhe hawefdh aehf wehf duwh uh gwegygwe gew ggglgkgkegdfakwegrkauwegrkawgerkaweygrkawegkug</p>
						</div>
					</div>
					<div class="col-md-12" style="margin-top: 10px;">
						<div class="col-md-11 pull-left">
							<div class="pull-right">
								<label for="username" style="margin-bottom: 0px;"><a><span style="color:#717B9B; font-size:12px;">Raju Baniya</span></a></label>
								<a><small style="color: #7D7D7D;">Replied on&nbsp;</small></a>
								<small style="color:8f949a"><span id="date">November 2, 2016&nbsp;</span> at <span id="time">11:15pm</span></small>
							</div><br>
							<p class="comment-replied" style="text-align:justify;">Thankyou u ferjioj a  h afhuahf ahufh ahfasuhd fauhefhanca cnauhe fuahunf ahdfuhefuhan huhe hawefdh aehf wehf duwh uh gwegygwe gew ggglgkgkegdfakwegrkauwegrkawgerkaweygrkawegkug</p>
						</div>
						<div class="col-md-1 pull-left">
							<img src="http://midas.com.np/elearning/assets/images/smita-pf_03.png" width="40px" height="40px" class="img-responsive"/>	  
						</div>

					</div>
				</div>
				<div class="panel-body-down">
					<div class="col-md-6" style="margin-top: 20px;">
						<div class="col-md-6 rate-txt">
							<p>Rate this answer</p>
						</div>
						<div class="col-md-6">
							<div class="acidjs-rating-stars">
								<form style="margin-top: 10px;">
								<input type="radio" name="group-1" id="group-1-0" value="5" /><label for="group-1-0"></label><!--
								--><input type="radio" name="group-1" id="group-1-1" value="4" /><label for="group-1-1"></label><!--
								--><input type="radio" name="group-1" id="group-1-2" value="3" /><label for="group-1-2"></label><!--
								--><input type="radio" name="group-1" id="group-1-3" value="2" /><label for="group-1-3"></label><!--
							--><input type="radio" name="group-1" id="group-1-4"  value="1" /><label for="group-1-4"></label>
						</form>
					</div>
				</div>
			</div>
			<div class="col-md-6 rating-box">
				<div class="col-md-6 rating-box-l" style="padding-top: 10px;padding-right: 0px;">
					<span style="color:#737373;font-size: 28px;font-weight:100;line-height: 25px;padding-left: 25px;" class="pull-right">4.0</span>
					<div class="acidjs-rating-stars pull-right">
						<form >
							<input type="radio" name="group-2" id="group-2-0" value="5" /><label for="group-2-0"></label><!--
							--><input type="radio" name="group-2" id="group-2-1" value="4" /><label for="group-2-1"></label><!--
							--><input type="radio" checked="checked" name="group-2" id="group-2-2" value="3" /><label for="group-2-2"></label><!--
							--><input type="radio" name="group-2" id="group-2-3" value="2" /><label for="group-2-3"></label><!--
						--><input type="radio" name="group-2" id="group-2-4"  value="1" /><label for="group-2-4"></label>
					</form>
				</div>
				<p style="margin-left: -6px;font-size: 12px;" class="pull-right"><img src="http://midas.com.np/elearning/assets/images/f-onlyme.png"style="margin: 0px;border: 0px;transform: scale(0.7);margin-top: -4px;">&nbsp;63,535,32 total</p>
			</div>
			<div class="col-md-6 rating-box-r"style="padding-top: 5px;">
				<div id="myProgress"><div id="myBar1">1021</div></div>
				<div id="myProgress"><div id="myBar2">490</div></div>
				<div id="myProgress"><div id="myBar3">400</div></div>
				<div id="myProgress"><div id="myBar4">350</div></div>
				<div id="myProgress"><div id="myBar5">1100</div></div>
			</div>
		</div>
		<div class="col-md-12 comment-reply-area">
			<div class="col-md-12"style="padding: 0px; margin-top: 0px;">
				<div class="col-md-1">
					<img src="http://midas.com.np/elearning/assets/images/smita-pf_03.png" width="40px" height="40px" class="img-responsive"/>	  
				</div>
				<div class=" col-md-11">
					<textarea placeholder="Write a comment.." rows="2" data-toggle="modal" data-target="#myModal"></textarea>
				</div>
			</div>
		</div>
	</div>
</div>
</div><!---->
</div>

</div>
</div>
</div>
				</div>
			</div>

				
		</div>
		<div class="col-md-3 col-sm-3 col-xs-12" id="stutech-list">
			<div class="col-md-12 col-sm-12 col-xs-12 teacher-list">
				<div class="col-md-12 col-sm-12 col-xs-12 teachers-list-h">
					<a href=""><p>11 Teachers</p></a>
				</div>
				<div class=" col-md-12 col-sm-12 col-xs-12 teachers-list-b">
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">	<img src="http://midas.com.np/elearning/assets/images/smita-pf_03.png" class="img-responsive "/></div>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">	<img src="http://midas.com.np/elearning/assets/images/smita-pf_03.png" class="img-responsive "/></div>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">	<img src="http://midas.com.np/elearning/assets/images/smita-pf_03.png" class="img-responsive "/></div>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">	<img src="http://midas.com.np/elearning/assets/images/smita-pf_03.png" class="img-responsive "/></div>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">	<img src="http://midas.com.np/elearning/assets/images/smita-pf_03.png" class="img-responsive "/></div>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">	<img src="http://midas.com.np/elearning/assets/images/smita-pf_03.png" class="img-responsive "/></div>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">	<img src="http://midas.com.np/elearning/assets/images/smita-pf_03.png" class="img-responsive "/></div>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">	<img src="http://midas.com.np/elearning/assets/images/smita-pf_03.png" class="img-responsive "/></div>
				</div>
				<div class=" col-md-12 col-sm-12 col-xs-12 teachers-list-f">
					<a href=""><p>Show all teachers</p></a>	
				</div>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12 teacher-list">
				<div class="col-md-12 col-sm-12 col-xs-12 teachers-list-h">
					<a href=""><p>32 Classmates</p></a>
				</div>
				<div class=" col-md-12 col-sm-12 col-xs-12 teachers-list-b">
					<div class="col-md-3">	<img src="http://midas.com.np/elearning/assets/images/smita-pf_03.png" class="img-responsive "/></div>
					<div class="col-md-3">	<img src="http://midas.com.np/elearning/assets/images/smita-pf_03.png" class="img-responsive "/></div>
					<div class="col-md-3">	<img src="http://midas.com.np/elearning/assets/images/smita-pf_03.png" class="img-responsive "/></div>
					<div class="col-md-3">	<img src="http://midas.com.np/elearning/assets/images/smita-pf_03.png" class="img-responsive "/></div>
					<div class="col-md-3">	<img src="http://midas.com.np/elearning/assets/images/smita-pf_03.png" class="img-responsive "/></div>
					<div class="col-md-3">	<img src="http://midas.com.np/elearning/assets/images/smita-pf_03.png" class="img-responsive "/></div>
					<div class="col-md-3">	<img src="http://midas.com.np/elearning/assets/images/smita-pf_03.png" class="img-responsive "/></div>
					<div class="col-md-3">	<img src="http://midas.com.np/elearning/assets/images/smita-pf_03.png" class="img-responsive "/></div>
				</div>
				<div class=" col-md-12 col-sm-12 col-xs-12 teachers-list-f">
					<a href=""><p>Show all Classmates</p></a>	
				</div>
			</div>
		</div>
	  
</div>
<script>
  $('.collapse').on('shown.bs.collapse', function(){
    $(this).parent().find(".arrow-down").removeClass("arrow-down").addClass("arrow-up");
  }).on('hidden.bs.collapse', function(){
    $(this).parent().find(".arrow-up").removeClass("arrow-up").addClass("arrow-down");
  });
</script> 