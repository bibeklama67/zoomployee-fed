
<?=doctype('html')?>

<html dir="ltr" lang="en-US">
<head>
<title>MiDas App</title>
<script>
  var base_url= "<?= base_url();?>";
</script>
<?php 
  $this->config->load('site_setting');
    $default=$this->config->item('default');
    $default_items=$this->config->item($default);
// print_r($default_items);
    $title=element('title',$default_items);

?>
 
<?php
      echo meta('content-type','text/html; charset=utf-8','equiv');
        $common_items=$this->config->item('common');
   
        $common_meta=element('meta',$common_items);
        $default_meta=element('meta',$default_items);
        if($common_meta && $default_meta)
          $meta=array_merge($common_meta,$default_meta);        
        else if($common_meta)
          $meta=$common_meta;
        else
          $meta=$default_meta;
          
        if(isset($meta) && is_array($meta))       
            foreach($meta as $k=>$v)          
              echo meta($k,$v); 
    ?>
<link rel="icon" type="image/png" href="http://midas.com.np/images/favicon.ico"> 
 <?php
        $common_css=element('css',$common_items);
        $default_css=element('css',$default_items);

  
        if(isset($common_css) && is_array($common_css))
        {
          $fold=element('folder',$common_css);
          if($fold)

          foreach($fold as $f)
          foreach (glob("assets/css/$f/*.css") as $filename)
          {
            // $filename=ltrim($filename,"assets/");

            $link_arr = array('href'=>$filename,
                'rel' =>'stylesheet',
                'type'=>'text/css'
              );

            echo link_tag($link_arr); 
          }
            
          unset($common_css['folder']); 
                
          foreach($common_css as $c):
            $link_arr = array('href'=>"assets/css/common/$c.css",
              'rel' =>'stylesheet',
              'type'=>'text/css'
            );
            echo link_tag($link_arr);
          endforeach;
          
        }
        
        unset($link_arr); unset($common_css); 
        
        if(isset($default_css) && is_array($default_css))
        {
     
          $fold=element('folder',$default_css);
          if($fold)
          foreach($fold as $f)
          foreach (glob("assets/css/$f/*.css") as $filename)
          {

            // $filename=ltrim($filename,"assets/");
           
            $link_arr = array('href'=>$filename,
                'rel' =>'stylesheet',
                'type'=>'text/css'
              );
            echo link_tag($link_arr); 
          }
          unset($default_css['folder']);
                

          foreach($default_css as $c):
            $link_arr = array('href'=>"assets/css/$default/$c.css",
              'rel' =>'stylesheet',
              'type'=>'text/css'
            );
            echo link_tag($link_arr);
          endforeach;
        }
          unset($link_arr); unset($default_css);                    
    ?>  
 <?php 
        $common_js=element('js',$common_items);
        $default_js=element('js',$default_items);
                
        if(isset($common_js) && is_array($common_js)):
        
          $fold=element('folder',$common_js);
          if($fold):
            foreach($fold as $f):
              foreach (glob("assets/js/$f/*.js") as $filename):
                // $filename=ltrim($filename,"assets/");
                $j_arr = array('type'=>'text/javascript',
                         'src' =>$filename);
                echo script_tag($j_arr);
              endforeach;
            endforeach;
          endif;
          
          unset($common_js['folder']);
          
          foreach($common_js as $j):
            $j_arr = array('type'=>'text/javascript',
                     'src' =>"assets/js/common/$j.js");
            echo script_tag($j_arr);
          endforeach;
          
        endif;
        
        unset($j_arr);unset($common_js);
        
        if(isset($default_js) && is_array($default_js)):
        
          $fold=element('folder',$default_js);
          if($fold):
            foreach($fold as $f):
              foreach (glob("assets/js/$f/*.js") as $filename):
                // $filename=ltrim($filename,'assets/');
                $j_arr = array('type'=>'text/javascript',
                         'src' =>$filename);
                echo script_tag($j_arr);
              endforeach;
            endforeach;
          endif;
          
          unset($default_js['folder']);
                
          foreach($default_js as $j):
            $j_arr = array('type'=>'text/javascript',
                     'src' =>"assets/js/$default/$j.js");
            echo script_tag($j_arr);
          endforeach;
        
        endif;
        unset($j_arr);unset($default_js);
    ?>  
  </head>
<body id="blogbody">
	<div class="head_main_wrap">
		  <div class="container" style="padding:10px 0;">
			<div class="col-lg-6 col-md-4 col-sm-3 col-xs-4" style=" margin: -9px; margin-left: -1px; padding-left: 7px;  ">
			<a href="http://midas.com.np/">
			  <img src="http://www.midas.com.np/assets/images/logo_transparent.png" class="img-responsive" style="padding-bottom: 0px;"></a>
			</div>
			<div class="col-lg-6 col-md-8 col-sm-9 col-xs-8" style="right: 10px; top:0px;">
			<i class="pull-right" style="color:#f27b06; padding:15px 0px; text-align: center; font-size: 20px"> Learn Whenever Wherever</i>
			</div>
		</div>
	</div> 

	<div class="container " style="padding: 0px 5px;  margin-bottom: 15px; ">
				
				
	</div>

	<script>
        $(document).ready(function() {
  function setHeight() {
    windowHeight = $(window).innerHeight();
      var xwin = windowHeight-380;
    $('.sec-block-bg').css('min-height', xwin);
  };
  setHeight();
  
  $(window).resize(function() {
    setHeight();
  });
});
    
</script>
</body>
</html>
