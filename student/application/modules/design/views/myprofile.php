<style>
	
</style>
<div class="myprofilee">

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 v-container">
	<div class="pfp-row">
	<span><strong >Smita Sherpa</strong></span>
	<img src="http://myschool.midas.com.np/api/setup/../../uploads/students/1621337.jpg" class="img-responsive img-thumbnail " style="width:80px; height:80px; ">
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 whitebg ">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd15">
	<h5 class="bread">My Profile      </h5>
</div>
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			<div class="table-responsive responsiv-table">
			<table class="profile-table">
				<tr>
					<td>Name </td>
					<td>: Smita Sherpa</td>
				</tr>
				<tr>
					<td>Mobile </td>
					<td>: 56464413456341</td>
				</tr>
				<tr>
					<td>Email </td>
					<td>: xyz@xyz.com</td>
				</tr>
				<tr>
					<td>Date Of Birth </td>
					<td>: 1990/02/02</td>
				</tr>
				<tr>
					<td>Gender </td>
					<td>: Male</td>
				</tr>
				<tr>
					<td>Blood Group </td>
					<td>: B+ </td>
				</tr>
				</table></div>
				
		</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 buttons" >
		<button type="button" data-toggle="modal" data-target="#updateprofile">Update Profile</button>
		<!--modal for update profile-->
 			<div id="updateprofile" class="modal fade" role="dialog">
  			<div class="modal-dialog">
			<!-- Modal content-->
    		<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Update your profile !</h4>
			  </div>
			  <div class="modal-body">
				<div class="row">
					<div class="col-md-3">
						<div class="text-center">
						  <img src="//placehold.it/100" class="avatar img-circle" alt="avatar">
						  <h6>Upload a different photo...</h6>

						  <input type="file" class="form-control">
						</div>
					</div>
					<div class="col-md-9">
						 <form class="form-horizontal" role="form">
          <div class="form-group">
            <label class="col-lg-3 control-label">First name :</label>
            <div class="col-lg-8">
              <input class="form-control" type="text" placeholder="Smita">
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">Last name :</label>
            <div class="col-lg-8">
              <input class="form-control" type="text" placeholder="Sherpa">
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">Mobile :</label>
            <div class="col-lg-8">
              <input class="form-control" type="number" placeholder="980000000">
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">Email :</label>
            <div class="col-lg-8">
              <input class="form-control" type="email" placeholder="xyz@gmail.com">
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">Date Of Birth :</label>
            <div class="col-lg-8">
              <input class="form-control" type="date" value="">
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">Gender :</label>
            <div class="col-lg-8">
            <input type="radio" name="gender" value="Male"> Male
			&nbsp;&nbsp;<input type="radio" name="gender" value="Female"> Female
			&nbsp;&nbsp;<input  type="radio" name="gender" value="Other"> Other
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">Blood Group :</label>
            <div class="col-lg-8">
              <input class="form-control" type="text" value="">
            </div>
          </div>
          
          
          <div class="form-group">
            <label class="col-md-3 control-label"></label>
            <div class="col-md-8">
              <input type="button" class="btn btn-primary" value="Save Changes">
              <span></span>
              <input type="reset" class="btn btn-default" value="Cancel">
            </div>
          </div>
        </form>
					</div>
				  </div>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			  </div>
			</div>

		  </div>
		</div>
		<!--modal for update profile ends-->
		<button type="button" data-toggle="modal" data-target="#changepassword">Change Password</button>
		<!--modal for change password-->
 			<div id="changepassword" class="modal fade" role="dialog">
  			<div class="modal-dialog">
			<!-- Modal content-->
    		<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Change your password</h4>
			  </div>
			  <div class="modal-body">
				<div class="row">
			<form class="form-horizontal" role="form">		
            <div class="form-group">
            <label class="col-md-3 control-label">Old Password :</label>
            <div class="col-md-8">
              <input class="form-control" type="password" value="janeuser">
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-3 control-label">Password:</label>
            <div class="col-md-8">
              <input class="form-control" type="password" value="11111122333">
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-3 control-label">Confirm password:</label>
            <div class="col-md-8">
              <input class="form-control" type="password" value="11111122333">
            </div>
				</div>
				<div class="form-group">
            <label class="col-md-3 control-label"></label>
            <div class="col-md-8">
              <input type="button" class="btn btn-primary" value="Save Changes">
              <span></span>
              <input type="reset" class="btn btn-default" value="Cancel">
            </div>
          </div>
				</form>
					
				  </div>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			  </div>
			</div>

		  </div>
		</div>
		<!--modal for change password-->
	</div>	
	
</div>	
	
</div>