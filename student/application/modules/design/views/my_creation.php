
 <div class="wrapper my-creations" >
   <div class="col-lg-12 col-md-12 col-sm-12" id="my-creations">
		<!--<div class="col-lg-3" id="creation-left-bar" style="padding-left:0;">
			<div class="col-lg-12 creation-left-bar"></div>
		</div>-->
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 creation-right-side" id="creation-right-side" style="padding: 0px;">
			<div class="myschool_wrapper">  
				<div class="col-lg-12  rt-block pad-fix">
							<h5 class="title_er">My Creations<span class="pull-right"></span></h5>
				</div>
				<div class="col-lg-12 no-padding white-bg">
				<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
					<div class="col-lg-12 col-md-12 col-sm-12 default-bg">
						<div class="pf-n-iptxt">
							<div class="rounded-pf-pic">
								<img src="http://midas.com.np/elearning/assets/images/smita-pf_03.png" class="img-responsive"/>
							</div>
							<div class="input-creation">
								<textarea placeholder="Post your creation here..." rows="2" data-toggle="modal" data-target="#myModal"></textarea>
							</div>
						</div>	
					</div>
					<div class="=col-lg12 col-md-12 post-btns default-bg">
						<div class="col-md-2"style="margin-top: 10px;">
							<button type="button" class="pull-right"><i class="fa fa-camera" aria-hidden="true" style="color:#808080; font-size: 20px;">&nbsp;&nbsp;</i></button>
						</div>
						<div class="col-md-6"style="margin-top: 10px;"></div>
						<div class="col-md-4 post-opt"style="margin-top: 5px;">
						  <div class="pull-right">
						  <div class="dropdown" style="display: inline-block;">
								<button class="btn btn-primary dropdown-toggle privacy-btn" type="button" data-toggle="dropdown">Story
								<span class="caret"></span></button>
								<ul class="dropdown-menu story-cust-ddm">

								</ul>
							</div>
							<div class="dropdown" style="display: inline-block;">
								<button class="btn btn-primary dropdown-toggle privacy-btn" type="button" data-toggle="dropdown"><img src="http://midas.com.np/elearning/assets/images/all.jpg" width="20px" height="20px">&nbsp;&nbsp;All
								<span class="caret"></span></button>
								<ul class="dropdown-menu cust-ddm">

								  <li><a href="#"><img src="http://midas.com.np/elearning/assets/images/myschool.jpg" width="20px" height="20px">&nbsp;&nbsp;My School</a></li>
								  <li><a href="#"><img src="http://midas.com.np/elearning/assets/images/myclass.jpg" width="20px" height="20px">&nbsp;&nbsp;My Class</a></li>
								  <li><a href="#"><img src="http://midas.com.np/elearning/assets/images/onlyme.jpg" width="20px" height="20px">&nbsp;&nbsp;Only Me</a></li>
								</ul>
							</div>
							 <input id="post-stat" name="submit" value="Post" class="btn btn-success btn-send post-creation" type="submit">
						  </div>	 
						</div>
					</div>
						<!--<div class=" col-lg-12 col-md-12 filter-creation">
						<div class="col-md-3"><label for="show-creations"><span style="color: #8c8c8c;font-weight: normal;font-size: 18px;">Show Creations From:</span></label</div>
						
							</div>
					<div class="col-md-9">
						<ul class="filter-creation-list">
						   		<li><a href="#"><img src="http://midas.com.np/elearning/assets/images/f-onlyme.png">&nbsp;&nbsp;Only Me</a></li>
						   		 <li><a href="#"><img src="http://midas.com.np/elearning/assets/images/f-myclass.png">&nbsp;&nbsp;My Class</a></li>
						   		  <li><a href="#"><img src="http://midas.com.np/elearning/assets/images/f-myschool.png">&nbsp;&nbsp;My School</a></li>
							   <li><a href="#"><img src="http://midas.com.np/elearning/assets/images/f-all.png">&nbsp;&nbsp;All</a></li>
								 
								 
								  
						</ul>
					</div>

				</div>-->
						<div class="col-lg-12 col-md-12 posted-creations">
							<div class="col-md-12" style="padding: 0px;">
								<div class="col-md-6" style="padding: 0px;">
									<div class="col-md-12" style="padding: 0px; margin-top: 0px;">
									  <div class="pull-left">
									    <img src="http://midas.com.np/elearning/assets/images/smita-pf_03.png" width="50px" height="50px" class="img-responsive"/>	  
									  </div>
									  <div class="pull-left" style="margin-left: 10px;">
										<label for="username" style="margin-bottom: 0px;"><span style="color:#717B9B;">Shritika Karki</span></label><br>
									    <small style="color: #7D7D7D;">Sidhartha Banasthali, Jhapa</small><br />
									    <small style="color: #7D7D7D;">March 10 at 10:39pm</small>
									  </div>
									</div>
								</div>
								<div class="col-md-6" style="padding-right: 0px;">
									<div class="dropdown pull-right">
                 						
                              			<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background: #ffffff;">
                              			 <i class="fa fa-chevron-down" aria-hidden="true" style="color: #B3B7BE;font-size: 10px;margin-top: -5px;"></i></button>
                              			 <ul class="dropdown-menu">
											 <li><a href="#">Edit</a></li>
											 <li><a href="#">Delete</a></li>
                          				</ul>
             </div>
								</div>
				                <div class="col-md-12">
				                	<p>Holi Started from MiDas!</p>
				                	<img src="http://midas.com.np/elearning/assets/images/posted-photo.png">
				                </div>
				                <div class="col-md-12 likes-n-comments">
				                	<div class="col-md-6">
				                		 <div class="pull-left" style="padding-top: 5px;">
				                		<div style="width:15px; height:15px; background:#15ff00; border-radius:50%;"></div>
				                		
				                	  </div>	
				                	</div>
				                	<div class="col-md-6">
				                	  <div class="pull-right likes-comments" style="padding-top: 5px;">
				                		 <button type="button"  ><i class="fa fa-thumbs-up" aria-hidden="true" style="font-size: 15px; color:#adb4bc;"></i></button><label for="likes">&nbsp;  1k </label> &nbsp; &nbsp;
                            			<button type="button" data-toggle="collapse" data-target="#comments" aria-expanded="false"><span class="glyphicon glyphicon-comment" style="font-size: 15px; color:#adb4bc;"></button> <label for="comments">&nbsp; 2k </label> &nbsp;
				                	  </div>	
				                	</div>
				                </div>
				                <div class="col-md-12 comments-display" style="margin-top: 5px; ">
				                	<div class="col-md-12" style="padding: 0px; margin-top: 0px;">
									  <div class="pull-left">
									    <img src="http://midas.com.np/elearning/assets/images/smita-pf_03.png" width="40px" height="40px" class="img-responsive"/>	  
									  </div>
									  <div class="pull-left" style="margin-left: 10px;">
										<label for="username" style="margin-bottom: 0px;"><a><span style="color:#717B9B; font-size:12px;">Raju Baniya</span></a></label><small>Beatiful Photographs</small> <br>
									    <a><small style="color: #7D7D7D;">Like</small></a>
									    <i class="fa fa-circle" aria-hidden="true" style="font-size:3px;"></i>
									    <a><small style="color: #7D7D7D;">Reply</small></a>
									    <i class="fa fa-circle" aria-hidden="true" style="font-size:3px;"></i>
									    <small style="color:8f949a">November 2, 2016 at 11:15pm</small>
									  </div>
									</div>
									<div class="col-md-12" style="margin-top: 0px;">
										<div class="comment-reply">
			                	    		<ul style="list-style-type: none;">
				                	    	<li><a><img src="http://midas.com.np/elearning/assets/images/angled-arrow.png" class="img-responsive"/></a></li>
				                			<li><a><img src="http://midas.com.np/elearning/assets/images/commenter.png" class="img-responsive"/></a></li>
				                			<li><label for="commeter" style="font-size:12px;"><a style="color:#3c5899;">Menuka Karki</a></label> <span style="color:#3c5899; font-size:11px;"> replied. 1 Reply</span></li>
				                			</ul>
				                		</div>
				                	</div>
				                	<div class="col-md-12 comment-reply-area">
				                		<div class="col-md-12" style="padding: 0px; margin-top: 0px;">
									  <div class="pull-left">
									    <img src="http://midas.com.np/elearning/assets/images/smita-pf_03.png" width="40px" height="40px" class="img-responsive"/>	  
									  </div>
									  <div class="pull-left" style="margin-left: 10px;">
										<textarea placeholder="Write a comment.." rows="2" data-toggle="modal" data-target="#myModal"></textarea>
									  </div>
									</div>
				                	</div>
				                </div>
				              </div>
      				    </div>
      				    <div class="col-lg-12 col-md-12 posted-creations">
							<div class="col-md-12" style="padding: 0px;">
								<div class="col-md-6" style="padding: 0px;">
									<div class="col-md-12" style="padding: 0px; margin-top: 0px;">
									  <div class="pull-left">
									    <img src="http://midas.com.np/elearning/assets/images/smita-pf_03.png" width="50px" height="50px" class="img-responsive"/>	  
									  </div>
									  <div class="pull-left" style="margin-left: 10px;">
										<label for="username" style="margin-bottom: 0px;"><span style="color:#717B9B;">Shritika Karki</span></label><br>
									    <small style="color: #7D7D7D;">Sidhartha Banasthali, Jhapa</small><br />
									    <small style="color: #7D7D7D;">March 10 at 10:39pm</small>
									  </div>
									</div>
								</div>
								<div class="col-md-6" style="padding-right: 0px;">
									<div class="dropdown pull-right">
                 						
                              			<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background: #ffffff;">
                              			 <i class="fa fa-chevron-down" aria-hidden="true" style="color: #B3B7BE;font-size: 10px;margin-top: -5px;"></i></button>
                              			 <ul class="dropdown-menu">
											 <li><a href="#">Edit</a></li>
											 <li><a href="#">Delete</a></li>
                          				</ul>
             </div>
								</div>
				                <div class="col-md-12">
				                	<p>Holi Started from MiDas!</p>
				                	<img src="http://midas.com.np/elearning/assets/images/posted-photo.png">
				                </div>
				                <div class="col-md-12 likes-n-comments">
				                	<div class="col-md-6">
				                		 <div class="pull-left" style="padding-top: 5px;">
				                		<div style="width:15px; height:15px; background:#15ff00; border-radius:50%;"></div>
				                		
				                	  </div>	
				                	</div>
				                	<div class="col-md-6">
				                	  <div class="pull-right likes-comments" style="padding-top: 5px;">
				                		 <button type="button"  ><i class="fa fa-thumbs-up" aria-hidden="true" style="font-size: 15px; color:#adb4bc;"></i></button><label for="likes">&nbsp;  1k </label> &nbsp; &nbsp;
                            			<button type="button" data-toggle="collapse" data-target="#comments" aria-expanded="false"><span class="glyphicon glyphicon-comment" style="font-size: 15px; color:#adb4bc;"></button> <label for="comments">&nbsp; 2k </label> &nbsp;
				                	  </div>	
				                	</div>
				                </div>
				                <div class="col-md-12 comments-display" style="margin-top: 5px; ">
				                	<div class="col-md-12" style="padding: 0px; margin-top: 0px;">
									  <div class="pull-left">
									    <img src="http://midas.com.np/elearning/assets/images/smita-pf_03.png" width="40px" height="40px" class="img-responsive"/>	  
									  </div>
									  <div class="pull-left" style="margin-left: 10px;">
										<label for="username" style="margin-bottom: 0px;"><a><span style="color:#717B9B; font-size:12px;">Raju Baniya</span></a></label><small>Beatiful Photographs</small> <br>
									    <a><small style="color: #7D7D7D;">Like</small></a>
									    <i class="fa fa-circle" aria-hidden="true" style="font-size:3px;"></i>
									    <a><small style="color: #7D7D7D;">Reply</small></a>
									    <i class="fa fa-circle" aria-hidden="true" style="font-size:3px;"></i>
									    <small style="color:8f949a">November 2, 2016 at 11:15pm</small>
									  </div>
									</div>
									<div class="col-md-12" style="margin-top: 0px;">
										<div class="comment-reply">
			                	    		<ul style="list-style-type: none;">
				                	    	<li><a><img src="http://midas.com.np/elearning/assets/images/angled-arrow.png" class="img-responsive"/></a></li>
				                			<li><a><img src="http://midas.com.np/elearning/assets/images/commenter.png" class="img-responsive"/></a></li>
				                			<li><label for="commeter" style="font-size:12px;"><a style="color:#3c5899;">Menuka Karki</a></label> <span style="color:#3c5899; font-size:11px;"> replied. 1 Reply</span></li>
				                			</ul>
				                		</div>
				                	</div>
				                	<div class="col-md-12 comment-reply-area">
				                		<div class="col-md-12" style="padding: 0px; margin-top: 0px;">
									  <div class="pull-left">
									    <img src="http://midas.com.np/elearning/assets/images/smita-pf_03.png" width="40px" height="40px" class="img-responsive"/>	  
									  </div>
									  <div class="pull-left" style="margin-left: 10px;">
										<textarea placeholder="Write a comment.." rows="2" data-toggle="modal" data-target="#myModal"></textarea>
									  </div>
									</div>
				                	</div>
				                </div>
				              </div>
      				    </div>
       				</div>    
       				<div class="col-lg-3 col-md-3 col-sm-3 col-x-12 creations-right-bar">
       					<div class="col-md-12 col-sm-12 col-xs-12 creations-right-bar-1">
       						<p>
       							Imagination is the beginning of creation. You imagine what you desire, you will what you imagine and at last you  create what you will. <br>
       							<span class="pull-right">- George Bernard Shaw</span>
       						</p>
       					</div>
       					
       					
       					<div class="col-md-12 col-sm-12 col-xs-12  creations-right-bar-2">
       						<div class="col-md-12 col-sm-12 col-xs-12 give-border-bottom ">
								<h4> Your Rating</h4>
							</div>
							<div class=" col-md-12 col-sm-12 col-xs-12 ratings">
							<div class="top-pad10">
								<p><span class="font16">Average Rating</span><br>
          <span><big class="mi44 bfont19">5 <span class="glyphicon glyphicon-star myy"></span></big></span>
        </p>
							</div>
							<div class="top-pad10">
								<p><span class="font16">Total Ratings</span><br>
        <span ><big class="bfont19">2</big></span></p>
							</div>
								
							</div>
		
       					</div>
       					<div class="col-md-12 col-sm-12 col-xs-12  creations-right-bar-3">
       						<div class="col-md-12 col-sm-12 col-xs-12 give-border-bottom  ">
								<p> Your Position</p>
							</div>
							<div class=" col-md-12 col-sm-12 col-xs-12 top-pad10 ">
								<p><span class="fontred">2</span>nd in your Class</p>
								<p><span class="fontred">5</span>th in your School</p>
								<p><span class="fontred">8</span>th in All</p>
							</div>
       					</div>
       				</div>
			</div>
		 </div>
					
						
		</div>
				
	</div>
</div>	

	