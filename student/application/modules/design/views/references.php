<div class="">
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pad-fix bread_wrap">
  <h5 class="bread"><a href="javascript:;" data-classid="10" class="classbtn breadcrumb">Grade SIX</a><i class="fa fa-chevron-right"></i><a href="javascript:;" class="subjectbtn breadcrumb" data-subjectname="Science And Environment - SIX" data-subjectid="76">Science And Environment - SIX</a> <i class="fa fa-chevron-right"></i> <span class="active"> Measurement</span></h5>
</div>
<div class="col-lg-8 pad-fix videos-fcontainer" style="border-right:20px solid #eef1f6;">

  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 v-container">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 v-insidec">

     <style>
       .fix-img{float:left; width:100%; color:#131313; font-weight:bold; font-size: 15px;}
       .v-tabmenu img{border:none !important; margin:0 25%;}
       a.thumbnail{min-height: 153px;}
       .v-tabmenu ul>li {
        padding: 5px;
      }
	  ul.chaps_list li{list-style:none; background:#f1f1f1;padding: 5px 10px;
    margin: 2px auto; list-style:none;}
ul.chaps_list li:nth-child(even){
background:#fbfbfb;
}
.title_vid_ref{
color:#227ec9 !important;
}
    </style>
    <!--      Tabbed menu starts            -->
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 v-tabmenu">
      <ul style="list-style:none;" class="row">
        <li class="pull-left col-xs-2">
         <a direction="+1" class="quiztype thumbnail pull-left" formdest=".mainform" data-videotype="eclass" data-subjectid="76" data-chapterid="529" data-topicid="0" href="student-progress/learn-course/" data-view="tf">
          <!-- <img class="pull-left" src="http://midas.com.np/elearning/assets/images/quiz-tf.jpg" class=""> --> 
          <div class="c100 p0 small orange pro_box_mar">
            <span>0/0</span>
            <div class="slice">
              <div class="bar"></div>
              <div class="fill"></div>
            </div>
          </div>

          <span class="fix-img text-center">True or False</span>

        </a>
      </li>
      <li class="pull-left col-xs-2">
       <a direction="+1" class="quiztype thumbnail pull-left" formdest=".mainform" data-videotype="eclass" data-subjectid="76" data-chapterid="529" data-topicid="0" href="student-progress/learn-course/" data-view="mcq">
        <!-- <img class="pull-left" src="http://midas.com.np/elearning/assets/images/quiz-mcq.jpg" class=""> --> 
        <div class="c100 p0 small orange pro_box_mar">
          <span>0/0</span>
          <div class="slice">
            <div class="bar"></div>
            <div class="fill"></div>
          </div>
        </div>

        <span class="fix-img text-center">Multiple Choice Questions</span>

      </a>
    </li>
    <li class="pull-left col-xs-2">
     <a direction="+1" class="quiztype thumbnail pull-left" formdest=".mainform" data-videotype="eclass" data-subjectid="76" data-chapterid="529" data-topicid="0" href="student-progress/learn-course/" data-view="fitb">
      <!-- <img class="pull-left" src="http://midas.com.np/elearning/assets/images/quiz-fitb.jpg" class=""> --> 
      <div class="c100 p0 small orange pro_box_mar">
        <span>0/0</span>
        <div class="slice">
          <div class="bar"></div>
          <div class="fill"></div>
        </div>
      </div>

      <span class="fix-img text-center">Fill in the blanks</span>

    </a>
  </li>
  <li class="pull-left col-xs-2">
   <a direction="+1" class="quiztype thumbnail pull-left" formdest=".mainform" data-videotype="eclass" data-subjectid="76" data-chapterid="529" data-topicid="0" href="student-progress/learn-course/" data-view="qa">
    <!-- <img class="pull-left" src="http://midas.com.np/elearning/assets/images/quiz-qa.jpg" class=""> --> 
    <div class="c100 p0 small orange pro_box_mar">
      <span>0/0</span>
      <div class="slice">
        <div class="bar"></div>
        <div class="fill"></div>
      </div>
    </div>

    <span class="fix-img text-center">Questions and Answers</span>

  </a>
</li>
<li class="pull-left col-xs-2">
 <a direction="+1" class="quiztype thumbnail pull-left" formdest=".mainform" data-videotype="eclass" data-subjectid="76" data-chapterid="529" data-topicid="0" href="student-progress/learn-course/" data-view="qato">
  <!-- <img class="pull-left" src="http://midas.com.np/elearning/assets/images/quiz-qato.jpg" class=""> --> 
  <div class="c100 p0 small orange pro_box_mar">
    <span>0/0</span>
    <div class="slice">
      <div class="bar"></div>
      <div class="fill"></div>
    </div>
  </div>

  <span class="fix-img text-center">Teachers Online</span>

</a>
</li>
<li class="pull-left col-xs-2">
 <a direction="+1" class="quiztype thumbnail pull-left" formdest=".mainform" data-videotype="eclass" data-subjectid="76" data-chapterid="529" data-topicid="0" href="student-progress/learn-course/" data-view="help">

   <div class="c100 p0 small orange pro_box_mar">
    <span>0/0</span>
    <div class="slice">
      <div class="bar"></div>
      <div class="fill"></div>
    </div>
  </div>
  <span class="fix-img text-center">Ask Questions To Teachers Online</span>
</a>
</li>                
</ul>
</div>
<div class="clear"></div>
<div class="secondaryform">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 thumbnail">
<ul class="chaps_list">
<li class="pull-left" style="width:100%;">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="col-lg-4"><img src="http://midas.com.np/elearning/assets/images/you_tn1.jpg" class="img-responsive" /></div>
<div class="col-lg-8">
<h4 class="title_vid_ref">10 Amazing Science Experiments Compilation.</h4>
<h6 style="margin:2px 0;">Home Science</h6>
<h6  style="margin:2px 0;">9 months ago. 29,99,999 views</h6>
<p style="font-size:12px; margin-bottom:0;">Lorem ipsum is the soles dolerem which is lorem of the ipsum and ipsum is soles of the doleram solace.</p>
</div>
</div></li>
<li class="pull-left" style="width:100%;">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="col-lg-4"><img src="http://midas.com.np/elearning/assets/images/you_tn2.jpg" class="img-responsive" /></div>
<div class="col-lg-8">
<h4 class="title_vid_ref">10 Amazing Science Experiments Compilation.</h4>
<h6 style="margin:2px 0;">Home Science</h6>
<h6  style="margin:2px 0;">9 months ago. 29,99,999 views</h6>
<p style="font-size:12px; margin-bottom:0;">Lorem ipsum is the soles dolerem which is lorem of the ipsum and ipsum is soles of the doleram solace.</p>
</div>
</div></li>
<li class="pull-left" style="width:100%;">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="col-lg-4"><img src="http://midas.com.np/elearning/assets/images/you_tn1.jpg" class="img-responsive" /></div>
<div class="col-lg-8">
<h4 class="title_vid_ref">10 Amazing Science Experiments Compilation.</h4>
<h6 style="margin:2px 0;">Home Science</h6>
<h6  style="margin:2px 0;">9 months ago. 29,99,999 views</h6>
<p style="font-size:12px; margin-bottom:0;">Lorem ipsum is the soles dolerem which is lorem of the ipsum and ipsum is soles of the doleram solace.</p>
</div>
</div></li>
<li class="pull-left" style="width:100%;">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="col-lg-4"><img src="http://midas.com.np/elearning/assets/images/you_tn2.jpg" class="img-responsive" /></div>
<div class="col-lg-8">
<h4 class="title_vid_ref">10 Amazing Science Experiments Compilation.</h4>
<h6 style="margin:2px 0;">Home Science</h6>
<h6  style="margin:2px 0;">9 months ago. 29,99,999 views</h6>
<p style="font-size:12px; margin-bottom:0;">Lorem ipsum is the soles dolerem which is lorem of the ipsum and ipsum is soles of the doleram solace.</p>
</div>
</div></li>
<li class="pull-left" style="width:100%;">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="col-lg-4"><img src="http://midas.com.np/elearning/assets/images/you_tn1.jpg" class="img-responsive" /></div>
<div class="col-lg-8">
<h4 class="title_vid_ref">10 Amazing Science Experiments Compilation.</h4>
<h6 style="margin:2px 0;">Home Science</h6>
<h6  style="margin:2px 0;">9 months ago. 29,99,999 views</h6>
<p style="font-size:12px; margin-bottom:0;">Lorem ipsum is the soles dolerem which is lorem of the ipsum and ipsum is soles of the doleram solace.</p>
</div>
</div></li>
<li class="pull-left" style="width:100%;">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="col-lg-4"><img src="http://midas.com.np/elearning/assets/images/you_tn2.jpg" class="img-responsive" /></div>
<div class="col-lg-8">
<h4 class="title_vid_ref">10 Amazing Science Experiments Compilation.</h4>
<h6 style="margin:2px 0;">Home Science</h6>
<h6  style="margin:2px 0;">9 months ago. 29,99,999 views</h6>
<p style="font-size:12px; margin-bottom:0;">Lorem ipsum is the soles dolerem which is lorem of the ipsum and ipsum is soles of the doleram solace.</p>
</div>
</div></li>

</ul>
</div>
</div>
</div>
</div>


</div>
<?php $this->load->view('sidebar');?></div>