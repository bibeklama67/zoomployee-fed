<style>
h5.bread{background:#eef1f6; font-weight:bold; padding:10px 5px; margin:2px 5px; color:#418755 !important;}
.progressbar{padding:0;}
.bread_wrap{border-bottom:1px solid #dfe4e8 !important;}
h5 span.active{color:#526b81;}
ul.chaps_list li{list-style:none; background:#f1f1f1;padding: 5px 10px;
    margin: 2px auto;}
ul.chaps_list li:nth-child(even){
background:#fbfbfb;
}
.progress{margin-top:5px;}
.mar-top-10{margin-top:10px;}
img.ads{
border:1px solid #d5d5d5; margin:0 auto;
}
span.pro_per{color:#d57656;}
.all_chap{
background:#f2f29c;
padding:20px;
text-align:center;
}

</style>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pad-fix bread_wrap">
<h5 class="bread">GRADE8 > SCIENCE & ENVIRONMENT > <span class="active">MEASUREMENT</span></h5>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin:10px 0;">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 thumbnail" style="margin-bottom:0; min-height:465px">
<div class="all_chap col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="col-lg-4 col-md-4 hidden-xs hidden-sm"></div>
<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 text-center"><h2 style="margin-top:13px; font-weight:bold;">Answer Sheet</h2></div>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 text-left">
<div class="col-lg-12">
<strong>No. of chapters: 24</strong>
</div>
<div class="col-lg-12">
<strong>Full Marks: 100</strong>
</div>
<div class="col-lg-12">
<strong style="color:#676568">Practice on: 2017-03-07</strong>
</div>
</div>
</div>
<div class="clearfix"></div>
</div>
</div>