<style>
	h5.bread{background:#eef1f6; font-weight:bold; padding:10px 5px; margin:2px 5px; color:#536780;}
	.progressbar{padding:0;}
	.bread_wrap{border-bottom:1px solid #dfe4e8 !important;}
	.contextbox{background:#c8c8fe; border-radius:4px; padding:5px;}
	.wrap-contbox{padding:10px; border-right:1px solid #dfe4e8; background:#fff;}
	.wrap_mid_cont{background:#eef1f6;}
	.wrap-right-contbox{padding:10px; border-left:1px solid #dfe4e8; background:#fff; margin-left:10px; min-width:393px; min-height:430px;}
	.right-contbox{border:1px solid #dcdcdc; padding:10px;}
	.top-bar-que{padding:5px; border-bottom:1px solid #cfcfcf;}
	.btn-que{background:#f6f7f9;border:1px solid #cfd0d4; border-radius:8px;padding: 3px 12px;}
	.rightcont-body{padding:10px;    min-height: 281px;}
	.rightcont-footer{background:#f5f7ec;}
	.btn_send{background:#/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#fffffe+0,fffffa+3,fdfef8+13,f7f7f5+16,f6f7eb+23,f6f8ed+26,f3f3ed+29,efefe8+32,eeeee6+35,f0f0e7+42,efeee1+45,ede9e4+48,e7e7de+55,e8e8e0+68,dfe1d9+74,dfddd6+77,d4dbce+84,d6d6cc+87,d5d7ca+90,d6d7cd+94,d2d2c9+100 */
		background: rgb(255,255,254); /* Old browsers */
		background: -moz-linear-gradient(top,  rgba(255,255,254,1) 0%, rgba(255,255,250,1) 3%, rgba(253,254,248,1) 13%, rgba(247,247,245,1) 16%, rgba(246,247,235,1) 23%, rgba(246,248,237,1) 26%, rgba(243,243,237,1) 29%, rgba(239,239,232,1) 32%, rgba(238,238,230,1) 35%, rgba(240,240,231,1) 42%, rgba(239,238,225,1) 45%, rgba(237,233,228,1) 48%, rgba(231,231,222,1) 55%, rgba(232,232,224,1) 68%, rgba(223,225,217,1) 74%, rgba(223,221,214,1) 77%, rgba(212,219,206,1) 84%, rgba(214,214,204,1) 87%, rgba(213,215,202,1) 90%, rgba(214,215,205,1) 94%, rgba(210,210,201,1) 100%); /* FF3.6-15 */
		background: -webkit-linear-gradient(top,  rgba(255,255,254,1) 0%,rgba(255,255,250,1) 3%,rgba(253,254,248,1) 13%,rgba(247,247,245,1) 16%,rgba(246,247,235,1) 23%,rgba(246,248,237,1) 26%,rgba(243,243,237,1) 29%,rgba(239,239,232,1) 32%,rgba(238,238,230,1) 35%,rgba(240,240,231,1) 42%,rgba(239,238,225,1) 45%,rgba(237,233,228,1) 48%,rgba(231,231,222,1) 55%,rgba(232,232,224,1) 68%,rgba(223,225,217,1) 74%,rgba(223,221,214,1) 77%,rgba(212,219,206,1) 84%,rgba(214,214,204,1) 87%,rgba(213,215,202,1) 90%,rgba(214,215,205,1) 94%,rgba(210,210,201,1) 100%); /* Chrome10-25,Safari5.1-6 */
		background: linear-gradient(to bottom,  rgba(255,255,254,1) 0%,rgba(255,255,250,1) 3%,rgba(253,254,248,1) 13%,rgba(247,247,245,1) 16%,rgba(246,247,235,1) 23%,rgba(246,248,237,1) 26%,rgba(243,243,237,1) 29%,rgba(239,239,232,1) 32%,rgba(238,238,230,1) 35%,rgba(240,240,231,1) 42%,rgba(239,238,225,1) 45%,rgba(237,233,228,1) 48%,rgba(231,231,222,1) 55%,rgba(232,232,224,1) 68%,rgba(223,225,217,1) 74%,rgba(223,221,214,1) 77%,rgba(212,219,206,1) 84%,rgba(214,214,204,1) 87%,rgba(213,215,202,1) 90%,rgba(214,215,205,1) 94%,rgba(210,210,201,1) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fffffe', endColorstr='#d2d2c9',GradientType=0 ); /* IE6-9 */
		border:1px solid #bfc2b7;
		border-radius:0;
		color:#5f4d5f;
		font-weight:bold;
	}
	.btn_upload{background:none; padding:2px;}
	.mar-10{margin:10px;}
	.wrap_mid_cont{border-bottom:1px solid #dfdfdf;}
	.videobox_wrap{margin-top:10px;}
	.videobox{ border:1px solid #e3e3e3; background:#efefef; border-top-left-radius:4px; border-top-right-radius:4px;}
	.videobox_body{background:#fff; padding:10px;}
	.video_link h5{color:#3a5ab1; font-weight:bold;}
</style>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pad-fix bread_wrap">
	<h5 class="bread">GRADE8 > SCIENCE</h5>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pad-fix wrap_mid_cont">
	<div class="pull-left wrap-contbox">
		<div class="contextbox"><img src="http://www.midas.com.np/images/youtube.jpg" /></div>
	</div>
	<div class="pull-left wrap-right-contbox">
		<div class="right-contbox text-right">
			<div class="top-bar-que">
				<button class="btn btn-que"><span class="glyphicon glyphicon-plus"></span>&nbsp;New Question</button>
			</div>
			<div class="rightcont-body">A quick brown fox jumps over the lazy dog</div>
			<div class="rightcont-footer">
				<button class="btn btn_upload pull-left mar-10"><img src="http://www.midas.com.np/images/img_upload.png" style="max-height:35px;" /></button>
				<textarea class="pull-left  mar-10" style="min-width:180px;"></textarea>
				<button class="btn btn_send pull-left  mar-10" style="margin-top:15px;">Send</button>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<div class="clearfix">
	</div>

</div>

<div class="videobox_wrap col-lg-12 col-md-12 col-sm-12 col-xs-12 pad-fix">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 videobox">
		<h5 style="font-weight:bold; color:#555555;">Other Videos of Velocity</h5>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 videobox_body thumbnail">
			<div class="col-lg-3 col-md-4 col-sm-3 col-xs-12">
				<a href="#" class="video_link"><img src="http://www.midas.com.np/images/you_tn.png" />
					<h5>Grade Three Scienc : Wild animals</h5>
				</a>
			</div>
			

		</div>
	</div>
</div>