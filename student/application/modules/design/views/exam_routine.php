<style>
.lt-top-block, .rt-block, .rt-btm-block{ background:#fff; border:1px solid #d5dee7;}
.mar-10-top{margin-top:10px;}
ul.ex_route_nav li{list-style:none; padding:5px 10px; border-bottom:1px solid #f5f6f8;}
ul.ex_route_nav li.active,ul.ex_route_nav li:hover{background:#f5f6f8; text-decoration:none;}
.btn-trans{background:none !important; border:none !important;}
h5.title_er{background:#eef1f6; margin:2px; padding:10px; color:#697a8a; font-weight:bold;}
.pad-fix{padding:0 !important;}
 ul.chaps_list li{list-style:none; background:#f1f1f1;padding: 5px 10px;
    margin: 2px auto; list-style:none;}
ul.chaps_list li:nth-child(even){
background:#fbfbfb;
}
.purple_border{border:2px solid #c8c8fe;}

</style>
<div class="col-lg-12 col-md-12 col-sm-12">
<div class="col-lg-3">
<div class="col-lg-12  lt-top-block">
<div class="col-lg-3"><img src="" class="img-responsive" /></div>
<div class="col-lg-9"><h5>Hi! Yash</h5><h6>Pupil</h6></div>
</div>
<div class="col-lg-12 rt-btm-block mar-10-top">
<ul class="ex_route_nav">
<a href="#"><li class="active">My Homework</li></a>
<a href="#"><li>My Attendance</li></a>
<a href="#"><li>My School Calendar</li></a>
<a href="#"><li>My Exam Routine</li></a>
<a href="#"><li>My Marks</li></a>
</ul>
</div>
</div>
<div class="col-lg-9">
<div class="col-lg-12  rt-block pad-fix">
<h5 class="title_er">My Exam Routine<span class="pull-right"><button class="btn-trans"><span class="glyphicon glyphicon-chevron-left"></span></button> &nbsp;FIRST UNIT TEST&nbsp;<button class="btn-trans"><span class="glyphicon glyphicon-chevron-right"></span></button>&nbsp;2017-03-08 AD</span></h5>
</div>
<div class="col-lg-12 rt-block" style="padding:15px;">
<div class="col-lg-12 purple_border pad_fix">
<ul class="chaps_list">
<li class="pull-left" style="width:100%;">
<div class="col-lg-12">
<div class="col-lg-4">
2017-03-04
</div>
<div class="col-lg-3">
12:30
</div>
<div class="col-lg-5">
Science & Environment
</div>
</div>

</li>
<li class="pull-left" style="width:100%;">
<div class="col-lg-12">
<div class="col-lg-4">
2017-03-04
</div>
<div class="col-lg-3">
12:30
</div>
<div class="col-lg-5">
Science & Environment
</div>
</div>

</li>
<li class="pull-left" style="width:100%;">
<div class="col-lg-12">
<div class="col-lg-4">
2017-03-04
</div>
<div class="col-lg-3">
12:30
</div>
<div class="col-lg-5">
Science & Environment
</div>
</div>

</li>
<li class="pull-left" style="width:100%;">
<div class="col-lg-12">
<div class="col-lg-4">
2017-03-04
</div>
<div class="col-lg-3">
12:30
</div>
<div class="col-lg-5">
Science & Environment
</div>
</div>

</li>
<li class="pull-left" style="width:100%;">
<div class="col-lg-12">
<div class="col-lg-4">
2017-03-04
</div>
<div class="col-lg-3">
12:30
</div>
<div class="col-lg-5">
Science & Environment
</div>
</div>

</li>
<li class="pull-left" style="width:100%;">
<div class="col-lg-12">
<div class="col-lg-4">
2017-03-04
</div>
<div class="col-lg-3">
12:30
</div>
<div class="col-lg-5">
Science & Environment
</div>
</div>

</li>
</ul>
</div>
</div>
</div>
</div>