<style>
	video {
		width: 100%    !important;
		height: auto   !important;
	}
	h5{margin: 0px 0px 5px 0px !important;}
	#arc-vdo{padding: 5px;}
	.vdo-caption h4,.vdo-caption h5{color: #365899 !important;padding: 0px 5px;}
	.vdo-caption h5{color: black !important;padding: 0px 5px;}
	.vdo-thumbs{padding: 5px;border-bottom: 1px solid rgba(245, 243, 243, 0.78);}
	.wrapthumbs-list.light-bg{
		background: rgba(250, 252, 255, 0.81);
		border: 1px solid rgba(221, 221, 221, 0.35);
	}
	.wrapthumbs-list{
		overflow-y: auto;
	}
	.wrapthumbs-list::-webkit-scrollbar {
		width: 6px;
	}
	.wrapthumbs-list::-webkit-scrollbar-track {
		background:transparent;
		border-radius: 20px;
	}
	.wrapthumbs-list::-webkit-scrollbar-thumb {
		background-color:#909090;
		border-radius: 20px;
	}
	.vdo-thumbs a h5{
		color: #365899 !important;
	}
	.rowvdos .pad5{
		padding: 5px;
	}
	.secvdo-list h5.bread.vdo-title{
		margin-top: 20px !important;
		margin-bottom: 10px !important;
	}
	.secvdo-list .vdo-caption h5{
		font-weight: bold;
		margin-top: 10px !important;
		color: black !important;
	}
</style>


<div id="arc-vdo" class="col-md-12 wrapper">
	<div class=" col-lg-8 col-sm-8 col-md-8 col-xs-12 pad-fix">
		<div class="play-vdo">
			<h5 class="bread vdo-title">
				<a href="javascript:;" data-classid="5" class="classbtn breadcrumb">Grade ONE<input type="hidden" id="homework_classid" value="5"></a><i class="fa fa-chevron-right"></i>
				<a href="javascript:;" class="subjectbtn breadcrumb" data-subjectname="SCIENCE AND ENVIRONMENT - ONE" data-subjectid="56" data-classname="ONE">SCIENCE AND ENVIRONMENT </a> <i class="fa fa-chevron-right"></i> 
				<span class="active"> Living  and non-living things</span>
			</h5>
			<video controls>
				<source src="http://vstore.midas.com.np/vstore/windows/(11)%20%E2%96%BA%20Planet%20Earth-%20Amazing%20nature%20scenery%20(1080p%20HD)%20-%20YouTube.MKV" type="video/mp4">
					<source src="movie.ogg" type="video/ogg">
						Your browser does not support the video tag.
					</video>
					<div class="vdo-caption">
						
						<div class="pull-left"><h4>support the video tag support the video tags upport the video tag</h4>
							<h5>support the video tag support the video tags upport the video tag</h5></div>
							<div class="pull-right">
								<a href="javascript:void(0)" onclick="javascript:genericSocialShare('http://www.facebook.com/sharer.php?u=http://www.midas.com.np/design/blogdetail')" ><img src="<?= base_url()?>assets/images/fb-sharebutton.png" width="30" height="30" class="img-responsive" id="fbsharebtn" style="margin-top: 15px;"></a></div>
								
							</div>
						</div>
						
						
					</div>
					<div class="col-lg-4 col-md-4 col-xs-12 col-sm-4 pad-fix">
						<div class="next-vdo">
							<div class="col-xs-12">
								<h5 class="bread vdo-title"> Playing Next	</h5>
								<div class="wrapthumbs-list light-bg">	
									<div class="vdo-thumbs">
										<a href="#">	<div class="col-xs-6 pad-fix">		
											<video class="" >
												<source src="http://vstore.midas.com.np/vstore/windows/(11)%20%E2%96%BA%20Planet%20Earth-%20Amazing%20nature%20scenery%20(1080p%20HD)%20-%20YouTube.MKV" type="video/mp4">			
													Your browser does not support the video tag.
												</video>
											</div></a>		
											<div class="vdo-caption col-xs-6 "><a href="#">
												<h5>
													support the video tag support the video tags upport the video tag
												</h5> </a>	
											</div> 
											<div class="clearfix"></div>
										</div>
										<div class="vdo-thumbs">
											<a href="#">	<div class="col-xs-6 pad-fix">		
												<video class="" >
													<source src="http://vstore.midas.com.np/vstore/windows/(11)%20%E2%96%BA%20Planet%20Earth-%20Amazing%20nature%20scenery%20(1080p%20HD)%20-%20YouTube.MKV" type="video/mp4">			
														Your browser does not support the video tag.
													</video>
												</div></a>		
												<div class="vdo-caption col-xs-6 "><a href="#">
													<h5>
														support the video tag support the video tags upport the video tag
													</h5> </a>	
												</div> 
												<div class="clearfix"></div>
											</div>
											<div class="vdo-thumbs">
												<a href="#">	<div class="col-xs-6 pad-fix">		
													<video class="" >
														<source src="http://vstore.midas.com.np/vstore/windows/(11)%20%E2%96%BA%20Planet%20Earth-%20Amazing%20nature%20scenery%20(1080p%20HD)%20-%20YouTube.MKV" type="video/mp4">			
															Your browser does not support the video tag.
														</video>
													</div></a>		
													<div class="vdo-caption col-xs-6 "><a href="#">
														<h5>
															support the video tag support the video tags upport the video tag
														</h5> </a>	
													</div> 
													<div class="clearfix"></div>
												</div>
												<div class="vdo-thumbs">
													<a href="#">	<div class="col-xs-6 pad-fix">		
														<video class="" >
															<source src="http://vstore.midas.com.np/vstore/windows/(11)%20%E2%96%BA%20Planet%20Earth-%20Amazing%20nature%20scenery%20(1080p%20HD)%20-%20YouTube.MKV" type="video/mp4">			
																Your browser does not support the video tag.
															</video>
														</div></a>		
														<div class="vdo-caption col-xs-6 "><a href="#">
															<h5>
																support the video tag support the video tags upport the video tag
															</h5> </a>	
														</div> 
														<div class="clearfix"></div>
													</div>
													<div class="vdo-thumbs">
														<a href="#">	<div class="col-xs-6 pad-fix">		
															<video class="" >
																<source src="http://vstore.midas.com.np/vstore/windows/(11)%20%E2%96%BA%20Planet%20Earth-%20Amazing%20nature%20scenery%20(1080p%20HD)%20-%20YouTube.MKV" type="video/mp4">			
																	Your browser does not support the video tag.
																</video>
															</div></a>		
															<div class="vdo-caption col-xs-6 "><a href="#">
																<h5>
																	support the video tag support the video tags upport the video tag
																</h5> </a>	
															</div> 
															<div class="clearfix"></div>
														</div>
													</div>	
												</div>
												
											</div>
										</div>
										<div class="clearfix"></div>
<!--
	<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pad-fix secvdo-list">
	<h5 class="bread vdo-title"> Other related videos</h5>
		<div class="rowvdos">
			<div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 pad5">
				<div class="vdo-thumbs">
					<a href="#">
					<div class="col-xs-12 pad-fix">		
					<video class="" >
					<source src="http://vstore.midas.com.np/vstore/windows/(11)%20%E2%96%BA%20Planet%20Earth-%20Amazing%20nature%20scenery%20(1080p%20HD)%20-%20YouTube.MKV" type="video/mp4">			
					Your browser does not support the video tag.
					</video>
				</div></a>		
					<div class="vdo-caption col-xs-12 pad-fix "><a href="#">
			<h5>
				support the video tag support the video tags upport the video tag
			</h5> </a>	
		</div> 
			<div class="clearfix"></div>
			</div>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 pad5">
				<div class="vdo-thumbs">
					<a href="#">
					<div class="col-xs-12 pad-fix">		
					<video class="" >
					<source src="http://vstore.midas.com.np/vstore/windows/(11)%20%E2%96%BA%20Planet%20Earth-%20Amazing%20nature%20scenery%20(1080p%20HD)%20-%20YouTube.MKV" type="video/mp4">			
					Your browser does not support the video tag.
					</video>
				</div></a>		
					<div class="vdo-caption col-xs-12 pad-fix "><a href="#">
			<h5>
				support the video tag support the video tags upport the video tag
			</h5> </a>	
		</div> 
			<div class="clearfix"></div>
			</div>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 pad5">
				<div class="vdo-thumbs">
					<a href="#">
					<div class="col-xs-12 pad-fix">		
					<video class="" >
					<source src="http://vstore.midas.com.np/vstore/windows/(11)%20%E2%96%BA%20Planet%20Earth-%20Amazing%20nature%20scenery%20(1080p%20HD)%20-%20YouTube.MKV" type="video/mp4">			
					Your browser does not support the video tag.
					</video>
				</div></a>		
					<div class="vdo-caption col-xs-12 pad-fix "><a href="#">
			<h5>
				support the video tag support the video tags upport the video tag
			</h5> </a>	
		</div> 
			<div class="clearfix"></div>
			</div>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 pad5">
				<div class="vdo-thumbs">
					<a href="#">
					<div class="col-xs-12 pad-fix">		
					<video class="" >
					<source src="http://vstore.midas.com.np/vstore/windows/(11)%20%E2%96%BA%20Planet%20Earth-%20Amazing%20nature%20scenery%20(1080p%20HD)%20-%20YouTube.MKV" type="video/mp4">			
					Your browser does not support the video tag.
					</video>
				</div></a>		
					<div class="vdo-caption col-xs-12 pad-fix "><a href="#">
			<h5>
				support the video tag support the video tags upport the video tag
			</h5> </a>	
		</div> 
			<div class="clearfix"></div>
			</div>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 pad5">
				<div class="vdo-thumbs">
					<a href="#">
					<div class="col-xs-12 pad-fix">		
					<video class="" >
					<source src="http://vstore.midas.com.np/vstore/windows/(11)%20%E2%96%BA%20Planet%20Earth-%20Amazing%20nature%20scenery%20(1080p%20HD)%20-%20YouTube.MKV" type="video/mp4">			
					Your browser does not support the video tag.
					</video>
				</div></a>		
					<div class="vdo-caption col-xs-12 pad-fix "><a href="#">
			<h5>
				support the video tag support the video tags upport the video tag
			</h5> </a>	
		</div> 
			<div class="clearfix"></div>
			</div>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 pad5">
				<div class="vdo-thumbs">
					<a href="#">
					<div class="col-xs-12 pad-fix">		
					<video class="" >
					<source src="http://vstore.midas.com.np/vstore/windows/(11)%20%E2%96%BA%20Planet%20Earth-%20Amazing%20nature%20scenery%20(1080p%20HD)%20-%20YouTube.MKV" type="video/mp4">			
					Your browser does not support the video tag.
					</video>
				</div></a>		
					<div class="vdo-caption col-xs-12 pad-fix "><a href="#">
			<h5>
				support the video tag support the video tags upport the video tag
			</h5> </a>	
		</div> 
			<div class="clearfix"></div>
			</div>
			</div>		
			</div>
	</div>
-->
</div>

<script>
	$(document).ready(function() {
		var divHeight = $('.play-vdo').height() + 60;	
		$('.wrapthumbs-list').css('height', divHeight+'px');
	});
</script>
<script type="text/javascript">
	function genericSocialShare(url){
		window.open(url,'sharer','toolbar=0,status=0,width=648,height=395');
		return true;
	}
</script>