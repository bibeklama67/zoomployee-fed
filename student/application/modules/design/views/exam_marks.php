<style>
.lt-top-block, .rt-block, .rt-btm-block{ background:#fff; border:1px solid #d5dee7;}
.mar-10-top{margin-top:10px;}
ul.ex_route_nav li{list-style:none; padding:5px 10px; border-bottom:1px solid #f5f6f8;}
ul.ex_route_nav li.active,ul.ex_route_nav li:hover{background:#f5f6f8; text-decoration:none;}
.btn-trans{background:none !important; border:none !important;}
h5.title_er{background:#eef1f6; margin:2px; padding:10px; color:#697a8a; font-weight:bold;}
.pad-fix{padding:0 !important;}
 ul.chaps_list li{list-style:none; background:#f1f1f1;padding: 5px 10px;
    margin: 2px auto; list-style:none; color:#848484;}
ul.chaps_list li:nth-child(even){
background:#fbfbfb;
}
.purple_border{border:2px solid #c8c8fe;}
table.table-purple tr td,table.table-purple tr th{ border:1px solid #c8c8fe;}
table.table-purple tr th{
color:#242555;
}
</style>
<div class="col-lg-12 col-md-12 col-sm-12" style="min-height:540px;">
<div class="col-lg-3" style="padding-left:0;">
<div class="col-lg-12  lt-top-block">
<div class="col-lg-3"><img src="" class="img-responsive" /></div>
<div class="col-lg-9"><h5>Hi! Yash</h5><h6>Pupil</h6></div>
</div>
<div class="col-lg-12 rt-btm-block mar-10-top" style="min-height:470px;">
<ul class="ex_route_nav">
<a href="#"><li class="active">My Homework</li></a>
<a href="#"><li>My Attendance</li></a>
<a href="#"><li>My School Calendar</li></a>
<a href="#"><li>My Exam Routine</li></a>
<a href="#"><li>My Marks</li></a>
</ul>
</div>
</div>
<div class="col-lg-9" style="padding-right:0;">
<div class="col-lg-12  rt-block pad-fix">
<h5 class="title_er">My Marks<span class="pull-right"><button class="btn-trans"><span class="glyphicon glyphicon-chevron-left"></span></button> &nbsp;FIRST UNIT TEST&nbsp;<button class="btn-trans"><span class="glyphicon glyphicon-chevron-right"></span></button></span></h5>
</div>
<div class="col-lg-12 rt-block" style="padding:15px; min-height:499px;">
<div class="col-lg-12 purple_border pad_fix">
<table class="table table-bordered table-purple table-striped">
<tr><td>Subject</td><td>Theory</td><td>Particle</td></tr>
<tr><td>Science & Environment</td><td>45/75</td><td>20/25</td></tr>
<tr><td>Science & Environment</td><td>45/75</td><td>20/25</td></tr>
<tr><td>Science & Environment</td><td>45/75</td><td>20/25</td></tr>
<tr><td>Science & Environment</td><td>45/75</td><td>20/25</td></tr>
<tr><td>Science & Environment</td><td>45/75</td><td>20/25</td></tr>
</table>
</div>
</div>
</div>
</div>