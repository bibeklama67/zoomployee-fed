<style>
h5.bread{background:#eef1f6; font-weight:bold; padding:10px 5px; margin:2px 5px; color:#418755 !important;}
.progressbar{padding:0;}
.bread_wrap{border-bottom:1px solid #dfe4e8 !important;}
h5 span.active{color:#526b81;}
ul.prac_list li{list-style:none; background:#f1f1f1;padding: 5px 10px;
    margin: 2px auto; width:100%}
ul.prac_list li:nth-child(even){
background:#fbfbfb;
}
.progress{margin-top:5px;}
.mar-top-10{margin-top:10px;}
img.ads{
border:1px solid #d5d5d5; margin:0 auto;
}
span.pro_per{color:#d57656;}
.btn-nqs{
display: inline-block;
    padding: 2px 10px;
    margin-bottom: 0;
    font-size: 12px;
    line-height: 1.428571;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid #cfd0d4;
    border-radius: 4px;
	background:#f7f7f9;
	color:#35629d;
	font-weight:bold;
}
.btn-prac{padding:0; background:none; border:none;}
.pad_tb_20{padding:20px 0;}
</style>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pad-fix bread_wrap">
<h5 class="bread">GRADE8 > SCIENCE & ENVIRONMENT > <span class="active">MEASUREMENT</span>
<span class="pull-right"><button class="btn btn-nqs"><span class="glyphicon glyphicon-plus">&nbsp;</span>New Question Set</button></span>
</h5>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pad-fix mar-top-10">
<ul class="prac_list">
<li class="pull-left">
    <div class="col-lg-12 pull-left">
    <div class="col-md-3 col-sm-3 col-xs-12 pad_tb_20"><strong>No of Chapters: 24</strong></div>
    <div class="col-md-3 col-sm-3 col-xs-12 pad_tb_20"><strong>Full Marks: 100</strong></div>
    <div class="col-md-3 col-sm-3 col-xs-12 pad_tb_20">Practice on 2017-02-03</div>
    <div class="col-md-3 col-sm-3 col-xs-12  text-right"><button class="btn btn-prac"><img src="http://www.midas.com.np/elearning/assets/images/brain.png" /></button><button class="btn btn-prac"><img src="http://www.midas.com.np/elearning/assets/images/key.png" /></button></div>
    </div>
</li>
<li class="pull-left">
    <div class="col-lg-12 pull-left">
    <div class="col-md-3 col-sm-3 col-xs-12 pad_tb_20"><strong>No of Chapters: 24</strong></div>
    <div class="col-md-3 col-sm-3 col-xs-12 pad_tb_20"><strong>Full Marks: 100</strong></div>
    <div class="col-md-3 col-sm-3 col-xs-12 pad_tb_20">Practice on 2017-02-03</div>
    <div class="col-md-3 col-sm-3 col-xs-12  text-right"><button class="btn btn-prac"><img src="http://www.midas.com.np/elearning/assets/images/brain.png" /></button><button class="btn btn-prac"><img src="http://www.midas.com.np/elearning/assets/images/key.png" /></button></div>
    </div>
</li>
<li class="pull-left">
    <div class="col-lg-12 pull-left">
    <div class="col-md-3 col-sm-3 col-xs-12 pad_tb_20"><strong>No of Chapters: 24</strong></div>
    <div class="col-md-3 col-sm-3 col-xs-12 pad_tb_20"><strong>Full Marks: 100</strong></div>
    <div class="col-md-3 col-sm-3 col-xs-12 pad_tb_20">Practice on 2017-02-03</div>
    <div class="col-md-3 col-sm-3 col-xs-12  text-right"><button class="btn btn-prac"><img src="http://www.midas.com.np/elearning/assets/images/brain.png" /></button><button class="btn btn-prac"><img src="http://www.midas.com.np/elearning/assets/images/key.png" /></button></div>
    </div>
</li>
<li class="pull-left">
    <div class="col-lg-12 pull-left">
    <div class="col-md-3 col-sm-3 col-xs-12 pad_tb_20"><strong>No of Chapters: 24</strong></div>
    <div class="col-md-3 col-sm-3 col-xs-12 pad_tb_20"><strong>Full Marks: 100</strong></div>
    <div class="col-md-3 col-sm-3 col-xs-12 pad_tb_20">Practice on 2017-02-03</div>
    <div class="col-md-3 col-sm-3 col-xs-12  text-right"><button class="btn btn-prac"><img src="http://www.midas.com.np/elearning/assets/images/brain.png" /></button><button class="btn btn-prac"><img src="http://www.midas.com.np/elearning/assets/images/key.png" /></button></div>
    </div>
</li>
<li class="pull-left">
    <div class="col-lg-12 pull-left">
    <div class="col-md-3 col-sm-3 col-xs-12 pad_tb_20"><strong>No of Chapters: 24</strong></div>
    <div class="col-md-3 col-sm-3 col-xs-12 pad_tb_20"><strong>Full Marks: 100</strong></div>
    <div class="col-md-3 col-sm-3 col-xs-12 pad_tb_20">Practice on 2017-02-03</div>
    <div class="col-md-3 col-sm-3 col-xs-12  text-right"><button class="btn btn-prac"><img src="http://www.midas.com.np/elearning/assets/images/brain.png" /></button><button class="btn btn-prac"><img src="http://www.midas.com.np/elearning/assets/images/key.png" /></button></div>
    </div>
</li>
<li class="pull-left">
    <div class="col-lg-12 pull-left">
    <div class="col-md-3 col-sm-3 col-xs-12 pad_tb_20"><strong>No of Chapters: 24</strong></div>
    <div class="col-md-3 col-sm-3 col-xs-12 pad_tb_20"><strong>Full Marks: 100</strong></div>
    <div class="col-md-3 col-sm-3 col-xs-12 pad_tb_20">Practice on 2017-02-03</div>
    <div class="col-md-3 col-sm-3 col-xs-12  text-right"><button class="btn btn-prac"><img src="http://www.midas.com.np/elearning/assets/images/brain.png" /></button><button class="btn btn-prac"><img src="http://www.midas.com.np/elearning/assets/images/key.png" /></button></div>
    </div>
</li>
<li class="pull-left">
    <div class="col-lg-12 pull-left">
    <div class="col-md-3 col-sm-3 col-xs-12 pad_tb_20"><strong>No of Chapters: 24</strong></div>
    <div class="col-md-3 col-sm-3 col-xs-12 pad_tb_20"><strong>Full Marks: 100</strong></div>
    <div class="col-md-3 col-sm-3 col-xs-12 pad_tb_20">Practice on 2017-02-03</div>
    <div class="col-md-3 col-sm-3 col-xs-12  text-right"><button class="btn btn-prac"><img src="http://www.midas.com.np/elearning/assets/images/brain.png" /></button><button class="btn btn-prac"><img src="http://www.midas.com.np/elearning/assets/images/key.png" /></button></div>
    </div>
</li>
<li class="pull-left">
    <div class="col-lg-12 pull-left">
    <div class="col-md-3 col-sm-3 col-xs-12 pad_tb_20"><strong>No of Chapters: 24</strong></div>
    <div class="col-md-3 col-sm-3 col-xs-12 pad_tb_20"><strong>Full Marks: 100</strong></div>
    <div class="col-md-3 col-sm-3 col-xs-12 pad_tb_20">Practice on 2017-02-03</div>
    <div class="col-md-3 col-sm-3 col-xs-12  text-right"><button class="btn btn-prac"><img src="http://www.midas.com.np/elearning/assets/images/brain.png" /></button><button class="btn btn-prac"><img src="http://www.midas.com.np/elearning/assets/images/key.png" /></button></div>
    </div>
</li>
</ul>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">
<nav aria-label="...">
  <ul class="pagination">
    <li class="disabled"><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
    <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
    <li><a href="#">2 <span class="sr-only">(current)</span></a></li>
    <li><a href="#">3 <span class="sr-only">(current)</span></a></li>
    <li><a href="#">4 <span class="sr-only">(current)</span></a></li>
    <li class="enabled"><a href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>
  </ul>
</nav>
</div>