<style>
h5.bread{background:#eef1f6; font-weight:bold; padding:10px 5px; margin:2px 5px; color:#418755 !important;}
.progressbar{padding:0;}
.bread_wrap{border-bottom:1px solid #dfe4e8 !important;}
h5 span.active{color:#526b81;}
ul.chaps_list li{list-style:none; background:#f1f1f1;padding: 5px 10px;
    margin: 2px auto;}
ul.chaps_list li:nth-child(even){
background:#fbfbfb;
}
.progress{margin-top:5px;}
.mar-top-10{margin-top:10px;}
img.ads{
border:1px solid #d5d5d5; margin:0 auto;
}
span.pro_per{color:#d57656;}
</style>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pad-fix bread_wrap">
<h5 class="bread">GRADE8 > SCIENCE & ENVIRONMENT > <span class="active">MEASUREMENT</span></h5>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pad-fix mar-top-10">
<div class="col-lg-8">
<ul class="chaps_list">
<li>
<div class="col-lg-12 pad-fix">
<div class="col-lg-4">
<h5>1. Measurement</h5>
</div>
<div class="col-lg-8">
<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 pad-fix">
<div class="progress">
  <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
    <span class="sr-only">60% Complete</span>
  </div>
</div>
</div>
<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
<span class="pro_per">60%</span>
</div>
</div>
</div>
<div class="clearfix"></div>
</li>
<li>
<div class="col-lg-12 pad-fix">
<div class="col-lg-4">
<h5>2. Force and Motion</h5>
</div>
<div class="col-lg-8">
<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 pad-fix">
<div class="progress">
  <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 45%;">
    <span class="sr-only">60% Complete</span>
  </div>
</div>
</div>
<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
<span class="pro_per">45%</span>
</div>
</div>
</div>
<div class="clearfix"></div>
</li>
<li>
<div class="col-lg-12 pad-fix">
<div class="col-lg-4">
<h5>1. Measurement</h5>
</div>
<div class="col-lg-8">
<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 pad-fix">
<div class="progress">
  <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
    <span class="sr-only">60% Complete</span>
  </div>
</div>
</div>
<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
<span class="pro_per">60%</span>
</div>
</div>
</div>
<div class="clearfix"></div>
</li>
<li>
<div class="col-lg-12 pad-fix">
<div class="col-lg-4">
<h5>2. Force and Motion</h5>
</div>
<div class="col-lg-8">
<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 pad-fix">
<div class="progress">
  <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 45%;">
    <span class="sr-only">60% Complete</span>
  </div>
</div>
</div>
<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
<span class="pro_per">45%</span>
</div>
</div>
</div>
<div class="clearfix"></div>
</li>
<li>
<div class="col-lg-12 pad-fix">
<div class="col-lg-4">
<h5>1. Measurement</h5>
</div>
<div class="col-lg-8">
<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 pad-fix">
<div class="progress">
  <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
    <span class="sr-only">60% Complete</span>
  </div>
</div>
</div>
<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
<span class="pro_per">60%</span>
</div>
</div>
</div>
<div class="clearfix"></div>
</li>
<li>
<div class="col-lg-12 pad-fix">
<div class="col-lg-4">
<h5>2. Force and Motion</h5>
</div>
<div class="col-lg-8">
<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 pad-fix">
<div class="progress">
  <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 45%;">
    <span class="sr-only">60% Complete</span>
  </div>
</div>
</div>
<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
<span class="pro_per">45%</span>
</div>
</div>
</div>
<div class="clearfix"></div>
</li>
</ul>
</div>
<?php $this->load->view('sidebar')?>

</div>