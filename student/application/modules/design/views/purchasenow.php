<style>

</style>
<div class="wrap-packagev" id="package-wrap">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pad-fix bread_wrap">
    <h5 class="bread">
     Please select a package of Grade N
    </h5>
  </div>
  <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
  	<div class="wrap-package">
  		<div class="package-header">
  			
  			<div class="package-ttl">
  			
  						<p><strong> Midas App - All Subject</strong></p>
  					
  				
  						
  									
  			</div>
  		</div>
  		<div class="clearfix"></div>
  		<div class="package-body">
			<div class="table-responsive">
		  <table class="table table-striped">
			
			<tbody>
			  <tr>
				<td>SCIENCE AND ENVIRONMENT<span></span></td>
				<td class="color-org">(MiDas eClass Included)</td>
			  </tr>
			   <tr>
				<td>SCIENCE AND ENVIRONMENT<span></span></td>
				<td class="color-org">(MiDas eClass Included)</td>
			  </tr>
			   <tr>
				<td>SCIENCE AND ENVIRONMENT<span></span></td>
				<td class="color-org">(MiDas eClass Included)</td>
			  </tr>
			  
			  
			</tbody>
		  </table>
		</div>
  		</div>
  		<div class="clearfix"></div>
  		<div class="package-footer">
  			<div class="wrap-btn text-center">
<!--  				<button type="submit" class="purchase-btn" data-toggle="modal" data-target="#way2pay"> Purchase</button>-->
 					<button type="submit" class="purchase-btn" data-toggle="collapse" data-target="#way2opt"> Proceed</button>
  			</div> 			
  			
  			<div id="way2opt" class="collapse">
  			<div class="wrap-opt ">
					<div class="wrap-duration">
						<p class="text-center"><strong>2 months</strong></p>
					</div>
					<div class="duration-price">
					<div class="pull-left">
						<p class="text-center pull-left">
						<strong>
							Your total cost for this package <br>
						Rs. 1500/-
						</strong></p>
					</div>
					<div class="pull-right" style="padding: 8px;">
						<button type="submit" class="purchase-btn" data-toggle="modal" data-target="#way2pay"> Proceed <i class="fa fa-angle-double-right" aria-hidden="true"></i></button>
					</div>
						<div class="clearfix"></div>
						
						
					</div>
				</div>
  			
				
				</div>
  			
  			<!-- Modal -->
				<div id="way2pay" class="modal fade" role="dialog">
				  <div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title text-center">o o o</h4>
					  </div>
					  <div class="modal-body">
						<div class="pur-reciept">
							<div class="package-ttl">  			
  								<p><strong> Midas App - All Subject</strong></p>				
  							</div>
  							<div class="table-responsive">
							  <table class="table table-striped">
								<tbody>
								  <tr>
									<td class="width100">Name<span></span></td>
									<td>Test Test Test</td>
								  </tr>
								   <tr>
									<td>Duration<span></span></td>
									<td >This Academic Year</td>
								  </tr>
								   <tr>
									<td>Mobile<span></span></td>
									<td >98564525811</td
								  </tr>
								  <tr>
									<td class="color-org">Price<span></span></td>
									<td class="color-org">Rs.1,500.00</td
								  </tr>

								</tbody>
							  </table>
						</div>
							
 							
  							
						</div>
						<h3 class="text-center payh3ttl">Make payments from our payment partners !</h3>
						<div class="payment-gate">
 								<div class="col-sm-4 col-xs-12 pad-5">
									<div class="paybtn ime">
										<a href="#">
											<img src="<?= base_url()?>assets/images/sm-ime.gif">
										</a>
 									</div>
 								</div>
 								<div class="col-sm-4 col-xs-12 pad-5">
									<div class="paybtn esewa">
										<a href="#">
											<img src="<?= base_url()?>assets/images/sm-esewa.gif">
										</a>
 									</div>
 								</div>
 								<div class="col-sm-4 col-xs-12 pad-5">
									<div class="paybtn npay">
										<a href="#">
												<img src="<?= base_url()?>assets/images/sm-npay.png">
										</a>
 									</div>
 								</div>
 							</div>
					  </div>
					  <div class="clearfix"></div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					  </div>
					</div>

				  </div>
				</div>
  		</div>
  	</div>
  </div>
  
</div>