<style>
	.wrap-popt .p-ttl h5{
		margin: 1px 0px;
	}
	.wrapp-image{
		min-height: 210px;
    background: #a5a8c4;
    padding: 35px 25px 20px;
	}
	.wrapp-image .inner-wrap{
		min-height: 155px;
		background: #7e82a5;
		border-radius: 5px;
		-ms-filter: "progid:DXImageTransform.Microsoft.Shadow(Strength=2, Direction=0, Color=#000000)";/*IE 8*/
-moz-box-shadow: 0 0 2px 1px rgba(0,0,0,0.2);/*FF 3.5+*/
-webkit-box-shadow: 0 0 2px 1px rgba(0,0,0,0.2);/*Saf3-4, Chrome, iOS 4.0.2-4.2, Android 2.3+*/
box-shadow: 0 0 2px 1px rgba(0,0,0,0.2);/* FF3.5+, Opera 9+, Saf1+, Chrome, IE10 */
filter: progid:DXImageTransform.Microsoft.Shadow(Strength=2, Direction=135, Color=#000000); /*IE 5.5-7*/
	}
	.row.abs{
/*		margin-top: -25px;*/
		margin-right: 0px !important;
		margin-left: 0px !important;
		position: relative;
    top: -25px;
	}
	.row.nabs{
		margin-right: 0px !important;
    margin-left: 0px !important;
    position: relative;
    top: -10px;
	}
	.row.abs .p-img img{
		-ms-filter: "progid:DXImageTransform.Microsoft.Shadow(Strength=2, Direction=0, Color=#000000)";/*IE 8*/
-moz-box-shadow: 0 0 2px 1px rgba(0,0,0,0.2);/*FF 3.5+*/
-webkit-box-shadow: 0 0 2px 1px rgba(0,0,0,0.2);/*Saf3-4, Chrome, iOS 4.0.2-4.2, Android 2.3+*/
box-shadow: 0 0 2px 1px rgba(0,0,0,0.2);/* FF3.5+, Opera 9+, Saf1+, Chrome, IE10 */
filter: progid:DXImageTransform.Microsoft.Shadow(Strength=2, Direction=135, Color=#000000); /*IE 5.5-7*/
		border-radius: 5px;
	}
	.row.nabs .p-img img{
		-ms-filter: "progid:DXImageTransform.Microsoft.Shadow(Strength=2, Direction=0, Color=#000000)";/*IE 8*/
-moz-box-shadow: 0 0 2px 1px rgba(0,0,0,0.2);/*FF 3.5+*/
-webkit-box-shadow: 0 0 2px 1px rgba(0,0,0,0.2);/*Saf3-4, Chrome, iOS 4.0.2-4.2, Android 2.3+*/
box-shadow: 0 0 2px 1px rgba(0,0,0,0.2);/* FF3.5+, Opera 9+, Saf1+, Chrome, IE10 */
filter: progid:DXImageTransform.Microsoft.Shadow(Strength=2, Direction=135, Color=#000000); /*IE 5.5-7*/
		
	}
</style>
<div class="wrap-packagev" id="package-wrap">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pad-fix bread_wrap">
		<h5 class="bread text-center">
    Payment Options
    </h5>
	
	</div>
	<div class="clearfix"></div>
	<div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
		<div class="wrap-popt">
			<div class="p-ttl">
				<h5 class="bread text-center">
					Cash on Delivery
				</h5>
			</div>
			<div class="p-img">
			<a href="#">
				<img src="<?= base_url()?>assets/images/payment-opt/home-delivery.png" class="hmd">
			</a>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
		<div class="wrap-popt">
			<div class="p-ttl">
				<h5 class="bread text-center">
   					 Nearby Shops
    			</h5>			
			</div>
			<div class="wrapp-image">
			<div class="inner-wrap">
			<div class="1-bar">
			<div class="row abs">
				<div class="col-md-4 col-sm-4 col-xs-4">
					<div class="p-img">	
						<a href="#">
							<img src="<?= base_url()?>assets/images/payment-opt/imepayay.png">
						</a>			
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4">
					<div class="p-img">
						<a href="#">
							<img src="<?= base_url()?>assets/images/payment-opt/esewapayay.png">
						</a>				
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4">
					<div class="p-img">	
						<a href="#">
							<img src="<?= base_url()?>assets/images/payment-opt/khaltipay.png">
						</a>			
					</div>
				</div>
			</div>
				
			</div>
			<div class="clearfix"></div>
			<div class="2-bar">
				<div class="row nabs">
				<div class="col-md-4 col-sm-4 col-xs-4">
					<div class="p-img">	
						<a href="#">
							<img src="<?= base_url()?>assets/images/payment-opt/stationary.png">
						</a>			
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4">
					<div class="p-img">
						<a href="#">
							<img src="<?= base_url()?>assets/images/payment-opt/mobileshops.png">
						</a>				
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4">
					<div class="p-img">	
						<a href="#">
							<img src="<?= base_url()?>assets/images/payment-opt/computer-shops.png">
						</a>			
					</div>
				</div>
			</div>
			</div>				
			</div>				
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
		<div class="wrap-popt">
			<div class="p-ttl">
				<h5 class="bread text-center">
    				Payment Partners
   				</h5>
			</div>
			<div class="p-img"></div>
		</div>
	</div>
	<div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
		<div class="wrap-popt">
			<div class="p-ttl">
				<h5 class="bread text-center">
					Banking Partner
				</h5>
			</div>
			<div class="p-img"></div>
		</div>
	</div>


</div>