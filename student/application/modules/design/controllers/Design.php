<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Design extends Front_controller {

	function __construct()
	{
		parent::__construct();
   $d=$this->config->item('default');
   $df=$this->config->item($d); 
   $df['js'][]='common/student_api';
   $df['js'][]='dashboard/dashboard';

   $this->config->set_item($d,$df);
   $this->load->model('dashboard_model','model');
   $this->load->model('frontend_model','frontmodel');

 }

 public function index()
 {
  $userid=$this->session->userdata('userid');
  $title=$this->input->post('title');
  $student_name=$this->session->userdata('student_name');

  $this->load->vars(array(
    'secondaryviewlink' =>    'teacheronline',
    'formview'          =>    'student_progress/progress/studenthome',
    'searchview'        =>    'templates/header/search',
    'student_name'      =>    $student_name
    ));

  $this->render_page();		
}	

public function to()
{
  $userid=$this->session->userdata('userid');
  $title=$this->input->post('title');
  $student_name=$this->session->userdata('student_name');

  $this->load->vars(array(
    'secondaryviewlink' =>    'landingpage',
    'formview'          =>    'student_progress/progress/studenthome',
    'searchview'        =>    'templates/header/search',
    'student_name'      =>    $student_name
    ));

  $this->render_page();   
} 


public function sidebar()
{
  $userid=$this->session->userdata('userid');
  $title=$this->input->post('title');
  $student_name=$this->session->userdata('student_name');

  $this->load->vars(array(
    'secondaryviewlink' =>    'sidebar',
    'formview'          =>    'student_progress/progress/studenthome',
    'searchview'        =>    'templates/header/search',
    'student_name'      =>    $student_name
    ));

  $this->render_page();   
} 

public function chapters()
{
  $userid=$this->session->userdata('userid');
  $title=$this->input->post('title');
  $student_name=$this->session->userdata('student_name');

  $this->load->vars(array(
    'secondaryviewlink' =>    'chapters',
    'formview'          =>    'student_progress/progress/studenthome',
    'searchview'        =>    'templates/header/search',
    'student_name'      =>    $student_name
    ));

  $this->render_page();   
} 

function signup($type=false)
{
  if($type=='parent')
    $this->load->view('signup/parent');
  else if($type=='student')
    $this->load->view('signup/student');  
  else
    $this->load->view('signup/main');
}

public function practice($slug=false)
{
  if($slug)
  {
    $this->load->vars(array(
      'secondaryviewlink' =>    'practicechapter',
      'formview'          =>    'student_progress/progress/studenthome',
      'searchview'        =>    'templates/header/search',
      'student_name'      =>    $student_name
      ));
    $this->render_page();   
  }
  else
  {
    $userid=$this->session->userdata('userid');
    $title=$this->input->post('title');
    $student_name=$this->session->userdata('student_name');

    $this->load->vars(array(
      'secondaryviewlink' =>    'practice',
      'formview'          =>    'student_progress/progress/studenthome',
      'searchview'        =>    'templates/header/search',
      'student_name'      =>    $student_name
      ));

    $this->render_page(); 
  }  
} 


public function detail($slug=false)
{
  if($slug=='question')
  {
    $this->load->vars(array(
      'secondaryviewlink' =>    'practiceques',
      'formview'          =>    'student_progress/progress/studenthome',
      'searchview'        =>    'templates/header/search',
      'student_name'      =>    $student_name
      ));
    $this->render_page();   
  }
  else
  {
    $userid=$this->session->userdata('userid');
    $title=$this->input->post('title');
    $student_name=$this->session->userdata('student_name');

    $this->load->vars(array(
      'secondaryviewlink' =>    'practiceans',
      'formview'          =>    'student_progress/progress/studenthome',
      'searchview'        =>    'templates/header/search',
      'student_name'      =>    $student_name
      ));

    $this->render_page(); 
  }  
} 

function references()
{
  $userid=$this->session->userdata('userid');
  $title=$this->input->post('title');
  $student_name=$this->session->userdata('student_name');

  $this->load->vars(array(
    'secondaryviewlink' =>    'references',
    'formview'          =>    'student_progress/progress/studenthome',
    'searchview'        =>    'templates/header/search',
    'student_name'      =>    $student_name
    ));

  $this->render_page(); 
}


function purchasenow()
{
  $userid=$this->session->userdata('userid');
  $title=$this->input->post('title');
  $student_name=$this->session->userdata('student_name');

  $this->load->vars(array(
    'secondaryviewlink' =>    'paymentoptions',
    'formview'          =>    'student_progress/progress/studenthome',
    'searchview'        =>    'templates/header/search',
    'student_name'      =>    $student_name
    ));

  $this->render_page(); 
}


function archiveclass(){

  $userid=$this->session->userdata('userid');
  $title=$this->input->post('title');
  $student_name=$this->session->userdata('student_name');

  $this->load->vars(array(
    'secondaryviewlink' =>    'archiveclass',
     'formview'          =>    'student_progress/progress/studenthome',
    'searchview'        =>    'templates/header/search',
    'student_name'      =>    $student_name
    ));

  $this->render_page(); 
  // $this->load->view('archiveclass');
}

function homework()
{
  $userid=$this->session->userdata('userid');
  $title=$this->input->post('title');
  $student_name=$this->session->userdata('student_name');

  $this->load->vars(array(
    'secondaryviewlink' =>    'homework',
    'formview'          =>    'student_progress/progress/studenthome',
    'searchview'        =>    'templates/header/search',
    'student_name'      =>    $student_name
    ));

  $this->render_page('myschool_view'); 
}

function calendar()
{
  $userid=$this->session->userdata('userid');
  $title=$this->input->post('title');
  $student_name=$this->session->userdata('student_name');

  $this->load->vars(array(
    'secondaryviewlink' =>    'calendar',
    'formview'          =>    'student_progress/progress/studenthome',
    'searchview'        =>    'templates/header/search',
    'student_name'      =>    $student_name
    ));

  $this->render_page('myschool_view'); 
}

function attendance()
{
  $userid=$this->session->userdata('userid');
  $title=$this->input->post('title');
  $student_name=$this->session->userdata('student_name');

  $this->load->vars(array(
    'secondaryviewlink' =>    'attendance',
    'formview'          =>    'student_progress/progress/studenthome',
    'searchview'        =>    'templates/header/search',
    'student_name'      =>    $student_name
    ));

  $this->render_page('myschool_view'); 
}

function exam_routine()
{
  $userid=$this->session->userdata('userid');
  $title=$this->input->post('title');
  $student_name=$this->session->userdata('student_name');

  $this->load->vars(array(
    'secondaryviewlink' =>    'exam_routine',
    'formview'          =>    'student_progress/progress/studenthome',
    'searchview'        =>    'templates/header/search',
    'student_name'      =>    $student_name
    ));

  $this->render_page('myschool_view'); 
}

function exam_marks()
{
  $userid=$this->session->userdata('userid');
  $title=$this->input->post('title');
  $student_name=$this->session->userdata('student_name');

  $this->load->vars(array(
    'secondaryviewlink' =>    'exam_marks',
    'formview'          =>    'student_progress/progress/studenthome',
    'searchview'        =>    'templates/header/search',
    'student_name'      =>    $student_name
    ));

  $this->render_page('myschool_view'); 
}

function my_creation()
{
  $userid=$this->session->userdata('userid');
  $title=$this->input->post('title');
  $student_name=$this->session->userdata('student_name');

  $this->load->vars(array(
    'secondaryviewlink' =>    'my_creation',
    'formview'          =>    'student_progress/progress/studenthome',
    'searchview'        =>    'templates/header/search',
    'student_name'      =>    $student_name
    ));

  $this->render_page('creation_view'); 
}

function forum()
{
  $userid=$this->session->userdata('userid');
  $title=$this->input->post('title');
  $student_name=$this->session->userdata('student_name');

  $this->load->vars(array(
    'secondaryviewlink' =>    'questions',
    'formview'          =>    'student_progress/progress/studenthome',
    'searchview'        =>    'templates/header/search',
    'student_name'      =>    $student_name
    ));

  $this->render_page('forum_view'); 
}

function listSubjects()
{
  $data = $this->input->post();
  $response = $data['response'];
  $title=$this->input->post('title');
  $this->load->vars(array(
    'response'          =>    $response,
    'title'             =>    $title

    ));
  $this->load->view('subjects', $response);
}


function listChapters()
{
  $data = $this->input->post();  
  $response = $data['response'];
  $title=$this->input->post('title');
  $this->load->vars(array(
    'response'          =>    $response,
    'title'             =>    $title,
    'subjectname'       =>    $data['subjectname']
    ));
  $this->load->view('chapters', $response);
}

function profile()
{
  $userid=$this->session->userdata('userid');
  $title=$this->input->post('title');
  $student_name=$this->session->userdata('student_name');

  $this->load->vars(array(
    'secondaryviewlink' =>    'myprofile',
    'formview'          =>    'student_progress/progress/studenthome',
    'searchview'        =>    'templates/header/search',
    'student_name'      =>    $student_name
    ));

  $this->render_page(); 
}

function blog()
{
  $this->load->view('blog'); 
}
function blogdetail()
{
  $this->load->view('blogdetail'); 
}
	function blogdetail01()
{
  $this->load->view('blogdetail01'); 
}
	function blogdetail02()
{
  $this->load->view('blogdetail02'); 
}
	
function livechapters()
{
  $this->load->view('livechapters');
}
	function email_template()
{
  $this->load->view('email-template');
}

function email_template1()
{
  $this->load->view('emailtemplate1');
}


	
function newmidasdesign()
{
  $this->load->view('newmidasdesign');
}
	
	function purchasenow1()
{
  $userid=$this->session->userdata('userid');
  $title=$this->input->post('title');
  $student_name=$this->session->userdata('student_name');

  $this->load->vars(array(
    'secondaryviewlink' =>    'purchasenow',
    'formview'          =>    'student_progress/progress/studenthome',
    'searchview'        =>    'templates/header/search',
    'student_name'      =>    $student_name
    ));

  $this->render_page(); 
}
	
		function newvdo_design()
{
  $userid=$this->session->userdata('userid');
  $title=$this->input->post('title');
  $student_name=$this->session->userdata('student_name');

  $this->load->vars(array(
    'secondaryviewlink' =>    'newvdo_design',
    'formview'          =>    'student_progress/progress/studenthome',
    'searchview'        =>    'templates/header/search',
    'student_name'      =>    $student_name
    ));

  $this->render_page(); 
}
	
}
