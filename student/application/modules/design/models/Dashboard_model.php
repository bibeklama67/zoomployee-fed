<?php 
 if(!defined('BASEPATH')) exit('direct access invalid');

class Dashboard_model extends CI_Model
{
		
	 function __construct()
    {
        parent::__construct();
        $db = $this->load->database('default', TRUE);
        // $this->otherdb = $this->load->database('otherdb',TRUE);
        // $this->smsdb =  $this->load->database('smsdb',TRUE);
        
    }

    function getprogress()
    {
      $total=array();
      $userid=$this->session->userdata('userid');
      $days=$this->input->post('days');
      if($days=='') $days='30';

        $sql1="select 
                * from
               (
                 select 
                   temp.*,newjoin.keypointtotal from
                    (
                         select count(progresskeypointid) as keypointviewed,chapterid,subjectid  from (select progresskeypointid,chapterid,subjectid from el_progress_keypoint where userid='$userid' and dataposteddatetime > CURRENT_TIMESTAMP - INTERVAL '$days year'   group by subjectid,chapterid,progresskeypointid order by dataposteddatetime desc) as tem group by subjectid,chapterid
                    ) as temp
                   join 
                   (
                      select count(keypointcontentid) as keypointtotal ,chapterid,subjectid from el_content_keypoint group by chapterid,subjectid
                   ) as newjoin 
                          on (temp.chapterid=newjoin.chapterid and temp.subjectid= newjoin.subjectid)
                ) as newtab
                join el_subject s on (s.subjectid=newtab.subjectid)
                join el_chapter c on (c.chapterid=newtab.chapterid)
                join el_class cl on (cl.classid=s.classid)";

       $sql2="select 
                * from
               (
                 select 
                   temp.*,newjoin.exercisetotal from
                    (
                         select count(progressexerciseid) as exerciseviewed,chapterid,subjectid  from (select progressexerciseid,chapterid,subjectid from el_progress_exercise where userid='$userid' and dataposteddatetime > CURRENT_TIMESTAMP - INTERVAL '$days days'   group by subjectid,chapterid,progressexerciseid order by dataposteddatetime desc) as tem group by subjectid,chapterid
                    ) as temp
                   join 
                   (
                      select count(exercisecontentid) as exercisetotal ,chapterid,subjectid from el_content_exercise group by chapterid,subjectid
                   ) as newjoin 
                          on (temp.chapterid=newjoin.chapterid and temp.subjectid= newjoin.subjectid)
                ) as newtab
                join el_subject s on (s.subjectid=newtab.subjectid)
                join el_chapter c on (c.chapterid=newtab.chapterid)
                join el_class cl on (cl.classid=s.classid)";

          $sql3="select 
                * from
               (
                 select 
                   temp.*,newjoin.videototal from
                    (
             select sum(viewlength) as videoviewed,chapterid,subjectid  from (select progressvideoid,chapterid,viewlength,subjectid from el_progress_video where userid='$userid' and dataposteddatetime > CURRENT_TIMESTAMP - INTERVAL '$days days'   group by subjectid,chapterid,progressvideoid order by dataposteddatetime desc) as tem group by subjectid,chapterid
                    ) as temp
                   join 
                   (
                      select sum(videolength) as videototal ,chapterid,subjectid from el_content_video group by chapterid,subjectid
                   ) as newjoin 
                          on (temp.chapterid=newjoin.chapterid and temp.subjectid= newjoin.subjectid)
                ) as newtab
                join el_subject s on (s.subjectid=newtab.subjectid)
                join el_chapter c on (c.chapterid=newtab.chapterid)
                join el_class cl on (cl.classid=s.classid)";

          $sql4="select 
                * from
               (
                 select 
                   temp.*,newjoin.quiztotal from
                    (
              select sum(markstot) as quizviewed,chapterid,subjectid  from (
               select cq.chapterid,cq.subjectid,
              case 
              when pq.attemptnumber=1 then cq.marks
              when pq.attemptnumber=2 then cq.marks/2
              else 0
              end as markstot
              from el_progress_quiz pq 
              join el_content_quiz cq on (pq.quizcontentid=cq.quizcontentid)
              where pq.userid='$userid' and pq.iscorrect='Y' and pq.dataposteddatetime > CURRENT_TIMESTAMP - INTERVAL '$days days' 
              ) as tem group by subjectid,chapterid
                    ) as temp
                   join 
                   (
                      select sum(marks) as quiztotal ,chapterid,subjectid from el_content_quiz group by chapterid,subjectid
                   ) as newjoin 
                          on (temp.chapterid=newjoin.chapterid and temp.subjectid= newjoin.subjectid)
                ) as newtab
                join el_subject s on (s.subjectid=newtab.subjectid)
                join el_chapter c on (c.chapterid=newtab.chapterid)
                join el_class cl on (cl.classid=s.classid)
;";
         $progressekeypoint= $this->db->query($sql1);
         $progressexercise= $this->db->query($sql2);
         $progressvideo= $this->db->query($sql3);
         $progressquiz= $this->db->query($sql4);

         $exercisearray =   $progressexercise->result('array');
         $keypointarray =   $progressekeypoint->result('array');
         $videoarray    =   $progressvideo->result('array');
         $quizarray     =   $progressquiz->result('array');
        
            foreach ($exercisearray as $key => $value) {
              $total[$value['alt_name'].',/'.$value['subjectname'].',/'.$value['chaptername'].',/'.$value['subjectid']]['exercise']=$value;
           }
           foreach ($keypointarray as $key => $value) {
              $total[$value['alt_name'].',/'.$value['subjectname'].',/'.$value['chaptername'].',/'.$value['subjectid']]['keypoint']=$value;
           }
            foreach ($videoarray as $key => $value) {
              $total[$value['alt_name'].',/'.$value['subjectname'].',/'.$value['chaptername'].',/'.$value['subjectid']]['video']=$value;
           }
            foreach ($quizarray as $key => $value) {
              $total[$value['alt_name'].',/'.$value['subjectname'].',/'.$value['chaptername'].',/'.$value['subjectid']]['quiz']=$value;
           }
         
          
        
         return $total;
 
    }

	// public function getclasswisesubject()
 //    {
 //    	          $this->db->SELECT('c.classid,c.classname,os.subjectname');
 //                $this->db->from('sch_class c');
 //                $this->db->join('sch_orgsubject os','c.classid=os.classid');

 //                $this->db->order_by('c.classid', 'ASC');
 //          //       $this->db->order_by('midas_products.subject', 'desc');
 //                $query= $this->db->get();
            
 //        		    return $query->result('array');
                
    	 
 //    }
    public function getproductbyremarks($slug=false)
    {
          try
          {
             if(!$slug) throw new exception('Error in data parameter'); 
                $remarks=explode('-',$slug);
              
                $data=implode(' ',$remarks);
                $this->db->limit(1);
                $this->db->select('*');
                $this->db->from('midas_products');
                $this->db->where('lower(remarks)',trim($data));
                $this->db->where('isfullpackage','Y');
                $query= $this->db->get();
       
               return ( $query->result('array'));
          }
      catch (Exception $e)
      {
            echo $e->getMessage();
      }
           
              
      }
        public function getproductbyclass($class)
       {
       
          try
          {
             if(empty($class)) throw new exception('Error in data parameter'); 
               
                
                $this->db->select('*');
                $this->db->from('midas_products');
                $this->db->where_in('class',$class);
                $this->db->where('isfullpackage','Y');
                $this->db->order_by('class','asc');
                $query= $this->db->get();
       
               return ( $query->result('array'));
          }
        catch (Exception $e)
        {
              echo $e->getMessage();
        }
           
              
      }

   

    public function checkdata($mobileno)
    {
       try
          {
             
             if(!($mobileno)) throw new exception('Error in data parameter'); 
                $this->otherdb->limit('1');
                $this->otherdb->select('*');
                $this->otherdb->from('productdownloadusers');
                $this->otherdb->where('mobilenumber',$mobileno);
                
                $querydata= $this->otherdb->get();
                // print_r($querydata->result('array'));exit;
                 return $querydata->result('array');

          }
      catch (Exception $e)
      {
            echo $e->getMessage();
      }
    }
     public function insertdetail($table,$data)
    {
      try
      {

          if((trim($table)=='')) throw new Exception("No table found ", 1);
          if(empty($data)) throw new Exception("Empty array found", 1);
      
          $this->otherdb->insert($table,$data); 
           if($this->otherdb->affected_rows())
               {
                return true;
               }
               else return false;

      }
     catch(Exception $e )
     {
        echo $e->getMessage();
     }
        
    }
    public function updatedetail($table,$data,$colname,$key)
    {
     try
      {

         if((trim($key)=='')) throw new Exception("No Specific key found ", 1);
              
               $this->otherdb->where($colname, $key);
               $this->otherdb->update($table, $data); 
               if($this->otherdb->affected_rows())
               {
                return true;
               }
               else return false;
            
     }
     catch(Exception $e )
     {
        echo $e->getMessage();
     }
  }

     public function checksms($mobileno)
    {
       try
        {

           if((trim($mobileno)=='')) throw new Exception("Mobile Number npt found ", 1);
                
                 $query=$this->smsdb->query("SELECT COUNT(*) as N FROM SMS_OUT 
                                                        WHERE MOBILE_NUMBER = '$mobileno' AND TO_DATE(REC_DATE) = TO_DATE(SYSDATE)");
                
                    // $querydata= $this->smsdb->result('array');
               
                 return $query->result('array');
             
       }
       catch(Exception $e )
       {
          echo $e->getMessage();
       }
  }

    public function getid()
    {
           
                 $query=$this->smsdb->query("SELECT NVL(MAX(ID),0)+1 as SMSOUTID FROM SMS_OUT");
                 // $querydata= $this->smsdb->result('array');
                return $query->result('array');
             
   }

   public function sendsms($data)
   {  

        // print_r($data);exit;
       return $this->smsdb->insert('SMS_OUT',$data); 

   }
   public function getorderid()
   {
      $this->otherdb->select('max("MIDASECLASSORDERID")');
      $this->otherdb->from("MIDASECLASSORDER");
      $data= $this->otherdb->get();
      return $data->result('array');

   }
   public function checkvalidation($data)
   {
       $this->otherdb->select('count("MIDASECLASSORDERID")');
       $this->otherdb->from("MIDASECLASSORDER");
       $this->otherdb->where($data);
       $data= $this->otherdb->get();
       return $data->result('array');
   }

   public function updateuser($userid)
   {
    $this->otherdb->set('createddatetime',date('Y-m-d H:i:s'));
    $this->otherdb->where('productdownloaduserid',$userid);
    $this->otherdb->update('productdownloadusers');
    if($this->otherdb->affected_rows())
    {
      return true;
    }
    else
    {
      return false;
    }
   }


   public function checkdownloaddetail($data)
   {
    try
    {
      if(empty($data)) throw new Exception("No data for checking", 1);
      $this->otherdb->select('productdownloadusersdetailid');
      $this->otherdb->from('productdownloadusersdetail');
      $this->otherdb->where($data);
      $data= $this->otherdb->get();
      $id= $data->result('array');
       if($this->otherdb->affected_rows())
    {
      return $id;
    }
    else
    {
      return false;
    }
      
    }
     catch(Exception $e )
       {
          echo $e->getMessage();
       }
   }
    public function insertdetails($table,$data)
    {
      try
      {

          if((trim($table)=='')) throw new Exception("No table found ", 1);
          if(empty($data)) throw new Exception("Empty array found", 1);
      
          $this->otherdb->insert($table,$data); 
           if($this->otherdb->affected_rows())
               {
                return $this->otherdb->insert_id();
               }
               else return false;

      }
     catch(Exception $e )
     {
        echo $e->getMessage();
     }
        
    }
}

