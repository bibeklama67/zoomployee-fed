<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forum extends Front_controller {

	function __construct()
	{
    
		parent::__construct();
   $d=$this->config->item('default');
   $df=$this->config->item($d); 
   $df['js'][]='student_progress/student_progress';

   $df['js'][]='common/student_api';
   $df['js'][]='dashboard/dashboard';
   $df['js'][]='ckeditor/ckeditor';
   // $df['js'][]='forum/nepali_unicode';
   $this->config->set_item($d,$df);
   // print_r($this->config->item($d));exit;
   
   $this->userid=$this->session->userdata('userid');
 
   
   // $this->load->model('dashboard_model','model');
   $this->userid=$this->session->userdata('user_id');
   $this->load->model('frontend_model','frontmodel');
   $this->load->helper('elearning_helper');
   //ini_set('error_reporting', E_ALL);
 }

 function homework()
 {
  $data = $this->input->post();
  $this->session->set_userdata('quizActivity', 'homeworklive');

  $this->load->vars(array(
    'data' => $data,
    'secondaryviewlink' => 'homework',
    ));
  $this->load->view('homework');   
} 

function webcam(){
    $this->load->view('webcam');
  }
  function saveimage(){
    $this->load->view('saveimage');
  }

function mainhomeworkhelp()
 {

  $data = $this->input->post();
  $this->session->set_userdata('quizActivity', 'homeworklive');
  //echo '<pre>'; var_dump($data); echo '</pre>';exit;
  $this->load->vars(array(
    'data' => $data['data'],
    'classid'         =>    $data['classid'],    
    'subjectid'         =>    $data['subjectid'],    
    'classname'         =>    $data['classname'],
    'subjectname'       =>    $data['subjectname'],
    'chaptername'       =>    $data['chaptername'],
    'secondaryviewlink' => 'homework',
    'filterkey'         =>  $data['filter'],
    'userid'=>$this->userid,
    ));
  $this->load->view('homework_wrapper');   
}

function lasthomeworkhelp()
 {
  $data = $this->input->post();
  $datas = $data['data'];
  $response = $datas['response'];
 //print_r($data);
  $this->session->set_userdata('quizActivity', 'homeworklive');
  //echo '<pre>'; var_dump($data); echo '</pre>';exit;
  $this->load->vars(array(
    'data' => $data['data'],
     'response'=>$response,
    'message'=>$data['message'],
    'classid'         =>    $data['classid'],    
    'subjectid'         =>    $data['subjectid'],    
    'classname'         =>    $data['classname'],
    'subjectname'       =>    $data['subjectname'],
    'chaptername'       =>    $data['chaptername'],
    'secondaryviewlink' => 'homework',
    'filterkey'         =>  $data['filter'],
    'frompagination'    =>  true,
    'source'=>@$data['source']

    ));
  $this->load->view('forum_posts');   
} 

function lasthomework()
 {
  $data = $this->input->post();
  //print_r($data);
  //var_dump($data);exit;
  $this->session->set_userdata('quizActivity', 'homeworklive');

  $this->load->vars(array(
    'data' => $data,
     'response'=>$data['response'],
    'message'=>$data['message'],
    'secondaryviewlink' => 'homework',
    'filterkey'         =>  $data['filter'],
    ));
  $this->load->view('forum_posts');   
}


function answer($questionid, $rating=0)
{
  $data = $this->input->post();
  $this->session->set_userdata('quizActivity', 'homeworklive');

  $this->load->vars(array('questionid'=>$questionid,'data' => $data, 'rating'=>$rating));
  $this->load->view('forum_answers');   
}

function newquestion()
{
  $data = $this->input->post();
  $this->session->set_userdata('quizActivity', 'homeworklive');

  $this->load->vars(array(
    'response'=>array($data['response']),
    ));
  $this->load->view('forum_posts');   
}

function newanswer()
{
 $data = $this->input->post();
 $this->session->set_userdata('quizActivity', 'homeworklive');
$row = $data['question'];
$question_view = '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6"><div class="col-sm-6 col-xs-6 pad-fix" ><p class="pull-right" style="margin-bottom: 0px;text-align: center;"><span class="font12">Average Rating</span><br><span class=" "><big class="mi44">'.round($row['averagerating'], 2).' <span class="glyphicon glyphicon-star myy"></span></big></span></p></div><div class="col-sm-6 col-xs-6 pad-fix "<p class="pull-right"style="margin-bottom: 0px;text-align: center;"><span class="font12">Total Ratings</span><br><span class=""><big>'.$row['totalrating'].'</big></span></p></div></div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 pad-fix" style="margin-top: -5px;"><div class="col-xs-6"><a href="javascript:;"  class="forum-actions" style="margin-left:10px; float:right;"><i class="fa fa-eye"></i><br> <span style="color:black;"><big>'.($row['views']>0?$row['views']:'0').'</big></span></a></div><div class="col-xs-6 "><a href="javascript:;"  class="forum-actions" style="margin-left:10px;float:left;"><i class="fa fa-comments"></i><br> <span style="color:black;"><big>'. ($row['comments']>0?$row['comments']:'0').'</big></span></a></div></div><div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 pad-fix"><div class="col-xs-4 indicator-bulb">';

    if($row['status'] == 'P'){

   $question_view .='<div class="col-xs-4 indicator-bulb"><i class="fa fa-circle que-pending" aria-hidden="true"></i></div><div class="col-xs-8 del-time"><i class="fa fa-times pull-right" aria-hidden="true"></i></div>';
    }else{

    $question_view .= '<div class="col-xs-4 indicator-bulb"><i class="fa fa-circle" aria-hidden="true"></i></div><div class="col-xs-8 del-time">  </div>';
    }
  $question_view  .='</div></div>';
 //echo"<pre>"; var_dump($data['question']['rating']); echo '</pre>';
 $this->load->vars(array(
  'question_view'=>$question_view,
  'response'=>$data['response'],
  'rating'=>$data['question']['rating'],
  'responseType'=>$data['type'],
  'hasteacheranswer'=>$data['question']['hasteacheranswer'],
  'dataQuestion'=>$row
  ));
 //$this->load->view('new_answers');
 $this->load->view('new_answers_all');
}

function viewaskedby()
{
  $data = $this->input->post();
  $this->session->set_userdata('quizActivity', 'homeworklive');

  $this->load->vars(array(
    'response'=>$data['response'],
    ));
  $this->load->view('list_view');
}

function getForumQuesForm()
{
  $this->load->view('forum_newpost');
}
function searchkeyword()
 {
  $data = $this->input->post();
  $datas = $data['data'];
  if($data['issuggestion']=='false')
  $response = $datas['response'];
  else
  {
    if(isset($datas['response'][0]))
    $response = $datas['response'];
    else
    $response = array(0=>$datas['response']);


  }

   $this->load->vars(array(
    'data' => $data['data'],
     'response'=>$response,
    'message'=>$data['message'],
    'classid'         =>    $data['classid'],    
    'subjectid'         =>    $data['subjectid'],    
    'classname'         =>    $data['classname'],
    'subjectname'       =>    $data['subjectname'],
    'chaptername'       =>    $data['chaptername'],
    'filterkey'         =>  $data['filter'],
    'source'=>@$data['source']

    ));
  $this->load->view('forum_posts');   
} 

}
