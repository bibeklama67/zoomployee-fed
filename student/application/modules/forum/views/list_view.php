<style>
    .views-wrapper ul {
        list-style: none;
    }
    .views-wrapper li {
        padding: 10px;
        border-bottom: 1px solid #ccc;
    }
    .views-wrapper li:last-child {
        padding: 10px !important;
        padding-bottom: 0 !important;
        border-bottom: none !important;
    }
</style>
<ul>
 <?php foreach($response as $row){?>
 <li class="row"><div class="col-xs-2 no-padidng" style="padding-left: 13px;">
    <img src="<?= $row['studentimage']?>" class="img-responsive"></div>
    <div class="col-xs-6">
        <?= $row['studentname']?>
        <br>
        <?= $row['organizationname']?>
    </div>
    <div class="col-xs-4 text-right">
        <?= $row['classname']?>
        <br>
        <?= $row['dataposteddatetime']?>
    </div></li>
    <?php }?>
</ul>