<link href="<?= base_url()?>/assets/css/template/editor/math.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url()?>/assets/css/template/editor/mathmml.css" rel="stylesheet" type="text/css" />
<style type="text/css">
  .forum-actions,.answer-actions{
    color: #aaa;
  }
  .forum-actions i, .answer-actions i{
    font-size: 18px;
  }
  .forum-actions.liked, .answer-actions.liked{
    color: #3f8654;
  }
  .forum-date{
    font-weight: 700;
    color: #0c50a3;
  }
  .askers-icon{
    padding:9px 9px 9px 15px
  }
  .get_answers, .post-answer{
    background-color: #0c50a3;
    border: #0c50a3;    
  }
  .get_answers:hover,.get_answers:focus,.get_answers:active,.post-answer:hover,.post-answer:focus,.post-answer:active{
    background-color: #0c50b3;
    border: #0c50a3;        
  }
  .answer-text{
    width: 90% !important;
    border: none !important;
    padding: 0 10px;
    outline: none;
  }
  .answers-list{
   /* margin-bottom: 10px;*/
 }
 a.forum-actions:hover, a.forum-actions:focus {
  color: #23527c;
  text-decoration: none;
} 
#home-workk .rate-txt p{
  font-size: inherit !important;
}
#cke_txtEditedQuestion{
  width: 100% !important;
}
.hide{display:none;}
.ui-autocomplete{
  width: 668px!important;
}
.panel-group{
  margin-bottom:-5px!important;
}
</style>

<div id="hhloading" style="display:none;text-align: center;z-index:  9999;position: absolute;width: 100%;top: 5px;"><i class="fa fa-circle-o-notch fa-spin" style="font-size:24px"></i></div>
<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/materialize/0.95.1/js/materialize.min.js"></script> -->
<?php


if(empty($response)){
  // echo'<pre>';
  //             print_r($response);
  //             exit;
  if(!$frompagination){
    ?>
    <div class="error-message">
      <?php
      //echo "No one has asked question regarding this chapter yet.";
      echo $message;

      ?>
    </div>
    <?php
  }
}else{
  $i=1;
  $vidtypearr=array('RV','LC');

  foreach ($response as $key => $row) {
    // var_dump($row);exit;
    if(@$row['questionsofstudentsid']=='' && !in_array(@$source,$vidtypearr))
    { 
     
      ?>
        <div class="error-message">
             Questions are not available !
        </div>

   <?php  exit;
    }

   
       
    $last_dataposteddatetime = $row['lastdate'];

    $qid =  $row['questionsofstudentsid'];

    if(in_array(@$source,$vidtypearr))
          {
           // echo $source;
            $row['islocked']='N';

            $row['questiontext']=$row['title'];
            if($source=='RV')
            $qid = $row['referenceid'];
            else
            $qid=$row['slidescontentid'];
                        $last_dataposteddatetime=10;
          }
     ?>

    <div class="panel-group" id="accordion<?= $qid?>" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="heading<?= $qid?>">
          <div class="row panel-heading-up">
           <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
             <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 no-padding">
             <p style="color:#0c50a3; font-size: 12px; font-weight:normal; padding-top:5px;"><strong><span id="questiontext<?= $qid?>"><?= $row['questiontext']?></span></strong>
              </div>
             
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
          <?php 
          if($i == 1){
            $questionaccordian="true";
          }else{
            $questionaccordian="false";
          }
          ?>
          <?php if(!in_array(@$source,$vidtypearr)):?>
          
          <a href="javascript:;"  class="forum-actions" style="margin-left:10px;"><i class="fa fa-eye"></i>
            <span style="color:black;margin-left: 5%;"><big><?=($row['views']>0?$row['views']:'0')?></big></span>
          </a>

        <?php endif;?>
          <?php 
          
          if($row['islocked'] =='N'){?>
          <a role="button" data-toggle="collapse" data-forumid="<?= $qid?>" data-rating="<?= $row['rating']?>" data-parent="#accordion" href="#collapse<?= $qid?>" aria-expanded="false" aria-controls="collapse<?= $qid?>" class="get_answers" data-count="<?php echo $i;?>"><i class="arrow-down"></i></a>
          <?php }else{?>
          <a href="javascript:;" class="unlockchap" data-chaptername="" data-subjectname="<?=$row['chaptername']?>" data-classid=""><img src="<?php echo base_url().'/assets/images/lockicon.png'?>"/></a>
          <?php }?>
        </div>
      </div>
      <div style="display: none;"><input type="range" id="rate" min="1" max="100" value="10" /><input type="range" id="pitch" min="0" max="2" value="1" /></div>
      <div class=" col-md-12 col-lg-12 col-xs-12 questions-here">
        <!-- <div style="display: none;"><input type="range" id="rate" min="1" max="100" value="10" /><input type="range" id="pitch" min="0" max="2" value="1" /></div> -->
        <div class="col-md-10">
            <?php
          //if($row['questiontext']){
            ?>
            <!-- <input type="hidden" id="posttext" value="<?= $row['questiontext']?>">
            <a href="javascript:;" id="speak" class="waves-effect waves-light btn speak" data-id="<?= $qid?>"><i class="fa fa-volume-up" aria-hidden="true"></i></a> -->
            <?php
          //}
            ?></p>
            <?php if(in_array(@$source,$vidtypearr)):
                          if($source=='RV')
                          $ilink='https://www.youtube.com/embed/'.$row['link'];
                          else
                          $ilink=$row['link'];
               ?>
              <span class="rvideo rvideo<?=$qid;?> hide">
              <iframe width="820" height="530"
              src="<?=$ilink;?>">
              </iframe>          
              </span>
              <?php  endif; ?>

          </div>
          <div class="col-md-2">
            <?php
            if($row['status'] == 'P'){
              ?>
              <div class="dropdown pull-right">
                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background: #e7defd; margin-top: 5px; border-radius: 0px; border-color: #e7defd;">
                  <i class="fa fa-chevron-down" aria-hidden="true" style="color: #034da2;font-size: 10px;margin-top: -5px;"></i>
                </button>
                <ul class="dropdown-menu">
                  <li><a href="javascript:;" class="edit_question" data-questionid="<?= $qid?>" data-mediatype="<?= $row['mediatype']?>">Edit</a></li>
                  <li><a href="javascript:;" class="delete_question" data-questionid="<?= $qid?>">Delete</a></li>
                </ul>
              </div>
              <?php
            }
            ?>

          </div>
          <?php 
              //if($row['mediatype']=='image'){
          if($row['questionimage']){
            ?>
            <a href="<?php echo $row['questionimage'];?>" target="_blank">
              <img class="img-responsive" src="<?php echo $row['questionimage']; ?>" style="width:50%" id="questionimage<?= $qid?>">
            </a>
            <?php
          }
          ?>

        </div>
      </div>

    </div>
    <div id="collapse<?=$qid?>" class="panel-collapse collapse answer-body" role="tabpanel" aria-labelledby="heading<?= $qid?>">
      <div class="panel-body"style="padding: 0px;">
        <div class="panel-body-up answers-wrapper" style="border-bottom: 2px solid #cecfff;">
      <!-- <span id="answertext<?= $qid?>">
        <a href="javascript:;" id="speak" class="waves-effect waves-light btn speak pull-right" data-id="<?= $qid?>" ><i class="fa fa-user pull-right "  aria-hidden="true"></i>
        </a>
      </span> -->
      <div class="answers-list row"></div>
    </div>

    <?php if(!in_array(@$source,$vidtypearr)): ?>
    <div class="panel-body-down">

      <div class="col-md-12 comment-reply-area">
      <div class="col-md-12"style="padding: 0px; margin: 10px 0px;">
         <!-- FOR USER IMAGE -->

            <div class="col-md-1 col-xs-1 pull-left" style="padding-right: 0">
            <img src="<?= ($this->session->userdata('image')!=null)?$this->session->userdata('image'):'http://dev.midas.com.np/assets/images/parent.png';?>" onerror="this.onerror=null;this.src='<?=$defaultimg?>'" style="border-radius:24px;" width="40px" height="40px" class="img-responsive"/>    
            </div>
                <!-- END FOR USER IMAGE -->
          
                    <!-- FOR ANSWER -->

            <div class="col-md-9 col-xs-9 col-sm-9 pull-left  ">
              <textarea id="txt-postanswer<?= $qid;?>" type="text" class="form-control answer-text"  placeholder="Type your comment here." data-questionid="<?= $qid?>" style=" background-color: #b5b5b51f;
    border-radius: 23px;
    color: black;
  margin-left: -20px;
    height:42px;
    padding-top: 1.5%;
    width: 110%!important;
    "></textarea>
            </div>
              
                <!-- END FOR ANSWER -->

                   <!-- FOR COMMENT BOX-->

 
          <div class="col-md-2 col-sm-2 col-xs-2 no-padding">
          <a href="" id="btn-postanswer<?= $qid;?>" class="btn btn-info post-answer pull-right disabled" style="bottom: 0 !important;height: 32px;border-radius: 4;margin-top:7px;" data-questionid="<?= $qid?>">POST</a>
                  

          </div>
       <!-- END FOR COMMENT BOX -->
          


                </div>
              </div>
              
          </div>
        <?php endif;?>

        </div>    
      </div>
    </div>

  </div>
  <?php $i++; }

}  ?>

<?php //var_dump($response); ?>
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
<script type="text/javascript">

  $(document).ready(function(){
    if($('#source').val()=='YW'){
    $("#questiontext").autocomplete({
      
    source: function( request, response ) {
      $('#closesearch').removeClass('hide');
        var infoData={classid:localStorage.getItem('eclassid'),subjectid:localStorage.getItem('currentsubjectid'),chapterid:localStorage.getItem('currentchapterid'),searchquery:request.term};

          $.when(getsuggestion(infoData)).then(function (data) {
            if(data.type=='success')
            {
              response( $.map(data.response, function(item) {
                return {
                    label: item,
                    value: item
                }
            }));
            }
            else
            {
              $('.ui-autocomplete').attr('style','display:none'); 
            }
               
        })
      }
      ,select: function(event, ui) {
          if(ui.item){
            // console.log("CONSOLESELECT>"+ui.item.value);
            showloader1();
            var formData = new FormData();
            formData.append('eclassid', localStorage.getItem('eclassid'));
            formData.append('subjectid', localStorage.getItem('currentsubjectid'));
            formData.append('chapterid', localStorage.getItem('currentchapterid'));
            formData.append('questiontext', ui.item.value);
            formData.append('getsuggestion',true);
            formData.append('getsimilar',true);
        $.when(postForumQuestion(formData)).then(function (data) {
            if (data.type == 'error' || data.hassuggestion==true) {
                hideloader1();
                $('#qncountshow').text('');
               if(data.hassuggestion==true){
                 data.message='This question hasnot been answered.';
               }
                alert(data.message);
                return false;
            }
            $('#qncountshow').text('1 Question found.');

            hhduration = 0;
                var postdata = { data: data, classname:localStorage.getItem('currentclassname'), subjectname: localStorage.getItem('currentsubjectname'), subjectid:localStorage.getItem('currentsubjectid'), chaptername:localStorage.getItem('currentchaptername'), classid: localStorage.getItem('eclassid'), filter: 'ALL',searchquery:ui.item.value,source:$('#source').val(),issuggestion:'true'  };
                $.ajax({
                    type: "POST",
                    url: base_url + "forum/searchkeyword",
                    data: postdata,
                    success: function (result) {
                        $('.searchdiv').html(result);
                        hideloader1();

                    }
                });
        })
      }
  }
      
  });

  $('#checksearch').click(function(){
    if($(this).is(':checked'))
    {
      $('.post-question').val('Post');
     $('#questiontext').attr('placeholder','Type your question to ask...');
    }
     
    else
    {
      $('.post-question').val('Search');
    $('#questiontext').attr('placeholder','Type your question to search...');
    }


});
    }
    $(document).off('click','#exam');
  $(document).on('click','#exam', function(e){
    $('.qacheck').prop('checked', false);
    $(this).prop('checked', true);
    sortqe('exam');

  });
  $(document).off('click','#video');
  $(document).on('click','#video', function(e){
    $('.qacheck').prop('checked', false);
    $(this).prop('checked', true);
    sortqe('video');

  });
  $(document).off('click','#all');
  $(document).on('click','#all', function(e){
    $('.qacheck').prop('checked', false);
    $(this).prop('checked', true);
    sortqe('all');

  });
  function sortqe(sort)
  {
    var chapterid=localStorage.getItem('currentchapterid');
    var subjectid=localStorage.getItem('currentsubjectid');
    var subjectname=localStorage.getItem('currentsubjectname');
   var chaptername= localStorage.getItem('currentchaptername');
   var classname= localStorage.getItem('currentclassname');
   var classid=localStorage.getItem('eclassid');
       
    showloader1();
 

    var infoData={Filter:'ALL',Subjectid:subjectid,Chapterid:chapterid,Duration: hhduration, Time: getCurrentDateTime(),Sort:sort};
    infoData.Iscount='Y';

   
    $.when(getForumQuestions(infoData)).then(function (data) {
        if (data.type == 'error') {
            hideloader1();

            var html='<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0;">'+
            '<div class="col-xs-12">'+
            '<div class="img-thumbnail col-lg-12 archive-error">'+data.message+
              '</div>'+
        '</div></div>';
            $('.searchdiv').html(html);
            return false;
        }
        hhduration = 0;
            var postdata = { data: data, classname:classname, subjectname: subjectname, subjectid:subjectid, chaptername:chaptername, classid: classid, filter: 'ALL' };
            $.ajax({
                type: "POST",
                url: base_url + "forum/lasthomeworkhelp",
                data: postdata,
                success: function (result) {
                  
                    $('.searchdiv').html(result);
                    $('#qncountshow').text(data.totalcount + ' Questions & answers found.');

                    hideloader1();

                }
            });
        });
  }
    $('.collapse').on('shown.bs.collapse', function(){
      $(this).parent().find(".arrow-down").removeClass("arrow-down").addClass("arrow-up");
    }).on('hidden.bs.collapse', function(){
      $(this).parent().find(".arrow-up").removeClass("arrow-up").addClass("arrow-down");
    });

    // CALCULATE DURATION SPENT
    var hh_interval = window.setInterval(function() {
      hhduration += 1000;   
          // console.log(qaduration);   
        }, 1000); 

    <?php
    foreach ($response as $key => $row) {    
        //if($row['status']=='Y' && $row['timer']!=0){ //if answer is approved show timer
          if($row['timer']!=0){ //if timer is set
            ?>   

            setInterval(function() {
              var curtime = new Date().valueOf();


              var timer = "<?= $row['timer']?>";

            // var timeDiff = curtime-timer;
            // var date=new Date(timeDiff);
            // var hours = date.getHours(); // minutes part from the timestamp
            // var minutes = date.getMinutes(); // seconds part from the timestamp
            // var seconds = date.getSeconds(); // will display time in 10:30:23 format
            // var formattedTime = hours + ':' + minutes + ':' + seconds;
            var timeDiff = timer-curtime;
            if(timeDiff>0){
              var date=new Date(timeDiff);
              var hours = Math.floor(timeDiff / 36e5); // minutes part from the timestamp
              var minutes = Math.floor(timeDiff % 36e5 / 60000); // seconds part from the timestamp
              var seconds = Math.floor(timeDiff % 60000 / 1000); // will display time in 10:30:23 format
              var formattedTime = hours + 'hrs ' + minutes + 'mins ' + seconds+'sec';
              var answer = "Answer in:"+formattedTime;
              $('.question-timer'+"<?= $row['questionsofstudentsid']?>").html(answer);
            }

          }, 1000);
            <?php   
          }     
        }
        ?>

      });

  $(document).off('change','.rating-radio');
  $(document).on('change','.rating-radio',function(){
    var infoData = { Questionsofstudentsid: $(this).data('refid'), Rating:$(this).val()};
        //var response = postQuestionRating(infoData);
        
        
        $.when(postQuestionRating(infoData)).then(function(data){

          if(data && data.type=='success'){
            console.log(data.response);
            var qid = data.response.questionsofstudentsid;
            var avg_rating = data.response.averagerating;
            var total_rating = data.response.totalrating;
            $('#avg-rating-'+qid).text(avg_rating);
            $('#total-rating-'+qid).text(total_rating)
          }
        });
      });

    </script>

    <script type="text/javascript">   
      var lastdate = "<?= $last_dataposteddatetime?>";
      // console.log(lastdate.length);
      $(window).data('ajaxready', true).scroll(300,function(e) {
        if ($(window).data('ajaxready') == false) return;
        if ($('#source').val()=='LC') return;

        if(lastdate.length==0) return;

        if (($(window).height() + $(window).scrollTop() +100) >= ($(document).height()-100)) {
          $('.cust-loader').removeClass('hidden');
          $(window).data('ajaxready', false);
          var filter = $('#select_dropdown').val();
          var searchquery = $('#searchquery').val();

          if($('#source').val()=='RV')
          {
            var referencepagination=$('#referencepagination').val();
            var infoData={Subjectid:localStorage.getItem('currentsubjectid'),Chapterid:localStorage.getItem('currentchapterid'),page:referencepagination};
            var params = {infoData:infoData,classname:localStorage.getItem('currentclassname'),subjectname:localStorage.getItem('currentsubjectname'),chaptername:localStorage.getItem('currentchaptername'),subjectid:localStorage.getItem('currentsubjectid')};

            $.when(getReferenceVideo(infoData)).then(function (data) {
              var postdata = {data:data,classname:params.classname,subjectname:params.subjectname,subjectid:params.subjectid,chaptername:params.chaptername,classid:localStorage.getItem('currentclassid'),filter:filter,source:'RV'};

              hhduration = 0;
            $('.cust-loader').addClass('hidden'); 
            if (data.type == 'success') {
                  $.ajax({
                  type:"POST",
                  url: base_url+"forum/lasthomeworkhelp",
                  data: postdata, 
                  success:function(result) {  
                    referencepagination=parseInt(referencepagination)+1;
                    $('#referencepagination').val(referencepagination);
                    $(window).data('ajaxready', true);  

                    $('.archive-forum').append(result);         
                  }
                });
              }

            })


          }
          else
          {
            var infoData = {Filter: filter, Subjectid:localStorage.getItem('currentsubjectid'),Chapterid:localStorage.getItem('currentchapterid'),lastdate:lastdate,Source:$('#source').val(),Duration:hhduration,Time:getCurrentDateTime(),Searchquery:searchquery};
          var params = {infoData:infoData,classname:localStorage.getItem('currentclassname'),subjectname:localStorage.getItem('currentsubjectname'),chaptername:localStorage.getItem('currentchaptername'),subjectid:localStorage.getItem('currentsubjectid')};
      //return false;
      if($('#source').val()=='YW')
      {
        var sort='all';
        if($('#all').prop("checked") == true){
            sort='all';
            }
            else  if($('#video').prop("checked") == true){
              sort='video';
            }
            else  if($('#exam').prop("checked") == true){
              sort='exam';
            }
           infoData.Sort=sort;
      }

      var infoData = params.infoData;
      $.when(getForumQuestions(infoData)).then(function(data){
        hhduration = 0;
        $('.cust-loader').addClass('hidden'); 
        if(data['type'] == 'success'){
          if($('#source').val()=='HH' || $('#source').val()=='LC'){
            var postdata = {data:data,classname:params.classname,subjectname:params.subjectname,subjectid:params.subjectid,chaptername:params.chaptername,classid:localStorage.getItem('currentclassid'),filter:filter};
            $.ajax({
              type:"POST",
              url: base_url+"forum/lasthomeworkhelp",
              data: postdata, 
              success:function(result) {  
                $(window).data('ajaxready', true);  

                $('.archive-forum').append(result);         
              }
            });
          }else if($('#source').val()=='YW' || $('#source').val()=='T'){
            var postdata = {response:data.response,classname:params.classname,subjectname:params.subjectname,subjectid:params.subjectid,chaptername:params.chaptername,classid:localStorage.getItem('currentclassid'),filter:filter};
            $.ajax({
              type:"POST",
              url: base_url+"forum/lasthomework",
              data: postdata, 
              success:function(result) {
                $(window).data('ajaxready', true);

                $('.searchdiv').append(result);              
              }
            });
          }
        }
      });

          }
         


    }
  });
</script>

