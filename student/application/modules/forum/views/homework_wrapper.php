<style>
  h5.bread{background:#eef1f6; font-weight:bold; padding:10px 5px; margin:2px 5px; color:#418755 !important;}
  .progressbar{padding:0;}
  .bread_wrap{border-bottom:1px solid #dfe4e8 !important;}
  h5 span.active{color:#526b81;}
  ul.chaps_list li{list-style:none; background:#f1f1f1;padding: 5px 10px;
    margin: 2px auto;}
    ul.chaps_list li:nth-child(even){
      background:#fbfbfb;
    }
    .progress{margin-top:5px;}
    .mar-top-10{margin-top:10px;}
    img.ads{
      border:1px solid #d5d5d5; margin:0 auto;
    }
    span.pro_per{color:#d57656;}
     img.loader1{
    position: fixed;
    top: 280px;
    right: 47%;
    z-index: 999;
  }
  .panel-group {
    margin-left: 10px;
    margin-right: 10px;
  }
  
  .answers-wrapper img{ height:auto !important; }
  
  .teacher_answer_heading{ margin-bottom:20px !important; }
  
  .answer-indivisual{ float:left !important; }
  
  </style>
  <?php  $subject = explode('-', $subjectname); ?>
<img  style="display: none;" class="loader1" width="80px" src="<?=base_url().'/assets/images/loader4.gif'?>"/>

  <?php //$this->load->view('templates/qe_breadcrum_view'); ?>
  <input type="hidden" id="subjectname" value="<?= $subjectname?>">


  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pad-fix" style="margin-left:-33px;width:108%">
    <div class="col-lg-12">
      <?php
    //var_dump($filterkey);
      // echo'<pre>';
      // print_r($data);
      // exit;
      
      if(!isset($subfilter) && !empty($subfilter))
          $subfilter=$subfilter;
      else
          $subfilter='ALL';

      $this->load->vars(array('data' => $data,'filterkey'=>$filterkey,'subfilter'=>$subfilter));

      $this->load->view('homework');
      ?>
    </div>
  
  </div>

  <script src="https://code.jquery.com/jquery-1.12.4.js"></script> 
 <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
  
<script>
  $(window).on('beforeunload', function() {
    $(window).scrollTop(0); 
});
</script>