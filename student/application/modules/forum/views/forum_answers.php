<?php
$responseType = $data['type'];
$dataQuestion = $data['question'];
$data = $data['response'];
$num_of_ans = (is_array($data)&&sizeof($data)>0)?sizeof($data):0;

$hasteacheranswer=$dataQuestion['hasteacheranswer'];

  // echo'<pre>';
  // print_r($dataQuestion['questionsofstudentsid']);
  // exit;
  // print_r($data);
  // exit;
?>
<div style="display: none;"><input type="range" id="rate" min="1" max="100" value="10" /><input type="range" id="pitch" min="0" max="2" value="1" /></div>
<?php
$prevanswerbyid = 0;
$count_teacher_ans = 0;
foreach($data as $key=>$row){
  //echo'<pre>';
 // print_r($data);
 //  exit;

 // if($key == 0){
  if($row['answerbytype']=='TEACHER' && $row['mediatype'] == 'text'){?>
  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 pull-right" >     

   <div id="answertextaudio<?=$row['answerstoquestionsid']?>" data-qplaintext='<?= @$row['qplaintext']?>' data-qid='<?=$dataQuestion['questionsofstudentsid']?>' data-answerid="<?=$row['answerstoquestionsid']?>">

     <a href="javascript:;" data-mediatype="<?=$row['mediatype']?>" data-answerid="<?=$row['answerstoquestionsid']?>" class="waves-effect waves-light btn speak pull-right"  data-qid="<?=$dataQuestion['questionsofstudentsid']?>" data-pause="" data-type="homeworkhelp">
     <!-- <i class="fa fa-pause-circle pull-right playicon "  aria-hidden="true"></i> -->
      <img id="soundimg<?=$dataQuestion['questionsofstudentsid']?>" src="<?=base_url();?>assets/images/On.png" style="width:30%" />
     </a>
     <input class="audiopause<?=$row['answerstoquestionsid']?>" type="hidden" name="pause<?=$row['answerstoquestionsid']?>" value='<?=$row['answerstoquestionsid']?>'>
     <!-- <input class="audiopause" type="hidden" name="pause" value='0'> -->
   </div> 
   
 </div>
  <?php //}
}

if($row['answerbytype']=='STUDENT'){?>


<div id="answer-indivisual-<?= $row['answerstoquestionsid'];?>" class="col-lg-10 col-md-10 col-sm-10 col-xs-12 answer-indivisual">
  <div class="col-xs-12 bg-student-ans" style="padding-top: 10px;">

   <div class="col-md-1 col-xs-1 pull-left" style="padding-right: 0">
    <img src="<?= $row['userimage']?$row['userimage']:base_url('assets/images/dummy-person.jpg')?>" width="40px" height="40px" class="img-responsive"/>    
  </div>
  <div class="col-md-3 col-xs-3 pull-left ">
    <label for="username" style="margin-bottom: 0px;"><a><span style="color:#717B9B; font-size:12px;"><?= $row['username']?></span></a></label><br>
    <!-- <a><small style="color: #7D7D7D;">Replied on&nbsp;</small></a>-->
    <small style="color:#8f949a"><span><?= $row['dataposteddatetime']?></span> <!-- at <span id="time">11:15pm</span> --></small><br>
  </div>
  <div class="col-sm-8 col-xs-12">
    <?php
    if($row['status'] == 'P' && $row['answerbyid']==$this->session->userdata('userid')){
      ?>
      <div class="dropdown pull-right">
        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background: #e7defd; margin-top: 5px; border-radius: 0px; border-color: #e7defd;">
          <i class="fa fa-chevron-down" aria-hidden="true" style="color: #034da2;font-size: 10px;margin-top: -5px;"></i>
        </button>
        <ul class="dropdown-menu">
          <li><a href="javascript:;" class="edit_answer" data-questionid="<?= $dataQuestion['questionsofstudentsid']?>" data-ansid="<?= $row['answerstoquestionsid'];?>">Edit</a></li>
          <li><a href="javascript:;" class="delete_answer" data-questionid="<?= $dataQuestion['questionsofstudentsid']?>" data-ansid="<?= $row['answerstoquestionsid'];?>">Delete</a></li>
        </ul>
      </div>
      <?php
    }
    ?>
  </div>
  
  <?php
      //if($count_teacher_ans==0):
  ?>
  <div class="col-sm-10 col-xs-12 answerstext">
    <p id="answerstext<?= $row['answerstoquestionsid'];?>"><?= $row['answertext']?></p>

    <?php 
    if($row['mediatype']=='image'){
      ?>
      <img class="img-responsive" src="<?php echo $row['answerimage']; ?>" style="width:96%">
      <?php
    }
    ?>
          <!-- <a href="javascript:;" class="answer-actions <?= ($row['postliked']=='Y'?'liked':'')?> like-answer" data-refid="<?= $row['answerstoquestionsid']?>" data-likes="<?= $row['likes']?>"><i class="fa fa-thumbs-up"></i></a>
          <a href="javascript:;" class="answer-actions <?= ($row['postliked']=='Y'?'liked':'')?> view-likes" data-refid="<?= $row['answerstoquestionsid']?>" data-actiontype="likes">
            <?php 
            if($row['likes']>1)
              $text = $row['likes'].' Likes';
            else if($row['likes']==1)
              $text = $row['likes'].' Like';
            else
              $text = 'Like';

            ?>
            <span class="likescount"><?= $text?></span>
          </a> -->
        </div>
        
        <?php
        //endif;
        ?>
        
      </div>
    </div>
    <?php }/*--end answers of student--*/
    else{
      ?>
      <!-- Add Teacher's Answer -->
      <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 pull-right answer-indivisual " style="margin-top: 10px;">
       <div class=" col-lg-12 col-md-12 col-sm-11 col-xs-11 pull-right teacher_answer_heading">


        <?php
        if($prevanswerbyid != $row['answerbyid']){
          $prevanswerbyid = $row['answerbyid'];

          ?>
          <div class="col-lg-8 col-md-8 col-sm-8 colxs-8">
            <div class="question-points-wrapper">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-6">
               <div class="col-sm-4 col-xs-6 pad-fix" >
                <p class="" style="margin-bottom: 0px;text-align: center;">
                  <span class="font12">Average Rating</span><br>
                  <span class=" ">
                    <big class="mi44">
                      <span id="avg-rating-<?=$questionid?>">
                        <?php echo $dataQuestion['averagerating'];//echo round($dataQuestion['averagerating'], 2); ?>

                      </span> 
                      <span class="glyphicon glyphicon-star myy"></span>
                    </big>
                  </span>
                </p>
              </div>
              <div class="col-sm-4 col-xs-6 pad-fix ">
                <p class="pull-right"style="margin-bottom: 0px;text-align: center;">
                  <span class="font12">Total Ratings</span><br>
                  <span class=""><big><span id="total-rating-<?=$questionid?>"><?php echo $dataQuestion['totalrating'] ?></span></big></span>
                </p>
              </div>
            </div>
          </div>

        </div>
        <div class="col-md-3 col-xs-3 pull-left midas-user">
          <label class="lblteachername" for="username" style="margin-bottom: 0px;" data-answereby="<?php echo $row['answerbyid'] ?>">
            <a><span style="color:#717B9B; font-size:12px;"><?= $row['username']?></span></a>
          </label><br>
          <!-- <a><small style="color: #7D7D7D;">Replied on&nbsp;</small></a>-->
          <small style="color:#8f949a"><span><?= $row['dataposteddatetime']?></span> <!-- at <span id="time">11:15pm</span> --></small><br>
        </div>
        <div class="col-md-1 col-xs-1 pull-left" style="padding-right: 0">
          <img class="teacher-profile-pic" src="<?= $row['userimage']?$row['userimage']:base_url('assets/images/dummy-person.jpg')?>" width="40px" height="40px" class="img-responsive" data-answereby="<?php echo $row['answerbyid'] ?>"/>    
        </div>
        <?php
      }else{

      }?>
      
    </div>
    
		<div class=" col-sm-12 col-xs-12 answerstext" >
			<div class="row">
				<div class="col-md-12 col-xs-12 pull-left">
				  <p class="para-ans"><?= $row['answertext']?></p>
				</div>
			</div>
      <?php 
      if($row['mediatype']=='image'){
        ?>
        <div class="row ">
          <div class="col-md-12">
            <img class="img-responsive pull-right " src="<?php echo $row['answerimage']; ?>" style="width:96%">
          </div>
        </div>
        <?php
      }
      if($row['mediatype']=='video'){
        ?>
        <div class="row ">
          <div class="col-md-12" style="text-align: right;">
            <div class=" wrapytv" style="width:780px!important">
              <?php
              if($row['videotype']=='youtube'||$row['videotype']=='vimeo'){
                $embedURL = getEmbedUrl($row['answervideolink']);
                ?>
                
                <iframe class="video" width="" height="" src="<?php echo $embedURL; ?>"></iframe>
                
                <?php
              }else if($row['videotype'] == 'selfserver'){
                if(@$row['vsourcetype']=='data')
                {
                  $patharr = explode('*#*', @$row['videodata'])        
                  ?>
                  <iframe id="myframe" src="https://stream.midas.com.np:8443/myplayer/index.html?subject=<?= $patharr[0]?>&amp;chapter=<?=$patharr[1]?>" allowfullscreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen"  msallowfullscreen="msallowfullscreen" oallowfullscreen="oallowfullscreen" webkitallowfullscreen="webkitallowfullscreen" style="height: 425px;width: 100%;"></iframe>
                  <?php }
                  else
                    {?>

                  <video id="hhanswervideo<?=$row['answerstoquestionsid']?>" class="hhanswervideo" controls controlsList="nodownload" width="100%" data-answerstoquestionsid="<?=$row['answerstoquestionsid']?>" poster="<?= $row['thumburl']?$row['thumburl']:'assets/images/img_placeholder.jpg'?>">
                    <source src="<?php echo $row['answervideolink'];?>" type="video/mp4">
                      <source src="<?php echo $row['answervideolink'];?>" type="video/ogg">
                        Your browser does not support the video tag.
                      </video>
                      <?php
                    }
                  }
                  ?>
                </div>

              </div>
            </div>
            <?php
          }
          ?>
        <!--<div class='row'>
          <div class="col-md-12">
            <a href="javascript:;" class="answer-actions <?= ($row['postliked']=='Y'?'liked':'')?> like-answer" data-refid="<?= $row['answerstoquestionsid']?>" data-likes="<?= $row['likes']?>"><i class="fa fa-thumbs-up"></i></a>
            <a href="javascript:;" class="answer-actions <?= ($row['postliked']=='Y'?'liked':'')?> view-likes" data-refid="<?= $row['answerstoquestionsid']?>" data-actiontype="likes">
              <?php 
              if($row['likes']>1)
                $text = $row['likes'].' Likes';
              else if($row['likes']==1)
                $text = $row['likes'].' Like';
              else
                $text = 'Like';

              ?>
              <span class="likescount"><?= $text?></span>
            </a>
          </div>
        </div>-->
      </div>
      
    </div>
    <?php
    $count_teacher_ans++;
    //echo $count_teacher_ans;
  }/*--end of teacher comments---*/
}?>
<div class="new-anslist"></div>
<?php
//echo '<pre>';var_dump($responseType);echo '</pre>';
if($this->session->userdata('usertype')== 'STUDENT'){
  if($hasteacheranswer=='Y' && strcasecmp('success', $responseType)==0 && $num_of_ans>0){
    ?>
    <div class="col-sm-12">
      <div class="col-sm-12 col-xs-12 dtahy">
        <div class="pull-right">
          <strong class="dtahytxt">Does this answer help you?</strong><br>
          <div class="acidjs-rating-stars my" style="margin-left: 30px;">
            <form style="">
              <input type="hidden" id="ratyQId" value="<?php echo $questionid;?>">
              <input type="radio" class="rating-radio" name="group-<?= $questionid?>" data-refid="<?= $questionid?>" id="group-<?= $questionid?>-0" value="5" <?php echo $rating==5?checked:''; ?>><label for="group-<?= $questionid?>-0"></label>
              <input type="radio" class="rating-radio" name="group-<?= $questionid?>" data-refid="<?= $questionid?>" id="group-<?= $questionid?>-1" value="4" <?php echo $rating==4?checked:''; ?>><label for="group-<?= $questionid?>-1"></label>
              <input type="radio" class="rating-radio" name="group-<?= $questionid?>" data-refid="<?= $questionid?>" id="group-<?= $questionid?>-2" value="3" <?php echo $rating==3?checked:''; ?>><label for="group-<?= $questionid?>-2"></label>
              <input type="radio" class="rating-radio" name="group-<?= $questionid?>" data-refid="<?= $questionid?>" id="group-<?= $questionid?>-3" value="2" <?php echo $rating==2?checked:''; ?>><label for="group-<?= $questionid?>-3"></label>
              <input type="radio" class="rating-radio" name="group-<?= $questionid?>" data-refid="<?= $questionid?>" id="group-<?= $questionid?>-4" value="1" <?php echo $rating==1?checked:''; ?>><label for="group-<?= $questionid?>-4"></label>
            </form>
          </div>
        </div>
      </div>
    </div>
    <?php
  }
}
//echo '<pre>';var_dump($this->session->userdata());echo '</pre>';
?>

<!-- <div class="modal" id="teacherModal" role="dialog" style="">
 <div class="modal-dialog"> -->

   <!-- Modal content-->
			  <!-- <div class="modal-content">
				<div class="modal-header">
				  <button type="button" class="close" data-dismiss="modal">&times;</button>
				  <h4 class="modal-title">Answerd by</h4>
				</div>
				<div class="modal-body">
					<div class="teacher-detail">
				  		<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
				  			<img src="../assets/images/dummyhead.png" class="img-responsive teachers-dummy-head modal-teacher-img">
							 <h4 class="modal-teacher-name">Loading...</h4>
				  		</div>
				  		<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 left-td">
							<table class="table">
								
								<tbody>
								<tr>
									<td style="color:#ce5d35; width:15%;">Education </td>
									<td><span style="color:#ce5d35; display:inline-flex;">: <p  style="color:#000; padding-left: 5px;" class="modal-teacher-education">Loading...</p></span></td>
									
								  </tr>
								  <tr>
									<td style="color:#ce5d35; width:15%;">Experience </td>
									<td><span style="color:#ce5d35; display:inline-flex;">: <p style="color:#000; padding-left: 5px;" class="modal-teacher-teachingyears">Loading...</p></span></td>
									
								  </tr>
								  <tr>
									<td style="color:#ce5d35; width:15%;">Subject </td>
									<td><span style="color:#ce5d35; display:inline-flex;">: <p style="color:#000; padding-left: 5px;" class="modal-teacher-teachingsubjects">Loading...</p></span></td>
									
								  </tr>
								  <tr>
									<td style="color:#ce5d35; width:15%;">School </td>
									<td>
									<span style="color:#ce5d35; display:inline-flex;">:<p  style="color:#fc9705; padding-left: 5px;" class="modal-teacher-currentschool" class="fontorange">Loading...</p>
									  <p style="color:#000; padding-left: 5px;" class="modal-teacher-prevschool"></p>
									  <p style="color:#000; padding-left: 5px;" class="modal-teacher-pastschool1"></p>
									  <p style="color:#000; padding-left: 5px;" class="modal-teacher-pastschool2"></p></span>
							  		</td>
									
								  </tr>
								  
								  
								</tbody>
							  </table>
             -->
             
             
             
				  			<!-- <p class="modal-teacher-education">Loading...</p>
							  <p class="modal-teacher-teachingyears">Loading...</p>
							  <p class="modal-teacher-teachingsubjects">Loading...</p>
							  <p class="modal-teacher-currentschool" class="fontorange">Loading...</p>
							  <p class="modal-teacher-prevschool">Loading...</p>
							  <p class="modal-teacher-pastschool1">Loading...</p>
							  <p class="modal-teacher-pastschool2">Loading...</p>-->
						<!-- </div>
					</div>
				</div>
				<div class="modal-footer clear-both">
				  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			  </div>

			</div>
    </div> -->


    <script type="text/javascript">
      $(document).ready(function(){
        $('.video').on('play',function(){
          console.log('played');

        });

        var videoduration=0;
        var video_interval;
        var isclear = true;
        $('video.hhanswervideo').off('play');
        $('video.hhanswervideo').on('play',function(){

          var duration = $(this);
          var answerid=$(this).data('answerstoquestionsid');
          var audio = document.getElementById("hhanswervideo"+answerid);

          $("video.hhanswervideo").not(this).each(function() {
            // console.log(videoduration);
            isclear = false;            
            if(this.pause()){
            }

          });

          if(video_interval)
          {
            window.clearInterval(video_interval);
            clearInterval(video_interval);
            // videoduration=0;
          }
          video_interval=window.setInterval(function(){
            videoduration += 1000;
            isclear = true;            

          },1000)

        // video_interval = window.window.setInterval(function() {
        //   videoduration += 1000;   
        //    console.log(videoduration+':'+answerid);  
        // }, 1000);
        // console.log(videoduration);



      });

        $('video.hhanswervideo').off('pause');
        $('video.hhanswervideo').on('pause',function(){
          var duration = $(this);
          var answerid=$(this).data('answerstoquestionsid');
          var audio = document.getElementById("hhanswervideo"+answerid);
        // console.log(answerid);
        if(audio.readyState > 0) 
        {
          console.log('paused');

          var videolength=audio.duration * 1000;
          var videoid=answerid;
          var type='answer';
          var infoData ={videoid:videoid,type:type,viewlength:videoduration,videolength:videolength};
          console.log(infoData);
          

          $.when(trackviewedvideo(infoData)).then(function(rdata){
            videoduration=0;            
            if(isclear)
            {            
              window.clearInterval(video_interval);
            }
            isclear = true;            
          });

        }

      });

      // $('video.hhanswervideo').off('ended');
      // $('video.hhanswervideo').on('ended',function(){
      //   console.log('ended');
      //   var duration = $(this);
      //   var answerid=$(this).data('answerstoquestionsid');
      //   var audio = document.getElementById("hhanswervideo"+answerid);
      //   var type='answer';
      //   var videolength=audio.duration * 1000;
      //   var videoid=answerid;
      //   var answerid=$(this).data('answerstoquestionsid');
      //   var audio = document.getElementById("hhanswervideo"+answerid);
      //   var infoData ={videoid:videoid,type:type,viewlength:videoduration,videolength:videolength};
      //     $.when(trackviewedvideo(infoData)).then(function(rdata){
      //       window.clearInterval(video_interval);
      //       videoduration=0;
      //     });
      // });

      $('video.hhanswervideo').one('play', function () {
        console.log('firstimeplayed');
        var answerid=$(this).data('answerstoquestionsid');
        var trackdata = {event:'hhanswervideo',answerid:answerid};
        trackLiveClass(trackdata);
        return false;
      }); 




      
      var source = $('#source').val();

      if(source == 'LC' || source=='YW'){
        $('.teacher_answer_heading').css('display','none');
      }
    });

  </script>

