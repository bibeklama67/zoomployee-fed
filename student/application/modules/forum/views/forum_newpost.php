<style type="text/css">
	.image_preview {
		width: 70px;
		margin-left: 10px;
		border-radius: 3px;
		display: none;
	}
	.target {
		border: solid 1px #aaa;
		min-height: 200px;
		width: 30%;
		margin-top: 1em;
		border-radius: 5px;
		cursor: pointer;
		transition: 300ms all;
		position: relative;
	}

	.contain {
		background-size: cover;
		position: relative;
		z-index: 10;
		top: 0px;
		left: 0px;
	}
	textarea {
		background-color: white;
	}
	.active_copypaste {
		box-shadow: 0px 0px 10px 10px rgba(0,0,255,.4);
	}
	#cke_questionecktext{
		width:100% !important;
	}

	.btn.fa.fa-pencil-square-o{

		font-size: 33px;
		margin-top: -7px;
		padding: 0;
	}

	pre{
		z-index: -10;
		display: none !important;
		visibility: hidden !important;
	}
	.inputapi-transliterate-indic-suggestion-menu-vertical{
		display: none !important;
	}

	.postcreation-header{
		padding: 0 !important;
		margin: 0 !important;
		background: #FFF;
		height: 34px;
		border-top-right-radius: 4px;
		border-top-left-radius: 4px;
	}

	.postcreation-title{
		width: 50%;
		padding: 0px 10px;
		display: block;
		float: left;
		color: #365899 !important;
		font-weight:600;
	}

	.postcreation-lang{
		float: right;
		width:100px;
		border-radius: 0;
		border: none;
	}

	.postcreation-textarea{
		border:none;
		border-bottom-left-radius: 4px;
		border-bottom-right-radius: 4px;
		margin-bottom: 10px;
		padding: 5px 10px;
	}
	.error{
		color:#f00;
	}
	#edit_question_error{
		float: left;
	}
.hide{
	display:none;
}
.ml-6{
	margin-left:6%!important;
}
.unclick{
	     pointer-events: none;
}
.cust-fa-icon a
{
	color:grey!important;
}
.custom-file-upload
{
	color:grey!important;
	background:none!important;
}
</style>

<div class="row" style="margin-left: 0px !important;margin-right: 0px !important;">
<div class="col-lg-12 col-md-12 col-sm-12 ask-question-div" style="padding-left: 0px;">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 default-bg">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pf-n-iptxt pad-fix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rounded-pf-pic pad-fix">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rounded-pf-pic pad-fix">
				    <div class="pull-left col-lg-8">
					<input type="checkbox" id="all"  value="all" checked class="qacheck"> All
					<input type="checkbox" id="video"  value="video" class="qacheck ml-6"> Video Answers Only
					<input type="checkbox" id="exam"  value="exam" class="qacheck ml-6"> Exam Questions Only
					</div>
					<div class="pull-right pad-fix" style="margin-right: 18%">
						<button type="button" id="en-button" data-value="English" class="langbtn active postcreation-lang"></button>
						<button type="button" id="np-button" data-value="Nepali" class="langbtn postcreation-lang"></button>
						<input type="hidden" name="language_selected" id="language_selected" value="English">
					</div>
				</div>
				<div class="col-md-1 col-sm-1 round-pf">
				<img src="<?= $this->session->userdata('image')?$this->session->userdata('image'):base_url('assets/images/dummy-person.jpg');?>" class="img-responsive img-circle pull-left" style="border-radius:50%;margin-top: 25%;"/>

				</div>
				<div class="col-md-10 col-sm-10">
				<textarea placeholder="Type your question to search..." rows="3" data-toggle="modal" data-target="#myModal" id="questiontext" class="addckeditor"></textarea>

				</div>
				<div class="col-md-1 col-sm-1" style="margin-left: -70px;">
				<a href="javascript:void(0)" id="closesearch" style="margin-left: 45%;" class="hide"><i class="fa fa-close" style="color: grey;font-size:17px;"></i></a><br/>
				<input name="submit" value="Search" class="btn btn-primary btn-send post-question" type="submit" style="margin-top:7px;">

				</div>




			</div>
		</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 post-btns default-bg">
		<div class="col-xs-8" style="margin-top: 10px; margin-left:65px;">
			<input type="checkbox" id="checksearch" name="checksearch"  value="1"> I want to ask a question.
			<span style="
			margin-left: 5%;    color: grey;
		" id="qncountshow">
		</span>
			
		</div>
		<div class="col-xs-6 error_btn_disable" style="margin-top: 7px;"></div>


		<div class="col-xs-6 post-opt" style="margin-top: -25px;">
			<!-- <div class="pull-right" id="post-stat">
				<input name="submit" value="Search" class="btn btn-success btn-send post-question" type="submit">
			</div> -->


			<div class="pull-right cust-fa-icon aa">
				<a href="javascript:;" data-toggle="tooltip" title="Camera" class="btn fa fa-camera qsupload unclick" aria-hidden="true" id="webcambutton">
				</a>

			</div>
			<div class="pull-right cust-fa-icon">
				<a href="javascript:;" data-toggle="tooltip" title="Paste Image" class="btn fa fa-clipboard qsupload unclick" aria-hidden="true" id="copypastebutton" >
				</a>
			</div>
			<div class="pull-right Wrapicon qsupload unclick">
				<label for="file-upload" data-toggle="tooltip" title="Upload Image" class="custom-file-upload btn">
				<i class="fa fa-upload fa-2x" style="margin-top: -7px;"></i>
				</label>
				<input id="file-upload" name="file_upload" type="file" class='btn qans-file'>
			</div>
				<div class="pull-right ">
				<a href="javascript:;"  aria-hidden="true" id="qncount" >
				</a>
			</div> 
			<!-- <div class="pull-right cust-fa-icon">
				<a href="javascript:;" data-toggle="tooltip" title="Text Editor" class="btn fa fa-pencil-square-o" aria-hidden="true" id="ckeditor" >
				</a>
			</div>  -->
			<div class="pull-right">
				<span id="refresh-button"></span>
			</div> 
		</div>
	</div>
</div>
</div>



<div class="modal fade" id="image-popup" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close close-image" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Image Attachment</h4>
			</div>
			<div class="modal-body">
				<img id="question_popup_image" class="image_preview img-responsive">
			</div>
			<div class="modal-footer">

				<button type="button" class="btn btn-default close-image" data-dismiss="modal">Close</button>
				<!-- <a href="javascript:void(0);" class="btn btn-success post-answer" data-dismiss="modal" data-questionid="<?php echo $questionsofstudentsid?>">POST</a> -->
				<input name="submit" value="Post" style="background: #4266b2;" class="btn btn-success btn-send post-question" data-dismiss="modal" type="submit">
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="webcam" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Camera</h4>
			</div>
			<div class="modal-body " style="height: 515px;">

			</div>
			<div class="modal-footer" style="padding: 10px !important;">

				<button style="margin-top: -3px; background: #4266b2;" type="button" id="save-webcam" class="btn btn-success btn-send">Post</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="copypaste-modal" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close close-pasteimage" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Paste Image</h4>
			</div>

			<div class="modal-body">
				<span id="error_message"></span>
				<div class="copy_paste_image">
					<div class="row">
						<div class="span4 target" id="copyimagepaste" style="float: left; margin-left: 20px; width: 93%;"></div>
						<input type="hidden" name="copyimage" id="copyimage">
					</div>
					<!-- <a href="" id="test" target="_blank" style="float: left;">See image in new window</a> -->
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default close-pasteimage" data-dismiss="modal">Close</button>
				<button style="margin-top: -3px; background: #4266b2;" type="button" id="save-copypaste" data-dismiss="modal" class="btn btn-success btn-send">Post</button>
			</div>
		</div>
	</div>
</div>

<!-- Question Text Edit -->
<div class="modal fade" id="ckeditor-modal-edit-question" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close close-edit-question" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Edit your Question</h4>
			</div>

			<div class="modal-body">
				<input type="hidden" name="" id="edit_question_id" class="edit_question_id">
				<textarea id="txtEditedQuestion" class="txtEditedQuestion"></textarea>

				<div id="image_edit_wrap" class="image_edit_wrap" >
					<div>
						<img src="" alt="" id="img_edited_question" class="img-responsive img_edited_question"><br>
						<span class="btn btn-info remove_btn" id="btn_remove_img_edited_question">Remove</span>
					</div>
					<div id="image_input">
						<label>Upload Files</label>
						<input type="file" name="" id="img_input_edited_question" accept="image/*">

					</div>
				</div>
			</div>
			<div class="modal-footer">
				<span id="edit_question_error" class="error edit_question_error"></span>
				<button type="button" class="btn btn-default close-edit-question" data-dismiss="modal">Close</button>
				<button style="margin-top: -3px; background: #4266b2;" type="button" id="btn_update_question" class="btn btn-success btn_update_question">Post</button>
			</div>
		</div>
	</div>
</div><!-- /Question Text Edit -->

<!-- Question Image Edit -->
<div class="modal fade" id="ckeditor-modal-edit-question-image" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close close-edit-question" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Edit your Question</h4>
			</div>

			<div class="modal-body">
				<input type="hidden" name="" id="edit_question_id">
				<div>
					<img src="" alt="" id="img_edited_question" class="img-responsive img_edited_question"><br>
					<span class="btn btn-info remove_btn" id="btn_remove_img_edited_question">Remove</span>
				</div>
				<div id="image_input">
					<label>Upload Files</label>
					<input type="file" name="" id="img_input_edited_question" accept="image/*">

				</div>
			</div>
			<div class="modal-footer">
				<span id="edit_question_error" class="error edit_question_error"></span>
				<button type="button" class="btn btn-default close-edit-question" data-dismiss="modal">Close</button>
				<button style="margin-top: -3px; background: #4266b2;" type="button" id="btn_update_question" class="btn btn-success btn_update_question btn_update_question_image">Post</button>
			</div>
		</div>
	</div>
</div><!-- /Question Image Edit -->
<!-- Comment Text Edit -->
<div class="modal fade" id="modal-edit-comment-text" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close close-edit-answer" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Edit your Comment</h4>
			</div>

			<div class="modal-body">
				<input type="hidden" name="" id="edit_comment_id" class="edit_comment_id">
				<input type="hidden" name="" id="edit_comment_question_id" class="edit_comment_question_id">
				<textarea id="txtEditedComment" class="txtEditedComment" placeholder="Write a comment.." style="width:100%"></textarea>
			</div>
			<div class="modal-footer">
				<span id="edit_comment_error" class="error edit_comment_error" style="float:left;"></span>
				<button type="button" class="btn btn-default close-edit-answer" data-dismiss="modal">Close</button>
				<button style="margin-top: -3px; background: #4266b2;" type="button" id="btn_update_comment" class="btn btn-success btn_update_comment">Post</button>
			</div>
		</div>
	</div>
</div><!-- /Comment Text Edit -->


<script>
	
	//for the copy paste of the image

	(function($) {
		var defaults;
		$.event.fix = (function(originalFix) {
			return function(event) {
				event = originalFix.apply(this, arguments);
				if (event.type.indexOf('copy') === 0 || event.type.indexOf('paste') === 0) {
					event.clipboardData = event.originalEvent.clipboardData;
				}
				return event;
			};
		})($.event.fix);
		defaults = {
			callback: $.noop,
			matchType: /image.*/
		};
		return $.fn.pasteImageReader = function(options) {
			if (typeof options === "function") {
				options = {
					callback: options
				};
			}
			options = $.extend({}, defaults, options);
			return this.each(function() {
				var $this, element;
				element = this;
				$this = $(this);
				return $this.bind('paste', function(event) {
					var clipboardData, found;
					found = false;
					clipboardData = event.clipboardData;
					return Array.prototype.forEach.call(clipboardData.types, function(type, i) {
						var file, reader;
						if (found) {
							return;
						}
						if (type.match(options.matchType) || clipboardData.items[i].type.match(options.matchType)) {
							file = clipboardData.items[i].getAsFile();
							reader = new FileReader();
							reader.onload = function(evt) {
								return options.callback.call(element, {
									dataURL: evt.target.result,
									event: evt,
									file: file,
									name: file.name
								});
							};
							reader.readAsDataURL(file);
							return found = true;
						}
					});
				});
			});
		};
	})(jQuery);



	// $("html").pasteImageReader(function(results) {
	// 	var dataURL, filename;
	// 	filename = results.filename, dataURL = results.dataURL;
	// 	$data.text(dataURL);
	// 	$size.val(results.file.size);
	// 	$type.val(results.file.type);
	// 	$test.attr('href', dataURL);
	// 	var img = document.createElement('img');
	// 	img.src= dataURL;
	// 	var w = img.width;
	// 	var h = img.height;
	// 	$width.val(w)
	// 	$height.val(h);
	// 	var src = dataURL.replace('data:image/png;base64,','')
	// 	$("#copyimage").val(src);
	// 	return $('.active_copypaste').html('<img src="'+dataURL+'" style="width:100%">');
	// });

	var $data, $size, $type, $test, $width, $height;
	$(function() {
		$data = $('.data');
		$size = $('.size');
		$type = $('.type');
		$test = $('#test');
		$width = $('#width');
		$height = $('#height');
		$('.target').on('click', function() {
			var $this = $(this);
			var bi = $this.css('background-image');
			if (bi!='none') {
				$data.text(bi.substr(4,bi.length-6));
			}


			$('.active_copypaste').removeClass('active_copypaste');
			$this.addClass('active_copypaste');

			$this.toggleClass('contain');

			$width.val($this.data('width'));
			$height.val($this.data('height'));
		// if ($this.hasClass('contain')) {
		// 	$this.css({'width':$this.data('width'), 'height':$this.data('height'), 'z-index':'10'})
		// } else {
		// 	$this.css({'width':'', 'height':'', 'z-index':''})
		// }

	})
	});

	$(document).off('click','#copypastebutton');
	$(document).on('click', '#copypastebutton', function(){
		if($('#question_popup_image').attr('src')){
			$('.error_btn_disable').text('You can only upload one media at a time.');
			return false;
		}
		if($(this).is('[disabled=disabled]')){
			$('.error_btn_disable').text('Please fill the question text before selecting any media.');
			return false;
		}
		$('#error_message').text('');
		$('#copyimagepaste').addClass('active_copypaste contain');
		$('#copypaste-modal').modal('show');
	});


	$(document).off('click','#ckeditor');
	$(document).on('click','#ckeditor', function(){
		if($(this).hasClass('archiveckeditor')){
			$('#ckeditor-modal .modal-body').html('<textarea id="questionecktext" class="questionecktext"></textarea>');
			$('#ckeditor-modal .modal-footer').html('<button type="button" class="btn btn-default ckeditor-close" data-dismiss="modal">Close</button><button style="margin-top: -3px; background: #4266b2;" type="button" id="ckeditor_done" data-dismiss="modal" class="btn btn-success btn-send">Post</button>');
		}
		$('#questiontext').val('');
		$('#nepaliquestiontext').val('');
		CKEDITOR.replace('questionecktext');
	// console.log('Testing...');
	$('#ckeditor-modal').modal();

});

	$(document).off('click','#webcambutton');
	$(document).on('click','#webcambutton', function(e){
		if($('#question_popup_image').attr('src')){
			$('.error_btn_disable').text('You can only upload one media at a time.');
			return false;
		}
		if($(this).is('[disabled=disabled]')){
			$('.error_btn_disable').text('Please fill the question text before selecting any media.');
			return false;
		}

		$.post(base_url+'forum/webcam',function(html){

			$('#webcam .modal-body').html(html);
			$('#webcam').modal('show');
		});
	});
	

</script>

