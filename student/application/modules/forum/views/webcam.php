
<!DOCTYPE html>
<html>
<head>
   
    <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
  -->

 
    <script type="text/javascript" src="/assets/js/template/forum/webcam.js"></script>
    <script>
        $(function(){
            //give the php file path
            webcam.set_api_url( 'forum/saveimage' );
            webcam.set_swf_url( '/assets/js/template/forum/webcam.swf' );
            webcam.set_quality( 100 ); // Image quality (1 - 100)
            webcam.set_shutter_sound( true ); // play shutter click sound
 
            var camera = $('#camera');
            camera.html(webcam.get_html(600, 460));
 
            $('#capture_btn').click(function(){
                //take snap
                webcam.snap();
            });
 
            //after taking snap call show image
            webcam.set_hook( 'onComplete', function(img){
                //console.log(img);
                img = img.trim();
                $('#show_saved_img').html('<img src="data:image/png;base64,' + img + '"><input type="hidden" name="webcam_img_show" id="webcam_img_show" value="'+img+'">');
                
                //reset camera for next shot
                webcam.reset();
            });
 
        });
    </script>
    <style>
              #camera_wrapper, #show_saved_img{float:left; width: 100%;}
        #camera_wrapper #camera embed#webcam_movie {
    width: 100%;
    transform: scale(0.7);
    position: absolute;
    left: -100px;
}     #show_saved_img img {
    width: 350;
    margin-left: 15px;
    margin-top: 100px;
    position: absolute;
    right: 10;
}
      #camera_wrapper button {
    margin-top: -10px;
    position: absolute;
    bottom: 40px;
    left: 25px;
}


        
    </style>
</head>
<body> 
    <!-- camera screen -->
    <div id="camera_wrapper">
    <div id="camera"></div>
    <br />
    <button id="capture_btn">Capture</button>
    </div>
    <!-- show captured image -->
    <div id="show_saved_img" >
        
    </div>
</body>
</html>