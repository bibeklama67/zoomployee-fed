<?php
$question = $question_view;
$data = $response;

$num_of_ans = (is_array($data)&&sizeof($data)>0)?sizeof($data):0;
?>

<?php foreach($data as $row){
  if($row['answerbytype']=='STUDENT'){?>
    <div id="answer-indivisual-<?= $row['answerstoquestionsid'];?>" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 answer-indivisual">
      <div class="col-xs-12 bg-student-ans" style="padding-top: 10px;">
       
       <div class="col-md-1 col-xs-1 pull-left" style="padding-right: 0">
        <img src="<?= $row['userimage']?$row['userimage']:base_url('assets/images/dummy-person.jpg')?>" width="40px" height="40px" class="img-responsive"/>    
      </div>
      <div class="col-md-3 col-xs-3 pull-left ">
        <label for="username" style="margin-bottom: 0px;"><a><span style="color:#717B9B; font-size:12px;"><?= $row['username']?></span></a></label><br>
       
        <small style="color:#8f949a"><span><?= $row['dataposteddatetime']?></span> </small><br>
      </div>
      <div class="col-sm-8 col-xs-12">
        <?php
        if($row['status'] == 'P' && $row['answerbyid']==$this->session->userdata('userid')){
          ?>
          <div class="dropdown pull-right">
            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background: #e7defd; margin-top: 5px; border-radius: 0px; border-color: #e7defd;">
              <i class="fa fa-chevron-down" aria-hidden="true" style="color: #034da2;font-size: 10px;margin-top: -5px;"></i>
            </button>
            <ul class="dropdown-menu">
              <li><a href="javascript:;" class="edit_answer" data-questionid="<?= $dataQuestion['questionsofstudentsid']?>" data-ansid="<?= $row['answerstoquestionsid'];?>">Edit</a></li>
              <li><a href="javascript:;" class="delete_answer" data-questionid="<?= $dataQuestion['questionsofstudentsid']?>" data-ansid="<?= $row['answerstoquestionsid'];?>">Delete</a></li>
            </ul>
          </div>
          <?php
        }
        ?>
      </div>

      <div class=" col-sm-10 col-xs-12 answerstext">
        <p id="answerstext<?= $row['answerstoquestionsid'];?>"><?= $row['answertext']?></p>

        <?php 
        if($row['mediatype']=='image'){
          ?>
          <img class="img-responsive" src="<?php echo $row['answerimage']; ?>" style="width:96%">
          <?php
        }
        ?>
        <!-- <a href="javascript:;" class="answer-actions <?= ($row['postliked']=='Y'?'liked':'')?> like-answer" data-refid="<?= $row['answerstoquestionsid']?>" data-likes="<?= $row['likes']?>"><i class="fa fa-thumbs-up"></i></a>
        <a href="javascript:;" class="answer-actions <?= ($row['postliked']=='Y'?'liked':'')?> view-likes" data-refid="<?= $row['answerstoquestionsid']?>" data-actiontype="likes">
          <?php 
          if($row['likes']>1)
            $text = $row['likes'].' Likes';
          else if($row['likes']==1)
            $text = $row['likes'].' Like';
          else
            $text = 'Like';

          ?>
          <span class="likescount"><?= $text?></span>
        </a> -->
      </div>

      </div>
    </div>
  <?php 
  }/*--end answers of student--*/
  else{
    ?>
    <!-- Add Teacher's Answer -->
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-right answer-indivisual " style="margin-top: 10px;">
      <div class=" col-lg-11 col-md-11 col-sm-11 col-xs-11 pull-right teacher_answer_heading">
        <div class="col-lg-8 col-md-8 col-sm-8 colxs-8"></div>
        <?php
        if($prevanswerbyid != $row['answerbyid']){
          $prevanswerbyid = $row['answerbyid'];
          ?>
          <div class="col-md-3 col-xs-3 pull-left midas-user">
            <label class="lblteachername" for="username" style="margin-bottom: 0px;" data-toggle="modal" data-target="#teacherModal" data-answereby="<?php echo $row['answerbyid'] ?>">
              <a><span style="color:#717B9B; font-size:12px;"><?= $row['username']?></span></a>
            </label><br>
            <small style="color:#8f949a"><span><?= $row['dataposteddatetime']?></span> <!-- at <span id="time">11:15pm</span> --></small><br>
          </div>
          <div class="col-md-1 col-xs-1 pull-left" style="padding-right: 0">
            <img src="<?= $row['userimage']?$row['userimage']:base_url('assets/images/dummy-person.jpg')?>" width="40px" height="40px" class="teacher-image img-responsive" data-toggle="modal" data-target="#teacherModal" data-answereby="<?php echo $row['answerbyid'] ?>"/>    
          </div>
          <?php
        }else{

        }?>

      </div>
      <div class=" col-sm-12 col-xs-12 answerstext" >
        <div class="row">
          <div class="col-md-12 col-xs-12 pull-left">
            <p class="para-ans"><?= $row['answertext']?></p>
          </div>
        </div>
        <?php 
        if($row['mediatype']=='image'){
          ?>
          <div class="row ">
            <div class="col-md-12">
              <img class="img-responsive pull-right " src="<?php echo $row['answerimage']; ?>" style="width:96%">
            </div>
          </div>
          <?php
        }
        if($row['mediatype']=='video'){

          if($row['videotype']=='youtube'||$row['videotype']=='vimeo'){
            $embedURL = getEmbedUrl($row['answervideolink']);
            ?>
            <div class="row ">
              <div class="col-md-12" style="text-align: right;">
                <iframe width="" height="" src="<?php echo $embedURL; ?>"></iframe>
              </div>
            </div>
            <?php
          }
        }
        ?>
        <div class='row'>
          <div class="col-md-12">
            <!-- <a href="javascript:;" class="answer-actions <?= ($row['postliked']=='Y'?'liked':'')?> like-answer" data-refid="<?= $row['answerstoquestionsid']?>" data-likes="<?= $row['likes']?>"><i class="fa fa-thumbs-up"></i></a>
            <a href="javascript:;" class="answer-actions <?= ($row['postliked']=='Y'?'liked':'')?> view-likes" data-refid="<?= $row['answerstoquestionsid']?>" data-actiontype="likes">
              <?php 
              if($row['likes']>1)
                $text = $row['likes'].' Likes';
              else if($row['likes']==1)
                $text = $row['likes'].' Like';
              else
                $text = 'Like';

              ?>
              <span class="likescount"><?= $text?></span> -->
            </a>
          </div>
        </div>
      </div>
      
    </div>
    <?php
  }/*--end of teacher answers---*/
}?>
<div class="new-anslist"></div>
<?php
if($hasteacheranswer=='Y' && strcasecmp('success', $responseType)==0 && $num_of_ans>0){
  ?>
  <div class="col-sm-12">
    <div class="col-sm-12 col-xs-12 dtahy">
      <div class="pull-right">
        <strong class="dtahytxt">Does this answer help you?</strong><br>
        <div class="acidjs-rating-stars my" style="margin-left: 30px;">
          <form style="">
            <input type="hidden" id="ratyQId" value="<?php echo $questionid;?>">
            <input type="radio" class="rating-radio" name="group-<?= $questionid?>" data-refid="<?= $questionid?>" id="group-<?= $questionid?>-0" value="5" <?php echo $rating==5?checked:''; ?>><label for="group-<?= $questionid?>-0"></label>
            <input type="radio" class="rating-radio" name="group-<?= $questionid?>" data-refid="<?= $questionid?>" id="group-<?= $questionid?>-1" value="4" <?php echo $rating==4?checked:''; ?>><label for="group-<?= $questionid?>-1"></label>
            <input type="radio" class="rating-radio" name="group-<?= $questionid?>" data-refid="<?= $questionid?>" id="group-<?= $questionid?>-2" value="3" <?php echo $rating==3?checked:''; ?>><label for="group-<?= $questionid?>-2"></label>
            <input type="radio" class="rating-radio" name="group-<?= $questionid?>" data-refid="<?= $questionid?>" id="group-<?= $questionid?>-3" value="2" <?php echo $rating==2?checked:''; ?>><label for="group-<?= $questionid?>-3"></label>
            <input type="radio" class="rating-radio" name="group-<?= $questionid?>" data-refid="<?= $questionid?>" id="group-<?= $questionid?>-4" value="1" <?php echo $rating==1?checked:''; ?>><label for="group-<?= $questionid?>-4"></label>
          </form>
        </div>
      </div>
    </div>
  </div>
  <?php
}
//echo '<pre>';var_dump($this->session->userdata());echo '</pre>';
?>

<script>
  //on add answer change the question part
  var question = '<?= $question?>';
  $('#accordion<?= $questionid?> .question-points-wrapper').html(question);
</script>
