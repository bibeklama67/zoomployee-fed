<?php 
if(!defined('BASEPATH')) exit('direct access invalid');

class Forum_model extends CI_Model
{
  
  function __construct()
  {
    parent::__construct();
    $db = $this->load->database('default', TRUE);
        // $this->otherdb = $this->load->database('otherdb',TRUE);
        // $this->smsdb =  $this->load->database('smsdb',TRUE);
    
  }

  function get_question_detail($questionsofstudentsid){
    
    $sql = $this->db->query("select DISTINCT q.questionsofstudentsid as qid, q.*,to_char(qf.dataposteddatetime,'YYYY-MM-DD') || ' ' || trim(to_char(qf.dataposteddatetime, 'HH12:MI AM'), '0') as dataposteddatetime,COALESCE(totr.averagerating,0) as averagerating, COALESCE(totr.totalrating,0) as totalrating, c.classname, qf.eclassid from forum.el_questionsof_students as q join forum.el_questionsfrom as qf on qf.questionsofstudentsid = q.questionsofstudentsid join org_class as c on c.\"CLASSTAG\"= qf.classtag LEFT JOIN (select sum(rating)/count(*) as averagerating,count(*) as totalrating,questionsofstudentsid from forum.el_questionsrating group by questionsofstudentsid) totr on totr.questionsofstudentsid = q.questionsofstudentsid where q.questionsofstudentsid= $questionsofstudentsid and q.status != 'C'");

    $result = $sql->row();

    if($result)
    {
      return $result;
    }
    else{

      return array();
    }
  }
}
