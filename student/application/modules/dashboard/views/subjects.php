<style>
 img.loader1{
  position: fixed;
  top: 280px;
  right: 47%;
  z-index: 999;
}
</style>
<?php 
if(empty($response))
  // echo 'No Subjects Available';
  echo '';
else{

  ?>
  <img  style="display: none;" class="loader1" width="80px" src="<?=base_url().'/assets/images/loader4.gif'?>"/>
  <div class="row-wrapper">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pad-fix">
      <?php
      if($this->session->userdata('classtype') != "" && $this->session->userdata('classtype')=='CC'){?>
      
      <h5  class="bread" id="<?= $response[0]['classid']; ?>"><?= $response[0]['classname']; ?>
      </h5>
    </div> 

    <?php }

    else{

     if($response[0]['classid']==$response[count($response)-1]['classid'])
     {
      ?>
      <h5 class="bread" id="<?= $response[0]['classid']; ?>">Grade <?= $response[0]['classname']; ?>
      </h5>
      <?php 
    }
  }?>
</div>
<?php
foreach ($response as $row) {
  if($response[0]['classid']==$response[count($response)-1]['classid'])  
    $subject = explode('-', $row['subjectname']);
  else
    $subject[0] = $row['subjectname']; 
  ?>   
  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 dia-wrap" >
    <a formdest=".mainform" data-subjectid="<?php echo $row['subjectid'];?>" data-classname="<?php echo $row['classname']; ?>" data-subjectname="<?php echo $row['subjectname'];?>" data-level="easy" data-view="keypoints" data-videotype="" href="dashboard/listChapters/" class="subjectbtn <?= $source?>" data-classid="<?= $row['classid'];?>">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bord-dia pad-fix subject-class-height">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  bg-tit-dia pad-fix" >

          <h6 class="head-tit-dia"><?php echo $subject[0];?></h6> 
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 subject-details" style="padding-left:0px;">

          <table>
            <tr>
              <td style="padding:5px;" width="91">

                <span>
                  <?php
                  if(trim($row['image']))
                    $image = $row['image'];
                  else
                    $image = base_url()."assets/images/subjectimg.png";

                  
                  ?>
                  <img src="<?= $image?>" class="img-responsive pull-right " style="width: 81px;height: 81px;     transform: scale(0.9);">
                  <!--<h2 style="border:3px solid #dadada;border-radius:50px; padding:15px;"> <?php //echo round($row['progress'],2);?>% <?php echo $row['chaptercount']; ?></h2>-->
                </span>

              </td>
              <td style="color:#8c8b8b; padding-left: 15px; font-size: 14px; font-weight: bold;">              
               <p> <?= ($source=='HH'?$row['qaprogress']:$row['chaptercount'].' Chapters') ; ?></p>
             </td>
           </tr>
         </table>
       </div>

   <!--  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <a formdest=".mainform" data-subjectid="<?php echo $row['subjectid'];?>" data-subjectname="<?php echo $row['subjectname'];?>" data-level="easy"   data-view="keypoints" data-videotype="" href="dashboard/listChapters/" class="subjectbtn btn btn-danger btn-cust start-button"><span class="home-start"><?php if($row['progress']=='0') echo 'Study Now'; else echo  'Resume'?></span> <img src="<?php echo image_url();?>arrow.png" class="pull-right"/></a>
  </div> -->

</div>
</a>
</div>
<?php }
}?>    


<?php
foreach ($eclassdata as $class) {?>
<?php
if($class['classid'] != $response[0]['classid']){
  if (in_array($source, $class['hiddenin']))
    continue;
  ?>
  <div class="row-wrapper">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pad-fix sa<?= $class['classid']; ?>">
      <?php if($class['CLASSTAG'] != "" && $class['CLASSTAG'] !='null'){?>
      <h5 class="bread subjectaccordian " id="<?= $class['classid']; ?>" data-toggle="collapse" style="cursor:pointer;">Grade <?= $class['classname']; ?></h5>
      <?php }
      else{
        ?>
        <h5 class="bread subjectaccordian" id="<?= $class['classid']; ?>" data-toggle="collapse" style="cursor:pointer;"><?= $class['classname']; ?></h5>
        <?php }?>
      </h5>
    </div>
    <div id="classselected<?= $class['classid']; ?>" class="collapse subjectwrapper">

      <!-- <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 dia-wrap " ></div> -->
    </div>
  </div>
  <?php
}
}
?>


<style type="text/css">
  .clickable.btn.btn-danger.btn-cust.start-button:hover{
    /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#aa0000+0,aa0000+7,c60102+40,c90303+47,d21715+50,d92e2c+53,dc3836+57,e84644+70,f95c5d+97,da4a45+100 */
    background: #aa0000; /* Old browsers */
    background: -moz-linear-gradient(top,  #aa0000 0%, #aa0000 7%, #c60102 40%, #c90303 47%, #d21715 50%, #d92e2c 53%, #dc3836 57%, #e84644 70%, #f95c5d 97%, #da4a45 100%); /* FF3.6-15 */
    background: -webkit-linear-gradient(top,  #aa0000 0%,#aa0000 7%,#c60102 40%,#c90303 47%,#d21715 50%,#d92e2c 53%,#dc3836 57%,#e84644 70%,#f95c5d 97%,#da4a45 100%); /* Chrome10-25,Safari5.1-6 */
    background: linear-gradient(to bottom,  #aa0000 0%,#aa0000 7%,#c60102 40%,#c90303 47%,#d21715 50%,#d92e2c 53%,#dc3836 57%,#e84644 70%,#f95c5d 97%,#da4a45 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#aa0000', endColorstr='#da4a45',GradientType=0 ); /* IE6-9 */

  }

</style>
<script>

</script>