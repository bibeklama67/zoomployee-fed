<style>
  h5.bread{background:#eef1f6; font-weight:bold; padding:10px 5px; margin:2px 5px; color:#418755 !important;}
  .progressbar{padding:0;}
  .bread_wrap{border-bottom:1px solid #dfe4e8 !important;}
  h5 span.active{color:#526b81;}
  ul.chaps_list li{list-style:none; background:#f1f1f1;padding: 5px 10px;
    margin: 2px auto;}
    ul.chaps_list li:nth-child(even){
      background:#fbfbfb;
    }
    .progress{margin-top:5px;}
    .mar-top-10{margin-top:10px;}
    img.ads{
      border:1px solid #d5d5d5; margin:0 auto;
    }
    span.pro_per{color:#d57656;}
  </style>
  <?php 
 // echo'<pre>';
 //  print_r($response);
 //  exit;
  

  if(empty($response))
    echo 'No Chapters Available';
  else{
    $subject = explode('-', $subjectname);
    ?>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pad-fix bread_wrap">
      <h5 class="bread"><a href="javascript:;" data-classid="<?= @$classid?>" class="classbtn breadcrumb"><?php if($this->session->userdata('classtype') != "" && $this->session->userdata('classtype')=='CC'){?><?= $classname ?><?php }else{ ?>Grade <?= $classname ?><?php }?></a>><span class="active"><a href="javascript:;" class="subjectbtn breadcrumb" data-subjectname="<?= $subjectname?>" data-subjectid="<?= $subjectid?>" data-classname="<?= $classname ?>"><?php echo isset($subjectname)?$subject[0]:''?></a></span>
	  <a href="javascript:;" data-classid="<?= @$classid?>" class="pull-right mini-home-btn classbtn breadcrumb"><img src="<?= base_url()?>assets/images/home-btn.png" class="homepng"> </a>
      </h5>
    </div>
    <input type="hidden" id="subjectname" value="<?= $subjectname?>">


    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pad-fix mar-top-10">
      <div class="col-lg-9">
        <ul class="chaps_list">
         <?php
         $i=1;
         foreach ($response as $row) {
          $class='';
          if($source=='HH' && $row['chaptertype'] != 2){
            $class ='btn-chapter-hh';
          }else if($row['chaptertype'] ==2){
            $class ='unlockchap';
          }else{
            $class ='chapterbtn';
          }
          ?> 
          <li class="<?= $row['haschildchapters']=='Y'?'childchaps':''?>">
           <a formdest=".mainform" data-classid="<?= @$classid?>"  data-subjectid="<?php echo $row['subjectid'];?>" data-chapterid="<?php echo $row['chapterid'];?>" data-classname="<?php echo $classname; ?>" data-chaptername="<?php echo $row['chaptername'];?>" data-level="easy" data-view="keypoints" data-videotype="" href="#" class="<?php echo $class?>">
            <div class="col-lg-12 pad-fix">
              <div class="col-lg-7">
               <div class="  col-md-9 col-sm-8 col-xs-12">
                <h5><?php echo $i; ?>. <?php echo $row['chaptername'];?></h5>
              </div>
              <div class=" col-md-3 col-sm-4 col-xs-12 no-padding">
              <?php 
              switch($source){
                case 'LC':
                $chaptertext = ($row['chaptertext']?'('.$row['chaptertext'].')':'');
                break;
                case 'HH':
                $chaptertext = $row['chaptertext'];
                break;
                default:
                $chaptertext = '--'.$row['topiccount'].' Topics'.'--';
                break;
                }?>
               <h6 style="color:black; font-size:12px;"><?= $chaptertext ?></h6>	
             </div>

           </div>
           <div class="col-lg-5">
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 pad-fix">
              <div class="progress">
                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo round($row['progress'],2);?>%;">
                  <span class="sr-only"><?php echo round($row['progress'],2);?>%Complete</span>
                </div>
              </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
              <span class="pro_per"><?php echo round($row['progress'],2);?>%</span>

            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
              <?php if($row['chaptertype'] ==2){?>
              <i class="fa fa-lock" aria-hidden="true"></i>
              <?php }?>
            </div>
          </div>
        </div>
      </a>
      <div class="clearfix"></div>
    </li>
    <?php $i++; }

  }?>   
</ul>
</div>
<?php

if($source=='HH' || $source=='LC')
{
  $this->load->vars(array('schedule'=>$response[0]['subjectschedule']));
  $this->load->view('templates/sidebar_new');
}
else
  $this->load->view('templates/sidebar');
?>

</div>

<div id="purchase_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">

      <div class="modal-body">
       <div class="wrapper purchasemsg">
         <div class="inner-wrap">
           <div class="text-center">
            <p class="pur-msg"><span class="basicp" >
             Thank you for showing the interest to purchase the content for MiDas – The Learning App.
           </span><br>
           <span class="basicp">
             We have received your purchase request. <br>We will contact you shortly. <br>
             Thank You!
           </span> </p>
         </div>

       </div>


     </div>
   </div>
   <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
  </div>
</div>

</div>
</div>
</div>
<!-- <script type="text/javascript">
      $(document).off('click', '.unlockchap');
$(document).on('click', '.unlockchap', function(){
  console.log('Testing...Testing..');
  //$('#ckeditor-modal').modal('show');
  //return confirm('Are you sure?');
  console.log($(this));
    var chaptername = $(this).data('chaptername');
  $("#subscription-dialog").dialog({
      title: chaptername,
      modal: true,      
      open: function() {
        $(this).html('Please purchase this chapter to view it\'s content.');
      },
      buttons : {
        "Purchase" : function() {
          console.log('Go to purchase page')
        },
        "Cancel" : function() {
          $(this).dialog("close");
        }
      }
    });
});

</script> -->