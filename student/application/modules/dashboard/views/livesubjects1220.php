<style type="text/css">
  .prevclass
  {
    color: #2f8eed !important;
    margin-bottom: 0;
    padding: 0px 3px !important;
  }

  .subtext{
    float: right;
    text-align: left;
    margin-left: 10px;
    color: #8c8b8b;
    padding-right: 5px;
    font-size: 13px;
    font-weight: bold; 
    margin-top: 0;
    /*text-align: center;*/
  }
</style>
<?php 


$showdemo = true;
$liveclasslist=[];
foreach ($response as $row) {
  if($row['islive'] == 'Y')
  {
    $showdemo = false;
    array_push($liveclasslist,$row);
   // break;
  }
}
// $demomessage = '<div class="col-xs-12"><h3 class="text-center"><strong style="color: #f80100">Classes are LIVE from 3 PM to 9 PM (Sunday to Friday)</strong><p>Sample LIVE Class.</p></h3></div>';
$samplelive = '<a href="javascript:;" class="btn btn-success" id="samplelive">Sample LIVE Class</a>';
$demomessage = '<div class="col-xs-12"><h3 class="fverdana livcc">
<strong style="color: #e70103"></strong>
</h3><h3 class="text-center"><strong style="color: #f80100">Classes are LIVE from 5 PM to 9 PM (Sunday to Friday)</strong><p style="font-size: 20px;margin: 12px;color: #895f5f;">Today\'s schedule: Science (5:30 PM to 7:30 PM), Nepali (6 PM to 8 PM), Maths (7 PM to 9 PM)</p>'.($this->session->userdata('userid')=='11000000011'?$samplelive:'').'</h3></div>';
 // $demomessage = '<div class="col-xs-12"><h3 class="text-center"><p><strong style="color: #f80100">On the auspicious occasion of Chhath Parwa, LIVE Class is not available today.</strong></p>'.($this->session->userdata('userid')=='11000000011'?$samplelive:'').'</h3></div>';

if($liveclasslist){

  ?>
  <div class="row-wrapper">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  pad-fix" >
      <?php
      if($this->session->userdata('classtype') != "" && $this->session->userdata('classtype')=='CC'){?>

      <h5  class="bread" id="<?= $response[0]['classid']; ?>"><?= $liveclasslist[0]['classname']; ?>
      </h5>
    </div> 

    <?php }

    else{
     if($liveclasslist[0]['classid']==$liveclasslist[count($liveclasslist)-1]['classid'])
     {
      ?>
      <h5 class="bread" id="<?= $response[0]['classid']; ?>">Grade <?= $liveclasslist[0]['classname']; ?>
      </h5>
      <?php 
    }
  }?>
</div>
<div class="col-md-12 pad-fix">
  <?php
// if($showdemo)
//echo $demomessage;

  foreach ($liveclasslist as $row) {
    if($liveclasslist[0]['classid']==$liveclasslist[count($liveclasslist)-1]['classid'])  
      $subject = explode('-', $row['subjectname']);
    else
      $subject[0] = $row['subjectname']; 
    ?>  
    <div class="">
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 dia-wrap" >
        <?php
        if($row['islive'] == 'Y'){
          $url = "liveclass/whiteboard";
          $class = "subjectbtn ".$source.' '.'islive';
        }
        else if($row['showsession']=='N')
        {
          $url = "javascript:;";
          $class = "available-soon";
        }
        else{
          $url = "dashboard/listChapters";
          $class = "subjectbtn ".$source;
        }
        ?>
        <a formdest=".mainform" data-subjectid="<?php echo $row['subjectid'];?>" data-classname="<?php echo $row['classname']; ?>" data-subjectname="<?php echo $row['subjectname'];?>" data-level="easy" data-view="keypoints" data-videotype="" href="<?= $url?>" class="<?= $class?>" data-sessionid = "<?= $row['sessionid']?>" data-classid="<?= $row['classid'];?>">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bord-dia pad-fix subject-class-height">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  bg-tit-dia pad-fix" >

              <h6 class="head-tit-dia"><?php echo $subject[0];?></h6> 
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 subject-details" style="padding-left:0px;">

              <table>
                <tr>
                  <td style="padding:5px;" >

                    <span>
                      <?php
                      if(trim($row['image']))
                        $image = $row['image'];
                      else
                        $image = base_url()."assets/images/subjectimg.png";
                      ?>
                      <img src="<?= $image?>" class="img-responsive pull-right " style="width: 81px;height: 81px;     transform: scale(0.9);">
                      <!--<h2 style="border:3px solid #dadada;border-radius:50px; padding:15px;"> <?php //echo round($row['progress'],2);?>% <?php echo $row['chaptercount']; ?></h2>-->
                    </span>

                  </td>
                  <?php
                  if($row['islive'] == 'Y'){
                    ?>
                    <td>
                      <!--                  <p class="livenow"> Live Now</p>-->
                      <img src="<?= base_url()?>assets/images/livennow-btn.png" class="live-btn">
                    </td>
                    <?php
                  }else{
                    ?>
                    <td>
                      <?php  
                  //if($row['livetimings']){
                      ?>
                      <!-- <p class="livetime">LIVE ON <?php echo '<br>'.$row['livetimings'];?></p> -->
                      <?php if($row['showsession']=='Y'){?>
                      <p class="livetime prevclass">Previous Class</p>
                      <p class="subtext"><?= ($row['qaprogress'])?></p>
                      <?php }
                      else{?>
                      <p class="livetime">Will Be LIVE Soon</p>
                      <?php 
                    }
                    ?>
                  </td>
                  <?php
                }
                ?>

              </tr>
            </table>
          </div>



        </div>
      </a>
    </div>
  </div> 
  <?php 
}
}
else{
//  if($showdemo)
  //echo $demomessage;

}


if($response){

  ?>

  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
    <h3 class="fverdana reccl" style="border-top: 2px solid #b4cae1;"><strong style="color: #2f8eed"></strong></h3>
  </div>
  <div class="">
    <?php

    foreach ($response as $row) {
      if($response[0]['classid']==$response[count($response)-1]['classid'])  
        $subject = explode('-', $row['subjectname']);
      else
        $subject[0] = $row['subjectname']; 
      ?> 
      
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 dia-wrap" style=" margin-bottom: 10px;">
        <?php
        if($row['islive'] == 'Y'){
          $url = "liveclass/whiteboard";
          $class = "subjectbtn ".$source;
        }
        else if($row['showsession']=='N')
        {
          $url = "javascript:;";
          $class = "available-soon";
        }
        else{
          $url = "dashboard/listChapters";
          $class = "subjectbtn ".$source;
        }
        ?>
        <a formdest=".mainform" data-subjectid="<?php echo $row['subjectid'];?>" data-classname="<?php echo $row['classname']; ?>" data-subjectname="<?php echo $row['subjectname'];?>" data-level="easy" data-view="keypoints" data-videotype="" href="<?= $url?>" class="<?= $class?>" data-sessionid = "<?= $row['sessionid']?>" data-classid="<?= $row['classid'];?>">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bord-dia pad-fix subject-class-height">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  bg-tit-dia pad-fix" >

              <h6 class="head-tit-dia"><?php echo $subject[0];?></h6> 
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 subject-details" style="padding-left:0px;">

              <table>
                <tr>
                  <td style="padding:5px;" >

                    <span>
                      <?php
                      if(trim($row['image']))
                        $image = $row['image'];
                      else
                        $image = base_url()."assets/images/subjectimg.png";
                      ?>
                      <img src="<?= $image?>" class="img-responsive pull-right " style="width: 81px;height: 81px;     transform: scale(0.9);">
                      <!--<h2 style="border:3px solid #dadada;border-radius:50px; padding:15px;"> <?php //echo round($row['progress'],2);?>% <?php echo $row['chaptercount']; ?></h2>-->
                    </span>

                  </td>
                  
                  <td>
                    <?php  
                  //if($row['livetimings']){
                    ?>
                    <!-- <p class="livetime">LIVE ON <?php echo '<br>'.$row['livetimings'];?></p> -->
                    <?php if($row['showsession']=='Y'){?>
                    <p class="livetime prevclass"><?= ($row['qaprogress'])?></p>
                    <!-- <p class="subtext"><?= ($row['qaprogress'])?></p> -->
                    <?php }
                    else{?>
                    <p class="livetime">Will Be LIVE Soon</p>
                    <?php 
                  }
                  ?>
                </td>
                <?php
                
                ?>

              </tr>
            </table>
          </div>



        </div>
      </a>
    </div>
    
    <?php 
  }?>
</div>
<?php }

?>

<div style="display: none"><?php echo '<pre>'; print_r($allsubjects); echo '</pre>'; echo $this->session->userdata('eclassid');?> </div>
<?php
foreach ($allsubjects as $class) {

  ?>
  <div class="row-wrapper">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pad-fix">
      <?php if($class['CLASSTAG'] != "" && $class['CLASSTAG'] !='null'){?>
      <h5 class="bread subjectaccordian " id="<?= $class['id']; ?>" data-toggle="collapse" style="cursor:pointer;">Grade <?= $class['classname']; ?></h5>
      <?php }
      else{
        ?>
        <h5 class="bread subjectaccordian" id="<?= $class['id']; ?>" data-toggle="collapse" style="cursor:pointer;"><?= $class['classname']; ?></h5>
        <?php }?>
      </h5>
    </div>
    <div id="classselected<?= $class['id']; ?>" class="collapse subjectwrapper">
  </div>
  <?php
}
?>

<!-- livesample modal -->

<div class="modal fade" id="livesample_modal" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
<!--
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
    -->
    <div class="modal-body">

      <video controls controlsList="nodownload" width="100%" height="88%" id="livesamplevideo">
        <source src="http://vstore.midas.com.np:81/vstore/liveclass/science/Grid%20for%20Science%20Final.mp4" type="video/mp4">
          <source src="http://vstore.midas.com.np:81/vstore/liveclass/science/Grid%20for%20Science%20Final.mp4" type="video/ogg">
            Your browser does not support the video tag.
          </video>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>


  <!-- SUBSCRIPTION DIALOG -->

  <div id="subscription-dialog"></div>
</div>
<div class="clearfix"></div>
<style type="text/css">
  .clickable.btn.btn-danger.btn-cust.start-button:hover{
    /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#aa0000+0,aa0000+7,c60102+40,c90303+47,d21715+50,d92e2c+53,dc3836+57,e84644+70,f95c5d+97,da4a45+100 */
    background: #aa0000; /* Old browsers */
    background: -moz-linear-gradient(top,  #aa0000 0%, #aa0000 7%, #c60102 40%, #c90303 47%, #d21715 50%, #d92e2c 53%, #dc3836 57%, #e84644 70%, #f95c5d 97%, #da4a45 100%); /* FF3.6-15 */
    background: -webkit-linear-gradient(top,  #aa0000 0%,#aa0000 7%,#c60102 40%,#c90303 47%,#d21715 50%,#d92e2c 53%,#dc3836 57%,#e84644 70%,#f95c5d 97%,#da4a45 100%); /* Chrome10-25,Safari5.1-6 */
    background: linear-gradient(to bottom,  #aa0000 0%,#aa0000 7%,#c60102 40%,#c90303 47%,#d21715 50%,#d92e2c 53%,#dc3836 57%,#e84644 70%,#f95c5d 97%,#da4a45 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#aa0000', endColorstr='#da4a45',GradientType=0 ); /* IE6-9 */

  }

</style>