<style>
h5.bread{background:#eef1f6; font-weight:bold; padding:10px 5px; margin:2px 5px; color:#418755 !important;}
.progressbar{padding:0;}
.bread_wrap{border-bottom:1px solid #dfe4e8 !important;}
h5 span.active{color:#526b81;}
ul.chaps_list li{list-style:none; background:#f1f1f1;padding: 5px 10px;
  margin: 2px auto;}
  ul.chaps_list li:nth-child(even){
    background:#fbfbfb;
  }
  .progress{margin-top:5px;}
  .mar-top-10{margin-top:10px;}
  img.ads{
    border:1px solid #d5d5d5; margin:0 auto;
  }
  span.pro_per{color:#d57656;}
  </style>

  <?php
    $subject = explode('-', $subjectname);
    ?>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pad-fix bread_wrap">
      <h5 class="bread"><a href="javascript:;" data-classid="<?= $this->session->userdata('eclassid')?>" class="classbtn breadcrumb">Grade <?= $classname ?></a>><span class="active"><a href="javascript:;" class="subjectbtn breadcrumb" data-subjectname="<?= $subjectname?>" data-subjectid="<?= $subjectid?>" data-classname="<?= $classname ?>"><?php echo isset($subjectname)?$subject[0]:''?></a></span>
      </h5>
    </div>
    <input type="hidden" id="subjectname" value="<?= $subjectname?>">


    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pad-fix mar-top-10">
      <div class="col-lg-9">
        <?php echo $html; ?>
      </div>
      <?php $this->load->view('templates/sidebar')?>

    </div>
    
