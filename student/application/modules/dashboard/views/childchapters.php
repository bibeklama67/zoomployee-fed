<style>
ul.sub.chaps_list li:nth-child(even) {
/*       background: #cfcfcf !important;*/
}
  ul.sub.chaps_list li{
        background: #f3f3f3 !important;
/*    border-radius: 5px;*/
}
  
  ul.sub.chaps_list li .progress {
    margin-top: 4px;
}
  ul.sub.chaps_list li h5, ul.sub.chaps_list li h6{
      margin: 5px 0px;}
  
  
  ul.sub.chaps_list{
    padding:15px 50px 15px;
/*        background: #e3e3e3;*/
/*    padding-top: 0px;*/
  }
  .wrapaccordian.childchaps .collapse.in{
/*
        border: 1px solid #ddd;
    margin-top: -2px;
    margin-bottom: 10px;
    border-top:0px;
*/
  }
  .wrapaccordian.childchaps .collapse.in p{
         padding-left: 10px;
    text-align: -webkit-center;
    margin-top: 5px;
    margin-bottom: 5px;
  }
</style>
      <ul class="sub chaps_list" >
       <?php
       $i=1;
       foreach ($response as $row) {
        $haschildchapters =$row['haschildchapters'];
        $class='';
        if($source=='HH' && $row['chaptertype'] != 2){
          $class ='btn-chapter-hh';
        }else if($row['chaptertype'] ==2){
          $class ='unlockchap';
        }else if($haschildchapters == 'Y'){
          $class ='chapteraccordiancollapse';
        }else{
          $class ='chapterbtn';
        }

        if($haschildchapters == 'Y'){
          $chapteraccordian='data-toggle="collapse" data-target="#chapteraccordian'.$row['chapterid'].'"';
        }else{
          $chapteraccordian='';
        }
        ?> 
        <li>
         <a formdest=".mainform" data-classid="<?= @$classid?>"  data-subjectid="<?php echo $row['subjectid'];?>" data-chapterid="<?php echo $row['chapterid'];?>" data-classname="<?php echo $classname; ?>" data-subjectname="<?= $subjectname?>" data-chaptername="<?php echo $row['chaptername'];?>" data-level="easy" data-view="keypoints" data-videotype="" href="#" class="<?php echo $class?>" <?= $chapteraccordian?>>
          <div class="col-lg-12 pad-fix">
            <div class="col-lg-12">
             <div class="  col-md-5 col-sm-8 col-xs-12">
              <h5><?php echo $i; ?>. <?php echo $row['chaptername'];?></h5>
              <div id="chapteraccordian<?=$row['chapterid']?>" class="collapse"></div>
            </div>
            <div class=" col-md-2 col-sm-3 col-xs-12 no-padding">
            <?php 
              switch($source){
                case 'LC':
                $chaptertext = ($row['chaptertext']?'('.$row['chaptertext'].')':'');
                break;
                case 'HH':
                $chaptertext = $row['chaptertext'];
                break;
                default:
                $chaptertext = '--'.$row['topiccount'].' Topics'.'--';
                break;
                }?>
               <h6 style="color:black; font-size:12px;"><?= $chaptertext ?></h6> 
           </div>
        <div class="col-lg-5">
          <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 pad-fix">
            <div class="progress">
              <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo round($row['progress'],2);?>%;">
                <span class="sr-only"><?php echo round($row['progress'],2);?>%Complete</span>
              </div>
            </div>
          </div>
          <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
            <span class="pro_per"><?php echo round($row['progress'],2);?>%</span>

          </div>

          <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
            <?php if($row['chaptertype'] ==2){?>
            <i class="fa fa-lock" aria-hidden="true"></i>
            <?php }?>
          </div>

        </div>
         </div>
         
      </div>
    </a>
    <div class="clearfix"></div>
  </li>
  <?php $i++; }?>   
</ul>


