<style>
  h5.bread{background:#eef1f6; font-weight:bold; padding:10px 5px; margin:2px 5px; color:#418755 !important;}
  .progressbar{padding:0;}
  .bread_wrap{border-bottom:1px solid #dfe4e8 !important;}
  h5 span.active{color:#526b81;}
  ul.chaps_list li{list-style:none; background:#f1f1f1;padding: 5px 10px;
    margin: 2px auto;}
    ul.chaps_list li:nth-child(even){
      background:#fbfbfb;
    }
    .progress{margin-top:5px;}
    .mar-top-10{margin-top:10px;}
    img.ads{
      border:1px solid #d5d5d5; margin:0 auto;
    }
    span.pro_per{color:#d57656;}
  </style>
  <?php 

 // echo'<pre>';
 //  print_r($response);
 //  exit;

  

  if(empty($response))
    echo 'No Chapters Available</p>';
  else{
    $subject = explode('-', $subjectname);
    ?>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pad-fix bread_wrap">
      <h5 class="bread">
        <a href="javascript:;" data-classid="<?= @$classid?>" class="classbtn breadcrumb"><?php if($this->session->userdata('classtype') != "" && $this->session->userdata('classtype')=='CC'){?><?= $classname ?>
          <?php }else{ ?>Grade <?= $classname ?><?php }?></a>><span class="active"><a href="javascript:;" class="usersubjectbtn breadcrumb" data-subjectname="<?= $subjectname?>" data-subjectid="<?= $subjectid?>" data-classname="<?= $classname ?>"><?php echo isset($subjectname)?$subject[0]:''?></a></span>
          <a href="<?= base_url()?>" class="pull-right mini-home-btn"><img src="<?= base_url()?>assets/images/home-btn.png" class="homepng"> </a>
        </h5>
      </div>
      <input type="hidden" id="subjectname" value="<?= $subjectname?>">


      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pad-fix mar-top-10">
        <div class="col-lg-9">
          
          <ul class="chaps_list">
           
          </ul>
        </div>
        <?php
      }
      if($source=='HH' || $source=='LC')
      {
        $this->load->vars(array('schedule'=>$response[0]['subjectschedule']));
        $this->load->view('templates/sidebar_new');
      }
      else
        $this->load->view('templates/sidebar');
      ?>

    </div>

    <div id="purchase_modal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">

          <div class="modal-body">
           <div class="wrapper purchasemsg">
             <div class="inner-wrap">
               <div class="text-center">
                <p class="pur-msg"><span class="basicp" >
                 Thank you for showing the interest to purchase the content for MiDas – The Learning App.
               </span><br>
               <span class="basicp">
                 We have received your purchase request. <br>We will contact you shortly. <br>
                 Thank You!
               </span> </p>
             </div>

           </div>


         </div>
       </div>
       <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
      </div>
    </div>

  </div>
</div>
<!-- accordian for chapter starts -->

<!-- accordian for chapter ends -->
</div>
<!-- <script type="text/javascript">
      $(document).off('click', '.unlockchap');
$(document).on('click', '.unlockchap', function(){
  console.log('Testing...Testing..');
  //$('#ckeditor-modal').modal('show');
  //return confirm('Are you sure?');
  console.log($(this));
    var chaptername = $(this).data('chaptername');
  $("#subscription-dialog").dialog({
      title: chaptername,
      modal: true,      
      open: function() {
        $(this).html('Please purchase this chapter to view it\'s content.');
      },
      buttons : {
        "Purchase" : function() {
          console.log('Go to purchase page')
        },
        "Cancel" : function() {
          $(this).dialog("close");
        }
      }
    });
});

</script> -->

<script type="text/javascript">
  $(document).ready(function(){
    // $('.chapteraccordiancollapse').trigger('click');
  });

</script>

