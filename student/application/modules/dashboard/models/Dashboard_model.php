<?php 
if(!defined('BASEPATH')) exit('direct access invalid');

class Dashboard_model extends CI_Model
{

  function __construct()
  {
    parent::__construct();
    $db = $this->load->database('default', TRUE);
    $this->sims = $this->load->database('sims', TRUE);
        // $this->otherdb = $this->load->database('otherdb',TRUE);
        // $this->smsdb =  $this->load->database('smsdb',TRUE);

  }

  function getSchoolList()
  {
    $post['districtid'] = $this->session->userdata('districtid');
    
    if($post['districtid']==27)
      $dist = 'o.districtid in (27,28,29)';
    else
      $dist = array('o.districtid'=>$post['districtid']);
    $cond = array('istemp'=>'N','exclude'=>'N','mergedfrom'=>null);
    $this->sims->select('organizationid,organizationname,districtname,case when address is null then \' \' else address end as address',false)->join('district d','d.districtid=o.districtid','left')->from('org_organization o')->where("length(organizationname) > 0")->where($dist)->where($cond)->order_by('organizationname');   
    $result = $this->sims->get()->result();
    return $result;
  }

  function getprogress()
  {
    $total=array();
    $userid=$this->session->userdata('userid');
    $days=$this->input->post('days');
    if($days=='') $days='30';

    $sql1="select 
                * from
    (
    select 
    temp.*,newjoin.keypointtotal from
    (
    select count(progresskeypointid) as keypointviewed,chapterid,subjectid  from (select progresskeypointid,chapterid,subjectid from el_progress_keypoint where userid='$userid' and dataposteddatetime > CURRENT_TIMESTAMP - INTERVAL '$days year'   group by subjectid,chapterid,progresskeypointid order by dataposteddatetime desc) as tem group by subjectid,chapterid
    ) as temp
    join 
    (
    select count(keypointcontentid) as keypointtotal ,chapterid,subjectid from el_content_keypoint group by chapterid,subjectid
    ) as newjoin 
    on (temp.chapterid=newjoin.chapterid and temp.subjectid= newjoin.subjectid)
    ) as newtab
    join el_subject s on (s.subjectid=newtab.subjectid)
    join el_chapter c on (c.chapterid=newtab.chapterid)
    join el_class cl on (cl.classid=s.classid)";

    $sql2="select 
                * from
    (
    select 
    temp.*,newjoin.exercisetotal from
    (
    select count(progressexerciseid) as exerciseviewed,chapterid,subjectid  from (select progressexerciseid,chapterid,subjectid from el_progress_exercise where userid='$userid' and dataposteddatetime > CURRENT_TIMESTAMP - INTERVAL '$days days'   group by subjectid,chapterid,progressexerciseid order by dataposteddatetime desc) as tem group by subjectid,chapterid
    ) as temp
    join 
    (
    select count(exercisecontentid) as exercisetotal ,chapterid,subjectid from el_content_exercise group by chapterid,subjectid
    ) as newjoin 
    on (temp.chapterid=newjoin.chapterid and temp.subjectid= newjoin.subjectid)
    ) as newtab
    join el_subject s on (s.subjectid=newtab.subjectid)
    join el_chapter c on (c.chapterid=newtab.chapterid)
    join el_class cl on (cl.classid=s.classid)";

    $sql3="select 
                * from
    (
    select 
    temp.*,newjoin.videototal from
    (
    select sum(viewlength) as videoviewed,chapterid,subjectid  from (select progressvideoid,chapterid,viewlength,subjectid from el_progress_video where userid='$userid' and dataposteddatetime > CURRENT_TIMESTAMP - INTERVAL '$days days'   group by subjectid,chapterid,progressvideoid order by dataposteddatetime desc) as tem group by subjectid,chapterid
    ) as temp
    join 
    (
    select sum(videolength) as videototal ,chapterid,subjectid from el_content_video group by chapterid,subjectid
    ) as newjoin 
    on (temp.chapterid=newjoin.chapterid and temp.subjectid= newjoin.subjectid)
    ) as newtab
    join el_subject s on (s.subjectid=newtab.subjectid)
    join el_chapter c on (c.chapterid=newtab.chapterid)
    join el_class cl on (cl.classid=s.classid)";

    $sql4="select 
                * from
    (
    select 
    temp.*,newjoin.quiztotal from
    (
    select sum(markstot) as quizviewed,chapterid,subjectid  from (
    select cq.chapterid,cq.subjectid,
    case 
    when pq.attemptnumber=1 then cq.marks
    when pq.attemptnumber=2 then cq.marks/2
    else 0
      end as markstot
    from el_progress_quiz pq 
    join el_content_quiz cq on (pq.quizcontentid=cq.quizcontentid)
    where pq.userid='$userid' and pq.iscorrect='Y' and pq.dataposteddatetime > CURRENT_TIMESTAMP - INTERVAL '$days days' 
    ) as tem group by subjectid,chapterid
    ) as temp
    join 
    (
    select sum(marks) as quiztotal ,chapterid,subjectid from el_content_quiz group by chapterid,subjectid
    ) as newjoin 
    on (temp.chapterid=newjoin.chapterid and temp.subjectid= newjoin.subjectid)
    ) as newtab
    join el_subject s on (s.subjectid=newtab.subjectid)
    join el_chapter c on (c.chapterid=newtab.chapterid)
    join el_class cl on (cl.classid=s.classid)
    ;";
    $progressekeypoint= $this->db->query($sql1);
    $progressexercise= $this->db->query($sql2);
    $progressvideo= $this->db->query($sql3);
    $progressquiz= $this->db->query($sql4);

    $exercisearray =   $progressexercise->result('array');
    $keypointarray =   $progressekeypoint->result('array');
    $videoarray    =   $progressvideo->result('array');
    $quizarray     =   $progressquiz->result('array');

    foreach ($exercisearray as $key => $value) {
      $total[$value['alt_name'].',/'.$value['subjectname'].',/'.$value['chaptername'].',/'.$value['subjectid']]['exercise']=$value;
    }
    foreach ($keypointarray as $key => $value) {
      $total[$value['alt_name'].',/'.$value['subjectname'].',/'.$value['chaptername'].',/'.$value['subjectid']]['keypoint']=$value;
    }
    foreach ($videoarray as $key => $value) {
      $total[$value['alt_name'].',/'.$value['subjectname'].',/'.$value['chaptername'].',/'.$value['subjectid']]['video']=$value;
    }
    foreach ($quizarray as $key => $value) {
      $total[$value['alt_name'].',/'.$value['subjectname'].',/'.$value['chaptername'].',/'.$value['subjectid']]['quiz']=$value;
    }



    return $total;

  }

	// public function getclasswisesubject()
 //    {
 //    	          $this->db->SELECT('c.classid,c.classname,os.subjectname');
 //                $this->db->from('sch_class c');
 //                $this->db->join('sch_orgsubject os','c.classid=os.classid');

 //                $this->db->order_by('c.classid', 'ASC');
 //          //       $this->db->order_by('midas_products.subject', 'desc');
 //                $query= $this->db->get();

 //        		    return $query->result('array');


 //    }
  public function getproductbyremarks($slug=false)
  {
    try
    {
     if(!$slug) throw new exception('Error in data parameter'); 
     $remarks=explode('-',$slug);

     $data=implode(' ',$remarks);
     $this->db->limit(1);
     $this->db->select('*');
     $this->db->from('midas_products');
     $this->db->where('lower(remarks)',trim($data));
     $this->db->where('isfullpackage','Y');
     $query= $this->db->get();

     return ( $query->result('array'));
   }
   catch (Exception $e)
   {
    echo $e->getMessage();
  }


}
public function getproductbyclass($class)
{

  try
  {
   if(empty($class)) throw new exception('Error in data parameter'); 


   $this->db->select('*');
   $this->db->from('midas_products');
   $this->db->where_in('class',$class);
   $this->db->where('isfullpackage','Y');
   $this->db->order_by('class','asc');
   $query= $this->db->get();

   return ( $query->result('array'));
 }
 catch (Exception $e)
 {
  echo $e->getMessage();
}


}

public function getsubjects($eclassid,$source){

  $this->db->SELECT('c.classid,c.classname,c.CLASSTAG,os.subjectname,os.subjectid,ch.chaptercount,os.icon');
  $this->db->from('el_class c');
  $this->db->join('el_subject os','c.classid=os.classid');
  $this->db->join('(select count(*)as chaptercount,subjectid from el_chapter where isactive = \'Y\' group by subjectid) as ch','ch.subjectid = os.subjectid','left');
  $this->db->where(array('os.isactive'=>'Y'));

  if($this->session->userdata('userid')=='11000268203')
  {
    $this->db->where('c.classid not in (2,3,4,38,51,52,53,50,56,48,47)');
  }

  if($source=='LC')
  {
    $this->db->select("mm.sessionid,case when mm.sessionid is null then 'N' else 'Y' end as iscurrlive, os.livetimings,mm.breaktill,os.showsession");

    if($this->session->userdata('userid')=='11000265069')
    {
      $this->db->join('(SELECT distinct sessionid,sessionname,breaktill,subjectid FROM ml_live_sessions ls JOIN ml_livesession_schedule sc ON sc.livesessionid = ls.livesessionid WHERE EXTRACT(DOW FROM now()) = sc.weekday and CURRENT_TIMESTAMP(0)::time BETWEEN sc.starttime  and sc.endtime GROUP BY subjectid,ls.sessionid,ls.sessionname,ls.breaktill) as mm', 'mm.subjectid = os.subjectid','left');
    }
    else{
      $this->db->join('(SELECT distinct sessionid,sessionname,breaktill,subjectid FROM ml_live_sessions ls JOIN ml_livesession_users u ON u.livesessionid = ls.livesessionid JOIN ml_livesession_schedule sc ON sc.livesessionid = ls.livesessionid WHERE u.isactive is true AND u.userid = '.$this->session->userdata('userid').' and ls.isactive is true and EXTRACT(DOW FROM now()) = sc.weekday and CURRENT_TIMESTAMP(0)::time BETWEEN sc.starttime  and sc.endtime GROUP BY subjectid,ls.sessionid,ls.sessionname,ls.breaktill) as mm', 'mm.subjectid = os.subjectid','left');
    }
    if($this->session->userdata('usertype') == 'TEACHER')
      $this->db->where(array('c.islive'=>'Y'));
    $this->db->order_by("c.priority, case when mm.sessionid is null then 'N' else 'Y' end desc ,os.sortorder");    
  }
  else
  {
    $this->db->order_by('c.priority, os.sortorder', 'ASC');    
  }
  $this->db->where('c.classid!='.$eclassid );
    // $this->db->where("c.isactive='Y'");
  $query= $this->db->get();
    // echo $this->db->last_query();

  $class = $query->result();
  // if($this->session->userdata('userid')=='1000000397')
  // {
  //   exit;
  // }
  $response = array();
  $classid = '';
  $cnt = -1;
  foreach ($class as $row) {
    if($classid!=$row->classid)
    {
      $classid = $row->classid;
      $cnt++;
      $response[$cnt]['subject'] = array();
    }
    $response[$cnt]['id'] = $row->classid;
    $response[$cnt]['classname'] = $row->classname;
    $response[$cnt]['CLASSTAG'] = $row->CLASSTAG;   
    array_push($response[$cnt]['subject'],array('subjectid'=>$row->subjectid,'subjectname'=>$row->subjectname,'chaptercount'=>$row->chaptercount,'icon'=>($row->icon?'http://myschool.midas.com.np/uploads/subjects/'.$row->icon:''),'islive'=>@$row->iscurrlive,"livetimings" => @$row->livetimings,"sessionid" => @$row->sessionid,"breaktill" => @$row->breaktill,"showsession" => @$row->showsession));
  }
  return $response;
}

public function checkdata($mobileno)
{
 try
 {

   if(!($mobileno)) throw new exception('Error in data parameter'); 
   $this->otherdb->limit('1');
   $this->otherdb->select('*');
   $this->otherdb->from('productdownloadusers');
   $this->otherdb->where('mobilenumber',$mobileno);

   $querydata= $this->otherdb->get();
                // print_r($querydata->result('array'));exit;
   return $querydata->result('array');

 }
 catch (Exception $e)
 {
  echo $e->getMessage();
}
}
public function insertdetail($table,$data)
{
  try
  {

    if((trim($table)=='')) throw new Exception("No table found ", 1);
    if(empty($data)) throw new Exception("Empty array found", 1);

    $this->otherdb->insert($table,$data); 
    if($this->otherdb->affected_rows())
    {
      return true;
    }
    else return false;

  }
  catch(Exception $e )
  {
    echo $e->getMessage();
  }

}
public function updatedetail($table,$data,$colname,$key)
{
 try
 {

   if((trim($key)=='')) throw new Exception("No Specific key found ", 1);

   $this->otherdb->where($colname, $key);
   $this->otherdb->update($table, $data); 
   if($this->otherdb->affected_rows())
   {
    return true;
  }
  else return false;

}
catch(Exception $e )
{
  echo $e->getMessage();
}
}

public function checksms($mobileno)
{
 try
 {

   if((trim($mobileno)=='')) throw new Exception("Mobile Number npt found ", 1);

   $query=$this->smsdb->query("SELECT COUNT(*) as N FROM SMS_OUT 
    WHERE MOBILE_NUMBER = '$mobileno' AND TO_DATE(REC_DATE) = TO_DATE(SYSDATE)");

                    // $querydata= $this->smsdb->result('array');

   return $query->result('array');

 }
 catch(Exception $e )
 {
  echo $e->getMessage();
}
}

public function getid()
{

 $query=$this->smsdb->query("SELECT NVL(MAX(ID),0)+1 as SMSOUTID FROM SMS_OUT");
                 // $querydata= $this->smsdb->result('array');
 return $query->result('array');

}

public function sendsms($data)
{  

        // print_r($data);exit;
 return $this->smsdb->insert('SMS_OUT',$data); 

}
public function getorderid()
{
  $this->otherdb->select('max("MIDASECLASSORDERID")');
  $this->otherdb->from("MIDASECLASSORDER");
  $data= $this->otherdb->get();
  return $data->result('array');

}
public function checkvalidation($data)
{
 $this->otherdb->select('count("MIDASECLASSORDERID")');
 $this->otherdb->from("MIDASECLASSORDER");
 $this->otherdb->where($data);
 $data= $this->otherdb->get();
 return $data->result('array');
}

public function updateuser($userid)
{
  $this->otherdb->set('createddatetime',date('Y-m-d H:i:s'));
  $this->otherdb->where('productdownloaduserid',$userid);
  $this->otherdb->update('productdownloadusers');
  if($this->otherdb->affected_rows())
  {
    return true;
  }
  else
  {
    return false;
  }
}


public function checkdownloaddetail($data)
{
  try
  {
    if(empty($data)) throw new Exception("No data for checking", 1);
    $this->otherdb->select('productdownloadusersdetailid');
    $this->otherdb->from('productdownloadusersdetail');
    $this->otherdb->where($data);
    $data= $this->otherdb->get();
    $id= $data->result('array');
    if($this->otherdb->affected_rows())
    {
      return $id;
    }
    else
    {
      return false;
    }

  }
  catch(Exception $e )
  {
    echo $e->getMessage();
  }
}
public function insertdetails($table,$data)
{
  try
  {

    if((trim($table)=='')) throw new Exception("No table found ", 1);
    if(empty($data)) throw new Exception("Empty array found", 1);

    $this->otherdb->insert($table,$data); 
    if($this->otherdb->affected_rows())
    {
      return $this->otherdb->insert_id();
    }
    else return false;

  }
  catch(Exception $e )
  {
    echo $e->getMessage();
  }

}
}

