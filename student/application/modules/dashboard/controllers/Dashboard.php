<?php
// ini_set('max_input_vars', '2500');
// ini_set('memory_limit', '256M');
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Front_controller {

	function __construct()
	{
		parent::__construct();
   $d=$this->config->item('default');
   $df=$this->config->item($d); 
       // $df['js'][]='forum/nepali_unicode';
   $df['js'][]='common/student_api';
   $df['js'][]='forum/pramukhime';
   $df['js'][]='forum/pramukhindic';
   
   $df['js'][]='dashboard/dashboard';
   $df['js'][]='common/pusher.min';   
   $df['js'][]='ckeditor/ckeditor';

   $this->config->set_item($d,$df);
   $this->load->model('dashboard_model','model');
   
   $this->load->model('frontend_model','frontmodel');

 }

 public function index()
 {
  // if($this->session->userdata('isdemosession'))
  //   redirect('/demoschool');

  $userid=$this->session->userdata('userid');
  $title=$this->input->post('title');
  $student_name=$this->session->userdata('student_name');

  $this->load->vars(array(
    // 'secondaryviewlink' =>    'dashboard',
    'formview'          =>    'dashboard',
    'searchview'        =>    'templates/header/search',
    'student_name'      =>    $student_name
    ));

  $this->render_page();		
}	



function childchapters(){
  $post=$this->input->post();
  $this->load->view('childchapters',array('response'=>$post['data'],'source'=>$post['source']));
}

public function tutorials()
{

  $userid=$this->session->userdata('userid');
  $title=$this->input->post('title');
  $student_name=$this->session->userdata('student_name');

  $this->load->vars(array(
    // 'secondaryviewlink' =>    'dashboard',
    'formview'          =>    'dashboard',
    'source'            =>    'T',
    'searchview'        =>    'templates/header/search',
    'student_name'      =>    $student_name
    ));

  $this->render_page();   
}

public function quizes()
{

  $userid=$this->session->userdata('userid');
  $title=$this->input->post('title');
  $student_name=$this->session->userdata('student_name');

  $this->load->vars(array(
    // 'secondaryviewlink' =>    'dashboard',
    'formview'          =>    'dashboard',
    'source'            =>    'QE',
    'searchview'        =>    'templates/header/search',
    'student_name'      =>    $student_name    
    ));

  $this->render_page();   
}

public function homework_help(){
/*  $d=$this->config->item('default');
   $df=$this->config->item($d);
   $df['js'][]='common/student_api';
   $df['js'][]='dashboard/dashboard';
   $df['js'][]='ckeditor/ckeditor'; 
  $df['js'][]='forum/nepali_unicode';
  $this->config->set_item($d,$df);*/

  $userid=$this->session->userdata('userid');
  $title=$this->input->post('title');
  $student_name=$this->session->userdata('student_name');

  $this->load->vars(array(
    // 'secondaryviewlink' =>    'dashboard',
    'formview'          =>    'dashboard',
    'source'            =>    'HH',
    'searchview'        =>    'templates/header/search',
    'student_name'      =>    $student_name
    ));

  $this->render_page();
}


function listSubjects()
{
  $data = $this->input->post('data');
  $eclassdata=$this->input->post('eclassdata');
  $response = $data['response'];
  // if($data['subjectstatus'] > -1){
  //   // if subjectstatus = 7 then purchase now

  //   $allsubjects = $this->model->getsubjects($this->session->userdata('eclassid'),$data['source']);
  // }else{
    $allsubjects = '';
  // }
  
  $title=$this->input->post('title');
  $vars = array(
    'response'          =>$response,
    'allsubjects'       =>$allsubjects,
    'classname'         =>$data['classname'],
    'source'            =>$data['source'],    
    'title'             =>$title,
    'data'              =>$data,
    'eclassdata'        =>$eclassdata

    );

  if($data['source'] == 'LC'){
    $view = 'livesubjects';
  }else{
    $view = 'subjects';
  }
  if($this->input->post('breadcrumb')=="true")
  {
    $this->load->vars(array('vars'=>$vars,'view'=>$view,'hassubview'=>true));
    $this->load->view('dashboard', $response);
  }
  else
  {
    $this->load->vars($vars);
    $this->load->view($view, $response);
  }
}
function listUsersSubjects()
{
  $source = $this->input->post('source');
  $usersubjects = $this->input->post('usersubjects');
  $userclass = $this->input->post('userclass');
  // if($data['subjectstatus'] > -1){
  //   // if subjectstatus = 7 then purchase now

  //   $allsubjects = $this->model->getsubjects($this->session->userdata('eclassid'),$data['source']);
  // }else{
    $allsubjects = '';
  // }
  // var_dump($this->input->post());
  $title=$this->input->post('title');
  $vars = array(
    'source'            =>$source,
    'userclass'      =>$userclass, 
    'usersubjects'      =>$usersubjects
    );
 
  if($data['source'] == 'LC'){
    $view = 'livesubjects';
  }else{
    $view = 'userSubjects';
  }
  if($this->input->post('breadcrumb')=="true")
  {
    $this->load->vars(array('vars'=>$vars,'view'=>$view,'hassubview'=>true));
    $this->load->view('dashboard', $response);
  }
  else
  {
    $this->load->vars($vars);
    $this->load->view($view, $response);
  }
}

function selectedClassSubject()
{
  $response = $this->input->post('data');
  $source=$this->input->post('source');
  $subjectstatus=$this->input->post('subjectstatus');
  if($subjectstatus > -1){
    // if subjectstatus = 7 then purchase now

    $allsubjects = $this->model->getsubjects($this->session->userdata('eclassid'),$source);
  }else{
    $allsubjects = '';
  }
  if($source == 'LC'){
    $view='liveselectedclasssubject';
  }else{
    $view='selectedclasssubject';
  }

  
  
  $vars=array(
    'response'=>$response,
    'source'=>$source,
    'allsubjects'=>$response,
    );
  $this->load->vars($vars);
  $this->load->view($view, $response);
}

function listChapters($source=false)
{
  $data = $this->input->post();  
  $response = $data['response'];
  $title=$this->input->post('title');
  $vars = array('response'    =>  $response,
    'title'       =>  $title,
    'subjectname' =>  $data['subjectname'],
    'classname'   =>  $data['classname'],
    'classid'   =>  $data['classid'],
    'source' => $source);  
  echo '<div style="display: none;"><pre>';
    // print_r($this->input->post());
  // echo phpinfo();
  echo'</pre></div>';
  if($this->input->post('breadcrumb')=="true")
  {
    $this->load->vars(array('vars'=>$vars,'view'=>'chapters','hassubview'=>true));
    $this->load->view('dashboard', $response);
  }
  else
  {
    $this->load->vars($vars);
    $this->load->view('chapters', $response);
  }
}

function listHomeworkQuestions($source=false){
  $data = $this->input->post();  
  $html = $data['html'];

  $title=$this->input->post('title');
  $vars = array('response'    =>  $response,
    'title'       =>  $title,
    'source' => $source);
  
  $this->load->vars($vars);
  $this->load->view('homework_help_questions', array('html' => $html));
}

public function liveclass(){
 $userid=$this->session->userdata('userid');
 $title=$this->input->post('title');
 $student_name=$this->session->userdata('student_name');

 $this->load->vars(array(
    // 'secondaryviewlink' =>    'dashboard',
  'formview'          =>    'dashboard',
  'source'            =>    'LC',
  'searchview'        =>    'templates/header/search',
  'student_name'      =>    $student_name
  ));

 $this->render_page();
}

}

