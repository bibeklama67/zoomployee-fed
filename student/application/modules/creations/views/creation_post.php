
<?php
if(!$creations){
  ?>
  <div class="error-message">Creation List is not available.</div>
  <?php
}else{


  foreach ($creations as $creation) {
     $last_dataposteddatetime = $creation['dataposteddatetime'];
    $edit_enable = false;   
    $rating_enable = false; 
    if($this->session->userdata('usertype') == 'PARENT'){
      if($this->session->userdata('asparent') == 'true'){
        if($this->session->userdata('userid') == $creation['userid']){
          if($creation['approved'] == 'P'){
            $edit_enable = true;
          }
        }
      }else{
        if($this->session->userdata('childid') == $creation['userid']){
          if($creation['approved'] == 'P'){
            $edit_enable = true;
          }
        }
      }
    }
    else if($this->session->userdata('usertype') == 'STUDENT'){
     if($this->session->userdata('userid') == $creation['userid']){
      if($creation['approved'] == 'P'){
        $edit_enable = true;
      }
    }else{
      $edit_enable = false;
    }
  }

  if($this->session->userdata('usertype')== 'STUDENT' ){
    if($this->session->userdata('userid') != $creation['userid']){
      $rating_enable = true;
    }
  }else if($this->session->userdata('usertype')== 'PARENT'){
    if($this->session->userdata('asparent') == 'true'){
      if($this->session->userdata('userid') != $creation['userid']){
        $rating_enable = true;
      }
    }else{
      if($this->session->userdata('childid') != $creation['userid']){
        $rating_enable = true;
      }
    }
  }


  if($creation['status'] == 'Y'){
    ?>

    <div class="col-lg-12 col-md-12 posted-creations" id="posted_creations<?= $creation['mycreationsid']?>">
      <div class="col-md-12" style="padding: 0px;">
        <div class="col-md-6" style="padding: 0px;">
          <div class="col-md-12" style="padding: 0px; margin-top: 0px;">
            <div class="pull-left">
              <img src="<?= $creation['profileimage']?$creation['profileimage']:base_url('assets/images/dummy-person.jpg');?>" width="50px" height="50px" class="img-responsive"/>
              <!--  <img src="<?php echo $creation['profileimage'];?>" width="50px" height="50px" class="img-responsive"/>     -->
            </div>
            <div class="pull-left" style="margin-left: 10px;">
              <label for="username" style="margin-bottom: 0px;"><span style="color:#717B9B;"><?php echo $creation['studentname']; ?></span></label><br>
              <small style="color: #7D7D7D;"><?php echo $creation['schoolname'] ?></small>
              <br>
              <span class="pull-left"><?= $creation['creationcategory'] ?></span><i class="fa fa-circle pull-left" aria-hidden="true" style="font-size:3px;margin: 5px;margin-top: 9px;display: block;"></i>
              <span class="pull-left"> <i class="glyphicon glyphicon-star myy" style="color: #bfd047;top: 2px;"></i> <?php echo $creation['averagerating'];?></span><i class="fa fa-circle pull-left" aria-hidden="true" style="font-size:3px;margin: 5px;margin-top: 9px;display: block;"></i>

              <i class="fa fa-user" aria-hidden="true"></i>
              <span class=""><?php echo $creation['totalrating']; ?></span>

            </div>
          </div>
        </div>
        <div class="col-md-5" style="padding-right: 0px;text-align: right;">
          <span>

            <small style="color: #7D7D7D;"><?php echo $creation['posteddatetime'] ?></small>
          </span>

        </div>
        <div class="col-xs-1">
          <?php
          if($edit_enable == true){
            ?>
            <div class="dropdown pull-right">

              <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background: #ffffff;">
                <i class="fa fa-chevron-down" aria-hidden="true" style="color: #B3B7BE;font-size: 10px;margin-top: -5px;"></i>
              </button>
              <ul class="dropdown-menu">
                <li><a href="javascript:;" class="edit_creation" data-mycreationsid="<?= $creation['mycreationsid']?>" data-creationtext="<?= $creation['posttext']?>" data-image="<?= $creation['postimagefile']?>" data-category="<?= $creation['creationcategory']?>">Edit</a></li>
                <li><a href="javascript:;" class="delete_creation" data-mycreationsid="<?= $creation['mycreationsid']?>">Delete</a></li>
              </ul>
            </div>       
            <?php
          }
          ?>
        </div>
        <div class="col-md-12">
          <p><?php echo $creation['posttext']; ?></p>
          <?php
          if($creation['postimagefile']){
            ?>
            <img src="<?php echo $creation['postimagefile']; ?>">
            <?php
          }
          ?>

        </div>
        <div class="col-md-12 likes-n-comments" data-approval="<?=$creation['approved']?>">
          <div class="col-md-6">
            <div class="pull-left" style="padding-top: 5px;">
              <?php if($creation['approved']=='P' && $this->session->userdata('userid') == $creation['userid']){?>
              <div style="width:15px; height:15px; background:#ce5d35; border-radius:50%;"></div>
              <?php }elseif($creation['approved']=='Y'  && $this->session->userdata('userid') == $creation['userid']){ ?>
              <div style="width:15px; height:15px; background:#669900; border-radius:50%;"></div>
              <?php }elseif($creation['approved']=='C'  && $this->session->userdata('userid') == $creation['userid']){?>
              <div style="width:15px; height:15px; background:red; border-radius:50%;"></div>
              <?php }?>
            </div>
          </div>
          <div class="col-md-6">
            <?php
              if($rating_enable == true){
                $rating = $creation['rating'];
                ?>
                <div class="acidjs-rating-stars my" style="margin-left: 30px;"> 
                  <form style="">
                    <input type="hidden" id="ratycreationId" value="<?= $creation['mycreationsid']?>">
                    <input type="radio" class="rating-radio" name="group-<?= $creation['mycreationsid']?>" data-refid="<?= $creation['mycreationsid']?>" id="group-<?= $creation['mycreationsid']?>-0" value="5" <?php echo $rating==5?checked:''; ?>><label for="group-<?= $creation['mycreationsid']?>-0"></label>
                    <input type="radio" class="rating-radio" name="group-<?= $creation['mycreationsid']?>" data-refid="<?= $creation['mycreationsid']?>" id="group-<?= $creation['mycreationsid']?>-1" value="4" <?php echo $rating==4?checked:''; ?>><label for="group-<?= $creation['mycreationsid']?>-1"></label>
                    <input type="radio" class="rating-radio" name="group-<?= $creation['mycreationsid']?>" data-refid="<?= $creation['mycreationsid']?>" id="group-<?= $creation['mycreationsid']?>-2" value="3" <?php echo $rating==3?checked:''; ?>><label for="group-<?= $creation['mycreationsid']?>-2"></label>
                    <input type="radio" class="rating-radio" name="group-<?= $creation['mycreationsid']?>" data-refid="<?= $creation['mycreationsid']?>" id="group-<?= $creation['mycreationsid']?>-3" value="2" <?php echo $rating==2?checked:''; ?>><label for="group-<?= $creation['mycreationsid']?>-3"></label>
                    <input type="radio" class="rating-radio" name="group-<?= $creation['mycreationsid']?>" data-refid="<?= $creation['mycreationsid']?>" id="group-<?= $creation['mycreationsid']?>-4" value="1" <?php echo $rating==1?checked:''; ?>><label for="group-<?= $creation['mycreationsid']?>-4"></label>
                  </form>
                </div> 
                <?php
              }
            
            ?>
            <div class="pull-right likes-comments" style="padding-top: 5px;">    
              <a href="javascript:;" class="like-creation<?= $creation['mycreationsid']?> make-like" id="like-creation" data-mycreationsid="<?= $creation['mycreationsid']?>" data-likecount="<?= $creation['likes']?>">
                <?php
                if($creation['postliked'] == 'Y'){
                  ?>
                  <i class="fa fa-thumbs-up likeimage<?= $creation['mycreationsid']?> liked" aria-hidden="true" style="font-size: 15px; color:#4268b3;"></i>
                  <?php
                }else{
                  ?>
                  <i class="fa fa-thumbs-up likeimage<?= $creation['mycreationsid']?>" aria-hidden="true" style="font-size: 15px; color:#adb4bc;"></i>
                  <?php
                }
                ?>
              </a>

              <a href="javascript:;" class="likedby" data-mycreationsid="<?= $creation['mycreationsid']?>" data-likecount ="<?= $creation['likes']?>">
                <label for="likes" class="likecount<?= $creation['mycreationsid']?>"><?php echo ($creation['likes'])?$creation['likes']:0; ?> </label>
              </a>
              &nbsp; &nbsp;
              <a href="javascript:;" role="button" data-toggle="collapse" data-mycreationsid="<?php echo $creation['mycreationsid'];?>" data-target="#comments<?= $creation['mycreationsid']?>" aria-expanded="false" aria-controls="collapse<?= $creation['mycreationsid']?>" class="get_comments"><span class="glyphicon glyphicon-comment" style="font-size: 15px; color:#adb4bc;"></a> <label for="comments" class="commentcount<?= $creation['mycreationsid']?>" data-count="<?php echo $creation['commentcount']; ?>">&nbsp; <?php echo ($creation['commentcount'])?$creation['commentcount']:0; ?> </label> &nbsp;
            </div>  
          </div>
        </div>
        <div class="col-md-12 col-xs-12 pad-fix comments-display collapse<?= $creation['mycreationsid']?>" id="comments<?= $creation['mycreationsid']?>"  style="margin-top: 5px; min-height: auto !important; "></div>
        <div class="col-md-12 comment-reply-area">
         <div class="col-md-12"style="padding: 0px; margin: 10px 0px;">
          <div class="col-md-1">
            <img src="<?= $this->session->userdata('image')?$this->session->userdata('image'):base_url('assets/images/dummy-person.jpg');?>" width="40px" height="40px" class="img-responsive"/> 
          </div>
          <div class=" col-md-11 no-padding" style="border-bottom: 1px solid #ccc;">
            <textarea class="txtReply<?= $creation['mycreationsid']?>" placeholder="Write a comment.." rows="2" data-toggle="modal" style="height: 65px;" data-target="#myModal" id="english_comment"></textarea>
           
          </div>
          <div class=" col-md-12 no-padding" style="margin-top: 5px;margin-bottom: -10px;">
            <button class="btn-reply btn btn-info post-comment" id="post-comment" data-mycreationsid="<?= $creation['mycreationsid']?>">Post</button>
           
          </div>
        </div>
      </div>
    </div>
  </div>

    <?php
  }
}
}
?>
<script type="text/javascript">
var lastdate = "<?= $last_dataposteddatetime?>";
  console.log(lastdate);
  console.log('Herere...')
$(window).data('ajaxready', true).scroll(300,function(e) {
  if ($(window).data('ajaxready') == false) return;

  //if ($(window).scrollTop() >= ($(document).height() - $(window).height())) {
if (($(window).height() + $(window).scrollTop() +1000) >= ($(document).height()-1000)) {
    $('.cust-loader').removeClass('hidden');
    $(window).data('ajaxready', false);
    if($('.show_creation_category').val()){
    var creationcategory = $('.show_creation_category').val();
    }else{
    var creationcategory = 'ALL';
    }
    //var creationcategory = $('.show_creation_category').val();
    var displayto = $('#creation_filter').val();
    var select = true;
    
    // return false;
    var asparent = localStorage.getItem('asparent');
    var infoData ={Displayto:displayto, creationcategory: creationcategory, asparent: asparent, lastdate:lastdate};

    $.when(getposts(infoData)).then(function(data){
      $('.cust-loader').addClass('hidden');
      

      if(data.type == 'success'){
      
      var postdata = {data: data, asparent: asparent};
      $.ajax({
        type:"POST",
        url: base_url+"creations/last_creation_post",
        data: postdata, 
        success:function(result) {
          $(window).data('ajaxready', true);
           
          $('.wrap-post').append(result);

        }
      });
    }
    });


  }
});
</script>
