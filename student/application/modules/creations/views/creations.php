<style>
	.image_preview {
		width: 70px;
		margin-left: 10px;
		border-radius: 3px;
		display: none;
	}
	.target {
		border: solid 1px #aaa;
		min-height: 200px;
		width: 30%;
		margin-top: 1em;
		border-radius: 5px;
		cursor: pointer;
		transition: 300ms all;
		position: relative;
	}

	.contain {
		background-size: cover;
		position: relative;
		z-index: 10;
		top: 0px;
		left: 0px;
	}
	textarea {
		background-color: white;
	}
	.active_copypaste {
		box-shadow: 0px 0px 10px 10px rgba(0,0,255,.4);
	}

	.btn.fa.fa-pencil-square-o{

		font-size: 33px;
		margin-top: -7px;
		padding: 0;
	}

	#cke_statusck{
		width:100% !important;
	}
	.postcreation-header{
		padding: 0 !important;
		margin: 0 !important;
		/*    background: #FFF;*/
		height: 34px;
		border-top-right-radius: 4px;
		border-top-left-radius: 4px;
	}

	.postcreation-title{
		width: 100%;
		padding: 0px;
		display: block;
		float: left;
		color: #365899 !important;
		font-weight:600;
	}

/*
.postcreation-lang{
    float: right;
    width:100px;
    border-radius: 0;
    border: none;
}
*/

.postcreation-textarea{
	border:none;
	border-bottom-left-radius: 4px;
	border-bottom-right-radius: 4px;
	margin-bottom: 10px;
	padding: 5px 10px;
}

pre{
	z-index: -10;
	display: none !important;
	visibility: hidden !important;
}
.inputapi-transliterate-indic-suggestion-menu-vertical{
	display: none !important;
}
</style>
<script src="http://www.google.com/jsapi"></script>
<!-- <link type="text/css" rel="stylesheet" href="http://www.unicodenepali.com/v3/styles.css" />
	<script type="text/javascript" src="http://www.unicodenepali.com/v3/j.js"></script> -->
	<?php 
/*if(isset($creations)){
	echo '<pre>'; var_dump($creations);echo '</pre>';
}*/

if($this->session->userdata('usertype')== 'STUDENT'){
	$post_enable = 'true';
}
if($this->session->userdata('usertype')== 'PARENT'){
	if($this->session->userdata('asparent') == 'false' && $this->session->userdata('creationpermission') != 6){
		$post_enable = 'true';
	}
}

?>


<div class="wrapper my-creations" >
	<div class="col-lg-12 col-md-12 col-sm-12" id="my-creations">
		<!--<div class="col-lg-3" id="creation-left-bar" style="padding-left:0;">
			<div class="col-lg-12 creation-left-bar"></div>
		</div>-->
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 creation-right-side" id="creation-right-side" style="padding: 0px;">
			<div class="myschool_wrapper">  
				<div class="col-lg-12  rt-block pad-fix">
					<h5 class="title_er">Creations<span class="pull-right"></span>&nbsp;&nbsp;
						<?php

						if($this->session->userdata('usertype')== 'PARENT'){
							?>
							<!-- <a href="javascript:;" class="user_creation_popup" id="user_creation_popup"><i class="fa fa-users" aria-hidden="true"></i></a> -->
							<?php
						}
						?>
						
					</h5>

				</div>
				<div class="col-lg-12 no-padding white-bg">
					<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
						<!-- <?php
						if($post_enable == 'true'){
							
							?> -->
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 default-bg ">
								<div class="col-xs-12 pf-n-iptxt">
									<div class="col-xs-2 rounded-pf-pic">
										<img src="<?= $this->session->userdata('image')?$this->session->userdata('image'):base_url('assets/images/dummy-person.jpg');?>" class="img-responsive"/>
									</div>
									<div class="col-xs-10 input-creation">
										<div class=" col-xs-12 postcreation-header">
											<div class="col-xs-9 pad-fix">
												<h5 class="postcreation-title pad-fix">Post your creation</h5> </div>
												<!--										<select class="form-control postcreation-lang"><option value="English">English</option><option value="Nepali">Nepali</option></select>-->
												<div class="col-xs-3 pad-fix">
													<div class="pull-right pad-fix" style="margin-top: 10px;margin-right: 5px;">
														<button type="button" id="np-button" data-value="Nepali" class="langbtn postcreation-lang post-creation-nepali"></button>
														<button type="button" id="en-button" data-value="English" class="langbtn active postcreation-lang"></button>
														<input type="hidden" name="language_selected" id="language_selected" value="English">
													</div>
												</div>
											</div>
											<div class="col-xs-12 pad-fix">
												<textarea id="txtStatus" placeholder="" rows="4" data-toggle="modal" data-target="#myModal" class="postcreation-textarea"></textarea>
												
												<div class="error_btn_disable" style="margin-top: -9px; color: #F00;"></div>
											</div>
										</div>
									</div>	
								</div>
								<div class="col-lg-12 col-md-12 post-btns default-bg clear-both">
									<div class="col-xs-12">

										<div class="col-xs-1"></div>
										<div class=" col-xs-3 pull-left">
											<select class="form-control category_select" style="height: 31px; margin-top: 4px;">
												<option value="">Select Category</option>
												<option value="DRAWING">Drawing</option>
												<option value="PLACE">Place Review</option>
												<option value="MOVIE">Movie Review</option>
												<option value="BOOK">Book Review</option>
												<option value="STORY">Story</option>
												<option value="POEM">Poem</option>
												<option value="PROJECT">Project</option>
											</select></div>
											<div class="pull-right post-opt" style="margin-top: 5px;">
												<div class="pull-right">
									<!-- <div class="dropdown" style="display: inline-block;">
										<button class="btn btn-primary dropdown-toggle privacy-btn" type="button" data-toggle="dropdown">Story
											<span class="caret"></span></button>
											<ul class="dropdown-menu story-cust-ddm">

											</ul>
										</div> -->
										<!-- <div class="dropdown" style="display: inline-block;">
											<button class="btn btn-primary dropdown-toggle privacy-btn" type="button" data-toggle="dropdown" id="display_show"><img src="http://midas.com.np/elearning/assets/images/all.jpg" width="20px" height="20px">&nbsp;&nbsp;All
												<span class="caret"></span></button>
												<ul class="dropdown-menu cust-ddm" id="ul_display_dropdown">
													<li class="display_dropdown active" data-text="ALL"><a href="javascript:;"><img src="http://midas.com.np/elearning/assets/images/all.jpg" width="20px" height="20px">&nbsp;&nbsp;All</a></li>
													<li class="display_dropdown" data-text="MYSCHOOL"><a href="javascript:;"><img src="http://midas.com.np/elearning/assets/images/myschool.jpg" width="20px" height="20px">&nbsp;&nbsp;My School</a></li>
													<li class="display_dropdown" data-text="MYCLASS"><a href="javascript:;"><img src="http://midas.com.np/elearning/assets/images/myclass.jpg" width="20px" height="20px">&nbsp;&nbsp;My Class</a></li>
													<li class="display_dropdown" data-text="ONLYME"><a href="javascript:;"><img src="http://midas.com.np/elearning/assets/images/onlyme.jpg" width="20px" height="20px">&nbsp;&nbsp;Only Me</a></li>
												</ul>
											</div> -->
											<input type="hidden" class="displayto" id="displayto" value="ALL">
											<input id="post-stat" name="submit" value="Post" class="btn btn-success btn-send post-creation" type="submit">
										</div>	 
									</div>
									<div class="pull-right">
								<!-- <button type="button" class="pull-right"><i class="fa fa-camera" aria-hidden="true" style="color:#808080; font-size: 20px;">&nbsp;&nbsp;</i>
							</button> -->

								<!-- <label for="file-upload" data-toggle="tooltip" title="Upload Image" class="custom-file-upload btn">

								</label>
								<input id="file-upload" name="file_upload" type="file" class="btn qans-file"> -->

								<span class="pull-left ">
									<span id="refresh-button"></span>
								</span> 
								<span class="pull-left cust-fa-icon" style="margin-top: 5px;">
									<a href="javascript:;" data-toggle="tooltip" title="Text Editor" class="btn fa fa-pencil-square-o" aria-hidden="true" id="ckeditor">
									</a>
								</span> 
								<span class="pull-left Wrapicon" style="margin-top: 5px;">
									<label for="file-upload" data-toggle="tooltip" title="Upload Image" class="custom-file-upload btn" style="margin-right: 0px !important;">

									</label>
									<input id="file-upload" name="file_upload" type="file" class="creation-file">
								</span>
								<span class="pull-left cust-fa-icon" style="margin-top: 5px;">
									<a href="javascript:;" data-toggle="tooltip" title="Paste Image" class="btn fa fa-clipboard" aria-hidden="true" id="copypastebutton">
									</a>
								</span>
								<span class="pull-left cust-fa-icon aa" style="margin-top: 5px;">
									<a href="javascript:;" data-toggle="tooltip" title="Camera" class="btn fa fa-camera" aria-hidden="true" id="webcambutton">
									</a>

								</span>
								
							</div>

						</div></div>
						<!-- <?php
					}
					
					?> -->
					<?php

					$filtertext_arrray = array(
						'ALL' => '<img src="assets/images/f-all.png"style="transform: scale(1.1);">&nbsp;&nbsp;<spanclass="filtertext">All</span >', 
						'ONLYME'=>'<img src="assets/images/f-onlyme.png" style="transform: scale(0.8);">&nbsp;&nbsp;<span class="filtertext">Only Me</span>',
						'MYCLASS' => '<img src="assets/images/f-myclass.png">&nbsp;&nbsp;<span class="filtertext">My Class</span>', 
						'MYSCHOOL'=>'<img src="assets/images/f-myschool.png"style="transform: scale(1.1);">&nbsp;&nbsp;<span class="filtertext">My School</span>',
						);
					
						?>

						<div class="dropdown pull-right">
							
							<button class="btn btn-secondary dropdown-toggle filter-dropdown" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="background: #ffffff;    color: #999998; padding-right: 0;"> <img src="<?php echo $assets_url; ?>assets/images/f-all.png"style="transform: scale(1.1);">&nbsp;&nbsp;<span class="filtertext">All</span>
								<i class="caret" aria-hidden="true" style="color: #B3B7BE;font-size: 10px;"></i>
							</button>
							<ul class="dropdown-menu" style="background: #FEFEFE;">
								<li>
									<a href="#" class="hh-post-filter" data-filterkey="ONLYME"><img src="<?php echo $assets_url; ?>assets/images/f-onlyme.png" style="transform: scale(0.8);">&nbsp;&nbsp;<span class="filtertext">Only Me</span></a>
								</li>
								<li>
									<a href="#" class="hh-post-filter" data-filterkey="MYCLASS"><img src="<?php echo $assets_url; ?>assets/images/f-myclass.png">&nbsp;&nbsp;<span class="filtertext">My Class</span></a>
								</li>
								<li>
									<a href="#" class="hh-post-filter" data-filterkey="MYSCHOOL"><img src="<?php echo $assets_url; ?>assets/images/f-myschool.png"style="transform: scale(1.1);">&nbsp;&nbsp;<span class="filtertext">My School</span></a>
								</li>
								<li>
									<a href="#" class="hh-post-filter" data-filterkey="ALL"><img src="<?php echo $assets_url; ?>assets/images/f-all.png"style="transform: scale(1.1);">&nbsp;&nbsp;<span class="filtertext">All</span></a>
								</li>
							</ul>
							<input type="hidden" class="creation_filter" id="creation_filter" value="ALL">
						</div>
						<div class=" col-xs-3 pull-right">
							<select class="form-control show_creation_category" style="height: 31px; margin-top: 4px;">
								<option value="">Select Category</option>
								<option value="DRAWING">Drawing</option>
								<option value="PLACE">Place Review</option>
								<option value="MOVIE">Movie Review</option>
								<option value="BOOK">Book Review</option>
								<option value="STORY">Story</option>
								<option value="POEM">Poem</option>
								<option value="PROJECT">Project</option>
							</select></div>
							<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 wrap-post" style="padding: 0px; margin-left: 12px;">

								<?php
								$this->load->view("creation_post");
								?>
								
							</div>
							
							<div class="cust-loader hidden">
								<img src="<?php echo $assets_url;?>assets/images/loader.gif" class="img-responsive" style="margin:auto;">
							</div>
						</div> 

						<?php
						$this->load->view('templates/creation_sidebar', $childposition);
						?>
					</div>


				</div>

			</div>
		</div>  

		<div id="dialog" title="Confirmation Required" style="display: none">
			Are you sure, you want delete this post?
		</div>

		<div class="modal fade" id="ckeditor-modal" role="dialog" data-backdrop="static" data-keyboard="false">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close ckeditor-close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Status</h4>
					</div>

					<div class="modal-body">
						<textarea id="statusck" class="statusck"></textarea>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default ckeditor-close" data-dismiss="modal">Close</button>

						<button style="margin-top: -3px; background: #4266b2;" type="button" id="ckeditor_done" data-dismiss="modal" class="btn btn-success btn-send">Post</button>
					</div>
				</div>
			</div>
		</div>


		<div class="modal fade in" id="image-popup" role="dialog" data-backdrop="static" data-keyboard="false">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close close-image" data-dismiss="modal">×</button>
						<h4 class="modal-title">Image Attachment</h4>
					</div>
					<div class="modal-body">
						<img id="creation_popup_image" class="image_preview img-responsive" style="display: block; width: 100%;" src="">
					</div>
					<div class="modal-footer">

						<button type="button" class="btn btn-default close-image" data-dismiss="modal">Close</button>
						<!-- <a href="javascript:void(0);" class="btn btn-success post-answer" data-dismiss="modal" data-questionid="">POST</a> -->
						<input name="submit" value="Post" style="background: #4266b2;" class="btn btn-success btn-send post-creation" data-dismiss="modal" type="submit">
					</div>
				</div>
			</div>
		</div>	

		<div class="modal fade" id="copypaste-modal" role="dialog" data-backdrop="static" data-keyboard="false">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" id="close_paste_image" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Paste Image</h4>
					</div>

					<div class="modal-body">
						<span id="error_message"></span>
						<div class="copy_paste_image">
							<div class="row">
								<div class="span4 target" id="copyimagepaste" style="float: left; margin-left: 20px; width: 93%;"></div>
								<input type="hidden" name="copyimage" id="copyimage">
							</div>
							<!-- <a href="" id="test" target="_blank" style="float: left;">See image in new window</a> -->
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" id="close_paste_image" data-dismiss="modal">Close</button>
						<button style="margin-top: -3px; background: #4266b2;" type="button" id="save-copypaste" data-dismiss="modal" class="btn btn-success btn-send">Post</button>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="webcam" data-backdrop="static" data-keyboard="false">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" id="webcam-close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Camera</h4>
					</div>
					<div class="modal-body " style="height: 515px;">

					</div>
					<div class="modal-footer" style="padding: 10px !important;">

        <button style="margin-top: -3px; background: #4266b2;" type="button" id="save-webcam" class="btn btn-success btn-send">Post</button><!-- 
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
    </div>
</div>
</div>
</div>

<div class="modal fade" id="edit_creation_modal" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close edit_creation_close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Edit Creation</h4>
			</div>

			<div class="modal-body">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default edit_creation_close" data-dismiss="modal">Close</button>

				<a href="javascript:;" style="margin-top: -3px; background: #4266b2;" id="update_creation" class="btn btn-success btn-send">Post</a>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="edit_comment_modal" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close edit_comment_close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Edit Comment</h4>
			</div>

			<div class="modal-body">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default edit_comment_close" data-dismiss="modal">Close</button>

				<a href="javascript:;" style="margin-top: -3px; background: #4266b2;" id="update_comment" class="btn btn-success btn-send">Post</a>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="likedby_modal" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Liked By</h4>
			</div>

			<div class="modal-body">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="user_creation" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Use creations & reviews as</h4>
			</div>
			<div class="modal-body">
				<ul>
					<li><a href="javascript:;" class="user_creation user_creation_parent" data-asparent="true" data-userid="<?= $this->session->userdata('userid')?>" style="display: block; width: 100%"><?php echo $this->session->userdata('student_name');?> </a></li>

					<li><a href="javascript:;" class="user_creation user_creation_child" data-asparent="false" data-userid="<?= $this->session->userdata('childid')?>" style="display: block; width: 100%"></a></li>


				</ul>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){

		pramukhIME.addKeyboard("PramukhIndic");
		pramukhIME.enable(); 	

	});

</script>
<script>
//for the copy paste of the image

(function($) {
	var defaults;
	$.event.fix = (function(originalFix) {
		return function(event) {
			event = originalFix.apply(this, arguments);
			if (event.type.indexOf('copy') === 0 || event.type.indexOf('paste') === 0) {
				event.clipboardData = event.originalEvent.clipboardData;
			}
			return event;
		};
	})($.event.fix);
	defaults = {
		callback: $.noop,
		matchType: /image.*/
	};
	return $.fn.pasteImageReader = function(options) {
		if (typeof options === "function") {
			options = {
				callback: options
			};
		}
		options = $.extend({}, defaults, options);
		return this.each(function() {
			var $this, element;
			element = this;
			$this = $(this);
			return $this.bind('paste', function(event) {
				var clipboardData, found;
				found = false;
				clipboardData = event.clipboardData;
				return Array.prototype.forEach.call(clipboardData.types, function(type, i) {
					var file, reader;
					if (found) {
						return;
					}
					if (type.match(options.matchType) || clipboardData.items[i].type.match(options.matchType)) {
						file = clipboardData.items[i].getAsFile();
						reader = new FileReader();
						reader.onload = function(evt) {
							return options.callback.call(element, {
								dataURL: evt.target.result,
								event: evt,
								file: file,
								name: file.name
							});
						};
						reader.readAsDataURL(file);
						return found = true;
					}
				});
			});
		});
	};
})(jQuery);



$("html").pasteImageReader(function(results) {
	var dataURL, filename;
	filename = results.filename, dataURL = results.dataURL;
	$data.text(dataURL);
	$size.val(results.file.size);
	$type.val(results.file.type);
	$test.attr('href', dataURL);
	var img = document.createElement('img');
	img.src= dataURL;
	var w = img.width;
	var h = img.height;
	$width.val(w)
	$height.val(h);
	var src = dataURL.replace('data:image/png;base64,','')
	$("#copyimage").val(src);
	return $('.active_copypaste').html('<img src="'+dataURL+'" style="width:100%">');
});

var $data, $size, $type, $test, $width, $height;
$(function() {
	$data = $('.data');
	$size = $('.size');
	$type = $('.type');
	$test = $('#test');
	$width = $('#width');
	$height = $('#height');
	$('.target').on('click', function() {
		var $this = $(this);
		var bi = $this.css('background-image');
		if (bi!='none') {
			$data.text(bi.substr(4,bi.length-6));
		}


		$('.active_copypaste').removeClass('active_copypaste');
		$this.addClass('active_copypaste');

		$this.toggleClass('contain');

		$width.val($this.data('width'));
		$height.val($this.data('height'));
		// if ($this.hasClass('contain')) {
		// 	$this.css({'width':$this.data('width'), 'height':$this.data('height'), 'z-index':'10'})
		// } else {
		// 	$this.css({'width':'', 'height':'', 'z-index':''})
		// }

	})
})
$('.langbtn').click(function(e) {
	$('.langbtn').not(this).removeClass('active');    
	$(this).toggleClass('active');
	e.preventDefault();
});


</script>

<!-- <script type="text/javascript" async>
	$(document).off('change','.postcreation-lang');
	$(document).on('change','.postcreation-lang', function(){
		var option = $(this).val();
		if(option == 'Nepali'){
        $('#description').nepalize();
  	//return false;
		}
		else
			return false;
	});
</script> -->