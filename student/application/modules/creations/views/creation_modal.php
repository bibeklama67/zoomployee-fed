<style type="text/css">
	#cke_edittxtStatus {
		width: 100% !important;
	}
	#editnepalitxtStatus{
		width: 568px !important;
	}
</style>
<form name="update_creation_form" id="update_creation_form" enctype="multipart/form-data">
	<table class="">
		<tr>
			<td>
			<input type="hidden" name="displayto" id="displayto" value="<?php echo $displayto; ?>">
				<input type="hidden" name="mycreationsid" id="mycreationsid" value="<?php echo $mycreationsid; ?>">
				<?php
				if($posttext){
					?>
					
					<input type="hidden" name="edit_language_selected" id="edit_language_selected" value="English">
					<label>Post:</label>
					<textarea id="edittxtStatus" placeholder="" rows="4" class="edittxtStatus"><?php echo $posttext; ?></textarea>
					<?php
				}
				if($postimagefile){
					?>
					<div class="remove_option">
						
						<img src="<?php echo $postimagefile;?>" alt="postimagefile" class="img-responsive">
						<span class="btn btn-info remove_btn" id="btn_remove">Remove</span>
					</div>
					<input type="hidden" name="pre_postimagefile" id="pre_postimagefile" value="<?php echo $postimagefile; ?>">
					<img id=output class="img-responsive">
					<div id="image_input">
						<label>Upload Files</label>
						<input type="file" name="postimagefile" id="edit_postimagefile">
						
					</div>
					
					<?php
				}
				// else
				// {
				?>
					<!-- <label>Upload Files</label>
					<input type="file" name="postimagefile"  id="edit_postimagefile" data-validation="required">
					<span id="edit_error"></span> -->
					<?php
				//}
					?>
					<!-- <input type="text" class="form-control" name="questiontext" value="<?php echo $questiontext; ?>"> -->
					<span id="edit_error"></span>

				</td>
			</tr>
			<tr>
				<td>
					<label>Category: </label>
					<select class="form-control edit_category_select" style="height: 31px; margin-top: 4px;">
						<option value="DRAWING" <?=$creationcategory=='DRAWING'?'selected="selected"':'';?>>Drawing</option>
						<option value="PLACE" <?=$creationcategory=='PLACE'?'selected="selected"':'';?>>Place Review</option>
						<option value="MOVIE" <?=$creationcategory=='MOVIE'?'selected="selected"':'';?>>Movie Review</option>
						<option value="BOOK" <?=$creationcategory=='BOOK'?'selected="selected"':'';?>>Book Review</option>
						<option value="STORY" <?=$creationcategory=='STORY'?'selected="selected"':'';?>>Story</option>
						<option value="POEM" <?=$creationcategory=='POEM'?'selected="selected"':'';?>>Poem</option>
						<option value="PROJECT" <?=$creationcategory=='PROJECT'?'selected="selected"':'';?>>Project</option>
					</select>

				</td>

			</tr>
		</table>
	</form>

	<script>

	// $('.editnepalitxtStatus').nepalize();

	var posttext ="<?= $posttext?>";
	if(posttext){
		CKEDITOR.replace('edittxtStatus');

	}

	$(document).off('click','.edit_creation_close');
	$(document).on('click','.edit_creation_close', function(){
		if(posttext){
			for ( instance in CKEDITOR.instances ){
				CKEDITOR.instances[instance].updateElement();
				CKEDITOR.instances[instance].setData('');
			}
			CKEDITOR.instances['edittxtStatus'].destroy();
		}
	});

	$(document).off('change','#edit_postimagefile');
	$(document).on('change','#edit_postimagefile', function(){
		var previewTarget=$('#output');
		var input = this;
		if (input.files && input.files[0]) {
			var reader = new FileReader();            
			reader.onload = function (e) {
				previewTarget.attr('src', e.target.result);
				previewTarget.show();			
			}
			reader.readAsDataURL(input.files[0]);
		}

		checkfile(this);
	});

	function checkfile(e)
	{

		var a=0;

		$("#error").text("");
		var ext3 =  $('#edit_postimagefile').val().split('.').pop().toLowerCase();
		if(ext3 == "")
		{
			a = 1;
		}
		else
		{
			if($.inArray(ext3, ['jpeg','jpg','gif','png']) == -1)
			{
				a = 0
			}
			else
			{
				a = 1;
			}
		}
		if(a != "1")
		{
			$("#edit_error").text("Invalid image");
			$("#edit_error").css("color", "red");
			return false;
		}
		else{
			$('#edit_error').text('');
			e.submit;
		}
	}

    //$('#contract_files').hide();
    $('#btn_remove').click(function () {

    	$('.remove_option').hide();
    	$('#edit_postimagefile').show();
    	$('#pre_postimagefile').val("");

    });
</script>