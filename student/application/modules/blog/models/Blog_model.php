<?php
class Blog_model extends CI_Model{
  function __construct()
  {
    parent::__construct(); 
    $this->db = $this->load->database('sims', TRUE);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
  }

  function district(){
    $result =$this->db->select('districtid,districtname')
    ->from('district')
    ->order_by('districtname')
    ->get()
    ->result();
    return $result;
  }

  function save(){
    try{

      $post=$this->input->post();
      $data = array(
       'name'         => $this->input->post('name'),
       'mobilenumber' => $this->input->post('mobilenumber'),
       'subject'      => $this->input->post('subject'),
       'districtid'  => $this->input->post('district_id'),
       'address'      => $this->input->post('address'),
       );

      $result =$this->db->insert('midasblogusers', $data); 
      if($result){
        $response = $result;
      } else {
        $response= array();
      }
      return $response;


    }catch(Exception $e){
      throw $e;
    }
  }

  function dataevent(){
    $IParray=array_values(array_filter(explode(',',$_SERVER['HTTP_X_FORWARDED_FOR'])));
    $ipaddress = end($IParray);
    $post=$this->input->post();
    $event=$this->input->post('event');
    $machinetype=$this->input->post('machinetype');
    $devicetype=$this->input->post('devicetype');
    $updatearr = array();

    $exrec = $this->db->select("*,posteddatetime+INTERVAL '5 minutes' as updateduration")->get_where('blogmobileusers',array('ipaddress'=>$ipaddress,'posteddate'=>date('Y-m-d'),'source'=>$this->input->post('source'),'machinetype'=>$machinetype))->row();
    switch ($event) {
      case 'close':
      $updatearr = array('close'=>date('Y-m-d H:i:s.u'));
      break;
      case 'web':
      $updatearr = array('web'=>date('Y-m-d H:i:s.u'));
      break;
      case 'app':
      $updatearr = array('app'=>date('Y-m-d H:i:s.u'));
      break;
      case 'joinnow':
      $updatearr = array('joinnow'=>date('Y-m-d H:i:s.u'));          
      break; 
      case 'joinnow2':
      $updatearr = array('joinnow2'=>date('Y-m-d H:i:s.u'));          
      break; 
      case 'pageload':
      $pagename = ['blog'];
      $blogname = ['see-preparation'];

      for($i=1;$i<10;$i++)
      {
        array_push($pagename,'blog'.$i);
        array_push($blogname,'see-preparation'.$i);
      }

      foreach ($blogname as $key => $bn) {
        if($bn==$this->input->post('source'))
          $updatearr[$pagename[$key]] = date('Y-m-d H:i:s.u');
      }
      break;         
    }



    if($exrec)
    {     
      if(date('Y-m-d H:i:s.u') > $exrec->updateduration)
        $updatearr['posteddatetime'] = date('Y-m-d H:i:s.u');      
      // $updatearr['source'] = $this->input->post('source');    
      $this->db->update('blogmobileusers',$updatearr,array('bloguserid'=>$exrec->bloguserid));
    }
    else{
      $updatearr['machinetype'] = $machinetype;
      $updatearr['ipaddress'] = $ipaddress;
      $updatearr['posteddate'] = date('Y-m-d');
      $updatearr['source'] = $this->input->post('source');
      $updatearr['posteddatetime'] = date('Y-m-d H:i:s.u');
      $updatearr['devicetype'] = $devicetype;
      // $updatearr['source'] = $this->uri->segment(2);
      $this->db->insert('blogmobileusers',$updatearr);
    }
  }



}
