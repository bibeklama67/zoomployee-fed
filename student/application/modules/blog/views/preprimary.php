<html dir="ltr" lang="en-US" xmlns="http://www.w3.org/1999/xhtml"
      xmlns:fb="http://ogp.me/ns/fb#">

<head>
<meta property="og:title" content="कक्षा १० को परिक्षा (SEE) को तयारिको लागि MiDas LIVE" />
<meta property="og:description" content="आफूले सिक्न चहाएको कुरा जहाँबाट जितबेला पनि सोध्नुहोस। शिक्षकहरूले तपाईलाई Internet बाट LIVE सिकाउनुहुन्छ। शिक्षकले सिकाएको Video Record गरेर पनि राखिएको हुन्छ। जसलाई तपाईले जतिबेला पनि दोहोर्याएर हेर्न सक्नुहुन्छ।" />
<meta property="og:image" content="<?= base_url()?>/assets/images/blog-img/MidasLive.png" />
<?php $this->view('/templates/header/blogheader');?>
	</head>

<body id="blogbody">
	<?php $this->view('/templates/header/blogtopheader');?> 

	<div id="ozdon" class="container " style="padding: 0px 5px;  margin-bottom: 15px; ">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pad-fix sec-block-bg give-border">
					<div class="wrap-content row1">
						<div class="wrap-arc-l">
										<div class="wrap-heading">											
											<p class="blog-p01" >
													माध्यामिक शिक्षा परीक्षा (SEE) को तयारीको लागि Online Coaching Class सुरु भएको छ।
											</p>												
										</div>
										<hr class="border-btm">
										<div class="misc">
                    
											<div class="col-sm-12" style="padding: 7px 5px;">
				<div class=" mar-ryt15">		
					<!-- Load Facebook SDK for JavaScript -->
					 <div id="fb-root"></div>
					 <script>
						 window.fbAsyncInit = function() {
						FB.init({
						  appId      : '1363536813767830',
						  xfbml      : true,
						  version    : 'v2.5'
						});
					  };
						 (function(d, s, id) {
					  var js, fjs = d.getElementsByTagName(s)[0];
					  if (d.getElementById(id)) return;
					  js = d.createElement(s); js.id = id;
					  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6";
					  fjs.parentNode.insertBefore(js, fjs);
					}(document, 'script', 'facebook-jssdk'));
													</script>
					<!-- Your share button code -->
					
					
					<?php $page_url = base_url()."blog/".$this->uri->segment(2)?>

<!--					<a href="javascript:void(0)" class="ui facebook button"><img src="<?= base_url()?>assets/images/fb-sharebutton.png" width="45" height="45" class="img-responsive" id="fbsharebtn"></a>-->
						<script>
//							$(".ui.facebook.button").click(function() {
//								FB.ui({
//									method: 'share',
//									href: '<?php echo $page_url?>',
//						}, function(response){});
//							})
						</script>


					<div class="fb-share-button pull-right" 
					data-href="<?php echo $page_url?>" 
					data-layout="button">
					
					
				  </div>
<hr>

	</div>
											</div>
												
										
										<div class="clearfix"></div>
										<hr class="">
										<div class="wrap-desc">
										<div class="col-sm-7 col-xs-12">
											<img src="<?= base_url()?>assets/images/blog-img/MidasLive.png" class="img-responsive" width="" height="" style="padding-bottom: 0px; margin-right: 15px; ">
										</div>
										<div class="col-sm-5 col-xs-12"><br>
											<div class="col-md-12 box-notice" style="">	
													<p>
														<strong>Subjects</strong>	<br>								
														Science, Maths, Opt. Maths, English, नेपाली<br>
														 <span class="text-center"></span>
													</p>	
											  </div>
											  <div class="clearfix"></div>
											  <div class="col-md-12 pad-fix">
											  	<p class="p-xs-font">देशै भरका कुनै पनि ठाउँका विद्यार्थीहरुले SEE मा उच्च स्कोर प्राप्त गर्न इन्टरनेट मार्फत Computer, Android Tablet वा Phone मा MiDas Online Coaching Class बाट पढ्न सक्नुहुन्छ।</p>
											  	<a  href= "<?= base_url()?>" class="hidden-xs" id="applynow" >
													<img src="<?= base_url()?>assets/images/TryNow.png" class="adjimg-xs">
													</a>
											  </div>
									
										
											<div class="clearfix"></div>
											<div class="col-md-12" >
<!--

													<a  href= "<?= base_url()?>" class="applynow01" id="applynow">
													<img src="<?= base_url()?>assets/images/TryNow.png" class="adjimg-xs">
													</a>
-->
											</div>
											<div class="clearfix"></div>
											</div>
											<div class="clearfix">
											</div>
											

											<p  class="blog-p04">Best Teachers are Online to Teach you Anytime Anywhere.</p>				
											 <div class="pull-left hidden-lg hidden-md hidden-sm visibl-xs">
													<a  href= "<?= base_url()?>" class="appevent" data-event="joinnow" id="applynow" >
													<img src="<?= base_url()?>assets/images/TryNow.png"  class="adjimg-xs">
																				</a>
											</div>
											<div class="clearfix"></div>
												<hr style="border:2px solid #9f8c8c; margin: 12px 0px;">

												
											<p class="blog-p05">
												
											<strong>Online Coaching Class का फाइदाहरू </strong>	<br>
											✓ लामो अनुभवप्राप्त एवम् विषयविज्ञ शिक्षकहरूले SEE मा धेरै Mark ल्याउन मद्दत गर्न इन्टरनेटबाट Coaching पढाउनुहुन्छ।    <br><br>
											✓ शिक्षकले हरेक Chapter बाट परिक्षामा आउन सक्ने प्रश्नका प्रकारहरू र प्रश्न पत्र योजनाको बारेमा बुझाउनुहुन्छ।  <br><br>
											✓ शिक्षकले हरेक Chapter बाट परिक्षामा आउन सक्ने प्रश्नहरू र तिनका उत्तरहरू परिक्षामा लेख्ने तरिका सहित बुझाउनु हुन्छ। <br><br>
											✓ शिक्षकले हरेक प्रश्नको उत्तर दिदां के गर्दा धेरै Mark आउँछ र के नगर्दा कति Mark काटिन्छ दुबै कुरा बुझाउनुहुन्छ।  <br><br>
											✓ शिक्षकहरुले Online Coaching मा सिकाएका Video Class हरु Record गरेर राखिएको हुन्छ, जसलाई विद्यार्थीहरुले जाहाँबाट जति वेला पनि <br> &nbsp; &nbsp; दोहोर्‍याएर हेरेर सिक्न सक्नुहुन्छ। <br><br>						
											✓ विध्यार्थीहरुले जहाँबाट जतिबेला पनि इन्टरनेट मार्फत Computer, Android Tablet वा Phone मा Online Coaching Class पढ्न सक्नुहुन्छ।   <br><br>
											✓ विध्यार्थीहरुले नजानेको कुरा जहाँबाट जतिबेला पनि सोध्न सक्नुहुन्छ।  <br><br>
											✓ अरू विध्यार्थीहरूले सोधेका प्रश्नका उत्तरहरूका Video हेरेर पनि सिक्न सकिन्छ।  <br><br>
											✓ MiDas LIVE मा परिक्षामा सोध्न सक्ने Sample प्रश्नहरु तथा त्यसका उत्तरहरु पनि राखिएको छ ।  <br><br>
											✓ MiDas LIVE मा पाठहरुको Concept बुझाउनको लागी Animated Video Lessons राखिएको छ। <br>
											
										</p>
<!-- 1. जाँचको तयारीको Special Videos<br>

अनुभबी शिक्षकहरूले SEE को तयारी गर्न चहाने विध्यार्थीहरूको लागि हरेक विषयका भिडियोहरू तयार गर्नुभएको छ। ती भिडियोहरू विध्यार्थीहरूले जहाँबाट, जहिले पनि, जति पटक पनि हेर्न सक्नुहुन्छ।<br><br>


२. आफुलाई जे आउदैन तेहि सिक्नुहोस<br>

हरेक विषयका अनुभबी शिक्षकहरू बेलुका ७ देखि ९ बजे सम्म [ Sunday to Friday] विध्यार्थीहरूलाई जाँचको तयारी गराउन उपलब्ध हुनुहुन्छ। विध्यार्थीहरूले कुनै पनि समयमा आफुले नजानेको वा नबुझेको प्रश्न सोध्न सक्नुहुन्छ। शिक्षकहरूले ति प्रश्नहरू MiDas LIVE मार्फत विध्यार्थीहरूको सामु बुझाएर गरिदिनुहुन्छ। शिक्षकले विध्यार्थीलाई बुझाउनु भएको भिडियो रेकर्ड गरेर पनि रखिएको हुन्छ जसले गर्दा, विध्यार्थीहरूले ति भिडियोहरू दोहोर्याएर हेर्न सक्नुहुन्छ।<br><br>

3. विध्यार्थीहरूको सिक्न सक्ने क्षमता अनुसार सिकाइने<br>

सबै विध्यार्थीहरूको सिक्न सक्ने क्षमता एउटै हुदैन। सबै विध्यार्थीहरूलाई एउटै तरिकाले सिकाउन मिल्दैन। MiDas LIVE मा शिक्षकहरूले विध्यार्थीहरूको क्षमता हेरिकन छुट्टा छुट्टै तरिकाले सिकाउनुहुन्छ।<br><br>

4. अरू विध्यार्थीहरूको प्रश्नबाट पनि सिक्न सकिने<br>

अरू विध्यार्थीहरूले शिक्षकहरूसंग सिकेको भिडियोहरू हेरेर पनि सिक्न सकिन्छ।<br><br>

5. समय र पैसाको बचत<br>

घरमा नै बसेर आफुले चहाएको बेला, आफुले नजानेको कुर सिक्न पाइने हुनाल समयको बचत हुन्छ। साथै यसरी पढ्दा खर्च पनि कम लाग्छ ।<br>

</p>  --> 
											<p  class="boxnotice2" style="">
<strong>
	Rate
</strong><br>
Rs. &nbsp;&nbsp;&nbsp; 500.00 per month [Any 1 Subject]<br>
Rs. &nbsp;&nbsp;&nbsp; 800.00 per month [Any 2 Subjects]<br>
Rs. 1, 000.00 per month [Any 3 Subjects]<br> 
Rs. 1, 200.00 per month [Any 4 Subjects]<br> 
Rs. 1, 500.00 per month [All 5 Subjects]<br>
Rs. 1, 500.00 for 5 months [Any 1 Subject]<br>
Rs. 2, 500.00 for 5 months [Any 2 Subjects]<br>
Rs. 3, 000.00 for 5 months [Any 3 Subjects]<br>
Rs. 4, 000.00 for 5 months [Any 4 Subjects]<br>
Rs. 5, 000.00 for 5 months [All 5 Subjects]	</p>
<br>

<div class="container pad-fix"> 
<div class="">
<!--
				<a class="" id="applynow" data-target="#apply-formm" data-toggle="modal" >
					<strong>Join Live Class Now</strong>
				</a>
-->
			</div>
		

			<!-- Modal -->
		<div id="apply-formm" class="modal fade" role="dialog">
		  <div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Join MiDas Live</h4>
			  </div>
			  <div class="modal-body">
				<div id="apply-form">

		<form class="form-horizontal" action="" id="blogform">
    <div class="form-group">
      <label class="control-label col-sm-5" for=""><span style="color:red">*</span>Full Name :</label>
      <div class="col-sm-6">
        <input type="text" class="form-control" id="" placeholder="" name="name">
      </div>
	</div>
     <div class="form-group">
      <label class="control-label col-sm-5" for=""><span style="color:red">*</span>Mobile Number :</label>
      <div class="col-sm-6">
        <input type="text" class="form-control numericonly" id="" placeholder="" name="mobilenumber" >
      </div>
    </div>
    <div class="form-group">
	  <label class="control-label col-sm-5" for=""><span style="color:red">*</span>Subjects :</label>
	  <div class="col-sm-6"> 
	          <input type="text" class="form-control" id="" placeholder="" name="subject">
	  </div>
	  </div>
     <div class="form-group">
	
	  <label class="control-label col-sm-5" for=""><span style="color:red">*</span>District :</label>
	    <div class="col-sm-6"> 
	           <!-- <input type="text" class="form-control" id="" placeholder="" name="district_id"> -->
	        <select name="district_id" class="form-control" id="district">
            <option value="">--Select District--</option>
            <?php foreach($districts as $row){ ?>
                <option value="<?php echo $row->districtid; ?>"><?php echo $row->districtname; ?></option>
            <?php } ?>
            </select>
	    </div>
	</div>
   <div class="form-group">
      <label class="control-label col-sm-5" for=""><span style="color:red">*</span>Address (City/Village Name ) :</label>
      <div class="col-sm-6">          
              <input type="text" class="form-control" id="" placeholder="" name="address">
      </div>    
	 
	</div>
   	
  </form>
</div>
			 <div id="blogresponse"></div>
			  </div>
			  <div class="modal-footer">
			  <button type="button" class="btn btn-default" id="blogsubmit">Submit</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>				
			  </div>
			</div>

		  </div>
		</div>
	
		
	
</div>


<p class="blog-p06">
	MiDas LIVE मार्फत सिक्न Internet को आवश्यक पर्छ।<br> </p>



<p class="blog-p06">
थप जानकारिको लागि:<br>
Ph: 9851228009, 9851170896, 01- 4244461
											</p>
<div class="">
	<a  href= "<?= base_url()?>" class="appevent" data-event="joinnow2" id="applynow" >
		<img src="<?= base_url()?>assets/images/TryNow.png" class="adjimg-xs">
	</a>
</div>

											<br><p  class="blog-p04">
											विद्यालय वा शिक्षकहरुले आफ्ना विद्यार्थीहरुलाई SEE को तयारी गराउन पनि <br>MiDas LIVE Online Coaching Class प्रयोग गर्न सक्नु हुन्छ। </p>

									</p>
									
									<hr style="border:2px solid #9f8c8c; margin: 12px 0px;">
									
										<p class="blog-p06">
										Student in all grades (Nursery to Grade 12) can join MiDas LIVE -  The Learning Platform.<br> </p>

										</div>
																			
									</div>

						
					</div>
					<div class="clearfix"></div>					
				</div>
				
	</div>
	</div>
	</script>
<!-- Go to www.addthis.com/dashboard to customize your tools -->

<!--       <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-59d9c5bfdb68590a"></script>-->
    
<?php $this->view('/templates/footer/footer');?>

<div id="mob-modal" class="modal fade" role="dialog" style="display: none;" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
	<!-- Modal content-->
	<div class="modal-content">								  
	  <div class="modal-body text-center">
	  	<div class="wrap">

	  	<img src="<?= base_url()?>/assets/images/logo_transparent_live.png" class="img-responsive blog-logo" style="padding-bottom: 0px;">
	  		<h3 class="bldtxt" style="margin-top:-5px; font-size: 18px; ">Online Coaching Class पढ्न</h3>
	  		<div class="col-xs-12 pad-fix appevent"  data-event="app">
	  			
	  		<a href="intent://midas.live#Intent;scheme=http;package=com.midas.midasstudent;end">
	  			<div class="make-box">								  		
					<div class="col-xs-3 mobpng-xs">
						<img src="http://www.midas.com.np/assets/images/phonetab.png" class="img-responsive" style="margin: 2px;">
					</div>
					<div class="col-xs-9 pad-fix">
						<h5 class="bluebold"><strong>Android Tablet</strong> वा <strong>Phone</strong> मा <strong>MiDas App Install</strong> गर्नुहोस।</h5>
					</div>
					<div class="clearfix"></div>								  			
	  			</div>
	  		</a>
	  		</div>
	  
	  	<div class="clearfix"></div>
	  		<div class="col-xs-12 pad-fix appevent" data-event="web">
	  			<h3 class="bldtxt1" style="font-size: 15px;">अथवा</h3>
	  			<a href="http://www.midas.live/login">
	  				<div class="make-box a2">
	  			<div class="col-xs-4">
	  				<img src="http://www.midas.com.np/assets/images/mvpc.png" class="img-responsive" style="margin:5px;">
	  			</div>
	  			<div class="col-xs-8 pad-fix">
	  				<h5 class="bluebold" style="margin-right:55px"><strong>Computer</strong> मा  
					<br><strong>www.midas.live</strong> मा जानुहोस।</h5>
	  			</div>
	  			<div class="clearfix"></div>
	  		</div>
	  			</a>
	  		
	  		</div>

	  	</div>
	  	<div class="clearfix"></div>
	 
	  </div>
	  <div class="modal-footer" style="padding: 5px;">
		<button type="button" class="btn btn-default appevent" data-dismiss="modal" data-event="close">Close</button>
	  </div>
	</div>

  </div>
</div>
</body>
</html>




		
	



	