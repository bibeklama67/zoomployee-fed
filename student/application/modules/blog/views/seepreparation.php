<html dir="ltr" lang="en-US" xmlns="http://www.w3.org/1999/xhtml"
      xmlns:fb="http://ogp.me/ns/fb#">

<head>
<meta property="og:title" content="कक्षा १० को परिक्षा (SEE) को तयारिको लागि MiDas LIVE" />
<meta property="og:description" content="आफूले सिक्न चहाएको कुरा जहाँबाट जितबेला पनि सोध्नुहोस। शिक्षकहरूले तपाईलाई Internet बाट LIVE सिकाउनुहुन्छ। शिक्षकले सिकाएको Video Record गरेर पनि राखिएको हुन्छ। जसलाई तपाईले जतिबेला पनि दोहोर्याएर हेर्न सक्नुहुन्छ।" />
<meta property="og:image" content="<?= base_url()?>/assets/images/blog-img/seepreparation.png" />
<?php $this->view('/templates/header/blogheader');?>
	</head>

<body id="blogbody">
	<?php $this->view('/templates/header/blogtopheader');?> 

	<div id="ozdon" class="container " style="padding: 0px 5px;  margin-bottom: 15px; ">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pad-fix sec-block-bg give-border">
					<div class="wrap-content row1">
						<div class="wrap-arc-l">
										<div class="wrap-heading">											
											<p class="blog-p04" >
													माध्यामिक शिक्षा परीक्षा (SEE) मा सजिलै धेरै Mark ल्याउन MiDas eCLASS - The Learning Platform
											</p>												
										</div>
										<hr class="border-btm">
										<div class="misc">
                    
											<div class="col-sm-12" style="padding: 7px 5px;">
				<div class=" mar-ryt15">		
					<!-- Load Facebook SDK for JavaScript -->
					 <div id="fb-root"></div>
					 <script>
						 window.fbAsyncInit = function() {
						FB.init({
						  appId      : '1363536813767830',
						  xfbml      : true,
						  version    : 'v2.5'
						});
					  };
						 (function(d, s, id) {
					  var js, fjs = d.getElementsByTagName(s)[0];
					  if (d.getElementById(id)) return;
					  js = d.createElement(s); js.id = id;
					  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6";
					  fjs.parentNode.insertBefore(js, fjs);
					}(document, 'script', 'facebook-jssdk'));
													</script>
					<!-- Your share button code -->
					
					
					<?php $page_url = base_url()."blog/".$this->uri->segment(2)?>

<!--					<a href="javascript:void(0)" class="ui facebook button"><img src="<?= base_url()?>assets/images/fb-sharebutton.png" width="45" height="45" class="img-responsive" id="fbsharebtn"></a>-->
						<script>
//							$(".ui.facebook.button").click(function() {
//								FB.ui({
//									method: 'share',
//									href: '<?php echo $page_url?>',
//						}, function(response){});
//							})
						</script>


					<div class="fb-share-button pull-right" 
					data-href="<?php echo $page_url?>" 
					data-layout="button">
					
					
				  </div>
<hr>

	</div>
											</div>
												
										
										<div class="clearfix"></div>
										<hr class="">
										<div class="wrap-desc">
										<div class="col-sm-7 col-xs-12">
											<img src="<?= base_url()?>assets/images/blog-img/seepreparation.png" class="img-responsive" width="" height="" style="padding-bottom: 0px; margin-right: 15px; "><br>
											<div class="col-md-12 box-notice" style="">	
													<p>
														<strong>Subjects</strong>	<br>								
														Science, Maths, Opt. Maths, English, नेपाली<br>
														 <span class="text-center"></span>
													</p>	
											  </div>										</div>
										<div class="col-sm-5 col-xs-12"><br>
											<!-- <div class="col-md-12 box-notice" style="">	
													<p>
														<strong>Subjects</strong>	<br>								
														Science, Maths, Opt. Maths, English, नेपाली<br>
														 <span class="text-center"></span>
													</p>	
											  </div> -->
											  <div class="clearfix"></div>
											  <div class="col-md-12 pad-fix">
							
											<p>
											<span style="color: black; font-size: 95%; line-height: 1.2">
											You may be studying Tuition somewhere but when you really want to learn,  MiDas eCLASS is there for you to help you.<br><br>
											<strong>MiDas eCLASS मा के छ?  </strong><br>
✓  अनुभबी शिक्षकहरूले पढाउनु भएको Tutorial Class को Video <br>
✓  परिक्षामा आउन सक्ने प्रश्नहरूको उत्तर लेख्ने तरिका सिकाएको Tutorial Class को Video<br>
✓  धेरै Mark आउने तरिका सिकाएको Tutorial Class को Video <br>
✓  किताबको पाठको Concept बुझाउने Animated Video Lesson <br>
✓  केहि नबुझे वा नजाने सोध्ने बितिकै उत्तर दिने Online Teachers <br>
<br></p>
													<p  class="blog-p03">विद्यार्थीहरुले Computer मा <a href="www.midaseclass.com">www.midaseclass.com</a> बाट र Android Tablet वा Phone मा <a href="https://play.google.com/store/apps/details?id=com.midas.midasstudent">MiDas eCLASS App</a> Install गरेर पढ्न सक्छन्।</p>	

											  	<a  href= "<?= base_url()?>" class="hidden-xs" id="applynow" >
													<img src="<?= base_url()?>assets/images/TryNow.png" class="adjimg-xs">
													</a>			
											  </div>
									
										
											<div class="clearfix"></div>
											<div class="col-md-12" >
											<!--

													<a  href= "<?= base_url()?>" class="applynow01" id="applynow">
													<img src="<?= base_url()?>assets/images/TryNow.png" class="adjimg-xs">
													</a>
-->
											</div>
											<div class="clearfix"></div>
											</div>
											<div class="clearfix">
											</div>
											
											
											 <div class="pull-left hidden-lg hidden-md hidden-sm visibl-xs"><a  href= "<?= base_url()?>" class="appevent" data-event="joinnow" id="applynow" ><img src="<?= base_url()?>assets/images/TryNow.png"  class="adjimg-xs">
																				</a>
											</div> 

											<div class="clearfix"></div>
<p  class="blog-p04">
											विद्यालय वा शिक्षकहरुले आफ्ना विद्यार्थीहरुलाई SEE को तयारी गराउन पनि MiDas eCLASS प्रयोग गर्न सक्नु हुन्छ। </p>
											
											<hr style="border:2px solid #9f8c8c; margin: 12px 0px;">


												
											<p class="blog-p05">
												
											<strong>SEE मा धेरै Mark ल्याउन MiDas eCLASS नै किन? </strong>	<br>

											
											✓ MiDas eCLASS मा SEE को तयारी गर्न चहाने विध्यार्थीहरूको लागि अनुभबी शिक्षकहरूले पढाउनु भएको हरेक विषयका Tutorial Class हरू Record गरेर राखिएको छ। ति Video हरू विध्यार्थीहरूले जहाँबाट, जहिले पनि, जति पटक पनि हेर्न सक्नुहुन्छ।<br><br>

											✓ विध्यार्थीहरूले आफुलाई जुन Topic आउदैन त्यसैको Tutorial Class को Video हेरेर सिक्न सक्नुहुन्छ।  <br><br>
											✓ MiDas eCLASS को Tutorial Class मा शिक्षकहरूले पढाउदा परिक्षामा कस्तो प्रकारको प्रश्नहरू आउछन र कस्तो प्रश्नको उत्तर कसरी लेख्दा धेरै Mark आउछ र के नगर्दा कति Mark काटिन्छ सिकाउनुहुन्छ। <br><br>
											✓ विध्यार्थीहरूले परिक्षामा आउन सक्ने प्रश्नहरू मध्य आफुलाई जुन प्रश्न आउदैन सोही प्रश्नको उत्तर लेख्ने तरिका सिकाएको Tutorial Class को Video हेरेर सिक्न सक्नुहुन्छ। <br><br>
											✓ MiDas eCLASS मा मुख्य विषयका पाठहरुको Concept बुझाउने Animated Video Lesson हरू राखिएको छ। यि Video Lesson हरूले विध्यार्थीहरुको पढाइमा रूची बढाउनुको साथै अप्ठेरा पाठहरु सजिलै बुझ्न मद्दत गर्छ। यसले गर्दा उनिहरूको सिकाइ दिर्घकालीन हुन्छ ।  <br><br>
											✓ पढेको कुर नबुझेमा, कुनै प्रश्नको उत्तर नजानेमा वा पढाइको बारेमा कुनै जिज्ञासा भएमा विध्यार्थीहरुले जहाँबाट जतिबेला पनि MiDas eCLASS मा सोध्न सक्छन्। हरेक विषयका सयौं शिक्षकहरू बेलुका ५ बजे देखि ९ बजे सम्म MiDas eCLASS मा Online हुनुहुन्छ। उहाँहरूले विद्यार्थीहरूलाई Tuition Teacher ले घरमै सिकाए जस्तै गरि हरेक प्रश्नको उत्तर तुरुन्त दिनुहुन्छ। 
											<br>
										</p>

<hr class="border-btm">

<p class="blog-p06">
Computer, Android Tablet वा Phone बाट MiDas eCLASS प्रयोग गर्न सकिन्छ। <br>
MiDas eCLASS बाट सिक्न Internet को आवश्यक पर्छ।  </p>

<br>
										<p class="blog-p06">
										SEE को तयारीको लागि MiDas eCLASS ले विद्यार्थीहरुलाई  कसरी मद्दत गर्छ थाहा पाउन 
www.midaseclass.com मा आजै Log In गर्नुहोस <br> 
वा Google Play बाट MiDas eCLASS App Install गर्नुहोस। <br>
यसको लागि पैसा लाग्दैन।<br><br>

यदि तपाईलाई मन पर्यो भने पैसा तिरेर MiDas eCLASS बाट पढ्न सक्नुहुन्छ।  
 <br><br>

IME Pay, eSEWA, Master Card वा Visa Card बाट MiDas eCLASS को लागि पैसा तिर्न सकिन्छ।
<br><br>

नजिकैको IME Counter, eSEWA Zone वा  पुस्तक, स्टेसनरि तथा मोबाइल पसलहरूमा गएर पनि पैसा तिर्न सकिन्छ। <br>
साथै देश भरिको कुनै पनि बैंकबाट पनि पैसा तिर्न सकिन्छ।  <br><br>

मुख्य शहरहरूमा Cash On Delivery (CoD) को पनि ब्यवस्था छ।

</p>
											<p  class="boxnotice2" style="">
<strong>
	Rate
</strong><br>

Rs. 1, 500.00 for 1 subject<br>
Rs. 2, 500.00 for 2 subjects<br>
Rs. 3, 000.00 for 3 subjects<br>
Rs. 4, 000.00 for 4 subjects<br>
Rs. 5, 000.00 for 5 subjects</p>
<br>


<div class="container pad-fix"> 
<div class="">
<!--
				<a class="" id="applynow" data-target="#apply-formm" data-toggle="modal" >
					<strong>Join Live Class Now</strong>
				</a>
-->
			</div>
		

			<!-- Modal -->
		<div id="apply-formm" class="modal fade" role="dialog">
		  <div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Join MiDas Live</h4>
			  </div>
			  <div class="modal-body">
				<div id="apply-form">

		<form class="form-horizontal" action="" id="blogform">
    <div class="form-group">
      <label class="control-label col-sm-5" for=""><span style="color:red">*</span>Full Name :</label>
      <div class="col-sm-6">
        <input type="text" class="form-control" id="" placeholder="" name="name">
      </div>
	</div>
     <div class="form-group">
      <label class="control-label col-sm-5" for=""><span style="color:red">*</span>Mobile Number :</label>
      <div class="col-sm-6">
        <input type="text" class="form-control numericonly" id="" placeholder="" name="mobilenumber" >
      </div>
    </div>
    <div class="form-group">
	  <label class="control-label col-sm-5" for=""><span style="color:red">*</span>Subjects :</label>
	  <div class="col-sm-6"> 
	          <input type="text" class="form-control" id="" placeholder="" name="subject">
	  </div>
	  </div>
     <div class="form-group">
	
	  <label class="control-label col-sm-5" for=""><span style="color:red">*</span>District :</label>
	    <div class="col-sm-6"> 
	           <!-- <input type="text" class="form-control" id="" placeholder="" name="district_id"> -->
	        <select name="district_id" class="form-control" id="district">
            <option value="">--Select District--</option>
            <?php foreach($districts as $row){ ?>
                <option value="<?php echo $row->districtid; ?>"><?php echo $row->districtname; ?></option>
            <?php } ?>
            </select>
	    </div>
	</div>
   <div class="form-group">
      <label class="control-label col-sm-5" for=""><span style="color:red">*</span>Address (City/Village Name ) :</label>
      <div class="col-sm-6">          
              <input type="text" class="form-control" id="" placeholder="" name="address">
      </div>    
	 
	</div>
   	
  </form>
</div>
			 <div id="blogresponse"></div>
			  </div>
			  <div class="modal-footer">
			  <button type="button" class="btn btn-default" id="blogsubmit">Submit</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>				
			  </div>
			</div>

		  </div>
		</div>
	
		
	
</div>



<p class="blog-p06">
थप जानकारिको लागि:<br>
Ph: 9851228009, 9851170896, 01- 4244461
											</p>
<div class="">
	<a  href= "<?= base_url()?>" class="appevent" data-event="joinnow2" id="applynow" >
		<img src="<?= base_url()?>assets/images/TryNow.png" class="adjimg-xs">
	</a>
</div>

									</p>
									
									<hr style="border:2px solid #9f8c8c; margin: 12px 0px;">
									
										<p class="blog-p06">
										Student of all grades (Nursery to Grade 10) also can join MiDas eCLASS -  The Learning Platform.<br> </p>

										</div>
																			
									</div>

						
					</div>
					<div class="clearfix"></div>					
				</div>
				
	</div>
	</div>
	</script>
<!-- Go to www.addthis.com/dashboard to customize your tools -->

<!--       <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-59d9c5bfdb68590a"></script>-->
    
<?php $this->view('/templates/footer/footer');?>

<div id="mob-modal" class="modal fade" role="dialog" style="display: none;" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
	<!-- Modal content-->
	<div class="modal-content">								  
	  <div class="modal-body text-center">
	  	<div class="wrap">

	  	<img src="<?= base_url()?>/assets/images/midaseclass001.png" class="img-responsive blog-logo" style="padding-bottom: 0px;">
	  		<h3 class="bldtxt" style="margin-top:-5px; font-size: 18px; ">Online Coaching Class पढ्न</h3>
	  		<div class="col-xs-12 pad-fix appevent"  data-event="app">
	  			
	  		<a href="intent://midas.live#Intent;scheme=http;package=com.midas.midasstudent;end">
	  			<div class="make-box">								  		
					<div class="col-xs-3 mobpng-xs">
						<img src="http://www.midas.com.np/assets/images/phonetab.png" class="img-responsive" style="margin: 2px;">
					</div>
					<div class="col-xs-9 pad-fix">
						<h5 class="bluebold"><strong>Android Tablet</strong> वा <strong>Phone</strong> मा <strong>MiDas App Install</strong> गर्नुहोस।</h5>
					</div>
					<div class="clearfix"></div>								  			
	  			</div>
	  		</a>
	  		</div>
	  
	  	<div class="clearfix"></div>
	  		<div class="col-xs-12 pad-fix appevent" data-event="web">
	  			<h3 class="bldtxt1" style="font-size: 15px;">अथवा</h3>
	  			<a href="http://www.midas.live/login">
	  				<div class="make-box a2">
	  			<div class="col-xs-4">
	  				<img src="http://www.midas.com.np/assets/images/mvpc.png" class="img-responsive" style="margin:5px;">
	  			</div>
	  			<div class="col-xs-8 pad-fix">
	  				<h5 class="bluebold" style="margin-right:55px"><strong>Computer</strong> मा  
					<br><strong>www.midas.live</strong> मा जानुहोस।</h5>
	  			</div>
	  			<div class="clearfix"></div>
	  		</div>
	  			</a>
	  		
	  		</div>

	  	</div>
	  	<div class="clearfix"></div>
	 
	  </div>
	  <div class="modal-footer" style="padding: 5px;">
		<button type="button" class="btn btn-default appevent" data-dismiss="modal" data-event="close">Close</button>
	  </div>
	</div>

  </div>
</div>
</body>
</html>




		
	



	