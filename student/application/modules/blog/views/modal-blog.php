<div id="mob-modal" class="modal fade" role="dialog" style="display: none;" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
	<!-- Modal content-->
	<div class="modal-content">								  
	  <div class="modal-body text-center">
	  	<div class="wrap">

	  	<img src="<?= base_url()?>/assets/images/logo_transparent_live.png" class="img-responsive blog-logo" style="padding-bottom: 0px;">
	  		<h3 class="bldtxt" style="margin-top:-5px; font-size: 18px; ">Online Coaching Class पढ्न</h3>
	  		<div class="col-xs-12 pad-fix appevent"  data-event="app">
	  			
	  		<a href="intent://midas.live#Intent;scheme=http;package=com.midas.midasstudent;end">
	  			<div class="make-box">								  		
					<div class="col-xs-3 mobpng-xs">
						<img src="http://www.midas.com.np/assets/images/phonetab.png" class="img-responsive" style="margin: 2px;">
					</div>
					<div class="col-xs-9 pad-fix">
						<h5 class="bluebold"><strong>Android Tablet</strong> वा <strong>Phone</strong> मा <strong>MiDas App Install</strong> गर्नुहोस।</h5>
					</div>
					<div class="clearfix"></div>								  			
	  			</div>
	  		</a>
	  		</div>
	  
	  	<div class="clearfix"></div>
	  		<div class="col-xs-12 pad-fix appevent" data-event="web">
	  			<h3 class="bldtxt1" style="font-size: 15px;">अथवा</h3>
	  			<a href="<?= base_url()?>/login/index/true">
	  				<div class="make-box a2">
					<div class="col-xs-4">
						<img src="http://www.midas.com.np/assets/images/mvpc.png" class="img-responsive" style="margin:5px;">
					</div>
					<div class="col-xs-8 pad-fix">
						<h5 class="bluebold" style="margin-right:55px"><strong>Computer</strong> मा  
						<br><strong>www.midas.live</strong> मा जानुहोस।</h5>
					</div>
					<div class="clearfix"></div>
	  			</div>
	  			</a>
	  		
	  		</div>

	  	</div>
	  	<div class="clearfix"></div>
	 
	  </div>
	  <div class="modal-footer" style="padding: 5px;">
		<button type="button" class="btn btn-default appevent" data-dismiss="modal" data-event="close">Close</button>
	  </div>
	</div>

  </div>
</div>