
<?php $this->view('/templates/header/blogheader');?>

<body id="blogbody">
	<div class="head_main_wrap">
		<div class="container" style="padding:10px 0;">
			<div class="col-lg-6 col-md-4 col-sm-3 col-xs-4" style=" margin: -9px; margin-left: -1px; padding-left: 7px;  ">
				<?php
				if($_SERVER['HTTP_HOST']=='www.midas.live')
					$src = base_url()."assets/images/mlogo4.png";
				else   
					$src = base_url()."assets/images/mlogo4.png.png";
				?>
				<a href="<?= base_url()?>">
					<img src="<?= $src?>" class="img-responsive" style="padding-bottom: 0px;"></a>
				</div>
				<div class="col-lg-6 col-md-8 col-sm-9 col-xs-8" style="right: 10px; top:0px;">
					<i class="pull-right" style="color:#f27b06; padding:15px 0px; text-align: center; font-size: 20px"> Learn Anytime Anywhere</i>
				</div>
			</div>
		</div> 

		<div class="container " style="padding: 0px 5px;  margin-bottom: 15px; ">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pad-fix sec-block-bg give-border">

				<div class="clearfix"></div>
				<div class="wrap-content row2">
					<div class=" col-lg-4 col-md-4 col-xs-12 pad-fix">
						<div class="wrap-arc-l th">
							<div class="wrap-img">
								<img src="http://www.midas.com.np/assets/images/blog-img/blogimg-01.png" class="img-responsive" style="padding-bottom: 0px;">
							</div>
							<div class="wrap-heading">
								<h3 class="heading-l">
									<a href="http://www.midas.com.np/design/blogdetail">
										शिक्षक, विद्यार्थी र अभिभावकलाई सिकाउने एउटै प्लेटफर्म
									</a>	
								</h3>
							</div>
							<div class="wrap-sdesc">
								<p>काठमाडौं : धेरै अभिभावकहरुको एउटै साझा समस्या छ, बालबालिकामा प्रविधिको लत। स्कुलबाट आएपछि होस् या स्कुल जानुअघि उनीहरु मोबाइल या अन्य ग्याजेटमा झुण्डिएका हुन्छन्।</p>
							</div>

						</div>
					</div>
					<div class=" col-lg-4 col-md-4 col-xs-12 pad-fix">
						<div class="wrap-arc-l th">
							<div class="wrap-img">
								<img src="http://www.midas.com.np/assets/images/blog-img/blogimg-01.png" class="img-responsive" style="padding-bottom: 0px;">
							</div>
							<div class="wrap-heading">
								<h3 class="heading-l">
									<a href="http://www.midas.com.np/design/blogdetail">
										शिक्षक, विद्यार्थी र अभिभावकलाई सिकाउने एउटै प्लेटफर्म
									</a>	
								</h3>
							</div>
							<div class="wrap-sdesc">
								<p>काठमाडौं : धेरै अभिभावकहरुको एउटै साझा समस्या छ, बालबालिकामा प्रविधिको लत। स्कुलबाट आएपछि होस् या स्कुल जानुअघि उनीहरु मोबाइल या अन्य ग्याजेटमा झुण्डिएका हुन्छन्।</p>
							</div>

						</div>
					</div>
					<div class=" col-lg-4 col-md-4 col-xs-12 pad-fix">
						<div class="wrap-arc-l th">
							<div class="wrap-img">
								<img src="http://www.midas.com.np/assets/images/blog-img/blogimg-01.png" class="img-responsive" style="padding-bottom: 0px;">
							</div>
							<div class="wrap-heading">
								<h3 class="heading-l">
									<a href="http://www.midas.com.np/design/blogdetail">
										शिक्षक, विद्यार्थी र अभिभावकलाई सिकाउने एउटै प्लेटफर्म
									</a>	
								</h3>
							</div>
							<div class="wrap-sdesc">
								<p>काठमाडौं : धेरै अभिभावकहरुको एउटै साझा समस्या छ, बालबालिकामा प्रविधिको लत। स्कुलबाट आएपछि होस् या स्कुल जानुअघि उनीहरु मोबाइल या अन्य ग्याजेटमा झुण्डिएका हुन्छन्।</p>
							</div>

						</div>
					</div>

				</div>
				<div class="wrap-content row2">
					<div class=" col-lg-4 col-md-4 col-xs-12 pad-fix">
						<div class="wrap-arc-l th">
							<div class="wrap-img">
								<img src="http://www.midas.com.np/assets/images/blog-img/blogimg-01.png" class="img-responsive" style="padding-bottom: 0px;">
							</div>
							<div class="wrap-heading">
								<h3 class="heading-l">
									<a href="http://www.midas.com.np/design/blogdetail">
										शिक्षक, विद्यार्थी र अभिभावकलाई सिकाउने एउटै प्लेटफर्म
									</a>	
								</h3>
							</div>
							<div class="wrap-sdesc">
								<p>काठमाडौं : धेरै अभिभावकहरुको एउटै साझा समस्या छ, बालबालिकामा प्रविधिको लत। स्कुलबाट आएपछि होस् या स्कुल जानुअघि उनीहरु मोबाइल या अन्य ग्याजेटमा झुण्डिएका हुन्छन्।</p>
							</div>

						</div>
					</div>
					<div class=" col-lg-4 col-md-4 col-xs-12 pad-fix">
						<div class="wrap-arc-l th">
							<div class="wrap-img">
								<img src="http://www.midas.com.np/assets/images/blog-img/blogimg-01.png" class="img-responsive" style="padding-bottom: 0px;">
							</div>
							<div class="wrap-heading">
								<h3 class="heading-l">
									<a href="http://www.midas.com.np/design/blogdetail">
										शिक्षक, विद्यार्थी र अभिभावकलाई सिकाउने एउटै प्लेटफर्म
									</a>	
								</h3>
							</div>
							<div class="wrap-sdesc">
								<p>काठमाडौं : धेरै अभिभावकहरुको एउटै साझा समस्या छ, बालबालिकामा प्रविधिको लत। स्कुलबाट आएपछि होस् या स्कुल जानुअघि उनीहरु मोबाइल या अन्य ग्याजेटमा झुण्डिएका हुन्छन्।</p>
							</div>

						</div>
					</div>
					<div class=" col-lg-4 col-md-4 col-xs-12 pad-fix">
						<div class="wrap-arc-l th">
							<div class="wrap-img">
								<img src="http://www.midas.com.np/assets/images/blog-img/blogimg-01.png" class="img-responsive" style="padding-bottom: 0px;">
							</div>
							<div class="wrap-heading">
								<h3 class="heading-l">
									<a href="http://www.midas.com.np/design/blogdetail">
										शिक्षक, विद्यार्थी र अभिभावकलाई सिकाउने एउटै प्लेटफर्म
									</a>	
								</h3>
							</div>
							<div class="wrap-sdesc">
								<p>काठमाडौं : धेरै अभिभावकहरुको एउटै साझा समस्या छ, बालबालिकामा प्रविधिको लत। स्कुलबाट आएपछि होस् या स्कुल जानुअघि उनीहरु मोबाइल या अन्य ग्याजेटमा झुण्डिएका हुन्छन्।</p>
							</div>

						</div>
					</div>

				</div>
			</div>

		</div>	

	</body>

	<?php $this->view('/templates/footer/footer');?>
