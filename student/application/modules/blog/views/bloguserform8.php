<html dir="ltr" lang="en-US" xmlns="http://www.w3.org/1999/xhtml"
      xmlns:fb="http://ogp.me/ns/fb#">

<head>
<meta property="og:title" content="कक्षा १० को परिक्षा (SEE) को तयारिको लागि MiDas LIVE" />
<meta property="og:description" content="आफूले सिक्न चहाएको कुरा जहाँबाट जितबेला पनि सोध्नुहोस। शिक्षकहरूले तपाईलाई Internet बाट LIVE सिकाउनुहुन्छ। शिक्षकले सिकाएको Video Record गरेर पनि राखिएको हुन्छ। जसलाई तपाईले जतिबेला पनि दोहोर्याएर हेर्न सक्नुहुन्छ।" />
<meta property="og:image" content="<?= base_url()?>/assets/images/blog-img/MidasLive8.png" />
<?php $this->view('/templates/header/blogheader');?>
	</head>

<body id="blogbody">
<?php $this->view('/templates/header/blogtopheader');?> 
	<div id="ozdon" class="container " style="padding: 0px 5px;  margin-bottom: 15px; ">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pad-fix sec-block-bg give-border">
					<div class="wrap-content row1">
						<div class="wrap-arc-l">
										<div class="wrap-heading" >
											<h1 class="heading-l text-center">
											
													कक्षा १० को परिक्षा (SEE) को तयारिको लागि <img src="<?= base_url()?>/assets/images/mlogo4.png" class="img-responsive" style="padding-bottom: 0px; max-width: 210px;display: inline-flex;">
													
											</h1>
										</div>
										<hr class="border-btm">
										<div class="misc">
                    
											<div class="col-sm-12" style="padding: 7px 5px;">
<p class="blog-p01">	
	आफूले सिक्न चहाएको कुरा जहाँबाट जितबेला पनि सोध्नुहोस। शिक्षकहरूले तपाईलाई Internet बाट <span style="color:#e91a27;">LIVE</span> सिकाउनुहुन्छ। शिक्षकले सिकाएको Video Record गरेर पनि राखिएको हुन्छ। जसलाई तपाईले जतिबेला पनि दोहोर्याएर हेर्न सक्नुहुन्छ। 
</p><div class=" mar-ryt15">		
					<!-- Load Facebook SDK for JavaScript -->
					 <div id="fb-root"></div>
					 <script>
						 window.fbAsyncInit = function() {
						FB.init({
						  appId      : '1363536813767830',
						  xfbml      : true,
						  version    : 'v2.5'
						});
					  };
						 (function(d, s, id) {
					  var js, fjs = d.getElementsByTagName(s)[0];
					  if (d.getElementById(id)) return;
					  js = d.createElement(s); js.id = id;
					  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6";
					  fjs.parentNode.insertBefore(js, fjs);
					}(document, 'script', 'facebook-jssdk'));
													</script>
					<!-- Your share button code -->
					
					
					<?php $page_url = base_url()."blog/".$this->uri->segment(2)?>

<!--					<a href="javascript:void(0)" class="ui facebook button"><img src="<?= base_url()?>assets/images/fb-sharebutton.png" width="45" height="45" class="img-responsive" id="fbsharebtn"></a>-->
						<script>
//							$(".ui.facebook.button").click(function() {
//								FB.ui({
//									method: 'share',
//									href: '<?php echo $page_url?>',
//						}, function(response){});
//							})
						</script>


					<div class="fb-share-button pull-right" 
					data-href="<?php echo $page_url?>" 
					data-layout="button">
					
				  </div>

	</div>
											</div>
												
										
										<div class="clearfix"></div>
										<hr class="">
										<div class="wrap-desc">
										<div class="col-sm-7 col-xs-12">
											<img src="<?= base_url()?>assets/images/blog-img/MidasLive.png" class="img-responsive" width="" height="" style="padding-bottom: 0px; margin-right: 15px; ">
										</div>
										<div class="col-sm-5 col-xs-12">
										<p>
											<strong style="color:#2071b9;font-size: 115%;">Ask questions - Anytime from Anywhere</strong><br></p>
											<p>
											<span style="color: black; font-size: 95%; line-height: 1.2">
											1. Students may be studying Tuition somewhere. But when they are studying at home, they may have Questions to ask. MiDas LIVE is there for them to help. <br><br>

											2. Some students may feel uneasy to ask questions in front of their friends. With MiDas LIVE, they can ask question privately.<br>
											 <br>
											3. In Tuition Class, students are allowed to ask question only from the currently running chapter. But students can ask any question from any chapter on MiDas LIVE.<br><br>
											4. Every Question-Answer is recorded so that students can revise when they get confused.<br>
											</span>
										</p>

								
										</div>
											
											<div class="clearfix"></div>
											<p style="color:#e91a27;text-align: center;font-size: 190%;">Ask questions anytime from anywhere, Best Teachers will answer you.</p>
												<div class="col-md-4 col-md-offset-3" style="padding: 0px 15px;background: rgba(255, 165, 0, 0.27);border: 1px solid orange; margin-bottom: 15px;">	
													<p>
														<strong>Subjects</strong>	<br>								
														Science, Maths, Opt. Maths, English, नेपाली<br>
														LIVE @ 7 PM to 9 PM [ Sunday to Friday]<br>
														 <span class="text-center"></span>
													</p>
													 <p class="text-center">
														कार्तिक देखि फाल्गुन सम्म
													 </p>	
											 </div>
											 	<div class="col-md-2" >
													<a  class="applynow01" id="applynow"  data-target="#apply-formm" data-toggle="modal">
														Apply Now
													</a>
											</div>
											<div class="clearfix"></div>
												<hr style="border:2px solid #9f8c8c; margin: 20px 0px;">

												
											<p style="clear: both; padding: 5px 15px;">
												
											<strong>MiDas LIVEका फाइदाहरू </strong>	<br>

1. जाँचको तयारीको Special Videos<br>

अनुभबी शिक्षकहरूले SEE को तयारी गर्न चहाने विध्यार्थीहरूको लागि हरेक विषयका भिडियोहरू तयार गर्नुभएको छ। ती भिडियोहरू विध्यार्थीहरूले जहाँबाट, जहिले पनि, जति पटक पनि हेर्न सक्नुहुन्छ।<br><br>


२. आफुलाई जे आउदैन तेहि सिक्नुहोस<br>

हरेक विषयका अनुभबी शिक्षकहरू बेलुका ७ देखि ९ बजे सम्म [ Sunday to Friday] विध्यार्थीहरूलाई जाँचको तयारी गराउन उपलब्ध हुनुहुन्छ। विध्यार्थीहरूले कुनै पनि समयमा आफुले नजानेको वा नबुझेको प्रश्न सोध्न सक्नुहुन्छ। शिक्षकहरूले ति प्रश्नहरू MiDas LIVE मार्फत विध्यार्थीहरूको सामु बुझाएर गरिदिनुहुन्छ। शिक्षकले विध्यार्थीलाई बुझाउनु भएको भिडियो रेकर्ड गरेर पनि रखिएको हुन्छ जसले गर्दा, विध्यार्थीहरूले ति भिडियोहरू दोहोर्याएर हेर्न सक्नुहुन्छ।<br><br>

3. विध्यार्थीहरूको सिक्न सक्ने क्षमता अनुसार सिकाइने<br>

सबै विध्यार्थीहरूको सिक्न सक्ने क्षमता एउटै हुदैन। सबै विध्यार्थीहरूलाई एउटै तरिकाले सिकाउन मिल्दैन। MiDas LIVE मा शिक्षकहरूले विध्यार्थीहरूको क्षमता हेरिकन छुट्टा छुट्टै तरिकाले सिकाउनुहुन्छ।<br><br>

4. अरू विध्यार्थीहरूको प्रश्नबाट पनि सिक्न सकिने<br>

अरू विध्यार्थीहरूले शिक्षकहरूसंग सिकेको भिडियोहरू हेरेर पनि सिक्न सकिन्छ।<br><br>

5. समय र पैसाको बचत<br>

घरमा नै बसेर आफुले चहाएको बेला, आफुले नजानेको कुर सिक्न पाइने हुनाल समयको बचत हुन्छ। साथै यसरी पढ्दा खर्च पनि कम लाग्छ ।<br>

</p>
											<p style="padding: 0px 15px;background: rgba(255, 165, 0, 0.27);border: 1px solid orange;">
<strong>
	Rate
</strong><br>
Rs. &nbsp;&nbsp;&nbsp;500.00 per month [Any 1 Subject]<br>
Rs. 1, 500.00 for 5 months [Any 1 Subject]<br>
Rs. 1, 000.00 per month [Any 3 Subjects]<br> 
Rs. 4, 000.00 for 5 months [Any 3 Subjects]<br> 
Rs. 1, 500.00 per month [All 5 Subjects]<br>
Rs. 5, 000.00 for 5 months [All 5 Subjects]	</p>
<br>

<div class="container pad-fix">
<div class="">
<!--
				<a class="" id="applynow" data-target="#apply-formm" data-toggle="modal" >
					<strong>Join Live Class Now</strong>
				</a>
-->
			</div>
		

			<!-- Modal -->
		<div id="apply-formm" class="modal fade" role="dialog">
		  <div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Join MiDas Live</h4>
			  </div>
			  <div class="modal-body">
				<div id="apply-form">

		<form class="form-horizontal" action="" id="blogform">
    <div class="form-group">
      <label class="control-label col-sm-5" for=""><span style="color:red">*</span>Full Name :</label>
      <div class="col-sm-6">
        <input type="text" class="form-control" id="" placeholder="" name="name">
      </div>
	</div>
     <div class="form-group">
      <label class="control-label col-sm-5" for=""><span style="color:red">*</span>Mobile Number :</label>
      <div class="col-sm-6">
        <input type="text" class="form-control numericonly" id="" placeholder="" name="mobilenumber" >
      </div>
    </div>
    <div class="form-group">
	  <label class="control-label col-sm-5" for=""><span style="color:red">*</span>Subjects :</label>
	  <div class="col-sm-6"> 
	          <input type="text" class="form-control" id="" placeholder="" name="subject">
	  </div>
	  </div>
     <div class="form-group">
	
	  <label class="control-label col-sm-5" for=""><span style="color:red">*</span>District :</label>
	    <div class="col-sm-6"> 
	           <!-- <input type="text" class="form-control" id="" placeholder="" name="district_id"> -->
	        <select name="district_id" class="form-control" id="district">
            <option value="">--Select District--</option>
            <?php foreach($districts as $row){ ?>
                <option value="<?php echo $row->districtid; ?>"><?php echo $row->districtname; ?></option>
            <?php } ?>
            </select>
	    </div>
	</div>
   <div class="form-group">
      <label class="control-label col-sm-5" for=""><span style="color:red">*</span>Address (City/Village Name ) :</label>
      <div class="col-sm-6">          
              <input type="text" class="form-control" id="" placeholder="" name="address">
      </div>    
	 
	</div>
   	
  </form>
</div>
			 <div id="blogresponse"></div>
			  </div>
			  <div class="modal-footer">
			  <button type="button" class="btn btn-default" id="blogsubmit">Submit</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>				
			  </div>
			</div>

		  </div>
		</div>
	
		
	
</div>


<p>
	MiDas LIVE मार्फत सिक्न Internet को आवश्यक पर्छ।<br> </p>



<p>
थप जानकारिको लागि:<br>
Ph: 9851228009, 9851170896
											</p>
											<div class="">
				<a class="" id="applynow"  data-target="#apply-formm" data-toggle="modal">
					Apply Now
				</a>
			</div>


									</p>
										</div>
																			
									</div>
						
					</div>
					<div class="clearfix"></div>					
				</div>
				
	</div>
	</div>
	</script>
<!-- Go to www.addthis.com/dashboard to customize your tools -->

<!--       <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-59d9c5bfdb68590a"></script>-->
    
<?php $this->view('/templates/footer/footer');?>

</body>
</html>



		
	



	