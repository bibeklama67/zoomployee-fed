<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends App_controller{
	function __construct(){

		parent::__construct();
		ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
		//error_reporting(E_ALL);
		$d=$this->config->item('default');
        $df=$this->config->item($d); 
        $df['js'][]='blog/blog';
		$this->config->set_item($d,$df);
        $this->load->model('blog_model','model');
        $this->load->library('form_validation');
     
        
      

	}

	public function index(){
		// $this->load->view('blogindex');
		redirect('blog/see_preparation');
    	
	}

	public function see_preparation(){
		$districts =$this->model->district();
		$vars      =array('districts'=>$districts);
		$this->load->vars($vars);
    	$this->load->view('bloguserform');
	}
	public function see_preparation3(){
		$districts =$this->model->district();
		$vars      =array('districts'=>$districts);
		$this->load->vars($vars);
    	$this->load->view('bloguserform3');
	}
	public function see_preparation4(){
		$districts =$this->model->district();
		$vars      =array('districts'=>$districts);
		$this->load->vars($vars);
    	$this->load->view('bloguserform');
	}
	public function see_preparation5(){
		$districts =$this->model->district();
		$vars      =array('districts'=>$districts);
		$this->load->vars($vars);
    	$this->load->view('bloguserform5');
	}
	public function see_preparation8(){
		$districts =$this->model->district();
		$vars      =array('districts'=>$districts);
		$this->load->vars($vars);
    	$this->load->view('bloguserform8');
	}
	public function see_preparation10(){
		$districts =$this->model->district();
		$vars      =array('districts'=>$districts);
		$this->load->vars($vars);
    	$this->load->view('bloguserform10');
	}

	public function dataevent()
	{
		$result=$this->model->dataevent();
	}

	public function save()
	{
		try{
			if(!$this->isFilter())
				throw new Exception("Required field is empty or Mobile number should be equal to 10 characters.", 1);

			$postedMobile = $this->input->post('mobilenumber');
			$firsttwochar =substr($postedMobile,0,2);

			if( $firsttwochar != '98')
				throw new Exception('Mobile Number must start with 98');

			$result =$this->model->save();
			if($result){
				$type="success";
				$message="Record Saved Successfully";
			}else{
				$type="error";
				$message="Error in processing Request";
			}
			
		}catch(Exception $e){
			$type="error";
			$message=$e->getMessage();
		}

		echo json_encode(array('type'=>$type,'message'=>$message));

		
	}

	function isFilter(){
    	$this->form_validation->set_rules('name', 'Full Name', 'required');
    	$this->form_validation->set_rules('mobilenumber', 'Mobile Number', 'required|min_length[10]|max_length[10]');
    	$this->form_validation->set_rules('subject', 'Subject', 'required');
    	$this->form_validation->set_rules('district_id', 'District', 'required');
    	$this->form_validation->set_rules('address', 'Address', 'required');
    	return $this->form_validation->run();
    }

	
	
}