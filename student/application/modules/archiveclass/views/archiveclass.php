<style>
	video {
		width: 100%    !important;
		height: auto   !important;
	}
	h5{margin: 0px 0px 5px 0px !important;}
	#arc-vdo{padding: 5px;}
	.vdo-caption h4,.vdo-caption h5{color: #365899 !important;padding: 0px 5px;}
	.vdo-caption h5{color: black !important;padding: 0px 5px;}
	.vdo-thumbs{padding: 5px;border-bottom: 1px solid rgba(245, 243, 243, 0.78);}
	.wrapthumbs-list.light-bg{
		background: rgba(250, 252, 255, 0.81);
		border: 1px solid rgba(221, 221, 221, 0.35);
	}
	.wrapthumbs-list{
		overflow-y: auto;
	}
	.wrapthumbs-list::-webkit-scrollbar {
		width: 6px;
	}
	.wrapthumbs-list::-webkit-scrollbar-track {
		background:transparent;
		border-radius: 20px;
	}
	.wrapthumbs-list::-webkit-scrollbar-thumb {
		background-color:#909090;
		border-radius: 20px;
	}
	.vdo-thumbs a h5{
		color: #365899 !important;
	}
	.vdo-thumbs.highlight{
		background: #939dcb1f;
	}
	.vdo-thumbs.highlight .thumnail-img {   
		border: 1px solid #e7e7e7;
	}
	.rowvdos .pad5{
		padding: 5px;
	}
	.secvdo-list h5.bread.vdo-title{
		margin-top: 20px !important;
		margin-bottom: 10px !important;
	}
	.secvdo-list .vdo-caption h5{
		font-weight: bold;
		margin-top: 10px !important;
		color: black !important;
	}
</style>



<div id="arc-vdo" class="col-md-12 wrapper">
	<div class=" col-lg-8 col-sm-8 col-md-8 col-xs-12 pad-fix">
		<div class="play-vdo">
			<?php if(!@$data['hidebread']){?>
			<h5 class="bread vdo-title">

				<a href="javascript:;" data-classid="<?php  echo $data['classid']; ?>" class="classbtn breadcrumb">Grade <?php echo $data['classname']; ?><input type="hidden" id="homework_classid" value="<?php echo $data['classid']; ?>"></a> <i class="fa fa-chevron-right"></i>
				<a href="javascript:;" class="usersubjectbtn breadcrumb" data-subjectname="<?= $data['subjectname']?>" data-subjectid="<?= $data['subjectid']?>" data-classname="<?php echo isset($classname)?$classname:$data['classname']?>"><?php echo isset($data['subjectname'])?explode('-',$data['subjectname'])[0]:''?></a> <i class="fa fa-chevron-right"></i>
				<span class="active breadcrumb"> <?php echo  $data['chaptername']?></span>
			</h5>
			<?php } ?>
			<?php 
			if($data['source'] == 'selfserver'){
				$rel ='';
				if(@$data['vsourcetype']=='data')
				// if(false)
				{
					$patharr = explode('*#*', @$data['videodata'])				
					?>
					<iframe id="myframe" src="https://stream.midas.com.np:8443/myplayer/index.html?subject=<?= $patharr[0]?>&amp;chapter=<?=$patharr[1]?>" allowfullscreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen"  msallowfullscreen="msallowfullscreen" oallowfullscreen="oallowfullscreen" webkitallowfullscreen="webkitallowfullscreen" style="max-height: 768px;width: 100%;min-height: 566px;border: none;"></iframe>
					<?php }
					else
						{?>

					<video id="archivevideo<?=$data['slidescontentid']?>" class="archivevideo" controls controlsList="nodownload" poster="<?= $data['thumb']?$data['thumb']:'assets/images/img_placeholder.jpg'?>" data-slidescontentid="<?=$data['slidescontentid']?>">
						<source src="<?php echo $data['link'];?>" type="video/mp4">
							<source src="<?php echo $data['link'];?>" type="video/ogg">
								Your browser does not support the video tag.
							</video>
							<?php }?>
							<?php
						}else if($data['source'] == 'youtube' || $data['source'] == 'vimeo'){
							if($data['source'] == 'youtube')
								$rel = '?rel=0';
							$embedURL = getEmbedUrl($data['link']);
							?>
							<div class="container wrapytv">
								<iframe class="video" src="<?php echo $embedURL.$rel; ?>"></iframe>
							</div>
							<?php
						}else if($data['source'] == 'flash'){
							?>
							<object classid="" style="width:100%" height="400" id="movie_name"><param name="movie" value="<?php echo $data['link']?>" /><object type="application/x-shockwave-flash" data="<?php echo $data['link'];?>" style="width:100%" height="400"><param name="movie" value="<?php echo $data['link']?>"/></object></object>
							<?php
						}
						?>
						<div class="vdo-caption">

							<div class="pull-left"><h4><?php echo $data['title']?></h5></div>

							<div class="clearfix"></div>
							<div class="shor-descr">
								<p><?php echo $data['description']?></p>
							</div>

						</div>
					</div>
					<div class="clearfix"></div>


				</div>
				<div class="col-lg-4 col-md-4 col-xs-12 col-sm-4 pad-fix">
					<div class="next-vdo">
						<div class="col-xs-12">
							<h5 class="bread vdo-title"> Videos List	</h5>
							<div class="wrapthumbs-list light-bg" data-ajaxready="true">							
								<?php
								$videos = $data['tutorialvideos'];
								foreach ($videos as $key=> $row) {
									$video_thumb_class = '';
									if($row['slidescontentid'] == $data['slidescontentid']){
										$video_thumb_class = 'highlight';

									}
									if($row['thumb']){
										$imgsrc = $row['thumb'];
									}else{
										$imgsrc = 'assets/images/img_placeholder.jpg';
									}
									?>
									<div class="vdo-thumbs <?= $video_thumb_class?>">
										<div class="col-xs-6 pad-fix">
											<div class="thumnail-img">
												<a href="javascript:;" data-index="<?= $key ?>" id="others_videos" data-slidecontentid = "<?php echo $row['slidescontentid']?>" data-thumb="<?php echo $row['thumb'] ?>" data-link="<?php echo $row['link']; ?>" data-duration="<?= $row['duration']?>" data-source="<?php echo $row['source']; ?>" data-publisheddate="<?php echo $row['publisheddate']?>" data-title="<?php echo $row['title']?>" data-description="<?php echo $row['description']?>" class="video_link <?= @$data['classname']?>"  data-vsourcetype="<?= @$row['vsourcetype']?>" data-videodata="<?= @$row['videodata']?>">
													<img class="archivethumb" src="<?php echo $imgsrc; ?>" />	
												</a>
												<div class="time-div">
													<p class="time-length"><?php
														$duration = explode(':',$row['duration']);
														if($duration[0]=='00'){
															echo $duration[1].':'.$duration[2];
														}else{
															echo $row['duration']?$row['duration']:'N/A';
														}

														?></p>
													</div>
													<div class="view-div">
														<p><i class="fa fa-eye"></i> <?= $row['views']?></p>	
													</div>
												</div>

											</div>

											<div class="vdo-caption col-xs-6 ">
												<a href="javascript:;"  data-index="<?= $key ?>" id="others_videos" data-slidecontentid = "<?php echo $row['slidescontentid']?>" data-duration="<?= $row['duration']?>" data-thumb="<?php echo $row['thumb'] ?>" data-publisheddate="<?php echo $row['publisheddate']?>" data-link="<?php echo $row['link']; ?>" data-source="<?php echo $row['source']; ?>" data-title="<?php echo $row['title']?>" class="video_link <?= @$data['classname']?>"  data-vsourcetype="<?= @$row['vsourcetype']?>" data-videodata="<?= @$row['videodata']?>">
													<h5>
														<?php echo $row['title'];?>
													</h5> 
												</a>

											</div> 
											<div class="clearfix"></div>
										</div>
										<?php
									// }
									}
									?>

								</div>

							</div>

						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<?php 
				if(isset($data['data']['response'])) { ?>
				<div id="" class="col-md-12 wrapper live-QnA pad5">
					<div class=" col-lg-8 col-sm-8 col-md-8 col-xs-12 pad-fix">
						<div class="title-QnA">
							<h5 class=" bread" style="padding: 4px 0px;">
								<p class="breadcrumb pull-left">Question &amp; Answers</p>
								<div class="pull-right">
									<a class="btn btn-info inline livequesbtns archivepostques" id="" data-toggle="collapse" data-target="#Qeditor" style="display: none;">Post a new question</a>
								</div>
								<div class="clearfix"></div>
							</h5>


							<div id="Qeditor" class="pad5 collapse">
								<div class="qanda" id="home-workk">
									<?php
									$this->load->view('forum/forum_newpost');
									?>
								</div>
							</div>
						</div>
						<div style="margin-left: 19px; color:#895f5f; font-size: 16px; margin-top: 16px; ">If you have any question to ask, please go to "Homework Help" tab and Post your Question.</div>
						<div class=" col-sm-12 col-xs-12 QnAs-div">
							<div class="archive-forum posted-questions-div">
								<?php
								$this->load->vars(array('response'=>$data['data']['response'],'message'=>$data['data']['message']));
								$this->load->view('forum/forum_posts')?>
							</div>
							<div class="cust-loader hidden">
								<img src="<?php echo base_url();?>assets/images/loader.gif" class="img-responsive" style="margin:auto;">
							</div>	
						</div>	
					</div>		
					<div class="col-md-4 pad-fix live-sidebar">
						<?php
						$this->load->view('templates/sidebar_new')?>
					</div>
					<div class="clearfix"></div>
				</div>
				<?php } ?>

				<script>
					$(document).ready(function() {
						var divHeight = $('.play-vdo').height()-41;	
						$('.wrapthumbs-list').css('height', divHeight+'px');
					});
				</script>
				<div class="modal fade" id="action-data" role="dialog">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title"></h4>
							</div>
							<div class="modal-body views-wrapper">

							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>

				<div class="modal fade" id="modal-edit-comment-text" role="dialog" data-backdrop="static" data-keyboard="false">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close close-edit-answer" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">Edit your Comment</h4>
							</div>

							<div class="modal-body">
								<input type="hidden" name="" id="edit_comment_id" class="edit_comment_id">
								<input type="hidden" name="" id="edit_comment_question_id" class="edit_comment_question_id">
								<textarea id="txtEditedComment" class="txtEditedComment" placeholder="Write a comment.." style="width:100%"></textarea>
							</div>
							<div class="modal-footer">
								<span id="edit_comment_error" class="error edit_comment_error" style="float:left;"></span>
								<button type="button" class="btn btn-default close-edit-answer" data-dismiss="modal">Close</button>
								<button style="margin-top: -3px; background: #4266b2;" type="button" id="btn_update_comment" class="btn btn-success btn_update_comment">Post</button>
							</div>
						</div>
					</div>
				</div>


				<!-- Question Delete -->
				<div id="dialog" title="Confirmation Required" style="display: none">
					Are you sure, you want delete this question?
				</div>


				<!-- Question Text Edit -->
				<div class="modal fade" id="ckeditor-modal-edit-question" role="dialog" data-backdrop="static" data-keyboard="false">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close close-edit-question" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">Edit your Question</h4>
							</div>

							<div class="modal-body">
								<input type="hidden" name="" id="edit_question_id" class="edit_question_id">
								<textarea id="txtEditedQuestion" class="txtEditedQuestion"></textarea>

								<div id="image_edit_wrap" class="image_edit_wrap" >
									<div>
										<img src="" alt="" id="img_edited_question" class="img-responsive img_edited_question"><br>
										<span class="btn btn-info remove_btn" id="btn_remove_img_edited_question">Remove</span>
									</div>
									<div id="image_input">
										<label>Upload Files</label>
										<input type="file" name="" id="img_input_edited_question" accept="image/*">

									</div>
								</div>
							</div>
							<div class="modal-footer">
								<span id="edit_question_error" class="error edit_question_error"></span>
								<button type="button" class="btn btn-default close-edit-question" data-dismiss="modal">Close</button>
								<button style="margin-top: -3px; background: #4266b2;" type="button" id="btn_update_question" class="btn btn-success btn_update_question">Post</button>
							</div>
						</div>
					</div>
				</div>

				<!-- Question Image Edit -->
				<div class="modal fade" id="ckeditor-modal-edit-question-image" role="dialog" data-backdrop="static" data-keyboard="false">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close close-edit-question" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">Edit your Question</h4>
							</div>

							<div class="modal-body">
								<input type="hidden" name="" id="edit_question_id">
								<div>
									<img src="" alt="" id="img_edited_question" class="img-responsive img_edited_question"><br>
									<span class="btn btn-info remove_btn" id="btn_remove_img_edited_question">Remove</span>
								</div>
								<div id="image_input">
									<label>Upload Files</label>
									<input type="file" name="" id="img_input_edited_question" accept="image/*">

								</div>
							</div>
							<div class="modal-footer">
								<span id="edit_question_error" class="error edit_question_error"></span>
								<button type="button" class="btn btn-default close-edit-question" data-dismiss="modal">Close</button>
								<button style="margin-top: -3px; background: #4266b2;" type="button" id="btn_update_question" class="btn btn-success btn_update_question btn_update_question_image">Post</button>
							</div>
						</div>
					</div>
				</div>

				<script>
				//window.addEventListener('scroll', checkScroll, false);
				$(document).ready(function(){
					pramukhIME.addKeyboard("PramukhIndic");
					pramukhIME.enable(); 	
				});

				// $('.collapse').on('shown.bs.collapse', function(){
				// 	$(this).parent().find(".arrow-down").removeClass("arrow-down").addClass("arrow-up");
				// }).on('hidden.bs.collapse', function(){
				// 	$(this).parent().find(".arrow-up").removeClass("arrow-up").addClass("arrow-down");
				// });
				var videoduration;
				var video_interval;
				$('video.archivevideo').on('play',function(){
					videoduration=0;
					video_interval = window.setInterval(function() {
						videoduration += 1000;   
						console.log(videoduration);  
					}, 1000);


				});

				$('video.archivevideo').on('pause',function(){
					console.log('pause')
					var duration = $(this);
					var slidescontentid=$(this).data('slidescontentid');
					var audio = document.getElementById("archivevideo"+slidescontentid);

					if(audio.readyState > 0) 
					{
						var videolength=audio.duration * 1000;
						var type='archive';
						var infoData ={videoid:slidescontentid,type:type,viewlength:videoduration,videolength:videolength};
						$.when(trackviewedvideo(infoData)).then(function(rdata){
							clearInterval(video_interval);
							videoduration=0;
						});

					}

				});

				$('video.archivevideo').one('play', function () {
					console.log('archivevideo');
					var slidescontentid=$(this).data('slidescontentid');
					var trackdata = {event:'video',videoid:slidescontentid};
					trackLiveClass(trackdata);
					return false;
				}); 
			</script>
