<input type="hidden" name="nextpage" id="nextpage" value="<?= $data['nextpage']?>">

<?php foreach ($tutorialvideos as $key =>$row) { 

	$class = '';
	if($key==0)
		$class = 'active';

	if($key % 4 == 0){
		?>
		<div class="item <?= $class?>">

			<div class="row-fluid">
			<?php }
			?>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" style="margin-bottom: 15px;">
					<div class="wrapvthumnail">
						<div class="thumnail-img">

							<?php
							if ( $row[ 'thumb' ] ) {
								$img = '<img  src="'.$row['thumb'].'" />';
							}else{
								$img = '<img  src="assets/images/img_placeholder.jpg" />';
							}
							?>
							<a href="javascript:;" id="others_videos" class="video_link others_videos "  data-index="<?= $key ?>"><?php echo $img; ?>


								<div class="time-div">
									<p class="time-length"><?php 
										$duration = explode(':',$row['duration']);
										if($duration[0]=='00'){
											echo $duration[1].':'.$duration[2];
										}else{
											echo $row['duration']?$row['duration']:'N/A';
										}
										?>
									</p>
								</div>
								<div class="view-div">
													<p><i class="fa fa-eye"></i> <?= $row['views']?></p>	
													</div>
							</a>
						</div>
						<a href="javascript:;" id="others_videos" data-slidecontentid = "<?php echo $row['slidescontentid']?>" data-link="<?php echo $row['link']; ?>" data-duration="<?= $row['duration']?>" data-thumb="<?php echo $row['thumb'] ?>" data-source="<?php echo $row['source']; ?>" data-publisheddate="<?php echo $row['publisheddate']?>" data-description="<?php echo $row['description']?>" data-title="<?php echo $row['title']?>" class="video_link others_videos " data-vsourcetype="<?= @$row['vsourcetype']?>" data-videodata="<?= @$row['videodata']?>">
						</a>
						<div class="wrap-vcap">
							<a href="javascript:;" id="others_videos" data-slidecontentid = "<?php echo $row['slidescontentid']?>" data-link="<?php echo $row['link']; ?>" data-duration="<?= $row['duration']?>" data-thumb="<?php echo $row['thumb'] ?>" data-source="<?php echo $row['source']; ?>" data-publisheddate="<?php echo $row['publisheddate']?>" data-description="<?php echo $row['description']?>" data-title="<?php echo $row['title']?>" class="video_link others_videos " data-vsourcetype="<?= @$row['vsourcetype']?>" data-videodata="<?= @$row['videodata']?>">
								<h5 class="vid_tn_tit"><?php echo $row['title']; ?></h5>

							</a>
						</div>

					</div>
				</div>


<?php
if(($key+1) % 4 == 0 || $key+1==$data['total']){ ?>
				<div class="clearfix"></div>

			</div><!--/row-fluid-->


		</div><!--/item-->
		<?php }?>
		<?php 
	}

	?>
	

		<a class="left carousel-control" href="#vdoCarousel" data-slide="prev" style="display:none;"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i></a>
	<?php
	if(($data['total'])>4){
		?>
		<!-- <a class="right carousel-control" href="#vdoCarousel" data-slide="next" data-nextpage="<?php echo $data['nextpage'];?>" onclick="displayTutorialvideos(<?=$data['chapterid']?>,<?=$data['subjectid']?>,<?=$data['shownext']?>,'LC',<?=$data['nextpage']?>)"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i></a> -->
		<a class="right carousel-control" href="#vdoCarousel" data-slide="next" data-nextpage="<?php echo $data['nextpage'];?>"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i></a>
		<?php
	}
	?>





	<script type="text/javascript">
	
		$('.left').on('click',function(){
			$('.carousel').carousel("prev"); 
		});


		$('.right').on('click',function(){
			$('.left').css('display','block');
		});
		$('input.blank-answer').eq('0').focus();


	</script>



