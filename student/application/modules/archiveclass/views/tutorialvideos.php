<style>
	.purple_bg {
		background: #c8c8fe;
	}
	
	.more_vids_tit {
		background: #eeeeee;
		padding: 10px;
		color: #565656;
		font-weight: bold;
		font-size: 14px;
	}
	
	h5.vid_tn_tit {
		color: #3b589a;
		font-weight: bold;
	}
	
	h5.bread {
		background: #eef1f6;
		font-weight: bold;
		padding: 10px 5px;
		margin: 2px 5px;
		color: #418755 !important;
	}
	
	.bread_wrap {
		border-bottom: 1px solid #dfe4e8 !important;
	}
	span.views:after {
		content: '•';
		margin: 0 4px;
	}
</style>
<!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pad-fix bread_wrap">
	<h5 class="bread">

		<a href="javascript:;" data-classid="<?php  echo $data['classid']; ?>" class="classbtn breadcrumb">Grade <?php echo $data['classname']; ?><input type="hidden" id="homework_classid" value="<?php echo $data['classid']; ?>"></a> <i class="fa fa-chevron-right"></i>
		<a href="javascript:;" class="usersubjectbtn breadcrumb" data-subjectname="<?= $data['subjectname']?>" data-subjectid="<?= $data['subjectid']?>" data-classname="<?php echo isset($classname)?$classname:$data['classname']?>"><?php echo isset($data['subjectname'])?explode('-',$data['subjectname'])[0]:''?></a> <i class="fa fa-chevron-right"></i>
		<span class="active breadcrumb"> <?php echo  $data['chaptername']?></span>
		<a href="javascript:;" data-classid="<?php  echo $data['classid']; ?>" class="pull-right mini-home-btn classbtn breadcrumb"><img src="<?= base_url()?>assets/images/home-btn.png" class="homepng"> </a>
	</h5>


</div> -->
<?php // $this->load->view('templates/qe_breadcrum_view'); ?>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0;">
	<?php
	if(empty($tutorialvideos) && ($data['data']['type'] =='error')){
		
		?>
		<div class="col-xs-12">
			<div class="img-thumbnail col-lg-12 archive-error">
				<?php echo $data['message']; ?>
			</div>
		</div>
		<?php
	} else {
		//if(!empty($tutorialvideos)){
		?>

		<!-- <div class="col-md-12 vdoslider pad-fix">
		<div id="vdoCarousel" class="carousel slide">
					<?php 
					// $totalrecs = $data['totalrecs'];
					// $totalcarousel = ceil($totalrecs/4);
					// for ($i=0; $i < $totalcarousel ; $i++) { 
						?>

						<li data-target="#vdoCarousel" data-slide-to="<?php echo $i?>" onclick="displayTutorialvideos(<?=$data['chapterid']?>,<?=$data['subjectid']?>,<?=$i?>,'LC',<?=$data['nextpage']?>)"></li>
						<?php 
				//	}
					?>

				<div class="carousel-inner">
			<?php //$this->load->view('tutorialslider'); ?>
			</div>
					
				</div>
		</div> -->
		<?php
	//	}

		?>
		
		<div class="clearfix"></div>

		<?php
		if($data['data']['type'] !='error'){
		?>
		 <!-- live-QnA -->
		<div id="" class="col-md-12 pad-fix wrapper">
			<div class=" col-lg-12 col-sm-12 col-md-12 col-xs-12 pad-fix give-border">
				<div class="title-QnA">
					<!-- <h5 class=" bread" style="padding: 4px 0px;">
						<p class="breadcrumb pull-left">Question & Answers</p>
						<div class="pull-right">
							<a class="btn btn-info inline livequesbtns archivepostques" id="" data-toggle="collapse" data-target="#Qeditor" style="display:none">Post a new question</a>
						</div>
						<div class="clearfix"></div>
					</h5> -->
					<div id="Qeditor" class="pad5 collapse">
						<div class="qanda" id="home-workk">
							<?php
							$this->load->view('forum/forum_newpost');
							?>
						</div>
					</div>
				</div>
				<!-- <div style="margin-left: 19px; color:#895f5f; font-size: 16px; margin-top: 16px; ">If you have any question to ask, please go to "Homework Help" tab and Post your Question.</div> -->
				<div class=" col-sm-12 col-xs-12 QnAs-div">
				<!-- posted-questions-div -->
					<div class="archive-forum">
					    <span style="margin-left: 32%;font-size: 14px;" id="tquestioncount"></span>
						<?php
						$this->load->vars(array('response'=>$data['data']['response'],'message'=>$data['data']['message'],'source'=>'LC'));
						$this->load->view('forum/forum_posts')?>
					</div>
					<div class="cust-loader hidden">
						<img src="<?php echo base_url();?>assets/images/loader.gif" class="img-responsive" style="margin:auto;">
					</div>	
				</div>	
			</div>	
			<!-- <div class="col-md-3 pad-fix live-sidebar">
				<?php
				//$this->load->view('templates/sidebar_new')?>
			</div> -->

		</div>	
		<?php
		}
		?>

		<div class="clearfix"></div>
		<?php } ?>
	</div>
	<style>
		#vdoCarousel{
			margin-top: 15px;
		}
		#vdoCarousel i.fa-chevron-circle-left, #vdoCarousel i.fa-chevron-circle-right {
			position: absolute;
			top:45%;
			left: 8px;
		}
		/* Removes the default 20px margin and creates some padding space for the indicators and controls */
		.vdoslider .carousel {
			margin-bottom: 0;
			padding: 0 10px 15px 10px;
		}
		/* Reposition the controls slightly */
		.carousel-control {
			left: -12px;
		}
		.vdoslider .carousel-control.right {
			right: 0px;
		}
		/* Changes the position of the indicators */
		.vdoslider .carousel-indicators {
			right: 50%;
			top: auto;
			bottom: 0px;
			margin-right: -19px;
		}
		/* Changes the colour of the indicators */
		.vdoslider .carousel-indicators li {
			background: #c0c0c0;
		}
		.vdoslider .carousel-indicators .active {
			background: #333333;
		}
		.carousel-control {
			position: absolute;
			top: 0;
			bottom: 0;
			left: 0;
			width: 3%;
			font-size: 20px;
			color: #fff;
			text-align: center;
			text-shadow: 0 1px 2px rgba(0,0,0,.6);
			background-color: grey;
			filter: alpha(opacity=50);
			opacity: .5;
		}
		.vdoslider  .carousel-control.left, .vdoslider .carousel-control.right {
			/* background-image: -webkit-linear-gradient(left,rgba(0,0,0,.5) 0,rgba(0,0,0,.0001) 100%); */
			background-image:unset;
			/* background-image: -webkit-gradient(linear,left top,right top,from(rgba(0,0,0,.5)),to(rgba(0,0,0,.0001))); */
			/* background-image: linear-gradient(to right,rgba(0,0,0,.5) 0,rgba(0,0,0,.0001) 100%); */
			filter: unset;
			/* background-repeat: repeat-x; */
		}

	</style>
	<script>
		// $(document).ready(function() {
		// 	$('#vdoCarousel').carousel({
		// 		interval: 10000
		// 	})
		// });

		$(document).ready(function(){
			// pramukhIME.addKeyboard("PramukhIndic");
			// pramukhIME.enable(); 	
		});

		// $('.collapse').on('shown.bs.collapse', function(){
		// 	$(this).parent().find(".arrow-down").removeClass("arrow-down").addClass("arrow-up");
		// }).on('hidden.bs.collapse', function(){
		// 	$(this).parent().find(".arrow-up").removeClass("arrow-up").addClass("arrow-down");
		// });

	</script>
