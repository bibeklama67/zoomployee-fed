<style>
	.purple_bg {
		background: #c8c8fe;
	}
	
	.more_vids_tit {
		background: #eeeeee;
		padding: 10px;
		color: #565656;
		font-weight: bold;
		font-size: 14px;
	}
	
	h5.vid_tn_tit {
		color: #3b589a;
		font-weight: bold;
	}
	
	h5.bread {
		background: #eef1f6;
		font-weight: bold;
		padding: 10px 5px;
		margin: 2px 5px;
		color: #418755 !important;
	}
	
	.bread_wrap {
		border-bottom: 1px solid #dfe4e8 !important;
	}
	span.views:after {
		content: '•';
		margin: 0 4px;
	}
</style>


<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0;">
	<?php
	if(empty($videos) && ($data['data']['type'] =='error')){
		
		?>
		<div class="col-xs-12">
			<div class="img-thumbnail col-lg-12 archive-error">
				<?php echo $data['message']; ?>
			</div>
		</div>
		<?php
	} else {
		?>

		
		<div class="clearfix"></div>

		<?php
		if($data['data']['type'] !='error'){
		?>
		<input type="hidden" id="referencepagination" value="1" />
		 <!-- live-QnA -->
		 
		<div id="" class="col-md-12 pad-fix wrapper">
			<div class=" col-lg-12 col-sm-12 col-md-12 col-xs-12 pad-fix give-border">
				
				<div class=" col-sm-12 col-xs-12 QnAs-div">
				<!-- posted-questions-div -->
					<div class="archive-forum">
					<span style="margin-left: 32%;font-size: 14px;" id="tquestioncount"></span>

						<?php
						$this->load->vars(array('response'=>$data['data']['response'],'message'=>$data['data']['message'],'source'=>'RV'));
						$this->load->view('forum/forum_posts')?>
					</div>
					<div class="cust-loader hidden">
						<img src="<?php echo base_url();?>assets/images/loader.gif" class="img-responsive" style="margin:auto;">
					</div>	
				</div>	
			</div>	
		
		</div>	
		<?php
		}
		?>

		<div class="clearfix"></div>
		<?php } ?>
	</div>

	<script>
	
		$(document).ready(function(){
			// pramukhIME.addKeyboard("PramukhIndic");
			// pramukhIME.enable(); 	
		});



	</script>
