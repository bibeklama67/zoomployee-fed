<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Archiveclass extends Front_controller {

	function __construct()
	{
		parent::__construct();
		$d=$this->config->item('default');
		$df=$this->config->item($d); 
		$df['js'][]='common/student_api';
		$df['js'][]='teachers_online/teachers_online';
		$df['js'][]='ckeditor/ckeditor';
		// $df['js'][]='liveclass/liveclass';

		$this->config->set_item($d,$df);
		$this->load->helper('elearning_helper');
		$this->simsdb =  $this->load->database('sims',TRUE);


	}

	function listTutorialvideos()
	{
		$data = $this->input->post();
  // print_r($data);
  // exit;
		$response = $data['response'];
		$this->session->set_userdata('tutorialActivity', 'vt');
		$this->load->vars(array(
			'classid'=>$data['classid'],
			'classname'=>$data['classname'],
			'subjectid'=>$data['subjectid'],
			'subjectname'=>$data['subjectname'],
			'chapterid'=>$data['chapterid'],
			'chaptername'=>$data['chaptername'],
			'tutorialvideos'    =>    $response,
			'total'             =>    $data['total'],
			'data' => $data
			));
		$this->load->view('tutorialvideos');
		// redirect('archiveclass/listTutorialvideos');
	}
	function listReferencevideos()
	{
		$data = $this->input->post();
  // print_r($data);
  // exit;
		$response = $data['response'];
		$this->session->set_userdata('tutorialActivity', 'vt');
		$this->load->vars(array(
			'classid'=>$data['classid'],
			'classname'=>$data['classname'],
			'subjectid'=>$data['subjectid'],
			'subjectname'=>$data['subjectname'],
			'chapterid'=>$data['chapterid'],
			'chaptername'=>$data['chaptername'],
			'videos'    =>    $response,
			'total'             =>    $data['total'],
			'data' => $data,
			));
		$this->load->view('referencevideos');
		// redirect('archiveclass/listTutorialvideos');
	}

	function tutorialslider(){
		$data = $this->input->post();

		$response = $data['response'];
		$this->session->set_userdata('tutorialActivity', 'vt');
		$this->load->vars(array(
			'tutorialvideos'    =>    $response,
			'total'             =>    $data['total'],
			'data' => $data
			));
		$this->load->view('tutorialslider');
	}



	function archive(){
		$data = $this->input->post();
		if($data['tutorialvideos'])
		{
			$data['publisheddate'] = $data['tutorialvideos'][$data['index']]['publisheddate'];
			$data['duration'] = $data['tutorialvideos'][$data['index']]['duration'];
			$data['link'] = $data['tutorialvideos'][$data['index']]['link'];
			$data['thumb'] = $data['tutorialvideos'][$data['index']]['thumb'];
			$data['description'] = $data['tutorialvideos'][$data['index']]['description'];
			$data['source'] = $data['tutorialvideos'][$data['index']]['source'];
			$data['title'] = $data['tutorialvideos'][$data['index']]['title'];
			$data['slidescontentid'] = $data['tutorialvideos'][$data['index']]['slidescontentid'];
			$data['vsourcetype'] = $data['tutorialvideos'][$data['index']]['vsourcetype'];
			$data['videodata'] = $data['tutorialvideos'][$data['index']]['videodata'];			
		}
		$data['classname'] = ($data['classname']?$data['classname']:'other-videos');
		// print_r($data['title']);
		$this->load->vars(array(

			'data'         =>    $data
			));
		$this->load->view('archiveclass'); 
	}
}
