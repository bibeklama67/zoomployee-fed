<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Livechat extends MX_Controller {

  function __construct()
  {
    parent::__construct();
    $d=$this->config->item('default');
    $df=$this->config->item($d); 
    $df['js'][]='login/login1';

   

    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    $this->config->set_item($d,$df);
    $this->load->helper('image_helper');
    $IParray=array_values(array_filter(explode(',',$_SERVER['HTTP_X_FORWARDED_FOR'])));
    @$this->ipaddress = end($IParray);
  }
  
  public function index()
  {
    if(!$_POST)
    {
      $data['hide']=false;
    }
    else
    {
      $data['post']=$_POST;
      $data['hide']=true;
    }
      $this->load->view('chat.php',$data);
  }
}

