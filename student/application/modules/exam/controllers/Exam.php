<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Exam extends Front_controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('model_exam','model');
		$this->load->helper('form','url');
		$this->load->library('form_validation','uri');
		
		//ini_set('display_errors',0);
		//ini_set('display_startup_errors',0);
		//error_reporting(E_ALL);
		
	  $this->classid   = $this->session->userdata('eclassid');
	  $this->studentid = $this->session->userdata('studentid');
	}
    
	public function index()
	{ 

		$cond = array('classid'=>$this->classid,'studentid'=>$this->studentid);  
	    $data = $this->model->currently_active_exam($cond);
		
	  $this->load->vars(array(
		'formview'          =>    '/exam/exam_content',
		'subview'        	=>    'exam',
		'source'			=>	  'Exam',
		'mydata' 			=>    $data
	
		));
		
	  $this->render_page();   
	}
	
	//This function controls the notification for student side
	function get_current_exam(){
		
		$sdata = $this->db->query("select istemp,status,ayearid from sch_studentcurrent where studentid='$this->studentid'")->result();		
		$sdata = element('0',$sdata);
		$cond = array('classid'=>$this->classid,'studentid'=>$this->studentid);
	    $data = $this->model->currently_active_exam($cond);
		$response = element('0',$data->response);
		if($response && isset($response->totlrecs) && $response->totlrecs > 0)
		{
			if($sdata->istemp=='N' && $sdata->status=='C' && $sdata->ayearid=='20')
				echo json_encode(array("show_exam"=>'yes','studentid'=>$this->studentid));
			else
				echo json_encode(array("show_exam"=>'err','studentid'=>$this->studentid));
		}else
			echo json_encode(array("show_exam"=>'no','studentid'=>$this->studentid));
	}
	
	function get_current_exam_details(){
		
		$sdata = $this->db->query("select istemp,status,ayearid from sch_studentcurrent where studentid='$this->studentid'")->result();		
		$sdata = element('0',$sdata);
		
		$subjectid=$this->input->post('subjectid');

		$cond = array('classid'=>$this->classid,'studentid'=>$this->studentid,'subjectid'=>$subjectid);
		
	    $data = $this->model->currently_active_exam($cond);
		
		$response = element('0',$data->response);
		
		if($response && isset($response->totlrecs) && $response->totlrecs > 0)
		{
			if($sdata->istemp=='N' && $sdata->status=='C' && $sdata->ayearid=='20')
				$this->load->view("/exam/exam_details",array('data'=>$data));
		}
	}
	
	function start_exam($exam_id=false)
	{
  
	  if(!$exam_id)
		$exam_id = $this->input->post('exam_id');
	
	  if(!$exam_id)
		  $exam_id = $this->session->userdata('exam_id');
	  
	  if($exam_id)
	  {
		$this->session->set_userdata('exam_id',$exam_id);
		$data = $this->model->get_exam_info(array('exam_id'=>$exam_id,'studentid'=>$this->studentid));
		$end_time = element('0',json_decode(element('0',$data->response)->scheduleinfo))->end_datetime;
		
		if(strtotime($end_time) < time())
			 { $data = 'expired'; $eg_info='expired'; $cur_progress='expired'; } 
		else {
				$this->session->set_userdata('exam_end_time',$end_time);
				$eg_info = $this->model->get_exam_groupinfo(array('exam_id'=>$exam_id,'studentid'=>$this->studentid));
				$cur_progress = $this->model->get_current_progress(array('exam_id'=>$exam_id,'studentid'=>$this->studentid));	
			}
	  }else{
		$data = false;
		$eg_info = false;
		$cur_progress = false;
	  }
	  	
	  $this->load->vars(array(
		'formview'          =>   '/exam/exam_content',
		'subview'        	=>      'qgroups',
		'source'			=>	 'Exam',
		'mydata' 			=>   $data,
		'eg_info'          	=> 	 $eg_info,
		'cur_progress'      =>   $cur_progress
		));
		
		$this->render_page();   
		
	}
	
	function get_next_question(){
		
		$end_time = $this->session->userdata('exam_end_time');
		if(strtotime($end_time) < time())
			 { 
				echo "<h3 style='color:red;margin:30px'>Sorry !! Your Exam Time Is Over. </h3>";
				die;
			 } 
			 
		$exam_id = $this->input->post('exam_id');
		$exam_groupid = $this->input->post('exam_groupid');
		
		if(!$exam_id)
			$exam_id = $this->session->userdata('exam_id');
		if(!$exam_groupid)
			$exam_groupid = $this->session->userdata('current_examgroupid');
		
		
		if(!$exam_id || !$this->studentid || !$exam_groupid)
			throw new Exception("Required Parameters Missing");
		
		$this->session->set_userdata('exam_groupid',$exam_groupid);
		
		$q_info = $this->model->get_next_question(array('exam_id'=>$exam_id,
														'exam_groupid'=>$exam_groupid,
														'studentid'=>$this->studentid));
		$this->load->view('/exam/questions',$q_info);
	}
	
	function save_and_next()
	{
		$end_time = $this->session->userdata('exam_end_time');
		if(strtotime($end_time) < time())
		{ 
			echo "<h3 style='color:red;margin:30px'>Sorry !! Your Exam Time Is Over. </h3>";
			die;
		}
		
		$postdata = $this->input->post();
		if(!element('exam_qid',$postdata))
		{
			echo "Sorry No Exam Record Provided. Please Refresh and try again";
			return;
		}
		
		$ans_text = element('ans_text',$postdata);
		if(is_array($ans_text))
			$postdata['ans_text']=implode('#',$ans_text);
		
		
		$postdata['studentid'] = $this->studentid;
		
		$data = $this->model->save_and_next($postdata);
		
		$this->load->view('/exam/questions',$data);
	}
  
}