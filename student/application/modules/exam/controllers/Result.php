<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Result extends Front_controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('model_result','model');
		$this->load->helper('form','url');
		$this->load->library('form_validation','uri');
		
		/*ini_set('display_errors',1);
		ini_set('display_startup_errors',1);
		error_reporting(E_ALL);*/
		
	  $this->classid   = $this->session->userdata('eclassid');
	  $this->studentid = $this->session->userdata('studentid');
	}
    
	public function index()
	{
		//var_dump($this->session->all_userdata());
	}
	
	function get_grade_sheet($ayearid,$examtype_id)
	{
		$cond = array('classid'=>$this->classid,'studentid'=>$this->studentid,'ayearid'=>$ayearid,'examtype_id'=>$examtype_id);
	    $data = $this->model->get_grade_sheet($cond);
		if($data->type=='success')
			$response=$data->response;
		else
			$response=false;
		
		 $this->load->vars(array(
		'formview'          =>   '/result/grade_sheet_ind',
		'subview'        	=>      'Exam',
		'source'			=>	 'qgroups',
		'data' 			=>   $response,
		));
		
		$this->render_page();  
			
	}
	
	function review_paper()
	{
		$postdata = $this->input->post();
		$data = $this->model->get_student_paper($postdata);
		$this->load->view('result/grade_paper_review',array('data'=>$data->response));
	}
}
?>