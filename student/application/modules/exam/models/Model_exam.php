<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Model_exam extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->burl = "http://myschool.midas.com.np/api/elearning/onlineexam/studentapi/";
		$this->load->library('curl');
	}	
	
	function currently_active_exam($cond)
	{
		 $url = $this->burl . 'get_current_exam'; 
		 $data = $this->curl->post_data($url,$cond);
		 $data = json_decode($data);
		 return $data;
	}
	
	function get_exam_info($cond)
	{
		 $url = $this->burl . 'get_exam_info'; 
		 $data = $this->curl->post_data($url,$cond);
		 $data = json_decode($data);
		 return $data;
	}
	
	function get_next_question($cond)
	{
		 $url = $this->burl . 'get_next_question'; 
		 $data = $this->curl->post_data($url,$cond);		 
		 $data = json_decode($data);
		 return $data;
	}
	
	function save_and_next($data)
	{
		 $url = $this->burl . 'save_and_next'; 
		 $data = $this->curl->post_data($url,$data);		 
		 $data = json_decode($data);
		 return $data;
	}
	
	function get_exam_groupinfo($cond)
	{
		$url = $this->burl . 'get_exam_groupinfo'; 
		 $data = $this->curl->post_data($url,$cond);		
		 $data = json_decode($data);
		 return $data;
	}
	
	function get_current_progress($cond)
	{
		 $url = $this->burl . 'get_current_progress'; 
		 $data = $this->curl->post_data($url,$cond);		 
		 $data = json_decode($data);
		 return $data;
	}
}
