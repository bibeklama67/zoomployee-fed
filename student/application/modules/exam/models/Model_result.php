<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Model_result extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->burl = "http://myschool.midas.com.np/api/elearning/onlineexam/report/";
		$this->load->library('curl');
	}	
	
	function get_grade_sheet($cond)
	{
		//var_dump($cond);
		 $url = $this->burl . 'get_grade_sheet'; 
		 $data = $this->curl->post_data($url,$cond);
		 $data = json_decode($data);
		 return $data;
	}
	
	
	function get_student_paper($cond)
	{
		 $url = $this->burl . 'get_student_paper'; 
		 $data = $this->curl->post_data($url,$cond);
		 $data = json_decode($data);
		 return $data;
	}
}
?>
