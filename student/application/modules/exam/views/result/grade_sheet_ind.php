<?php 
$ex   = $data->extra;
$data = $data->data;
$df   = element('0',$data);
$studentid = $this->session->userdata('studentid');
$chlds = $this->session->userdata('children');
foreach($chlds as $c)
{
	if($c['studentid'] == $studentid)
	{
		$orgname=$c['orgname'];
		$add = $c['orgaddress'];
		$dis = $c['orgdistrict'];
		$orgid = $c['orgid'];
		break;
	}
}

?>
<table id="student_list">
	<tr><th colspan="4" align="center" class="text-center"></th></tr>
	<tr>
		<td colspan="4" align="center" class="text-center orgname">
			<h1><?=$orgname?></h1>
			<h3><?=$add?>, <?=$dis?></h3>
		</td>
	</tr>
	<tr>
		<th colspan="4" class="rtitle" style="text-align:center">
			<h4 style="color:#fff !important;padding:8px 0px;margin:0px;font-size:2.2rem;"><?=$df->examtype?> Report</h4>
		</th>
	</tr>
	<tr>
		<td colspan="4" style="border:0px" id="stuinfo">
			<table style="width:100%;border:0px;">
				<tr>
					<td style="border:0px;max-width:250px">
						<span class="ttl"><strong>NAME OF THE STUDENT :</strong></span> <?=$df->studentfirstname?> <?=$df->studentlastname?> 
					</td>
				
					<td style="border:0px">
						<span class="ttl"><strong>REGD No : </strong></span> <?=$df->studentrollno?>
					</td>
				
					<td style="border:0px;text-align:right">
						<span class="ttl"><strong>CLASS : </strong></span> <?=$df->classname?> (<?=$df->section?>) 
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<th class="text-right" style="font-size:1.4rem"> SN </th>
		<th style="font-size:1.4rem"> Subject </th>
		<th class="text-left" style="font-size:1.4rem"> Obtained Grade </th>
		<th class="text-right" style="font-size:1.4rem"> Grade Point </th>
	</tr>
	<?php $result=true; $i=1; $comp=true; foreach($data as $d): 
			if($d->result_status=='f'):
	?>
		<tr> <th style="background:white;color:maroon;text-align:center" colspan="4"> Your result has been withheld. Please contact to your Class Teacher. </th> </tr>
	<?php $result=false; break; endif; ?>
		<tr>
			<td class="text-right"><?=$i++?></td>
			<td
			  <?php if(!empty($d->ate_studentid) && $d->copy_corrected=='t') : ?>
					class="review_paper" data-exam_id="<?=$d->exam_id?>" data-subjectid="<?=$d->subjectid?>" data-studentid="<?=$d->ate_studentid?>" title="Click Here to Review Answers"
			  <?php endif; ?>
			><?=$d->subjectname?></td>
			<?php if(empty($d->ate_studentid)): $comp=false; ?>
				<td colspan="2" class="text-center error"> ABSENT </td>
			<?php elseif($d->copy_corrected != 't'): $comp=false; ?>
				<td colspan="2" class="text-center error"> Copy Not Yet Checked </td>
			<?php else: ?>
			<td class="text-left"><?=$d->grade?></td>
			<td class="text-right"><?php printf("%.2f",$d->grade_point)?></td>
			<?php endif; ?>
		</tr>
	<?php endforeach; ?>
	<tr> 
		<th colspan="4" style="border:0px">
			<table style="border:0px;width:100%">
				<tr>
					<th style="border:0px"><span class="rtitle" style="width:150px;display:inline-block;">ATTENDANCE : </span> 
						<?php
							if($result && $ex->total_class > 0):
								if($ex->present_class > $ex->total_class)
									echo "100.00%";
								else
									printf("%.2f%s",$ex->present_class/$ex->total_class *100,"%"); /*?> [ <?=$ex->present_class?>/<?=$ex->total_class?> ] <?php */ 
							endif;
						?>
					</th>
					<th style="border:0px"><!-- span class="rtitle" style="width:150px;display:inline-block;">HOMEWORK :</span> 
							<?php printf("%.2f%s",$ex->done/$ex->given * 100,"%")?>
						-->
					</th>
					<th align="right" style="text-align:right;border:0px"><span class="rtitle"> GPA : </span> 
						<span style="width:30px;display:inline-block;text-align:right">
							<?php if($result && $comp): printf("%.2f",$df->sgpa); else: echo'----'; endif; ?>
						</span> 
					</th>
				</tr>
				<tr>
					<th style="border:0px" class="text-left" colspan="2"><span style="font-size:1.2rem">Exam Held Between: <?=$df->min?> and <?=$df->max?></span></th>
					<th style="border:0px" class="text-right"> <BR>
					<div style="text-align:center;display:inline-block;font-size:1.5rem;background:#eee;color:#3c8dbc;padding:2px 6px;border-radius:20px">PRINCIPAL</div> </th>
				</tr>
			</table>
		</th>
	</tr>
</table>
<div style="font-size:1.3rem;text-align:center;padding:10px;font-weight:bold"> **Please Click On Subject Name to view Answers</div>
<div style="height:100px"></div>
<style>
	table#student_list { width:80%;margin:0 auto; background:#fafafa; border:6px solid #3c8dbc; }
	table#student_list tr td,table#student_list tr th{ padding:8px 10px; border:1px solid #ddd; }
	table#student_list tr th { background:#3c8dbc;padding:3px 15px; font-size:1.2em; color:#eee; padding:5px; }
	table#student_list td.orgname{ background#fefefe !important; }
	td.error{ color: crimson !important; background:#ffe5e5;}
	span.error{ color:crimson; border:1px solid crimson; }
	.ttl{ color:#3c8dbc; }
	span .strong{ font-weight:bold; }
	.review_paper{ color:#555; font-weight:bold; cursor:pointer; }
	.review_paper:hover{ text-decoration:underline; }
	
	.styleclassqgroups{ background: #fff;
    padding: 40px;
    margin-left: 17px;
    max-width: 1265px; }
</style>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-AMS_HTML"></script>
<script>
	$(function(){
		
		$('.styleclassqgroups').on('click','.review_paper',function(e){
			e.preventDefault();
			e.stopPropagation();
			let url = '/exam/result/review_paper';
			$('#studentbody').load(url,{exam_id:$(this).data('exam_id'),subjectid:$(this).data('subjectid'),studentid:$(this).data('studentid')},function(data){
				MathJax.Hub.Queue(["Typeset",MathJax.Hub,"render-me"]);
				$('div#gstuinfo').html($('td#stuinfo').html());
				$('#studentModal').modal('show');
			});
		});
	});
</script>
<div id="studentModal" class="modal fade" role="dialog">
	<div class="modal-dialog" style=" overflow-y: initial !important; width:65%; ">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-check-circle"></i> Review answers <!-- i class="fa fa-file-excel-o" data-id="studentbody" data-name="Midas_Export_StudentList" style="margin-left:20px"> Download</i --></h4>	
			</div>
			<div class="modal-body" id="studentbody" style=" height: 80%; overflow-y: auto;">
			</div>
			<div class="modal-footer">
				<button type="button" class="closed btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
