<?php
	function number2roman($N){
        $c='IVXLCDM';
		$c='ivxlcdm';
		for($a=5,$b=$s='';$N;$b++,$a^=7)
		{
			$o=$N%$a;
			$N=$N/$a^0;
            while($o--)
			{
				$g=$o>2?$b+$N-($N&=-2)+$o=1:$b;
				//if(!empty($g))
					$s=@$c[$g].$s;
			}	
				
		}
		
        return $s;
	}
   ?>
   
<?php if(isset($data) && is_array($data)): ?>
	  <div class='qpaper'>
		<?php //var_dump($data); ?>
			<div id="gexaminfo" style="padding:10px;">
				<?php $dd = element('0',$data); ?>
				<center><table style="width:90%">
					<th width="33.33%" class="text-left"><span class="ttl">FM: </span> <?=$dd->fm?></th>
					<th width="33.33%" class="text-center"><span class="ttl">PM: </span> <?=$dd->pm?></th>
					<th class="text-right"><span class="ttl">MY MARKS: </span> <?php printf("%.2f",$dd->om); ?> [ <span class="ttl"><?php printf("%.2f%s",$dd->om/$dd->fm*100,"%"); ?></span> ]</th>
				</table></center>
			</div>
			<div id="gstuinfo" style="padding:10px;margin:10px;border-bottom:2px solid #3c8dbc"></div>
			
		   <?php
				 $mq='A'; $sq=1; $ssq=1;
				 $omg='';
				 $ogid='';
				 $oaid='';
				foreach($data as $d): ?>
					 <?php 
					if($omg != $d->group_name):  $sq=1; $ssq=1; ?>
					 <div class='groupname'> 
						<?=$d->group_name?>
					 </div>
					 <?php endif ;?>
					<?php if(!empty($d->infoid) && $oaid!=$d->infoid): ?>
						<div class='passage'>
							<div class='qtitle'><?=$mq++?>. <?=$d->question_title?> </div>
							
							<div class="passagetext">
								<div class="ptitle"> <?=$d->info_title?> </div>
							<?=$d->additional_info?></div>
						</div>
					<?php endif; ?>
					<?php if($ogid != $d->exam_groupid): ?>
					<div class='grouptitle'> 
						<?php if(empty($d->infoid)): ?> <?=$mq++?> . <?php $sq=1; $ssq=1; else: ?> <?=$sq++?>. <?php $ssq=1; endif; ?> <?=$d->group_title?>  <div class='pull-right'>[ <?=$d->total_qto_solve?> x <?=$d->marks?> = <?=$d->total_qto_solve * $d->marks?> ]</div>
					</div>
					<?php $sq=1; endif; ?>
					<div class="qtext <?php if($d->isskipped=='t') echo 'qskipped'; elseif($d->provided_answer==$d->correct_answer) echo 'qcorrect'; elseif($d->correct_answer!='') echo 'qwrong'; ?>">
					<?php if(empty($d->isskipped) && empty($d->isfinalized)): ?>
						<div class="qrow">
							<div class="qcol sno"></div>
							<div class="qcol">
								<center>	
									<div class="error" style="display:inline-block;clear:both;margin:0 auto;text-decoration:none"> You Did not attempt this question. </div>
								</center>
							</div>
						</div>
					<?php endif; ?>
						<div class='qrow'>
						  <div class='qcol sno'> 
							 <?php if(empty($d->infoid)): $ssq=1; ?> 
									<?=$sq++?>. 
							 <?php else: ?> 
									<?=number2roman($ssq++)?>. 
							 <?php endif; ?>
						  </div>
						  <div class='qcol' id="question_<?=$d->attempt_id?>"> 
								<?php if($d->qtype!='FITB') : ?>
											<?=$d->question_text?> 
								 <?php else: ?>
											<?php
												$question_text=$d->question_text;	
											if($d->isskipped!='t')
											{
												$question_text = str_replace('&amp;hellip;', '...', htmlentities($question_text));
											
												$question_text = html_entity_decode($question_text);
												$parts = explode('#',$d->provided_answer);
												$cans = explode('#',$d->correct_answer);
												$pats=array();
												for($i=0;$i<count($parts);$i++)
												{
													if($parts[$i] == $cans[$i])
														$parts[$i] = " <U class='pa pcorrect'>  $parts[$i] </U> "; 
													else if($parts[$i] != $cans[$i])
														$parts[$i] = "&nbsp;&nbsp; <U class='pa pwrong'>  $parts[$i] </U>&nbsp;&nbsp;[<U class='ca'><i class='fa fa-check-circle'>  $cans[$i]  </i></U>] &nbsp;&nbsp; "; 
													else
														$fcls='';
												
													$pats[$i]='/(\.{2,}|_{2,}|-{2,})+/i';
												}
											
												$question_text = preg_replace($pats,$parts,$question_text); 
											}
											echo $question_text;
										?>	
								 <?php endif; ?>
							<center>
								<?php 	$opts=array();
										if($d->qtype=='MCQ'): $ssq=1; 
											$opt = explode('####',$d->qoptions); ; 
											$i=1;
											foreach($opt as $o): 
												$qo = explode('$$$$',$o);	

											$opts[$qo[1]]=$qo[0];
											
											if($d->provided_answer == $qo[1] && $d->correct_answer== $qo[1])
												$ncls='pcorrect';
											else if($d->correct_answer == $qo[1])
												$ncls='correct';
											else if($d->provided_answer == $qo[1])
												$ncls='pwrong';
											else
												$ncls='';											
								?>
								<div class="col-md-6 opt <?=$ncls?>"> 
										<span class='opts'> <?=number2roman($ssq++)?>. <?=$qo[0]?> </span> 
								</div>
								<?php endforeach;
										elseif($d->qtype=='TF'):
											$opts=array('T'=>'True', 'F'=>'False',''=>'-----');
											$qo=array('F','T');
											$i=rand(0,1);
								
											if($d->provided_answer == $d->correct_answer && $d->provided_answer==$qo[$i])
											  $ncls='pcorrect';
											else if($d->provided_answer == $qo[$i])
											  $ncls='pwrong';
											else if($d->correct_answer == $qo[$i])
											  $ncls='correct';
											else
											  $ncls='';
								?>
								<div class="col-md-6 opt <?=$ncls?>"><span class='opts'><?=number2roman($ssq++)?>. 
											<?php if($qo[$i]=='T') : ?> True <?php else: ?> False <?php endif; ?> </span></div>
								<?php 
										$ro = $qo[$i] == 'T' ? 'F' : 'T';
										
										if($d->provided_answer == $d->correct_answer && $d->provided_answer==$ro)
											  $ncls='pcorrect';
											else if($d->provided_answer == $ro)
											  $ncls='pwrong';
											else if($d->correct_answer == $ro)
											  $ncls='correct';
											else
											  $ncls='';
								?>
								<div class="col-md-6 opt <?=$ncls?>"><span class='opts'n><?=number2roman($ssq++)?>. 	 <?php if($ro=='T'): ?> True <?php else: ?> False <?php endif; ?></span>
								</div>									
								<?php endif; ?>
								<div style="clear:both"></div>
								<?php if($d->isskipped!='t'){ ?>
								<div class="extra_info" id="extra_info_<?=$d->attempt_id?>">
									<?php 
										if(!empty($d->start_time) && !empty($d->end_time))
										{ 
											
											$di=date_diff(new DateTime($d->end_time),new DateTime($d->start_time)); $takentime=str_replace('0 days','',$di->format('%a days %H:%I:%S')); 
											$giventime=gmdate('H:i:s',$d->allowed_time_min*60);
											if(strtotime($takentime) > (strtotime($giventime)+15) ):
									?>
												<span class='ltime'> Time: ( Allowed:<?=$giventime?>, Taken:<?=$takentime?> ) </span>
									<?php
											else:
									?>
												<span class='rtime'>
													Time: (Allowed:<?=$giventime?>, Taken:<?=$takentime?> )
												</span>
									<?php
											endif;
										}	
									?>		
								<?php //}else { ?> 			
										<?php 	
											if($d->is_correct=='t'): 
												$spncls = 'rtime';	
											elseif($d->is_correct=='f'): 
												$spncls = 'ltime';   
											else: 
												$spncls='';	
											endif; 
										?>
											
											<span class="<?=$spncls?>">
												<?php echo element($d->is_correct,array(''=>'---------','t'=>'Correct','f'=>'Incorrect')) ?>
											</span>
									
										<span class='omarks <?=$spncls?>'>
											Marks: <?php printf("%.2f",$d->obtainedmarks); ?> / <?php printf("%.2f",$d->marks);?>
										</span>
									
										<?php if($d->qtype=='QA' || $d->qtype=='FITB'): ?>
											<div class="provided_answer" id="provided_answer_<?=$d->attempt_id?>">	
												<p style="font-weight:bold"> Provided Answer </p>
												<p>
													<?php 
														if(!empty($d->provided_answer))
															echo $d->provided_answer;
														else
															echo "<span style='color:red'>Student did not provide any answer.</span>";
													?>
												</p>
											</div>
										<?php endif; 
											if(!empty($d->teachers_comment)): ?>
										
												<div class="teachers_comment" id="teachers_comment_<?=$d->attempt_id?>">
													<p> Teacher's Comment </p>
													<p><?=$d->teachers_comment?><p>
												</div>
												
											<?php endif; ?>		
								</div>
								<?php } ?>
								
							</center>
						  </div>
						</div>
					</div>	
			<?php $omg=$d->group_name;  $ogid=$d->exam_groupid; $oaid=$d->infoid;
				  endforeach; ?>
	  </div>
	 <style> 
		div.notice{ font-size:0.85em; color:crimson; text-align:center;padding:5px; } 
		div.options *:not(input,.math-tex){ all:initial; }
		div.groupname{ text-align:center; margin:0 auto;width:90%;font-size:2em; }
		div.grouptitle,div.qtitle{ text-align:left; margin:0 auto;width:90%;font-size:1.3em; font-weight:bold; padding:20px; }
		
		div.passagetext *:not(input,.math-tex){ all:initial; font-size:1em !important; }
		div.passagetext p span,div.passagetext span span{ margin:0px; font-family:initial; font-size:initial; }
		div.passagetext p,div.passagetext span{ 
				font-size:1.1em !important; margin-left: .3in; 
				text-align: justify; /* display:inline-block; */
				font-family:initial; 
				font-family:'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif !important;
				margin-bottom:20px;
				font-weight:400;
				}
				
		div.passagetext{ text-align:left; font-size:1em; margin:0 auto; width:90%; border:1px solid #ddd; border-radius:20px; padding:20px; background:#fefefe; }
		
		div.ptitle{ text-align:center;font-size:1.4em; margin:10 auto;padding:20px; width:90%; font-weight:bold; }
		
		div.qtext{ display:table; border:1px solid #ccc; padding:5px 10px; width:99%; margin:8px auto; 
		border-collapse:separate; table-layout:fixed; font-weight:bold; background:#fefefe; }
		div.qrow{ display:table-row; }
		div.qcol{ display:table-cell; vertical-align:top; padding:5px 10px; }
		div.sno{ 	width:25px !important; }
		div.opt{ text-align:left; padding:5px 0px; font-weight:normal; }
		
		div.qpaper{ background:#fafafa; margin:0 auto;border:1px solid #ddd; }
		
		div.opt span.opts{ display:inline-block; padding:3px 10px;
						   border-radius:5px; border:2px solid transparent; border-radius:30px; 
						   background:#efefef;
						 }
		div.pcorrect span.opts{ border:2px solid #006400; color:#556B2F; 	}
		div.correct{ color:blue }
		div.pwrong span.opts{ border:2px solid #DC143C; color:#DC143C; }
		
		div.qskipped{  text-decoration:line-through; background:#FFC0CB; }
		div.qskipped *{ text-decoration:line-through; }
	
		div.extra_info{ display:block; padding:5px; margin:5px; text-align:left; border-top:1px solid #ddd; }
		div.extra_info span{display:inline-block; font-weight:600; font-size:0.8em; border-radius:10px; margin:5px; padding:5px 10px; background:#efefef; }
		span.ltime{ border:2px solid #FFC0CB; color:maroon;  }
		span.rtime{ border:1px solid #90EE90; color:#228B22;	}
		
		span.add_cmnt,span.chk_ans{ color:#337ab7;  cursor:pointer; border:2px solid #efefef; }
		span.update_changes{ color:#eee;  cursor:pointer;background:#337ab7 !important; }
		span.update_changes:hover{ background:darkblue !important;text-decoration:underline; }
		
		span.add_cmnt:hover,span.chk_ans:hover{ border:2px solid #337ab7; text-decoration:underline; } 
		
		
		div.qcorrect{ border:2px solid #8FBC8F; }
		div.qwrong{ border:2px solid #FA8072; }
		
		div.qcol u.pa{ background:#efefef; border-radius:30px; border:1px solid transparent; padding:3px 10px;  } 
		div.qcol u.ca{ color:blue; }
		div.qcol u.pa.pcorrect{ border:1px solid #8FBC8F; }
		div.qcol u.pa.pwrong{ border:2px solid #FA8072; }
		
		div.provided_answer{ border:2px solid #ddd; margin:10px auto; font-weight:normal; font-size:13px; padding:10px; text-align:left !important; }
			
		div.provided_answer img{ width:90% !important; height:auto !important; cursor:pointer; }
		div.provided_answer{ background:#fff; }
		
		div.comments{ width:90%; margin:10px auto; border:2px solid #ddd; padding:5px; background:#fff; }
		div.comments *{ border:0px; width:100%; height:100px; }
		div#studentbody div.extra_info{ text-align:left; }
		
		div.modal-header,div.modal-footer{ background:#3c8dbc;color:#fefefe; text-shadow:0 2px 5px; opacity:0.8; padding:5px 10px !important; }
		div.modal-header * { color:#fff !important; }
		div.modal-header h4 { font-size:1em; font-family:"Trebuchet MS" !important; }
		div.modal-footer button{ padding:2px; border:1px solid #ccc;}
		div.modal-body{ background:#f5f5f5; }
		div.error{
			color:red;
			text-align:center;
			font-size:1.2em;
			padding:3px 10px;
			border:2px solid crimson;
			border-radius:20px;
		}
		
		div.success{
			color:green;
			text-align:center;
			font-size:1.2em;
			padding:3px 10px;
			border:2px solid #8FBC8F;
			border-radius:20px;
		}
		
		span.success{ border:1px solid #8FBC8F !important; background:#ADFF2F !important; }
		
	 </style>
	 <script> 
			$(function(){
				$('div#studentModal').on('dblclick','.provided_answer img',function(){
					var w = window.open($(this).attr('src'),'_blank');
					w.focus();
				}); 
				
			});
	</script>
				
<?php endif; ?>
