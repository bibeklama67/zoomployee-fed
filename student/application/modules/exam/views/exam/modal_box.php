<?php
 $ed = $mydata->response;
 //var_dump($ed);
 if(!isset($ed)){ $ed=false; return; } ?>
<div id="examModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
  
		<!-- Modal content-->
		<div class="modal-content">
		 
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" style="text-align:center"><?=$ed?$ed->examtype:''?></h4>
				<div class="ex_sbtitle col-md-3">Class: <?=$ed?$ed->classname:''?></div> 
				<div class="ex_sbtitle col-md-3">Subject: <?=$ed?$ed->subjectname:''?></div>
				<div class="ex_sbtitle col-md-3">FM: <?=$ed?$ed->fm:''?>  [ <?=$ed?(integer)$tm->total_marks:''?> ] </div>
				<div class="ex_sbtitle col-md-3">PM: <?=$ed?$ed->pm:''?></div>
			</div>
			
			<div class="modal-body">
				<?php 
					$grp_name = false; 
					$oinfoid  =  false;
					$ogrpid   =  false;
					$main = 1;
					$sub = 'a';
					$ss = 1;
					$exam_id = element('0',$ginfo);
					$exam_id = $exam_id->exam_id; 
					
					foreach($ginfo as $g): 
						$qi=$qinfo[$g->exam_groupid];
						$qd = element('data',$qi);
						$d  = element('0',$qd);
						$i=1;
				?>
						<div class="passage">
							<?php if($grp_name != $g->group_name){ ?>
							<h3><center><?=$g->group_name?></center></h3>
							<?php 
							 }
								foreach($qd as $d)
								{
							?>	
								<?php 
									if( $oinfoid != $d->infoid ):
										if(!empty($d->infoid)) { $sub='a'; $ss=1; ?>
											<h5> <?=$main++?>. <?=$d->question_title?> </h5>
											 <div style="text-align:center;font-weight:bold"><?=$d->info_title?> </div>
											<div class="passage_text">
												<?=trim($d->additional_info,"'")?>
											</div>
								<?php } endif; ?>
						
								<?php //if($ogrpid != $g->exam_groupid):
									  if($i==1):
								?>
										<div class="questions">
											<h5>
												<?php if(!empty($d->infoid)) { ?> <?=$sub++?>. <?php $ss=1; } else { ?> <?=$main++?>. <?php $sub='a'; $ss=1; } ?> 
												<?=$g->group_title?> 
													<?php if($g->total_qto_solve != $g->total_qto_display): ?>
														( Attempt any <?=$g->total_qto_solve?> )
													<?php endif; ?>
													 <span class="pull-right"> <?=$g->marks?>x<?=$g->total_qto_solve?>=<?=$g->marks*$g->total_qto_solve?></span>
													 <BR><span class="red">[ 
													<?php if($g->isdynamic=='f'){ ?>
															Same Questions will be asked to all students. Students will get <?=$g->allowed_time_min?> minutes for each question.
													<?php } else { ?>
															Students will see only <?=$g->total_qto_display?> Questions, randomly shuffled from the available. Students will get <?=$g->allowed_time_min?> minutes for each question. 
													<?php } ?>
													  ]</span>	
											</h5>	 
												
											<div class="questiontop-wrap">
								<?php //endif; 
									   endif;
								?>
						
												<div class="question-wrap">
													<div class="checkbox">
														<?php if(!empty($d->infoid)){ ?> 
																<?=number2roman($ss)?>. 
														<?php $ss++; } else {?> 
																<?=$sub++?>.
														<?php
																$ss=1;
														} ?>
													</div>
													<div class="qtext">
														<?php if($d->qtype!='FITB') : ?>
																	<?=$d->question_text?> 
														 <?php else: ?>
																	<?=preg_replace('/(\.\.|__|--)+/i',"<U style='color:blue'> $d->correct_answer </U>",$d->question_text)?>	
														 <?php endif; ?>
														 <?php if($d->qtype=='MCQ'): ?>
															<div class="mcq_opt">
																<?php $opts = explode('####',$d->option_value); 
																	   $oc=1;
																	   $ca = $d->correct_answer;
																			foreach($opts as $o){
																					$od=explode('$$$$',$o);
																					if(!empty($od[0])): ?>
																				<div class="col-md-6" style="padding:5px; <?php if($ca == $od[1]){ ?> color:blue;font-weight:bold <?php }?>">
																					<?=number2roman($oc++)?>. <?=$od[0]?>
																				</div>	
																			<?php 
																				   endif;
																					//echo $o;
																			}
																		?> 
															</div>
														 <?php endif; ?>
														 <?php if($d->qtype=='TF'): ?>
															<div class="mcq_tf">
																<?php  
																	$oc=1;
																	$ca = $d->correct_answer;
																	
																	?>
																	<div class="col-md-6" style="padding:5px; <?php if($ca == 'T'){ ?> color:blue;font-weight:bold <?php }?>">
																		<?=number2roman($oc++)?>. True
																	</div>
																	<div class="col-md-6" style="padding:5px; <?php if($ca == 'F'){ ?> color:blue;font-weight:bold <?php }?>">
																		<?=number2roman($oc++)?>. False
																	</div>		
															</div>
														 <?php endif; ?>
														 <?php if($d->qtype=='QA'): ?>
															<div class="mcq_qa">
																Ans: <?=nl2br($d->correct_answer);?>
															</div>
														 <?php endif; ?>	
													</div>
												</div>
									
								<?php if($i == count($qd)): ?>						
											</div>
										</div>					
						</div>	
								<?php
						endif; $i++;
									$oinfoid = $d->infoid;	
								} 
						$grp_name=$g->group_name;		
					endforeach; ?>
				
			</div>  
		  
			<div class="modal-footer">
				<div style="font-size:12px">* Once Finalized Questions Cannot be Added/Removed and Groups Cannot Be Modified</div>
				<div class="pull-left" id="verifymsg"></div>
				<button class="btn btn-primary btn-lte Verify" 
					type="button" id="btnverify" data-examid="<?=$exam_id?>">Verify And Finalize</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<style> .ex_sbtitle{ font-size:1.2em; text-align:center; margin-top:10px; } </style>
<style>
	.passage_text *:not(input,.math-tex){ all:initial; font-family:arial; font-size:1em; font-weight:bold; }
	.passage_text p{ display:block !important; margin:10px 0px !important;}
	.passage_text p * { line-height:1.5 !important; }
	.passage { max-width:90%; margin:0 auto; }
	
	.questiontop-wrap{ display:table; table-layout:fixed; width:96%; margin-left:2%; }
	.question-wrap{ display:table-row; }
	.question-wrap:hover{ background:#abc; }
	.checkbox {display:table-cell; vertical-align:top; padding:5px; width:20px; }
	.qtext{ display:table-cell; padding:5px; }
	.qtext *:not(input,.math-tex){ all:initial; margin:0px 5px !important; 
							 font-family:arial; 
							 font-size:1em; 
							 font-weight:bold; 
							 line-height:2; }
							 
	.qtext p{ margin-left:0px !important; } 
	span.red{ color:red; font-size:12px; font-weight:bold; }
	.success{ color:green; }
</style>
<?php
	function number2roman($N){
        $c='IVXLCDM';
		for($a=5,$b=$s='';$N;$b++,$a^=7)
		{
			$o=$N%$a;
			$N=$N/$a^0;
            while($o--)
			{
				$g=$o>2?$b+$N-($N&=-2)+$o=1:$b;
				//if(!empty($g))
					$s=@$c[$g].$s;
			}	
				
		}
		
        return $s;
	}
?><script>
$(function(){
	var segment="<?=$this->segment?>";
	$('#btnverify').off('click');
	$('#btnverify').on('click',function(e){
		var vurl = '/' + segment + '/exam/exam_setup/finalize_exam/<?=$exam_id?>';
		var exam_id = $(this).data('examid');
		
		$.post(vurl,{exam_id:exam_id},function(data){
			if(data.status=='error')
				$('#verifymsg').addClass('error').removeClass('success');
			else
			{
				$('#verifymsg').removeClass('error').addClass('success');
				$('#exam_paper_submit').attr('disabled',true).addClass('hidden');
			}
			
			$('#verifymsg').html( data.msg ); 
			
		},'json');
	});
});
</script>