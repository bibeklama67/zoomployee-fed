<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-AMS_HTML"></script>
<style>
	.modal-header{ background: #f5f5f5; color:#365899; font-weight:normal; border:2px solid #C8C8FE; border-radius:5px; }
	.exam_group_detail{ background: #eaf1e4; color:#365899; padding:5px; font-size:1.2em;}
	.g_questions{ border:2px solid #C8C8FE; margin:0px 10px 10px 10px; }
	.questions{ padding:10px; background:#f5f5f5;}
	.qid { background:#fff; border-radius:0px 20px 0px 20px; padding:10px; margin:10px;border:1px solid #ddd; }
	
	@media only screen and (min-device-width:320px) and (max-device-width:480px)
	{
		.container { width:100% !important; padding:0px !important; margin:0px !important; }
		
		.mcont table{ }
		#exam-table tr th, #exam-table tr td{
			font-size:12px !important;
			padding:4px !important; 
		}
		#exam-table input {
			font-size:10px !important;

		}
		#timer{
			text-align:center;
		}
		footer
		{
			display:none;
			position: relative!important;
			padding-top:20px!important;
		}
		
		.container ul.nav_main{ display:none; }

		div.g_questions{ margin:0px; }
		
		.submitexam-btn{
			margin-top: 9px;
		}
		.qgroupwrapper{
			padding:10px 5px!important;
		}
		
		.row{ margin:0px !important; padding:0px !important; }
		.questions { padding:0px; margin:0px; border:0px; }
		.qid{ margin:0px; padding:0px; width:100% !important; border:0px; border-radius:0px; }
		.question-wrap{ padding:0px; margin:0px; border:0px; border-radius:0px; }
	}

/* END OF FOR EXAM  */
	
	.confirm-box,.skip-box{
			display:flex !important; 
			flex-direction:row;
		}
		.skip-box{ padding-left:10px; }
		
	.confirm-box input,.skip-box input{ width:20px; height:20px; }
		
</style>
<?php 

	if(isset($mydata) && $mydata=='expired')
	{ 
			echo "<h3 style='color:red;margin:30px'>Sorry !! Your Exam Time Is Over. </h3>";
			die;
	}
	
	if(isset($mydata) && isset($mydata->response))
	{
		$ed = $mydata->response; 
		$ed = element('0',$ed); 
	} else{ 
			echo "<h3 style='color:red;margin:30px'>Exam Information Not Supplied</h3>";
			die;
	  }
		$orgname = $this->session->userdata('org');
		if(!$orgname)
		{
			$stuid = $this->session->userdata('studentid'); 
			$child = $this->session->userdata('children'); 
			$cur_child = false;
			if($child)
			{
				foreach($child as $c)
				{
					if($c['studentid'] == $stuid)
					{ $cur_child = $c; break; }
				}	
			
				$orgname = element('orgname',$cur_child);
				$address = element('address',$cur_child);
				$district = element('district',$cur_child);
			}else{ $orgname = ''; $address=''; $district=''; }
		}else{
			$district = $this->session->userdata('district');
			$address = $this->session->userdata('address');
		}
?>
<div class="qgroupwrapper" style="padding:20px 40px;">
	<div class="exam_info">
		<div class="modal-header" style="padding:5px;margin-bottom:10px">
			<h4 style="text-align:center; color:#365899 !important"> 
				<?=$orgname;?> <span style="font-size:14px">(<?=$address;?>, <?=$district?> ) </span>
			</h4>
			
			<h4 style="text-align:center;color:#365899 !important;margin-bottom:10px;padding-bottom:10px;border-bottom:1px solid #ddd">	
				   <?=$ed?$ed->examtype:''?>
			</h4>	
			<div style="clear:both"></div>
			
					<div class="col-md-2"> Subject: <?=$ed?$ed->subjectname:''?> </div>
					<div class="ex_sbtitle col-md-2" style="text-align:left">
								FM: <?=$ed?$ed->fm:''?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; PM: <?=$ed?$ed->pm:''?>
					</div>
					<div class="ex_sbtitle col-md-3" style="text-align:left">
								Class: <?=$ed?$ed->classname:''?>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								Subject: <?=$ed?$ed->subjectname:''?>
					</div> 
					<div class="col-md-5" style="text-align:left">
							<?php $sc=element('0',json_decode($ed->scheduleinfo)); ?>
							 Start Date/Time: <?=date('Y-m-d H:i',strtotime($sc->start_datetime));?> &nbsp;&nbsp;&nbsp;&nbsp; End Date/Time: <?=date('Y-m-d H:i',strtotime($sc->end_datetime)); ?>
					</div>				
		</div>		
	</div>	
	
	<div class="exam_groups">
		<?php 
			$eg_info = $eg_info->response; 
			$completed_groups = array();
			$current_examgroupid = false;
			$current_examqid = false;
			
			$cur_info = isset($cur_progress) && isset($cur_progress->response) ? $cur_progress->response:false;
			if(isset($cur_info) && !is_null($cur_info) && !empty($cur_info))
			{
				$completed_groups = explode(',',$cur_info->completed_groups);
				$current_examgroupid = $cur_info->current_examgroupid;
				$current_examqid = $cur_info->current_examqid;
			}
			
			$ogrpname='';
			foreach($eg_info as $ginfo):
			
				  $gextraclass=''; 
				  if($ginfo->exam_groupid != $current_examgroupid && !empty($current_examgroupid))
				  {
					  if(!($current_examqid && $current_examgroupid && in_array($current_examgroupid,$completed_groups)))
						$gextraclass='hidden';
				  }
				  if(in_array($ginfo->exam_groupid,$completed_groups))
					  $gextraclass = 'completed';
				  
			?>
					<div class="exam_group <?=$gextraclass?>" id="exam_group_<?=$ginfo->exam_groupid?>" >
						<?php if($ogrpname == $ginfo->group_name){ $cls="hidden"; }else{ $cls=''; } ?>			
								<div class="exam_group_head <?=$cls?>" style="color:#365899;background:#fff;margin:0 auto; margin-bottom:0px;" 
									  id="grp_head_<?=$ginfo->exam_groupid?>" >
										<h3 style="text-align:center;"> <?=$ginfo->group_name?> </h3>
								</div>
						<div class="g_questions">
							<div class="exam_group_detail">
								<div style="border-bottom:1px solid #fff;padding:5px 20px; margin-bottom:5px; " >
									This group Contains total number of <?=$ginfo->total_qto_display?> questions. 
									<?php if($ginfo->total_qto_display != $ginfo->total_qto_solve): ?>
											You need to solve only <?=$ginfo->total_qto_solve?> questions.
									<?php endif; ?> 
									To solve each questions you will get <?=$ginfo->allowed_time_min?> minutes.
								</div>
								<?php if( $gextraclass != 'completed' && $ginfo->exam_groupid != $current_examgroupid ): ?>
									<div style="border-bottom:1px solid #fff;padding:5px 20px; margin-bottom:5px;position:relative;display:grid; grid-template-columns: 1fr auto;">
									Your time starts when you click on "START" button.
									<span>

									<?=form_button("Start","START","class='btn btn-success g_start' data-exam_groupid='$ginfo->exam_groupid' data-exam_id='$ginfo->exam_id' style=''")?>
									</span>
								</div>	
							
								<?php endif; ?>
									<div style="clear:both"></div>
								<div class="passage hidden" data-infoid=''> </div>
								<div style="padding-left:20px; padding-right:20px">
									<?=$ginfo->group_order?>. <?=$ginfo->group_title?> 
									<div class="pull-right" style="margin-right:10px"> 
										[ <?=$ginfo->total_qto_solve?> x <?=$ginfo->marks?> = <?=$ginfo->total_qto_solve*$ginfo->marks?> ]
									</div>
									<div style="clear:both"></div>
								</div>
							</div>
							<div class="questions" data-qid=''>	
								<?php if( ($ginfo->exam_groupid == $current_examgroupid) &&
											!in_array($ginfo->exam_groupid,$completed_groups)
										){
											$this->load->view('questions',array('response'=>$cur_info->qinfo));
								}?>
							</div>
						</div>
					</div>
		
		<?php 
			$ogrpname = $ginfo->group_name;
			endforeach; 
		?>	
	</div>
</div>

<script src="<?=base_url()?>/assets/js/template/ckeditor/ckeditor.js"></script>
<script>
	
	var ckconfig = {width:'100%',height:'105',extraPlugins:'mathjax',
			baseHref:'<?=base_url()?>uploads/editor/online_exam/',
			forcePasteAsPlainText: true,
			filebrowserBrowseUrl: '',
			autoParagraph: false,
			enterMode : CKEDITOR.ENTER_BR,
			shiftEnterMode: CKEDITOR.ENTER_P,
			toolbar:[
					['Undo', 'Redo', '-',
					 'Bold', 'Italic','-','JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ,'-','TextColor','BGColor','-','RemoveFormat'], 
					['Image','Symbol'],['Maximize']
	] } ;	
		
	CKEDITOR.on( 'dialogDefinition', function( ev ) {
		// Take the dialog name and its definition from the event data
		var dialogName = ev.data.name;
		var dialogDefinition = ev.data.definition;
		if ( dialogName == 'image' || dialogName == 'link' ) 
		{
			// Remove upload tab
			dialogDefinition.removeContents('advanced');
			dialogDefinition.removeContents('Link');
			dialogDefinition.dialog.on('show', function () {
               this.selectPage('Upload');
			   $('.cke_dialog_ui_hbox_last .cke_dialog_ui_button').hide();
			   $('.cke_dialog_ui_input_text').attr('readonly',true);
			});
		}
	});	
		
		
	$(function(){
		$('button.g_start').click(function(){
			let grpid = $(this).data('exam_groupid');
			let exid = $(this).data('exam_id');
			let this_btn = $(this);
			
			$('.exam_group_head').not('#grp_head_'+grpid+',.completed .exam_group_head').addClass("hidden");
			$('#grp_head_'+grpid).removeClass("hidden");
			
			$('.exam_group').not('#exam_group_'+grpid+',.completed').addClass('hidden');
			
			$('#exam_group_'+grpid).find('.questions').html('<center><img src="/assets/images/loader.gif"/> </center>');
			
			let url = '/exam/get_next_question';
			
			$('#exam_group_'+grpid).find('.questions').load(url,{exam_groupid:grpid,exam_id:exid},
				function(data)
				{
					  if($('.hid_passage').length > 0)
					  {
						 $('#exam_group_'+grpid).find('.passage').html($('.hid_passage').html()).removeClass('hidden');
					  }
					  let HUB = MathJax.Hub; MathJax.Hub.Queue(["Typeset",HUB,"render-me"]);
					  
					  this_btn.addClass('hidden');
					  
				});	
		});
		
		$('#exam_group_<?=$current_examgroupid?>').find('.passage').html($('.hid_passage').html()).removeClass('hidden');
		
		$('div.exam_groups').on('submit','form.submit_answer',function(e){
			e.preventDefault();
			e.stopPropagation();
			let grpid = $('input[name=exam_groupid]').val();
			let fdata = $(this).serializeArray();
			let ans_text = 'tbd';
			
			if( $(this).find('input[name="ans_text[]"]').length >= 1)
			{
				for(i=0; i< $(this).find('input[name="ans_text[]"]').length ; i++)
				{
					let val = $('input[name="ans_text[]"]')[0].value;
					if($.trim(val) == '')
					{
						ans_text = ''; 
						break; 
					}else{
						ans_text = $.trim(val);
					}
				}
				
			}else{
					ans_text = $(this).find('input[name=ans_text]');
					if(ans_text.attr('type')=='checkbox' || ans_text.attr('type')=='radio')
					{
						if( $(this).find('input[name=ans_text]:checked').length <= 0)
							ans_text='';
						else
							ans_text = $(this).find('input[name=ans_text]:checked').val();
					}else{
						ans_text = $.trim(CKEDITOR.instances.ans_text.getData());
						//ans_text = $.trim($(this).find('input[name=ans_text]').val());
					}
			}
			
			if($(this).find('input[name=confirm]:checked').length <= 0 && 
				 $(this).find('input[name=skip]:checked').length <= 0)
				{ 
				  alert("Check Confirm to Save; Skip to Discard"); 
				  return false;
				}
				
			if( $(this).find('input[name=confirm]:checked').length == 1 && 
				ans_text == '' 
			 ){ alert("You must provide answer to submit"); 
					return false;
				}
			
			if( $(this).find('input[name=skip]:checked').length == 1 && 
				ans_text != '' 
			 ){ 
				if(!confirm("You have provided your answer. Do you really want to skip?"))
					return false;
			}	
			
			
			let url = $(this).attr('action');
			
			$('#exam_group_'+grpid).find('.questions').load(url,fdata,
				function(data)
				{
					  if($('.hid_passage').length > 0)
					  {
						  $('#exam_group_'+grpid).find('.passage').html($('.hid_passage').html()).removeClass('hidden');
					  }
					  let HUB = MathJax.Hub; MathJax.Hub.Queue(["Typeset",HUB,"render-me"]);
				});
			
			return false;
		});
		
		$('div.exam_groups').on('change','input[name=confirm],input[name=skip]',function()
		{
			if( $(this).attr('name') == 'skip' && $(this).prop('checked') )
				$('input[name=confirm]').prop('checked',false);
			
			if( $(this).attr('name') == 'confirm' && $(this).prop('checked') )
				$('input[name=skip]').prop('checked',false);
			
		});
		
		
		$('div.exam_groups').on('click','button.complete_exam',function(){
				
				let grpid = $(this).data('exam_groupid');			
				$('#exam_group_'+grpid).addClass('completed');
				$('#exam_group_'+grpid).find('div.questions').addClass('hidden');
				$('.passage').addClass("hidden");	
				$('.exam_group_head').removeClass("hidden");
				$('#grp_head_'+grpid).addClass("hidden");
				$('.exam_group').not('#exam_group_'+grpid+',.completed').removeClass('hidden');
				
		});
		
	});
	
	var timeo;
	function start_timer()
	{
		if( $('#timer').length ){
			var time = $.trim($('#timer').html());
			var secs = toSecs(time);
			secs = secs - 1; 
			var tm = toTime(secs);
			$('#timer').html(tm);
			if(secs <= 0){
				clearTimeout(timeo);
				$('#timer').html("00:00:00");
			}
			else
			{
			  if(timeo)
				  clearTimeout(timeo);
			  
			  timeo = setTimeout(start_timer,1000);
			}
		}
	}
	
	function toSecs(str) 
	{
		var p = str.split(':'),
			s = 0, m = 1;

		while (p.length > 0) {
			s += m * parseInt(p.pop(), 10);
			m *= 60;
		}

		return s;
	}
	
	function toTime(seconds) 
	{
		return [
			parseInt(seconds / 60 / 60),
			parseInt(seconds / 60 % 60),
			parseInt(seconds % 60)
		]
        .join(":")
        .replace(/\b(\d)\b/g, "0$1")
	}
</script>
<style>
	.passage_text *:not(input,.math-tex){ all:initial; font-family:arial; font-size:0.8em; font-weight:bold; }
	.passage_text p{ display:block !important; margin:10px 0px !important;}
	.passage_text p * { line-height:1.5 !important; }
	.passage { max-width:90%; margin:0 auto; }
	.passage_text{ background:#ccc; color:888;padding:10px;border-radius:10px;margin-bottom:10px; }
	#timer{ color:red; }
	div.completed { background:green; }
</style>