<?=form_open("/exam/save_and_next","class='submit_answer'")?>
<div class="qid">
					
<?php $d=isset($response)?$response:false; 
	if($d && isset($d->exam_group_complete)):
?>
	<div style="color:green;text-align:center;padding:20px;font-size:1.3em">
		Congratulations!! You Have Completed This Group. 
		To Attempt Other Groups Please <a href="<?=base_url()?>/exam/start_exam" style="background:blue;padding:5px 10px;color:#ddd; border:1px solid #ddd; border-radius:5px" > Click Here </a>
				
		<?php //form_button('clk',"Click Here","class='btn btn-info complete_exam' data-exam_groupid='$d->exam_group_complete'"); ?>
	</div>
<?php 	
	elseif($d):
?>
<?=form_hidden('current_qorder',$d->q_order)?>
<?=form_hidden('exam_id',$d->exam_id)?>
<?=form_hidden('exam_qid',$d->exam_qid)?>
<?=form_hidden('exam_groupid',$d->exam_groupid)?>
<?=form_hidden('qid',$d->qid)?>
 <div class="question-wrap" >
	<?php if(!empty($d->infoid)){ ?> 
			<div class="hid_passage hidden">
				<h5> <?=$d->question_title?> </h5>
				<h3 style="text-align:center"><?=$d->info_title?></h3>
				<div class="passage_text"> <?=$d->additional_info?> </div>
			</div>
	<?php } ?>
	<div class="checkbox">
		<?php if(!empty($d->infoid)){ ?> 
				<?=number2roman($d->q_order)?>.
		<?php } else {?> 
				<?=$d->q_order?>.
		<?php } ?>
	</div>
	<div class="qtext">
		<?php if($d->qtype!='FITB') : ?>
					<?=$d->question_text?> 
		 <?php else: ?>
					<?php 
					$question_text = str_replace('&amp;hellip;', '...', htmlentities($d->question_text));
					
					$question_text = html_entity_decode($question_text);
					
					$question_text = preg_replace('/(\.\.|__|--)+/i',"<input type='text' name='ans_text[]' />",$question_text); 
					echo $question_text;
					?>	
		 <?php endif; ?>
		 <?php if($d->qtype=='MCQ'): ?>
			<div class="mcq_opt">
				<?php $opts = explode('####',$d->option_value); 
					   $oc=1;
					   $ca = $d->correct_answer;
							foreach($opts as $o){
									$od=explode('$$$$',$o);
									if(!empty($od[0])): ?>
								<div class="col-md-6" style="padding:5px;">
									<?=form_radio('ans_text',$od[1],false,"style='width:20px;height:20px'")?><?=number2roman($oc++)?>. <?=$od[0]?> 
								</div>	
							<?php 
								   endif;
							}
						?> 
			</div>
		 <?php endif; ?>
		 <?php if($d->qtype=='TF'): ?>
			<div style="color:blue; font-weight:bold; padding:10px;"> Your Answer: </div>
			<div class="mcq_tf">
				<?php  $oc=1; ?>
					<div class="col-md-6" style="padding:5px;">
						<?=number2roman($oc++)?>. True 
						<?=form_radio('ans_text','T',false,'style="width:20px;height:20px"')?>
					</div>
					<div class="col-md-6" style="padding:5px;>">
						<?=number2roman($oc++)?>. False
						<?=form_radio('ans_text','F',false,'style="width:20px;height:20px"')?>
					</div>		
			 </div>
		 <?php endif; ?>
		 <?php if($d->qtype=='QA'): ?>
			<div class="mcq_qa">
				<div style="color:blue; font-weight:bold; padding:10px;"> Your Answer: </div>
				<?=form_textarea("ans_text",'','class="ans_text dock" id="ans_text"')?>
				<script> $(function(){ CKEDITOR.replace('ans_text',ckconfig); }); </script>
				<div style="background:#ccc; color:blue;padding:5px;margin:5px;text-align:center">
					To upload image, please (1) click on image icon (2) click on "choose File" Then (3) Click on "Send it to the server"  (4) Click on "Ok".
				</div>
			</div>
		 <?php endif; ?>
		 <div style="clear:both"></div>
		 <?php if( isset($d->updated_rows) && $d->updated_rows == 0 ): ?>
			  <div style="color:red;padding:5px;text-align:center;background:#f5f5f5;border:1px solid #ccc;margin-top:10px;">
				You had already submitted the last question. First confirmed answer will prevail. You can continue with the current question.
			  </div>
		 <?php endif; ?>
		 <div style="clear:both"></div>
		<div style="width:100%;background:#eaf1e4;padding:5px 3px;margin-top:10px;border-radius:5px;border:1px solid #ddd">
			<div style="display:flex;flex-wrap:wrap;justify-content:space-around;margin-top:7px">
				<div style="">
					Skipped: <span style="color:red"> <?php echo $d->attempt->skipped?> </span>
					/ Confirmed: <span style="color:blue"> <?php echo $d->attempt->confirmed?> </span>
				</div>
				<div class="" style="display: flex;flex-wrap: wrap;text-align: center;justify-content: center;margin-top:1px">
					<div style="">
						Remaining Time <i class="fa fa-clock-o"></i> :
					</div>
					<div style="" id="timer">
						<?=gmdate('H:i:s', floor($d->allowed_time_min * 60))?>
					</div>
				</div>
				
				<div class="" style="display:flex; flex-wrap:nowrap;margin-top:-6px">
										
						<div class="confirm-box" style="vertical-align:middle">
							<div style="display:inline-block;margin-top:3px"> Confirm : &nbsp; </div>
							<?=form_checkbox('confirm','C',false)?> 
						</div>
						
						<div class="skip-box" style="vertical-align:middle">
							<div style="display:inline-block;margin-top:3px"> Skip :&nbsp; </div>
							<?=form_checkbox('skip','S',false)?> 
						</div>
				
				</div>	
					<div class='mbtn-submit' style="margin-top:-8px">
						<span style="margin-left:5px!important;">
							<?=form_submit("submit","Submit","class='btn btn-info btn-submit submitexam-btn' id='submit_answer'")?>
						 </span>
					</div>				
			</div>
		</div>
		<div style="font-size:0.9em; color:#365899; padding:2px 10px;font-weight:bold">After you provide your answer, please check on "Confirm" and click on "Submit" button to post your answer. <br>
				If you can't answer the question, please put check on "Skip" and click on "Submit".
		</div>
	</div>
</div>
<?php else: ?>
	<div class="error"> No Question Information Found. <?=$message?> </div>
<?php endif; ?>
<?=form_close(); ?>
<style>
	.qid{ display:table; table-layout:fixed; width:98%; }
	.question-wrap{ display:table-row; }
	.checkbox {display:table-cell; vertical-align:top; padding:5px; width:20px; }
	.qtext{ display:table-cell; padding:5px; }
	.qtext *:not(input,.math-tex){ all:initial; margin:0px 5px !important; 
							 font-family:arial; 
							 font-size:1em; 
							 font-weight:bold; 
							 line-height:2; } 
							 
	.qtext p{ margin-left:0px !important; } 
	 span.red{ color:red; font-size:12px; font-weight:bold; }
	.success{ color:green; }
	.ans_text { width:96% }
	.btn-submit { background:#0c50a3; border:#0c50a3;  }
</style>
<?php
	function number2roman($N){
        $c='ivxlcdm';
		for($a=5,$b=$s='';$N;$b++,$a^=7)
		{
			$o=$N%$a;
			$N=$N/$a^0;
            while($o--)
			{
				$g=$o>2?$b+$N-($N&=-2)+$o=1:$b;
				$s=@$c[$g].$s;
			}	
				
		}
        return $s;
	}
?>
<script> 
	$(function(){ 
		
		if(typeof(timeo) != 'undefined' && timeo)
			clearTimeout(timeo);
		
		start_timer(); 
		
		$(this).bind("contextmenu", function(e) {
			 e.preventDefault();
			 e.stopPropagation();
			 //alert('Sorry, This Functionality Has Been Disabled!');
			 return false;
		});
		
		$(document).keydown(function(event) {
			var pressedKey = String.fromCharCode(event.keyCode).toLowerCase();
			if (event.ctrlKey && (pressedKey == "c" || pressedKey == "u")) {
						alert('Sorry, This Functionality Has Been Disabled!');
						event.preventDefault();
						event.stopPropagation();
						return false;
			} 
			
			if (event.ctrlKey && (pressedKey == "c" || pressedKey == "v")) {
						alert('Sorry, This Functionality Has Been Disabled!');
						event.preventDefault();
						event.stopPropagation();
						return false;
			}
			
			if (event.ctrlKey || event.altKey) {
						event.preventDefault();
						event.stopPropagation();
						return false;
			}
			
			
	});
		
		 /*$(window).on('beforeunload',function(e) {  
					e.preventDefault();
					e.stopPropagation();
					return "Your Answer Might Not Have Been Submitted. Do you really want to Leave?";
			}); */
	}); 
</script>