<style>
  img.loader1{
    position: fixed;
    top: 280px;
    right: 47%;
    z-index: 999;
  }
  .pillsDiv{
    margin-top: 5 !important;
    margin-bottom:5px !important;
  }
</style>

<img  style="display: none;" class="loader1" width="80px" src="<?=base_url().'/assets/images/loader4.gif'?>"/>
<div class="container">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pad-fix sec-block-bg give-border">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 progressbar" style="min-height:530px;">
			<div id="dashboard_wrapper">
			  <div class="breadDiv"></div>
			  <div class="pillsDiv"></div>
			  <div class="row">
				  <?php $this->load->view($subview); ?>
			  </div>			  
			</div>
			<?php   
				if(@$hassubview)
				{
					$this->load->vars($vars);
					$this->load->view($view);
				}
			?>
		</div>        
	</div>
</div>
<!-- POPUP RICHTEXT -->
<!-- div class="modal fade" id="ckeditor-modal" role="dialog" data-backdrop="static" data-keyboard="false" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title">Ask a Question</h4>
      </div>

      <div class="modal-body">   

        <div class="newqueswrap" style="display: none;">

          <input type="hidden" id="livequeschapter">
          <textarea id="questionecktext" class="questionecktext form-control"></textarea>
        </div>  
      </div>
      <div class="modal-footer">
        <div class="error-msg col-xs-10" style="display: none;"></div>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button style="margin-top: -3px; background: #4266b2;" type="button" id="ckeditor_done" data-dismiss="modal" class="btn btn-success btn-send">Post</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="liveqmodal" role="dialog">
  <div class="modal-dialog modal-lg modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="clearfix"></div>
      <div class="modal-body" style="padding-top:10px;">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default billclose" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#schoolupdate-modal" style="display: none;">Open Modal</button>

<!-- POPUP Form -->
<!-- div class="modal fade" id="schoolupdate-modal" role="dialog" data-backdrop="static" data-keyboard="false" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title">Update School</h4>
      </div>

      <div class="modal-body">   
        
      </div>
      <div class="modal-footer">
        <div class="error-msg col-xs-10" style="display: none;"></div>
        <button style="margin-top: -3px; background: #4266b2;" type="button" id="updateschool" data-dismiss="modal" class="btn btn-success">Save</button>
      </div>
    </div>
  </div>
</div -->

