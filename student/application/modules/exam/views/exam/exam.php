<div class="mcont" style="background:#fff;border:0px solid #000;margin:20px 40px;">	
		<div style="background:#eaf1e4;color:rgb(66, 135, 245);border:1px solid #c8c8fe;padding:10px">
			<h3> Instruction for the Students </h3>
			<ul style="margin-left:20px"> 
				<li>Students can take their exam by clicking on the start button</li>
				<li>Once start button is clicked, students are enrolled into the exam. </li>
				<li>Students are allowed specific amount of time for solving each questions. Answers might be submitted after expiry of the allocated time but marks may not be awarded. </li>
				<li>If students have confirmed or skipped any answers; further review will not be allowed.  </li>
				<li>Students can start answering with any group; But once started all the questions of that group should be answered and finalized before starting with the new group of questions. </li>
				<li>Students may take breaks between the groups. </li>
				<li>Students can re-attempt the unfinalized questions and uncompleted groups until Exam End Date/Time; if Internet failures or other uncontrollable situation happens. In such case students should immediately notify the school administration about the event.</li>
				<li>Students can sit for the exam in their comfortable time between given Start and End Date/Time.</li>
				<li>Once End Date/Time expires the exam-list will be removed from this section and student cannot attempt with the exam.</li>
				<li>Once End Date/Time expires student cannot submit answers even though he/she has already entered into the exam section.</li>
				<li>Timer shown on the exam screen is only indicative. Timer may restart on different ocasion; but actual time will be counted just after student sees the question for the first time.</li>
			</ul>
		</div>
		<div style="clear:both;height:20px"></div>
	<?php
        if(!isset($mydata) || empty($mydata))
			$mydata=false;
		
		if($mydata && $mydata->type == 'error'){ ?>
			<div style="display:table;height:100%;width:100%">
				<div style="display:table-row">
					<div style="display:table-cell;vertical-align:middle;text-align:center">
						<?=$mydata->message?>; 
					</div>
				</div>
			</div>
		<?php } else if($mydata && isset($mydata->response))
					{ 
						
						$edata = $mydata->response;	  
						$currenttime = array_pop($edata);
						$currenttime = $currenttime->current_timestamp;
		?>
					<?php if(count($edata)>0)
						{
						?>
							<h3 style="text-align:center"> You have following exams. </h3>
						<?php } ?>
					<table border="1" width="100%"   id="exam-table">	
						<?php if(count($edata)>0)
						{
						?>
						<tr> 
							<!-- <th> SN</th> -->
							<th> Exam Name</th>
							<!-- <th> Class </th> -->
							<th> Subject</th>
							<th> Start Date</th>
							<th> End Date</th>
							<th> Action</th>
						</tr>
					<?php $i=1;
						
							foreach($edata as $e):
								$schinfo = $e->scheduleinfo;
								$schinfo = element('0',json_decode($schinfo));
					?>
						<tr>
							<!-- <td><?php // echo $i++?></td> -->
							<td><?=$e->examtype?></td>
							<!-- <td><?php // echo $e->classname?></td> -->
							<td><?= ucfirst(strtolower($e->subjectname));?></td>
							<td><?=str_replace('T',' ',$schinfo->start_datetime)?></td>
							<td><?=str_replace('T',' ',$schinfo->end_datetime)?></td>
							<td>
							<?php if(strtotime($currenttime) < strtotime($schinfo->start_datetime)):
									echo "Come Again When Exam Starts";
								  elseif (strtotime($currenttime) > strtotime($schinfo->end_datetime)):
									echo "Exam Time is Over";
								  else: ?>
								<?=form_open("/exam/start_exam")?>
									<?=form_hidden("exam_id",$e->exam_id)?>
									<?=form_submit("Start","Start","class='btn btn-primary' data-examid='$e->exam_id'")?>
								<?=form_close()?>
							<?php endif; ?>
							</td>
							
						</tr>
					<?php 
							endforeach;
						}else{
					?>
						<tr><td colspan="7" align="middle" style="color:red"> You don't have any exams today. </td></tr>
					<?php
						}
					?>
					</table>
					<?php
						  } else{
							  
					?>	
						<div class="error" style="color:red"> Something Went Wrong </div>
					<?php	  }
					?>			
		
</div>
<script>
	$(function(){
		//alert("Hello");
	});
</script>
<style>
	#exam-table{ border-collapse:seperate !important; background:#f5f5f5; }
	#exam-table tr th, #exam-table tr td {
		
		  border:1px solid #fff; }
	#exam-table tr th { background: #c8c8fe; color:#333; font-weight:normal; }
</style>