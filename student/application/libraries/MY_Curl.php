<?php
 class MY_Curl extends CI_Curl
 { 
	function __construct()
	{
		parent::__construct();
		$this->ci=&get_instance();
		$this->heads=array();
		// $this->reset_headers();
	}
	
	function set_header($ky,$val)
	{
		$this->heads[$ky] = $val;
	}
	
	function get_curl_headers()
	{
		return $this->headers;
	}
	
	function remove_header($ky)
	{
		unset($this->heads[$ky]);
	}
	
	function reset_headers()
	{	
		//var_dump($this->ci->session);exit;
		if(!$this->ci->session->userdata('childid'))
		{
			$postdata=$_POST;
			if(!isset($postdata['Childid']))
				$postdata=$_GET;
	
			if(isset($postdata['Childid']))
			{
				$this->ci->session->set_userdata('eclassid',$postdata['classid']);
				$this->ci->session->set_userdata('studentid',$postdata['studentid']);
				$this->ci->session->set_userdata('childid',$postdata['Childid']);
				$this->ci->session->set_userdata('isparent',$postdata['Isparent']);
				$this->ci->session->set_userdata('mobileno',$postdata['Mobileno']);
				$this->ci->session->set_userdata('orgid',$postdata['Orgid']);
				$this->ci->session->set_userdata('studentcode',$postdata['Studentcode']);
				$this->ci->session->set_userdata('userid',$postdata['Userid']);
				$this->ci->session->set_userdata('sectionid',$postdata['sectionid']);
				$this->ci->session->set_userdata('teacher',$postdata['isteacher']);
				$this->ci->session->set_userdata('org',@$postdata['org']);
				$this->ci->session->set_userdata('district',@$postdata['district']);
				$this->ci->session->set_userdata('address',@$postdata['address']);
				$this->ci->session->set_userdata('ismobile','Y');
			}
			
		}
		 if($this->ci->session->userdata('teacher')=='Y')
			{
				$this->ci->session->set_userdata('userid','1001897910');
				$this->ci->session->set_userdata('childid','11000699997');
				$this->ci->session->set_userdata('orgid','10044344');
			}

		if(!$this->ci->session->userdata('userid'))
		{
				$this->ci->session->set_userdata('userid','1001897910');
				$this->ci->session->set_userdata('childid','11000699997');
				$this->ci->session->set_userdata('orgid','10044344');
		}
		
		$this->heads=array(
			'Apikey'=>'0cfbd2e2053e0d7eee49e642552cc94e',
			'Childid'=>$this->ci->session->userdata('childid'),
			'Isparent'=>$this->ci->session->userdata('isparent')?$this->ci->session->userdata('isparent'):'true',
			'Machinetype'=> 'web',
			'Mobileno'=> $this->ci->session->userdata('mobileno')?$this->ci->session->userdata('mobileno'):'9841532962',
			'Orgid'=>$this->ci->session->userdata('orgid'),
			'Studentcode'=> $this->ci->session->userdata('studentcode')?$this->ci->session->userdata('studentcode'):'s2t2b6r', 
			'Userid'=>$this->ci->session->userdata('userid'),
			'Classid'=>$this->ci->session->userdata('eclassid'),
			'Sectionid'=>$this->ci->session->userdata('sectionid'),
			'Isteacher'=>$this->ci->session->userdata('teacher'),
			'Isecademy'=>'Y',
			'Content-Type'=>'application/json'
		);
		//var_dump($this->heads);exit;
	}
	
	function post_json_raw($url,$data)
	{
		
		$fields_string = json_encode($data);
		
		$head = array();
		foreach($this->heads as $k=>$v)
		{
			$head[] = $k .':'. $v;
		}
		
		$this->headers = $head;
		
		$options=array(CURLOPT_SSL_VERIFYPEER => FALSE,
					   CURLOPT_SSL_VERIFYHOST => FALSE,
					   CURLOPT_FOLLOWLOCATION => FALSE,
					   CURLOPT_USERAGENT 	  => $this->ci->input->server('HTTP_USER_AGENT'),
					   CURLOPT_RETURNTRANSFER => TRUE
					   );
					   
		$this->create($url);			   
		$this->post($fields_string,$options);
		
		$response=$this->execute();
		
		echo curl_error($this->session);
				
		return $response;
	}
	
	function post_data($url,$data)
	{	
			 $this->reset_headers();

		
		$fields_string = http_build_query($data);
		
		$head=array();
		foreach($this->heads as $k=>$v)
		{
			if(trim(strtolower($k)) != 'content-type')
				$head[]= $k.':'.$v ;
		}
		// if($url=='https://myschool.midas.com.np/api/elearning/teacherend/studentApi/geteclass')
		// 	{
		// 		var_Dump($head);exit;
		// 	}
		$options=array(CURLOPT_SSL_VERIFYPEER => FALSE,
					   CURLOPT_SSL_VERIFYHOST => FALSE,
					   CURLOPT_FOLLOWLOCATION => FALSE,
					   CURLOPT_USERAGENT 	  => $this->ci->input->server('HTTP_USER_AGENT'),
					   CURLOPT_RETURNTRANSFER => TRUE,
					   CURLOPT_HTTPHEADER     => $head
					   );					   
		
		
		
		$this->create($url);			   
		$this->post($fields_string,$options);
		
		$response=$this->execute();
	
		//var_dump($response);exit;
		// var_dump($head);
		// var_dump($fields_string,$options);
		// exit;
		
		return $response;
	}
	
	function get_data($url,$data)
	{
		$this->reset_headers();

		$fields_string = urldecode(http_build_query($data));
		$url.="?$fields_string";
		
		$this->create($url);
		
		$head=array();
		foreach($this->headers as $k=>$v)
		{
			if(trim(strtolower($k)) != 'content-type')
				$head[]="$k: $v";
		}
		
		$options=array(CURLOPT_SSL_VERIFYPEER => FALSE,
					   CURLOPT_SSL_VERIFYHOST => FALSE,
					   CURLOPT_USERAGENT => $this->ci->input->server('HTTP_USER_AGENT'),
					   CURLOPT_RETURNTRANSFER => TRUE,
					   CURLOPT_HTTPHEADER     => $head
					);
	
		$this->options($options);
				
		$responseXML= $this->execute();
		
		return $responseXML;
	}
 }
?>