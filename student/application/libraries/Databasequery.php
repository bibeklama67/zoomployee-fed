<?php
class Databasequery{

	function __construct()
		{
				$this->ci = &get_instance();
			$this->db= $this->ci->load->database('default', TRUE);
		}	

function start_transaction($tran_name=false){
		$this->db->trans_start($tran_name);
	}	
	
	function check_status()
	{
		if ($this->db->trans_status() === FALSE)
			throw new Exception('Transaction Failed.');
	}
	
	function end_transaction()
	{
		$this->db->trans_complete();
	}
	
	function insert_all($table,$data)
	{
		if(!$this->db->insert_batch($table,$data))
			throw new Exception('Database Operation Error.');
	}

	
	function delete_data($table,$cond=false)
	{
		//if(!$this->session->userdata('user_id'))
			//throw new Exception('Session Expired');
		try{
			if(!$cond)
				throw new Exception('Batch Deletion Error');
			$this->db->delete($table,$cond);			
			return $this->db->affected_rows();
		 }catch(Exception $e)
			{
				throw $e;
			}
	}
	
	function save_data($table,$fields,$key=false)
	{			
		if($this->db->dbdriver != 'postgre')
		{
			if($key && !element($key,$fields))
				$fields[$key]=$this->getNextId($key);
		}		
		else
			unset($fields[$key]);
			
		try{
			  
				if($this->countdim($fields)>1)
				{
					$re=$this->db->insert_batch($table,$fields);
				}
				else
				{
					$re=$this->db->insert($table,$fields);
				}

				if(!$re)
					throw new Exception('Insertion Error');
				else if($this->db->dbdriver=='postgre')
					$fields[$key]=$this->db->insert_id();
			}catch(Exception $e)
				{
					throw $e;
				}	
		if($key)

			return $fields[$key];
		else
			return $this->db->affected_rows();			
	}
	
	function update_data($table,$fields,$cond)
	{
				
		if(!$cond)
				throw new Exception('Please Supply Update Condition');
		try{				
		  $upd=$this->db->update($table,$fields,$cond);
			  
			   if(!$upd)	
					throw new Exception('Error Updating Data');
			   return $this->db->affected_rows();
		}catch(Exception $e)
			{
				throw $e;
			}
	}
	
	function get_data($table,$cond=false,$limit=false,$offset=0,$order=false,$asc='ASC')
	{
		
		//if(!$this->session->userdata('user_id') && $table!='modules')
			//throw new Exception('Session Expired');
			
		if(!$table)
			throw new Exception('Please Specify Tablename');
		try{
			if($order) {	
				if(is_array($order)) {
					foreach($order as $okey => $oval) {
						$this->db->order_by($okey, $oval);
					}
					
				} else {
					$this->db->order_by($order,$asc);	
				}
			}
			if($cond)
			{
				if($limit){
						$res=$this->db->get_where($table,$cond,$limit,$offset);
						if($res->num_rows()>0)
							$res=$res->result();
						else
							$res=array();
				}else{
						$res=$this->db->get_where($table,$cond);
						//echo $this->db->last_query();
						if($res->num_rows()>0)
							$res=$res->result();
						else
							$res=array();
					}
			}else{				
					if($limit)
					{
						 $res=$this->db->get($table,$limit,$offset);
						 if($res->num_rows()>0)
							$res=$res->result();
						else
							$res=array();
					}else{
						   $res=$this->db->get($table);
						   if($res->num_rows()>0)
								$res=$res->result();
							else
								$res=array();
						}
				}
			}catch(Exception $e)
				{
					throw $e;
				}
		return $res;
	}

	function countdim($array)
	{
		if (is_array(reset($array)))
			$return = $this->countdim(reset($array)) + 1;
		else
			$return = 1;
		return $return;
	}

	public function getmaxbycolumn($table=false,$column=false,$cond=array())
    {
        // print_r($cond);
        $this->db->select("max($column) as max")->from($table)->where($cond);
        $query = $this->db->get();
        // echo $this->db->last_query();
        $max=  $query->first_row('array');
        return $max['max'];
    }
    
	 public function getminbycolumn($table=false,$column=false,$cond=array())
    {

        $this->db->select("min($column) as min")->from($table)->where($cond);
        $query = $this->db->get();
        $min=  $query->first_row('array');
        return $min['min'];
    }
    public function getmaxminbycolumn($table=false,$column=false,$cond=array())
    {

        $this->db->select("min($column) as min,max($column) as max")->from($table)->where($cond);
        $query = $this->db->get();
        $min=  $query->first_row('array');
        return $min;
    }
	
}