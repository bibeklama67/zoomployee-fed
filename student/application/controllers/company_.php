<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Company extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
		parent::__construct();
	}

	public function index()
	{
		
	}
	
	
	function privacy(){
	
	$id=$_GET['id'];

		//id = 0 'GENERAL'

		if($id == '0')
		{
				$this->load->view('privacy');
		}
		else if($id == '146')   // FOR MCVTC
		{
				$this->load->view('privacymcvtc');
		}
			else if($id == '106')   // FOR B & B
		{
				$this->load->view('privacybnb');
		}


	
	}

	function terms(){
			

			$id=$_GET['id'];

		//id = 0 'GENERAL'

		if($id == '0')
		{
				$this->load->view('terms');	
		}
		else if($id == '146')   // FOR MCVTC
		{
				$this->load->view('termsmcvtc');	
		}
			else if($id == '106')   // FOR B & B
		{
				$this->load->view('termsbnb');	
		}
	}



}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */