<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Quiz_Controller extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->db=$this->load->database('sims',TRUE);

		$bypass['bypass_controller'] = array('pages','design','forum','demoschool');
		$controller = $this->router->fetch_class();
		if(!in_array($controller, $bypass['bypass_controller']))
		{	
			
			 

		}
	
         
	}		

	function render_page($view=false) {

		if(!$view)
			$view = 'quiz_view';	 
		$this->load->view($view);
	}

	function render_demo_page($view=false) {

		if(!$view)
			$view = 'demo_view';	 
		$this->load->view($view);
	}
  		

}
