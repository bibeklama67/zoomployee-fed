<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class App_controller extends MY_Controller
{
	function __construct(){
		parent::__construct();
	}

	function render_page($view=false) {

		if(!$view)
			$view = 'app_view';	 
		$this->load->view($view);
	}
}
