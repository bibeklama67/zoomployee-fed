<?php
  if(!defined('BASEPATH')) exit('No direct script access allowed');

class Liveteacher_controller extends MY_Controller
{
	 function __construct()
	 {
		 parent::__construct();

		 if(!$this->session->userdata('isteacherloggedin') && $this->uri->segment('2')=='sessions'){
		 	
		 	$this->session->set_userdata('redirectToCurrent', current_url());
		 		redirect('liveteachers');	
		 }

		
	 }		

	  function render_page($view=false) {
			
			if(!$view)
			$view = 'front_view';	 
          $this->load->view($view);
  		}
  		// function load_login()
  		// {
  	
  		// 	$this->data['type']='login';
	   //  	$this->load->view('front_view', $this->data);
  		// }

}
