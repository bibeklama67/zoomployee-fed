<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Front_Controller extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->db=$this->load->database('sims',TRUE);
		$bypass['bypass_controller'] = array('pages','design','forum','demoschool','teacherend','quiz','updroutine','eclass');
		$controller = $this->router->fetch_class();

	
	

		$juniorclass_arr=array('38','2','3','4','5','6','7');
		if(isset($_GET['e']))
			$this->session->set_userdata('eclassid',$_GET['e']);
		if(in_array($this->session->userdata('eclassid'), $juniorclass_arr))
		{
			$this->session->set_userdata('loaddesign','new');
		}
		else
		{
		 $this->session->set_userdata('loaddesign','old');

		}
		
		// var_dump($bypass['bypass_controller'] );
		// var_dump($controller);
		// var_dump($this->router->fetch_class(),$this->router->fetch_method());exit;
	
		if(!in_array($controller, $bypass['bypass_controller']) && $this->session->userdata('teacher')!='Y')
		{		
			

			
			$currentuserid=$this->session->userdata('userid');
			$currentsessionid=$this->session->userdata('session_id');
			$previoussession=$this->db->order_by('usersessionid','desc')->get_where('usersessions',array('userid'=>$currentuserid,'sessionid'=>$currentsessionid))->row();
			if((!$this->session->userdata('isloggedin') || $previoussession->loggedouttime) && $this->session->userdata('isdemosession')!==TRUE){
				$this->session->sess_destroy();
				$this->session->set_userdata('redirectToCurrent', current_url());
				//echo base_url();exit;
				echo "<script>window.location.href = '".base_url()."login';</script>";
				// redirect('login');	
				exit;
			}
			 

		}
		
		
	}		

	function render_page($view=false) {

		if(!$view)
			$view = 'front_view';	 
		$this->load->view($view);
	}

	function render_demo_page($view=false) {

		if(!$view)
			$view = 'demo_view';	 
		$this->load->view($view);
	}
  		// function load_login()
  		// {

  		// 	$this->data['type']='login';
	   //  	$this->load->view('front_view', $this->data);
  		// }

}
