  <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

     if ( ! function_exists('asset_url()'))
       {
	       function asset_url()
	       {
	          return base_url().'assets/';
	       }
       }

       if ( ! function_exists('js_url()'))
        {
	       function js_url()
	       {
	          return base_url().'assets/js/';
	       }
        }
       if ( ! function_exists('css_url()'))
        {
	       function css_url()
	       {
	          return base_url().'assets/css/';
	       }
        }
       if ( ! function_exists('image_url()'))
        {
	       function image_url()
	       {
	          return base_url().'assets/images/';
	       }
        }
        if ( ! function_exists('videos_url()'))
        {
	       function video_url()
	       {
	          return base_url().'assets/videos/';
	       }
        }