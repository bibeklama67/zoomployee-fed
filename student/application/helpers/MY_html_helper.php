<?php
/**
  * Auto added to load js files *
  */
  
if ( ! function_exists('script_tag'))
{ 
	function script_tag($src = '', $lan = 'JavaScript')
	{
		$defer = '';		
		$CI =& get_instance();
		
//		echo '<pre>';
//		print_r($src);

		$link = '<script ';

		if (is_array($src))
		{
			foreach ($src as $k=>$v)
			{
				if ($k == 'src' AND strpos($v, '://') === FALSE)
				{
					
						$link .= 'src="'.$CI->config->slash_item('base_url').$v.'" ';
//								if($v !='assets/js/default/jquery-2.1.4.min.js' )
//									$defer = 'defer';
				
				}
				else
				{
					$link .= "$k=\"$v\" ";
				}
			}

			$link .= " ".$defer."></script>";
		}
		else
		{
			if ( strpos($src, '://') !== FALSE)
			{
				$link .= 'src="'.$src.'" ';
			}
			else
			{
				$link .= 'src="'.$CI->config->slash_item('base_url').$src.'" ';
			}

			/*$link .= 'language="'.$lan.'" ';*/

			$link .= '></script>';
		}


		return $link; 
	}
} 

if ( ! function_exists('object_to_array'))
{ 

	function object_to_array($obj_array=false,$idfield=false,$valfield=false,$activeonly=false,$blank=true,$humanize=true)
		{
			try{
										
				if(!($obj_array && is_array($obj_array) && $idfield ))
					throw new Exception('Object Could not be converted to array');	
							
				$ret_array=array();
				if($blank)
					$ret_array['']='-----------------';
				if($activeonly)	
					foreach($obj_array as $obj)
					{	
						if($obj->inactive=='f')
						{
							if(!$valfield)
								$ret_array[$obj->$idfield]=$obj;
							else if($valfield)
								$ret_array[$obj->$idfield]=$obj->$valfield;	
						}								
					}
				else	
					foreach($obj_array as $obj)
					{		
						if($valfield && $valfield!='nothing')
						   if($humanize)
								$value=humanize($obj->$valfield);
						   else	
						   		$value=$obj->$valfield;									
						if(!$valfield)
							$ret_array[$obj->$idfield]=$obj;
						else if($valfield=='nothing')
							$ret_array[]=$obj->$idfield;
						else
							$ret_array[$obj->$idfield]=$value;		
					}		
				return $ret_array;
				
			}catch(Exception $e)
				{
					return array('err'=>$e->getMessage());
				}	
		}
}	


