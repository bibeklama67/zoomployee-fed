<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


    function insertlog($data) {

    $CI = &get_instance();
    if($CI->input->post('orgid'))
    {
      $orgid=$CI->input->post('orgid');
    }else if($CI->input->get_request_header('Orgid', True) != null)
    {
      $orgid=$CI->input->get_request_header('Orgid', True);
    }else
    {
      $orgid=0;
    }
      $data['userid']= ($CI->input->get_request_header('Userid', True))?$CI->input->get_request_header('Userid', True) : 0;
      $data['mobileno']= ($CI->input->get_request_header('Mobileno', True))?$CI->input->get_request_header('Mobileno', True) : 0;
      $data['devicemodel']=$CI->input->get_request_header('Device', True);
      $data['deviceip']=$CI->input->get_request_header('Ipaddress', True);
      $data['deviceid']=$CI->input->get_request_header('Gcm', True);
      $data['macid']=$CI->input->get_request_header('Androidid', True);
      $data['imei']=$CI->input->get_request_header('Imei', True);
      $data['orgid']=$orgid;

      if($CI->input->get_request_header('Orgid', True))
      {
        
        $qry = $CI->db->where('orna_orgid', $orgid)
                          ->get('gb_orna_orgname');
  
          $org= $qry->row();
          $orgname= $org->orna_orgname;
        $data['appname']=$orgname.'APP';
      }else
      {
        $data['appname']='Mero Doctor APP';
      }
       
                   
                   
  $data['useddate']=date('Y/m/d');
   $data[ 'usedtime']=date('h:i:s');
   $data['posteddatetime']=date('Y/m/d h:i:s');
    if($CI->db->insert('useractivity_log',$data))
        return true;
    else
        return false;

    }

     function insertsmslog($data) {
    $CI = &get_instance();

     $data['createddatetime']=date('Y/m/d h:i:s');
    if($CI->db->insert('smssentlog',$data))
        return true;
    else
        return false;

    }

    