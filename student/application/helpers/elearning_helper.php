<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

function getTimeDuration($datetime)
{

    $date = new DateTime($datetime);

    $interval = $date->diff(new DateTime('now'));
    $returntext = false;
    if($interval->y)
    {
        if($interval->format('%y')==1)
            $suffix = '';
        else
            $suffix = 's';
        if(!$returntext)
            $returntext = $interval->format('%y year'.$suffix.' ago');
    }
    if($interval->m)
    {
     if($interval->format('%m')==1)
        $suffix = '';
    else
        $suffix = 's';
    if(!$returntext)
        $returntext = $interval->format('%m month'.$suffix.' ago');
}
if($interval->d)
{
 if($interval->format('%d')==1)
    $suffix = '';
else
    $suffix = 's';
if(!$returntext)
    $returntext = $interval->format('%d day'.$suffix.' ago');
}
if($interval->h)
{
 if($interval->format('%h')==1)
    $suffix = '';
else
    $suffix = 's';
if(!$returntext)
    $returntext = $interval->format('%h hour'.$suffix.' ago');
}
if($interval->i)
{
 if($interval->format('%i')==1)
    $suffix = '';
else
    $suffix = 's';
if(!$returntext)
    $returntext = $interval->format('%i minute'.$suffix.' ago');
}
if($interval->s)
{
 if($interval->format('%s')==1)
    $suffix = '';
else
    $suffix = 's';
if(!$returntext)
    $returntext = $interval->format('%s second'.$suffix.' ago');
}
if(!$returntext)
    $returntext = 'Just Now';

return $returntext;
}

// function getEmbedUrl($url,$source){             
//         //http://www.youtube.com/watch?v=YYpVUwUXvCY
//         //http://vimeo.com/31240369      
//     if($source=='youtube'){
//         $url = "http://www.youtube.com/watch?v=$url";
//     } 

//     if(isset($_SERVER['HTTPS']) && @$_SERVER['HTTPS'] == 'on')
//         $http = 'https';
//     else
//         $http = 'http';
//     $image_url = parse_url($url);
//     if(isset($image_url['host'])){
//         if($image_url['host'] == 'www.youtube.com' || $image_url['host'] == 'youtube.com'){
//             $array = explode("&", $image_url['query']);
//             return '<iframe src="https://www.youtube.com/embed/'.substr($array[0], 2).'"width="738" height="415" frameborder="0" allowfullscreen></iframe>';
//             return $http.'://www.youtube.com/embed/'.substr($array[0], 2);
//         } else if($image_url['host'] == 'www.youtu.be' || $image_url['host'] == 'youtu.be'){
//             $array = explode("/", $image_url['path']);
//             return '<iframe src="https://www.youtube.com/embed/'.$array[1].'"width="738" height="415" frameborder="0" allowfullscreen></iframe>';
//         } else if($image_url['host'] == 'www.vimeo.com' || $image_url['host'] == 'vimeo.com'){
//             $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/".substr($image_url['path'], 1).".php"));
//             return '<iframe src="//player.vimeo.com/video/'.substr($image_url['path'], 1).'" width="738" height="415" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
//         }else if($source == 'selfserver'){
//             return '<video width="738" height="415" autoplay><source src="'.$url.'" type="video/mp4"></video>';
//         }           
//     }
//     else
//     {
//         return '';  
//     }
// }

function getEmbedUrl($url){

        //http://www.youtube.com/watch?v=YYpVUwUXvCY
        //http://vimeo.com/31240369             
    if(isset($_SERVER['HTTPS']) && @$_SERVER['HTTPS'] == 'on')
        $http = 'https';
    else
        $http = 'http';
    $image_url = parse_url($url);
    if(isset($image_url['host'])){
        if($image_url['host'] == 'www.youtube.com' || $image_url['host'] == 'youtube.com'){
            $array = explode("&", $image_url['query']);
            return $http.'://www.youtube.com/embed/'.substr($array[0], 2);
                //return "http://img.youtube.com/vi/".substr($array[0], 2)."/0.jpg";
        } else if($image_url['host'] == 'www.youtu.be' || $image_url['host'] == 'youtu.be'){
            $array = explode("/", $image_url['path']);
            return $http.'://www.youtube.com/embed/'.$array[1];
                //return "http://img.youtube.com/vi/".$array[1]."/0.jpg";
        } else if($image_url['host'] == 'www.vimeo.com' || $image_url['host'] == 'vimeo.com'){
            $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/".substr($image_url['path'], 1).".php"));
            return $http.'://player.vimeo.com/video/'.substr($image_url['path'], 1);
                //return $hash[0]["thumbnail_large"];
        }  
            /*else if($image_url['host'] == 'www.facebook.com' || $image_url['host'] == 'facebook.com'){
                return "https://graph.facebook.com".substr($image_url['path'], 2)."/picture";
            }*/
        }
        else
        {
            return '';  
        }
        
}

?>