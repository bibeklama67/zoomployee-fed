<?php

class Migration_tbl_user_name extends CI_Migration {

    public function up() {
         $this->myforge  = $this->load->dbforge($this->load->database('local',true),true);
   
        $this->myforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            )
        ));
        $this->myforge->add_key('id', TRUE);
        $this->myforge->create_table('tbl_user_name');
    }

    public function down() {
        $this->myforge->drop_table('tbl_user_name');
    }

}