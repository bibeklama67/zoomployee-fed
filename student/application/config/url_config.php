<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//if( (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ) ||(isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')){
if (isset($_SERVER['HTTPS']) &&
    ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
    isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
    $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') 
	{
		$config['assets_url']='https://asstes.midas.com.np/';
		$config['api_url']='https://myschool.midas.com.np/';
		//$config['api_url']='https://midas.ecademy.cloud/';

		$config['base_url'] = $_SERVER['HTTP_X_FORWARDED_PROTO'].'://'.$_SERVER['HTTP_HOST'];
		//$config['base_url']='https://www.midasecademy.com/';
	}else{
		$config['assets_url']='http://asstes.midas.com.np/';
		$config['api_url']='http://myschool.midas.com.np/';
		//$config['api_url']='http://midas.ecademy.cloud/';

		$config['base_url'] = $_SERVER['HTTP_X_FORWARDED_PROTO'].'://'.$_SERVER['HTTP_HOST'];
	}
