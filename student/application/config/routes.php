<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

// $route['default_controller'] = 'dashboard/homework_help';
//$route['default_controller'] = 'dashboard/index';
$route['default_controller'] = 'home/index';
// $route['/'] = 'dashboard/index';
$route['BacKenD/(:any)'] = "backend/index";
$route['progress_old'] = "student-progress/progress_old";
$route['tutorials'] = "dashboard/tutorials";
$route['quizes'] = "dashboard/quizes";
$route['practice-exam'] = "dashboard/practiceexam";
$route['download-app'] = "pages/download_app";
$route['homework_help'] = "dashboard/homework_help";
$route['forgot-password'] = "signup/forgot_password";
$route['forgot-password/(:any)'] = "signup/forgot_password/$1";
$route['shops'] = "pages/shops";
$route['message/(:any)/(:any)'] = "pages/message/$1/$2";
$route['walkthrough'] = "pages/walkthrough";
$route['purchase/ime/confirm'] = "purchase/confirmIme";
$route['purchase/ime/verify'] = "purchase/verifyIme";
$route['live-class'] = "dashboard/liveclass";
$route['schoolsignup'] = "pages/schoolsignup";
$route['school'] = "pages/schoolsignup";
$route['complain'] = "pages/complain";
$route['support'] = "pages/support";
$route['ncell-faq'] = "pages/faq/ncell";
$route['faq/(:any)'] = "pages/faq/$1";
$route['faq'] = "pages/faq";
$route['api/ncell'] = "purchase/api/ncell";
$route['demoschool/videos'] = "pages/demoschool/videos";
$route['demoschool'] = "pages/demoschool";
$route['studentlogin'] = "login/desktop";
$route['testing'] = "quiztest/index";
$route['privacy'] = "company/privacy";
$route['terms'] = "company/terms";
$route['Parent-GeneralSurvey']="home/surveyform/1";
$route['GeneralStudentApplicationForm']="home/surveyform/2";
$route['StudentAppForm']="home/surveyform/2";
$route['Parent-RevisedSurvey']="home/surveyform/3";
$route['ApplicationForm']="home/surveyform/4";
$route['HigherEducationForm']="home/surveyform/5";
$route['TeacherApplication']="home/surveyform/6";
$route['DoctorApplicationForm']="home/surveyform/7";
$route['smsAppinstall'] = "home/surveyform/8";



// $route['frontend/'] = "frontend/index/$1";
// $route['products/(:any)'] = "frontend/products/$1";
// $route['downloadandroid/(:any)'] = "frontend/downloadandroid/$1";
// $route['order/(:any)'] = "frontend/orderform/$1";
// $route['downloads/(:any)'] = "frontend/downloadform/$1";
// $route['downloaddetails/(:any)'] = "frontend/downloaddetails/$1";

// $route['admin'] = 'backend';

// print_r($route);exit;

// FOR EACADEMY
$route['bookform'] = "home/bookform";
$route['thanks'] = "home/thanks";
$route['etour'] = "home/etour";
$route['apply'] = "home/apply";
$route['applicationform'] = "home/application/0";
$route['applicationform1'] = "home/application/1";
$route['applicationform2'] = "home/application/2";
$route['applicationform3'] = "home/application/3";
$route['applicationform4'] = "home/application/4";
$route['applicants'] = "home/applicants";
$route['faqtour'] = "home/faqtour/0";
$route['faqtour1'] = "home/faqtour/1";
$route['faqtour2'] = "home/faqtour/2";
$route['faqtour3'] = "home/faqtour/3";
$route['faqtour4'] = "home/faqtour/4";
$route['talktous'] = "home/joinconference";

//updated by lawa raj neupane
/*$route['aptitude-test-class'] = 'aptitude/aptitude_test_class/form';
$route['aptitude-test-class/(:any)'] = 'aptitude/aptitude_test_class/form/$1';
$route['aptitude-test-class-submit'] = 'aptitude/aptitude_test_class/aptitude_test_class_submit';
$route['aptitude-test-class-getsubject'] = 'aptitude/aptitude_test_class/aptitude_test_class_getsubject';
$route['aptitude-subject-url'] = 'aptitude/aptitude_test_class/aptitude_test_subject_urls';
*/

$route['404_override'] = '';
$route['translate_uri_dashes'] = TRUE;
