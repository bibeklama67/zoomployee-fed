<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/********************** CONFIGS FOR ADMIN TEMPALTE *****************************/

$config['default']='template';
$config['template']['title']='Midas-E-Learning';						
$config['template']['css']=array(); 
								
								
$config['template']['js']=array();
						  /*array('all/jquery',
								'all/loader.min',								
								'all/bootstrap-datetimepicker.min',
								'all/general',
								'all/jquery-ui',
								'all/jquery.form',
								'all/common');*/

/********************** CONFIGS FOR LOGIN TEMPALTE *****************************/

// $config['login']['css']=array('layout');
// $config['login']['pages']=array('main'=>'normal_login');
// $config['login']['title']='Login to PMS';
// $config['login']['loginTitle']=' Hospital Management ';


/********************** CONFIGS FOR USER TEMPALTE *****************************/

// $config['template']['pages']=array(
//  									'top'=>'topbar',
// 									'side'=>'sidebar',
// 									'main'=>'',
// 									'content'=>'',
// 									'foot'=>''
// 								 );
								 //footer'

/********************** CONFIGS COMMON DATA *****************************/

$config['common']['orgname']='MiDas';
$config['common']['copyright']='copyright &copy; 2016 Midas Pvt. Ltd.';
$config['common']['developer']='Sagar Chapagain';
$config['common']['js']=array('folder'=>array('default'));
$config['common']['css']=array('folder'=>array('default'));
$config['common']['meta']=array('viewport'=>'width=device-width, initial-scale=1.0');

/**
* ************************************************************************
*/

