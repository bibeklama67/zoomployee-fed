<?php include("partials/header.php");?>

<section class="welcome-banner position-relative">
    <img src="assets/img/welcome-bg.jpg" alt="">
    <div class="container d-flex h-100 align-items-center">
        <div class="row">
            <div class="col-xl-6 text-content">
                <span>Remote Staff
                    $7.00-$8.50/hour</span>
                <p class="text-white">We are the leading provider of remote admin, marketing, sales, bookkeeping,
                    design, customer support,
                    and e-commerce staff for small businesses.</p>
                <h6 class="text-white">No additional fees. Interview today. Cancel any time. </h6>
                <a href="#" class="btn btn-success">Get Started</a>
            </div>
        </div>
    </div>
</section>

<section class="home intro">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6 video-div">
                <img src="assets/img/home-intro.png" alt="">
            </div>
            <div class="col-md-5 text-content">
                <h4>We understand your needs, to save you money, make your staffing structure more flexible, and find
                    you the
                    best talent in the market.</h4>
                <p>We are one of the market leaders in recruiting top quality yet affordable remote staff for small
                    businesses, startup companies, private investors, and self-employed professionals. Our remote
                    professionals are based in the Philippines or Mexico, and are college-educated, fluent in English,
                    hard-working, friendly, and positive, and available for hire from $7 per hour. It will take only a
                    few days to hire an assistant or a specialist through us, and you can terminate your contract any
                    time by giving us a 2-weeks notice for individual positions and a 4-weeks notice for team hires.
                </p>
            </div>
        </div>
    </div>
</section>
<section class="home why-us">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 heading">
                <h4>Why Zoomployee?</h4>
                <h2>3 Great Reasons to Choose Zoomployee</h2>
            </div>
            <div class="col-11">
                <div class="row">
                    <div class="col-md-6 col-lg-4 feature-card-wrapper">
                        <div class="feature-card">
                            <div class="img-div">
                                <img src="assets/img/feature1.jpg" alt="">
                            </div>
                            <div class="text-content">
                                <div class="count">
                                    1
                                </div>
                                <h4>Affordable Solutions</h4>
                                <p>We offer one of the most affordable solutions in the market, starting from $7 per
                                    hour for an
                                    assistant and from $22 per hour for a 3-expert team hire. Our pricing structure is
                                    fully
                                    transparent and there are no hidden fees or charges.
                                </p>
                                <a href="#">
                                    Explore Our Hourly Rates
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4 feature-card-wrapper">
                        <div class="feature-card">
                            <div class="img-div">
                                <img src="assets/img/feature2.jpg" alt="">
                            </div>
                            <div class="text-content">
                                <div class="count">
                                    2
                                </div>
                                <h4>Top Quality Talent</h4>
                                <p>Our remote professionals are college-educated, multi-skilled, and fluent in English.
                                    They are
                                    experts in their fields and receive cultural sensitivity training before they join
                                    us. We
                                    pre-screen them for you and they are available immediately.
                                </p>
                                <a href="#">
                                    View Our Role Profiles
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4 feature-card-wrapper">
                        <div class="feature-card">
                            <div class="img-div">
                                <img src="assets/img/feature1.jpg" alt="">
                            </div>

                            <div class="text-content">
                                <div class="count">
                                    3
                                </div>
                                <h4>Excellent Support</h4>
                                <p>We are proud of our exceptionally high customer satisfaction rate and always put our
                                    customers first. To support you, we offer free business growth consultant advice and
                                    don’t
                                    ask you for your credit card details or a long-term commitment.
                                </p>
                                <a href="#">
                                    Find Out More About Us
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
<section class="home testimonial">
    <div class="container">
        <div class="row">
            <div class="owl-carousel owl-theme home-testimonial-carousel">
                <div class="item col-xl-7 mx-auto testimonial-card-wrapper">
                    <div class="text-content">
                        <p>“I found out about Zoomployee right when the pandemic occurred. It was really convenient for
                            expanding during the pandemic. It was extremely simple to sign up and it took less than five
                            minutes. Their representatives greeted me with respect and courtesy through all steps. They
                            always responded quickly. Their staffing solution helped us solve our limited office space
                            problem. My overall experience has been amazing.”</p>
                    </div>
                    <div class="name"> – Safe House Locksmith, Zoomployee Client</div>
                    <div class="rating d-flex justify-content-center">
                        <img src="assets/img/rating-star-lg.svg" alt="">
                        <img src="assets/img/rating-star-lg.svg" alt="">
                        <img src="assets/img/rating-star-lg.svg" alt="">
                        <img src="assets/img/rating-star-lg.svg" alt="">
                        <img src="assets/img/rating-star-lg.svg" alt="">
                    </div>
                </div>
                <div class="item col-xl-7 mx-auto testimonial-card-wrapper">
                    <div class="text-content">
                        <p>“I found out about Zoomployee right when the pandemic occurred. It was really convenient for
                            expanding during the pandemic. It was extremely simple to sign up and it took less than five
                            minutes. Their representatives greeted me with respect and courtesy through all steps. They
                            always responded quickly. Their staffing solution helped us solve our limited office space
                            problem. My overall experience has been amazing.”</p>
                    </div>
                    <div class="name"> – Safe House Locksmith, Zoomployee Client</div>
                    <div class="rating d-flex justify-content-center">
                        <img src="assets/img/rating-star-lg.svg" alt="">
                        <img src="assets/img/rating-star-lg.svg" alt="">
                        <img src="assets/img/rating-star-lg.svg" alt="">
                        <img src="assets/img/rating-star-lg.svg" alt="">
                        <img src="assets/img/rating-star-lg.svg" alt="">
                    </div>
                </div>
                <div class="item col-xl-7 mx-auto testimonial-card-wrapper">
                    <div class="text-content">
                        <p>“I found out about Zoomployee right when the pandemic occurred. It was really convenient for
                            expanding during the pandemic. It was extremely simple to sign up and it took less than five
                            minutes. Their representatives greeted me with respect and courtesy through all steps. They
                            always responded quickly. Their staffing solution helped us solve our limited office space
                            problem. My overall experience has been amazing.”</p>
                    </div>
                    <div class="name"> – Safe House Locksmith, Zoomployee Client</div>
                    <div class="rating d-flex justify-content-center">
                        <img src="assets/img/rating-star-lg.svg" alt="">
                        <img src="assets/img/rating-star-lg.svg" alt="">
                        <img src="assets/img/rating-star-lg.svg" alt="">
                        <img src="assets/img/rating-star-lg.svg" alt="">
                        <img src="assets/img/rating-star-lg.svg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="home experienced-staff">
    <div class="container">
        <div class="row">
            <div class="col-12 heading">
                <h2>Experienced Staff Available Immediately</h2>
                <h5>If you need to fill one of the roles below, we have pre-qualified candidates ready to be interviewed
                    today.
                </h5>
            </div>
            <div class="col-md-6 list-content text-content styled-list">
                <ul>
                    <li>
                        <h5> <svg width="12" height="20" viewBox="0 0 12 20" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path d="M2 2L10 10L2 18" stroke="url(#paint0_linear_30_197)" stroke-width="2.5"
                                    stroke-linecap="round" stroke-linejoin="round" />
                                <defs>
                                    <linearGradient id="paint0_linear_30_197" x1="6" y1="2" x2="6" y2="18"
                                        gradientUnits="userSpaceOnUse">
                                        <stop stop-color="#F03B7A" />
                                        <stop offset="1" stop-color="#7372C0" />
                                    </linearGradient>
                                </defs>
                            </svg>Badass Virtual Assistant</h5>
                        <span>$7.00/hour</span>
                    </li>
                    <li>
                        <h5> <svg width="12" height="20" viewBox="0 0 12 20" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path d="M2 2L10 10L2 18" stroke="url(#paint0_linear_30_197)" stroke-width="2.5"
                                    stroke-linecap="round" stroke-linejoin="round" />
                                <defs>
                                    <linearGradient id="paint0_linear_30_197" x1="6" y1="2" x2="6" y2="18"
                                        gradientUnits="userSpaceOnUse">
                                        <stop stop-color="#F03B7A" />
                                        <stop offset="1" stop-color="#7372C0" />
                                    </linearGradient>
                                </defs>
                            </svg>Social Media Superstar</h5>
                        <span>$7.00/hour</span>
                    </li>
                    <li>
                        <h5> <svg width="12" height="20" viewBox="0 0 12 20" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path d="M2 2L10 10L2 18" stroke="url(#paint0_linear_30_197)" stroke-width="2.5"
                                    stroke-linecap="round" stroke-linejoin="round" />
                                <defs>
                                    <linearGradient id="paint0_linear_30_197" x1="6" y1="2" x2="6" y2="18"
                                        gradientUnits="userSpaceOnUse">
                                        <stop stop-color="#F03B7A" />
                                        <stop offset="1" stop-color="#7372C0" />
                                    </linearGradient>
                                </defs>
                            </svg>Real Estate Assistant</h5>
                        <span>$7.00/hour</span>
                    </li>
                    <li>
                        <h5> <svg width="12" height="20" viewBox="0 0 12 20" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path d="M2 2L10 10L2 18" stroke="url(#paint0_linear_30_197)" stroke-width="2.5"
                                    stroke-linecap="round" stroke-linejoin="round" />
                                <defs>
                                    <linearGradient id="paint0_linear_30_197" x1="6" y1="2" x2="6" y2="18"
                                        gradientUnits="userSpaceOnUse">
                                        <stop stop-color="#F03B7A" />
                                        <stop offset="1" stop-color="#7372C0" />
                                    </linearGradient>
                                </defs>
                            </svg>Customer Service Virtuoso</h5>
                        <span>$7.00/hour</span>
                    </li>
                    <li>
                        <h5> <svg width="12" height="20" viewBox="0 0 12 20" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path d="M2 2L10 10L2 18" stroke="url(#paint0_linear_30_197)" stroke-width="2.5"
                                    stroke-linecap="round" stroke-linejoin="round" />
                                <defs>
                                    <linearGradient id="paint0_linear_30_197" x1="6" y1="2" x2="6" y2="18"
                                        gradientUnits="userSpaceOnUse">
                                        <stop stop-color="#F03B7A" />
                                        <stop offset="1" stop-color="#7372C0" />
                                    </linearGradient>
                                </defs>
                            </svg>Badass Virtual Assistant</h5>
                        <span>$7.00/hour</span>
                    </li>
                    <li>
                        <h5> <svg width="12" height="20" viewBox="0 0 12 20" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path d="M2 2L10 10L2 18" stroke="url(#paint0_linear_30_197)" stroke-width="2.5"
                                    stroke-linecap="round" stroke-linejoin="round" />
                                <defs>
                                    <linearGradient id="paint0_linear_30_197" x1="6" y1="2" x2="6" y2="18"
                                        gradientUnits="userSpaceOnUse">
                                        <stop stop-color="#F03B7A" />
                                        <stop offset="1" stop-color="#7372C0" />
                                    </linearGradient>
                                </defs>
                            </svg>Badass Virtual Assistant</h5>
                        <span>$7.00/hour</span>
                    </li>
                    <li>
                        <h5> <svg width="12" height="20" viewBox="0 0 12 20" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path d="M2 2L10 10L2 18" stroke="url(#paint0_linear_30_197)" stroke-width="2.5"
                                    stroke-linecap="round" stroke-linejoin="round" />
                                <defs>
                                    <linearGradient id="paint0_linear_30_197" x1="6" y1="2" x2="6" y2="18"
                                        gradientUnits="userSpaceOnUse">
                                        <stop stop-color="#F03B7A" />
                                        <stop offset="1" stop-color="#7372C0" />
                                    </linearGradient>
                                </defs>
                            </svg>Badass Virtual Assistant</h5>
                        <span>$7.00/hour</span>
                    </li>
                    <li>
                        <h5> <svg width="12" height="20" viewBox="0 0 12 20" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path d="M2 2L10 10L2 18" stroke="url(#paint0_linear_30_197)" stroke-width="2.5"
                                    stroke-linecap="round" stroke-linejoin="round" />
                                <defs>
                                    <linearGradient id="paint0_linear_30_197" x1="6" y1="2" x2="6" y2="18"
                                        gradientUnits="userSpaceOnUse">
                                        <stop stop-color="#F03B7A" />
                                        <stop offset="1" stop-color="#7372C0" />
                                    </linearGradient>
                                </defs>
                            </svg>Badass Virtual Assistant</h5>
                        <span>$7.00/hour</span>
                    </li>
                    <li>
                        <h5> <svg width="12" height="20" viewBox="0 0 12 20" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path d="M2 2L10 10L2 18" stroke="url(#paint0_linear_30_197)" stroke-width="2.5"
                                    stroke-linecap="round" stroke-linejoin="round" />
                                <defs>
                                    <linearGradient id="paint0_linear_30_197" x1="6" y1="2" x2="6" y2="18"
                                        gradientUnits="userSpaceOnUse">
                                        <stop stop-color="#F03B7A" />
                                        <stop offset="1" stop-color="#7372C0" />
                                    </linearGradient>
                                </defs>
                            </svg>Badass Virtual Assistant</h5>
                        <span>$7.00/hour</span>
                    </li>
                    <li>
                        <h5> <svg width="12" height="20" viewBox="0 0 12 20" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path d="M2 2L10 10L2 18" stroke="url(#paint0_linear_30_197)" stroke-width="2.5"
                                    stroke-linecap="round" stroke-linejoin="round" />
                                <defs>
                                    <linearGradient id="paint0_linear_30_197" x1="6" y1="2" x2="6" y2="18"
                                        gradientUnits="userSpaceOnUse">
                                        <stop stop-color="#F03B7A" />
                                        <stop offset="1" stop-color="#7372C0" />
                                    </linearGradient>
                                </defs>
                            </svg>Badass Virtual Assistant</h5>
                        <span>$7.00/hour</span>
                    </li>
                    <li>
                        <h5> <svg width="12" height="20" viewBox="0 0 12 20" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path d="M2 2L10 10L2 18" stroke="url(#paint0_linear_30_197)" stroke-width="2.5"
                                    stroke-linecap="round" stroke-linejoin="round" />
                                <defs>
                                    <linearGradient id="paint0_linear_30_197" x1="6" y1="2" x2="6" y2="18"
                                        gradientUnits="userSpaceOnUse">
                                        <stop stop-color="#F03B7A" />
                                        <stop offset="1" stop-color="#7372C0" />
                                    </linearGradient>
                                </defs>
                            </svg>Badass Virtual Assistant</h5>
                        <span>$7.00/hour</span>
                    </li>
                    <li>
                        <h5> <svg width="12" height="20" viewBox="0 0 12 20" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path d="M2 2L10 10L2 18" stroke="url(#paint0_linear_30_197)" stroke-width="2.5"
                                    stroke-linecap="round" stroke-linejoin="round" />
                                <defs>
                                    <linearGradient id="paint0_linear_30_197" x1="6" y1="2" x2="6" y2="18"
                                        gradientUnits="userSpaceOnUse">
                                        <stop stop-color="#F03B7A" />
                                        <stop offset="1" stop-color="#7372C0" />
                                    </linearGradient>
                                </defs>
                            </svg>Badass Virtual Assistant</h5>
                        <span>$7.00/hour</span>
                    </li>
                </ul>
            </div>
            <div class="col-md-6 img-div">
                <img src="assets/img/experienced-staff.jpg" alt="">
            </div>
        </div>
        <div class="row reviews">
            <div class="col-md-4 review-card-wrapper">
                <div class="review-card">
                    <div class="info">
                        <div class="d-flex">
                            <div class="img-div">
                                <span>M</span>
                            </div>
                            <h6 class="name">Mariana Tirado <span>December 09,2020</span></h6>
                        </div>
                        <div class="logo"><img src="assets/img/google-logo.png" alt=""></div>
                    </div>
                    <div class="rating">
                        <img src="assets/img/rating-star.svg" alt="">
                        <img src="assets/img/rating-star.svg" alt="">
                        <img src="assets/img/rating-star.svg" alt="">
                        <img src="assets/img/rating-star.svg" alt="">
                        <img src="assets/img/rating-star.svg" alt="">
                        <img class="verified" src="assets/img/rating-verified.svg" alt="">
                    </div>
                    <div class="text-content">
                        <p>Adam and his team are truly top of the line! I needed to hire someone qualified in a hurry
                            and Adam and his team had two qualified candidates in less than 24 hours. </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 review-card-wrapper">
                <div class="review-card">
                    <div class="info">
                        <div class="d-flex">
                            <div class="img-div">
                                <span>M</span>
                            </div>
                            <h6 class="name">Mariana Tirado <span>December 09,2020</span></h6>
                        </div>
                        <div class="logo"><img src="assets/img/google-logo.png" alt=""></div>
                    </div>
                    <div class="rating">
                        <img src="assets/img/rating-star.svg" alt="">
                        <img src="assets/img/rating-star.svg" alt="">
                        <img src="assets/img/rating-star.svg" alt="">
                        <img src="assets/img/rating-star.svg" alt="">
                        <img src="assets/img/rating-star.svg" alt="">
                        <img class="verified" src="assets/img/rating-verified.svg" alt="">
                    </div>
                    <div class="text-content">
                        <p>Adam and his team are truly top of the line! I needed to hire someone qualified in a hurry
                            and Adam and his team had two qualified candidates in less than 24 hours. </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 review-card-wrapper">
                <div class="review-card">
                    <div class="info">
                        <div class="d-flex">
                            <div class="img-div">
                                <span>M</span>
                            </div>
                            <h6 class="name">Mariana Tirado <span>December 09,2020</span></h6>
                        </div>
                        <div class="logo"><img src="assets/img/google-logo.png" alt=""></div>
                    </div>
                    <div class="rating">
                        <img src="assets/img/rating-star.svg" alt="">
                        <img src="assets/img/rating-star.svg" alt="">
                        <img src="assets/img/rating-star.svg" alt="">
                        <img src="assets/img/rating-star.svg" alt="">
                        <img src="assets/img/rating-star.svg" alt="">
                        <img class="verified" src="assets/img/rating-verified.svg" alt="">
                    </div>
                    <div class="text-content">
                        <p>Adam and his team are truly top of the line! I needed to hire someone qualified in a hurry
                            and Adam and his team had two qualified candidates in less than 24 hours. </p>
                    </div>
                </div>
            </div>
            <div class="col-12 text-center review-info">
                <strong>Google</strong> rating score: <strong>5</strong> of 5, based on <strong>13</strong> reviews.
            </div>
        </div>
    </div>
</section>

<section class="home terms-and-conditions position-relative">
    <img src="assets/img/tc-vector1.png" alt="" class="tc-vector1">
    <img src="assets/img/tc-vector2.png" alt="" class="tc-vector2">
    <img src="assets/img/tc-vector3.png" alt="" class="tc-vector3">
    <img src="assets/img/tc-vector4.png" alt="" class="tc-vector4">
    <img src="assets/img/tc-star1.png" alt="" class="tc-vector5">
    <img src="assets/img/tc-star2.png" alt="" class="tc-vector6">
    <img src="assets/img/tc-star3.png" alt="" class="tc-vector7">
    <div class="container position-relative">
        <div class="row">
            <div class="col-md-6 text-content">
                <h2>Our Terms & Conditions
                </h2>
                <h6>We will send you a formal contract for your review and signature when you are ready hire, but here
                    is a quick overview of how it works:</h6>
                <ul>
                    <li> You interview & hire a remote assistant for 50 hours/week;</li>
                    <li> They work with you directly during your business hours;</li>
                    <li> Your dedicated specialist will work exclusively for you;</li>
                    <li>First week is your trial week, you can cancel any time;</li>
                    <li>After the first week, cancel by giving a 2-weeks notice;</li>
                    <li>The same hourly rate applies on weekends & holidays;</li>
                    <li>We never charge any other fees, only the hourly rate;</li>
                    <li>You will be invoiced weekly, we auto-charge your card;</li>
                    <li>We ask you for a refundable security deposit (2-weeks);</li>
                    <li>Treat our invoice as a business expense (no 1099 needed).</li>
                </ul>
                <a href="#" class="btn btn-success"><svg width="37" height="37" viewBox="0 0 37 37" fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd"
                            d="M31.025 5.24688C27.6031 1.825 23.0406 0 18.25 0C8.2125 0 0 8.2125 0 18.25C0 21.4438 0.912514 24.6375 2.50939 27.375L0 36.5L9.58128 33.9906C12.3188 35.3594 15.2844 36.2719 18.25 36.2719C28.2875 36.2719 36.5 28.0594 36.5 18.0219C36.5 13.2312 34.4469 8.66875 31.025 5.24688ZM18.25 33.3063C15.5125 33.3063 12.775 32.6219 10.4938 31.2531L10.0375 31.025L4.33436 32.6219L5.93126 27.1469L5.47499 26.4625C3.87811 23.9531 3.19374 21.2156 3.19374 18.4781C3.19374 10.2656 10.0375 3.42188 18.25 3.42188C22.3563 3.42188 26.0063 5.01875 28.9719 7.75625C31.9375 10.7219 33.3063 14.3719 33.3063 18.4781C33.3063 26.4625 26.6906 33.3063 18.25 33.3063ZM26.4625 21.9C26.0063 21.6719 23.725 20.5312 23.2688 20.5312C22.8125 20.3031 22.5843 20.3031 22.3562 20.7594C22.1281 21.2156 21.2157 22.1281 20.9875 22.5844C20.7594 22.8125 20.5312 22.8125 20.075 22.8125C19.6187 22.5844 18.25 22.1281 16.425 20.5312C15.0563 19.3906 14.1438 17.7938 13.9156 17.3375C13.6875 16.8813 13.9157 16.6531 14.1438 16.425C14.3719 16.1969 14.6 15.9687 14.8281 15.7406C15.0562 15.5125 15.0563 15.2844 15.2844 15.0563C15.5125 14.8281 15.2844 14.6 15.2844 14.3719C15.2844 14.1438 14.3719 11.8625 13.9156 10.95C13.6875 10.2656 13.2313 10.2656 13.0032 10.2656C12.775 10.2656 12.5468 10.2656 12.0906 10.2656C11.8625 10.2656 11.4062 10.2656 10.95 10.7219C10.4937 11.1781 9.35314 12.3187 9.35314 14.6C9.35314 16.8812 10.95 18.9344 11.1781 19.3906C11.4062 19.6187 14.3718 24.4094 18.9343 26.2344C22.8125 27.8312 23.4969 27.375 24.4094 27.375C25.3219 27.375 27.1469 26.2344 27.375 25.3219C27.8312 24.1813 27.8313 23.2688 27.6031 23.2688C27.375 22.1281 26.9188 22.1281 26.4625 21.9Z"
                            fill="white" />
                    </svg>
                    WhatsApp Us</a>
            </div>
            <div class="col-md-6 img-div">
                <img src="assets/img/terms-and-conditions.jpg" alt="">
            </div>
        </div>
    </div>
</section>

<section class="home blogs">
    <div class="container">
        <div class="row">
            <div class="col-12 heading text-center">
                <h2>The Latest from Our Blog</h2>
            </div>
        </div>
        <div class="row blog-card-wrapper">
            <div class="col-xl-4 col-md-6 blog-card-wrapper">
                <div class="blog-card">
                    <h6>Monitoring Productivity of Your Remote Staff</h6>
                    <p>One of the biggest challenges of working with staff who work from home is monitoring their
                        productivity and performance.
                    </p>
                    <div class="d-flex justify-content-between">
                        <a href="#">View Detail</a>
                        <span>December 4,2021</span>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 blog-card-wrapper">
                <div class="blog-card">
                    <h6>Monitoring Productivity of Your Remote Staff</h6>
                    <p>One of the biggest challenges of working with staff who work from home is monitoring their
                        productivity and performance.
                    </p>
                    <div class="d-flex justify-content-between">
                        <a href="#">View Detail</a>
                        <span>December 4,2021</span>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 blog-card-wrapper">
                <div class="blog-card">
                    <h6>Monitoring Productivity of Your Remote Staff</h6>
                    <p>One of the biggest challenges of working with staff who work from home is monitoring their
                        productivity and performance.
                    </p>
                    <div class="d-flex justify-content-between">
                        <a href="#">View Detail</a>
                        <span>December 4,2021</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include("partials/footer.php");?>