<?php include("partials/header.php")?>
<section class="package-detail light-bg-primary">
    <div class="container">
        <div class="row heading">
            <div class="col-md-9 text-content">
                <h2>Hunting Deers in the Chitwan’s Jungle</h2>
                <span>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat
                    duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet. </span>
            </div>
            <div class="col-md-3 actions text-center">
                <a href="#" class="btn btn-primary text-white">Book this trip !</a>
                <div>
                    <ul class="list-unstyled d-flex mb-0 justify-content-center">
                        <li><a href="https://www.facebook.com/DANUBEHOMEBYBRIHATGROUP/" target="_blank"><i
                                    class="fab fa-facebook-f"></i> </a></li>
                        <li><a href="https://instagram.com/danubehomenepal" target="_blank"><i
                                    class="fab fa-instagram"></i> </a></li>
                        <li><a href="https://twitter.com/danubehome?lang=en" target="_blank"><i
                                    class="fab fa-twitter"></i> </a></li>

                        <li><a href="https://www.linkedin.com/company/danube-home" target="_blank"><i
                                    class="fab fa-linkedin-in"></i> </a></li>

                        <li><a href="https://www.youtube.com/channel/UCp2dYWlDaAq0UVm2VI5OFdw" target="_blank"><i
                                    class="fab fa-youtube"></i> </a></li>

                        <li><a href="https://www.snapchat.com/add/danubehome" target="_blank"><i
                                    class="fab fa-snapchat-ghost"></i> </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid banner-img p-0">
        <img src="assets/img/package-detail.png" alt="">
    </div>
    <div class="detail-content-wrapper container">
        <div class="row">
            <div class="col-xl-4 sidebar">
                <div class="content-wrapper">
                    <ul>
                        <li class="active"><a href="#overview">Overview</a></li>
                        <li><a href="#introduction">Introduction</a></li>
                        <li><a href="#itinerary">Itinerary</a></li>
                        <li><a href="#includes">Price Includes</a></li>
                        <li><a href="#excludes">Price Excludes</a></li>
                        <li><a href="#additional-information">Additional Information</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-xl-8 detail-content">
                <div class="nav-content overview row mb-80" id="overview">
                    <div class="col-12 heading">
                        <h2>Overview</h2>
                        <span>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia
                            consequat duis enim velit mollit. Exercitation veniam.</span>
                    </div>
                    <div class="col-xl-3 col-md-4 col-sm-6 feature-card">
                        <div class="img-div">
                            <img src="assets/img/duration-icon.svg" alt="">
                        </div>
                        <div class="text-content">
                            <span class="title">Duration</span>
                            <span class="value">10 Days</span>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-4 col-sm-6 feature-card">
                        <div class="img-div">
                            <img src="assets/img/area-icon.svg" alt="">
                        </div>
                        <div class="text-content">
                            <span class="title">Hunting Area</span>
                            <span class="value">Chitwan</span>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-4 col-sm-6 feature-card">
                        <div class="img-div">
                            <img src="assets/img/animal-icon.svg" alt="">
                        </div>
                        <div class="text-content">
                            <span class="title">Animal</span>
                            <span class="value">Deer</span>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-4 col-sm-6 feature-card">
                        <div class="img-div">
                            <img src="assets/img/altitude-icon.svg" alt="">
                        </div>
                        <div class="text-content">
                            <span class="title">Altitute</span>
                            <span class="value">500 Km</span>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-4 col-sm-6 feature-card">
                        <div class="img-div">
                            <img src="assets/img/level-icon.svg" alt="">
                        </div>
                        <div class="text-content">
                            <span class="title">Level</span>
                            <span class="value">Medium</span>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-4 col-sm-6 feature-card">
                        <div class="img-div">
                            <img src="assets/img/seasons-icon.svg" alt="">
                        </div>
                        <div class="text-content">
                            <span class="title">Seasons</span>
                            <span class="value">All Month</span>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-4 col-sm-6 feature-card">
                        <div class="img-div">
                            <img src="assets/img/transport-icon.svg" alt="">
                        </div>
                        <div class="text-content">
                            <span class="title">Transport</span>
                            <span class="value">Zeep</span>
                        </div>
                    </div>
                </div>
                <div class="nav-content introduction row mb-80" id="introduction">
                    <div class="col-12 heading">
                        <h2>Introduction</h2>
                    </div>
                    <div class="text-content expandable-content mh-270">
                        <p>Nunc sed augue lacus viverra vitae congue eu consequat ac. Proin fermentum leo vel orci
                            porta. Non sodales neque sodales ut etiam. Pharetra magna ac placerat vestibulum lectus
                            mauris. Feugiat sed lectus vestibulum mattis ullamcorper. In fermentum posuere urna nec
                            tincidunt praesent semper feugiat nibh. Volutpat sed cras ornare arcu. Auctor eu augue ut
                            lectus. Viverra nibh cras pulvinar mattis nunc. Morbi leo urna molestie at. Lacus
                            suspendisse faucibus interdum posuere lorem. Eget dolor morbi non arcu risus quis varius
                            quam quisque. Senectus et netus et malesuada fames ac turpis egestas sed. Eu volutpat odio
                            facilisis mauris sit amet. Morbi tempus iaculis urna id volutpat lacus laoreet. At in tellus
                            integer feugiat scelerisque varius. Magna ac placerat vestibulum lectus. Sed euismod nisi
                            porta lorem mollis aliquam.</p>
                        <p>Nunc sed augue lacus viverra vitae congue eu consequat ac. Proin fermentum leo vel orci
                            porta. Non sodales neque sodales ut etiam. Pharetra magna ac placerat vestibulum lectus
                            mauris. Feugiat sed lectus vestibulum mattis ullamcorper. In fermentum posuere urna nec
                            tincidunt praesent semper feugiat nibh. Volutpat sed cras ornare arcu. Auctor eu augue ut
                            lectus. Viverra nibh cras pulvinar mattis nunc. Morbi leo urna molestie at. Lacus
                            suspendisse faucibus interdum posuere lorem. Eget dolor morbi non arcu risus quis varius
                            quam quisque. Senectus et netus et malesuada fames ac turpis egestas sed. Eu volutpat odio
                            facilisis mauris sit amet. Morbi tempus iaculis urna id volutpat lacus laoreet. At in tellus
                            integer feugiat scelerisque varius. Magna ac placerat vestibulum lectus. Sed euismod nisi
                            porta lorem mollis aliquam.</p>
                    </div>
                    <div class="read-more-btn">
                        Show More
                    </div>
                </div>
                <div class="nav-content row itinerary mb-80" id="itinerary">
                    <div class="col-12 heading">
                        <h2>Itinerary</h2>
                    </div>
                    <div class="col-12">
                        <div class="accordion" id="accordionExample">
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingOne">
                                    <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Mea aeterno eleifend antiopam ad nam no
                                    </button>
                                </h2>
                                <div id="collapseOne" class="accordion-collapse collapse show"
                                    aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                        incididunt ut
                                        labore et dolore magna aliqua. Ut eu sem integer vitae justo eget magna
                                        fermentum iaculis.
                                        Enim blandit volutpat maecenas volutpat blandit aliquam. In fermentum et
                                        sollicitudin ac
                                        orci phasellus egestas. Ut faucibus pulvinar elementum integer enim neque
                                        volutpat ac
                                        tincidunt. Habitant morbi tristique senectus et netus et malesuada fames ac.
                                        Massa ultricies
                                        mi quis hendrerit dolor magna eget est lorem. Habitant morbi tristique senectus
                                        et netus et
                                        malesuada fames. Aliquam id diam maecenas ultricies mi eget mauris pharetra et.
                                        Interdum
                                        velit euismod in pellentesque.
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingTwo">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Amet minim mollit non deserunt ullamco est
                                    </button>
                                </h2>
                                <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo"
                                    data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <strong>This is the second item's accordion body.</strong> It is hidden by
                                        default, until
                                        the collapse plugin adds the appropriate classes that we use to style each
                                        element. These
                                        classes control the overall appearance, as well as the showing and hiding via
                                        CSS
                                        transitions. You can modify any of this with custom CSS or overriding our
                                        default variables.
                                        It's also worth noting that just about any HTML can go within the
                                        <code>.accordion-body</code>, though the transition does limit overflow.
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingThree">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#collapseThree" aria-expanded="false"
                                        aria-controls="collapseThree">
                                        Sit aliqua dolor do amet sint
                                    </button>
                                </h2>
                                <div id="collapseThree" class="accordion-collapse collapse"
                                    aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <strong>This is the third item's accordion body.</strong> It is hidden by
                                        default, until the
                                        collapse plugin adds the appropriate classes that we use to style each element.
                                        These
                                        classes control the overall appearance, as well as the showing and hiding via
                                        CSS
                                        transitions. You can modify any of this with custom CSS or overriding our
                                        default variables.
                                        It's also worth noting that just about any HTML can go within the
                                        <code>.accordion-body</code>, though the transition does limit overflow.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="nav-content row includes mb-80" id="includes">
                    <div class="col-12 heading">
                        <h2>Price Includes</h2>
                    </div>
                    <div class="col-12 list-content">
                        <ul>
                            <li>
                                Nunc sed augue lacus viverra vitae congue eu consequat ac.
                            </li>
                            <li>
                                Proin fermentum leo vel orci porta. Non sodales neque sodales ut etiam.
                            </li>
                            <li>
                                Pharetra magna ac placerat vestibulum lectus mauris.
                            </li>
                            <li>
                                Feugiat sed lectus vestibulum mattis ullamcorper. In fermentum posuere urna nec
                                tincidunt
                                praesent semper feugiat nibh. Volutpat sed cras ornare arcu.
                            </li>
                            <li>
                                Auctor eu augue ut lectus. Viverra nibh cras pulvinar mattis nunc. Morbi leo urna
                                molestie
                                at. Lacus suspendisse faucibus interdum posuere lorem.
                            </li>
                            <li>
                                Eget dolor morbi non arcu risus quis varius quam quisque. Senectus et netus et malesuada
                                fames ac turpis egestas sed.</li>
                        </ul>
                    </div>
                </div>
                <div class="nav-content row excludes mb-80" id="excludes">
                    <div class="col-12 heading">
                        <h2>Price Excludes</h2>
                    </div>
                    <div class="col-12 list-content">
                        <ul>
                            <li>
                                Nunc sed augue lacus viverra vitae congue eu consequat ac.
                            </li>
                            <li>
                                Proin fermentum leo vel orci porta. Non sodales neque sodales ut etiam.
                            </li>
                            <li>
                                Pharetra magna ac placerat vestibulum lectus mauris.
                            </li>
                            <li>
                                Feugiat sed lectus vestibulum mattis ullamcorper. In fermentum posuere urna nec
                                tincidunt
                                praesent semper feugiat nibh. Volutpat sed cras ornare arcu.
                            </li>
                            <li>
                                Auctor eu augue ut lectus. Viverra nibh cras pulvinar mattis nunc. Morbi leo urna
                                molestie
                                at. Lacus suspendisse faucibus interdum posuere lorem.
                            </li>
                            <li>
                                Eget dolor morbi non arcu risus quis varius quam quisque. Senectus et netus et malesuada
                                fames ac turpis egestas sed.</li>
                        </ul>
                    </div>
                </div>
                <div class="nav-content introduction row mb-80" id="additional-information">
                    <div class="col-12 heading">
                        <h2>Additional Information</h2>
                    </div>
                    <div class="text-content">
                        <p>Nunc sed augue lacus viverra vitae congue eu consequat ac. Proin fermentum leo vel orci
                            porta. Non sodales neque sodales ut etiam. Pharetra magna ac placerat vestibulum lectus
                            mauris. Feugiat sed lectus vestibulum mattis ullamcorper. In fermentum posuere urna nec
                            tincidunt praesent semper feugiat nibh. Volutpat sed cras ornare arcu. Auctor eu augue ut
                            lectus. Viverra nibh cras pulvinar mattis nunc. Morbi leo urna molestie at. Lacus
                            suspendisse faucibus interdum posuere lorem. Eget dolor morbi non arcu risus quis varius
                            quam quisque. Senectus et netus et malesuada fames ac turpis egestas sed. Eu volutpat odio
                            facilisis mauris sit amet. Morbi tempus iaculis urna id volutpat lacus laoreet. At in tellus
                            integer feugiat scelerisque varius. Magna ac placerat vestibulum lectus. Sed euismod nisi
                            porta lorem mollis aliquam.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="home testimonials pt-0">
    <div class="container">
        <div class="row">
            <div class="owl-theme owl-carousel testimonial-carousel">
                <div class="item testimonial-card">
                    <div class="details">
                        <div class="img-div">
                            <img src="assets/img/testimonial-img.png" alt="">
                        </div>
                        <div class="info">
                            <span>Theresa Webb</span>
                            <span>Australia</span>
                        </div>
                    </div>
                    <div class="text-content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua. Nec nam aliquam sem et tortor consequat id porta nibh. Neque
                            sodales ut etiam sit amet nisl. Elementum sagittis vitae et leo duis ut diam quam nulla.
                            Magna fermentum iaculis eu non diam phasellus vestibulum. Id aliquet risus feugiat in ante
                            metus dictum at tempor. Ipsum dolor sit amet consectetur adipiscing. Urna neque viverra
                            justo nec ultrices. Aliquet eget sit amet tellus. Vulputate enim nulla aliquet porttitor
                            lacus luctus. Diam maecenas ultricies mi eget. Velit </p>
                    </div>
                </div>
                <div class="item testimonial-card">
                    <div class="details">
                        <div class="img-div">
                            <img src="assets/img/testimonial-img.png" alt="">
                        </div>
                        <div class="info">
                            <span>Theresa Webb</span>
                            <span>Australia</span>
                        </div>
                    </div>
                    <div class="text-content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua. Nec nam aliquam sem et tortor consequat id porta nibh. Neque
                            sodales ut etiam sit amet nisl. Elementum sagittis vitae et leo duis ut diam quam nulla.
                            Magna fermentum iaculis eu non diam phasellus vestibulum. Id aliquet risus feugiat in ante
                            metus dictum at tempor. Ipsum dolor sit amet consectetur adipiscing. Urna neque viverra
                            justo nec ultrices. Aliquet eget sit amet tellus. Vulputate enim nulla aliquet porttitor
                            lacus luctus. Diam maecenas ultricies mi eget. Velit </p>
                    </div>
                </div>
                <div class="item testimonial-card">
                    <div class="details">
                        <div class="img-div">
                            <img src="assets/img/testimonial-img.png" alt="">
                        </div>
                        <div class="info">
                            <span>Theresa Webb</span>
                            <span>Australia</span>
                        </div>
                    </div>
                    <div class="text-content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua. Nec nam aliquam sem et tortor consequat id porta nibh. Neque
                            sodales ut etiam sit amet nisl. Elementum sagittis vitae et leo duis ut diam quam nulla.
                            Magna fermentum iaculis eu non diam phasellus vestibulum. Id aliquet risus feugiat in ante
                            metus dictum at tempor. Ipsum dolor sit amet consectetur adipiscing. Urna neque viverra
                            justo nec ultrices. Aliquet eget sit amet tellus. Vulputate enim nulla aliquet porttitor
                            lacus luctus. Diam maecenas ultricies mi eget. Velit </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="gallery-section">
    <div class="container">
        <div class="row">
            <div class="col-12 heading">
                <h2>
                    Recent Images of this Trip
                </h2>
            </div>
        </div>

    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 px-0 owl-theme owl-carousel gallery-carousel" id="lightgallery">
                <div class="item gallery-card-wrapper" data-category="csw" data-src="assets/img/gallery-1.png"
                    data-sub-html="">
                    <img src="assets/img/gallery-1.png" alt="">
                </div>
                <div class="item gallery-card-wrapper" data-category="csw" data-src="assets/img/gallery-2.png"
                    data-sub-html="">
                    <img src="assets/img/gallery-2.png" alt="">
                </div>
                <div class="item gallery-card-wrapper" data-category="csw" data-src="assets/img/gallery-3.png"
                    data-sub-html="">
                    <img src="assets/img/gallery-3.png" alt="">
                </div>
                <div class="item gallery-card-wrapper" data-category="csw" data-src="assets/img/gallery-4.png"
                    data-sub-html="">
                    <img src="assets/img/gallery-4.png" alt="">
                </div>
                <div class="item gallery-card-wrapper" data-category="csw" data-src="assets/img/gallery-1.png"
                    data-sub-html="">
                    <img src="assets/img/gallery-1.png" alt="">
                </div>
                <div class="item gallery-card-wrapper" data-category="csw" data-src="assets/img/gallery-2.png"
                    data-sub-html="">
                    <img src="assets/img/gallery-2.png" alt="">
                </div>
                <div class="item gallery-card-wrapper" data-category="csw" data-src="assets/img/gallery-3.png"
                    data-sub-html="">
                    <img src="assets/img/gallery-3.png" alt="">
                </div>
                <div class="item gallery-card-wrapper" data-category="csw" data-src="assets/img/gallery-4.png"
                    data-sub-html="">
                    <img src="assets/img/gallery-4.png" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="more-packages  pb-800">
    <div class="container">
        <div class="row">
            <div class="col-12 heading">
                <h2>More Exciting Packages</h2>
            </div>
        </div>
        <div class="row list-wrapper">
            <div class="col-md-4 col-sm-6 col-xl-3 package-card-wrapper">
                <a href="package-detail.php" class="package-card d-block">
                    <div class="overlay"></div>
                    <div class="mask"></div>
                    <img src="assets/img/package1.png" alt="">
                    <div class="text-content">
                        <span class="title">Package Name</span>
                        <span class="info">5 days Max 10 People</span>
                    </div>
                </a>
            </div>
            <div class="col-md-4 col-sm-6 col-xl-3 package-card-wrapper">
                <a href="package-detail.php" class="package-card d-block">
                    <div class="overlay"></div>
                    <div class="mask"></div>
                    <img src="assets/img/package1.png" alt="">
                    <div class="text-content">
                        <span class="title">Package Name</span>
                        <span class="info">5 days Max 10 People</span>
                    </div>
                </a>
            </div>
            <div class="col-md-4 col-sm-6 col-xl-3 package-card-wrapper">
                <a href="package-detail.php" class="package-card d-block">
                    <div class="overlay"></div>
                    <div class="mask"></div>
                    <img src="assets/img/package1.png" alt="">
                    <div class="text-content">
                        <span class="title">Package Name</span>
                        <span class="info">5 days Max 10 People</span>
                    </div>
                </a>
            </div>
            <div class="col-md-4 col-sm-6 col-xl-3 package-card-wrapper">
                <a href="package-detail.php" class="package-card d-block">
                    <div class="overlay"></div>
                    <div class="mask"></div>
                    <img src="assets/img/package1.png" alt="">
                    <div class="text-content">
                        <span class="title">Package Name</span>
                        <span class="info">5 days Max 10 People</span>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>
<?php include("partials/footer.php")?>