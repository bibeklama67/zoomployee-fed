<?php include("partials/header.php")?>
<section class="welcome-banner position-relative">
    <img src="assets/img/welcome-bg.jpg" alt="">
    <div class="container d-flex h-100 align-items-center">
        <div class="row">
            <div class="col-xl-6 text-content">
                <span>Remote Staff
                    $7.00-$8.50/hour</span>
                <p class="text-white">We are the leading provider of remote admin, marketing, sales, bookkeeping,
                    design, customer support,
                    and e-commerce staff for small businesses.</p>
                <h6 class="text-white">No additional fees. Interview today. Cancel any time. </h6>
                <a href="#" class="btn btn-success">Get Started</a>
            </div>
        </div>
    </div>
</section>

<section class="contact-us intro">
    <div class="container">
        <div class="row">
            <div class="col-12 col-xl-9 mx-auto">
                <h2>
                    We Would Love to Hear From You
                </h2>
                <p>Please get in touch as soon as possible, we would love to help you free up your time so that you can
                    focus on high-value tasks that will help you grow your business instead of wasting your time on
                    administrative tasks and activities that are not well-matched with your skillset and preferences. We
                    will offer you fast and friendly support and advice, work with you to find the most cost-effective
                    solution for your needs, and never push you into committing. Let’s put our heads together to solve
                    your challenges and grow your business!
                </p>
                <a href="#" class="btn btn-success"><svg width="37" height="37" viewBox="0 0 37 37" fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd"
                            d="M31.025 5.24688C27.6031 1.825 23.0406 0 18.25 0C8.2125 0 0 8.2125 0 18.25C0 21.4438 0.912514 24.6375 2.50939 27.375L0 36.5L9.58128 33.9906C12.3188 35.3594 15.2844 36.2719 18.25 36.2719C28.2875 36.2719 36.5 28.0594 36.5 18.0219C36.5 13.2312 34.4469 8.66875 31.025 5.24688ZM18.25 33.3063C15.5125 33.3063 12.775 32.6219 10.4938 31.2531L10.0375 31.025L4.33436 32.6219L5.93126 27.1469L5.47499 26.4625C3.87811 23.9531 3.19374 21.2156 3.19374 18.4781C3.19374 10.2656 10.0375 3.42188 18.25 3.42188C22.3563 3.42188 26.0063 5.01875 28.9719 7.75625C31.9375 10.7219 33.3063 14.3719 33.3063 18.4781C33.3063 26.4625 26.6906 33.3063 18.25 33.3063ZM26.4625 21.9C26.0063 21.6719 23.725 20.5312 23.2688 20.5312C22.8125 20.3031 22.5843 20.3031 22.3562 20.7594C22.1281 21.2156 21.2157 22.1281 20.9875 22.5844C20.7594 22.8125 20.5312 22.8125 20.075 22.8125C19.6187 22.5844 18.25 22.1281 16.425 20.5312C15.0563 19.3906 14.1438 17.7938 13.9156 17.3375C13.6875 16.8813 13.9157 16.6531 14.1438 16.425C14.3719 16.1969 14.6 15.9687 14.8281 15.7406C15.0562 15.5125 15.0563 15.2844 15.2844 15.0563C15.5125 14.8281 15.2844 14.6 15.2844 14.3719C15.2844 14.1438 14.3719 11.8625 13.9156 10.95C13.6875 10.2656 13.2313 10.2656 13.0032 10.2656C12.775 10.2656 12.5468 10.2656 12.0906 10.2656C11.8625 10.2656 11.4062 10.2656 10.95 10.7219C10.4937 11.1781 9.35314 12.3187 9.35314 14.6C9.35314 16.8812 10.95 18.9344 11.1781 19.3906C11.4062 19.6187 14.3718 24.4094 18.9343 26.2344C22.8125 27.8312 23.4969 27.375 24.4094 27.375C25.3219 27.375 27.1469 26.2344 27.375 25.3219C27.8312 24.1813 27.8313 23.2688 27.6031 23.2688C27.375 22.1281 26.9188 22.1281 26.4625 21.9Z"
                            fill="white" />
                    </svg>
                    WhatsApp Us</a>
            </div>
        </div>
    </div>
</section>
<section class="contact-us form-section">
    <img src="assets/img/contact-vector1.png" alt="" class="vector1">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-md-5 text-content">
                <h2>Tell Us About Your Requirement</h2>
                <p>That you for your interest in working with us. So that we can find the best-fit, affordable staffing
                    solution for you, in the last field below, please include what tasks you are planning to allocate to
                    your remote staff, what are your preferred working hours for them, what key skills they should have
                    (for instance, knowledge of specific software packages), when do you need them to start, and
                    anything else you might want to share with us so that we can find the best solution for you. We will
                    review your requirements and send you a number of solutions to choose from that we think will give
                    you the best value for money.
                </p>
            </div>
            <form class="col-md-5 form-wrapper">
                <div class="form-group">
                    <label for="">Name (required)</label>
                    <div class="input-group"><svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                            xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M20 21V19C20 17.9391 19.5786 16.9217 18.8284 16.1716C18.0783 15.4214 17.0609 15 16 15H8C6.93913 15 5.92172 15.4214 5.17157 16.1716C4.42143 16.9217 4 17.9391 4 19V21"
                                stroke="#031629" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                            <path
                                d="M12 11C14.2091 11 16 9.20914 16 7C16 4.79086 14.2091 3 12 3C9.79086 3 8 4.79086 8 7C8 9.20914 9.79086 11 12 11Z"
                                stroke="#031629" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                        </svg>

                        <input type="text" class="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="">Email (required)</label>
                    <div class="input-group">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M4 4H20C21.1 4 22 4.9 22 6V18C22 19.1 21.1 20 20 20H4C2.9 20 2 19.1 2 18V6C2 4.9 2.9 4 4 4Z"
                                stroke="#031629" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                            <path d="M22 6L12 13L2 6" stroke="#031629" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" />
                        </svg>

                        <input type="text" class="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="">Phone (required)</label>
                    <div class="input-group">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M21.9999 16.92V19.92C22.0011 20.1985 21.944 20.4741 21.8324 20.7293C21.7209 20.9845 21.5572 21.2136 21.352 21.4018C21.1468 21.5901 20.9045 21.7335 20.6407 21.8227C20.3769 21.9119 20.0973 21.945 19.8199 21.92C16.7428 21.5856 13.7869 20.5341 11.1899 18.85C8.77376 17.3146 6.72527 15.2661 5.18993 12.85C3.49991 10.2412 2.44818 7.27097 2.11993 4.17997C2.09494 3.90344 2.12781 3.62474 2.21643 3.3616C2.30506 3.09846 2.4475 2.85666 2.6347 2.6516C2.82189 2.44653 3.04974 2.28268 3.30372 2.1705C3.55771 2.05831 3.83227 2.00024 4.10993 1.99997H7.10993C7.59524 1.9952 8.06572 2.16705 8.43369 2.48351C8.80166 2.79996 9.04201 3.23942 9.10993 3.71997C9.23656 4.68004 9.47138 5.6227 9.80993 6.52997C9.94448 6.8879 9.9736 7.27689 9.89384 7.65086C9.81408 8.02482 9.6288 8.36809 9.35993 8.63998L8.08993 9.90997C9.51349 12.4135 11.5864 14.4864 14.0899 15.91L15.3599 14.64C15.6318 14.3711 15.9751 14.1858 16.3491 14.1061C16.723 14.0263 17.112 14.0554 17.4699 14.19C18.3772 14.5285 19.3199 14.7634 20.2799 14.89C20.7657 14.9585 21.2093 15.2032 21.5265 15.5775C21.8436 15.9518 22.0121 16.4296 21.9999 16.92Z"
                                stroke="#031629" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                        </svg>

                        <input type="text" class="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="">Your company name and website (if any)</label>
                    <div class="input-group">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M3 3L10.07 19.97L12.58 12.58L19.97 10.07L3 3Z" stroke="#031629" stroke-width="2"
                                stroke-linecap="round" stroke-linejoin="round" />
                            <path d="M13 13L19 19" stroke="#031629" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" />
                        </svg>

                        <input type="text" class="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="">How did you hear about us ? (required)</label>
                    <div class="input-group">
                        <select class="form-control">
                            <option value="">Search Engine</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="">Please tell us about your staffing requirements: (required)</label>
                    <div class="input-group">
                        <textarea class="form-control" name="" id="" cols="30" rows="30"></textarea>
                    </div>
                </div>
                <div class="col-12">
                    <button class="btn btn-success d-block w-100">Submit</button>
                </div>
            </form>
        </div>
    </div>
</section>
<section class="contact-us business-hours">
    <div class="container">
        <div class="row">
            <div class="col-xl-5 mx-auto">
                <h2>Our Business Hours</h2>
                <p>Our head office is located in Miami, Florida and all hours below are EST. Please don’t hesitate to
                    WhatsApp us outside of these hours and we will respond as soon as possible. Alternatively, please
                    email:<strong> info@zoomployee.com</strong>. Thank you!
                </p>
                <div class="info-wrapper styled-list text-content">
                    <ul>
                        <li>
                            <h5> <svg width="12" height="20" viewBox="0 0 12 20" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M2 2L10 10L2 18" stroke="url(#paint0_linear_30_197)" stroke-width="2.5"
                                        stroke-linecap="round" stroke-linejoin="round" />
                                    <defs>
                                        <linearGradient id="paint0_linear_30_197" x1="6" y1="2" x2="6" y2="18"
                                            gradientUnits="userSpaceOnUse">
                                            <stop stop-color="#F03B7A" />
                                            <stop offset="1" stop-color="#7372C0" />
                                        </linearGradient>
                                    </defs>
                                </svg>Monday</h5>
                            <span>9:00 am - 7:00 pm</span>
                        </li>
                        <li>
                            <h5> <svg width="12" height="20" viewBox="0 0 12 20" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M2 2L10 10L2 18" stroke="url(#paint0_linear_30_197)" stroke-width="2.5"
                                        stroke-linecap="round" stroke-linejoin="round" />
                                    <defs>
                                        <linearGradient id="paint0_linear_30_197" x1="6" y1="2" x2="6" y2="18"
                                            gradientUnits="userSpaceOnUse">
                                            <stop stop-color="#F03B7A" />
                                            <stop offset="1" stop-color="#7372C0" />
                                        </linearGradient>
                                    </defs>
                                </svg>Monday</h5>
                            <span>9:00 am - 7:00 pm</span>
                        </li>
                        <li>
                            <h5> <svg width="12" height="20" viewBox="0 0 12 20" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M2 2L10 10L2 18" stroke="url(#paint0_linear_30_197)" stroke-width="2.5"
                                        stroke-linecap="round" stroke-linejoin="round" />
                                    <defs>
                                        <linearGradient id="paint0_linear_30_197" x1="6" y1="2" x2="6" y2="18"
                                            gradientUnits="userSpaceOnUse">
                                            <stop stop-color="#F03B7A" />
                                            <stop offset="1" stop-color="#7372C0" />
                                        </linearGradient>
                                    </defs>
                                </svg>Monday</h5>
                            <span>9:00 am - 7:00 pm</span>
                        </li>
                        <li>
                            <h5> <svg width="12" height="20" viewBox="0 0 12 20" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M2 2L10 10L2 18" stroke="url(#paint0_linear_30_197)" stroke-width="2.5"
                                        stroke-linecap="round" stroke-linejoin="round" />
                                    <defs>
                                        <linearGradient id="paint0_linear_30_197" x1="6" y1="2" x2="6" y2="18"
                                            gradientUnits="userSpaceOnUse">
                                            <stop stop-color="#F03B7A" />
                                            <stop offset="1" stop-color="#7372C0" />
                                        </linearGradient>
                                    </defs>
                                </svg>Monday</h5>
                            <span>9:00 am - 7:00 pm</span>
                        </li>
                        <li>
                            <h5> <svg width="12" height="20" viewBox="0 0 12 20" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M2 2L10 10L2 18" stroke="url(#paint0_linear_30_197)" stroke-width="2.5"
                                        stroke-linecap="round" stroke-linejoin="round" />
                                    <defs>
                                        <linearGradient id="paint0_linear_30_197" x1="6" y1="2" x2="6" y2="18"
                                            gradientUnits="userSpaceOnUse">
                                            <stop stop-color="#F03B7A" />
                                            <stop offset="1" stop-color="#7372C0" />
                                        </linearGradient>
                                    </defs>
                                </svg>Monday</h5>
                            <span>9:00 am - 7:00 pm</span>
                        </li>
                        <li>
                            <h5> <svg width="12" height="20" viewBox="0 0 12 20" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M2 2L10 10L2 18" stroke="url(#paint0_linear_30_197)" stroke-width="2.5"
                                        stroke-linecap="round" stroke-linejoin="round" />
                                    <defs>
                                        <linearGradient id="paint0_linear_30_197" x1="6" y1="2" x2="6" y2="18"
                                            gradientUnits="userSpaceOnUse">
                                            <stop stop-color="#F03B7A" />
                                            <stop offset="1" stop-color="#7372C0" />
                                        </linearGradient>
                                    </defs>
                                </svg>Monday</h5>
                            <span>9:00 am - 7:00 pm</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="contact-us map-section">
    <iframe
        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3533.301503603128!2d85.31521531479993!3d27.677074782804393!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb18fe1ce61fa1%3A0x333ae24f5072a69f!2sMakura%20Creations!5e0!3m2!1sen!2snp!4v1640325934282!5m2!1sen!2snp"
        width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
</section>
<section class="contact-us notice-wrapper">
    <div class="container">
        <div class="row justify-content-between align-items-center">
            <div class="col-md-6 col-xl-7 text-content">
                <span>
                    If you are anything like us, you hate reaching out to companies with great websites because they
                    will call and email you to sell you services you don’t need for weeks afterwards. We promise that we
                    will never do that.
                </span>
            </div>
            <div class="col-md-6 col-xl-4">
                <a class="btn btn-success" href="#">Find Out More About Us</a>
            </div>
        </div>
    </div>
</section>
<?php include("partials/footer.php")?>